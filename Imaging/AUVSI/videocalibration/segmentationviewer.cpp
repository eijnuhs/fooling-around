// $Id: segmentationviewer.cpp,v 1.1.1.1.2.14 2005/01/23 05:24:37 cvs Exp $
//

#include "segmentationviewer.h"
#include <qpainter.h>
#include <sstream>
#ifdef DEBUG
#include <iostream>
#endif
#include <qcheckbox.h>
#include "segmentation.h"
#include <qtextedit.h>
#include <qstring.h>
#include <qspinbox.h>
#include <qcombobox.h>
#include "videointerpreter2d.h"
#include <qevent.h>
#include <qlabel.h>
#include "paintbox.h"
#include <qmessagebox.h>

SegmentationViewer::SegmentationViewer( QWidget * parent, char const * name ) : FrameBufferViewer( parent, name ), segmentation( 0 ), showX( 0 ), showY( 0 )
{
}

void
SegmentationViewer::mousePressEvent( QMouseEvent * e )
{
  double scaleX = static_cast< double> ( segmentation->segmentedFrame.width ) / static_cast< double> ( width() );
  double scaleY = static_cast< double> ( segmentation->segmentedFrame.height ) / static_cast< double> ( height() );

  showX = static_cast<int>( scaleX * static_cast<double>( e->x() ) );
  showY = static_cast<int>( scaleY * static_cast<double>( e->y() ) );
  
  FrameBufferViewer::mousePressEvent( e );
}

void
SegmentationViewer::paintChild( QPainter & p)
{
  if ( segmentation != 0 )
    {
      QString s;      
      QMutexLocker qml(&segmentation->mutex);
      s = QString( "Number of Regions %1" ).arg(segmentation->regionList.size());

      for(uint i = 0; i < segmentation->regionList.size(); ++i)
	{
	  if(segmentation->regionList[i].numPixels > 1)
	    {
	      drawRegion(p, segmentation->regionList[i]);
	    }
	}
           
      mainWindow->txtROI->setText( s );
    }  
}

void SegmentationViewer::drawRegion(QPainter &p, const RunRegion & r)
{
  int width, height,x,y,centreX,centreY;
  QString label(QString::number(r.numPixels));
  p.setPen(Qt::blue);
  centreX = r.getCOMXI();
  centreY = r.getCOMYI();
  width = r.xMax - r.xMin + 1;
  height = r.yMax - r.yMin + 1;
  frameToWidgetCoordinates((int)r.xMin,(int)r.yMin,&x,&y);
  frameToWidgetCoordinates(width,height,&width,&height);
  frameToWidgetCoordinates(centreX,centreY,&centreX,&centreY);
  p.drawRect(x,y, width, height);
  
  PaintBox::drawPoint( p, centreX, centreY,  5, label, 2);

  //  std::cout << r;
  //  std::cout << "Widget coords: (" << y << "," << x << "," << height << "," << width << ")\n";
}

// void 
// SegmentationViewer::newFrame()
// {
//   updateQImageFromFrameBuffer( & segmentation->segmentedFrame );
// }


void 
SegmentationViewer::setSegmentation( Segmentation * seg )
{
  this->segmentation = seg;
  if(segmentation)
    {
      //      segmentation->setMaxLength((unsigned int)maxL);
      //      segmentation->setBlurring(mainWindow->chkBlurSegmentation->isChecked());
      //      segmentation->setFilterRegions(mainWindow->chkFilterRegions->isChecked());
      mainWindow->spnThreshold->setValue(segmentation->getThreshold());
      mainWindow->spnIntThreshold->setValue(segmentation->getIntThreshold());      
    }
}

void SegmentationViewer::setMode(const QString & mode)
{
  int modeCode = 0; 
  if(mode == "Segmentation")
    {
      modeCode = SM_SEGMENTATION;
    }
  else if(mode == "Collect Bg")
    {
      modeCode = SM_MOTION;
    }
  else if(mode == "Scanlines")
    {
      modeCode = SM_SCANLINES;
    }
  else if(mode == "Bg Remove")
    {
      modeCode = SM_SEG_AND_MOTION;
    }
  segmentation->setMode(modeCode);
}

void SegmentationViewer::setThreshold(int threshold)
{
  segmentation->setThreshold(threshold);
}
void SegmentationViewer::setIntThreshold(int threshold)
{
  segmentation->setIntThreshold(threshold);
}



void SegmentationViewer::printName()
{
  std::cout << "SegmentationViewer\n";
}

void SegmentationViewer::saveBackground()
{
  segmentation->saveBackground();
  QMessageBox::information ( this, "Ergo::Message", "Background information saved.", QMessageBox::Ok);
}

#if 0
void SegmentationViewer::enableFiltering(bool enabled)
{
  segmentation->setFilterRegions(enabled);
}
void SegmentationViewer::enableBlur(bool enabled)
{
  segmentation->setBlurring(enabled);
}

void SegmentationViewer::setMMPerPixel(int mm)
{
  segmentation->setMMPerPixel((unsigned int)mm);
}
void SegmentationViewer::setMaxLength(int maxL)
{
  segmentation->setMaxLength((unsigned int)maxL);
}
void SegmentationViewer::setMinLength(int minL)
{
  segmentation->setMinLength((unsigned int)minL);
}

#endif

