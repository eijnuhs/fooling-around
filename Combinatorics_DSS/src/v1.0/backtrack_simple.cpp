#include "backtrack_simple.h"

#define DEBUG 0
#define START 16
#define STOP  120
/*
 * Make use of a column farm to 
 * */

using namespace std;

enum {MATRIX = 0, NUMERIC};
enum {ADDONE=0, WEIGHTED};

DSS_Matrix::DSS_Matrix(unsigned int rows)
{
	N 					= rows;
	c_index 			= 0;
	P 					= (1 << rows) - 2;	//all 0s and 1s are ignored 
	col_farm 			= NULL;
	weighted_col_farm 	= NULL;
	matrix				= NULL;
	createMatrix(rows);
	found = 0;
	
}

/*
 * Iterative Backtracking algorithm. Only hunts for M thus
 * 1 solution is enough
 * 
 * */
void
DSS_Matrix::backtrack_iterative(){
	
}

/*
 * Recursive Backtracking algorithm
 * !!! Naive approach !!!
 * 
 * */
void
DSS_Matrix::backtrack_recursive(int type){
	switch (type){
		case ADDONE:
			for(unsigned int first = 1;first<P;first=(first << 1) | 1){
				matrix[0]=first;
				backtrack_recursive_ADDONE(1);
			}
			break;
		case WEIGHTED:
			for(unsigned int first = 1;first<P;first=(first << 1) | 1){
				matrix[0]=first;
				cout << "Doing weight " << first << endl;
				backtrack_recursive_WEIGHTED(1);				
			}
			break;
	}
}
void 
DSS_Matrix::backtrack_recursive_WEIGHTED(unsigned int col_index){
	//found solution
	if(col_index == M){
		found ++;
		if(found == 1){
			printMatrix(NUMERIC);
		}
		return;
	}
	//continue searching
	unsigned int start_index,weight,prev_col;
	prev_col = matrix[col_index-1];
	weight= bitsSet(prev_col) - 1;
	for(unsigned int i=0;i<P && weight < N-1;i++){
		unsigned int c = weighted_col_farm[weight][i];
		if(c == 0){
			weight++;
			i=0;
			continue;
		}
		if(c > prev_col){
			start_index = i;
			break;
		}
	}
	weight= bitsSet(prev_col) - 1;
	for(unsigned int i = start_index;i < P && weight < N-1; i++){
		unsigned int c = weighted_col_farm[weight][i];
		if(c == 0){
			weight++;
			i=0;
			continue;
		}
		//checking
		if(isValidMatrix(c,col_index)){
			//col_index;
			matrix[col_index] = c;
			backtrack_recursive_WEIGHTED(col_index+1);
			//return only 1 solution
			//if(found > 0) return;
		}		
	}
}
void 
DSS_Matrix::backtrack_recursive_ADDONE(unsigned int col_index){
	//found solution
	if(col_index == M){
		found ++;
		if(found == 1){
			printMatrix(NUMERIC);
		}
		return;
	}
	//continue searching
	unsigned int start = matrix[col_index-1] + 1;
	for(unsigned int i = start;i < P; i = i+1){
		//checking
		if(isValidMatrix(i,col_index)){
			//col_index;
			matrix[col_index] = i;
			backtrack_recursive_ADDONE(col_index+1);
			//return only 1 solution
			if(found > 0) return;
		}		
	}
}
unsigned int**
DSS_Matrix::generateWeightedColumn(){
	//dellocate memory	
	if(weighted_col_farm != NULL){
		int n_cols = sizeof(weighted_col_farm);
		for( int i = 0 ; i < n_cols; i++ )
			delete[] weighted_col_farm[i];
		delete[] weighted_col_farm;
	}
	
	//columns assigned by weight
	weighted_col_farm = new unsigned int *[N-1];
	if (weighted_col_farm == NULL) return NULL;
	
	for (unsigned int i = 0; i < N-1; i++){
		weighted_col_farm[i] = new unsigned int[P];
		//initialized to 0
		for(unsigned int j = 0;j < P;j++){
			weighted_col_farm[i][j] = 0;
		}
	}
		
	//keep track of last index
	int last_index[N-1];
	for(unsigned int i = 0;i<N-1;i++){
			last_index[i] = 0;
	}
	//simple sorting into bins
	unsigned int n,w;
	for(unsigned int i=0;i<P;i++){
		n = i+1;
		w = bitsSet(n);
		weighted_col_farm[w-1][last_index[w-1]++] = n;		
	}
	
	/*
	for(int i = 0;i<N-1;i++){
		for(int j=0;j<P && weighted_columns[i][j] != 0;j++){
			cout << weighted_columns[i][j] << " ";
		}
		cout << endl;
	}*/
	
	return weighted_col_farm;
}
/*
 * Generate all the possible columns given N this allows 
 * forward checking and constraint progation to take place
 * 
 * (!!! not implement in this version !!!)
 * Skips repeated columns for example 1100 and 0011 since rows are 
 * interchangeable 
 * */
void 
DSS_Matrix::generateColumnFarm()
{
	//dellocate memory	
	if(col_farm != NULL){
		int n_cols = sizeof(col_farm);
		for( int i = 0 ; i < n_cols; i++ )
			delete[] col_farm[i];
		delete[] col_farm;
	}
	
	//allocate new memory
	col_farm = new unsigned int *[M];
	if (col_farm != NULL) {
		for (unsigned int i = 0; i < M; i++) {
			col_farm[i] = new unsigned int[P];
		}
	}
	
	//initializing the column farms.
	for(unsigned int i = 0; i < M ; i++){
		resetColumnFarm(i);
	}
}
/*
 * Generate individual columns
 * */
void 
DSS_Matrix::resetColumnFarm(unsigned int index){
	if(col_farm == NULL)
		return;
	
	if(DEBUG == 1){
		cout << "farm index " << index << " resetting" << endl;
	}
	
	// reset column farm index
	for(unsigned int j = 0; j < P; j++){
		col_farm[index][j] = j+1;
	}
}

void
DSS_Matrix::createMatrix(unsigned int col_size)
{
	M = col_size;
	found = 0;
	if(matrix != NULL)
		delete[] matrix;
	
	//new matrix
	matrix = new unsigned int[M];
	
	//initialize to zero
	for(unsigned int i = 0;i < M;i++)	matrix[i] = 0;
}

void 
DSS_Matrix::printMatrix(int type)
{
	if(matrix == NULL)
		return;
	
	if(type == NUMERIC){
		cout << "[ ";
		for(unsigned int i = 0; i < M; i++){
			cout << matrix[i] << " ";
		}
		cout << "]" << endl;
	}
	else if(type == MATRIX){
		
	}
}

//Testing functions

/*
 * This assumes the current matrix is correct and only test the new
 * column input. Returns true if matrix smaller than 3
 * 
 * test_matrix 		the matrix to test
 * input			the new column input to test
 * col				the column index
 **/
//todo combine both type of testing matrix together
bool 
DSS_Matrix::isValidMatrix(unsigned int input,unsigned int col_index){
	if(DEBUG == 1)
		cout << "current index " << col_index << endl;
	
	//test pair
	for(unsigned int i = 0; i < col_index; i++){
		if(checkPair(matrix[i],input) == false)
			return false;
	}
	if(DEBUG == 1)
		cout << "test pair ok" << endl;
	
	//test triple
	for(unsigned int i = 0; col_index > 0 && i < col_index-1; i++){
		for(unsigned int j = i+1; j < col_index; j++){
			if(checkTriple(matrix[i],matrix[j],input) == false)
				return false;
			
		}
	}
	if(DEBUG == 1)
		cout << "test triple ok" << endl;
	return true;	
}

bool 
DSS_Matrix::isValidMatrix()
{
	return testRulePair(matrix) && testRuleTriple(matrix);
}

/* Simple search through every pair 
 * This uses bitwise operations
 * */
bool 
DSS_Matrix::checkPair(unsigned int i, unsigned int j){
	unsigned a = i & j;
	if(a != i && a != j)
		return true;
	else 
		return false;
}
bool
DSS_Matrix::checkTriple(unsigned int i, unsigned int j, unsigned int k)
{
	unsigned a = i ^ j ^ k ^ (i & j & k);
	if(a != 0)
		return true;
	else 
		return false;
}
bool 
DSS_Matrix::testRulePair(unsigned int * test_matrix)
{
	for(unsigned int i = 0; i < M-1; i++){
		for(unsigned int j = i+1; j < M; j++){
			if(checkPair(test_matrix[i],test_matrix[j]) == false)
				return false;
		}
	}
	return true;
}
bool 
DSS_Matrix::testRuleTriple(unsigned int * test_matrix)
{
	for(unsigned int i = 0; i < M-2; i++){
		for(unsigned int j = i+1; j < M-1; j++){
			for(unsigned int k = j+1; k < M; k++){
				if(checkTriple(test_matrix[i],test_matrix[j],test_matrix[k]) == false)
					return false;
			}
		}
	}
	return true;
}	
void 
DSS_Matrix::setMatrix(unsigned int * new_matrix,unsigned int col_size, unsigned int row_size)
{
	resetMatrix(col_size,row_size);
	matrix = new_matrix;
}

void 
DSS_Matrix::resetMatrix(unsigned int col_size, unsigned int row_size)
{
	if(matrix != NULL)
		delete[] matrix;
	
	matrix 		= NULL;	
	M 			= col_size;
	N 			= row_size;
	c_index 	= col_size;
	P 			= (1 << row_size) -2;	//all 0s and 1s are ignored 
	found 		= 0;
}

unsigned int
DSS_Matrix::solutionFound(){
	return found;
}
void
DSS_Matrix::printMatrixSize(){
	cout 	<< "M = " << M << " N = " << N << endl;
}
void
DSS_Matrix::setMValue(unsigned int m){
	M = m;
}
int 
main()
{
	int n_size = 7;
	DSS_Matrix *bt = new DSS_Matrix(n_size);
	bt->generateWeightedColumn();
	int i;
	for(i = START;i < STOP;i++){
		bt->createMatrix(i);
		bt->printMatrixSize();
		bt->backtrack_recursive(WEIGHTED);
		//bt->backtrack_iterative();
		cout << "total found " << bt->solutionFound() << endl<< endl;
		if(bt->solutionFound() == 0) {
			break;
		}
	}
	cout << "Largest M FOR N = " << n_size << " is " << i -1 << endl;
}

