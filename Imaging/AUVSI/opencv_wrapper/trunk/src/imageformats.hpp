#ifndef _IMAGE_FORMATS_H
#define _IMAGE_FORMATS_H

#include <inttypes.h>
#ifndef V4L_COMPATIBILITY
#ifndef __user
#define __user
#endif
#include <asm/mman.h>
#include <asm/errno.h>
#include <unistd.h>
#include <asm/types.h>
#include <linux/videodev2.h>
//#include <linux/videodev.h>
#else
#include "v4lcompatibility.hpp"
#endif



#include <string>

// A short list of (hopefully) supported image formats
namespace ImageFormats{
  typedef  enum
    {
      RGB565 = 0,
      RGB24,
      BGR24,
      RGB32,
      BGR32,
      GREY,
      UNSUPPORTED,
      COUNT
    } ImageFormat;
  
  typedef enum
    {
      TOP,
      BOTTOM,
      INTERLACED
    } Fields;
  
  static const char * STRINGS[COUNT] = {"rgb565", "rgb24", "bgr24", "rgb32", "bgr32", "grey", "Unsupported format"};
  static const uint32_t V4L2[COUNT - 1] = { V4L2_PIX_FMT_RGB565, V4L2_PIX_FMT_RGB24, V4L2_PIX_FMT_BGR24, V4L2_PIX_FMT_RGB32, V4L2_PIX_FMT_BGR32, V4L2_PIX_FMT_GREY};
  
  inline const char * getFormatString(ImageFormat fmt)
  {
    if(fmt < COUNT)
      return STRINGS[fmt];
    else 
      return "ImageFormats::getFormatString() passed index out of range.\n";
  }
  
}
#endif

