#ifndef STRING_ROUTINES_H
#define STRING_ROUTINES_H

#include <string>
#include "error.hpp"

namespace umbotica
{
  /// returns the extension of a filename. or "" if the filname
  /// contains no dot
  inline std::string extension(const std::string & filename)
  {
    std::string result;
    // find the dot
    std::string::size_type dotpos = filename.rfind('.');
    if(dotpos != std::string::npos)
      result = filename.substr(dotpos + 1);
    
    return result;
  }
  

  /// returns the filename with the extension removed or
  /// the whole filename if it contains no dot.
  inline std::string basefilename(const std::string & filename)
  {
    std::string result = filename;
    // find the dot
    std::string::size_type dotpos = filename.rfind('.');
    if(dotpos != std::string::npos)
      result = filename.substr(0, dotpos);
    
    return result;
  }

  /// replaces the 'target' with 'replacement' searching forward through
  /// the string and skipping 'skip' occurences
  inline std::string replace(std::string str, char target, char replacement, unsigned int skip = 0)
  {
    std::string::size_type pos = 0;
    while( (pos = str.find(target, pos)) != std::string::npos)
      {
	if(skip)
	  {
	    skip--;
	    pos++;
	  }
	else
	  str[pos] = replacement;
      }
    return str;
  }

  /// replaces the 'target' with 'replacement' searching backward through
  /// the string and skipping 'skip' occurences
  inline std::string replace_reverse(std::string str, char target, char replacement, unsigned int skip = 0)
  {
    std::string::size_type pos = std::string::npos;
    while( (pos = str.rfind(target, pos)) != std::string::npos)
      {
	if(skip)
	  {
	    skip--;
	    pos--;
	  }
	else
	  str[pos] = replacement;
      }
    return str;
  }
}

#endif

