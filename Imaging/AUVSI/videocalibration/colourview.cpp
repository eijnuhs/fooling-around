#include "colourview.h"
#include <qpainter.h>
#include <qpen.h>
#include <qbrush.h>
#include <iostream>

ColourView::ColourView(QWidget * parent, const char * name) : QWidget(parent,name)
{

}
ColourView::~ColourView()
{

}

void ColourView::paintEvent ( QPaintEvent * pe)
{
  QPainter p(this);
  //  std::cout << "painting ColourView\n";

  p.setPen(Qt::black);
  p.fillRect(0,0,width(),height(), QBrush(colour));
  p.drawRect(0,0,width(),height());
}
void ColourView::setColour(const QColor & c)
{
  //  std::cout << "ColourView got new colour.\n";
  colour = c;
  update();
}
