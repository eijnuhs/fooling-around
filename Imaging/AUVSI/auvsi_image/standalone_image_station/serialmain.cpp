/*
  Runs the gcslogger which records raw serial stream
  and records the interpreted telemetry in the db.
  Start from command line using something like:
  telemetryLogger --input=/dev/ttyS0
*/

#include <csignal>
#include <string>
#include <iostream>
#include "boost/program_options.hpp"
#include "gcslogger.hpp"
#include "sqlinterface.hpp"
#include "boost/date_time/posix_time/posix_time.hpp"

namespace po = boost::program_options;

static void sig_handler(int);

int main(int argc, char* argv[])
{
  po::options_description desc;
  desc.add_options()
    ("input", po::value<std::string>(), "set input device")
    ;

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  std::cout << "input file is: " << vm["input"].as<std::string>() << std::endl;

  signal(SIGINT, sig_handler);

  boost::posix_time::ptime reportTime = boost::posix_time::second_clock::local_time();
  std::string logFileName = std::string("raw-" + boost::posix_time::to_iso_string(reportTime) + ".log");
  std::cout << "logging to: " << logFileName << std::endl;

  std::cout << "setting up logger...\n";
  GCSLogger<SqlInterface> logger(vm["input"].as<std::string>(), logFileName);
  std::cout << "logger ready\n";
  
  if (logger.okay())
    {
      while (true)
	{
	  logger.recordNextPacket();
	}
    }
  
  return 0;
}

static void sig_handler(int)
{
  std::cout << "Program interupted\n";

  exit(0);	
}
