#ifndef _UM_IMAGE_TRAITS_H_
#define _UM_IMAGE_TRAITS_H_

#include <inttypes.h>
#include <vigra/numerictraits.hxx>
#include <vigra/accessor.hxx>
#include "umaccessors.hpp"
#include "imageformats.hpp"
#include "bgrvalue.hxx"
#include "bgravalue.hxx"
#include "rgb565value.hxx"
#include <boost/lexical_cast.hpp>
#include <cv.h>

namespace umbotica {

  template<typename IsIntegral>
  struct FormatTraits;

  template<>
  struct FormatTraits<vigra::VigraTrueType>
  {
    template<typename T>
    static std::string format(T val){return boost::lexical_cast<std::string>((int)val);}
  };

  template<>
  struct FormatTraits<vigra::VigraFalseType>
  {
    template<typename T>
    static std::string format(T val){return boost::lexical_cast<std::string>(val);}
  };

  template<class FundamentalPixelType>
  struct CvTraits;
  
  template<>
  struct CvTraits<uint8_t>
  {
    enum{ Depth = IPL_DEPTH_8U };
  };

  template<>
  struct CvTraits<int8_t>
  {
    enum{ Depth = IPL_DEPTH_8S };
  };
  
  template<>
  struct CvTraits<int16_t>
  {
    enum{ Depth = IPL_DEPTH_16S };
  };

  template<>
  struct CvTraits<int32_t>
  {
    enum{ Depth = IPL_DEPTH_32S };
  };
  
  template<>
  struct CvTraits<float>
  {
    enum{ Depth = IPL_DEPTH_32F };
  };
  
  template<>
  struct CvTraits<double>
  {
    enum{ Depth = IPL_DEPTH_64F };
  };

  // when more pixels are added we'll add more v4l2 types
  template<class PixelType>
  struct V4L2Traits
  {
    enum { Format = 0};
    enum { ImageFormat = ImageFormats::UNSUPPORTED };
    static const char * cvColorModel();
    static const char * cvChannelSeq();
  };
  

  template<>
  struct V4L2Traits<vigra::RGBValue<uint8_t> >
  {
    enum { Format = V4L2_PIX_FMT_RGB24 };
    enum { ImageFormat = ImageFormats::RGB24 };
    static const char * cvColorModel(){return "RGB ";}
    static const char * cvChannelSeq(){return "RGB ";}
  };

  template<>
  struct V4L2Traits<vigra::RGBAValue<uint8_t> >
  {
    enum { Format = V4L2_PIX_FMT_RGB32 };
    enum { ImageFormat = ImageFormats::RGB32 };
    static const char * cvColorModel(){return "RGB ";}
    static const char * cvChannelSeq(){return "ARGB";}
  };

  template<>
  struct V4L2Traits<vigra::BGRAValue<uint8_t> >
  {
    enum { Format = V4L2_PIX_FMT_BGR32 };
    enum { ImageFormat = ImageFormats::BGR32 };
    static const char * cvColorModel(){return "RGB ";}
    static const char * cvChannelSeq(){return "ABGR ";}
  };

  template<>
  struct V4L2Traits<vigra::BGRValue<uint8_t> >
  {
    enum { Format = V4L2_PIX_FMT_BGR24 };
    enum { ImageFormat = ImageFormats::BGR24 };
    static const char * cvColorModel(){return "RGB ";}
    static const char * cvChannelSeq(){return "BGR ";}
  };

  template<>
  struct V4L2Traits<uint8_t>
  {
    enum { Format = V4L2_PIX_FMT_GREY };
    enum { ImageFormat = ImageFormats::GREY};
    static const char * cvColorModel(){return "GREY";}
    static const char * cvChannelSeq(){return "GREY";}
  };


  template<class PixelType, int NumChannels>
  struct MultiChannelPixelTraits;
  
  template<class PixelType>
  struct MultiChannelPixelTraits<PixelType, 4>
  {
    typedef typename umbotica::RGBAccessor<PixelType> RGBAccessorType;
    typedef typename umbotica::RGBToGrayAccessor<PixelType> GrayAccessorType;
    typedef typename umbotica::RGBAAccessor<PixelType> RGBAAccessorType;
  };
  
  

  template<class PixelType>
  struct MultiChannelPixelTraits<PixelType, 3>
  {
    typedef typename umbotica::RGBAccessor<PixelType> RGBAccessorType;
    typedef typename umbotica::RGBToGrayAccessor<PixelType> GrayAccessorType;
    typedef typename umbotica::RGBToRGBAAccessor<PixelType> RGBAAccessorType;
  };
    

  template<class PixelType, class IsScalar>
  struct BasePixelTraits;
  
  template<class PixelType>
  struct BasePixelTraits<PixelType, vigra::VigraTrueType>
  {
    enum { NumberOfChannels = 1 };
    enum { CvDepth = CvTraits<PixelType>::Depth };
    typedef PixelType component_type;
    typedef umbotica::GrayToRGBAccessor<PixelType> RGBAccessorType;
    typedef vigra::StandardAccessor<PixelType> GrayAccessorType;
    typedef umbotica::GrayToRGBAAccessor<PixelType> RGBAAccessorType;
  };
  
  template<class PixelType>
  struct BasePixelTraits<PixelType, vigra::VigraFalseType>
  {
    enum { NumberOfChannels = PixelType::static_size };
    enum { CvDepth = CvTraits< typename PixelType::value_type >::Depth };
    typedef typename PixelType::value_type component_type;
    typedef typename MultiChannelPixelTraits<PixelType, NumberOfChannels>::RGBAccessorType RGBAccessorType;
    typedef typename MultiChannelPixelTraits<PixelType, NumberOfChannels>::GrayAccessorType GrayAccessorType;
    typedef typename MultiChannelPixelTraits<PixelType, NumberOfChannels>::RGBAAccessorType RGBAAccessorType;
  };

  template<>
  struct BasePixelTraits<vigra::Rgb565Value, vigra::VigraFalseType>
  {
    enum { NumberOfChannels = 3 };
    enum { CvDepth = CvTraits< vigra::Rgb565Value::value_type >::Depth };
    typedef vigra::Rgb565Value::value_type component_type;
    typedef vigra::SimpleRGBAccessor<vigra::Rgb565Value> RGBAccessorType;
    typedef umbotica::RGBToGrayAccessor<vigra::Rgb565Value> GrayAccessorType;
    typedef vigra::SimpleRGBToRGBAAccessor<vigra::Rgb565Value> RGBAAccessorType;
  };
  
  
  template<class PixelType>
  struct PixelTraits
  {
    typedef BasePixelTraits<PixelType, typename vigra::NumericTraits<PixelType>::isScalar> traits;
    enum { NumberOfChannels = traits::NumberOfChannels };
    enum { CvDepth =  traits::CvDepth };
    typedef typename traits::component_type component_type;
    typedef typename traits::RGBAccessorType RGBAccessor;
    typedef typename traits::GrayAccessorType GrayAccessor;
    typedef typename traits::RGBAAccessorType RGBAAccessor;
    enum { BytesPerPixel = sizeof(PixelType)};
    static std::string description(){return boost::lexical_cast<std::string>((int)traits::NumberOfChannels) + " channel " + typeid(component_type).name();}
  };
  
} // namespace umbotica


#endif
