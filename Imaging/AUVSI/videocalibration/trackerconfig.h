#ifndef __TRACKER_CONFIG_H_
#define __TRACKER_CONFIG_H_

#include "configuration.h"
#include <vector>


class Trackable;
class ColourMap;
class Ball;

class TrackerConfig : public Configuration
{
 public:
  TrackerConfig(ColourMap * cm = 0);
  ~TrackerConfig();
  std::vector<Trackable *> getBotList();
  Ball * getBall();
 private:
  ColourMap * cmap;
  Ball * ball;
  bool loadedBotList;
};

#endif
