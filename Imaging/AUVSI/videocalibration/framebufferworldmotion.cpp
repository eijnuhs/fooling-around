#include "framebufferworldmotion.h"
#include <assert.h>
#include <qpainter.h>
#include <iostream>
#include <fstream>
#include "run.h"

// the minimum variance (also the variance step value)
#define V_MIN 50 
#define PADDING 2

FrameBufferWorldMotion::FrameBufferWorldMotion( double mmperpix, PlayingField field) :
  FrameBufferWorld(mmperpix, field) ,
  pixelVariance(height,width),
  medianIntensity(CELL_DIM+PADDING,CELL_DIM+PADDING, 127),
  initialized(false),
  varianceThresholds(height,width),
  intensityThresholds(CELL_DIM+PADDING,CELL_DIM+PADDING, 127)
{
  initCMask();
}

FrameBufferWorldMotion::FrameBufferWorldMotion() :
  FrameBufferWorld(),
  pixelVariance(),
  medianIntensity(CELL_DIM+PADDING,CELL_DIM+PADDING, 127),
  initialized(false),
  varianceThresholds(height,width),
  intensityThresholds(CELL_DIM+PADDING,CELL_DIM+PADDING, 127)

{
  initCMask();
}

void FrameBufferWorldMotion::initCMask()
{
  int cmask[3][3] = { {1,1,1},
		      {1,2,1},
		      {1,1,1} };
  divisor = 0;
  for(uint i = 0; i < 3; i++)
    for(uint j = 0; j < 3; j++)
	divisor += cm[i][j] = cmask[i][j];
}

void FrameBufferWorldMotion::initialize( double mmperpix, PlayingField field )
{
  FrameBufferWorld::initialize(mmperpix,field);
  pixelVariance.initialize(height,width);
  varianceThresholds.initialize(height,width);
}

FrameBufferWorldMotion::~FrameBufferWorldMotion( )
{
  // 0
}
  
void FrameBufferWorldMotion::addBackgroundData(FrameBuffer * inFrame)
{
  RawPixel thisPixel, inPixel;
  unsigned int variance;
  unsigned int sse; // sum squared error
  // variables for calculating median intensity.
  unsigned int intensity;
  unsigned int medIntensity;
  unsigned int iRow;
  unsigned int iCol;
  unsigned int rowSize;
  unsigned int colSize;

  rowSize = (inFrame->height / CELL_DIM) + 1;
  colSize = (inFrame->width  / CELL_DIM) + 1;
  FrameBufferIterator inIter(inFrame,0,0);
  FrameBufferIterator thisIter(this,0,0);
  assert(this->height == inFrame->height);
  assert(this->width  == inFrame->width);
  
  if( ! initialized )
    {
      memcpy(buffer,inFrame->buffer,frameSize);
      initialized = true;
    }
  else
    {

      for(uint i = 0; i < height; ++i)
	{
	  inIter.goPosition(i,0);
	  thisIter.goPosition(i,0);
	  
	  iRow = i / rowSize;

	  for(uint j = 0; j < width; ++j)
	    {
	      iCol = j / colSize;
	      inIter.getPixel(&inPixel);
	      thisIter.getPixel(&thisPixel);
	      
	      // update the pixel variance map
	      variance = pixelVariance.get(i,j);
	      sse = thisPixel.sumSquaredError(inPixel);
	      // std::cout << "sse: " << sse << "\n";
	      // std::cout << "variance: " << variance << "\n";
	      if(sse > variance)
		variance += V_MIN;
	      else if(sse < variance)
		variance -= V_MIN;
	      if(variance < V_MIN)
		variance = V_MIN;
	      pixelVariance.set(i,j, variance); 

	      // update the region median intensity
	      intensity = inIter.getIntensity();
	      medIntensity = medianIntensity.get(iRow,iCol);
	      if(intensity > medIntensity)
		medianIntensity.set(iRow,iCol,++medIntensity);
	      else if(intensity < medIntensity)
		medianIntensity.set(iRow,iCol,--medIntensity);
		
		  
	      
	      // update the background pixel
	      thisPixel.medianShift(inPixel);
	      
	      thisIter.setPixel(thisPixel);
	      inIter.goRight();
	      thisIter.goRight();
	    }
	}
    }
  updateIntensityThresholds();
  updateVarianceThresholds();
}


// multiple pixel comparison removal with summed variances.
void FrameBufferWorldMotion::removeBackground(FrameBufferWorld * inFrame, 
					      FrameBufferWorld * outFrame, 
					      RawPixel removed)
{
  assert(this->height    == inFrame->height );
  assert(this->width     == inFrame->width  );
  assert(inFrame->height == outFrame->height);
  assert(inFrame->width  == outFrame->width );

  // an array of the sum-squared errors to save processing time.
  unsigned int errorMap[height+2][width+2];
  unsigned long avgError;
  //  int transferOffset;

  // used to kill pixels based on region median intensity.
  unsigned int iRow;
  unsigned int iCol;
  unsigned int rowSize;
  unsigned int colSize;

  rowSize = (inFrame->height / CELL_DIM) + 1;
  colSize = (inFrame->width  / CELL_DIM) + 1;  		  

  RawPixel thisPixel, inPixel, outPixel, transferPixel;
  FrameBufferIterator inIter(inFrame,0,0);
  FrameBufferIterator thisIter(this,0,0);
  FrameBufferIterator outIter(outFrame,0,0);
  FrameBufferIterator transferIter(inFrame,0,0);

  //===============================================
  //first we initialize the errorMap

  // initialize the top-leftmost 4 items
  // Note that throughout this method we treat incoming pixels whose intensity
  // is less than the intensity threshold as having no error.
  if(inIter.getIntensity() > intensityThresholds.get(0,0))
    {
      inIter.getPixel(&inPixel);
      thisIter.getPixel(&thisPixel);
      errorMap[0][0] = errorMap[0][1] = errorMap[1][0] = errorMap[1][1] = thisPixel.sumSquaredError(inPixel);
    }
  else
    {
      errorMap[0][0] = errorMap[0][1] = errorMap[1][0] = errorMap[1][1] = 0;
    }
  inIter.goDown();
  thisIter.goDown();
  // initialize the leftmost two columns.
  for(uint i = 2; i < height + 2; ++i)
    {
      iRow = i / rowSize;
      if(inIter.getIntensity() > intensityThresholds.get(iRow,0))
	{
	  inIter.getPixel(&inPixel);
	  thisIter.getPixel(&thisPixel);
	  errorMap[i][0] = errorMap[i][1] = thisPixel.sumSquaredError(inPixel);
	}
      else
	{
	  errorMap[i][0] = errorMap[i][1] = 0;
	}
      inIter.goDown();
      thisIter.goDown();
    }

  // initialize the top two rows.
  inIter.goPosition(0,1);
  thisIter.goPosition(0,1);
  for(uint i = 2; i < width + 2; ++i)
    {
      iCol = i / colSize;
      if(inIter.getIntensity() > intensityThresholds.get(0,iCol))
	{
	  inIter.getPixel(&inPixel);
	  thisIter.getPixel(&thisPixel);
	  errorMap[0][i] = errorMap[1][i] = thisPixel.sumSquaredError(inPixel);
	}
      else
	{
	  errorMap[0][i] = errorMap[1][i] = 0;
	}
      inIter.goRight();
      thisIter.goRight();
    }
  // end initialization
  //==============================================

  for(uint i = 0; i < height; ++i)
    {
      inIter.goPosition(i,1);
      thisIter.goPosition(i,1);
      inIter.goDown();
      thisIter.goDown();
      outIter.goPosition(i,0);
      transferIter.goPosition(i,0);
      iRow = i / rowSize;

      for(uint j = 0; j < width; ++j)
	{
	  iCol = j / colSize;
	  inIter.getPixel(&inPixel);
	  thisIter.getPixel(&thisPixel);
	  // calculate the new bottom right ssError
	  if(inIter.getIntensity() > intensityThresholds.get(iRow,iCol))
	    errorMap[i+2][j+2] = thisPixel.sumSquaredError(inPixel);
	  else
	    errorMap[i+2][j+2] = 0;

	  if(transferIter.getIntensity() > (intensityThresholds.get(iRow,iCol)))
	    {
	      transferIter.getPixel(&transferPixel);
	      	      
	      // sum the errors.
	      avgError = cm[0][0] * errorMap[ i ][j] + cm[0][1] * errorMap[ i ][j+1] + cm[0][2] * errorMap[ i ][j+2] +
		cm[1][0] * errorMap[i+1][j] + cm[1][1] * errorMap[i+1][j+1] + cm[1][2] * errorMap[i+1][j+2] +
		cm[2][0] * errorMap[i+2][j] + cm[2][1] * errorMap[i+2][j+1] + cm[2][2] * errorMap[i+2][j+2] ;
	      
	      avgError = avgError / divisor;
	      
	     // remove pixels.
	      if(avgError < varianceThresholds.get(i,j) )
		{
		  // stdgdb er::cout << "FBWM::removed (" << i << "," << j << ")\n";
		  outIter.setPixel(removed);
		  
		}
	      else
		{
		  //std::cout << "FBWM::set (" << i << "," << j << ")\n";
		  outIter.setPixel(transferPixel);
		}
	    }
	  else
	    {
	      outIter.setPixel(removed);
	    }
	  
	  outIter.goRight();
	  inIter.goRight();
	  thisIter.goRight();
	  transferIter.goRight();
	}
    }
}  

unsigned int FrameBufferWorldMotion::getAvgIntensity(unsigned int row, unsigned int col)
{
  unsigned int cellrow;
  unsigned int cellcol;
  
  cellrow = row / ((height / CELL_DIM) + 1);
  cellcol = col / ((width / CELL_DIM) + 1);
  //  cellrow = (row * CELL_DIM) / height;
  //  cellcol = (col * CELL_DIM) / width;
  cellrow = Geometry::clamp(0,cellrow, CELL_DIM);
  cellcol = Geometry::clamp(0,cellcol, CELL_DIM);

  return medianIntensity.get(cellrow,cellcol);

}



bool FrameBufferWorldMotion::loadData(std::string filePrefix)
{
  std::string temp;
  bool retVal = true;
  
  std::cout << "loading " << filePrefix << " image\n";
  temp = filePrefix + ".ppm";
  retVal = loadImage(temp);

  if(retVal)
    {
      std::cout << "loading " << filePrefix << " data\n";
      temp = filePrefix + ".dat";

      retVal = loadFromFile(temp);
    }
  initialized = retVal;
  return retVal;
}

void FrameBufferWorldMotion::saveData(std::string filePrefix)
{
  std::string temp;
  temp = filePrefix + ".ppm";
  
  saveImage(temp);

  
  temp = filePrefix + ".dat";
  saveToFile(temp);
  
}

bool FrameBufferWorldMotion::loadFromFile(std::string filename)
{
  bool good = true;
  int temp;
  std::ifstream file( filename.c_str() );
  if ( ! file )
    {
      std::cerr << "FrameBufferWorldMotion::loadFromFile(" << filename << ") unable to open input file\n";
      good = false;
    }
  else
    {
       
      pixelVariance.initialize(height,width);
      // load variances
      for(uint i = 0; i < height; i++)
	for(uint j = 0; j < width; j++)
	  {
	    file >> temp;
	    pixelVariance.set(i,j,temp);
	  }

      medianIntensity.initialize(CELL_DIM + PADDING, CELL_DIM + PADDING);
      // load avg intensities
      for(uint i = 0; i < CELL_DIM + PADDING; i++)
	for(uint j = 0; j < CELL_DIM + PADDING; j++)
	  {
	    file >> temp;
	    medianIntensity.set(i,j,temp);
	  }
      updateIntensityThresholds();
      updateVarianceThresholds();
    }

  return good;
}

void FrameBufferWorldMotion::saveToFile(std::string filename)
{
  std::ofstream file( filename.c_str() );
  int temp;
  if ( ! file )
    {
      std::cerr << "FrameBufferWorldMotion::saveToFile(" << filename << ") unable to open output file\n";
      return;
    }

      // load variances
      for(uint i = 0; i < height; i++)
	{
	  for(uint j = 0; j < width; j++)
	    {
	      temp = pixelVariance.get(i,j);
	      file << temp << " ";
	    }
	  file << "\n";
	}

      // load avg intensities
      for(uint i = 0; i < CELL_DIM + PADDING; i++)
	{
	  for(uint j = 0; j < CELL_DIM + PADDING; j++)
	    {
	      temp = medianIntensity.get(i,j);
	      file << temp << " ";
	    }
	  file << "\n";
	}


}

void FrameBufferWorldMotion::saveImage(std::string filename)
{
  std::ofstream file( filename.c_str() );
  FrameBufferIterator fbit(this);
  RawPixel pix;

  if ( ! file )
    {
      std::cerr << "FrameBufferWorldMotion::saveImage(" << filename << ") unable to open output file\n";
      exit( 1 );
    }

  file << height << " " << width << "\n";
  for(uint i = 0; i < height; i++)
    {
      fbit.goRow(i);
      for(uint j = 0; j < width; j++)
	{
	  fbit.getPixel(&pix);
	  file << pix.red << " ";
	  file << pix.green << " ";
	  file << pix.blue << " ";
	  fbit.goRight();
	}
      file << "\n";
    }
}
bool FrameBufferWorldMotion::loadImage(std::string filename)
{
  unsigned int h,w;
  bool good = true;
  RawPixel pix;

  std::ifstream file( filename.c_str() );
  if ( ! file )
    {
      std::cerr << "FrameBufferWorldMotion::loadFromFile(" << filename << ") unable to open input file\n";
      good = false;
    }
  else
    {
      file >> h >> w;
      
      if(h == height && w == width)
	{
	  FrameBuffer::initialize(RGB32, width, height);
	  
	  FrameBufferIterator fbit(this);
	  
	  for(uint i = 0; i < height; i++)
	    {
	      fbit.goRow(i);
	      for(uint j = 0; j < width; j++)
		{
		  file >> pix.red;
		  file >> pix.green;
		  file >> pix.blue;
		  fbit.setPixel(pix);
		  fbit.goRight();
		}
	    }
	}
      else
	{
	  std::cout << "FrameBufferWorldMotion::loadFromFile(" << filename << ") image is wrong size\n";
	  good = false;
	}
      file.close();
    }

  return good;
}

void FrameBufferWorldMotion::addBackgroundData(std::vector<Run> & unused, FrameBuffer * inFrame)
{
  assert(this->height == inFrame->height);
  assert(this->width  == inFrame->width);
  RawPixel thisPixel, inPixel;
  unsigned int variance;
  unsigned int sse; // sum squared error
  FrameBufferIterator inIter(inFrame);
  FrameBufferIterator thisIter(this);
  Run run;
  unsigned int row, col;

  for(uint i = 0; i < unused.size(); ++i)
    {
      run = unused[i];
      row = run.row;
      col = run.col;
      inIter.goPosition(row, col);
      thisIter.goPosition(row, col);
      for(uint j = 0; j < run.length; ++j)
	{
	  inIter.getPixel(&inPixel);
	  thisIter.getPixel(&thisPixel);

	  // update the pixel variance map
	  variance = pixelVariance.get(row,col);
	  sse = thisPixel.sumSquaredError(inPixel);
	  // std::cout << "sse: " << sse << "\n";
	  // std::cout << "variance: " << variance << "\n";
	  if(sse > variance)
	    variance += V_MIN;
	  else if(sse < variance)
	    variance -= V_MIN;
	  if(variance < V_MIN)
	    variance = V_MIN;
	  pixelVariance.set(row,col, variance); 
	  
	  thisPixel.medianShift(inPixel);
	  
	  thisIter.setPixel(thisPixel);
	  inIter.goRight();
	  thisIter.goRight();
	}
    }
  updateVarianceThresholds();
}

void FrameBufferWorldMotion::setVarianceThreshold(unsigned int vThreshold)
{
  globalVThreshold = vThreshold;
  updateVarianceThresholds();
}
void FrameBufferWorldMotion::setIntensityThreshold(unsigned int iThreshold)
{
  globalIThreshold = iThreshold;
  updateIntensityThresholds();
}

unsigned int FrameBufferWorldMotion::getVarianceThreshold()
{
  return globalVThreshold;
}
unsigned int FrameBufferWorldMotion::getIntensityThreshold()
{
  return globalIThreshold;
}

void FrameBufferWorldMotion::updateIntensityThresholds()
{
  for(uint row = 0; row < intensityThresholds.getHeight(); row++)
    for(uint col = 0; col < intensityThresholds.getWidth(); col++)
      intensityThresholds.set(row,col,(medianIntensity.get(row,col) * globalIThreshold) / 100);
}

void FrameBufferWorldMotion::updateVarianceThresholds()
{
  unsigned int lt,rt,up,dn, threshold, avgVariance;
  
  for(uint row = 0; row < height; row++)
    {
      for(uint col = 0; col < width; col++)
	{
	  // sum the pixel variances
	  lt = (col == 0) ? col : col-1;
	  rt = (col == width - 1) ? col : col+1;
	  up = (row == 0) ? row : row-1;
	  dn = (row == height - 1) ? row : row+1;
	  
	  avgVariance = cm[0][0] * pixelVariance.get(up,lt)   + cm[0][1] * pixelVariance.get(up,col)   + cm[0][2] * pixelVariance.get(up,rt)   +
	                cm[1][0] * pixelVariance.get(row ,lt) + cm[1][1] * pixelVariance.get(row ,col) + cm[1][2] * pixelVariance.get(row ,rt) +
	                cm[2][0] * pixelVariance.get(dn,lt)   + cm[2][1] * pixelVariance.get(dn,col)   + cm[2][2] * pixelVariance.get(dn,rt)    ;
	  
	  avgVariance = avgVariance / divisor;
	  
	  threshold = (globalVThreshold * avgVariance) / 100 ;
	  varianceThresholds.set(row,col,threshold);
	}
    }
}

