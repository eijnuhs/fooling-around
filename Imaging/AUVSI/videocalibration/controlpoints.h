// $Id: controlpoints.h,v 1.1.1.1.2.3 2004/10/04 13:50:16 cvs Exp $
//

#ifndef _CONTROL_POINTS_H_
#define _CONTROL_POINTS_H_

#include <vector>
#include <ostream>
#include <qpainter.h>

class ControlPoint
{
 public:
  ControlPoint( double xw, double yw, double zw, double xm, double ym, double zm );
  double xw, yw, zw;
  double xm, ym, zm;
  friend std::ostream & operator<<( std::ostream & os, ControlPoint & p );
};

class ControlPoints
{
public:
  ControlPoints ();
  virtual ~ ControlPoints ();
  void addPoint (struct ControlPoint point);
  void addPoint (double Xw, double Yw, double Zw = 0.0);
  struct ControlPoint getPoint (int index);
  struct ControlPoint *getPointRef (int index);
  struct ControlPoint *selectPoint (int x, int y);
  int size (void);
  void clear (void);

  std::vector<ControlPoint> points;
};

#endif
