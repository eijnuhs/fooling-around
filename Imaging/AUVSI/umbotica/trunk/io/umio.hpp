#ifndef UMIO_HPP
#define UMIO_HPP
#include <iostream>
#include <string>

// a bunch of helper functions for io operations.
namespace umbotica{
  // send a list to a stream
  template<typename ConstIterator>
  inline void toStream(std::ostream & stream, ConstIterator begin, ConstIterator end, std::string delimiter = " ", std::string prefix = "", std::string postfix = "")
  {
    ConstIterator i = begin;
    stream << prefix;
    if(i != end)
      {
	stream << *i;
	i++;
	for( ; i != end; ++i)
	  stream << delimiter << *i;
      }
    stream << postfix;
  }

  template<typename listType>
  inline void fromStream(std::istream & stream, listType & theList)
  {
    typedef typename listType::value_type value_type;
    //    copy(std::istream_iterator<typename listType::value_type>(stream), std::istream_iterator<typename listType::value_type>(), back_inserter(theList));
    //    std::istream_iterator<typename listType::value_type> i(stream), end;
    //    for(; i != end; ++i)
    //      theList.push_back(*i);
    while(!stream.eof())
      {
	value_type temp;
	stream >> temp;
	theList.push_back(temp);
      }   
  }
}
#endif
