#ifndef RGB565_H
#define RGB565_H

#include <inttypes.h>
#include "utilities.hpp"
// Utility functions for extracting color data from rgb 565 pixels:

namespace umbotica {

inline int red565(uint16_t pixel)
{
  return (int)((pixel & 0xf800) >> 8);
}
inline int green565(uint16_t pixel)
{
  return (int)((pixel & 0x07e0) >> 3);
}
inline int blue565(uint16_t pixel)
{
  return (int)((pixel & 0x001f) << 3);
}

inline uint16_t setRed565(int val, uint16_t pixel)
{
  val = umbotica::clamp(val,0,255);
  return (pixel & 0xf800) | ((val & 0xf8) << 8);
}
inline uint16_t setGreen565(int val, uint16_t pixel)
{
  val = umbotica::clamp(val,0,255);
  return (pixel & 0x07e0) | ((val & 0xfc) << 3);
}
inline uint16_t setBlue565(int val, uint16_t pixel)
{
  val = umbotica::clamp(val,0,255);
  return (pixel & 0x001f) | ((val & 0xf8) >> 3);
}

}
#endif
