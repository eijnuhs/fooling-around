// $Id: videodevice.cpp,v 1.1.1.1 2004/09/16 23:12:05 cvs Exp $
// Jacky Baltes <jacky@cs.umanitoba.ca>

#include "videodevice.h"

VideoDevice::VideoDevice(std::string driver, std::string devname, std::string input, std::string standard, int fps, int width, int height, int depth )
  :
  driver( driver ),
  devname( devname ),
  input( input ),
  standard( standard ),
  fps( fps ),
  width( width ),
  height( height ),
  depth( depth ),
  error( true )
{
}

VideoDevice::~VideoDevice()
{

}

bool VideoDevice::getError( void ) const
{
  return error;
}

int VideoDevice::getWidth( void ) const
{
  return width;
}

int VideoDevice::getHeight( void ) const
{
  return height;
}

