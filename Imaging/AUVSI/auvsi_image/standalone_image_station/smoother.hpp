#ifndef SMOOTHER_H
#define SMOOTHER_H

#include <vector>
#include <vigra/tinyvector.hxx>
#include "telemetry.hpp"
#include "sqlinterface.hpp"
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/format.hpp>
#include "localregression.hpp"
#include <string>
#include <iostream>
#include <cmath>

enum SmoothSource {
  Fresh, // resmooth from stored telemetry
  Db // get from smoothed table in database
};

struct EstimatedTrack {
  boost::posix_time::ptime lastUpdate;
  double m_estimatedTrack;
};


//DataSource must expose a member function
//struct telemetry getNeighbors(boost::posix_time::ptime target);
template <class DataSource>
class Smoother
{
public:
  Smoother();
  ~Smoother();

  //////////////////////////////////////////////////////////////////
  // Returns an interpolated telemetry value in parameter 'smoothed'
  // SmoothSource should be one of
  //  - Fresh
  //  - Db [NOT IMPLEMENTED]
  // Fresh indicates to retrieve data fresh from the
  //   telemetry table, and write to the smoothed
  //   table
  // Db indicates to retrieve the data already in the
  //   smoothed table [NOT IMPLEMENTED]
  // Returns true on success
  // Returns false if not enough data is yet in the database for 
  // smoothing
  //////////////////////////////////////////////////////////////////
  bool getSmoothed(boost::posix_time::ptime target, struct telemetry * t, SmoothSource source);
  double pTimeToDouble(boost::posix_time::ptime time);

private:
  std::vector<struct telemetry> neighbors;
  DataSource * fetcher;
  double avgRecentTurnRate(boost::posix_time::ptime now);
  double turnRate(int airspeedFPS, double rollRadians);
  struct EstimatedTrack eTrack;
};

/////////////////////////////////////////////////////////
// IMPLEMENTATION
/////////////////////////////////////////////////////////
template <class DataSource>
Smoother<DataSource>::Smoother()
{
  try
    {
      fetcher = new DataSource();
    }
  catch (std::exception & e)
    {
      std::cerr << "No connection to telemetry data source." << std::endl;
      throw e;
    }
  eTrack.m_estimatedTrack = -1;
}

template <class DataSource>
Smoother<DataSource>::~Smoother()
{
  delete (fetcher);
}

template <class DataSource>
bool Smoother<DataSource>::getSmoothed(boost::posix_time::ptime target, struct telemetry * t, SmoothSource source)
{
  LRSmoother<double, double> lrsmoother(2);
  bool success = true;

  int neighborFindIndex;
  int prevNeighbor;
  int nextNeighbor;

  bzero(t, sizeof(struct telemetry));

  if (source == Fresh)
    {
      double targetDouble = pTimeToDouble(target);
      
      neighbors = fetcher->getNeighbors(target, -2, 2);

      /*
      for (unsigned int i = 0; i < neighbors.size(); i++)
	{
	  std::cout << neighbors[i].time << ", ";
	  std::cout << boost::format("%f") % pTimeToDouble(neighbors[i].time) << ", ";
	  std::cout << neighbors[i].longitude << ", ";
	  std::cout << neighbors[i].latitude << ", ";
	  std::cout << neighbors[i].altitude << ", ";
	  std::cout << neighbors[i].pitch << ", ";
	  std::cout << neighbors[i].roll << ", ";
	  std::cout << neighbors[i].heading << "\n";
	}
      */

      //////////////////////////////////////////////////
      // THIS IS THE REQUIRED AMMOUNT OF TELEMETRY 
      // DATAPOINTS THAT MUST BE FOUND WITHIN
      // +- 5 SECONDS FROM THE TARGET TIME
      //////////////////////////////////////////////////

      if (neighbors.size() > 3)
	{
	  std::vector< TrainingExample<double, double> > examples;
	  
	  t->time = target;


	  for (neighborFindIndex = 0; neighborFindIndex < (int)neighbors.size()-1; neighborFindIndex++)
	    {
	      if (neighbors[neighborFindIndex].time < target && neighbors[neighborFindIndex+1].time > target)
		{
		  break;
		}
	    }
	  if ( neighborFindIndex == (int)neighbors.size()-1 )
	    {
	      neighborFindIndex--;
	    }

	  prevNeighbor = neighborFindIndex;
	  nextNeighbor = neighborFindIndex+1;

	  
	  // smooth longitude
	  for (int i = prevNeighbor; i <= nextNeighbor; i++)
	    {
	      double seconds = pTimeToDouble(neighbors[i].time);
	      double longitude = neighbors[i].longitude;
	      examples.push_back(TrainingExample<double,double>(seconds, longitude));

	    }
	  //t->longitude = lrsmoother.getSmoothed(targetDouble, examples);
	  t->longitude = ((targetDouble - examples[0].x) / (examples[1].x - examples[0].x)) * (examples[1].y - examples[0].y) + examples[0].y;
	  
	  // smooth latitude
	  for (int i = prevNeighbor; i <= nextNeighbor; i++)
	    {
	      double latitude = neighbors[i].latitude;
	      examples[i - prevNeighbor].y = latitude;
	    }
	  //t->latitude = lrsmoother.getSmoothed(targetDouble, examples);
	  t->latitude = ((targetDouble - examples[0].x) / (examples[1].x - examples[0].x)) * (examples[1].y - examples[0].y) + examples[0].y;

	  // smooth altitude
	  for (int i = prevNeighbor; i <= nextNeighbor; i++)
	    {
	      double altitude = neighbors[i].altitude;
	      examples[i - prevNeighbor].y = altitude;
	    }
	  //t->altitude = (int)lrsmoother.getSmoothed(targetDouble, examples);
	  t->altitude = (int)(((targetDouble - examples[0].x) / (examples[1].x - examples[0].x)) * (examples[1].y - examples[0].y) + examples[0].y);
	  
	  // smooth pitch
	  for (int i = prevNeighbor; i <= nextNeighbor; i++)
	    {
	      double pitch = neighbors[i].pitch;
	      examples[i - prevNeighbor].y = pitch;
	    }
	  //t->pitch = lrsmoother.getSmoothed(targetDouble, examples);
	  t->pitch = ((targetDouble - examples[0].x) / (examples[1].x - examples[0].x)) * (examples[1].y - examples[0].y) + examples[0].y;

	  // smooth airspeed
	  for (int i = prevNeighbor; i <= nextNeighbor; i++)
	    {
	      double airspeed = neighbors[i].airspeed;
	      examples[i - prevNeighbor].y = airspeed;
	    }
	  //t->airspeed = (int)lrsmoother.getSmoothed(targetDouble, examples);
	  t->airspeed = (int)(((targetDouble - examples[0].x) / (examples[1].x - examples[0].x)) * (examples[1].y - examples[0].y) + examples[0].y);

	  // smooth roll
	  for (int i = prevNeighbor; i <= nextNeighbor; i++)
	    {
	      double roll = neighbors[i].roll;
	      examples[i - prevNeighbor].y = roll;
	    }
	  //t->roll = lrsmoother.getSmoothed(targetDouble, examples);
	  t->roll = ((targetDouble - examples[0].x) / (examples[1].x - examples[0].x)) * (examples[1].y - examples[0].y) + examples[0].y;

	  // smooth heading

	  // heading is incorrect as we didn't have time to work out the whole wind thing
	  for (int i = prevNeighbor; i <= nextNeighbor; i++)
	    {
	      double heading = neighbors[i].heading;
	      examples[i - prevNeighbor].y = heading;
	    }
	  //t->heading = lrsmoother.getSmoothed(targetDouble, examples);
	  t->heading = ((targetDouble - examples[0].x) / (examples[1].x - examples[0].x)) * (examples[1].y - examples[0].y) + examples[0].y;

	  ////////////////////////////////////////////////
	  // smooth track - uses estimate based on roll
	  ////////////////////////////////////////////////
	  
	  //for (int i = 0; i < (int)neighbors.size(); i++)
	  //  {
	  //    double track = neighbors[i].track;
	  //    examples[i].y = track;
	  //  }

	  // gets the neighbors directly adjacent to the target
	  double track1 = neighbors[prevNeighbor].track;
	  double track2 = neighbors[nextNeighbor].track;
	  
	  // account for wraparound
	  if (fabs(track1 - track2) > 180)
	    {
	      if (track1 > 180)
		{
		  track1 -= 360;
		}
	      if (track2 > 180)
		{
		  track2 -= 360;
		}
	    }

	  // smooth using only the directly adjacent neighbor headings... 
	  examples = std::vector< TrainingExample<double, double> >();
	  examples.push_back(TrainingExample<double,double>(pTimeToDouble(neighbors[prevNeighbor].time), track1));
	  examples.push_back(TrainingExample<double,double>(pTimeToDouble(neighbors[nextNeighbor].time), track2));
	  
	  double smoothedTrack = lrsmoother.getSmoothed(targetDouble, examples);
	  // this track is probably still wrong unless the aircraft has been steady for two seconds
	  
	  //std::cout << "smoothed track: " << smoothedTrack << "\n";
	  //std::cout << "avgRecentTurnRate: " << avgRecentTurnRate(target) << "\n";
	  
	  // Note: "5" is five degrees per second.
	  if (avgRecentTurnRate(target) < 5 || eTrack.m_estimatedTrack == -1)
	    {
	      /*
	      std::cout << "\n\n////////////////////////////////////////////////////\n";
	      std::cout << "good track data right now... updating with it\n";
	      std::cout << "\n\n////////////////////////////////////////////////////\n";
	      */
	      eTrack.lastUpdate = target;
	      eTrack.m_estimatedTrack = smoothedTrack;
	    }
	  else
	    {
	      std::cout << "bad track data... updating using roll\n";
	      boost::posix_time::time_duration difference = target-eTrack.lastUpdate;
	      double seconds = difference.seconds();
	      seconds += (double)difference.fractional_seconds() / 1000000.0;
	      eTrack.m_estimatedTrack += (seconds) * turnRate(t->airspeed, t->roll);
	      eTrack.lastUpdate = target;
	    }
	  // clamp to [0,359.99]
	  if (eTrack.m_estimatedTrack < 0)
	    {
	      eTrack.m_estimatedTrack += 360;
	    }
	  else if (eTrack.m_estimatedTrack >= 360)
	    {
	      eTrack.m_estimatedTrack -= 360;
	    }

	  t->track = eTrack.m_estimatedTrack;
	  
	  // write to DB
	  fetcher->writeSmoothed(t);
	}
      else
	{
	  std::cout << "Not enough telemetry... only got " << neighbors.size() << " \n";
	  success = false;
	}
    }
  else // get smoothed from smoothed table in database
    {
      // must read telemetry from smoothed db and return that value
      std::cerr << "NOT IMPLEMENTED\n";
      success = false;
    }

  return success;
}

template <class DataSource>
double Smoother<DataSource>::pTimeToDouble(boost::posix_time::ptime time)
{
  std::string timeString = boost::posix_time::to_iso_string(time);

  int tIndex = timeString.find_first_of('T');
  std::string timeOnly = timeString.substr(tIndex+1, timeString.length() - tIndex);
  double minutes = 60 * atoi(timeOnly.substr(0,2).c_str());
  minutes += atoi(timeOnly.substr(2,2).c_str());
  
  double seconds = atoi(timeOnly.substr(4,2).c_str());
  double microseconds;
  if (timeOnly.length() > 6)
    {
      microseconds = atof(timeOnly.substr(7,timeOnly.length() - 7).c_str()) / (double)1000000.0;
    }
  else
    {
      microseconds = 0;
    }
  seconds += microseconds;
  
  minutes += seconds / 60.0;

  return minutes;
}

template <class DataSource>
double Smoother<DataSource>::avgRecentTurnRate(boost::posix_time::ptime now)
{
  std::vector<struct telemetry> recent = fetcher->getNeighbors(now, -2, 0);
  double turnRateSum = 0;
  
  for (int i = 0; i < (int)recent.size(); i++)
    {
      turnRateSum += turnRate(recent[i].airspeed, recent[i].roll);
    }

  if (recent.size() != 0)
    {
      return turnRateSum / (int)recent.size();
    }
  else
    {
      return 0;
    }
}

template <class DataSource>
double Smoother<DataSource>::turnRate(int airspeedFPS, double rollRadians)
{
  double degreesPerSecond;
  double secondsPerDegree;
  if (rollRadians != 0)
    {
      if (airspeedFPS == 0)
	{
	  airspeedFPS = 72;
	}
      secondsPerDegree = M_PI * airspeedFPS / (180 * 32 * tan(rollRadians));
      degreesPerSecond = 1/secondsPerDegree;
    }
  else
    {
      degreesPerSecond = 0;
    }
  return degreesPerSecond;
}

#endif
