// Stores a GPS coordinate in decimal degrees of longitude and
// latitude. Also allows conversion from a coordinate to an offset in
// metres from a user-specified reference GPS coordinate.

// TODO: Change metres to feet

#ifndef _GPS_H
#define _GPS_H

#include <vigra/tinyvector.hxx>
#include <string>

const long double EARTH_RADIUS = 6372795.477598; // meters

class GpsCoord : public vigra::TinyVector<double, 2>
{
public:
  // Positive longitude is east of the prime meridan. Positive
  // latitude is north of the equator.
  GpsCoord (double const & longitude = 0.0, double const & latitude = 0.0 );
  // Constructs a gpsCoordinate from an ICAO formatted gps string
  // Coordinate format: "DDMMSSNdddmmsse"
  // DD is degrees latitude
  // MM is minutes latitude
  // SS is seconds latitude
  // N is either 'N' or 'S'
  // ddd is degrees longitude
  // mm is minutes longitude
  // ss is seconds longitude
  // e is either 'E' or 'W'
  GpsCoord ( std::string icaoCoordinate );
  ~GpsCoord();

  // Returns the offset of this coordinate from the reference coordinate.
  vigra::TinyVector<double, 2> inMetres() const;

  // Sets the reference coordinate for conversion to metres.
  static void setReference(GpsCoord _reference);
  static GpsCoord gpsCoordFromMetres( double metresEast, double metresNorth );

  // Gives a gps location from the current point in meters.
  GpsCoord fromMetres(double metresEast, double metresNorth) const;
  vigra::TinyVector<double, 2> distanceInMetres(GpsCoord g) const;
  double straightLineDistance(GpsCoord g) const;

  double longitude() const;
  double latitude() const;

  std::string latitudeString() const;
  std::string longitudeString() const;
  
  std::string toCompetitionString() const;
  
private:
  static GpsCoord reference;
};

#endif
