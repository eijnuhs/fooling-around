#include "filevideodevice.h"
#include <boost/filesystem/convenience.hpp>
#include <qstringlist.h>
#include <iostream>
#include <qimage.h>
#include "framebuffer.h"
#include "framebufferrgb32.h"

namespace fs = boost::filesystem;

FileVideoDevice::FileVideoDevice( std::string path ) :
  VideoDevice( "File", path, "none", "none", 30,640,480,32 ), idx(0)
{
  
  std::cout << "creating a file video device based on directory: " << path << std::endl;
  // load and sort the image file names
  if(fs::exists(path))
    {
      fs::directory_iterator i = fs::directory_iterator(path);
      fs::directory_iterator end = fs::directory_iterator();
      
      bool hw_init = false;
      
      QStringList formatlist = QImage::inputFormatList();
      
      QStringList::iterator fl = formatlist.begin(),flend = formatlist.end();
      if(fl != flend)
	{
	  std::cout << "Formats accepted: " << (*fl).ascii();
	  fl++;
	  
	  for( ; fl != flend; fl++)
	    {
	      std::cout << ", " << (*fl).ascii();
	    }
	  std::cout << std::endl;
	}
      
      for (; i != end; ++i)
	{
	  std::cout << "Testing file: " << i->native_directory_string() << std::endl;
	  if(i->has_leaf())
	    {
	      std::cout << "extension: " << extension(*i) << std::endl;
	      QStringList format = formatlist.grep(extension(*i).c_str(), false);
	      if(format.size() > 0)
		{
		  // make sure the image is of an accepted type...
		  image_files.push_back(*i);
		  if(!hw_init)
		    {
		      QImage temp;
		      temp.load(i->native_directory_string().c_str());
		      m_height = temp.height();
		      m_width = temp.width();
		      hw_init = true;
		    }
		}
	      else if(extension(*i) == ".ppm")
		{
		  image_files.push_back(*i);
		  if(!hw_init)
		    {
		      FrameBufferRGB32 frame;
		      frame.inFromPPM(i->native_directory_string());
		      m_height = frame.height;
		      m_width = frame.width;
		      hw_init = true;
		    }
		}
	    }
	}
      
      sort(image_files.begin(), image_files.end());
      std::cout << "accepted image files: " << image_files.size() << std::endl;
    }
  else
    {
      std::cout << "The path \"" << path << "\" does not exist\n";
      exit(1);
    }
  
  height = m_height;
  width = m_width;
  
}
FileVideoDevice::~FileVideoDevice( )
{

}

bool FileVideoDevice::getError( void ) const
{
  return false;
}
int FileVideoDevice::startCapture( void )
{
  return image_files.size() == 0;
}
int FileVideoDevice::stopCapture( void )
{
  return 0;
}
void FileVideoDevice::nextFrame( FrameBuffer *frame )
{
  QImage temp_frame;
  if(image_files.size() > 0)
    {
      temp_frame.load(image_files[idx].native_directory_string());
      
      //  printName();
      //  std::cout << "FBV update QImage: " << (void *)frame << "\n";
      frame->initialize(FrameBuffer::RGB32 , temp_frame.width(), temp_frame.height() );      
      
      FrameBufferIterator fIter( frame );
      for( unsigned int i = 0; i < frame->height; i++ )
	{
	  fIter.goRow( i );
	  for( unsigned int j = 0; j < frame->width; j++, fIter.goRight() )
	    {
	      QRgb pix = temp_frame.pixel(j,i);
	      RawPixel pixel(qRed(pix), qGreen(pix), qBlue(pix));
	      fIter.setPixel( pixel ,0);
	    }
	} 
      idx = (idx + 1) % image_files.size();
    }
}
int FileVideoDevice::releaseCurrentBuffer( void )
{
  return 0;
}
bool FileVideoDevice::isInterlaced( void )
{
  return false;
}

int FileVideoDevice::getWidth() const
{
  return m_width;
}
int FileVideoDevice::getHeight() const
{
  return m_height;
}
