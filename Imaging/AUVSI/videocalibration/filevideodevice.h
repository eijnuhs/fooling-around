
#ifndef _FILE_VIDEODEVICE_H_
#define _FILE_VIDEODEVICE_H_

#include <string>
#include <vector>
#include "boost/filesystem/operations.hpp" // includes boost/filesystem/path.hpp
#include "boost/filesystem/fstream.hpp"    // ditto
#include "videodevice.h"



// Forward declarations
class FrameBuffer;

class FileVideoDevice : public VideoDevice
{
public:
  FileVideoDevice( std::string driver);
  ~FileVideoDevice( );

  bool getError( void ) const;
  int startCapture( void );
  int stopCapture( void );
  void nextFrame( FrameBuffer *curFrame );
  int releaseCurrentBuffer( void );
  bool isInterlaced( void );
  int getWidth() const;
  int getHeight() const;

  std::string path;
  std::vector<boost::filesystem::path> image_files;

  unsigned int m_width, m_height;
  unsigned int idx;
};

#endif
