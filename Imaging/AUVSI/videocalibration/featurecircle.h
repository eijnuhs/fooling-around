#ifndef __FEATURE_CIRCLE_H_
#define __FEATURE_CIRCLE_H_

// helper struct.
struct offset
{
  double row;
  double column;
  double theta;
};

// a class to help define offsets from a centre that
// represent a circle used for feature detection.

class FeatureCircle
{
 public:
  FeatureCircle(double init_radius, double init_radPerPix);
  unsigned int getSize() const;
  struct offset operator[](unsigned int idx) const;
  inline double getRadPerPix(){ return radPerPix; }
  inline double getRadius(){ return radius; }

 private:
  double radius;
  double radPerPix;
  unsigned int size;
  struct offset * offsetArray;
};

#endif

