/************************************************************************/
/*                                                                      */
/*               Copyright 1998-2002 by Ullrich Koethe                  */
/*       Cognitive Systems Group, University of Hamburg, Germany        */
/*                                                                      */
/*    This file is part of the VIGRA computer vision library.           */
/*    ( Version 1.3.2, Jan 27 2005 )                                    */
/*    You may use, modify, and distribute this software according       */
/*    to the terms stated in the LICENSE file included in               */
/*    the VIGRA distribution.                                           */
/*                                                                      */
/*    The VIGRA Website is                                              */
/*        http://kogs-www.informatik.uni-hamburg.de/~koethe/vigra/      */
/*    Please direct questions, bug reports, and contributions to        */
/*        koethe@informatik.uni-hamburg.de                              */
/*                                                                      */
/*  THIS SOFTWARE IS PROVIDED AS IS AND WITHOUT ANY EXPRESS OR          */
/*  IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED      */
/*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. */
/*                                                                      */
/************************************************************************/


#ifndef VIGRA_BGRAVALUE_HXX
#define VIGRA_BGRAVALUE_HXX

#include <cmath>    // abs(double)
#include <cstdlib>  // abs(int)
#include "vigra/config.hxx"
#include "vigra/numerictraits.hxx"
#include "vigra/accessor.hxx"
#include "vigra/tinyvector.hxx"

namespace vigra {

/********************************************************/
/*                                                      */
/*                      BGRAValue                        */
/*                                                      */
/********************************************************/

/** \brief Class for a single BGRA value.

    This class contains four values (of the specified type) that
    represent red, green, and blue color channels as well as the alpha
    channel. There are four possibilities to access these values:
    accessor functions (\ref red(), \ref green(), \ref blue(), \ref
    alpha()), index operator (operator[](dx), where 0 is alpha, 1 is
    red, 2 is green and 3 is blue) and iterator (STL-compatible random access
    iterator that references the four channels in turn). The latter two
    methods, together with the necessary embedded typedefs, ensure
    compatibility of a BGRAValue with a STL vector.

    \ref BGRAValueOperators "Arithmetic operations" are defined as component-wise applications of these
    operations. Addition, subtraction, and multiplication of two BGRAValues
    (+=, -=, *=, +, -, *, unary -), multiplication and division of an
    BGRAValue with a double, and NumericTraits/PromoteTraits are defined,
    so that BGRAValue fulfills the requirements of a \ref LinearAlgebra.

    A number of \ref BGRAValueAccessors "accessors" are provided
    that support access to BGRAValues as a whole, to a selected
    color component, or to the luminance value.

    <b>\#include</b> "<a href="bgrvalue_8hxx-source.html">vigra/bgrvalue.hxx</a>"<br>
    Namespace: vigra
*/
template <class VALUETYPE>
class BGRAValue
: public TinyVector<VALUETYPE, 4>
{
    typedef TinyVector<VALUETYPE, 4> Base;
  public:
        /** STL-compatible definition of valuetype
        */
    typedef VALUETYPE value_type;
        /** STL-compatible definition of iterator
        */
    typedef typename TinyVector<VALUETYPE, 4>::iterator iterator;
        /** STL-compatible definition of const iterator
        */
    typedef typename TinyVector<VALUETYPE, 4>::const_iterator const_iterator;

        /** Construct from explicit color values
	 */
  BGRAValue(value_type red, value_type green, value_type blue, value_type alpha)
    : Base(blue, green, red, alpha)
     {}

        /** Construct gray value
        */
    BGRAValue(value_type gray)
    : Base(gray, gray, gray, gray)
    {}

        /** Construct from another sequence (must have length 4!)
        */
    template <class Iterator>
    BGRAValue(Iterator i, Iterator end)
    : Base(i[0], i[1], i[2], i[3])
    {}

        /** Default constructor (sets all components to 0)
        */
    BGRAValue()
    : Base(0, 0, 0, 0)
    {}

#if !defined(TEMPLATE_COPY_CONSTRUCTOR_BUG)

    BGRAValue(BGRAValue const & r)
    : Base(r)
    {}

    BGRAValue & operator=(BGRAValue const & r)
    {
        Base::operator=(r);
        return *this;
    }

#endif // TEMPLATE_COPY_CONSTRUCTOR_BUG


        /** Copy constructor.
        */
    template <class U>
    BGRAValue(BGRAValue<U> const & r)
    : Base(r)
    {}

        /** Copy assignment.
        */
    template <class U>
    BGRAValue & operator=(BGRAValue<U> const & r)
    {
        Base::operator=(r);
        return *this;
    }

        /** construct from TinyVector
        */
    BGRAValue(TinyVector<value_type, 4> const & r)
    : Base(r)
    {}

        /** assign TinyVector.
        */
    BGRAValue & operator=(TinyVector<value_type, 4> const & r)
    {
        Base::operator=(r);
        return *this;
    }

        /** Unary negation (construct BGRAValue with negative values)
        */
    BGRAValue operator-() const
    {
        return BGRAValue(-red(), -green(), -blue(), -alpha());
    }

        /** Access red component.
        */
  value_type & red() { return (*this)[2]; }

        /** Access green component.
        */
    value_type & green() { return (*this)[1]; }

        /** Access blue component.
        */
    value_type & blue() { return (*this)[0]; }

        /** Access alpha component.
        */
    value_type & alpha() { return (*this)[3]; }

        /** Get red component.
        */
    value_type const & red() const { return (*this)[2]; }

        /** Get green component.
        */
    value_type const & green() const { return (*this)[1]; }

        /** Get blue component.
        */
    value_type const & blue() const { return (*this)[0]; }

        /** Get alpha component.
        */
    value_type const & alpha() const { return (*this)[3]; }

        /** Calculate luminance.
        */
    value_type luminance() const {
         return detail::RequiresExplicitCast<value_type>::cast(0.3*red() + 0.59*green() + 0.11*blue()); }

        /** Calculate magnitude.
        */
    typename NumericTraits<VALUETYPE>::RealPromote
    magnitude() const {
         return VIGRA_CSTD::sqrt(
            (typename NumericTraits<VALUETYPE>::RealPromote)squaredMagnitude());
    }

        /** Calculate squared magnitude.
        */
    typename NumericTraits<VALUETYPE>::Promote
    squaredMagnitude() const {
         return red()*red() + green()*green() + blue()*blue();
    }

        /** Set red component. The type <TT>V</TT> of the passed
            in <TT>value</TT> is automatically converted to <TT>VALUETYPE</TT>.
        */
    template <class V>
    void setRed(V value) { (*this)[2] = detail::RequiresExplicitCast<value_type>::cast(value); }

        /** Set green component.The type <TT>V</TT> of the passed
            in <TT>value</TT> is automatically converted to <TT>VALUETYPE</TT>.
        */
    template <class V>
    void setGreen(V value) { (*this)[1] = detail::RequiresExplicitCast<value_type>::cast(value); }

        /** Set blue component.The type <TT>V</TT> of the passed
            in <TT>value</TT> is automatically converted to <TT>VALUETYPE</TT>.
        */
    template <class V>
    void setBlue(V value) { (*this)[0] = detail::RequiresExplicitCast<value_type>::cast(value); }

        /** Set alpha component.The type <TT>V</TT> of the passed
            in <TT>value</TT> is automatically converted to <TT>VALUETYPE</TT>.
        */
    template <class V>
    void setAlpha(V value) { (*this)[3] = detail::RequiresExplicitCast<value_type>::cast(value); }


    template <class V>
    void setRGB(V r, V g, V b) 
    { 
        (*this)[2] = detail::RequiresExplicitCast<value_type>::cast(r); 
        (*this)[1] = detail::RequiresExplicitCast<value_type>::cast(g); 
        (*this)[0] = detail::RequiresExplicitCast<value_type>::cast(b); 
	(*this)[3] = NumericTraits<VALUETYPE>::zero();
    }

    template <class V>
    void setRGBA(V r, V g, V b, V a) 
    { 
        (*this)[2] = detail::RequiresExplicitCast<value_type>::cast(r); 
        (*this)[1] = detail::RequiresExplicitCast<value_type>::cast(g); 
        (*this)[0] = detail::RequiresExplicitCast<value_type>::cast(b); 
        (*this)[3] = detail::RequiresExplicitCast<value_type>::cast(a); 
    }
};

/********************************************************/
/*                                                      */
/*                     BGRAValue Comparison              */
/*                                                      */
/********************************************************/

/** \addtogroup BGRAValueOperators Functions for BGRAValue

    \brief <b>\#include</b> "<a href="bgrvalue_8hxx-source.html">vigra/bgrvalue.hxx</a>

    These functions fulfill the requirements of a Linear Algebra.
    Return types are determined according to \ref BGRAValueTraits.

    Namespace: vigra
    <p>

 */
//@{
    /// component-wise equal 
template <class V1, class V2>
inline
bool
operator==(BGRAValue<V1> const & l, BGRAValue<V2> const & r)
{
  return (l.red() == r.red()) &&
    (l.green() == r.green())  &&
    (l.blue() == r.blue())    &&
    (l.alpha() == r.alpha());
}

    /// component-wise not equal
template <class V1, class V2>
inline
bool
operator!=(BGRAValue<V1> const & l, BGRAValue<V2> const & r)
{
  return (l.red() != r.red()) ||
    (l.green() != r.green())  ||
    (l.blue() != r.blue())    ||
    (l.alpha() != r.alpha());
}


//@}

/********************************************************/
/*                                                      */
/*                      BGRAValue-Traits                 */
/*                                                      */
/********************************************************/

/** \page BGRAValueTraits Numeric and Promote Traits of BGRAValue
    The numeric and promote traits for BGRAValues follow
    the general specifications for \ref NumericPromotionTraits.
    They are implemented in terms of the traits of the basic types by
    partial template specialization:

    \code

    template <class T>
    struct NumericTraits<BGRAValue<T> >
    {
        typedef BGRAValue<typename NumericTraits<T>::Promote> Promote;
        typedef BGRAValue<typename NumericTraits<T>::RealPromote> RealPromote;

        typedef typename NumericTraits<T>::isIntegral isIntegral;
        typedef VigraFalseType isScalar;

        // etc.
    };

    template <class T1, class T2>
    struct PromoteTraits<BGRAValue<T1>, BGRAValue<T2> >
    {
        typedef BGRAValue<typename PromoteTraits<T1, T2>::Promote> Promote;
    };
    \endcode

    <b>\#include</b> "<a href="bgrvalue_8hxx-source.html">vigra/bgrvalue.hxx</a>"<br>
    Namespace: vigra

*/

#if !defined(NO_PARTIAL_TEMPLATE_SPECIALIZATION)

template <class T>
struct NumericTraits<BGRAValue<T> >
{
    typedef BGRAValue<T> Type;
    typedef BGRAValue<typename NumericTraits<T>::Promote> Promote;
    typedef BGRAValue<typename NumericTraits<T>::RealPromote> RealPromote;
    typedef BGRAValue<typename NumericTraits<T>::ComplexPromote> ComplexPromote;
    typedef T ValueType; 
    
    typedef typename NumericTraits<T>::isIntegral isIntegral; 
    typedef VigraFalseType isScalar; 
    typedef VigraFalseType isOrdered;
    typedef VigraFalseType isComplex; 

    static BGRAValue<T> zero() {
        return BGRAValue<T>(NumericTraits<T>::zero());
    }
    static BGRAValue<T> one() {
        return BGRAValue<T>(NumericTraits<T>::one());
    }
    static BGRAValue<T> nonZero() {
        return BGRAValue<T>(NumericTraits<T>::nonZero());
    }

    static Promote toPromote(BGRAValue<T> const & v) {
        return Promote(v);
    }
    static RealPromote toRealPromote(BGRAValue<T> const & v) {
        return RealPromote(v);
    }
    static BGRAValue<T> fromPromote(Promote const & v) {
        return BGRAValue<T>(NumericTraits<T>::fromPromote(v.red()),
                           NumericTraits<T>::fromPromote(v.green()),
                           NumericTraits<T>::fromPromote(v.blue()));
    }
    static BGRAValue<T> fromRealPromote(RealPromote const & v) {
        return BGRAValue<T>(NumericTraits<T>::fromRealPromote(v.red()),
                           NumericTraits<T>::fromRealPromote(v.green()),
                           NumericTraits<T>::fromRealPromote(v.blue()));
    }
};

template <class T1, class T2>
struct PromoteTraits<BGRAValue<T1>, BGRAValue<T2> >
{
    typedef BGRAValue<typename PromoteTraits<T1, T2>::Promote> Promote;
};

template <class T>
struct PromoteTraits<BGRAValue<T>, double >
{
    typedef BGRAValue<typename NumericTraits<T>::RealPromote> Promote;
};

template <class T>
struct PromoteTraits<double, BGRAValue<T> >
{
    typedef BGRAValue<typename NumericTraits<T>::RealPromote> Promote;
};

#else // NO_PARTIAL_TEMPLATE_SPECIALIZATION

#define BGRAVALUE_NUMTRAITS(T) \
template<>\
struct NumericTraits<BGRAValue<T> >\
{\
    typedef BGRAValue<T> Type; \
    typedef BGRAValue<NumericTraits<T>::Promote> Promote; \
    typedef BGRAValue<NumericTraits<T>::RealPromote> RealPromote; \
    typedef BGRAValue<NumericTraits<T>::ComplexPromote> ComplexPromote; \
    typedef T ValueType; \
    \
    typedef NumericTraits<T>::isIntegral isIntegral; \
    typedef VigraFalseType isScalar; \
    typedef VigraFalseType isOrdered; \
    typedef VigraFalseType isComplex; \
    \
    static BGRAValue<T> zero() { \
        return BGRAValue<T>(NumericTraits<T>::zero()); \
    }\
    static BGRAValue<T> one() { \
        return BGRAValue<T>(NumericTraits<T>::one()); \
    }\
    static BGRAValue<T> nonZero() { \
        return BGRAValue<T>(NumericTraits<T>::nonZero()); \
    }\
    \
    static Promote toPromote(BGRAValue<T> const & v) { \
        return Promote(v); \
    }\
    static RealPromote toRealPromote(BGRAValue<T> const & v) { \
        return RealPromote(v); \
    }\
    static BGRAValue<T> fromPromote(Promote const & v) { \
        BGRAValue<T> res;\
        BGRAValue<T>::iterator d = res.begin();\
        Promote::const_iterator s = v.begin();\
        for(; d != res.end(); ++d, ++s)\
            *d = NumericTraits<T>::fromPromote(*s);\
        return res;\
    }\
    static BGRAValue<T> fromRealPromote(RealPromote const & v) {\
        BGRAValue<T> res;\
        BGRAValue<T>::iterator d = res.begin();\
        RealPromote::const_iterator s = v.begin();\
        for(; d != res.end(); ++d, ++s)\
            *d = NumericTraits<T>::fromRealPromote(*s);\
        return res;\
    }\
};

#define BGRAVALUE_PROMTRAITS1(type1) \
template<> \
struct PromoteTraits<BGRAValue<type1>, BGRAValue<type1> > \
{ \
    typedef BGRAValue<PromoteTraits<type1, type1>::Promote> Promote; \
    static Promote toPromote(BGRAValue<type1> const & v) { \
        return static_cast<Promote>(v); } \
};

#define BGRAVALUE_PROMTRAITS2(type1, type2) \
template<> \
struct PromoteTraits<BGRAValue<type1>, BGRAValue<type2> > \
{ \
    typedef BGRAValue<PromoteTraits<type1, type2>::Promote> Promote; \
    static Promote toPromote(BGRAValue<type1> const & v) { \
        return static_cast<Promote>(v); } \
    static Promote toPromote(BGRAValue<type2> const & v) { \
        return static_cast<Promote>(v); } \
};

BGRAVALUE_NUMTRAITS(unsigned char)
BGRAVALUE_NUMTRAITS(int)
BGRAVALUE_NUMTRAITS(float)
BGRAVALUE_NUMTRAITS(double)
BGRAVALUE_PROMTRAITS1(unsigned char)
BGRAVALUE_PROMTRAITS1(int)
BGRAVALUE_PROMTRAITS1(float)
BGRAVALUE_PROMTRAITS1(double)
BGRAVALUE_PROMTRAITS2(float, unsigned char)
BGRAVALUE_PROMTRAITS2(unsigned char, float)
BGRAVALUE_PROMTRAITS2(int, unsigned char)
BGRAVALUE_PROMTRAITS2(unsigned char, int)
BGRAVALUE_PROMTRAITS2(int, float)
BGRAVALUE_PROMTRAITS2(float, int)
BGRAVALUE_PROMTRAITS2(double, unsigned char)
BGRAVALUE_PROMTRAITS2(unsigned char, double)
BGRAVALUE_PROMTRAITS2(int, double)
BGRAVALUE_PROMTRAITS2(double, int)
BGRAVALUE_PROMTRAITS2(double, float)
BGRAVALUE_PROMTRAITS2(float, double)

#undef BGRAVALUE_NUMTRAITS
#undef BGRAVALUE_PROMTRAITS1
#undef BGRAVALUE_PROMTRAITS2

#endif // NO_PARTIAL_TEMPLATE_SPECIALIZATION


/********************************************************/
/*                                                      */
/*                      BGRAValue-Arithmetic             */
/*                                                      */
/********************************************************/

/** \addtogroup BGRAValueOperators
 */
//@{
    /// componentwise add-assignment
template <class V1, class V2>
inline
BGRAValue<V1> &
operator+=(BGRAValue<V1> & l, BGRAValue<V2> const & r)
{
    l.red() += r.red();
    l.green() += r.green();
    l.blue() += r.blue();
    l.alpha() += r.alpha();
    return l;
}

    /// componentwise subtract-assignment
template <class V1, class V2>
inline
BGRAValue<V1> &
operator-=(BGRAValue<V1> & l, BGRAValue<V2> const & r)
{
    l.red() -= r.red();
    l.green() -= r.green();
    l.blue() -= r.blue();
    l.alpha = r.alpha();
    return l;
}

    /// componentwise multiply-assignment
template <class V1, class V2>
inline
BGRAValue<V1> &
operator*=(BGRAValue<V1> & l, BGRAValue<V2> const & r)
{
    l.red() *= r.red();
    l.green() *= r.green();
    l.blue() *= r.blue();
    l.alpha() *= r.alpha();
    return l;
}

    /// componentwise scalar multiply-assignment
template <class V>
inline
BGRAValue<V> &
operator*=(BGRAValue<V> & l, double r)
{
    l.red() *= r;
    l.green() *= r;
    l.blue() *= r;
    l.alpha() *= r;
    return l;
}

    /// componentwise scalar divide-assignment
template <class V>
inline
BGRAValue<V> &
operator/=(BGRAValue<V> & l, double r)
{
    l.red() /= r;
    l.green() /= r;
    l.blue() /= r;
    l.alpha() /= r;
    return l;
}

using VIGRA_CSTD::abs;

    /// component-wise absolute value
template <class T>
inline
BGRAValue<T> abs(BGRAValue<T> const & v) {
    return BGRAValue<T>(abs(v.red()), abs(v.green()),  abs(v.blue()), abs(v.alpha()));
}



    /// component-wise addition
template <class V1, class V2>
inline
typename PromoteTraits<BGRAValue<V1>, BGRAValue<V2> >::Promote
operator+(BGRAValue<V1> const & r1, BGRAValue<V2> const & r2)
{
    typename PromoteTraits<BGRAValue<V1>, BGRAValue<V2> >::Promote res(r1);

    res += r2;

    return res;
}

    /// component-wise subtraction
template <class V1, class V2>
inline
typename PromoteTraits<BGRAValue<V1>, BGRAValue<V2> >::Promote
operator-(BGRAValue<V1> const & r1, BGRAValue<V2> const & r2)
{
    typename PromoteTraits<BGRAValue<V1>, BGRAValue<V2> >::Promote res(r1);

    res -= r2;

    return res;
}

    /// component-wise multiplication
template <class V1, class V2>
inline
typename PromoteTraits<BGRAValue<V1>, BGRAValue<V2> >::Promote
operator*(BGRAValue<V1> const & r1, BGRAValue<V2> const & r2)
{
    typename PromoteTraits<BGRAValue<V1>, BGRAValue<V2> >::Promote res(r1);

    res *= r2;

    return res;
}

    /// component-wise left scalar multiplication
template <class V>
inline
typename NumericTraits<BGRAValue<V> >::RealPromote
operator*(double v, BGRAValue<V> const & r)
{
    typename NumericTraits<BGRAValue<V> >::RealPromote res(r);

    res *= v;

    return res;
}

    /// component-wise right scalar multiplication
template <class V>
inline
typename NumericTraits<BGRAValue<V> >::RealPromote
operator*(BGRAValue<V> const & r, double v)
{
    typename NumericTraits<BGRAValue<V> >::RealPromote res(r);

    res *= v;

    return res;
}

    /// component-wise scalar division
template <class V>
inline
typename NumericTraits<BGRAValue<V> >::RealPromote
operator/(BGRAValue<V> const & r, double v)
{
    typename NumericTraits<BGRAValue<V> >::RealPromote res(r);

    res /= v;

    return res;
}

/// cross product
/// This has no definition for a four component vector but we'll leave it
/// in for the BGR.
template <class V1, class V2>
inline
typename PromoteTraits<BGRAValue<V1>, BGRAValue<V2> >::Promote
cross(BGRAValue<V1> const & r1, BGRAValue<V2> const & r2)
{
    typedef typename PromoteTraits<BGRAValue<V1>, BGRAValue<V2> >::Promote
      Res;
    return  Res(r1.green()*r2.blue() - r1.blue()*r2.green(),
                r1.blue()*r2.red() - r1.red()*r2.blue(),
                r1.red()*r2.green() - r1.green()*r2.red());
}

    /// dot product
template <class V1, class V2>
inline
typename PromoteTraits<V1, V2>::Promote
dot(BGRAValue<V1> const & r1, BGRAValue<V2> const & r2)
{
  return r1.red()*r2.red() + r1.green()*r2.green() + r1.blue()*r2.blue();
}

using VIGRA_CSTD::ceil;

    /** Apply ceil() function to each BGR component.
    */
template <class V>
inline
BGRAValue<V>
ceil(BGRAValue<V> const & r)
{
    return BGRAValue<V>(ceil(r.red()),
                       ceil(r.green()),
                       ceil(r.blue()),
			ceil(r.alpha()));
}

using VIGRA_CSTD::floor;

    /** Apply floor() function to each BGR component.
    */
template <class V>
inline
BGRAValue<V>
floor(BGRAValue<V> const & r)
{
    return BGRAValue<V>(floor(r.red()),
                       floor(r.green()),
                       floor(r.blue()),
			floor(r.alpha()));
}

//@}

//@}
//@}


} // namespace vigra

#endif // VIGRA_BGRAVALUE_HXX
