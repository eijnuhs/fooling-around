#ifndef _UM_ACCESSORS_H_
#define _UM_ACCESSORS_H_

#include <vigra/accessor.hxx>
#include <vigra/rgbvalue.hxx>
#include "rgbavalue.hxx"


namespace umbotica {

/********************************************************/
/*                                                      */
/*                      RGBValue-Accessors              */
/*                                                      */
/********************************************************/

/** \addtogroup DataAccessors
*/
//@{
/** \defgroup RGBValueAccessors Accessors for RGBValue */
//@{
    /** Encapsulate access to rgb values.

    <b>\#include</b> "<a href="rgbvalue_8hxx-source.html">vigra/rgbvalue.hxx</a>"<br>
    Namespace: vigra
    */
template <class RGBVALUE>
class RGBAccessor
: public vigra::VectorAccessor<RGBVALUE>
{
  public:
  typedef typename RGBVALUE::value_type component_type;
  typedef typename vigra::RGBValue<component_type> value_type;

  
  template <class V, class RGBIterator>
  void set(V v, RGBIterator i)
  {
    i->setRGB(v.red(), v.green(), v.blue());
  }

  template <class RGBIterator>
  value_type get(RGBIterator i)
  {
    return value_type(i->red(), i->green(), i->blue());
  }

  /** Get value of the red component
   */
  template <class RGBIterator>
  component_type const & red(RGBIterator const & rgb) const
  {
    return (*rgb).red();
  }

  template <class V, class RGBIterator>
  void setRGB(V r, V g, V b, RGBIterator const & rgb) const
  {
b    (*rgb).setRGB( r, g, b );
  }

    
  /** Set value of the red component. The type <TT>V</TT> of the passed
      in <TT>value</TT> is automatically converted to <TT>component_type</TT>.
  */
  template <class V, class RGBIterator>
  void setRed(V value, RGBIterator const & rgb) const
  {
    (*rgb).setRed(value);
  }
  
  /** Get value of the red component at an offset
   */
  template <class RGBIterator, class DIFFERENCE>
  component_type const & red(RGBIterator const & rgb, DIFFERENCE diff) const
  {
    return rgb[diff].red();
    }

  /** Set value of the red component at an offset. The type <TT>V</TT> of the passed
      in <TT>value</TT> is automatically converted to <TT>component_type</TT>.
  */
  template <class V, class RGBIterator, class DIFFERENCE>
  void setRed(V value, RGBIterator const & rgb, DIFFERENCE diff) const
  {
    rgb[diff].setRed(value);
  }
  
  /** Get value of the green component
   */
  template <class RGBIterator>
  component_type const & green(RGBIterator const & rgb) const
  {
    return (*rgb).green();
  }
  
  /** Set value of the green component. The type <TT>V</TT> of the passed
      in <TT>value</TT> is automatically converted to <TT>component_type</TT>.
  */
  template <class V, class RGBIterator>
  void setGreen(V value, RGBIterator const & rgb) const
  {
    (*rgb).setGreen(value);
  }
  
  /** Get value of the green component at an offset
   */
  template <class RGBIterator, class DIFFERENCE>
  component_type const & green(RGBIterator const & rgb, DIFFERENCE d) const
  {
    return rgb[d].green();
  }
  
  /** Set value of the green component at an offset. The type <TT>V</TT> of the passed
      in <TT>value</TT> is automatically converted to <TT>component_type</TT>.
  */
  template <class V, class RGBIterator, class DIFFERENCE>
  void setGreen(V value, RGBIterator const & rgb, DIFFERENCE d) const
  {
    rgb[d].setGreen(value);
  }
  
  /** Get value of the blue component
   */
  template <class RGBIterator>
  component_type const & blue(RGBIterator const & rgb) const
  {
    return (*rgb).blue();
  }
  
  /** Set value of the blue component. The type <TT>V</TT> of the passed
      in <TT>value</TT> is automatically converted to <TT>component_type</TT>.
  */
  template <class V, class RGBIterator>
  void setBlue(V value, RGBIterator const & rgb) const
  {
    (*rgb).setBlue(value);
  }
  
  /** Get value of the blue component at an offset
   */
  template <class RGBIterator, class DIFFERENCE>
  component_type const & blue(RGBIterator const & rgb, DIFFERENCE d) const
  {
    return rgb[d].blue();
  }
  
  /** Set value of the blue component at an offset. The type <TT>V</TT> of the passed
      in <TT>value</TT> is automatically converted to <TT>component_type</TT>.
  */
  template <class V, class RGBIterator, class DIFFERENCE>
  void setBlue(V value, RGBIterator const & rgb, DIFFERENCE d) const
  {
    rgb[d].setBlue(value);
  }
  
  // these methods allow the accessors to be used as adaptors during image saving and
  // loading.
  template<class RGBIterator>
  unsigned int size(RGBIterator const &)
  {
    return 3;
  }
  
  template <class V, class RGBIterator>
  void setComponent(V const & value, RGBIterator const & i, int idx) const
  {
    // HACK to support saving and loading
    //    (*i)[idx] = vigra::detail::RequiresExplicitCast<component_type>::cast(value);
    switch(idx)
      {
      case 0:
	setRed(value,i);
	break;
      case 1:
	setGreen(value,i);
	break;
      default:
	setBlue(value,i);
      }
  }
  
  template <class RGBIterator>
  component_type const & getComponent(RGBIterator const & i, int idx) const
  {
    // HACK to support loading and saving
    //    return (*i)[idx];
    if(idx == 0)
      return red(i);
    else if( idx == 1 )
      return green(i);
    else 
      return blue(i);
  }
};
 

/********************************************************/
/*                                                      */
/*                       RedAccessor                    */
/*                                                      */
/********************************************************/

    /** Encapsulate access to red band of an rgb value.

    <b>\#include</b> "<a href="rgbvalue_8hxx-source.html">vigra/rgbvalue.hxx</a>"<br>
    Namespace: vigra
    */
template <class RGBVALUE>
class RedAccessor
{
  public:
    typedef typename RGBVALUE::value_type value_type;

        /** Get value of the red component
        */
    template <class ITERATOR>
    value_type const & operator()(ITERATOR const & i) const {
        return (*i).red();
    }

        /** Get value of the red component at an offset
        */
    template <class ITERATOR, class DIFFERENCE>
    value_type const & operator()(ITERATOR const & i, DIFFERENCE d) const
    {
        return i[d].red();
    }

        /** Set value of the red component. The type <TT>V</TT> of the passed
            in <TT>value</TT> is automatically converted to <TT>value_type</TT>.
        */
    template <class V, class ITERATOR>
    void set(V value, ITERATOR const & i) const {
        (*i).setRed(value);
    }


        /** Set value of the red component at an offset. The type <TT>V</TT> of the passed
            in <TT>value</TT> is automatically converted to <TT>value_type</TT>.
        */
    template <class V, class ITERATOR, class DIFFERENCE>
    void set(V value, ITERATOR const & i, DIFFERENCE d) const
    {
        i[d].setRed(value);
    }

  // these methods allow the accessors to be used as adaptors during image saving and
  // loading.
  template<class RGBIterator>
  unsigned int size(RGBIterator const &)
  {
    return 1;
  }
  
  template <class V, class RGBIterator>
  void setComponent(V const & value, RGBIterator const & i, int idx) const
  {
    i->setRed(vigra::detail::RequiresExplicitCast<value_type>::cast(value));
  }
  
  template <class RGBIterator>
  value_type const & getComponent(RGBIterator const & i, int) const
  {
    return i->red();
  }

};

/********************************************************/
/*                                                      */
/*                     GreenAccessor                    */
/*                                                      */
/********************************************************/

    /** Encapsulate access to green band of an rgb value.

    <b>\#include</b> "<a href="rgbvalue_8hxx-source.html">vigra/rgbvalue.hxx</a>"<br>
    Namespace: vigra
    */
template <class RGBVALUE>
class GreenAccessor
{
  public:
    typedef typename RGBVALUE::value_type value_type;

        /** Get value of the green component
        */
    template <class ITERATOR>
    value_type const & operator()(ITERATOR const & i) const {
        return (*i).green();
    }

        /** Get value of the green component at an offset
        */
    template <class ITERATOR, class DIFFERENCE>
    value_type const & operator()(ITERATOR const & i, DIFFERENCE d) const
    {
        return i[d].green();
    }

        /** Set value of the green component. The type <TT>V</TT> of the passed
            in <TT>value</TT> is automatically converted to <TT>value_type</TT>.
        */
    template <class V, class ITERATOR>
    void set(V value, ITERATOR const & i) const {
        (*i).setGreen(value);
    }


        /** Set value of the green component at an offset. The type <TT>V</TT> of the passed
            in <TT>value</TT> is automatically converted to <TT>value_type</TT>.
        */
    template <class V, class ITERATOR, class DIFFERENCE>
    void set(V value, ITERATOR const & i, DIFFERENCE d) const
    {
        i[d].setGreen(value);
    }
  // these methods allow the accessors to be used as adaptors during image saving and
  // loading.
  template<class RGBIterator>
  unsigned int size(RGBIterator const &)
  {
    return 1;
  }
  
  template <class V, class RGBIterator>
  void setComponent(V const & value, RGBIterator const & i, int) const
  {
    i->setGreen(vigra::detail::RequiresExplicitCast<value_type>::cast(value));
  }
  
  template <class RGBIterator>
  value_type const & getComponent(RGBIterator const & i, int) const
  {
    return i->green();
  }

};

/********************************************************/
/*                                                      */
/*                     BlueAccessor                     */
/*                                                      */
/********************************************************/

    /** Encapsulate access to blue band of an rgb value.

    <b>\#include</b> "<a href="rgbvalue_8hxx-source.html">vigra/rgbvalue.hxx</a>"<br>
    Namespace: vigra
    */
template <class RGBVALUE>
class BlueAccessor
{
  public:
    typedef typename RGBVALUE::value_type value_type;

        /** Get value of the blue component
        */
    template <class ITERATOR>
    value_type const & operator()(ITERATOR const & i) const {
        return (*i).blue();
    }

        /** Get value of the blue component at an offset
        */
    template <class ITERATOR, class DIFFERENCE>
    value_type const & operator()(ITERATOR const & i, DIFFERENCE d) const
    {
        return i[d].blue();
    }

        /** Set value of the blue component. The type <TT>V</TT> of the passed
            in <TT>value</TT> is automatically converted to <TT>value_type</TT>.
        */
    template <class V, class ITERATOR>
    void set(V value, ITERATOR const & i) const {
        (*i).setBlue(value);
    }


        /** Set value of the blue component at an offset. The type <TT>V</TT> of the passed
            in <TT>value</TT> is automatically converted to <TT>value_type</TT>.
        */
    template <class V, class ITERATOR, class DIFFERENCE>
    void set(V value, ITERATOR const & i, DIFFERENCE d) const
    {
        i[d].setBlue(value);
    }

  // these methods allow the accessors to be used as adaptors during image saving and
  // loading.
  template<class RGBIterator>
  unsigned int size(RGBIterator const &)
  {
    return 1;
  }
  
  template <class V, class RGBIterator>
  void setComponent(V const & value, RGBIterator const & i, int idx) const
  {
    i->setBlue(vigra::detail::RequiresExplicitCast<value_type>::cast(value));
  }
  
  template <class RGBIterator>
  value_type const & getComponent(RGBIterator const & i, int idx) const
  {
    return i->blue();
  }
};

/********************************************************/
/*                                                      */
/*                  RGBToGrayAccessor                   */
/*                                                      */
/********************************************************/

    /** Encapsulate access to luminance of an rgb value.

    <b>\#include</b> "<a href="rgbvalue_8hxx-source.html">vigra/rgbvalue.hxx</a>"<br>
    Namespace: vigra
    */
template <class RGBVALUE>
class RGBToGrayAccessor
{
public:
  typedef typename RGBVALUE::value_type value_type;
  
  /** Get value of the luminance
   */
  template <class ITERATOR>
  value_type operator()(ITERATOR const & i) const 
  {
    return (*i).luminance(); 
  }

  /** Get value of the luminance at an offset
   */
  template <class ITERATOR, class DIFFERENCE>
  value_type operator()(ITERATOR const & i, DIFFERENCE d) const
  {
    return i[d].luminance();
  }
  
  template<class RGBIterator>
  unsigned int size(RGBIterator const &)
  {
    return 1;
  }
  
  template <class V, class RGBIterator>
  void setComponent(V const & value, RGBIterator const & i, int idx) const
  {
    i->setRGB(vigra::detail::RequiresExplicitCast<value_type>::cast(value));
  }
  
  template <class RGBIterator>
  value_type const & getComponent(RGBIterator const & i, int idx) const
  {
    return i->luminance();
  }

  template <class V, class RGBIterator>
  void set(V const & value, RGBIterator const & i) const
  {
    value_type v;
    v = vigra::detail::RequiresExplicitCast<value_type>::cast(value);
    i->setRGB(v,v,v);
  }  

  template <class RGBIterator>
  value_type const & get(RGBIterator const & i) const
  {
    return i->luminance();
  }


};


/********************************************************/
/*                                                      */
/*                  GrayToRGBAccessor                   */
/*                                                      */
/********************************************************/

    /** Create an RGB view for a grayscale image by making all three channels
        equal.

    <b>\#include</b> "<a href="rgbvalue_8hxx-source.html">vigra/rgbvalue.hxx</a>"<br>
    Namespace: vigra
    */
template <class VALUETYPE>
class GrayToRGBAccessor
{
   public:
  typedef typename vigra::RGBAValue<VALUETYPE> value_type;
  typedef VALUETYPE component_type;

  /** Get RGB value for the given pixel.
   */
  template <class ITERATOR>
  value_type operator()(ITERATOR const & i) const {
    return value_type(*i,*i,*i); }
  
  /** Get RGB value at an offset
   */
  template <class ITERATOR, class DIFFERENCE>
  value_type operator()(ITERATOR const & i, DIFFERENCE d) const
  {
    return value_type(i[d],i[d],i[d]);
  }
  

  template <class Iterator>
  component_type const & red(Iterator const & i) const
  {
    return *i;
  }

  template <class Iterator, class Difference>
  component_type const & red(Iterator const & i, Difference d) const
  {
    return i[d];
  }

  template <class V, class Iterator>
  void setRed(V value, Iterator const & i) const
  {
    *i = vigra::detail::RequiresExplicitCast<component_type>::cast(value);
  }

  template <class V, class Iterator, class Difference>
  void setRed(V value, Iterator const & i, Difference diff) const
  {
    i[diff] = vigra::detail::RequiresExplicitCast<component_type>::cast(value);
  }

  //

  template <class Iterator>
  component_type const & blue(Iterator const & i) const
  {
    return *i;
  }

  template <class Iterator, class Difference>
  component_type const & blue(Iterator const & i, Difference d) const
  {
    return i[d];
  }

  template <class V, class Iterator>
  void setBlue(V value, Iterator const & i) const
  {
    *i = vigra::detail::RequiresExplicitCast<component_type>::cast(value);
  }

  template <class V, class Iterator, class Difference>
  void setBlue(V value, Iterator const & i, Difference diff) const
  {
    i[diff] = vigra::detail::RequiresExplicitCast<component_type>::cast(value);
  }

  //

  template <class Iterator>
  component_type const & green(Iterator const & i) const
  {
    return *i;
  }

  template <class Iterator, class Difference>
  component_type const & green(Iterator const & i, Difference d) const
  {
    return i[d];
  }

  template <class V, class Iterator>
  void setGreen(V value, Iterator const & i) const
  {
    *i = vigra::detail::RequiresExplicitCast<component_type>::cast(value);
  }

  template <class V, class Iterator, class Difference>
  void setGreen(V value, Iterator const & i, Difference diff) const
  {
    i[diff] = vigra::detail::RequiresExplicitCast<component_type>::cast(value);
  }





  template <class ITERATOR>
  unsigned int size(ITERATOR const & )
  {
    return 3;
  }
  
  template <class V, class Iterator>
  void setComponent(V const & value, Iterator const & i, int) const
  {
    component_type c = vigra::detail::RequiresExplicitCast<component_type>::cast(value);
    *i = c;
  }
  
  template <class Iterator>
  component_type const & getComponent(Iterator const & i, int idx) const
  {
    return *i;
  }

  //////////////
  template <class V, class RGBIterator>
  void set(V const & value, RGBIterator const & i) const
  {
    *i = vigra::detail::RequiresExplicitCast<component_type>::cast(value.luminance());
  }  

  template <class RGBIterator>
  value_type const & get(RGBIterator const & i) const
  {
    return value_type(*i,*i,*i);
  }


  
};


/********************************************************/
/*                                                      */
/*                      RGBAValue-Accessors              */
/*                                                      */
/********************************************************/

/** \addtogroup DataAccessors
*/
//@{
/** \defgroup RGBAValueAccessors Accessors for RGBAValue */
//@{
    /** Encapsulate access to rgb values.

    <b>\#include</b> "<a href="rgbvalue_8hxx-source.html">vigra/rgbvalue.hxx</a>"<br>
    Namespace: vigra
    */
template <class RGBAVALUE>
class RGBAAccessor
  : public vigra::VectorAccessor<RGBAVALUE>
{
public:
  
  typedef typename RGBAVALUE::value_type component_type;
  
  /** Get value of the red component
   */
  template <class RGBAIterator>
  component_type const & red(RGBAIterator const & rgb) const
  {
    return (*rgb).red();
  }
  
  template <class V, class RGBAIterator>
  void setRGB(V r, V g, V b, RGBAIterator const & rgb) const
  {
    (*rgb).setRGBA( r, g, b, 0);
  }
  
  template <class V, class RGBAIterator>
  void setRGBA(V r, V g, V b, V a, RGBAIterator const & rgb) const
  {
    (*rgb).setRGBA( r, g, b, a );
  }
  
  
  /** Set value of the red component. The type <TT>V</TT> of the passed
      in <TT>value</TT> is automatically converted to <TT>component_type</TT>.
  */
  template <class V, class RGBAIterator>
  void setRed(V value, RGBAIterator const & rgb) const
  {
    (*rgb).setRed(value);
    }
  
  /** Get value of the red component at an offset
   */
  template <class RGBAIterator, class DIFFERENCE>
  component_type const & red(RGBAIterator const & rgb, DIFFERENCE diff) const
  {
    return rgb[diff].red();
  }
  
  /** Set value of the red component at an offset. The type <TT>V</TT> of the passed
      in <TT>value</TT> is automatically converted to <TT>component_type</TT>.
  */
    template <class V, class RGBAIterator, class DIFFERENCE>
    void setRed(V value, RGBAIterator const & rgb, DIFFERENCE diff) const
  {
    rgb[diff].setRed(value);
  }
  
  /** Get value of the green component
   */
  template <class RGBAIterator>
  component_type const & green(RGBAIterator const & rgb) const
  {
    return (*rgb).green();
  }
  
  /** Set value of the green component. The type <TT>V</TT> of the passed
      in <TT>value</TT> is automatically converted to <TT>component_type</TT>.
  */
  template <class V, class RGBAIterator>
  void setGreen(V value, RGBAIterator const & rgb) const
  {
    (*rgb).setGreen(value);
  }
  
  /** Get value of the green component at an offset
   */
  template <class RGBAIterator, class DIFFERENCE>
  component_type const & green(RGBAIterator const & rgb, DIFFERENCE d) const
  {
    return rgb[d].green();
  }
  
  /** Set value of the green component at an offset. The type <TT>V</TT> of the passed
      in <TT>value</TT> is automatically converted to <TT>component_type</TT>.
  */
  template <class V, class RGBAIterator, class DIFFERENCE>
  void setGreen(V value, RGBAIterator const & rgb, DIFFERENCE d) const
  {
    rgb[d].setGreen(value);
  }
  
  /** Get value of the blue component
   */
  template <class RGBAIterator>
  component_type const & blue(RGBAIterator const & rgb) const
  {
    return (*rgb).blue();
  }
  
  /** Set value of the blue component. The type <TT>V</TT> of the passed
            in <TT>value</TT> is automatically converted to <TT>component_type</TT>.
  */
  template <class V, class RGBAIterator>
  void setBlue(V value, RGBAIterator const & rgb) const
  {
    (*rgb).setBlue(value);
    }
  
  /** Get value of the blue component at an offset
   */
  template <class RGBAIterator, class DIFFERENCE>
  component_type const & blue(RGBAIterator const & rgb, DIFFERENCE d) const
  {
    return rgb[d].blue();
  }
  
  /** Set value of the blue component at an offset. The type <TT>V</TT> of the passed
      in <TT>value</TT> is automatically converted to <TT>component_type</TT>.
        */
  template <class V, class RGBAIterator, class DIFFERENCE>
  void setBlue(V value, RGBAIterator const & rgb, DIFFERENCE d) const
  {
    rgb[d].setBlue(value);
  }
  
  ////////////////////////
  /** Get value of the alpha component
   */
  template <class RGBAIterator>
  component_type const & alpha(RGBAIterator const & rgb) const
  {
    return (*rgb).alpha();
  }
  
  /** Set value of the alpha component. The type <TT>V</TT> of the passed
      in <TT>value</TT> is automatically converted to <TT>component_type</TT>.
  */
  template <class V, class RGBAIterator>
  void setAlpha(V value, RGBAIterator const & rgb) const
  {
    (*rgb).setAlpha(value);
  }
  
  /** Get value of the alpha component at an offset
   */
  template <class RGBAIterator, class DIFFERENCE>
  component_type const & alpha(RGBAIterator const & rgb, DIFFERENCE d) const
  {
    return rgb[d].alpha();
  }
  
  /** Set value of the alpha component at an offset. The type <TT>V</TT> of the passed
      in <TT>value</TT> is automatically converted to <TT>component_type</TT>.
  */
  template <class V, class RGBAIterator, class DIFFERENCE>
  void setAlpha(V value, RGBAIterator const & rgb, DIFFERENCE d) const
  {
    rgb[d].setAlpha(value);
  }
  
  
};

/********************************************************/
/*                                                      */
/*                     AlphaAccessor                     */
/*                                                      */
/********************************************************/

    /** Encapsulate access to alpha band of an rgb value.

    <b>\#include</b> "<a href="rgbvalue_8hxx-source.html">vigra/rgbvalue.hxx</a>"<br>
    Namespace: vigra
    */
template <class RGBAVALUE>
class AlphaAccessor
{
  public:
    typedef typename RGBAVALUE::value_type value_type;

        /** Get value of the alpha component
        */
    template <class ITERATOR>
    value_type const & operator()(ITERATOR const & i) const {
        return (*i).alpha();
    }

        /** Get value of the alpha component at an offset
        */
    template <class ITERATOR, class DIFFERENCE>
    value_type const & operator()(ITERATOR const & i, DIFFERENCE d) const
    {
        return i[d].alpha();
    }

        /** Set value of the alpha component. The type <TT>V</TT> of the passed
            in <TT>value</TT> is automatically converted to <TT>value_type</TT>.
        */
    template <class V, class ITERATOR>
    void set(V value, ITERATOR const & i) const {
        (*i).setAlpha(value);
    }


        /** Set value of the alpha component at an offset. The type <TT>V</TT> of the passed
            in <TT>value</TT> is automatically converted to <TT>value_type</TT>.
        */
    template <class V, class ITERATOR, class DIFFERENCE>
    void set(V value, ITERATOR const & i, DIFFERENCE d) const
    {
        i[d].setAlpha(value);
    }
};


/********************************************************/
/*                                                      */
/*                  RGBToRGBAAccessor                   */
/*                                                      */
/********************************************************/

    /** Create an RGBA view for an RGB image.

    <b>\#include</b> "<a href="rgbvalue_8hxx-source.html">vigra/rgbvalue.hxx</a>"<br>
    Namespace: vigra
    */
template <class RGBVALUE>
class RGBToRGBAAccessor
{
public:
  typedef typename RGBVALUE::value_type component_type;
  typedef typename vigra::RGBAValue<component_type> value_type;
  
  /** Get RGBA value for the given pixel.
   */
  template <class ITERATOR>
  value_type operator()(ITERATOR const & i) const {
    return value_type(i->red(),i->green(),i->blue(),vigra::NumericTraits<component_type>::zero()); }
  
  /** Get RGBA value at an offset
   */
  template <class ITERATOR, class DIFFERENCE>
  value_type operator()(ITERATOR const & i, DIFFERENCE d) const
  {
    return value_type(i[d].red(),i[d].green(),i[d].blue(),vigra::NumericTraits<component_type>::zero());
  }

  template <class ITERATOR>
  component_type const & red(ITERATOR const & i){ return i->red();}

  template <class ITERATOR>
  component_type  const & blue(ITERATOR const & i){ return i->blue();}

  template <class ITERATOR>
  component_type  const & green(ITERATOR const & i){ return i->green();}

  template <class ITERATOR>
  component_type  const & alpha(ITERATOR const & i){ return vigra::NumericTraits<component_type>::zero();}

  template <class ITERATOR, class DIFFERENCE>
  component_type  const & red(ITERATOR const & i, DIFFERENCE d){ return i[d].red();}

  template <class ITERATOR, class DIFFERENCE>
  component_type  const & blue(ITERATOR const & i, DIFFERENCE d){ return i[d].blue();}

  template <class ITERATOR, class DIFFERENCE>
  component_type  const & green(ITERATOR const & i, DIFFERENCE d){ return i[d].green();}

  template <class ITERATOR, class DIFFERENCE>
  component_type  const & alpha(ITERATOR const & i, DIFFERENCE d) const{ return vigra::NumericTraits<component_type>::zero();}

  template <class V, class RGBIterator>
  void setRed(V value, RGBIterator const & rgb) const
  {
    (*rgb).setRed(value);
  }
  
  template <class V, class RGBIterator, class DIFFERENCE>
  void setRed(V value, RGBIterator const & rgb, DIFFERENCE d) const
  {
    rgb[d].setRed(value);
  }

  template <class V, class RGBIterator>
  void setBlue(V value, RGBIterator const & rgb) const
  {
    (*rgb).setBlue(value);
  }
  
  template <class V, class RGBIterator, class DIFFERENCE>
  void setBlue(V value, RGBIterator const & rgb, DIFFERENCE d) const
  {
    rgb[d].setBlue(value);
  }


  template <class V, class RGBIterator>
  void setGreen(V value, RGBIterator const & rgb) const
  {
    (*rgb).setGreen(value);
  }
  
  template <class V, class RGBIterator, class DIFFERENCE>
  void setGreen(V value, RGBIterator const & rgb, DIFFERENCE d) const
  {
    rgb[d].setGreen(value);
  }

  template <class V, class RGBIterator>
  void setAlpha(V, RGBIterator const & ) const
  {

  }
  
  template <class V, class RGBIterator, class DIFFERENCE>
  void setAlpha(V , RGBIterator const & , DIFFERENCE ) const
  {
  }


  
  // these methods allow the accessor to be used
  // as an adaptor in load/save functions.
  template <class ITERATOR>
  unsigned int size(ITERATOR const & i)
  {
    return 4;
  }

  template <class V, class RGBIterator>
  void setComponent(V const & value, RGBIterator const & i, int idx) const
  {
    if(idx > 0 && idx < 3)
      (*i)[idx] = vigra::detail::RequiresExplicitCast<component_type>::cast(value);
  }
  
  template <class RGBIterator>
  component_type const & getComponent(RGBIterator const & i, int idx) const
  {
    if(idx > 0 && idx < 3)
      return (*i)[idx];
    else
      return vigra::NumericTraits<component_type>::zero();
  }

  template <class V, class RGBIterator>
  void set(V const & value, RGBIterator const & i) const
  {
    typedef typename V::value_type value_t;
    value_t r,g,b;
    r = value.red();
    g = value.green();
    b = value.blue();
    i->setRGB(vigra::detail::RequiresExplicitCast<component_type>::cast(r),
	      vigra::detail::RequiresExplicitCast<component_type>::cast(g),
	      vigra::detail::RequiresExplicitCast<component_type>::cast(b));
  }  

  template <class RGBIterator>
  value_type const & get(RGBIterator const & i) const
  {
    return value_type(i->red(), i->green(), i->blue());
  }

  
};



/********************************************************/
/*                                                      */
/*                  GrayToRGBAAccessor                   */
/*                                                      */
/********************************************************/

    /** Create an RGBA view for a grayscale image by making all three channels
        equal.

    <b>\#include</b> "<a href="rgbvalue_8hxx-source.html">vigra/rgbvalue.hxx</a>"<br>
    Namespace: vigra
    */
template <class VALUETYPE>
class GrayToRGBAAccessor
{
   public:
  typedef typename vigra::RGBAValue<VALUETYPE> value_type;
  typedef VALUETYPE component_type;

  /** Get RGB value for the given pixel.
   */
  template <class ITERATOR>
  value_type operator()(ITERATOR const & i) const {
    return value_type(*i,*i,*i); }
  
  /** Get RGB value at an offset
   */
  template <class ITERATOR, class DIFFERENCE>
  value_type operator()(ITERATOR const & i, DIFFERENCE d) const
  {
    return value_type(i[d],i[d],i[d]);
  }
  

  template <class Iterator>
  component_type const & red(Iterator const & i) const
  {
    return *i;
  }

  template <class Iterator, class Difference>
  component_type const & red(Iterator const & i, Difference d) const
  {
    return i[d];
  }

  template <class V, class Iterator>
  void setRed(V value, Iterator const & i) const
  {
    *i = vigra::detail::RequiresExplicitCast<component_type>::cast(value);
  }

  template <class V, class Iterator, class Difference>
  void setRed(V value, Iterator const & i, Difference diff) const
  {
    i[diff] = vigra::detail::RequiresExplicitCast<component_type>::cast(value);
  }

  //

  template <class Iterator>
  component_type const & blue(Iterator const & i) const
  {
    return *i;
  }

  template <class Iterator, class Difference>
  component_type const & blue(Iterator const & i, Difference d) const
  {
    return i[d];
  }

  template <class V, class Iterator>
  void setBlue(V value, Iterator const & i) const
  {
    *i = vigra::detail::RequiresExplicitCast<component_type>::cast(value);
  }

  template <class V, class Iterator, class Difference>
  void setBlue(V value, Iterator const & i, Difference diff) const
  {
    i[diff] = vigra::detail::RequiresExplicitCast<component_type>::cast(value);
  }

  //

  template <class Iterator>
  component_type const & green(Iterator const & i) const
  {
    return *i;
  }

  template <class Iterator, class Difference>
  component_type const & green(Iterator const & i, Difference d) const
  {
    return i[d];
  }

  template <class V, class Iterator>
  void setGreen(V value, Iterator const & i) const
  {
    *i = vigra::detail::RequiresExplicitCast<component_type>::cast(value);
  }

  template <class V, class Iterator, class Difference>
  void setGreen(V value, Iterator const & i, Difference diff) const
  {
    i[diff] = vigra::detail::RequiresExplicitCast<component_type>::cast(value);
  }





  template <class ITERATOR>
  unsigned int size(ITERATOR const & i)
  {
    return 3;
  }
  
  template <class V, class Iterator>
  void setComponent(V const & value, Iterator const & i, int) const
  {
    component_type c = vigra::detail::RequiresExplicitCast<component_type>::cast(value);
    *i = c;
  }
  
  template <class Iterator>
  component_type const & getComponent(Iterator const & i, int idx) const
  {
    return *i;
  }

  //////////////
  template <class V, class RGBIterator>
  void set(V const & value, RGBIterator const & i) const
  {
    typedef typename V::value_type value_t;
    value_t v = value.luminance();
    *i = vigra::detail::RequiresExplicitCast<component_type>::cast(v);
  }  

  template <class RGBIterator>
  value_type const & get(RGBIterator const & i) const
  {
    return value_type(*i,*i,*i,*i);
  }


};




} // namespace umbotica

#endif
