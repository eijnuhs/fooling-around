#include <iostream>
//#include <umimage.hpp>
#include "vigra/stdimage.hxx"
#include "vigra/impex.hxx"
using namespace vigra; 
using namespace std;

template<class SRC_ITERATOR, class SRC_ACCESSOR, 
	 class DEST_ITERATOR_A, class DEST_ACCESSOR_A, 
	 class DEST_ITERATOR_B, class DEST_ACCESSOR_B>
void deinterlace(SRC_ITERATOR upperLeft, SRC_ITERATOR lowerRight, SRC_ACCESSOR src_accessor, 
		 DEST_ITERATOR_A destTopFieldUL, DEST_ACCESSOR_A destAccessor_A, 
		 DEST_ITERATOR_B destBotFieldUL, DEST_ACCESSOR_B destAccessor_B)
{

  std::cout << "Deinterlacing image\n";
  int row = 0, col = 0;
  int field = 0;
  SRC_ITERATOR src = upperLeft;
  DEST_ITERATOR_A dstTop = destTopFieldUL;
  DEST_ITERATOR_B dstBot = destBotFieldUL;
  for( ; src.y < lowerRight.y; src.y++)
    {
      //      std::cout << "Processing Row: " << row++ << "\n";
      src.x = upperLeft.x;
      field = (src.y - upperLeft.y) % 2;
      if(field == 0) // process the top field
	{
	  //	  std::cout << "Writing scanline to top field\n";
	  dstTop.x = destTopFieldUL.x;
	  for( ; src.x < lowerRight.x; ++src.x, ++dstTop.x)
	    {
	      destAccessor_A.set(src_accessor(src),dstTop);
	    }
	  dstTop.y++;
	}
      else
	{
	  //	  std::cout << "Writing scanline to bottom field\n";
	  dstBot.x = destBotFieldUL.x;
	  for( ; src.x < lowerRight.x; ++src.x, ++dstBot.x)
	    {
	      destAccessor_B.set(src_accessor(src),dstBot);
	    }
	  dstBot.y++;
	}
    }



}
		 


void deinterlace(std::string filename)
{
  try
    {
      // read image given as first argument
      // file type is determined automatically
      vigra::ImageImportInfo info(filename.c_str());
      
      if(info.isGrayscale())
        {
	  // create a gray scale image of appropriate size
	  vigra::BImage in(info.width(), info.height());
	  vigra::BImage outTop(info.width(), (info.height() / 2) + (info.height() % 2));
	  vigra::BImage outBot(info.width(), info.height() / 2);
	  // import the image just read
	  importImage(info, destImage(in));
          deinterlace(in.upperLeft(), in.lowerRight(), in.accessor(), outTop.upperLeft(), outTop.accessor(), outBot.upperLeft(), outBot.accessor());


	  std::string::size_type dotpos = filename.rfind('.');
	  std::string topFilename = filename;
	  std::string botFilename = filename;
	  topFilename.insert(dotpos,"-top");
	  botFilename.insert(dotpos,"-bot");

	  std::string::size_type moveDot = topFilename.rfind('.',dotpos);
	  while(moveDot != std::string::npos)
	    {
	      topFilename[moveDot] = '-';
	      botFilename[moveDot] = '-';
	      moveDot = topFilename.rfind('.',moveDot);
	    }
	  std::cout << "Creating top-field file: " <<  topFilename << "\n";
	  std::cout << "Creating bottom-field file: " << botFilename << "\n";
	  // write the image to the file given as second argument
	  // the file type will be determined from the file name's extension
	  exportImage(srcImageRange(outTop), vigra::ImageExportInfo("top.jpg"));//vigra::ImageExportInfo(topFilename.c_str()));
	  exportImage(srcImageRange(outBot), vigra::ImageExportInfo("bot.jpg"));//vigra::ImageExportInfo(botFilename.c_str()));
        }
      else
        {
	  // create a gray scale image of appropriate size
	  vigra::BRGBImage in(info.width(), info.height());
	  vigra::BRGBImage outTop(info.width(), (info.height() / 2) + (info.height() % 2));
	  vigra::BRGBImage outBot(info.width(), info.height() / 2);
	  // import the image just read
	  importImage(info, destImage(in));
          deinterlace(in.upperLeft(), in.lowerRight(), in.accessor(), outTop.upperLeft(), outTop.accessor(), outBot.upperLeft(), outBot.accessor());


	  std::string::size_type dotpos = filename.rfind('.');
	  std::string topFilename = filename;
	  std::string botFilename = filename;
	  topFilename.insert(dotpos,"-top");
	  botFilename.insert(dotpos,"-bot");
	  
	  std::string::size_type moveDot = topFilename.rfind('.',dotpos);
	  while(moveDot != std::string::npos)
	    {
	      topFilename[moveDot] = '-';
	      botFilename[moveDot] = '-';
	      moveDot = topFilename.rfind('.',moveDot);
	    }

	  std::cout << "Processing file: " << filename << "\n";
	  std::cout << "Creating top-field file: " <<  topFilename << "\n";
	  std::cout << "Creating bottom-field file: " << botFilename << "\n";

	  // write the image to the file given as second argument
	  // the file type will be determined from the file name's extension
	  exportImage(srcImageRange(outTop), vigra::ImageExportInfo(topFilename.c_str()));
	  exportImage(srcImageRange(outBot), vigra::ImageExportInfo(botFilename.c_str()));

        }
    }
  catch (vigra::StdException & e)
    {
      // catch any errors that might have occured and print their reason
      std::cout << e.what() << std::endl;
      //      return 1;
    }
}


int main(int argc, char ** argv)
{
  if(argc == 0)
    {
      std::cout << "Usage: deinterlace file1 [file2, file3, ...]\n";
      return 1;
    }
  
  
  // for each file on the command line
    for(int i = 0; i < argc; ++i)
      {
        deinterlace(argv[i]);      
      }
  
}
