/****************************************************************************
** HistogramViewer meta object code from reading C++ file 'histogramviewer.h'
**
** Created: Fri Feb 17 15:31:25 2006
**      by: The Qt MOC ($Id: qt/moc_yacc.cpp   3.3.4   edited Jan 21 18:14 $)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#undef QT_NO_COMPAT
#include "../histogramviewer.h"
#include <qmetaobject.h>
#include <qapplication.h>

#include <private/qucomextra_p.h>
#if !defined(Q_MOC_OUTPUT_REVISION) || (Q_MOC_OUTPUT_REVISION != 26)
#error "This file was generated using the moc from 3.3.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

const char *HistogramViewer::className() const
{
    return "HistogramViewer";
}

QMetaObject *HistogramViewer::metaObj = 0;
static QMetaObjectCleanUp cleanUp_HistogramViewer( "HistogramViewer", &HistogramViewer::staticMetaObject );

#ifndef QT_NO_TRANSLATION
QString HistogramViewer::tr( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "HistogramViewer", s, c, QApplication::DefaultCodec );
    else
	return QString::fromLatin1( s );
}
#ifndef QT_NO_TRANSLATION_UTF8
QString HistogramViewer::trUtf8( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "HistogramViewer", s, c, QApplication::UnicodeUTF8 );
    else
	return QString::fromUtf8( s );
}
#endif // QT_NO_TRANSLATION_UTF8

#endif // QT_NO_TRANSLATION

QMetaObject* HistogramViewer::staticMetaObject()
{
    if ( metaObj )
	return metaObj;
    QMetaObject* parentObject = QWidget::staticMetaObject();
    static const QUParameter param_slot_0[] = {
	{ "hist", &static_QUType_ptr, "Histogram", QUParameter::In }
    };
    static const QUMethod slot_0 = {"newHistogram", 1, param_slot_0 };
    static const QUParameter param_slot_1[] = {
	{ "i", &static_QUType_int, 0, QUParameter::In }
    };
    static const QUMethod slot_1 = {"setMax", 1, param_slot_1 };
    static const QMetaData slot_tbl[] = {
	{ "newHistogram(const Histogram&)", &slot_0, QMetaData::Public },
	{ "setMax(int)", &slot_1, QMetaData::Public }
    };
    metaObj = QMetaObject::new_metaobject(
	"HistogramViewer", parentObject,
	slot_tbl, 2,
	0, 0,
#ifndef QT_NO_PROPERTIES
	0, 0,
	0, 0,
#endif // QT_NO_PROPERTIES
	0, 0 );
    cleanUp_HistogramViewer.setMetaObject( metaObj );
    return metaObj;
}

void* HistogramViewer::qt_cast( const char* clname )
{
    if ( !qstrcmp( clname, "HistogramViewer" ) )
	return this;
    return QWidget::qt_cast( clname );
}

bool HistogramViewer::qt_invoke( int _id, QUObject* _o )
{
    switch ( _id - staticMetaObject()->slotOffset() ) {
    case 0: newHistogram((const Histogram&)*((const Histogram*)static_QUType_ptr.get(_o+1))); break;
    case 1: setMax((int)static_QUType_int.get(_o+1)); break;
    default:
	return QWidget::qt_invoke( _id, _o );
    }
    return TRUE;
}

bool HistogramViewer::qt_emit( int _id, QUObject* _o )
{
    return QWidget::qt_emit(_id,_o);
}
#ifndef QT_NO_PROPERTIES

bool HistogramViewer::qt_property( int id, int f, QVariant* v)
{
    return QWidget::qt_property( id, f, v);
}

bool HistogramViewer::qt_static_property( QObject* , int , int , QVariant* ){ return FALSE; }
#endif // QT_NO_PROPERTIES
