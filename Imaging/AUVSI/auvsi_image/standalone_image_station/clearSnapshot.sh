#!/bin/bash

mysql --user=root -e "drop table snapshots" auvsi

mysql --user=root -e "create table snapshots (NUMBER integer primary key, TIME timestamp(14), MICROSECONDS integer(11));" auvsi 

mysqlshow --user=root auvsi snapshots

echo "Contents of table: "

mysql --user=root -e "select * from snapshots" auvsi