// $Id: tsaiplanarcalibration.h,v 1.1.1.1.2.5 2004/11/18 21:54:20 cvs Exp $
// A Baseclass for different calibration methods.

#ifndef _TSAI_PLANAR_CALIBRATION_H_
#define _TSAI_PLANAR_CALIBRATION_H_

#include <math.h>
#include "calibration.h"
#include "minpack/lmdif.h"

inline double
CBRT (double x)
{
  if (x == 0)
    return (0);
  else if (x > 0)
    return (pow (x, (double) 1.0 / 3.0));
  else
    return (-pow (-x, (double) 1.0 / 3.0));
}

inline double
SQRT (double x)
{
  return sqrt (fabs (x));
}

/* Commonly used macros */
#define ABS(a)          (((a) > 0) ? (a) : -(a))
#define SIGNBIT(a)      (((a) > 0) ? 0 : 1)
#define SQR(a)          ((a) * (a))
#define CUB(a)          ((a)*(a)*(a))
#define MAX(a,b)        (((a) > (b)) ? (a) : (b))
#define MIN(a,b)        (((a) < (b)) ? (a) : (b))

/* Some math libraries may not have the sincos() routine */
#ifdef _SINCOS_
void sincos (double x, double *s, double *c);
#define SINCOS(x,s,c)   sincos(x,&s,&c)
#else
double sin (double x), cos (double x);
#define SINCOS(x,s,c)   s=sin(x);c=cos(x)
#endif

#define MAX_POINTS 512

struct calibration_data
{
  int point_count;		/* [points]      */
  double xw[MAX_POINTS];	/* [mm]          */
  double yw[MAX_POINTS];	/* [mm]          */
  double zw[MAX_POINTS];	/* [mm]          */
  double Xf[MAX_POINTS];	/* [pix]         */
  double Yf[MAX_POINTS];	/* [pix]         */
};

struct calibration_constants
{
  double f;			/* [mm]          */
  double kappa1;		/* [1/mm^2]      */
  double p1;			/* [1/mm]        */
  double p2;			/* [1/mm]        */
  double Tx;			/* [mm]          */
  double Ty;			/* [mm]          */
  double Tz;			/* [mm]          */
  double Rx;			/* [rad]         */
  double Ry;			/* [rad]         */
  double Rz;			/* [rad]         */
  double r1;			/* []            */
  double r2;			/* []            */
  double r3;			/* []            */
  double r4;			/* []            */
  double r5;			/* []            */
  double r6;			/* []            */
  double r7;			/* []            */
  double r8;			/* []            */
  double r9;			/* []            */
};


/* An arbitrary tolerance factor */
#define EPSILON		1.0E-8

/* Parameters controlling MINPACK's lmdif() optimization routine. */
/* See the file lmdif.f for definitions of each parameter.        */
#define REL_SENSOR_TOLERANCE_ftol    1.0E-5	/* [pix] */
#define REL_PARAM_TOLERANCE_xtol     1.0E-7
#define ORTHO_TOLERANCE_gtol         0.0
#define MAXFEV                       (1000*n)
#define EPSFCN                       1.0E-16	/* Do not set to zero! */
#define MODE                         1	/* variables are scaled internally */
#define FACTOR                       100.0

class TsaiPlanarCalibration:public Calibration
{
public:
  TsaiPlanarCalibration( int width, int height);
  TsaiPlanarCalibration( Camera camera, std::string calibPointsFilename );
  TsaiPlanarCalibration( Camera camera );
  TsaiPlanarCalibration( std::string cameraFilename, std::string calibPointsFilename );
  
  void loadCalibrationData (std::string filename);

  void recalibrate (void);
  
  void worldCoordToImageCoord (double const xw, double const yw, double const zw, double * const Xf, double * const Yf);
  void imageCoordToWorldCoord (double const Xfd, double const Yfd, double const zw, double * const xw, double * const yw);
  void worldCoordToCameraCoord (double xw, double yw, double zw, double *xc, double *yc, double *zc);
  void cameraCoordToWorldCoord (double xc, double yc, double zc, double *xw, double *yw, double *zw);
  void distortedToUndistortedSensorCoord (double Xd, double Yd, double *Xu, double *Yu);
  void undistortedToDistortedSensorCoord (double Xu, double Yu, double *Xd, double *Yd);
  void distortedToUndistortedImageCoord (double Xfd, double Yfd, double *Xfu, double *Yfu);
  void undistortedToDistortedImageCoord (double Xfu, double Yfu, double *Xfd, double *Yfd);
  void imageCoordToCameraCoord (double Xfd, double Yfd, double zc, double *xc, double *yc);
  std::string getErrors( void );
  double getMaxError( void );
  void printCameraData();
  
 private:
  void coplanarCalibration (void);
  void coplanarCalibrationWithFullOptimization (void);
  void ccComputeXdYdAndRSquared (void);
  void ccComputeU (void);
  void ccComputeTxAndTy (void);
  void ccComputeR (void);
  void ccComputeApproximateFAndTz (void);
  void ccComputeExactFAndTz (void);
  int  ccComputeExactFAndTzError (int *m_ptr, int *n_ptr, double *params, double *err);
  void ccThreeParmOptimization (void);
  void ccRemoveSensorPlaneDistortionFromXdAndYd (void);
  int ccFiveParmOptimizationWithLateDistortionRemovalError (int * m_ptr, int * n_ptr, double * params, double * err);
  void ccFiveParmOptimizationWithLateDistortionRemoval (void);
  int ccFiveParmOptimizationWithEarlyDistortionRemovalError (int * m_ptr, int * n_ptr, double * params, double * err);
  void ccFiveParmOptimizationWithEarlyDistortionRemoval (void);
  int ccNicOptimizationError (int * m_ptr, int * n_ptr, double * params, double * err);
  void ccNicOptimization (void);
  int ccFullOptimizationError (int *m_ptr, int *n_ptr, double *params, double * err);
  void ccFullOptimization (void);
  void solveRPYTransform (void);
  void applyRPYTransform (void);

  void generateCalibrationData (void);
  void calcErrorStats (void);


  struct calibration_constants cc;
  // struct calibration_data cd;

/* Local working storage */
  double Xd[MAX_POINTS];
  double Yd[MAX_POINTS];
  double r_squared[MAX_POINTS];
  double U[7];

  // Variables that hold the calibration errors
  double mean, stddev, max, sse;
  
  static class TsaiPlanarCalibration * lmdif_this;
  static int wrapperCcComputeExactFAndTzError( int * m_ptr, int * n_ptr, double * params, double * err ); 
  static int wrapperCcFiveParmOptimizationWithLateDistortionRemovalError( int * m_ptr, int * n_ptr, double * params, double * err );
  static int wrapperCcFiveParmOptimizationWithEarlyDistortionRemovalError(int * m_ptr, int * n_ptr, double * params, double * err );
  static int wrapperCcNicOptimizationError(int * m_ptr, int * n_ptr, double * params, double * err );
  static int wrapperCcFullOptimizationError(int * m_ptr, int * n_ptr, double * params, double * err );


};

#endif
