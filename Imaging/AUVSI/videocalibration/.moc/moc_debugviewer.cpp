/****************************************************************************
** DebugViewer meta object code from reading C++ file 'debugviewer.h'
**
** Created: Fri Feb 17 15:31:27 2006
**      by: The Qt MOC ($Id: qt/moc_yacc.cpp   3.3.4   edited Jan 21 18:14 $)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#undef QT_NO_COMPAT
#include "../debugviewer.h"
#include <qmetaobject.h>
#include <qapplication.h>

#include <private/qucomextra_p.h>
#if !defined(Q_MOC_OUTPUT_REVISION) || (Q_MOC_OUTPUT_REVISION != 26)
#error "This file was generated using the moc from 3.3.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

const char *DebugViewer::className() const
{
    return "DebugViewer";
}

QMetaObject *DebugViewer::metaObj = 0;
static QMetaObjectCleanUp cleanUp_DebugViewer( "DebugViewer", &DebugViewer::staticMetaObject );

#ifndef QT_NO_TRANSLATION
QString DebugViewer::tr( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "DebugViewer", s, c, QApplication::DefaultCodec );
    else
	return QString::fromLatin1( s );
}
#ifndef QT_NO_TRANSLATION_UTF8
QString DebugViewer::trUtf8( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "DebugViewer", s, c, QApplication::UnicodeUTF8 );
    else
	return QString::fromUtf8( s );
}
#endif // QT_NO_TRANSLATION_UTF8

#endif // QT_NO_TRANSLATION

QMetaObject* DebugViewer::staticMetaObject()
{
    if ( metaObj )
	return metaObj;
    QMetaObject* parentObject = FrameBufferViewer::staticMetaObject();
    static const QUParameter param_slot_0[] = {
	{ "frame", &static_QUType_ptr, "FrameBuffer", QUParameter::In }
    };
    static const QUMethod slot_0 = {"newFrame", 1, param_slot_0 };
    static const QMetaData slot_tbl[] = {
	{ "newFrame(FrameBuffer*)", &slot_0, QMetaData::Public }
    };
    static const QUParameter param_signal_0[] = {
	{ "hist", &static_QUType_ptr, "Histogram", QUParameter::In }
    };
    static const QUMethod signal_0 = {"newHistogram", 1, param_signal_0 };
    static const QMetaData signal_tbl[] = {
	{ "newHistogram(const Histogram&)", &signal_0, QMetaData::Public }
    };
    metaObj = QMetaObject::new_metaobject(
	"DebugViewer", parentObject,
	slot_tbl, 1,
	signal_tbl, 1,
#ifndef QT_NO_PROPERTIES
	0, 0,
	0, 0,
#endif // QT_NO_PROPERTIES
	0, 0 );
    cleanUp_DebugViewer.setMetaObject( metaObj );
    return metaObj;
}

void* DebugViewer::qt_cast( const char* clname )
{
    if ( !qstrcmp( clname, "DebugViewer" ) )
	return this;
    return FrameBufferViewer::qt_cast( clname );
}

#include <qobjectdefs.h>
#include <qsignalslotimp.h>

// SIGNAL newHistogram
void DebugViewer::newHistogram( const Histogram& t0 )
{
    if ( signalsBlocked() )
	return;
    QConnectionList *clist = receivers( staticMetaObject()->signalOffset() + 0 );
    if ( !clist )
	return;
    QUObject o[2];
    static_QUType_ptr.set(o+1,&t0);
    activate_signal( clist, o );
}

bool DebugViewer::qt_invoke( int _id, QUObject* _o )
{
    switch ( _id - staticMetaObject()->slotOffset() ) {
    case 0: newFrame((FrameBuffer*)static_QUType_ptr.get(_o+1)); break;
    default:
	return FrameBufferViewer::qt_invoke( _id, _o );
    }
    return TRUE;
}

bool DebugViewer::qt_emit( int _id, QUObject* _o )
{
    switch ( _id - staticMetaObject()->signalOffset() ) {
    case 0: newHistogram((const Histogram&)*((const Histogram*)static_QUType_ptr.get(_o+1))); break;
    default:
	return FrameBufferViewer::qt_emit(_id,_o);
    }
    return TRUE;
}
#ifndef QT_NO_PROPERTIES

bool DebugViewer::qt_property( int id, int f, QVariant* v)
{
    return FrameBufferViewer::qt_property( id, f, v);
}

bool DebugViewer::qt_static_property( QObject* , int , int , QVariant* ){ return FALSE; }
#endif // QT_NO_PROPERTIES
