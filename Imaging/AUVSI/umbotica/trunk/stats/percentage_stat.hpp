#ifndef UMBOTICA_PERCENTAGE_STAT_HPP
#define UMBOTICA_PERCENTAGE_STAT_HPP

#include "runtime_stat_tracker.hpp"
#include <sstream>
#include <iomanip>
#include "../threads/null_mutex.hpp"

namespace umbotica { namespace stats {
#ifdef UMBOTICA_NO_STATISTICS

  namespace detail
  {
    template<typename MUTEX_T = umbotica::threads::null_mutex>
    class Incrementer
    {
    public:
      Incrementer()
      {
	// 0
      }
      
      Incrementer & operator++(){return *this;}
      Incrementer & operator++(int){return *this;}
      Incrementer & operator+=(unsigned long){return *this;}
    };
  }

  template<typename MUTEX_T = umbotica::threads::null_mutex>
  class Percentage : public umbotica::stats::RuntimeStat
  {
  public:
    typedef MUTEX_T mutex_t;
    typedef typename mutex_t::scoped_lock lock_t;

    template<typename STAT_TRACKER_T>
    Percentage(std::string category, std::string name, STAT_TRACKER_T * statTrack) :
      RuntimeStat(category, name, statTrack)
    {
      // 0
    }
    
    Percentage() : RuntimeStat()
    {
      // 0
    } 
    
    void insert(std::string)
    {
    }

    umbotica::stats::detail::Incrementer<mutex_t> operator[](std::string)
    {
      return umbotica::stats::detail::Incrementer<mutex_t>();
    }
    
    std::string results() const
    {
      return "No runtime stats";
    }
  };



#else

  namespace detail
  {
    template<typename MUTEX_T = umbotica::threads::null_mutex>
    class Incrementer
    {
    public:
      typedef MUTEX_T mutex_t;
      typedef typename mutex_t::scoped_lock lock_t;
      Incrementer(unsigned long & val, mutex_t & mutex) : m_val(val), m_mutex(mutex)
      {
	// 0
      }
      
      Incrementer & operator++(){lock_t lock(m_mutex); m_val++; return *this;}
      Incrementer & operator++(int){lock_t lock(m_mutex); m_val++; return *this;}
      Incrementer & operator+=(unsigned long val){lock_t lock(m_mutex); m_val += val; return *this;}

    private:
      unsigned long & m_val;
      mutable mutex_t & m_mutex;
    };
  }



  template<typename MUTEX_T = umbotica::threads::null_mutex>
  class Percentage : public umbotica::stats::RuntimeStat
  {
  public:
    typedef MUTEX_T mutex_t;
    typedef typename mutex_t::scoped_lock lock_t;
    
    template<typename STAT_TRACKER_T>
    Percentage(std::string category, std::string name, STAT_TRACKER_T * statTrack) :
      RuntimeStat(category, name, statTrack)
    {
      // 0
    }
    
    Percentage() : RuntimeStat()
    {
      // 0
    } 
    
    void insert(std::string name)
    {
      lock_t lock(m_mutex);
      std::map<std::string, unsigned long>::iterator i = m_counts.find(name);
      if(i == m_counts.end())
	m_counts[name] = 0;
    }

    umbotica::stats::detail::Incrementer<mutex_t> operator[](std::string part)
    {
      return umbotica::stats::detail::Incrementer<mutex_t>(m_counts[part], m_mutex);
    }
    
    std::string results() const
    {
      lock_t lock(m_mutex);
      std::stringstream stream;
      double total = 0;
      unsigned int nameWidth = 0;
      unsigned long maxCount = 0;
      std::map<std::string, unsigned long>::const_iterator counts_it = m_counts.begin();
      for( ; counts_it != m_counts.end(); counts_it++)
	{
	  total += counts_it->second;
	  nameWidth = std::max(nameWidth, counts_it->first.size());
	  maxCount = std::max(maxCount, counts_it->second);
	}
      
      unsigned int places = 0;
      while(maxCount > 0)
	{
	  places++;
	  maxCount /= 10;
	}

      if(total == 0)
	{
	  counts_it = m_counts.begin();
	  for( ; counts_it != m_counts.end(); counts_it++)
	    {
	      stream << "\n";
	      stream << std::setfill(' ') << std::setw(nameWidth + 2) << (counts_it->first + ": ");
	      stream << "  0   0.0%";
	    }
	  places = 1;
	}
      else
	{
	  counts_it = m_counts.begin();
	  for( ; counts_it != m_counts.end(); counts_it++)
	    {
	      stream << "\n";
	      stream << std::setfill(' ') << std::setw(nameWidth + 2) << (counts_it->first + ": ");
	      stream << std::setw(places + 2) << counts_it->second << "   ";

	      stream << std::setprecision(2) << std::setw(6) << std::setiosflags(std::ios::right | std::ios::fixed | std::ios::showpoint);
 	      double percentage = 100.0 * ((double)counts_it->second / total);
	      stream << percentage << "%";
	    }
	}

      stream << "\n";
      stream << std::setfill(' ') << std::setw(nameWidth + 2) << "total: ";
      stream << std::setw(places + 2) << (int)total;

      return stream.str();
    }

  private:
    std::map<std::string, unsigned long> m_counts;
    mutable mutex_t m_mutex;
  };

#endif // UMBOTICA_NO_STATISTICS

}}

#endif
