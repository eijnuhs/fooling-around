
#include <qevent.h>
#include "videocalibrationviewer.h"
#include "videocalibration.h"
#include "framebufferviewer.h"
#undef min
#undef max
#include <sstream>
#include "framebuffer.h"
#include <qpainter.h>
#include <qcolor.h>
#include <iostream>
#include <time.h>
#include <inttypes.h>
#include <mainwindow.h>
#include <qcheckbox.h>
#include <qtabwidget.h>
#include <qcombobox.h>
#include <assert.h>
#include "pixel.h"
#include <qpixmap.h>
#include "mask.h"
#include "framebufferrgb32.h"
#include "framebufferworld.h"
#include "paintbox.h"
#include "controlpointscarpetcalib.h"

#include "visitedmap.h"
#include "imageprocessing.h"
#include <qspinbox.h>
#include <qfiledialog.h>
#include <qlineedit.h>
#include <qcheckbox.h>
#include <qlabel.h>
#include "colourview.h"

using namespace std;

VideoCalibrationViewer::VideoCalibrationViewer( QWidget * parent, char const * name )
  : FrameBufferViewer( parent, name ),
    vCal( 0 ),
    grabbed ( 0 )
{
  this->QWidget::setMouseTracking(true);
}

VideoCalibrationViewer::~VideoCalibrationViewer()
{
}

void VideoCalibrationViewer::setVideoCalibration(VideoCalibration * init_vCal )
{
  vCal = init_vCal; 
  // perform requisite initialization
  //  setIntensityMin(mainWindow->spnIntensityMin->value());
  //  setIntensityMax(mainWindow->spnIntensityMax->value());
  setPixelMin(mainWindow->spnPixelsMin->value());
  setPixelMax(mainWindow->spnPixelsMax->value());
  setState(mainWindow->cboCalibrationState->currentText().ascii());
  setFeatureX(mainWindow->lneFeatureWidth->text());
  setFeatureY(mainWindow->lneFeatureHeight->text());
  setFillOn(mainWindow->chkFillOutside->isChecked());
  setControlPoints();

#ifdef XXDEBUG
  std::cout << "VideoCalibrationViewer::setVideoCalibration\n";
  std::cout << "vCal = " << (void *) vCal << std::endl;
#endif
}

void VideoCalibrationViewer::setFeatureX(const QString & fx)
{
#ifdef XXDEBUG
  std::cout << "VideoCalibrationViewer::setFeatureX: " << atof(fx.ascii()) << "\n";
#endif
  if(vCal)
    vCal->setFeatureX(atof(fx.ascii()));
}
void VideoCalibrationViewer::setFeatureY(const QString & fy)
{
#ifdef XXDEBUG
  std::cout << "VideoCalibrationViewer::setFeatureY: "  << atof(fy.ascii()) << " \n";
#endif
  if(vCal)
    vCal->setFeatureY(atof(fy.ascii()));
}

void 
VideoCalibrationViewer::resizeEvent(QResizeEvent * e)
{
  setControlPoints();
  QWidget::resizeEvent(e);
}

void VideoCalibrationViewer::setControlPoints()
{
  if(vCal && frame)
    {
      vCal->setControlPoints(frame->height, frame->width);
    }
}

 
void VideoCalibrationViewer::newFrame( FrameBuffer * frame )
{
#ifdef XXDEBUG
  std::cout << "VideoCalibrationViewer::nextFrame received signal newFrame" << endl;
#endif
  FrameBufferViewer::newFrame( frame );
}

void 
VideoCalibrationViewer::paintChild( QPainter & p)
{

#ifdef XXDEBUG
  std::cout << "VideoCalibrationViewer::paintChild called" << endl;
  std::cout << "Widget: " <<  QWidget::width() << ", " << QWidget::height() << endl; 
#endif
  int wx,wy; // widget x and y

  if ( vCal == 0 )
    {
      std::cerr << "ERROR: VideoCalibrationViewer vCal is not set. Unable to continue" << endl;
      exit( EXIT_FAILURE );
    }

  // paint control points and lines between them.
  ControlPoints pts(vCal->getPoints());
  for(int i = 0; i < pts.size() ; ++i)
    {
      FrameBufferViewer::frameToWidgetCoordinates(pts.points[i].xm, pts.points[i].ym, &wx, &wy);
      PaintBox::drawPoint(p, wx, wy);
    }

  if(pts.size() == 4)
    {
      p.setPen(Qt::red);
      int wx0, wy0, wx1, wy1, wx2, wy2, wx3, wy3;
      FrameBufferViewer::frameToWidgetCoordinates(pts.points[0].xm,pts.points[0].ym, &wx0, &wy0);
      FrameBufferViewer::frameToWidgetCoordinates(pts.points[1].xm,pts.points[1].ym, &wx1, &wy1);
      FrameBufferViewer::frameToWidgetCoordinates(pts.points[2].xm,pts.points[2].ym, &wx2, &wy2);
      FrameBufferViewer::frameToWidgetCoordinates(pts.points[3].xm,pts.points[3].ym, &wx3, &wy3);
      p.drawLine  (wx0,wy0,wx1,wy1);
      p.drawLine  (wx1,wy1,wx2,wy2);
      p.drawLine  (wx2,wy2,wx3,wy3);
      p.drawLine  (wx3,wy3,wx0,wy0);
    }

  mainWindow->lblSquares->setText("Squares: " + QString::number(vCal->getSquares().size()));
#ifdef XXDEBUG
  cout << "setting lblSquares: " << "Squares " << vCal->getSquares().size() << endl;
  cout << "Result: " << mainWindow->lblSquares->text().ascii() << endl;
#endif
  // if it is the setup state, paint the squares.
  // if it is the calibration state, paint the squares and block coordinates and update the error.

  if(mainWindow->cboCalibrationState->currentText() == "Setup")
    {
      // Setup state.
      for (list < Square >::const_iterator it = vCal->getSquares().begin (); it != vCal->getSquares().end (); ++it)
	{
	  // paint square
	  FrameBufferViewer::screenToWidgetCoordinates((*it).x, (*it).y, &wx, &wy);
	  PaintBox::drawPoint(p, wx,wy, 5, Qt::cyan);
	}
    }
  else 
    {
      // Calibration state.
      //      mainWindow->lneCalibError->setText(QString::number(vCal->getCalibrationError()));
      for (list < Square >::const_iterator it = vCal->getSquares().begin (); it != vCal->getSquares().end (); ++it)
	{
	  // paint square & block coordinates.
	  FrameBufferViewer::screenToWidgetCoordinates((*it).x, (*it).y, &wx, &wy);
	  QString label = QString::number((*it).block_x) + "," + QString::number((*it).block_y);
	  int even = ((*it).block_x) % 2;
	  QColor color = even == 0 ? Qt::green : Qt::cyan;
	  PaintBox::drawPoint(p, wx,wy, 5, label, 5, color, color);
      	}
    }

  mainWindow->lblCalibrationError->setText(QString(vCal->getCalibrationErrorMessage()));
#ifdef XXDEBUG
  cout << "setting lblCalibrationError: " << vCal->getCalibrationErrorMessage() << endl;
  cout << "Result: " << mainWindow->lblCalibrationError->text().ascii() << endl;
#endif
}



void VideoCalibrationViewer::mousePressEvent(QMouseEvent * e)
{
  int sx,sy;
  FrameBufferViewer::widgetToFrameCoordinates(e->x(), e->y(), &sx, &sy);
  
  if(e->button() == Qt::LeftButton)
    {
      grabbed = vCal->controlPoint(sx, sy);
      if(grabbed == 0)
	{
	  updateIntensity(e);
	}
    }
  else if ( e->button() == Qt::RightButton )
    {
      updateIntensity(e);
    }  
  
#ifdef XXDEBUG
   std::cout << "VideoInterpreter2dViewer::mousePressEvent\n";
 std::cout << "Screen: (" << e->x() << "," << e->y() << ")\n";
  if(grabbed)
   std::cout << "Grabbed: " << *grabbed << std::endl;
  else
    std::cout << " Nothing grabbed." << std::endl;
  //  std::cout << "Num of CPs: " <<  vi2d->rotationMatrix.points.size() << endl;
#endif

  FrameBufferViewer::mousePressEvent(e);
}


// modified to use colour!
void VideoCalibrationViewer::updateIntensity(QMouseEvent * e)
{
  int sx,sy;
  FrameBufferViewer::widgetToFrameCoordinates(e->x(), e->y(), &sx, &sy); 
  if ( ( image != 0 ) && ( e->x() >= 0 ) && ( e->y() < width() - 1 ) && ( e->y() >= 0 ) && ( e->y() < height() - 1 ) )
    {
       uint8_t *p = (uint8_t *)( ( uint32_t * ) image->scanLine( sy ) + sx);
       Pixel pix(*(p+2),*(p+1),*(p+0));
       if(e->button() == Qt::RightButton)
 	{
 	  vCal->colour.reset();
 	}
       else if(e->button() == Qt::LeftButton)
 	{
	  vCal->addColour(sx,sy);
 	}

    }

  RawPixel av = vCal->colour.getAverageColour();
  QColor q(av.red,av.green,av.blue);
  mainWindow->vc_colourView->setColour(q);
}


void VideoCalibrationViewer::mouseMoveEvent(QMouseEvent * e)
{
  updateControlPoint(e->x(), e->y());
  if(!grabbed )
    {
      updateIntensity(e);
    }      

#ifdef XXDEBUG
  std::cout << "VideoInterpreter2dViewer::mouseMoveEvent\n";
  std::cout << "(" << e->x() << "," << e->y() << ")\n";
#endif

  FrameBufferViewer::mouseMoveEvent(e);
    
}

void VideoCalibrationViewer::mouseReleaseEvent(QMouseEvent * e)
{
  updateControlPoint(e->x(), e->y());
  if(grabbed)
    {
#ifdef XXDEBUG
      std::cout << "VideoInterpreter2dViewer::mouseReleaseEvent\n";
      std::cout << "(" << e->x() << "," << e->y() << ")\n";
#endif
      grabbed = 0;
    }
    

}

void VideoCalibrationViewer::updateControlPoint(int wx, int wy)
{

  if(grabbed)
    {
      wx = Geometry::clamp(10, wx, QWidget::width() - 10);
      wy = Geometry::clamp(10, wy, QWidget::height() - 10);
      int sx,sy;
      // adjust the screen co-ordinates of the point. 
      FrameBufferViewer::widgetToFrameCoordinates(wx,wy,&sx,&sy);
      grabbed->xm = sx;
      grabbed->ym = sy; 
#ifdef XXDEBUG
      std::cout << "wx,wy: " << wx << "," << wy << endl;
      std::cout << "sx,sy: " << sx << "," << sy << endl;
      std::cout << "Clamp w,h : " << QWidget::width() << "," << QWidget::height() << endl;
      std::cout << "Updating Control point: " << *grabbed << std::endl;
#endif      
    
    }

}

void VideoCalibrationViewer::setIntensityMin(int iMin)
{
#ifdef XXDEBUG
  std::cout << "VideoCalibrationViewer setting intensityMin " << iMin << std::endl;
#endif
  if(vCal)
    vCal->setIntensityMin(iMin);
}
void VideoCalibrationViewer::setIntensityMax(int iMax)
{
#ifdef XXDEBUG
  std::cout << "VideoCalibrationViewer setting intensityMax " << iMax << std::endl;
#endif
  if(vCal)
    vCal->setIntensityMax(iMax);
}
void VideoCalibrationViewer::setPixelMin(int pMin)
{
#ifdef XXDEBUG
  std::cout << "VideoCalibrationViewer setting pixelMin " << pMin << std::endl;
#endif
  if(vCal)
    vCal->setPixelMin(pMin);
}
void VideoCalibrationViewer::setPixelMax(int pMax)
{
#ifdef XXDEBUG
  std::cout << "VideoCalibrationViewer setting pixelMax " << pMax << std::endl;
#endif
  if(vCal)
    vCal->setPixelMax(pMax);
}
void VideoCalibrationViewer::setState(const QString & state)
{
#ifdef XXDEBUG
  std::cout << "VideoCalibrationViewer setting state " << state.ascii() << std::endl;
#endif
  if(vCal)
    vCal->setState(state.ascii());
}

void VideoCalibrationViewer::checkEnabled(const QString & tab)
{
#ifdef XXDEBUG
  std::cout << "VideoCalibrationViewer::checkEnabled, tab = " << tab.ascii() << std::endl;
#endif

  if (vCal)
    {
      if(tab == QString("C&alibration"))
	{
#ifdef XXDEBUG
	  std::cout << "VideoCalibration enabled" <<  std::endl;
#endif
	  vCal->enableProcessing(true);
	}
      else
	{
#ifdef XXDEBUG
	  std::cout << "VideoCalibration disabled" <<  std::endl;
#endif
	  vCal->enableProcessing(false);
	}
    }
}

void VideoCalibrationViewer::setFillOn(bool setOn)
{
#ifdef XXDEBUG
  std::cout << "VideoCalibration::setFillOn" <<  std::endl;
#endif
  if(vCal)
    vCal->setExternalFill(setOn);
}

void VideoCalibrationViewer::loadCalibration()
{
  QString s = QFileDialog::getOpenFileName(QString(vCal->getCalibrationFilename()) , "Configuration files (*.dat *.cfg)", mainWindow, "open file dialog", "Choose a file to open" );
  if(!s.isNull() && !s.isEmpty())
    {
      vCal->loadCalibration(s.ascii());
    }
}

void VideoCalibrationViewer::saveCalibration()
{
  QString s = QFileDialog::getSaveFileName( QString(vCal->getCalibrationFilename()), "Configuration files (*.dat *.cfg)", mainWindow, "save file dialog", "Choose a filename to save under" );
  if(!s.isNull() && !s.isEmpty())
    {
      vCal->saveCalibration(s.ascii());
    }

}

void VideoCalibrationViewer::resetControlPoints()
{

}


void VideoCalibrationViewer::updateQImageFromFrameBuffer( FrameBuffer * frame )
{
  QMutexLocker qml(&vCal->mutex);
  FrameBufferViewer::updateQImageFromFrameBuffer( frame );
}
