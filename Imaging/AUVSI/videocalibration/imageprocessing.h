// $Id: imageprocessing.h,v 1.1.1.1.2.4 2005/01/26 01:21:55 cvs Exp $
//
// Jacky Baltes <jacky@cs.umanitoba.ca> Fri Jun 18 16:08:21 CDT 2004
//

#ifndef _IMAGE_PROCESSING_H_
#define _IMAGE_PROCESSING_H_

#include "pixel.h"
#include "framebuffer.h"
#include "run.h"

class FeatureDetector;
class VisitedMap;
class ColourDefinition;

class ImageProcessing
{
 public:
  enum EdgeDirection
    {
      NO_EDGE,
      UP,
      DOWN,
      ANY
    };

  enum FeatureType
    {
      NONE = 0,
      VERT_UP,
      VERT_DOWN,
      HORIZ_UP,
      HORIZ_DOWN,
      ANGLE45_UP,
      ANGLE45_DOWN,
      ANGLE135_UP,
      ANGLE135_DOWN,
      CORNER_BL,
      CORNER_BR,
      CORNER_TL,
      CORNER_TR,
      SINGLETON,
      CENTER,
      LAST
    };
  enum ThresholdLocation
    {
      TOP_LEFT = 0,
      BOTTOM_LEFT,
      TOP_RIGHT,
      BOTTOM_RIGHT,
      THRESHOLD_SIZE
    };

  static enum EdgeDirection calcEdgeDirection( RawPixel const & p1, RawPixel const & p2, double threshold );
  static int matchFeature( FeatureDetector f1, FeatureDetector f2 );
  static enum FeatureType classifyFeature( RawPixel pixels[ 3 ][ 3 ], double threshold, double confidence[ LAST ]  );
  static void highlightFeatures( FrameBuffer * frame, double threshold );
  static double detectCircle( FrameBuffer * frame, RawPixel color, unsigned int radius, unsigned int * xc = 0, unsigned int * yc = 0 );
  static int doFloodFill(FrameBufferIterator fbit, VisitedMap & visitedMap, int xc[], int yc[], int minIntensity, int maxIntensity, int maxPixels, int numPixels, bool clobberPixels = false);
  static int doFloodFill(FrameBufferIterator fbit, VisitedMap & visitedMap, int xc[], int yc[], ColourDefinition * colour, int maxPixels, int numPixels, bool clobberPixels = false);
  static void thresholdImage(FrameBuffer * frame, unsigned int thresholds[THRESHOLD_SIZE], RawPixel killed = RawPixel(0,0,0));
  //  static RunList runLengthEncode(FrameBuffer * frame, unsigned int x, unsigned int y, unsigned int height, unsigned int width);
};

class FeatureDetector
{
 public:
  FeatureDetector( );
  FeatureDetector( enum ImageProcessing::FeatureType type, enum ImageProcessing::EdgeDirection tl, enum ImageProcessing::EdgeDirection t, enum ImageProcessing::EdgeDirection tr, enum ImageProcessing::EdgeDirection l, enum ImageProcessing::EdgeDirection h, enum ImageProcessing::EdgeDirection r, enum ImageProcessing::EdgeDirection bl, enum ImageProcessing::EdgeDirection b, enum ImageProcessing::EdgeDirection br );
  

  enum ImageProcessing::FeatureType type;
  enum ImageProcessing::EdgeDirection map[ 3 ][ 3 ];
};  



#endif
