#include <iostream>
#include <qapplication.h> // for the QT binding.
#include <agmainwindow.hpp> // The main window class.
#include <agmanager.hpp>    // The AutoGui layout manager.
#include <agshapes.hpp> // for drawing datapoints
#include <boost/bind.hpp>
#include <umimage.hpp>
#include <umimagetypedefs.hpp>
#include <agdockwindow.hpp>
#include <agref.hpp>
#include <umimagegui.hpp>
#include "auvsi_image/calibration/auvsi_calibration.hpp"
#include "auvsi_common/gps.hpp"

const int CANVAS_WIDTH = 800;
const int CANVAS_HEIGHT = 800;

const int HighResWidth = 2592;
const int HighResHeight = 1944;

const int LowResWidth = 640;
const int LowResHeight = 480;

enum CameraType displayCamera = HighRes;

class CalibrationTest
{
public:
  UmRGB24 image;
  std::vector<AGShape> dataShapes;
  boost::shared_ptr<UmImageAgContainer<UmRGB24::PixelType> > m_image;
  
  CalibrationTest()
  {
    image.resize(CANVAS_WIDTH, CANVAS_HEIGHT);
    m_image.reset(new UmImageAgContainer<UmRGB24::PixelType>());
    m_image->setImage(image);
    roll = 0;
    pitch = 0;
    yaw = 0;
    latitude = 50.0;
    longitude = -90.0;
    altitude = 200;

    std::string pointsFile1("../calibration/highResolution.dat");
    std::string pointsFile2("../calibration/lowrespoints.dat");
    AuvsiCalibration::Instance()->initialize(HighRes, HighResWidth, HighResHeight, pointsFile1);
    AuvsiCalibration::Instance()->initialize(LowRes, LowResWidth, LowResHeight, pointsFile2);

    GpsCoord::setReference( GpsCoord(-90.0, 50.0) );
    GpsCoord bboxTopLeft( -90.1, 50.1 ); // bounding box of the canvas in gps coords
    GpsCoord bboxBottomRight( -89.9, 49.9 ); // bounding box of the canvas in gps coords

    // make sure the same scale is used in the x and y direction
    topLeftMetresX = GpsCoord(-90.0, 50.0).inMetres()[0] - 500;
    topLeftMetresY = GpsCoord(-90.0, 50.0).inMetres()[1] + 500;
    bottomRightMetresX = GpsCoord(-90.0, 50.0).inMetres()[0] + 500;
    bottomRightMetresY = GpsCoord(-90.0, 50.0).inMetres()[1] - 500;


  }

  void updateGraphics()
  {
    dataShapes.clear();

    int canvasX = 0;
    int canvasY = 0;

    AGColor color( 255, 0, 0 );

    GpsCoord gpsOrigin(-90,50);

    // Draw reference point
    AGShape origin;

    metresToCanvas(gpsOrigin.inMetres()[0], gpsOrigin.inMetres()[1], &canvasX, &canvasY);
    origin.circle( canvasX, canvasY, 3, color );

    AGShape originLabel;
    originLabel.text("(-90.0, 50)", CANVAS_WIDTH / 2, CANVAS_HEIGHT / 2 + 20, color, QFont("Arial") );

    // Get the bounding quadralateral of the image->ground transformation



    /*
    GpsCoord topLeft = AuvsiCalibration::Instance()->gpsCoordFromImageCoord( displayCamera, 0, 0,
									     roll, pitch, yaw,
									     GpsCoord( longitude, latitude ),
									     altitude );
    GpsCoord topRight = AuvsiCalibration::Instance()->gpsCoordFromImageCoord( displayCamera, HighResWidth - 1, 0,
									     roll, pitch, yaw,
									     GpsCoord( longitude, latitude ),
									     altitude );
    GpsCoord bottomLeft = AuvsiCalibration::Instance()->gpsCoordFromImageCoord( displayCamera, 0, HighResHeight - 1,
									     roll, pitch, yaw,
									     GpsCoord( longitude, latitude ),
									     altitude );
    GpsCoord bottomRight = AuvsiCalibration::Instance()->gpsCoordFromImageCoord( displayCamera, HighResWidth - 1, HighResHeight - 1,
									     roll, pitch, yaw,
									     GpsCoord( longitude, latitude ),
									     altitude );
    */

    // this is for the high res camera being rotated 90 degrees
    double useYaw = yaw + M_PI / 2; // rotate camera 90 degrees
    // have to reverse pitch and roll
    double useRoll = pitch;
    double usePitch = -roll; // to make sure positive roll is right wing low still

    
    GpsCoord topLeft = AuvsiCalibration::Instance()->gpsCoordFromImageCoord( displayCamera, 0, 0,
									     useRoll, usePitch, useYaw,
									     GpsCoord( longitude, latitude ),
									     altitude );
    GpsCoord topRight = AuvsiCalibration::Instance()->gpsCoordFromImageCoord( displayCamera, HighResWidth - 1, 0,
									     useRoll, usePitch, useYaw,
									     GpsCoord( longitude, latitude ),
									     altitude );
    GpsCoord bottomLeft = AuvsiCalibration::Instance()->gpsCoordFromImageCoord( displayCamera, 0, HighResHeight - 1,
									     useRoll, usePitch, useYaw,
									     GpsCoord( longitude, latitude ),
									     altitude );
    GpsCoord bottomRight = AuvsiCalibration::Instance()->gpsCoordFromImageCoord( displayCamera, HighResWidth - 1, HighResHeight - 1,
									     useRoll, usePitch, useYaw,
									     GpsCoord( longitude, latitude ),
									     altitude );

    // Get the corners
    std::vector<int> corners;

    metresToCanvas(bottomRight.inMetres()[0], bottomRight.inMetres()[1], &canvasX, &canvasY);
    corners.push_back(canvasX);
    corners.push_back(canvasY);

    metresToCanvas(bottomLeft.inMetres()[0], bottomLeft.inMetres()[1], &canvasX, &canvasY);
    corners.push_back(canvasX);
    corners.push_back(canvasY);

    metresToCanvas(topLeft.inMetres()[0], topLeft.inMetres()[1], &canvasX, &canvasY);
    corners.push_back(canvasX);
    corners.push_back(canvasY);

    AGShape shadow;
    shadow.multiLine( corners, color );

    std::vector<int> front;
    front.clear();

    metresToCanvas(topLeft.inMetres()[0], topLeft.inMetres()[1], &canvasX, &canvasY);
    front.push_back(canvasX);
    front.push_back(canvasY);

    metresToCanvas(topRight.inMetres()[0], topRight.inMetres()[1], &canvasX, &canvasY);
    front.push_back(canvasX);
    front.push_back(canvasY);

    color = AGColor( 0, 255, 0 );
    AGShape shadowFront;
    shadowFront.multiLine( front, color );

    std::vector<int> right;
    right.clear();

    metresToCanvas(topRight.inMetres()[0], topRight.inMetres()[1], &canvasX, &canvasY);
    right.push_back(canvasX);
    right.push_back(canvasY);

    metresToCanvas(bottomRight.inMetres()[0], bottomRight.inMetres()[1], &canvasX, &canvasY);
    right.push_back(canvasX);
    right.push_back(canvasY);

    color = AGColor( 0, 0, 255 );
    AGShape shadowRight;
    shadowRight.multiLine( right, color );

    dataShapes.push_back( origin );
    dataShapes.push_back( originLabel );
    dataShapes.push_back( shadow );
    dataShapes.push_back( shadowFront );
    dataShapes.push_back( shadowRight );

    m_image->setImage( image );
  }

  boost::shared_ptr<AGImageContainer> container()
  {
    return boost::dynamic_pointer_cast<AGImageContainer>( m_image );
  }
  
  std::vector<AGShape> shapes()
  {
    return dataShapes;
  }

  void setLongitude(double newVal)
  {
    longitude = newVal;
    updateGraphics();
  }

  void setLatitude(double newVal)
  {
    latitude = newVal;
    updateGraphics();
  }

  void setAltitude(double newVal)
  {
    altitude = newVal;
    updateGraphics();
  }

  void setRoll( double newVal )
  {
    roll = newVal;
    updateGraphics();
  }

  void setPitch( double newVal )
  {
    pitch = newVal;
    updateGraphics();
  }

  void setYaw ( double newVal )
  {
    yaw = newVal;
    updateGraphics();
  }

  double getLongitude()
  {
    return longitude;
  }

  double getLatitude()
  {
    return latitude;
  }

  double getAltitude()
  {
    return altitude;
  }

  double getRoll()
  {
    return roll;
  }

  double getPitch()
  {
    return pitch;
  }

  double getYaw()
  {
    return yaw;
  }
  
private:
  double pitch;
  double roll;
  double yaw;
  double latitude;
  double longitude;
  double altitude;

  double topLeftMetresX;
  double topLeftMetresY;
  double bottomRightMetresX;
  double bottomRightMetresY;

  void metresToCanvas(double xMetres, double yMetres, int *canvasX, int *canvasY)
  {
    *canvasX = (int)(CANVAS_WIDTH * (xMetres - topLeftMetresX) / (bottomRightMetresX - topLeftMetresX));
    *canvasY = (int)(CANVAS_HEIGHT * (yMetres - topLeftMetresY) / (bottomRightMetresY - topLeftMetresY));
  }

};

int main(int argc, char ** argv)
{
  QApplication a(argc, argv);
  AGMainWindow w;
  
  CalibrationTest p;
  
  a.connect( &a, SIGNAL( lastWindowClosed() ), &a, SLOT( quit() ) );
  AGManager & gui = *w.getAGManager();

  p.updateGraphics();
  
  gui.pushGroup("Image View");
  gui.addFrameBuffer("Test View", boost::function<boost::shared_ptr<AGImageContainer>() >(boost::bind(&CalibrationTest::container,&p)), boost::function<std::vector<AGShape>()>(boost::bind(&CalibrationTest::shapes, &p)));
  gui.setWindowSize(800,800);
  gui.popGroup();

  gui.pushGroup("Control");
  gui.addDouble("Latitude", boost::bind(&CalibrationTest::getLatitude, &p),
		boost::bind(&CalibrationTest::setLatitude, &p, _1),
		49.9, 50.1, 0.001);
  gui.nextLine();
  gui.addDouble("Longitude", boost::bind(&CalibrationTest::getLongitude, &p),
		boost::bind(&CalibrationTest::setLongitude, &p, _1),
		-90.1, -89.9, 0.001);
  gui.nextLine();
  gui.addDouble("Altitude", boost::bind(&CalibrationTest::getAltitude, &p),
		boost::bind(&CalibrationTest::setAltitude, &p, _1),
		-1000, 1000, 1);
  gui.nextLine();

  gui.addDouble("Roll", boost::bind(&CalibrationTest::getRoll, &p),
		boost::bind(&CalibrationTest::setRoll, &p, _1),
		-2*M_PI, 2*M_PI, 0.1);
  gui.nextLine();

  gui.addDouble("Pitch", boost::bind(&CalibrationTest::getPitch, &p),
		boost::bind(&CalibrationTest::setPitch, &p, _1),
		-2*M_PI, 2*M_PI, 0.1);
  gui.nextLine();

  gui.addDouble("Yaw", boost::bind(&CalibrationTest::getYaw, &p),
		boost::bind(&CalibrationTest::setYaw, &p, _1),
		-2*M_PI, 2*M_PI, 0.1);
  gui.nextLine();

  gui.addButton("Update", boost::bind(&CalibrationTest::updateGraphics, &p));
  gui.nextLine();

  gui.popGroup();
  
  w.resize(1000,700);
  w.show();
  a.exec();
}
