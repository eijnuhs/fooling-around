#ifndef __RUN_H_
#define __RUN_H_

#include <vector>
#include <iostream>
#include <list>
#include "trackable.h"
#include "pixel.h"

struct RunPoint
{
  RunPoint(unsigned int r, unsigned int c){row = r; col = c;}
  unsigned int row;
  unsigned int col;
};



// classes used for run length encoding of an image.
class Run;
class FrameBuffer;
typedef std::vector<Run> RunList;

class Run
{
 public:
  Run(unsigned int row = 0, unsigned int col = 0,
      RawPixel colour = RawPixel(0,0,0), unsigned int length = 0, int parent = -1);
  virtual ~Run();
  bool isBelow(const Run & run);
  int containsPoint(unsigned int pRow, unsigned int pCol);
  // these functions are similar to the above.  the 
  // run (or point) must overlap on the same row.
  int overlaps(RunPoint p);
  int overlaps(Run r);
  int overlaps(unsigned int row);


  unsigned int colMax() const;
  unsigned int getCOMX() const;
  unsigned int getCOMY() const;

  int getRunIdx(unsigned int row, unsigned int col);
  unsigned int row;
  unsigned int col;
  //  int colour;
  RawPixel colour;
  unsigned int length;
  int parent;  
  friend std::ostream & operator<<(std::ostream & stream, const Run & run);


};


class RunRegion
{
 public:
  RunRegion(unsigned int xMin = 0,unsigned int xMax = 0, 
	    unsigned int yMin = 0, unsigned int xMax = 0,
	    unsigned int numPixels = 0);
  RunRegion(const Run & run); 
  void absorb(RunRegion & region);
  void absorb(const Run & run);
  unsigned int xMin, xMax, yMin, yMax;
  unsigned int numPixels;
  bool used;
  enum Trackable::TrackableTypes type;
  int subtype;
  double theta;
  int getCOMXI() const;
  double getCOMXD() const;
  int getCOMYI() const;
  double getCOMYD() const;
  double getAvgCentreX() const;
  double getAvgCentreY() const;
  int getAvgCentreXI() const;
  int getAvgCentreYI() const;
  double getBBCentreX() const;
  double getBBCentreY() const;
  int getBBCentreXI() const;
  int getBBCentreYI() const;

  double getHWRatio() const;
  unsigned int getHeight() const;
  unsigned int getWidth() const;

  double getHWRatio();
  unsigned int getHeight();
  unsigned int getWidth();

  Pixel getAvgColour();

  friend std::ostream & operator<<(std::ostream & stream, const RunRegion & run);


  private:
  double centreOfMassX, centreOfMassY;
  RawPixel colour;
};


typedef std::vector<RunRegion> SimpleRegionList;

class RegionList : public SimpleRegionList
{
 public:
  RegionList();
  ~RegionList();

  void createRegions(FrameBuffer * frame, unsigned int row, unsigned int col, 
		     unsigned int height, unsigned int width);
  void createRegions(FrameBuffer * frame);
  int getParentRegionIdx(unsigned int row, unsigned int col);
  std::vector<Run> getUnusedRuns();
  std::vector<Run> getRegionRunsOnRow(unsigned int row, unsigned int parentIdx);
  
 private:
  void runLengthEncode(FrameBuffer * frame,unsigned int startRow, unsigned int startCol, 
		     unsigned int height, unsigned int width);
  void runRegionMerge();
  void newRunRegion(unsigned int runIdx);


  RunList runList;
  // parent is used for double indirect parenting to associate runs to parent regions

  // uses overlaps() as a comparison.
  template<class T> int binarySearchRunList(T rhs);


  std::vector<int> parent; 
};
#endif

