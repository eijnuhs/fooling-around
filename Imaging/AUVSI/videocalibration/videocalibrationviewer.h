
#ifndef _VIDEO_CALIBRATION_VIEWER_H_
#define _VIDEO_CALIBRATION_VIEWER_H_

#include <qwidget.h>
#include <qevent.h>
#include "framebufferviewer.h"
#include <qimage.h>
#include <time.h>
#include "videointerpreter2d.h"
#include <mainwindow.h>
#include <qstring.h>

///// Forward Declarations /////
class VideoCalibration;

class VideoCalibrationViewer : public FrameBufferViewer
{
  
  Q_OBJECT

 public: 
  VideoCalibrationViewer( QWidget * parent = 0, char const * name = 0 );
  ~VideoCalibrationViewer();
  void paintChild(QPainter & p);
  void setVideoInterpreter2D( VideoInterpreter2D * vi2d );
  void mousePressEvent(QMouseEvent * e);
  void mouseMoveEvent(QMouseEvent * e);
  void mouseReleaseEvent(QMouseEvent * e);
  void setVideoCalibration(VideoCalibration * init_vCal );
  void resizeEvent(QResizeEvent * e);
public slots:
  void setIntensityMin(int iMin);
  void setIntensityMax(int iMax);
  void setPixelMin(int pMin);
  void setPixelMax(int pMax);
  void setState(const QString & state);
  void checkEnabled(const QString & tab);
  void setFeatureX(const QString & fx);
  void setFeatureY(const QString & fy);
  void setControlPoints();
  void loadCalibration();
  void saveCalibration();
  void setFillOn(bool setOn);
  void resetControlPoints( void );
  virtual void newFrame( FrameBuffer * );
  void updateQImageFromFrameBuffer( FrameBuffer * frame );
 private:
  void updateControlPoint(int xm, int ym);
  void updateIntensity(QMouseEvent * e);

  VideoCalibration * vCal;
  ControlPoint * grabbed;
};

#endif


