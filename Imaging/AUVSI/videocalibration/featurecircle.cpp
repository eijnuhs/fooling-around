#include "featurecircle.h"
#include <math.h>
#include <iostream>

#define TWO_PI M_PI * 2.0

// FeatureCircle::

FeatureCircle::FeatureCircle(double init_radius, double init_radPerPix) :
  radius(init_radius),
  radPerPix(init_radPerPix),
  offsetArray(0)
{
  double angle;
  size = (unsigned int)rint(TWO_PI / radPerPix);
  angle = 0;
  if(size > 0)
    {
      //      std::cout << "init FeatureCircle: rad: " << init_radius << "\n";
      offsetArray = new offset[size];
      // initialize the array with the offsets.
      // x=r cos theta, y=r sin theta
      for(unsigned int i=0; i < size; i++)
	{
	  offsetArray[i].column = radius * cos(angle);
	  offsetArray[i].row = radius * sin(angle);
	  offsetArray[i].theta = angle;
	  angle += radPerPix;
	  //	  std::cout << "[" << i << "]: (" << offsetArray[i].row << "," << offsetArray[i].column 
	  //		    << "), " << offsetArray[i].theta << "\n";
	}
    }
}
unsigned int FeatureCircle::getSize() const
{
  return size;
}
struct offset FeatureCircle::operator[](unsigned int idx) const
{
  if(offsetArray)
    return offsetArray[idx];
  else
    return struct offset();
}

// private:
// double radius;
// double radPerPix;
// unsigned int size;
// struct offset * offsetArray;
