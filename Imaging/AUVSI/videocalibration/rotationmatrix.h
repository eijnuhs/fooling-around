#ifndef _ROTATIONMATRIX_H_
#define _ROTATIONMATRIX_H_

#include "matrix/matrix.h"
#include "controlpoints.h"
#include "playingfield.h"
#include <string>
///// Forward Declarations /////
class Calibration;


class RotationMatrix
{
public:
  RotationMatrix ( std::string filename );
  ~RotationMatrix ();
  void rotate (double x, double y, double *ox, double *oy);
  void rotatei (double x, double y, double *ox, double *oy);
  void resetToScreen (class FrameBuffer *frame);

  ControlPoint *controlPoint (int x, int y);
  ControlPoint *controlPoint (int index);

  void computeRotationMatrix (Calibration * calib);
  void setControlPoints (ControlPoints & points);
  void printMatrix( void );
  void loadMatrix(std::string filename);
  void saveMatrix(std::string filename);
  void setToIdentity();

  /*! Convert a rotation matrix to a string */
  operator  const char *();

  ControlPoints points;
  std::string filename;

protected:
  dmat rotM;
  dmat rotMi;

  void updateInverse ();
};

#endif

