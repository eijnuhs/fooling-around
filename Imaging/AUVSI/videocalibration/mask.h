// $Id: mask.h,v 1.1.1.1 2004/09/16 23:12:05 cvs Exp $
// Jacky Baltes <jacky@cs.umanitoba.ca> Fri Jun  4 11:35:38 CDT 2004
//

#ifndef _MASK_H_
#define _MASK_H_

class VideoInterpreter2D;

class Mask
{
 public:
  Mask( VideoInterpreter2D * vi2d );
  unsigned int getMin( unsigned int row ) const;
  unsigned int getMax( unsigned int row ) const;

 protected:

 private:
  unsigned int * xMin;
  unsigned int * xMax;
};


#endif
