#ifndef _DEBUG_VIEWER_H
#define _DEBUG_VIEWER_H

#include "framebufferviewer.h"
#include "controlpoints.h"
#include "configuration.h"
#include "robot.h"

class VideoInterpreter2D;
class FrameBuffer;
class HistogramViewer;



class DebugViewerConfig : public Configuration {
 public:
  DebugViewerConfig() : Configuration("DebugViewer"){}
  ~DebugViewerConfig() {};
};


class DebugViewer : public FrameBufferViewer
{
  Q_OBJECT
 public:
  DebugViewer( QWidget * parent = 0, char const * name = 0 );
  ~DebugViewer();

  void initialize(DebugViewerConfig * dbc);
  
  void mousePressEvent(QMouseEvent * e);
  void paintChild(QPainter & p);
  void setVideoInterpreter(VideoInterpreter2D * v);
  void setHistogramViewer(HistogramViewer * hv);
  void updateQImageFromFrameBuffer( FrameBuffer * frame );
  void mouseDoubleClickEvent ( QMouseEvent * e ) ;
 public slots:
  void newFrame(FrameBuffer * frame);

 signals:
  void newHistogram(const Histogram & hist);

 private:
 void updateControlPoint();

  ControlPoint point;
  Robot * bot;
  VideoInterpreter2D * vi2d;
  bool initialized;
  double theta;
  int subtype;
  HistogramViewer * histview;
  int state;
};

#endif
