// $Id: videostream.h,v 1.1.1.1.2.7 2004/12/28 19:30:05 cvs Exp $
// Jacky Baltes <jacky@cs.umanitoba.ca>

#ifndef _FILEVIDEOSTREAM_H_
#define _FILEVIDEOSTREAM_H_

#include <qthread.h>
#include "videodevice.h"
#include <string>
#include <qobject.h>
#include "framebufferrgb32.h"
#include <time.h> //#include <linux/time.h>
#include "boost/filesystem/operations.hpp" // includes boost/filesystem/path.hpp
#include "boost/filesystem/fstream.hpp"    // ditto
#include <vector>

// Forward declarations
class Segmentation;
class VideoInterpreter2D;
class VideoCalibration;
class FilevideostreamConfig;
class Tracker;

class FileVideoStream : public QObject, public QThread
{
  Q_OBJECT

 public:
  FileVideoStream(std::string path);

  void run( void );
  //  void setSegmentation( Segmentation * segmentation );
  Segmentation * getSegmentation( void );
  VideoDevice * getVideoDevice() const;
  VideoCalibration * getVideoCalibration( void );
  void setVideoCalibration(VideoCalibration * vCalibration );
  void setVideoInterpreter2D( VideoInterpreter2D * vi2d );
  void setTracker(Tracker * trak);
  void updateFPS( void );

  bool done;
  bool isInterlaced();
  static double framesPerSecond;

  unsigned int width();
  unsigned int height();

 signals:
  void newFrame( FrameBuffer * frame );
  void fpsUpdate(double fps);

 private: 
  void sendFPSupdate();
  FrameBufferRGB32 ipFrame;
  //  Segmentation * segmentation;
  VideoInterpreter2D * videoInterpreter2D;
  VideoCalibration * vCalibration;

  Tracker * tracker;
  struct timeval prev;
  std::string path;
  std::vector<boost::filesystem::path> image_files;

  unsigned int m_width, m_height;

};

#endif
