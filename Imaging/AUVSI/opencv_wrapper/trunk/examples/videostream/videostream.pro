TEMPLATE	= app
LANGUAGE	= C++

AUTOGUI_LIB_PATH = ../../../autogui
CONFIG_LIB_PATH  = ../../../configuration
UTILS_INCLUDE_PATH = ../../../utils
CVWRAP_LIB_PATH = ../../

# include the optional videostream component
VIDEOSTREAM = 1
UMIMAGEGUI = 1

!exists( .firstqmake ){
system(cat ../../README)
system(touch .firstqmake)
}

include( $${CVWRAP_LIB_PATH}/cvwrapinclude.pro  )
!exists( $${CVWRAP_LIB_PATH}/cvwrapinclude.pro ){
  error( OpenCv Wrapper Library not found...It may be checked out from the sculpin cvs.)
}

include( $${AUTOGUI_LIB_PATH}/autoguiinclude.pro)
!exists($${AUTOGUI_LIB_PATH}/autoguiinclude.pro){
  error( AutoGui library not found...It may be checked out from the sculpin cvs.)
}

SOURCES	+= main.cpp 

QMAKE_CXXFLAGS = -ggdb -pthread

unix {
  UI_DIR = .ui
  MOC_DIR = .moc
  OBJECTS_DIR = .obj
}

HEADERS += sampleprocessing.hpp 

