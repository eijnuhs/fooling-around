// $Id: square.h,v 1.1.1.1 2004/09/16 23:12:05 cvs Exp $
//

#ifndef _SQUARE_H_
#define _SQUARE_H_

class Square
{
public:
  Square::Square ();
  operator  const char *();

  static const int PIXMAP_SIZE = 64;

  int min_x, max_x, min_y, max_y, num_pixels;
  double median_x, median_y;
  double avg_x, avg_y;
  double minmax_x, minmax_y;

  /*! Coordinates in pixel coordinates */
  double x, y;

  /*! Black and white pixmap of the square */
  unsigned char pixmap[PIXMAP_SIZE][PIXMAP_SIZE];	/* Bitmap for the square */

  /*! Coordinates in camera coordinates */
  double xi, yi;

  /*! Coordinates of corresponding real world point */
  double xr, yr;

  /*! Block coordinates */
  int block_x, block_y;
  double u;
};

#endif
