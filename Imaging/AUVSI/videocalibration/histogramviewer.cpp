#include "histogramviewer.h"
#include <qpainter.h>
#include <iostream>
#include <qcolor.h>

HistogramViewer::HistogramViewer( QWidget * parent, const char * name, WFlags f ) :
  QWidget(parent, name, f),
  max(0)
{
  QWidget::setPaletteBackgroundColor(QColor(200,200,100));
  
}
HistogramViewer::~HistogramViewer()
{
   // 0
}

void HistogramViewer::newHistogram(const Histogram & hist)
{
  //  std::cout << "Got Histogram\n";
  this->hist = hist;
  update();
}


void HistogramViewer::paintEvent ( QPaintEvent * )
{
  // std::cout << "painitng histogram ";
  unsigned int wHeight, wWidth, barWidth, barHeight, x,y, scale;
  wHeight = height();
  wWidth = width();
  
  if(max == 0)
    scale = (hist.getMax() > 0) ? hist.getMax() : 1;
  else
    scale = max;

  QPainter p(this);
  
  // draw bounding box
  p.setPen(Qt::black);
  p.drawRect(0,0,wWidth,wHeight);

  barWidth = (wWidth / hist.size());
  if(barWidth < 1)
    barWidth = 1;

  barHeight = wHeight;
  if(barHeight < 1)
    barHeight = 1;
  
  for(unsigned int i = 0; i < hist.size(); i++)
    {
      p.setPen(QColor(i,i,i));
      p.setBrush(QColor(i,i,i));
      // draw the associated bar for each 
      x = i * barWidth;
      y = (barHeight * ((hist.get(i) <= scale) ? hist.get(i) : scale)) / scale ;
      
      p.drawRect(x,barHeight - y,barWidth + 2,y);
      //      std::cout << "(" << x << "," << y << "), ";
    }

  // paint the thresholds:
  p.setBrush(Qt::red);
  p.setPen(Qt::red);
  for(uint i = 0; i < thresholds.size(); ++i)
    {
      //      std::cout << "painting i: " << i << " = " << thresholds[i] << "\n";
      // draw the associated bar for each 
      x = thresholds[i] * barWidth;
      y = (barHeight * ((hist.get(thresholds[i]) <= scale) ? hist.get(thresholds[i]) : scale)) / scale ;
      //      std::cout << x << "," << y << "\n";
      p.drawRect(x,barHeight,barWidth + 2,barHeight - y);
    }

  //  std::cout << "\n";
}


void HistogramViewer::setMax(int i)
{
  if(i >= 0)
    max = i;
}
