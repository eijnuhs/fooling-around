// $Id: objectviewer.h,v 1.1.1.1.2.1 2004/12/28 19:30:05 cvs Exp $
// Jacky Baltes jacky@cs.umanitoba.ca Tue May 25 17:58:28 CDT 2004
//

#ifndef _OBJECT_VIEWER_H_
#define _OBJECT_VIEWER_H_

#include "framebufferviewer.h"

class ObjectViewer : public FrameBufferViewer
{
  Q_OBJECT

 public:
  ObjectViewer( QWidget * parent = 0, char const * name = 0 );
  ~ObjectViewer( );
  void updateQImageFromFrameBuffer( FrameBuffer * frame );
  void printName();
 private:
};

#endif

