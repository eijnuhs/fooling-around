/*! $Id: playingfield.cpp,v 1.1.1.1.2.1 2005/01/23 05:24:37 cvs Exp $
 * Various information about the playing field
 */

using namespace std;

#include "playingfield.h"

PlayingField::PlayingField ( double w, double h, double m )
{
  width = w;
  height = h;
  margin = m;
}

bool
PlayingField::isInside( double x, double y )
{
  bool result = false;
  if ( ( x >= - margin ) && ( x <= width + margin ) )
    {
      if ( ( y >= - margin ) && ( y <= height + margin ) )
	{
	  result = true;
	}
    }
  return result;
}
      
double PlayingField::getFullHeight()
{
  return (2.0 * margin) + height;
}
double PlayingField::getFullWidth()
{
  return (2.0 * margin) + width;
}



