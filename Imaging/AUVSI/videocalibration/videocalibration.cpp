#include "videocalibration.h"
#include <iostream>
#include "videointerpreter2d.h"
#include "visitedmap.h"
#include "imageprocessing.h"
#include "framebuffer.h"

#define STATE_SETUP "Setup"
#define STATE_CALIBRATION "Calibrate"

VideoCalibration::VideoCalibration(VideoInterpreter2D * init_vi2d) :
  preprocessEnabled(false),
  minPixels(0),
  maxPixels(0),
  minIntensity(0),
  maxIntensity(0),
  featureX(0),
  featureY(0),
  fillOn(true),
  initCalibration(false),
  points(0),
  mousex(-1),
  mousey(-1)
{
  vi2d = init_vi2d;
  setControlPoints(vi2d->getFrameWidth(), vi2d->getFrameHeight());

}

VideoCalibration::~VideoCalibration()
{
  //nothing!
}

void VideoCalibration::recalibrate(FrameBuffer * frame)
{
  struct GET_entry edges[4];

  // build the edges for the scanline finding algorithm.
  if (points != 0)
    {
      double x1, y1;
      double x2, y2;

      x1 = points->getPoint (0).xm;
      y1 = points->getPoint (0).ym;
      x2 = points->getPoint (1).xm;
      y2 = points->getPoint (1).ym;

      edges[0].xmin = x1;
      edges[0].ymin = y1;
      edges[0].xmax = x2;
      edges[0].ymax = y2;

      x1 = points->getPoint (1).xm;
      y1 = points->getPoint (1).ym;
      x2 = points->getPoint (2).xm;
      y2 = points->getPoint (2).ym;

      edges[1].xmin = x1;
      edges[1].ymin = y1;
      edges[1].xmax = x2;
      edges[1].ymax = y2;

      x1 = points->getPoint (2).xm;
      y1 = points->getPoint (2).ym;
      x2 = points->getPoint (3).xm;
      y2 = points->getPoint (3).ym;

      edges[2].xmin = x1;
      edges[2].ymin = y1;
      edges[2].xmax = x2;
      edges[2].ymax = y2;

      x1 = points->getPoint (3).xm;
      y1 = points->getPoint (3).ym;
      x2 = points->getPoint (0).xm;
      y2 = points->getPoint (0).ym;

      edges[3].xmin = x1;
      edges[3].ymin = y1;
      edges[3].xmax = x2;
      edges[3].ymax = y2;
    }

  // calculate the scanlines
  list < struct sPoint >scanLines;  
  calcScanLinesPolygon (edges, sizeof (edges) / sizeof (edges[0]), scanLines);

  // floodfill along the scanlines to find squares.
  findSquares (frame, scanLines);

  
  // find the corner squares.
  const Square *p[4];
  for (int i = 0; i < 4; i++)
    {
      p[i] = 0;
      for (list < Square >::const_iterator it = squares.begin ();
	   it != squares.end (); ++it)
	{
	  double psx, psy;
	  psx = points->getPoint(i).xm;
	  psy = points->getPoint(i).ym * (frame->interlaced ? 1 : 2);
	  Square square = *it;
	  if ((p[i] == 0) ||
	      (hypot(p[i]->x - psx, p[i]->y - psy) >
	       hypot (square.x - psx, square.y - psy)))
	    {
	      p[i] = &(*it);
	    }
	}
    }

  if(state == STATE_CALIBRATION && squares.size () > 5)
    { 
      // assign block coordinates along the edge lines
      assignBlockCoordinates (squares, vi2d->calibration.camera, 
			      frame->interlaced);

      // compute the calibration based on the points
      TsaiPlanarCalibration tmpCalib (vi2d->calibration.camera);
      tmpCalib.newCalibrationPoints (squares);
      // compute remaining block coordinates based on the
      // temporary calibration object.
      computeBlockCoordinates (squares, tmpCalib);
      // now the calibration object will accept all points.
      tmpCalib.newCalibrationPoints (squares);
      // check if error is worse than current error.
      if( (initCalibration && tmpCalib.getMaxError() < 11 ) || 
	  (tmpCalib.getMaxError() < vi2d->calibration.getMaxError()) )
	{
	  vi2d->calibration.newCalibrationPoints (squares);
	  initCalibration = false;
	  vi2d->updateScreenToWorldMap();
	  vi2d->updateWorldToScreenMap();
	}
    }

}


// calculates scanlines based on a polygon defined by edgePoints[]
void
VideoCalibration::calcScanLinesPolygon (struct GET_entry edgePoints[], int  n, list < struct sPoint >&retList)
{
  list < struct GET_entry >GET;
  list < struct GET_entry >AET;

  int yinf, y;

  yinf = 9999;

#ifdef XXDEBUG
  cout << "VideoCalibration::calcScanLinesPolygon entry\n";

  int count = 0;
  for (int i = 0; i < n; i++)
    {
      cout << "Edge[" << count++ << "]=(" << edgePoints[i].
	xmin << "," << edgePoints[i].ymin << ")->(" << edgePoints[i].
	xmax << "," << edgePoints[i].ymax << ")\n";
    }  
#endif

  for (int i = 0; i < n; i++)
    {
      struct GET_entry get_entry;
      double x1, x2, y1, y2;

      x1 = edgePoints[i].xmin;
      y1 = edgePoints[i].ymin;
      x2 = edgePoints[i].xmax;
      y2 = edgePoints[i].ymax;

      if (y1 < y2)
	{
	  get_entry.ymin = y1;
	  get_entry.xmin = x1;
	  get_entry.ymax = y2;
	  get_entry.xmax = x2;
	}
      else
	{
	  get_entry.ymin = y2;
	  get_entry.xmin = x2;
	  get_entry.ymax = y1;
	  get_entry.xmax = x1;
	}
      insertGET (get_entry, &GET);
      int ymin = (int) rint (get_entry.ymin);
      if (ymin < yinf)
	{
	  yinf = ymin;
	}
    }
#ifdef XXXXDEBUG
  {
    int count = 0;
    cout << "VideoCalibration::calcScanLinesPolygon\n";
    for (list < struct GET_entry >::iterator it = GET.begin ();
	 it != GET.end (); ++it)
      {
	cout << "Edge[" << count++ << "]=(" << it->xmin << "," << it->
	  ymin << ")->(" << it->xmax << "," << it->ymax << ")\n";
      }
  }
#endif

  if (yinf < 0)
    {
      yinf = 0;
    }

  y = yinf;
  do
    {
#ifdef XXXXDEBUG
      {
	int count = 0;
	cout << "VideoCalibration::calcScanLinesPolygon loop\n";
	for (list < struct GET_entry >::iterator it = AET.begin ();
	     it != AET.end (); ++it)
	  {
	    cout << "Edge[" << count++ << "]=(" << it->xmin << "," << it->
	      ymin << ")->(" << it->xmax << "," << it->ymax << ")\n";
	  }
      }
#endif
      addEdgesToAET (GET, y, &AET);
#ifdef XXXXDEBUG
      {
	int count = 0;
	cout <<
	  "VideoCalibration::calcScanLinesPolygon after adding edges\n";
	for (list < struct GET_entry >::iterator it = AET.begin ();
	     it != AET.end (); ++it)
	  {
	    cout << "Edge[" << count++ << "]=(" << it->xmin << "," << it->
	      ymin << ")->(" << it->xmax << "," << it->ymax << ")\n";
	  }
      }
#endif
      removeEdgesFromAET (y, &AET);
#ifdef XXXXDEBUG
      {
	int count = 0;
	cout <<
	  "VideoCalibration::calcScanLinesPolygon after adding and removing edges\n";
	for (list < struct GET_entry >::iterator it = AET.begin ();
	     it != AET.end (); ++it)
	  {
	    cout << "Edge[" << count++ << "]=(" << it->xmin << "," << it->
	      ymin << ")->(" << it->xmax << "," << it->ymax << ")\n";
	  }
      }
#endif
      AET.sort (comp);
#ifdef XXXXDEBUG
      {
	int count = 0;
	cout <<
	  "VideoCalibration::calcScanLinesPolygon after adding/removing/sorting edges, scanline = "
	  << y << "\n";
	for (list < struct GET_entry >::iterator it = AET.begin ();
	     it != AET.end (); ++it)
	  {
	    cout << "Edge[" << count++ << "]=(" << it->xmin << "," << it->
	      ymin << ")->(" << it->xmax << "," << it->
	      ymax << ", islope = " << it->islope << ")\n";
	  }
      }
#endif

      int even = 1;
      int lastX = -123;
      for (list < struct GET_entry >::iterator it = AET.begin ();
	   it != AET.end (); ++it)
	{
	  int x;

	  x = (int) rint (it->xmin);
	  if (even == 0)
	    {
	      struct sPoint p;

	      p.xMin = lastX;
	      p.xMax = x;
	      p.y = y;
	      retList.push_back (p);
	    }
	  lastX = x;
	  even = 1 - even;
	}
      y = y + 1;
      updateEdgesInAET (AET);
    }
  while ((!AET.empty ()) && (y < 600));
  return;
}



// uses a flood fill to find calibration squares along the list of scanlines.
void VideoCalibration::findSquares(FrameBuffer * frame, 
				   list <struct sPoint>  scanlines)
{
  bool clobberPixels = (state == STATE_SETUP); 
  FrameBufferIterator fbit(frame);
  VisitedMap vMap;
  Pixel pixel;
  int pixelCount = 0;
  int pixelTotal = 0;
  int intensity;
  int xc[maxPixels];
  int yc[maxPixels];
  int numPixels;
  int sumX;
  int sumY;
  bool found;

  // initialize the visited map
  if(fillOn)  
    {
      // allow fill outside the edges
      vMap.initialize(frame);
    }
  else
    {
      // fill only within the edges
      vMap.initialize(frame, scanlines);
    }

  // clear the old squares
  squares.clear();

  // floodfill along scanlines to find squares.
  for (list <struct sPoint>::const_iterator it = scanlines.begin (); it != scanlines.end (); ++it)
    {
      struct sPoint p(*it);
      fbit.goPosition((unsigned int)p.y, (unsigned int)p.xMin);
      for (int x = p.xMin; x <= p.xMax; x++, fbit.goRight())
	{
	  pixelTotal++;
	  Square square;
	  fbit.getPixel(&pixel);
	  intensity = (int) pixel.intensity();
	  if ( !vMap.isVisited(x, p.y) && colour.isMatch(pixel) )
	    {
	      pixelCount++;
	      numPixels = ImageProcessing::doFloodFill (fbit,vMap, xc, yc, &colour, maxPixels, 0, clobberPixels);

	      if ((numPixels > minPixels) && (numPixels < maxPixels))
		{
		  sumX =  0;
		  sumY =  0;
		  for (int i = 0; i < numPixels; i++)
		    {
		      sumX = sumX + xc[i];
		      sumY = sumY + (yc[i] * ((frame->interlaced) ? 1 : 2));
		    }
		  square.avg_x = (double) sumX / (double) numPixels;
		  square.avg_y = (double) sumY / (double) numPixels;
		  square.x = square.avg_x;
		  square.y = square.avg_y;
		  Square fs;
		  found = false;
		  for (list < struct Square >::iterator s = squares.begin ();
		       s != squares.end (); ++s)
		    {
		      if (hypot
			  (s->avg_x - square.avg_x,
			   s->avg_y - square.avg_y) <= 3.0)
			{
			  found = true;
			  fs = *s;
			  break;
			}
		    }
		  if (!found)
		    {
		      squares.push_back (square);
		    }
		  else
		    {
		      fs.avg_x = (fs.avg_x + square.avg_x) / 2.0;
		      fs.avg_y = (fs.avg_y + square.avg_y) / 2.0;
		      fs.x = (fs.x + square.x) / 2.0;
		      fs.y = (fs.y + square.y) / 2.0;
		    }
		} // end if the square is the right size
	    } // end if the pixel is the right intensity and has not been visited.
	} // end for xMin to xMax
    } // end while there are remaining scan lines.

}



void
VideoCalibration::updateEdgesInAET (list < struct GET_entry >&AET)
{
  for (list < struct GET_entry >::iterator iter = AET.begin (); iter != AET.end (); ++iter)
    {
      iter->xmin = iter->xmin + iter->islope;
    }
}


void
VideoCalibration::insertGET (struct GET_entry &new_edge, list < struct GET_entry >*GET)
{
  int ymin;
  bool inserted = false;

  ymin = (int) rint (new_edge.ymin);
  if (new_edge.ymax - new_edge.ymin != 0)
    {
      double inv_slope =
	(new_edge.xmax - new_edge.xmin) / (new_edge.ymax - new_edge.ymin);
      new_edge.islope = inv_slope;
      for (list < struct GET_entry >::iterator iter = GET->begin ();
	   iter != GET->end (); ++iter)
	{
	  if ((iter->ymin > new_edge.ymin) ||
	      (((int) rint (iter->ymin) == (int) rint (new_edge.ymin))
	       && (iter->xmin > new_edge.xmin)))
	    {
	      GET->insert (iter, new_edge);
	      inserted = true;
	      break;
	    }
	}
      if (!inserted)
	{
	  GET->push_back (new_edge);
	}
    }
}

void
VideoCalibration::addEdgesToAET (list < struct GET_entry >&GET, int scanline, list < struct GET_entry >*AET)
{
  for (list < struct GET_entry >::iterator iter = GET.begin ();
       iter != GET.end (); ++iter)
    {
      if (((int) rint (iter->ymin) == scanline) && (iter->ymax > scanline))
	{
	  AET->push_back (*iter);
	}
    }
}

void
VideoCalibration::removeEdgesFromAET (int scanline, list < struct GET_entry >*AET)
{
  AET->remove_if (AET_EdgeDone (scanline));
}



void VideoCalibration::setIntensityMin(int iMin)
{
#ifdef XXDEBUG
  std::cout << "VideoCalibration::setIntensityMin: " << iMin <<  std::endl;
#endif
  minIntensity = iMin;
}
void VideoCalibration::setIntensityMax(int iMax)
{
#ifdef XXDEBUG
  std::cout << "VideoCalibration::setIntensityMax: " << iMax <<  std::endl;
#endif
  maxIntensity = iMax;
}
void VideoCalibration::setPixelMin(int pMin)
{
#ifdef XXDEBUG
  std::cout << "VideoCalibration::setPixelMin: " << pMin <<  std::endl;
#endif
  minPixels = pMin;
}
void VideoCalibration::setPixelMax(int pMax)
{
#ifdef XXDEBUG
  std::cout << "VideoCalibration::setPixelMax: " << pMax <<  std::endl;
#endif
  mutex.lock();
  maxPixels = pMax;
  mutex.unlock();
}
void VideoCalibration::setState(std::string state)
{
#ifdef XXDEBUG
  std::cout << "VideoCalibration::setState: " << state <<  std::endl;
#endif
      this->state = state;
      if(state == STATE_CALIBRATION)
	initCalibration = true;
}
void VideoCalibration::enableProcessing(bool enable)
{
  preprocessEnabled = enable;
#ifdef XXDEBUG
  std::cout << "VideoCalibration::enableProcessing called. enable: " << enable << std::endl ;
  std::cout << "preprocessEnabled: " << preprocessEnabled << std::endl ;
#endif
  
}

bool VideoCalibration::preprocessFrame(FrameBuffer * frame)
{
  bool retval = false;
  if(preprocessEnabled && frame)
    {
      QMutexLocker qml(frame->mutex);
      QMutexLocker qml2(&mutex);
      updateColour(frame);

      points->clamp(frame->width, frame->height);
      recalibrate(frame);
      retval = true;
    }
  return retval;
}

void VideoCalibration::setControlPoints(int height, int width)
{
  mutex.lock();
  if (points)
    {
      delete points;
    }
  points = new ControlPointsCarpetCalib(width, height);
  mutex.unlock();
}



ControlPoints & VideoCalibration::getPoints()
{
  return *points;
}

list <Square> & VideoCalibration::getSquares()
{
  return squares;
}



ControlPoint *  VideoCalibration::controlPoint (int sx, int sy)
{
  if (points)
    return points->selectPoint (sx, sy);
  else
    return 0;
}

void
VideoCalibration::assignBlockCoordinates (list < Square > & squares, Camera camera, bool interlaced)
{
  const Square *p[4];

  // Compute the four squares that are closest to the control points.
  for (int i = 0; i < 4; i++)
    {
      double psx, psy;
      psx = points->getPoint(i).xm;
      psy = points->getPoint(i).ym * (interlaced ? 1 : 2);
      p[i] = 0;
      for (list < Square >::const_iterator it = squares.begin (); it != squares.end (); ++it)
	{
	  Square square = *it;
	  if ((p[i] == 0) ||
	      (hypot( p[i]->x - psx, p[i]->y - psy) > hypot (square.x - psx, square.y - psy)))
	    {
	      p[i] = &(*it);
	    }
	}
    }

  assignLine (squares, p[0]->x, p[0]->y, p[1]->x, p[1]->y, true, 0, 0);
  assignLine (squares, p[0]->x, p[0]->y, p[3]->x, p[3]->y, false, 0, 0);

  int max_block_x = -1, max_block_y = -1;
  for (list < Square >::const_iterator it = squares.begin (); it != squares.end (); ++it)
    {
      Square square = *it;
      if ((square.block_x != -1) && (square.block_y != -1))
	{
	  if (square.block_x > max_block_x)
	    {
	      max_block_x = square.block_x;
	    }
	  if (square.block_y > max_block_y)
	    {
	      max_block_y = square.block_y;
	    }
	}
    }
  if (max_block_x > 0)
    {
      assignLine (squares, p[3]->x, p[3]->y, p[2]->x, p[2]->y, true, 0, max_block_y);
    }
  if (max_block_y > 0)
    {
      assignLine (squares, p[1]->x, p[1]->y, p[2]->x, p[2]->y, false, max_block_x, 0);
    }

  int count = 0;
  for (list < struct Square >::iterator it = squares.begin (); it != squares.end (); ++it)
    {
      Square *square = &(*it);

      camera.imageCoordToSensorCoord (square->x, square->y, &(square->xi), &(square->yi));
      if ((square->block_x != -1) && (square->block_y != -1))
	{
	  square->xr = 1000.0 + square->block_x * featureX;
	  square->yr = 1000.0 + square->block_y * featureY;

	  count++;
	}
    }

  if (count < 3)
    {
      cerr <<  "GuiState_Calibration::assignBlockCoordinates " 
	   << "insufficient squares assigned from the baseline\n";
      for (list < struct Square >::iterator it = squares.begin ();
	   it != squares.end (); ++it)
	{
	  Square *square = &(*it);
	  square->block_x = -1;
	  square->block_y = -1;
	}
    }
}


void
VideoCalibration::computeBlockCoordinates (list < Square > &squares, TsaiPlanarCalibration calib)
{

  for (list < struct Square >::iterator it = squares.begin (); it != squares.end (); ++it)
    {
      Square *square = &(*it);

      if ((square->block_x == -1) || (square->block_y == -1))
	{
	  calib.imageCoordToWorldCoord (square->x, square->y, 0.0, &(square->xr), &(square->yr));
	  square->block_x = (int) rint ((square->xr - 1000.0) / featureX);
	  square->block_y = (int) rint ((square->yr - 1000.0) / featureY);
	  //	  cout << "Assigned square: " << square << endl;
	}
    }

}


int
VideoCalibration::assignLine (list < Square > &squares, double x1, double y1, double x2, double y2, bool horizontal, int block_x, int block_y)
{

  Square *extractLine[512];

  /*! Set all the square pointers to 0 */
  memset (extractLine, 0, sizeof (extractLine));

  /*! Setup pointer to the end of insertion sort */
  int end = 0;

  for (list < Square >::iterator it = squares.begin (); it != squares.end (); ++it)
    {
      Square *square = &(*it);
      double distToLine;
      double x3 = square->x;
      double y3 = square->y;

      if ((x1 != x2) || (y1 != y2))
	{
	  double u =
	    ((x3 - x1) * (x2 - x1) +
	     (y3 - y1) * (y2 - y1)) / ((x2 - x1) * (x2 - x1) + (y2 -
								y1) * (y2 -
								       y1));
	  double x = x1 + u * (x2 - x1);
	  double y = y1 + u * (y2 - y1);

	  distToLine = sqrt ((x3 - x) * (x3 - x) + (y3 - y) * (y3 - y));
#ifdef XXXXDEBUG
	  cout << "GuiState_Calibration::assignLine\n";
	  cout << "Distance from point (" << x3 << "," << y3
	    << ") to the line (" << x1 << "," << y1 << ") to (" << x2 << ","
	    << y2 << ") is " << distToLine << " pixels\n";
#endif
	  if (distToLine < 10.0)
	    {
	      if (horizontal)
		{
		  square->block_x = -1;
		  square->block_y = block_y;
		}
	      else
		{
		  square->block_x = block_x;
		  square->block_y = -1;
		}
	      int curr = end;
	      for (curr = end; curr > 0; curr--)
		{
		  if (extractLine[curr - 1]->u < u)
		    {
		      break;
		    }
		  else
		    {
		      extractLine[curr] = extractLine[curr - 1];
		    }
		}
	      extractLine[curr] = square;
	      square->u = u;
	      end++;
	    }
	}
    }

  for (int i = 0; i < end; i++)
    {
      if (horizontal)
	{
	  extractLine[i]->block_x = i * 2;
	}
      else
	{
	  extractLine[i]->block_y = i * 2;
	}
    }

#ifdef XXXXDEBUG
  cout << "GuiState_Calibration::extractLine found " << end <<
    " squares on the line (" << x1 << "," << y1 << ") to (" << x2 << "," << y2
    << ")" << endl;
  for (list < struct Square >::const_iterator it = squares.begin (); it != squares.end (); ++it)
    {
      Square square = *it;
      cout << "GuiState_Calibration::assignLine Assigned square (" << square.
	x << "," << square.y << ") block coordinates (" << square.
	block_x << "," << square.block_y << ")\n";
    }
#endif
  return end;
}



void VideoCalibration::setFeatureX(double init_featureX)
{
#ifdef XXDEBUG
  cout << " VideoCalibration::setFeatureX: " << init_featureX << endl;
#endif
  featureX = init_featureX;
}

void VideoCalibration::setFeatureY(double init_featureY)
{
#ifdef XXDEBUG
  cout << " VideoCalibration::setFeatureY: " << init_featureY << endl;
#endif
  featureY = init_featureY;
}

void VideoCalibration::setExternalFill(bool init_fillOn)
{
#ifdef XXDEBUG
  cout << " VideoCalibration::setExternalfill: " << init_fillOn << endl;
#endif
  fillOn = init_fillOn;
}

void VideoCalibration::saveCalibration(std::string filename)
{
  vi2d->calibration.saveCalibration(filename);
}
void VideoCalibration::loadCalibration(std::string filename)
{
  vi2d->calibration.loadCalibrationPoints(filename);
}

std::string VideoCalibration::getCalibrationFilename( void )
{
  return vi2d->calibration.getPointsFile();
}

std::string VideoCalibration::getCalibrationErrorMessage( void )
{
  return vi2d->calibration.getErrors();
}


void VideoCalibration::addColour(int mx, int my)
{
  mousex = mx;
  mousey = my;
}

void VideoCalibration::updateColour(FrameBuffer * frame)
{

  if(mousex != -1 && mousey != -1)
    {
      Pixel pix;
      frame->getPixel(mousey,mousex,&pix);
      pix.update();
      colour.addPixel(pix);
      mousex = mousey = -1;
    }

}
