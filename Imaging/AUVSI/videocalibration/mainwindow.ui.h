/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you want to add, delete, or rename functions or slots, use
** Qt Designer to update this file, preserving your code.
**
** You should not define a constructor or destructor in this file.
** Instead, write your code in functions called init() and destroy().
** These will automatically be called by the form's constructor and
** destructor.
*****************************************************************************/

void Form1::init()
{
    QDoubleValidator * validator = new QDoubleValidator(0.01, 1000.0, 3, this);
    lneFeatureWidth->setValidator(validator);
    lneFeatureHeight->setValidator(validator);
    lneFeatureWidth->setText("176.00");
    lneFeatureHeight->setText("171.00");
}

void Form1::helpIndex()
{

}


void Form1::helpContents()
{

}


void Form1::helpAbout()
{

}
