#ifndef UMBOTICA_COUNT_STAT_HPP
#define UMBOTICA_COUNT_STAT_HPP

#include "runtime_stat_tracker.hpp"
#include <sstream>

namespace umbotica { namespace stats {


#ifdef UMBOTICA_NO_STATISTICS

  template<typename VALUE_TYPE, typename MUTEX_TYPE = umbotica::threads::null_mutex>
  class Sum : public umbotica::stats::RuntimeStat
  {
  public:
    typedef VALUE_TYPE value_type;

    template<typename STAT_TRACKER_T>
    Sum(std::string category, std::string name, STAT_TRACKER_T * statTrack) :
      RuntimeStat(category, name, statTrack)
    {
      // 0
    }
    
    Sum() : RuntimeStat()
    {
      // 0
    } 

    Sum & operator++()
    {
      return *this;
    }
    
    Sum & operator++(int)
    {
      return *this;
    }

    std::string results() const
    {
      return "No runtime_stats";
    }

    Sum & operator+=(double)
    {
      return *this;
    }
  };


#else

  template<typename VALUE_TYPE, typename MUTEX_TYPE = umbotica::threads::null_mutex>
  class Sum : public umbotica::stats::RuntimeStat
  {
  public:
    typedef VALUE_TYPE value_type;
    typedef MUTEX_TYPE mutex_t;
    typedef typename mutex_t::scoped_lock lock_t;

    template<typename STAT_TRACKER_T>
    Sum(std::string category, std::string name, STAT_TRACKER_T * statTrack) :
      RuntimeStat(category, name, statTrack), m_count(0)
    {
      // 0
    }
    
    Sum() : RuntimeStat(), m_count(0)
    {
      // 0
    } 

    std::string results() const
    {
      lock_t lock(m_mutex);
      std::stringstream stream;
      stream << m_count;
      return stream.str();
    }

    Sum & operator+=(value_type amount)
    {
      lock_t lock(m_mutex);
      m_count += amount;
      return *this;
    }

  private:
    value_type m_count;
    mutable mutex_t m_mutex;
  };

#endif // UMBOTICA_NO_STATISTICS

} } // namespace umbotica::stats


#endif
