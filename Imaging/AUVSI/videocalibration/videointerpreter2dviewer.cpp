// $Id: videointerpreter2dviewer.cpp,v 1.1.1.1.2.16 2005/02/06 00:44:02 cvs Exp $
// A QT widget that displays the scene geometry videointerpreter2D viewer
// Jacky Baltes <jacky@cs.umanitoba.ca> Tue Oct 21 21:46:10 CDT 2003

#include <qevent.h>
#include "videointerpreter2dviewer.h"
#include "framebufferviewer.h"
#undef min
#undef max
#include <sstream>
#include "framebuffer.h"
#include <qpainter.h>
#include <qcolor.h>
#include <iostream>
#include <time.h>
#include <inttypes.h>
#include <mainwindow.h>
#include <qcheckbox.h>
#include <qtabwidget.h>
#include <qcombobox.h>
#include <assert.h>
#include "pixel.h"
#include <qpixmap.h>
#include "mask.h"
#include "framebufferrgb32.h"
#include "framebufferworld.h"
#include "paintbox.h"
#include <qstring.h>
#include <qfiledialog.h>
#include <qstatusbar.h>
#include <qspinbox.h>
#include <qmutex.h>

using namespace std;

VideoInterpreter2DViewer::VideoInterpreter2DViewer( QWidget * parent, char const * name )
  : FrameBufferViewer( parent, name ),
    vi2d( 0 ),
    grabbed ( 0 )
{
  setWFlags( WRepaintNoErase );

  QWidget * mparent = gotoParent( this, 5 );
  mainWindow = dynamic_cast<Form1 *>(mparent);
  assert( mainWindow != 0 );
}

VideoInterpreter2DViewer::~VideoInterpreter2DViewer()
{
}

void VideoInterpreter2DViewer::newFrame( FrameBuffer * frame )
{
#ifdef XXDEBUG
  std::cout << "VideoInterpreter2DViewer::nextFrame received signal newFrame" << endl;
#endif
  FrameBufferViewer::newFrame( frame );
}

void 
VideoInterpreter2DViewer::paintChild( QPainter & p)
{
#ifdef XXDEBUG
  std::cout << "VideoInterpreter2DViewer::paintEvent called" << endl;
  std::cout << "Frame Size: (" << frame->width << "," << frame->height << ")\n";

#endif

  if ( vi2d == 0 )
    {
      std::cerr << "ERROR: VideoInterpreter2DViewer vi2d is not set. Unable to continue" << endl;
      exit( EXIT_FAILURE );
    }

  QString mode(mainWindow->comboBox1->currentText());

  if ( mode != "Overhead View" )
    {

      if ( mainWindow->chkShowField->isChecked()  )
	{
	  drawRotationMatrix( p );
	}
      if ( mainWindow->chkShowMask->isChecked()  )
	{
	  drawMask( p );
	}
      if ( mainWindow->chkShowCtlPts->isChecked()  )
	{
	  drawControlPoints( p );
	}
    }
}

void VideoInterpreter2DViewer::setVideoInterpreter2D( VideoInterpreter2D * vi2d )
{
  this->vi2d = vi2d;
}

QWidget * VideoInterpreter2DViewer::gotoParent( QWidget * w, int level )
{
  QWidget * result = 0;

  if ( w == 0 )
    {
      result = 0;
    }
  else
    {
      if ( level <= 0 )
	{
	  result = w;
	}
      else
	{
	  result = gotoParent( w->parentWidget(), level - 1 );
	}
    }
  return result;
}

void VideoInterpreter2DViewer::drawRotationMatrix( QPainter & p )
{
  showScreenPoint( p, 0.0, 0.0, "O" );
  showScreenPoint( p, vi2d->field.width, 0.0, "X" );
  showScreenPoint( p, 0.0, vi2d->field.height, "Y" );
  showScreenPoint( p, vi2d->field.width, vi2d->field.height, "R" );
  
  for ( double x = 100.0; x < vi2d->field.width; x = x + 100.0 )
    {
      for ( double y = 100.0; y < vi2d->field.height; y = y + 100.0 )
	{
	  int xx1, yy1;
	  vi2d->worldToScreenCoordinates( x, y, 0.0, & xx1, & yy1 );

	  int xx2, yy2;
	  vi2d->worldToScreenCoordinates( x - 100.0, y, 0.0, & xx2, & yy2 );
	      
	  p.setPen( Qt::green );
	  p.drawLine( xx2, yy2, xx1, yy1 );	  
	  
	  int xx3, yy3;
	  vi2d->worldToScreenCoordinates( x, y - 100.0, 0.0, & xx3, & yy3 );
	  
	  p.setPen( Qt::red );
	  p.drawLine( xx3, yy3, xx1, yy1 );	  
	}
    }
}


#if 0
void VideoInterpreter2DViewer::drawRotationMatrix( QPainter & p )
{
  showScreenPoint( p, 0.0, 0.0, "O" );
  showScreenPoint( p, vi2d->field.width, 0.0, "X" );
  showScreenPoint( p, 0.0, vi2d->field.height, "Y" );
  showScreenPoint( p, vi2d->field.width, vi2d->field.height, "R" );
  
  double xDim = vi2d->field.width / 20.0;
  double yDim = vi2d->field.height / 20.0;


  for ( double x = 100.0; x < vi2d->field.width; x = x + xDim )
    {
      for ( double y = 100.0; y < vi2d->field.height; y = y + yDim )
	{
	  int xx1, yy1;
	  vi2d->worldToScreenCoordinates( x, y, 0.0, & xx1, & yy1, !grabbed);
	  screenToWidgetCoordinates(xx1, yy1, &xx1, &yy1);
	  int xx2, yy2;
	  vi2d->worldToScreenCoordinates( x - xDim, y, 0.0, & xx2, & yy2, !grabbed );
 	  screenToWidgetCoordinates(xx2, yy2, &xx2, &yy2);

	  p.setPen( Qt::green );
	  p.drawLine( xx2, yy2, xx1, yy1 );	  
	  
	  int xx3, yy3;
	  vi2d->worldToScreenCoordinates( x, y - yDim, 0.0, & xx3, & yy3, !grabbed);
	  screenToWidgetCoordinates(xx3, yy3, &xx3, &yy3);
	  
	  p.setPen( Qt::red );
	  p.drawLine( xx3, yy3, xx1, yy1 );	  
	}
    }
  
}

#endif

void VideoInterpreter2DViewer::showScreenPoint( QPainter & p, double x, double y, std::string label )
{
  int xx1, yy1;
  vi2d->worldToScreenCoordinates( x, y, 0.0, & xx1, & yy1, !grabbed);
  screenToWidgetCoordinates(xx1,yy1, &xx1, &yy1);

  p.fillRect( xx1 - 5, yy1 - 5, 10, 10, QColor( 255, 20, 255 ) );
  QString s( label.c_str() );
  p.setPen( Qt::red );
  p.drawText( xx1, yy1, s );
}

void
VideoInterpreter2DViewer::drawMask( QPainter & p )
{
  int wx1,wy1,wx2,wy2;
  for( unsigned int i = 0; i < vi2d->getHeight(); i++ )
    {
      p.setPen( QColor( 128, 128, 128 ) );
      screenToWidgetCoordinates(0,(int)i,&wx1,&wy1);
      screenToWidgetCoordinates((int)vi2d->mask->getMin( i ),(int)i , &wx2, &wy2);
      p.drawLine(wx1,wy1,wx2,wy2 );
      p.setPen( QColor( 128, 128, 128 ) );
      screenToWidgetCoordinates((int)vi2d->getWidth() - 1, (int)i,&wx1,&wy1);
      screenToWidgetCoordinates((int)vi2d->mask->getMax( i ), (int)i, &wx2, &wy2);
      p.drawLine(wx1,wy1,wx2,wy2);
    }
}


void VideoInterpreter2DViewer::mousePressEvent(QMouseEvent * e)
{
  QString mode(mainWindow->comboBox1->currentText());
  if(mode != "Overhead View")
    {  
      int sx,sy;
      widgetToScreenCoordinates(e->x(), e->y(), &sx, &sy);
      grabbed = vi2d->rotationMatrix.controlPoint(sx, sy);
    }

#ifdef XXDEBUGF
  std::cout << "VideoInterpreter2dViewer::mousePressEvent\n";
  std::cout << "Screen: (" << e->x() << "," << e->y() << ")\n";
  if(grabbed)
    std::cout << "Grabbed: " << *grabbed << std::endl;
  else
    std::cout << " Nothing grabbed." << std::endl;
  std::cout << "Num of CPs: " <<  vi2d->rotationMatrix.points.size() << endl;
  
  
  for(int i = 0; i < vi2d->rotationMatrix.points.size(); ++i)
    {
      std::cout << "Point " << i << ": " << *(vi2d->rotationMatrix.controlPoint(i)) << endl;
    }
#endif

}


void VideoInterpreter2DViewer::mouseMoveEvent(QMouseEvent * e)
{
  updateControlPoint(e->x(), e->y());
#ifdef XXDEBUG
  int sx,sy;
  double  wx,wy;
  widgetToScreenCoordinates(e->x(),e->y(),&sx,&sy);
  vi2d->screenToWorldCoordinates((unsigned int)sx,(unsigned int)sy,0.0,&wx,&wy);
  QString s(QString("Widget(%1,%2), Screen(%3,%4), World(%5,%6)").arg(e->x()).arg(e->y()).arg(sx).arg(sy).arg(wx).arg(wy));
  mainWindow->statusBar()->message( s, 1000 );
#endif
#ifdef XXDEBUG
  std::cout << "VideoInterpreter2dViewer::mouseMoveEvent\n";
  std::cout << "(" << e->x() << "," << e->y() << ")\n";
#endif

    
}

void VideoInterpreter2DViewer::mouseReleaseEvent(QMouseEvent * e)
{
  updateControlPoint(e->x(), e->y());
  if(grabbed)
    {
#ifdef XXDEBUG
      std::cout << "VideoInterpreter2dViewer::mouseReleaseEvent\n";
      std::cout << "(" << e->x() << "," << e->y() << ")\n";
#endif
      vi2d->updateScreenToWorldMap();
      vi2d->updateWorldToScreenMap();
      grabbed = 0;
    }
    

}

void VideoInterpreter2DViewer::updateControlPoint(int xm, int ym)
{

  if(grabbed)
    {
      // adjust the screen co-ordinates of the point. 
      xm = Geometry::clamp(0, xm, QWidget::width() - 1);
      ym = Geometry::clamp(0, ym, QWidget::height() - 1);
      int sx,sy;
      widgetToScreenCoordinates(xm, ym, &sx, &sy);
      grabbed->xm = sx;
      grabbed->ym = sy;
      // grabbed->xm = xm;
      // grabbed->ym = ym;

#ifdef XXDEBUG
      std::cout << "Updating Control point: " << *grabbed << std::endl;
#endif      
      // recalculate the rotation matrix
      vi2d->rotationMatrix.computeRotationMatrix(&(vi2d->calibration));
    }

}

void VideoInterpreter2DViewer::drawControlPoints( QPainter & p )
{
  for(int i = 0; i < vi2d->rotationMatrix.points.size(); ++i)
    {
      ControlPoint point(vi2d->rotationMatrix.points.points[i]);
      int wx,wy;
      QString label = "(" + QString::number(point.xw) + ", " + QString::number(point.yw) + ")";
      screenToWidgetCoordinates(point.xm, point.ym, &wx, &wy);
      PaintBox::drawPoint(p, wx, wy, 7, label, 10,Qt::red, Qt::blue);
      // PaintBox::drawPoint(p, point.xm, point.ym, 7, label, 10,Qt::red, Qt::blue);
    }
}

void VideoInterpreter2DViewer::loadRotation()
{
  QString s = QFileDialog::getOpenFileName(vi2d->getRotationFilename(), "Configuration files (*.dat *.cfg)", mainWindow, "open file dialog", "Choose a file to open" );
  if(!s.isNull() && !s.isEmpty())
      vi2d->loadRotation(s.ascii());
}
void VideoInterpreter2DViewer::saveRotation()                                            
{
  QString s = QFileDialog::getSaveFileName( vi2d->getRotationFilename(), "Configuration files (*.dat *.cfg)", mainWindow, "save file dialog", "Choose a filename to save under" );
  if(!s.isNull() && !s.isEmpty())
    vi2d->saveRotation(s.ascii());
}

void VideoInterpreter2DViewer::resetControlPoints()
{
  vi2d->resetControlPoints();
}


void VideoInterpreter2DViewer::updateQImageFromFrameBuffer( FrameBuffer * frame )
{
  QString mode(mainWindow->comboBox1->currentText());
    if ( mode != "Overhead View" )
      {
	FrameBufferViewer::updateQImageFromFrameBuffer( frame );
      }
    else
      {
	double scaleFx = mainWindow->spnIntScale->value();
	FrameBufferWorld ipFrame;
	ipFrame.initialize( scaleFx, vi2d->field );
	//	mutex.lock();
	vi2d->interpolateFrame( frame, & ipFrame,mainWindow->chkPtf->isChecked() );
	//      mutex.unlock();
	FrameBufferViewer::updateQImageFromFrameBuffer( & ipFrame );
      }
}

void VideoInterpreter2DViewer::printName()
{
  std::cout << "VideoInterpreter2DViewer\n";
}
