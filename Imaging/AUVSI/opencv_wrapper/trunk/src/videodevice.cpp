// $Id: videodevice.cpp,v 1.1.1.1 2005/04/25 03:02:45 cvs Exp $
// Jacky Baltes <jacky@cs.umanitoba.ca>

#include "videodevice.h"

VideoDevice::VideoDevice( std::string driver, std::string devname, std::string input, std::string standard, int fps, int width, int height, ImageFormats::ImageFormat format, bool interlaced)
  :
  driver( driver ),
  devname( devname ),
  input( input ),
  standard( standard ),
  fps( fps ),
  width( width ),
  height( height ),
  error( true ),
  format(format),
  interlaced(interlaced)
{
}

VideoDevice::~VideoDevice()
{

}

bool VideoDevice::getError( void ) const
{
  return error;
}

int VideoDevice::getWidth( void ) const
{
  return width;
}

int VideoDevice::getHeight( void ) const
{
  return height;
}

