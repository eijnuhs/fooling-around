#include "tobject.h"
#include "framebuffer.h"
#include "framebufferworld.h"
#include "run.h"
#include "framebufferrgb32.h"
#include "videointerpreter2d.h"


// private:
// RegionList regions;

TObject::TObject(std::string name, double height, double radius, double mmPerPix, enum Trackable::TrackableTypes classtype) :
  Trackable(classtype,name,height,radius,mmPerPix)
{
  // 0 
}
TObject::~TObject()
{
  
}

bool TObject::isMe(RunRegion & region, FrameBuffer * inFrame, FrameBufferWorld * worldFrame, unsigned int avgIntensity, bool updatePos)
{
  double lrx,lry,wx,wy;
  bool success = false;
  FrameBuffer * scratch = Trackable::scratchpad( );
  FrameBufferIterator fbit(scratch);
  Pixel p;
  double rx, ry;
  double offset;
  double const pixelDim = ( getRadius() * 6 ) / scratch->width ;

  std::cout << "TObject::isMe checking region of size " << region.numPixels << ", min " << minPixels << ", max " << maxPixels << "\n";

  if(region.numPixels < maxPixels && region.numPixels > minPixels )
    {
      lrx = region.getCOMXD();
      lry = region.getCOMYD();
      worldFrame->bufferToWorldCoordinates(lrx,lry,&wx,&wy);
      offset = -(((double)(scratch->height) * pixelDim)/2.0);
      
      ry = wy + offset;
      for(unsigned int row = 0; row < scratch->height; ++row)
	{
	  rx = wx + offset;
	  fbit.goPosition(row,0);
	  for(unsigned int col = 0; col < scratch->width; ++col)
	    {
	      //	      std::cout << row << "," << col << "-->" << wy << "," << wx << "\n";
	      Trackable::vi2d->interpolatePixel( inFrame, rx, ry, 0.0, &p);

	      fbit.setPixel(p);

	      rx += pixelDim;

	      fbit.goRight();
	    }
	  ry += pixelDim;
	}
      
      if(updatePos)
	{
	  setPosition(wx,wy, 0.0, inFrame->t); 
	}
      success = true;
    }
  std::cout << "TObject::isMe returns " << success << "\n";
  return success;
}

// methods required by trackable.
bool TObject::isMe(RunRegion & region, FrameBuffer * inFrame, FrameBufferWorld * worldFrame, unsigned int avgIntensity)
{
  return isMe(region, inFrame, worldFrame, avgIntensity, true);
}

bool TObject::trackMe(FrameBuffer * inFrame, FrameBufferWorld * worldFrame)
{
   bool success = false;
   double wx,wy;
   double pixelDim = (Trackable::getRadius() * 8.0) / (double)Trackable::scratchpad()->height;
   unsigned int lrx,lry;
   // check if the centre of the object is black in the interpolated frame;
   Trackable::predictPosition(wx,wy,inFrame->t);
   if(!worldFrame->worldToBufferCoordinates(wx,wy,&lrx,&lry))
     {
       if(worldFrame->getIntensity(lry,lrx))
	 {
	       if(success = findMe(inFrame, pixelDim, &wx,&wy))
		 {
		   setPosition(wx,wy, 0.0, inFrame->t); 
		   Trackable::maskObject(worldFrame);  
		 }
	 }
     }

  return success;
}

bool TObject::findMe(FrameBuffer * inFrame, double pixelDim, double * rwx, double * rwy, WorldRegion * theRegion)
{
#if 0
  double px,py, offset;
  double wx,wy, lrx, lry;
  unsigned int maxPix;
  int maxIdx;
  RawPixel black(0,0,0);
  bool success = false;
  bool onRun = false;

  //  std::cout << "TObject::findMe()\n";
  
  if(scratch && inFrame && getPercentFound() > 0.01)
    {
      // std::cout << "scratchpad: (" << scratch->height << "," << scratch->width << ")\n";
      //std::cout << "pixelDim: " << pixelDim << "\n";
      offset = -(((double)(scratch->height) * pixelDim)/2.0);
      Trackable::predictPosition(px,py,inFrame->t);
      FrameBufferIterator fbit(scratch);
      wy = py + offset;
      // make sure the predicted position is on the field
      if (Trackable::vi2d->interpolatePixel( inFrame, px, py, 0.0, &p))
	{
	  for(unsigned int row = 0; row < scratch->height; ++row)
	    {
	      wx = px + offset;
	      fbit.goPosition(row,0);
	      onRun = false;
	      for(unsigned int col = 1; col < scratch->width; ++col)
		{
		  //	      std::cout << row << "," << col << "-->" << wy << "," << wx << "\n";
		  Trackable::vi2d->interpolatePixel( inFrame, wx, wy, 0.0, &p);
		  p.update();
		  if(p.intensity() != 0 )
		    {
		      fbit.setPixel(p);
		      onRun = true;
		    }
		  else if(onRun)
		    { // this is an unsophisticated region bulking algorithm
		      fbit.setPixel(p);
		      onRun = false;
		      //		      std::cout << "bulking\n";
		    }
		  else
		    {
		      fbit.setPixel(black);
		    }
		  wx += pixelDim;
		  fbit.goRight();
		}
	      wy += pixelDim;
	    }
	  regionList.createRegions(scratch);
	  maxPix = 0;
	  maxIdx = -1;
	  for(unsigned int i = 0; i < regionList.size(); ++i)
	    {
	      if(regionList[i].numPixels > maxPix)
		{
		  maxPix = regionList[i].numPixels;
		  maxIdx = i;
		}
	    }

	  if(maxIdx != -1)
	    {
	      success = true;
	      lrx = regionList[maxIdx].getCOMXD();
	      lry = regionList[maxIdx].getCOMYD();
	      
	      //      std::cout << "region: (" << lrx << "," << lry << ")\n";
	      *rwx = px + offset + (lrx * pixelDim);
	      *rwy = py + offset + (lry * pixelDim);
	      //	      std::cout << "predicted: (" << px << "," << py << ")\n";
	      //	      std::cout << "offset: (" << offset << "," << offset << ")\n";
	      //	      std::cout << "world: (" << wx << "," << wy << ")\n";
	      //	      std::cout << "found tobject in scratchpad!\n";
	      fbit.goPosition((int)floor(lry), (int)floor(lrx));
	      fbit.setPixel(RawPixel(255,0,0));
	      
	      if(theRegion)
		*theRegion = WorldRegion(*rwx, *rwy, regionList[maxIdx], pixelDim);
	    

	    }
	  else
	    {
	      fbit.goPosition(1,1);
	      fbit.setPixel(RawPixel(0,0,255));
	    }  
	}
      //      else
      //	{
	  //	  std::cout << "Not found on field: (" << px << "," << py << "\n";
      //	}
    }
  return success;
#endif
  return false;
}

void TObject::output (std::ostream & os)
{ 
  double wx=0.0,wy=0.0,vx = 0.0, vy = 0.0;
  FrameBuffer * scratch = Trackable::scratchpad();

  vx = isnan (dx) ? 0.0 : dx;
  vy = isnan (dy) ? 0.0 : dy;
  
  if(everFound)
    getAdjustedCoordinates(&wx,&wy);

  os << (int)type << ' ' << name << ' ' << (found ? "Found" : "NoFnd") << ' '
    << wx << ' ' << wy << ' ' << height << ' '
    << theta << ' ' << vx << ' ' << vy << "\n";
  if ( ( found ) && ( 0 != scratch ) )
    {
      FrameBufferIterator fbit(scratch);
      RawPixel pixel;

      os << "P6\n";
      os << "# " << name << " " << wx << " " << wy << " " << theta << "\n";
      os << scratch->width << " " << scratch->height << "\n";
      os << 255 << "\n";

      for( unsigned int i = 0; i < scratch->height; i++ )
	{
	  fbit.goPosition( i, 0 );
	  for( unsigned int j = 0; j < scratch->width; j++ )
	    {
	      fbit.getPixel( & pixel );
	      os.put( pixel.red != 0 ? pixel.red : 1 );
	      os.put( pixel.green != 0 ? pixel.green : 1 );
	      os.put( pixel.blue != 0 ? pixel.blue : 1 );
	      fbit.goRight( );
	    }
	}
      os << "End of Pixmap\n";
    }
}

