// $Id: controlpoints.cpp,v 1.1.1.1.2.3 2004/10/04 13:50:16 cvs Exp $
//

using namespace std;

#include "controlpoints.h"
#include <vector>
#include <math.h>
#include <sstream>
#include <iostream>

ControlPoint::ControlPoint( double xw, double yw, double zw, double xm, double ym, double zm )
  : xw( xw ), yw( yw ), zw ( zw ), xm( xm ), ym (ym ), zm ( zm )
{
  
}

ControlPoints::ControlPoints ()
{
  // Initialize the vector
  points = vector < struct ControlPoint >();
}

ControlPoints::~ControlPoints ()
{
  // Destroy the vector
  //  ~points;
}

struct ControlPoint
ControlPoints::getPoint (int index)
{
  return points[index];
}

struct ControlPoint *
ControlPoints::getPointRef (int index)
{
  return &points[index];
}

void
ControlPoints::addPoint (struct ControlPoint point)
{
  points.push_back (point);
}

void
ControlPoints::addPoint ( double xw, double yw, double zw )
{
  struct ControlPoint point( xw, yw, zw, 0.0, 0.0, 0.0 );
  points.push_back (point);
}


int
ControlPoints::size (void)
{
  return points.size ();
}

struct ControlPoint *
ControlPoints::selectPoint (int x, int y)
{
  const int maxDist = 10;
  double dx, dy;
  struct ControlPoint *ret = 0;

  for (vector < struct ControlPoint >::iterator p = points.begin ();
       p != points.end (); ++p)
    {
      dx = x - p->xm;
      dy = y - p->ym;

      if (hypot (dx, dy) < maxDist)
	{
	  ret = &(*p);
	  break;
	}
    }
  return ret;
}

void
ControlPoints::clear (void)
{
  points.clear ();
}

std::ostream & 
operator<<( std::ostream & os, ControlPoint & p )
{
  os << "[" << p.xw << "/" << p.yw << "/" << p.zw << "," << p.xm << "/" << p.ym << "/" << p.zm << "]";
  return os;
}




