
#include "trackerviewer.h"
#include "tracker.h"
#include "paintbox.h"
#include "trackable.h"
#include "videointerpreter2d.h"
#include <qstring.h>
#include "mainwindow.h"
#include "trackablewidget.h"
#include "robot.h"

#define VEL_SCALE 100.0

TrackerViewer::TrackerViewer( QWidget * parent, char const * name) :
  FrameBufferViewer(parent,name)
{
  // 0
}
TrackerViewer::~TrackerViewer()
{
  // 0
}
void TrackerViewer::paintChild(QPainter & p)
{
  double wx,wy;
  int sx,sy;
  int sx2, sy2;
  int wgx,wgy,wgx2,wgy2;
  double theta;
  Trackable * current;
  QString label;
  
  // update stats.
  mainWindow->trackableWidget->updateDisplay();

  // draw info.
  if(tracker && vi2d)
    {
      // paint each trackable item if it is found.
      for(uint i = 0; i < tracker->trackable.size(); ++i)
	{
	  current = tracker->trackable[i];
	  if(current->getPercentFound() > 0.03)
	    {
	      // draw centre and name
	      //	      label = QString("%1 %2%").arg(current->getName()).arg(current->getPercentFound() * 100);
	      label = QString("%1").arg(current->getName());
	      current->getScreenCoordinates(&sx,&sy);
	      screenToWidgetCoordinates( sx, sy, &wgx, &wgy );
	      PaintBox::drawPoint( p, wgx, wgy,  7, label, 8,Qt::blue,Qt::cyan);

	      wx = current->getX();
	      wy = current->getY();

	      if(current->getType() == Trackable::ROBOT)
		{
		  // paint theta. 
		  theta = current->getTheta();
		  vi2d->worldToScreenCoordinates(wx + (current->getRadius() * cos(theta)), wy + (current->getRadius() * sin(theta)), 0.0, &sx2,&sy2);
		  screenToWidgetCoordinates( sx2, sy2, &wgx2, &wgy2 );
		  p.setPen(QPen(Qt::red, 2));
		  p.drawLine(wgx,wgy,wgx2,wgy2);
		}
	      // paint velocity.
	      vi2d->worldToScreenCoordinates(wx + (current->getDx() * VEL_SCALE), wy + (current->getDy()*VEL_SCALE), 0.0, &sx2,&sy2);
	      screenToWidgetCoordinates( sx2, sy2, &wgx2, &wgy2);
	      p.setPen(QPen(Qt::blue, 1));
	      p.drawLine(wgx,wgy,wgx2,wgy2);
	      // paint the bounding box.
	      vi2d->worldToScreenCoordinates(wx - current->getRadius(), wy - current->getRadius(), 0.0, &sx,&sy);
	      vi2d->worldToScreenCoordinates(wx + current->getRadius(), wy + current->getRadius(), 0.0, &sx2,&sy2);
	      screenToWidgetCoordinates( sx, sy, &wgx, &wgy );
	      screenToWidgetCoordinates( sx2, sy2, &wgx2, &wgy2);
	      p.drawRect(wgx,wgy,wgx2-wgx, wgy2-wgy);
	    }
	}
    }
}

void TrackerViewer::setTracker(Tracker * tracker)
{
  this->tracker = tracker;
}

void TrackerViewer::setVideoInterpreter(VideoInterpreter2D * init_vi2d)
{
  vi2d = init_vi2d;
}
