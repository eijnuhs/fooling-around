#ifndef _QUICKTIME_PROCESSING_H
#define _QUICKTIME_PROCESSING_H

#include "umimage.hpp"
#include "umimagetypedefs.hpp"
#include <string>

#include "cameraTool.h"

class QuickTimeProcessing
{
public:
  typedef RGB24Pixel pixel_type;
  typedef UmImage<pixel_type> image_type;

  QuickTimeProcessing();
  ~QuickTimeProcessing();

  int initialize();
  void startCamera();
  void stopCamera();

  image_type getFrame();

  int getWidth();
  int getHeight();
  int getFPS();

  bool stillRunning();
private:
  cameraEngine* camera;
  int width;
  int height;
  int fps;
};

#endif
