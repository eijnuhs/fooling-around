#ifndef __VIDEO_INTERPRETER_2D_CONFIG_H_
#define __VIDEO_INTERPRETER_2D_CONFIG_H_

#include "configuration.h"
#include "controlpoints.h"

class VideoInterpreter2D;

class VideoInterpreter2DConfig : public Configuration
{
 public:
  VideoInterpreter2DConfig();
  ~VideoInterpreter2DConfig();
  ControlPoints getControlPoints(VideoInterpreter2D * vi2d);
 
  
 private:
  

};

#endif


