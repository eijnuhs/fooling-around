#ifndef UMBOTICA_ERROR_HPP
#define UMBOTICA_ERROR_HPP


#ifndef NDEBUG

#include <stdexcept>
#include <sstream>

// note that this will include the tostring header which includes
// s_tuple() functions to stringify any values that define
// operator<<.  Because assertThrowRuntimeError is a macro,
// the s_tuple calls will disappear if NDEBUG is enabled.
#include "io/tostring.hpp"


namespace umbotica { namespace detail {
  inline void throw_runtime_error(std::string function, std::string file,
				int line, std::string message)
  {
	std::stringstream s;
	s << file << ":" << line << " " << function << "() " << message;

	throw(std::runtime_error(s.str()));
  }
}}

#define assertThrowRuntimeError(condition, message) \
  if(!condition) umbotica::detail::throw_runtime_error(__FUNCTION__,__FILE__,__LINE__,std::string( #condition ) + " failed: "  + message)

#define throwRuntimeError(message) \
  umbotica::detail::throw_runtime_error(__FUNCTION__,__FILE__,__LINE__, message)

#else

#define assertThrowRuntimeError(condition, message)
#define throwRuntimeError(message)

#endif

#endif
