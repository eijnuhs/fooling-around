#ifndef UMBOTICA_UTILITIES_H
#define UMBOTICA_UTILITIES_H

#include "../error.hpp"
#include <cmath>

namespace umbotica { 
  template<typename T>
  inline T min(T x, T y)
  {
    return x < y ? x : y;
  }

  template<typename T>
  inline T max(T x, T y)
  {
    return x > y ? x : y;
  }


  template<typename T>
  inline T clamp(T val, T min_val, T max_val)
  {
    return umbotica::min(max_val, umbotica::max(val, min_val));
  }

  template<typename T>
  inline T mix(T x, T y, double alpha)
  {
    alpha = umbotica::clamp(alpha, 0.0, 1.0);
    return (x * alpha) + (y * (1.0 - alpha));
  }


  // convert a value from one range to another.
  template<typename T, typename U>
  inline U convertValue(T value, T fromLB, T fromUB, U toLB, U toUB)
  {    
    T offset = value - fromLB;
    T fromRange  = fromUB - fromLB;

    T toRange = (T)(toUB - toLB);

    return toLB + (U)((offset * toRange) / fromRange);
  }

  inline double randOne()
  {
    return (double)random() / (double)RAND_MAX;
  }

  inline double smoothstep(double val, double lb, double ub)
  {
    assertThrowRuntimeError(lb < ub, "upper bound less than lower bound. [lb,ub]: " + umbotica::s_tuple(lb,ub) );
    double result;
    if(val >= ub)
      result = 1.0;
    else if(val <= lb)
      result = 0.0;
    else
      {
	double x = (val - lb) / (ub - lb); 
	result = x*x*(3.0-(2.0*x));
      }
    assertThrowRuntimeError(result <= 1.0 && result >= 0.0, "result " + s_tuple(result) + "is out of range.");
    return result;

  }

  
  // performs symmetric rounding to the nearest int
  inline int symmetricRound(double value)
  {
    int bottom = (int)floor(value);
    int top = (int)ceil(value);
    int result;
    
    double bottomDiff = fabs(bottom - value);
    double topDiff = fabs(top - value);
    
    if (bottom == top)
    {
      result = top; // or bottom... it doesn't matter, you're trying to round an int
    }
    else if (bottomDiff < topDiff)
    {
      result = bottom;
    }
    else if (topDiff < bottomDiff)
    {
      result = top;
    }
    else if ( (topDiff == bottomDiff) && (value > 0) )
    {
      result = top; // 0.5 rounds up to 1
    }
    else if ( (topDiff == bottomDiff) && (value < 0) )
    {
      result = bottom; // -0.5 rounds down to -1
    }
    else
    {
      result = 0;
      throwRuntimeError("Rounding error.");
    }
    
    return result;
  }

}



#endif
