#include "colourmap.h"
#include "pixel.h"
#include <iostream>
#include <fstream>


ColourMap::ColourMap(std::string filename) :
  cMap(),
  selected(cMap.end()),
  lastFilename(filename)
{
  loadFromFile(filename);
}

ColourMap::~ColourMap()
{

}

ColourMap::ColourMap(ColourMapConfig * cmc) :
  cMap(),
  selected(cMap.end()),
  lastFilename("")
{
  if(cmc)
    {
      lastFilename = cmc->getString("colourFile", "/home/jacky/video_config/ergo_colours.dat");
      loadFromFile(lastFilename);	
    }
  else
    reset();
}

void ColourMap::reset()
{
  std::cout << "ColourMap::reset()\n";

  cMap.clear();
  cMap["default"] = ColourDefinition();
  selected = cMap.begin();
}

void ColourMap::addColour(std::string colourName)
{
  cMap[colourName] = ColourDefinition();
  selected = cMap.find(colourName);
}

void ColourMap::updateSelected(const Pixel & p)
{
  if(selected != cMap.end())
    {
      selected->second.addPixel(p);
    }
}
void ColourMap::resetSelected()
{
  if(selected != cMap.end())
    {
      selected->second.reset();
    }
}
void ColourMap::selectNext()
{
  if(selected != cMap.end())
    {
      selected++;
      if(selected == cMap.end())
	selected = cMap.begin();
    }
  else
    {
      selected = cMap.begin();
    }

}
void ColourMap::selectPrevious()
{
  if(selected != cMap.begin() && selected != cMap.end())
    {
      selected--;
    }
  else if(cMap.size() > 0)
    {
      selected = cMap.end();
      selected--;
    }

}
std::string ColourMap::getSelectedName()
{
  if(selected != cMap.end())
    {
      return selected->first;
    }
  else
    return "No Colour Selected";
}
ColourDefinition * ColourMap::getSelectedColour()
{
  
  if(selected == cMap.end() || cMap.size() == 0)
    {
      if(cMap.size() > 0)
	selected = cMap.begin();
      else
	reset();
    }
  
  return &selected->second;

}

RawPixel ColourMap::getSelectedAvgColour()
{
  return getSelectedColour()->getAverageColour();
}

bool ColourMap::loadFromFile(std::string filename)
{
  bool success = false;
  std::string name;
  ColourDefinition cd;
  cMap.clear();
  std::ifstream file(filename.c_str());
  if(file)
    {
      while(!file.eof())
	{
	  file >> name;
	  file >> cd;
	  cMap[name] = cd;
	}
      file.close();
      success = true;
      lastFilename = filename;
    }
  else
    {
      std::cerr << "Colours::loadFromFile() unable to open file: " << filename << "\n";
    }
  
  if(!success || cMap.size() == 0)
    {
      std::cerr << "Colours::loadFromFile() No colours loaded.  Setting default colour" << filename << "\n";
      reset();
    }
  else
    {
      selected = cMap.begin();
    }

  return success;
}

std::string ColourMap::getLastFilename()
{
  return lastFilename;
}

bool ColourMap::saveToFile(std::string filename)
{
  bool success = false;

  if(filename != "")
    {
      std::ofstream file(filename.c_str());
      if(file)
	{
	  CMAP_TYPE::iterator it = cMap.begin();
	  while(it != cMap.end())
	    {
	      file << it->first << "\n";
	      file << it->second << "\n\n";
	      it++;
	    }
	  file.close();
  	  success = true;
	}
      else
	{
	  std::cerr << "Colours::loadFromFile(std::string filename) error saving to file: " << filename << "\n";
	}
	
    }
  
  return success;
}


ColourDefinition * ColourMap::getColour(const std::string & colourName)
{
  // according to the STL manual, this will insert a new colour
  // definition if necessary.
  return &cMap[colourName];    
}
