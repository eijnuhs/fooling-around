#ifndef _SAMPLE_PROCESSING_H_
#define _SAMPLE_PROCESSING_H_

#include "umimage.hpp"
#include "umimagelocker.hpp"
#include "umimagegui.hpp"
#include <boost/bind.hpp>
#include <agmanager.hpp>

template<class PIXELTYPE>
class SampleProcessing
{
public:
  typedef UmImage<PIXELTYPE> image_type;
  typedef UmImage<float> processing_type;
  SampleProcessing(): m_processing(4){}

  // the function call operator that us to pass this object in
  // place of a function pointer to the VideoStream.
  void operator()(UmImage<PIXELTYPE> & image)
  {
    if(!image.isNull())
      {
	UmImageLocker lock1(m_image);

	m_image = image;
	UmImageLocker lock2(m_processed);
	m_processed.copyFrom(m_image);
	if(m_processing == 1)
	  cvSobel(&m_processed, &m_processed, 1,0,3);
	else if(m_processing == 2)
	  cvSobel(&m_processed, &m_processed, 0,1,3);
	else if(m_processing == 3)
	  cvLaplace(&m_processed, &m_processed, 3);
	else if(m_processing == 4)
	  cvSmooth(&m_processed, &m_processed,CV_GAUSSIAN,7,7);
      }
  }
  UmImage<PIXELTYPE> & getImage(){return m_image;}
  processing_type & getProcessed(){return m_processed;}
  unsigned int getPIdx(){ return m_processing; }
  void setPIdx(unsigned int p){ m_processing = p; }

private:
  unsigned int m_processing;
  UmImage<PIXELTYPE> m_image;
  processing_type m_processed;
};


template<class PIXELTYPE>
inline void addToGui(AGManager & gui, SampleProcessing<PIXELTYPE> & b, std::string name, int updateMsec)
{
  gui.pushGroup("raw", false);
  gui.setWindowSize(640,480);
  addToGui(gui, b.getImage(), "Raw Image", updateMsec,true);
  gui.popGroup();

  gui.pushGroup("processed", false);
  gui.setWindowSize(640,480);
  addToGui(gui, b.getProcessed(), "processed", updateMsec,true);
  // add other processing buttons.
  char * list[] = {"No Processing", "Sobel x", "Sobel y", "Laplace", "Gaussian Blur"};
  std::vector<std::string> stlist(list, list + 5);
  // this syntax is really awful.  I'll have to change the AGManager class to accept accessors and mutators more easily.
  gui.addIndexedList("Processing Choice", 
		     boost::bind(&SampleProcessing<PIXELTYPE>::getPIdx,&b), 
		     boost::bind(&SampleProcessing<PIXELTYPE>::setPIdx,&b,_1), stlist);
  gui.popGroup();

}



#endif
