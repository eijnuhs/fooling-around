// $Id: videoviewer.h,v 1.1.1.1.2.1 2004/12/28 19:30:05 cvs Exp $
// A QT widget that displays various video information
// Jacky Baltes <jacky@cs.umanitoba.ca> Tue Oct 21 21:46:10 CDT 2003

#ifndef _VIDEO_VIEWER_H_
#define _VIDEO_VIEWER_H_

#include "framebufferviewer.h"
#include <mainwindow.h>

class VideoViewer : public FrameBufferViewer
{
  Q_OBJECT

 public:
  VideoViewer( QWidget * parent = 0, char const * name = 0 );
  virtual void updateQImageFromFrameBuffer( FrameBuffer * frame );
  virtual void printName();  
 private:
};

#endif
