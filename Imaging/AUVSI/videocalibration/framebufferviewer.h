// $Id: framebufferviewer.h,v 1.1.1.1.2.10 2004/12/28 19:30:05 cvs Exp $
// A QT widget that displays a FrameBuffer
// Jacky Baltes <jacky@cs.umanitoba.ca> Tue Oct 21 21:46:10 CDT 2003

#ifndef _FRAME_BUFFER_VIEWER_H_
#define _FRAME_BUFFER_VIEWER_H_

//#define SAFE_DRAW

#include <qwidget.h>
#include <qevent.h>
#include <qimage.h>
#include <time.h>
#include <sys/time.h>
#include <time.h>
#ifdef SAFE_DRAW
#include <qmutex.h>
#endif
#include <qtimer.h>

// Forward declarations
class Form1;
class FrameBuffer;

class FrameBufferViewer : public QWidget
{
  Q_OBJECT

 public: 
  FrameBufferViewer( QWidget * parent = 0, char const * name = 0 );
  ~FrameBufferViewer();

  static void startTimer( int msec );

  virtual void paintEvent( QPaintEvent * e);
  virtual void mousePressEvent( QMouseEvent * );
  virtual void mouseMoveEvent( QMouseEvent * e );
  virtual void mouseDoubleClickEvent ( QMouseEvent * e );
  virtual void paintChild(QPainter & p);  // method for child classes to override if they want full widget paint functionality during a paint event
  virtual void displayColourInfo(QMouseEvent * e);
  virtual void updateQImageFromFrameBuffer( FrameBuffer * frame );

  void screenToWidgetCoordinates( int sx, int sy, int * wx, int * wy );
  void screenToWidgetCoordinates( double sx, double sy, int * wx, int * wy );
  void widgetToScreenCoordinates( int wx, int wy, int * sx, int * sy );
  void frameToWidgetCoordinates( int fx, int fy, int * wx, int * wy );
  void frameToWidgetCoordinates( double fx, double fy, int * wx, int * wy );
  void widgetToFrameCoordinates( int wx, int wy, int * fx, int * fy );
  void screenToFrameCoordinates( int sx, int sy, int * fx, int * fy );
  void frameToScreenCoordinates( int fx, int fy, int * sx, int * sy );
  virtual void printName();
 public slots:
  virtual void newFrame( FrameBuffer * );
  virtual void redraw();

 protected:
  void checkImageWidthAndHeight( FrameBuffer * frame );
  QWidget * gotoParent( QWidget * widget, int level ) const;
  Form1 * mainWindow;
  QImage * image;
  FrameBuffer * frame;
#ifdef SAFE_DRAW
  QMutex mutex;
#endif

 private:
  struct timeval prev;
  double fps;
  bool needRedraw;
  static QTimer * redrawTimer;
};

#endif

