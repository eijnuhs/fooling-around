// $Id: imageprocessing.cpp,v 1.1.1.1.2.9 2005/01/26 01:21:55 cvs Exp $
//
// Jacky Baltes <jacky@cs.umanitoba.ca> Fri Jun 18 16:08:21 CDT 2004
//

#include "imageprocessing.h"
#include "pixel.h"
#include "framebuffer.h"
#include "geometry.h"
#include "visitedmap.h"
#include "colourdefinition.h"

#ifdef DEBUG_PTF
#include <iostream>
#endif

FeatureDetector features[] = 
  {
    FeatureDetector( ImageProcessing::VERT_UP, ImageProcessing::UP, ImageProcessing::NO_EDGE, ImageProcessing::NO_EDGE, ImageProcessing::UP, ImageProcessing::NO_EDGE, ImageProcessing::NO_EDGE, ImageProcessing::UP, ImageProcessing::NO_EDGE, ImageProcessing::NO_EDGE ),

    FeatureDetector( ImageProcessing::VERT_DOWN, ImageProcessing::DOWN, ImageProcessing::NO_EDGE, ImageProcessing::NO_EDGE, ImageProcessing::DOWN, ImageProcessing::NO_EDGE, ImageProcessing::NO_EDGE, ImageProcessing::DOWN, ImageProcessing::NO_EDGE, ImageProcessing::NO_EDGE ),

    FeatureDetector( ImageProcessing::HORIZ_UP, ImageProcessing::UP, ImageProcessing::UP, ImageProcessing::UP, ImageProcessing::NO_EDGE, ImageProcessing::NO_EDGE, ImageProcessing::NO_EDGE, ImageProcessing::NO_EDGE, ImageProcessing::NO_EDGE, ImageProcessing::NO_EDGE ),

    FeatureDetector( ImageProcessing::HORIZ_DOWN, ImageProcessing::DOWN, ImageProcessing::DOWN, ImageProcessing::DOWN, ImageProcessing::NO_EDGE, ImageProcessing::NO_EDGE, ImageProcessing::NO_EDGE, ImageProcessing::NO_EDGE, ImageProcessing::NO_EDGE, ImageProcessing::NO_EDGE ),

    //    FeatureDetector( ImageProcessing::ANGLE45_UP, ImageProcessing::UP, ImageProcessing::UP, ImageProcessing::NO_EDGE, ImageProcessing::UP, ImageProcessing::NO_EDGE, ImageProcessing::NO_EDGE, ImageProcessing::NO_EDGE, ImageProcessing::NO_EDGE, ImageProcessing::NO_EDGE ),

    //    FeatureDetector( ImageProcessing::ANGLE45_DOWN, ImageProcessing::DOWN, ImageProcessing::DOWN, ImageProcessing::NO_EDGE, ImageProcessing::DOWN, ImageProcessing::NO_EDGE, ImageProcessing::NO_EDGE, ImageProcessing::NO_EDGE, ImageProcessing::NO_EDGE, ImageProcessing::NO_EDGE ),

    //    FeatureDetector( ImageProcessing::ANGLE135_UP, ImageProcessing::NO_EDGE, ImageProcessing::UP, ImageProcessing::UP, ImageProcessing::NO_EDGE, ImageProcessing::NO_EDGE, ImageProcessing::UP, ImageProcessing::NO_EDGE, ImageProcessing::NO_EDGE, ImageProcessing::NO_EDGE ),

    //    FeatureDetector( ImageProcessing::ANGLE135_DOWN, ImageProcessing::NO_EDGE, ImageProcessing::DOWN, ImageProcessing::DOWN, ImageProcessing::NO_EDGE, ImageProcessing::NO_EDGE, ImageProcessing::DOWN, ImageProcessing::NO_EDGE, ImageProcessing::NO_EDGE, ImageProcessing::NO_EDGE )
  };


enum ImageProcessing::EdgeDirection
ImageProcessing::calcEdgeDirection( RawPixel const & p1, RawPixel const & p2, double threshold )
{
  enum EdgeDirection result = NO_EDGE;

  if ( p1.diffMax( p2 ) > threshold )
    {
      if ( p1.intensity() > p2.intensity() )
	{
	  result = UP;
	}
      else
	{
	  result = DOWN;
	}
    }
  return result;
}

enum ImageProcessing::FeatureType 
ImageProcessing::classifyFeature( RawPixel pixels[ 3 ][ 3 ], double threshold, double * confidence  )
{
  enum ImageProcessing::FeatureType result = ImageProcessing::NONE;

  FeatureDetector edgeInfo;

  edgeInfo.map[ 0 ][ 0 ] = ImageProcessing::calcEdgeDirection( pixels[ 0 ][ 0 ], pixels[ 1 ][ 1 ], threshold );
  edgeInfo.map[ 0 ][ 1 ] = ImageProcessing::calcEdgeDirection( pixels[ 0 ][ 1 ], pixels[ 1 ][ 1 ], threshold );
  edgeInfo.map[ 0 ][ 2 ] = ImageProcessing::calcEdgeDirection( pixels[ 0 ][ 2 ], pixels[ 1 ][ 1 ], threshold );
  
  edgeInfo.map[ 1 ][ 0 ] = ImageProcessing::calcEdgeDirection( pixels[ 1 ][ 0 ], pixels[ 1 ][ 1 ], threshold );
  edgeInfo.map[ 1 ][ 1 ] = ImageProcessing::calcEdgeDirection( pixels[ 1 ][ 1 ], pixels[ 1 ][ 1 ], threshold );
  edgeInfo.map[ 1 ][ 2 ] = ImageProcessing::calcEdgeDirection( pixels[ 1 ][ 2 ], pixels[ 1 ][ 1 ], threshold );
  
  edgeInfo.map[ 2 ][ 0 ] = ImageProcessing::calcEdgeDirection( pixels[ 2 ][ 0 ], pixels[ 1 ][ 1 ], threshold );
  edgeInfo.map[ 2 ][ 1 ] = ImageProcessing::calcEdgeDirection( pixels[ 2 ][ 1 ], pixels[ 1 ][ 1 ], threshold );
  edgeInfo.map[ 2 ][ 2 ] = ImageProcessing::calcEdgeDirection( pixels[ 2 ][ 2 ], pixels[ 1 ][ 1 ], threshold );

  int max = 7;  // 7/9 = Minimum to classify something as a feature
  int index = -1;
  for( unsigned int i = 0; i < sizeof( features ) / sizeof( features[ 0 ] ); i++ )
    {
      int c = ImageProcessing::matchFeature( edgeInfo, features[ i ] );
      if ( c > max ) 
	{
	  max = c;
	  index = i;
	  result = features[ i ].type;
	}
      if ( confidence != 0 )
	{
	  * confidence = static_cast< double > ( c ) / 9.0;
	}
    }
  return result;
}

int 
ImageProcessing::matchFeature( FeatureDetector f1,  FeatureDetector f2 )
{
  int result = 0;
  
  for( int i = 0; i < 3; i++ )
    {
      for( int j = 0; j < 3; j++ )
	{
	  if ( ( f1.map[ i ][ j ] == ImageProcessing::ANY ) || ( f2.map[ i ][ j ] == ImageProcessing::ANY ) || ( f1.map[ i ][ j ] == f2.map[ i ][ j ] ) )
	    {
	      result++;
	    }
	}
    }
  return result;
}

FeatureDetector::FeatureDetector( enum ImageProcessing::FeatureType type, enum ImageProcessing::EdgeDirection tl, enum ImageProcessing::EdgeDirection t, enum ImageProcessing::EdgeDirection tr, enum ImageProcessing::EdgeDirection l, enum ImageProcessing::EdgeDirection h, enum ImageProcessing::EdgeDirection r, enum ImageProcessing::EdgeDirection bl, enum ImageProcessing::EdgeDirection b, enum ImageProcessing::EdgeDirection br )
{
  this->type = type;
  map[ 0 ][ 0 ] = tl;
  map[ 0 ][ 1 ] = t;
  map[ 0 ][ 2 ] = tr;

  map[ 1 ][ 0 ] = l;
  map[ 1 ][ 1 ] = h;
  map[ 1 ][ 2 ] = r;

  map[ 2 ][ 0 ] = bl;
  map[ 2 ][ 1 ] = b;
  map[ 2 ][ 2 ] = br;

}

FeatureDetector::FeatureDetector( )
{
}

void
ImageProcessing::highlightFeatures( FrameBuffer * frame, double threshold )
{
  FeatureDetector edgeInfo;
  
  RawPixel pixels[ 3 ][ 3 ];
  RawPixel center;
  
  frame->getPixel( frame->height / 2, frame->width / 2, &center );

  for( unsigned int i = 1; i < frame->height - 1; i++ )
    {
      FrameBufferIterator it( frame, i, 1 );
      for( unsigned int j = 1; j < frame->width - 1; j++, it.goRight() )
	{
	  //	  it.getPixel( & pixels[ 0 ][ 0 ],  - frame->bytesPerLine - frame->bytesPerPixel );
	  it.getPixel( & pixels[ 0 ][ 1 ],  - frame->bytesPerLine );
	  //	  it.getPixel( & pixels[ 0 ][ 2 ],  - frame->bytesPerLine + frame->bytesPerPixel );
	  it.getPixel( & pixels[ 1 ][ 0 ],                        - frame->bytesPerPixel );
	  //	  it.getPixel( & pixels[ 1 ][ 1 ]);
	  it.getPixel( & pixels[ 1 ][ 2 ],                        + frame->bytesPerPixel );
	  //	  it.getPixel( & pixels[ 2 ][ 0 ],  + frame->bytesPerLine - frame->bytesPerPixel );
	  it.getPixel( & pixels[ 2 ][ 1 ],  + frame->bytesPerLine );
	  //	  it.getPixel( & pixels[ 2 ][ 2 ],  + frame->bytesPerLine + frame->bytesPerPixel );
	  RawPixel spixel( 0, 0, 0 );
	  enum EdgeDirection edge;

	  if ( NO_EDGE != ( edge = calcEdgeDirection( center, pixels[ 0 ][ 1 ], threshold ) ) )
	    {
	      spixel.red = 255;
	    }
	  if ( NO_EDGE != ( edge = calcEdgeDirection( center, pixels[ 0 ][ 2 ], threshold ) ) )
	    {
	      spixel.red = 255;
	    }
	  if ( NO_EDGE != ( edge = calcEdgeDirection( center, pixels[ 1 ][ 0 ], threshold ) ) )
	    {
	      spixel.green = 255;
	    }
	  if ( NO_EDGE != ( edge = calcEdgeDirection( center, pixels[ 1 ][ 2 ], threshold ) ) )
	    {
	      spixel.green = 255;
	    }
	  
	  it.setPixel( spixel );
	}
    }

}

#if 0
double
ImageProcessing::detectCircle( FrameBuffer * frame, unsigned int radius )
{
  FeatureDetector edgeInfo;
  
  RawPixel pixels[ frame->height ][ frame->width ];
  FrameBufferIterator it( frame );
  for( unsigned int i = 0; i < frame->height; i++ )
    {
      it.goRow( i );
      for( unsigned int j = 0; j < frame->width; j++, it.goRight( ) )
	{
	  it.getPixel( & pixels[ i ][ j ] );
	}
    }
  
  for( unsigned int i = 1; i < frame->height - 1; i++ )
    {
      it.goPosition( i, 1 );
      for( unsigned int j = 1; j < frame->width - 1; j++, it.goRight() )
	{
	  enum EdgeDirection edge;
	  bool isEdge = false;
	  RawPixel sPixel = pixels[ i ][ j ] / 2.0;
	  int count = 0;
	  if ( NO_EDGE != ( edge = calcEdgeDirection( pixels[ i ][ j ], pixels[ i - 1 ][ j ], 1000.0 ) ) )
	    {
	      count++;
	      sPixel.red |= 0x80;
	    }
	  if ( NO_EDGE != ( edge = calcEdgeDirection( pixels[ i ][ j ], pixels[ i + 1 ][ j ], 1000.0 ) ) )
	    {
	      count++;
	      sPixel.red |= 0x40;
	    }
	  if ( NO_EDGE != ( edge = calcEdgeDirection( pixels[ i ][ j ], pixels[ i ][ j - 1 ], 1000.0 ) ) )
	    {
	      count++;
	      sPixel.green |= 0x80;
	    }
	  if ( NO_EDGE != ( edge = calcEdgeDirection( pixels[ i ][ j ], pixels[ i ][ j  + 1 ], 1000.0 ) ) )
	    {
	      count++;
	      sPixel.green |= 0x40;
	    }
	  it.setPixel( sPixel );
	}
    }
  return 0.0;
}
#endif

#if 1
double
ImageProcessing::detectCircle( FrameBuffer * frame, RawPixel const color, unsigned int radius, unsigned int * xc, unsigned int *yc ) 
{
  double sum = 0.0;
  unsigned int sumX = 0;
  unsigned int sumY = 0;
  unsigned int cX = frame->width / 2;
  unsigned int cY = frame->height / 2;
  unsigned int numPoints = 0;
  double result = -1000.0;

  for( unsigned int i = 0; i < frame->height; i++ )
    {
      FrameBufferIterator it( frame, i, 1 );
      for( unsigned int j = 0; j < frame->width; j++, it.goRight() )
	{
	  RawPixel pix;
	  it.getPixel( & pix );

	  double pixelDiff = color.diffMax( pix );
	  if ( pixelDiff < 1000 )
	    {
	      sumX = sumX + j;
	      sumY = sumY + i;
	      numPoints++;
	    }
	  else
	    {
	      //	      it.setPixel( RawPixel( 0, 0, 100 ) );
	    }
	  unsigned int distance = Geometry::manhattenDistance( cX, cY, j, i ); 
	  
	  if ( ( distance < radius ) && ( pixelDiff < 2000 ) )
	    {
	      sum++;
	    }
	  else if ( ( distance < radius ) && ( pixelDiff >= 2000 ) )
	    {
	      sum--;
	    }
	  else if ( ( distance > radius ) && ( pixelDiff < 2000 ) )
	    {
	      sum--;
	    }
	  else
	    {
	      sum++;
	    }
	}
    }
  if ( 7 * numPoints > 10 * radius * radius )
    {
      if ( ( *xc != 0 ) && ( *yc != 0 ) )
	{
	  *xc = sumX / numPoints;
	  *yc = sumY / numPoints;
	}
      result = sum / ( frame->width * frame->height );
    }
#ifdef XXDEBUG
  std::cout << "Detect Circle " << sum / ( frame->width * frame->height ) << std::endl;
#endif  
  return result;
}
#endif








int 
ImageProcessing::doFloodFill(FrameBufferIterator fbit, VisitedMap & vMap, int xc[], int yc[], int minIntensity, int maxIntensity, int maxPixels, int numPixels, bool clobberPixels)
{
#ifdef XXXXDEBUG
  std::cout << "In ImageProcessing::doFloodFill()\n";
#endif

  if (numPixels < maxPixels)
    {

      unsigned int px,py;
      Pixel pixel;
      int intensity;
      //      std::cout << "Get Pixel\n";
      //      std::cout << "Buffer: " << (void *)(fbit.frame->buffer) << std::endl;
      //std::cout << "Getting Pixel: " << (void *)fbit.pixelPtr << std::endl;
      fbit.getPixel(&pixel);
      intensity = (int)( pixel.intensity() );
      px = fbit.getColumn();
      py = fbit.getRow();

      if ( intensity <= maxIntensity && intensity >= minIntensity && !vMap.isVisited(px,py))
	{
#ifdef XXDEBUG
	  std::cout << "numPixels: " << numPixels << std::endl;
	  std::cout << "maxPixels: " << maxPixels << std::endl;
	  std::cout << "intensity: " << intensity << std::endl;
	  std::cout << "maxIntensity: " << maxIntensity << std::endl;
	  std::cout << "minIntensity: " << minIntensity << std::endl;
#endif
	  xc[numPixels] = px;
	  yc[numPixels] = py;
	  numPixels++;

	  vMap.setVisited(px,py);		// Mark the pixel as visited
	  if ( clobberPixels )
	    {
#ifdef XXDEBUG
	      std::cout << "Clobbering pixel\n";
#endif
	      pixel.red = 255;
	      pixel.green = 0;
	      pixel.blue = 0;
	      fbit.setPixel(pixel);
	    }
	  // four pixel flood fill
	  if(fbit.goUp())
	    {
	      if(!vMap.isVisited(fbit.getColumn(), fbit.getRow()))
		numPixels = doFloodFill (fbit, vMap, xc, yc, minIntensity, maxIntensity, maxPixels, numPixels, clobberPixels);
	      fbit.goDown();
	    }
	  if(fbit.goRight())
	    {
	      if(!vMap.isVisited(fbit.getColumn(), fbit.getRow()))
		numPixels = doFloodFill (fbit, vMap, xc, yc, minIntensity, maxIntensity, maxPixels, numPixels, clobberPixels);
	      fbit.goLeft();
	    }
	  if(fbit.goLeft())
	    {
	      if(!vMap.isVisited(fbit.getColumn(), fbit.getRow()))
		numPixels = doFloodFill (fbit, vMap, xc, yc, minIntensity, maxIntensity, maxPixels, numPixels, clobberPixels);
	      fbit.goRight();
	    }
	  if(fbit.goDown())
	    {
	      if(!vMap.isVisited(fbit.getColumn(), fbit.getRow()))
		numPixels = doFloodFill (fbit, vMap, xc, yc, minIntensity, maxIntensity, maxPixels, numPixels, clobberPixels);
	    }
	  
	} // end if this pixel matches intensity
#ifdef XXDEBUG
      else if(!vMap.isVisited(px,py))
	{
	  if ( clobberPixels )
	    {
	      pixel.red = 0;
	      pixel.green = 0;
	      pixel.blue = 255;
	      fbit.setPixel(pixel);
	    }
	}
#endif
    } // end if we have room for more pixels.
  return numPixels;
}



int 
ImageProcessing::doFloodFill(FrameBufferIterator fbit, VisitedMap & vMap, int xc[], int yc[], ColourDefinition * colour, int maxPixels, int numPixels, bool clobberPixels)
{
#ifdef XXDEBUG
  std::cout << "In ImageProcessing::doFloodFill()\n";
#endif

  if (numPixels < maxPixels)
    {

      unsigned int px,py;
      Pixel pixel;
      //      std::cout << "Get Pixel\n";
      //      std::cout << "Buffer: " << (void *)(fbit.frame->buffer) << std::endl;
      //std::cout << "Getting Pixel: " << (void *)fbit.pixelPtr << std::endl;
      fbit.getPixel(&pixel);
      px = fbit.getColumn();
      py = fbit.getRow();

      if(colour->isMatch(pixel))
	{
#ifdef XXDEBUG
	  std::cout << "numPixels: " << numPixels << std::endl;
	  std::cout << "maxPixels: " << maxPixels << std::endl;
	  std::cout << "intensity: " << intensity << std::endl;
#endif
	  xc[numPixels] = px;
	  yc[numPixels] = py;
	  numPixels++;

	  vMap.setVisited(px,py);		// Mark the pixel as visited
	  if ( clobberPixels )
	    {
#ifdef XXDEBUG
	      std::cout << "Clobbering pixel\n";
#endif
	      pixel.red = 255;
	      pixel.green = 0;
	      pixel.blue = 0;
	      fbit.setPixel(pixel);
	    }
	  // four pixel flood fill
	  if(fbit.goUp())
	    {
	      if(!vMap.isVisited(fbit.getColumn(), fbit.getRow()))
		numPixels = doFloodFill (fbit, vMap, xc, yc, colour, maxPixels, numPixels, clobberPixels);
	      fbit.goDown();
	    }
	  if(fbit.goRight())
	    {
	      if(!vMap.isVisited(fbit.getColumn(), fbit.getRow()))
		numPixels = doFloodFill (fbit, vMap, xc, yc, colour, maxPixels, numPixels, clobberPixels);
	      fbit.goLeft();
	    }
	  if(fbit.goLeft())
	    {
	      if(!vMap.isVisited(fbit.getColumn(), fbit.getRow()))
		numPixels = doFloodFill (fbit, vMap, xc, yc, colour, maxPixels, numPixels, clobberPixels);
	      fbit.goRight();
	    }
	  if(fbit.goDown())
	    {
	      if(!vMap.isVisited(fbit.getColumn(), fbit.getRow()))
		numPixels = doFloodFill (fbit, vMap, xc, yc, colour, maxPixels, numPixels, clobberPixels);
	    }
	  
	} // end if this pixel matches intensity
#ifdef XXDEBUG
      else if(!vMap.isVisited(px,py))
	{
	  if ( clobberPixels )
	    {
	      pixel.red = 0;
	      pixel.green = 0;
	      pixel.blue = 255;
	      fbit.setPixel(pixel);
	    }
	}
#endif
    } // end if we have room for more pixels.
  return numPixels;
}









#if 0
double
ImageProcessing::detectCircle( FrameBuffer * frame, RawPixel const p, unsigned int radius, unsigned int * xc, unsigned int *yc )
{
  FeatureDetector edgeInfo;

  RawPixel pixels[ frame->height ][ frame->width ];
  FrameBufferIterator it( frame );
  for( unsigned int i = 0; i < frame->height; i++ )
    {
      it.goRow( i );
      for( unsigned int j = 0; j < frame->width; j++, it.goRight( ) )
	{
	  it.getPixel( & pixels[ i ][ j ] );
	}
    }
  
  unsigned int count = 0;
  for( unsigned int i = 1; i < frame->height - 1; i++ )
    {
      it.goPosition( i, 1 );
      for( unsigned int j = 1; j < frame->width - 1; j++, it.goRight() )
	{
	  enum EdgeDirection edge;
	  RawPixel sPixel = RawPixel( pixels[ i ][ j ] );

	  if ( NO_EDGE != ( edge = calcEdgeDirection( pixels[ i ][ j ], pixels[ i - 1 ][ j ], 1000.0 ) ) )
	    {
	      count++;
	      sPixel.red = RawPixel( 255, 0, 0 );
	    }
	  if ( NO_EDGE != ( edge = calcEdgeDirection( pixels[ i ][ j ], pixels[ i + 1 ][ j ], 1000.0 ) ) )
	    {
	      count++;
	      sPixel = RawPixel( 255, 0, 0 );
	    }
	  if ( NO_EDGE != ( edge = calcEdgeDirection( pixels[ i ][ j ], pixels[ i ][ j - 1 ], 1000.0 ) ) )
	    {
	      count++;
	      sPixel = RawPixel( 255, 0, 0 );
	    }
	  if ( NO_EDGE != ( edge = calcEdgeDirection( pixels[ i ][ j ], pixels[ i ][ j  + 1 ], 1000.0 ) ) )
	    {
	      count++;
	      sPixel = RawPixel( 255, 0, 0 );
	    }
	  it.setPixel( sPixel );
	}
    }
  return static_cast< double > ( count ) / static_cast< double >( frame->width * frame->height ) ;
}
#endif




#if 0
int ImageProcessing::doFloodFill(FrameBufferIterator fbit, VisitedMap & vMap, int xc[], int yc[], int minIntensity, int maxIntensity, int maxPixels, int numPixels, bool clobberPixels)
{
#ifdef XXDEBUG
  std::cout << "In ImageProcessing::doFloodFill()\n";
#endif

  if (numPixels < maxPixels)
    {

      unsigned int px,py;
      Pixel pixel;
      int intensity;
      //      std::cout << "Get Pixel\n";
      //      std::cout << "Buffer: " << (void *)(fbit.frame->buffer) << std::endl;
      //std::cout << "Getting Pixel: " << (void *)fbit.pixelPtr << std::endl;
      fbit.getPixel(&pixel);
      intensity = (int)( pixel.intensity() );
      px = fbit.getColumn();
      py = fbit.getRow();

      if ( intensity <= maxIntensity && intensity >= minIntensity && !vMap.isVisited(px,py))
	{
#ifdef XXDEBUG
	  std::cout << "numPixels: " << numPixels << std::endl;
	  std::cout << "maxPixels: " << maxPixels << std::endl;
	  std::cout << "intensity: " << intensity << std::endl;
	  std::cout << "maxIntensity: " << maxIntensity << std::endl;
	  std::cout << "minIntensity: " << minIntensity << std::endl;
#endif
	  xc[numPixels] = px;
	  yc[numPixels] = py;
	  numPixels++;

	  vMap.setVisited(px,py);		// Mark the pixel as visited
	  if ( clobberPixels )
	    {
#ifdef XXDEBUG
	      std::cout << "Clobbering pixel\n";
#endif
	      pixel.red = 255;
	      pixel.green = 0;
	      pixel.blue = 0;
	      fbit.setPixel(pixel);
	    }
	  int i, jInit, j;
	  i = fbit.goUp() ? -1 : 0;
	  jInit = fbit.goLeft() ? -1 : 0;
	  
	  while (i++ <= 1)
	    {
	      bool moveOk = true;
	      for(j = jInit; j <= 1 && moveOk; j++)
		{
		  if ( !((i == 0) && (j == 0)) && !vMap.isVisited(fbit.getColumn(), fbit.getRow()))
		    {
#ifdef XXDEBUG
		      std::cout << "Calling floodfill on the pixel: (" << i << "," << j << ") offset from this pixel\n";
		      std::cout << "This Pixel: (" << fbit.getRow() << "," << fbit.getColumn()<< ")\n";
#endif
		      numPixels = doFloodFill (fbit, vMap, xc, yc, minIntensity, maxIntensity, maxPixels, numPixels, clobberPixels);
		    }
		  moveOk = fbit.goRight();
		}
	      while(jInit < --j)
		{
		  fbit.goLeft();
		}
	      if( ! fbit.goDown() )
		{
		  break;
		}
	    } // end outer while
	} // end if this pixel matches intensity
#ifdef XXDEBUG
      else if(!vMap.isVisited(px,py))
	{
	  if ( clobberPixels )
	    {
	      pixel.red = 0;
	      pixel.green = 0;
	      pixel.blue = 255;
	      fbit.setPixel(pixel);
	    }
	}
#endif
    } // end if we have room for more pixels.
  return numPixels;
}


#endif

void ImageProcessing::thresholdImage(FrameBuffer * frame, unsigned int thresholds[THRESHOLD_SIZE], RawPixel killed)
{
  unsigned int regionThreshold[3*3];
  unsigned int rows[3];
  unsigned int cols[3];
  rows[0] = frame->height / 3;
  rows[1] = rows[0] + rows[0];
  rows[2] = frame->height;
  cols[0] = frame->width / 3;
  cols[1] = cols[0] + cols[0];

  // row 1
  regionThreshold[0] = thresholds[TOP_LEFT];
  regionThreshold[1] = (thresholds[TOP_LEFT] + thresholds[TOP_RIGHT]) / 2;
  regionThreshold[2] = thresholds[TOP_RIGHT];
  // row 3
  regionThreshold[6] = thresholds[BOTTOM_LEFT];
  regionThreshold[7] = (thresholds[BOTTOM_LEFT] + thresholds[BOTTOM_RIGHT]) / 2;
  regionThreshold[8] = thresholds[BOTTOM_RIGHT];
  // row 2
  regionThreshold[3] = (thresholds[TOP_LEFT] + thresholds[BOTTOM_LEFT]) / 2;
  regionThreshold[4] = (regionThreshold[1] + regionThreshold[7]) / 2;
  regionThreshold[5] =  (thresholds[TOP_RIGHT] + thresholds[BOTTOM_RIGHT]) / 2;

  unsigned int currentRow = 0;

  int regionIdx = 0;

  // run through the image killing pixels below the
  // specified threshold in each region.
  FrameBufferIterator fIt(frame,0,0);
  for(uint i = 0; i < frame->height; ++i)
    {
      fIt.goPosition(i,0);
      unsigned int currentColumn = 0;
      regionIdx = (currentRow * 3);
      for(uint j = 0; j < frame->width; ++j, fIt.goRight())
	{
	  // this kills the pixel;
	  if(fIt.getIntensity() < regionThreshold[regionIdx])
	    {
	      fIt.setPixel(killed);
	    }
	  // this increments the region index
	  if(j > cols[currentColumn] )
	    {
	      currentColumn++;
	      regionIdx = (currentRow*3) + currentColumn;
	    }
	} 
      if(i > rows[currentRow])
	{
	  currentRow++;
	}
    }

} 
#if 0
RunList ImageProcessing::runLengthEncode(FrameBuffer * frame, unsigned int x, unsigned int y, unsigned int height, unsigned int width)
{
  Run run;
  RunList runList;
  unsigned int row, col, xEnd, yEnd;
  FrameBufferIterator fbit(frame);
  runList.clear();
  xEnd = x + width;
  yEnd = y + height;

  //  std::cout << "rle enter\n";
  for(row = y; row < yEnd; ++row)
    {
      fbit.goPosition(row,0);
      // initialize the first run.
      run.row = row;
      run.col = x;
      run.colour = fbit.getIntensity();
      fbit.goRight();
      for(col = x + 1; col < xEnd; ++col)
	{
	  if(fbit.getIntensity() == 0 && run.colour != 0)
	    {
	      // store the run
	      run.length = col - run.col;
	      runList.push_back(run);
	      //  std::cout << "storing: " << run;
	      // initialize a new run
	      run.col = col;
	      run.colour = fbit.getIntensity();
	    }
	  else if(fbit.getIntensity() != 0 && run.colour == 0)
	    {
	      // start a run
	      run.col = col;
	      run.colour = fbit.getIntensity();
	    }
	  fbit.goRight();
	} // end for each pixel in a row
      // store end-of-row runs
      if(run.colour != 0)
	{
	  run.length = frame->width - run.col;
	  runList.push_back(run);
	  // std::cout << "storing: " << run;
	} // end if

    } // end for each row.
  
  //  std::cout << "rle exit\n";
  return runList;
}
#endif
