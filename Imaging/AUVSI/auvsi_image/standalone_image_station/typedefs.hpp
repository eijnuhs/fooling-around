#ifndef TYPEDEFS_H
#define TYPEDEFS_H

#include <vector>
#include <vigra/tinyvector.hxx>
#include <vigra/edgedetection.hxx> // edgel.
#include <vigra/multi_array.hxx> 
#include <inttypes.h>
#include <umbotica/math/radian.hpp>

// tiny vector is a full featured vector class.
// this creates a two-component version for use
// as a point.  Members are accessed using [].
typedef vigra::TinyVector<int,2> ControlPoint2;
typedef vigra::TinyVector<int,2> Point2D;
typedef vigra::TinyVector<float,2> StepPoint;
typedef vigra::TinyVector<double,2> Point2Df;
typedef vigra::TinyVector<double,2> GPSPoint;
typedef std::vector<ControlPoint2> CPList;
typedef std::vector<vigra::Edgel> EdgelVector;
typedef vigra::MultiArray<3,float> HoughAllocator;
typedef HoughAllocator::difference_type AllocatorIdx;

typedef vigra::MultiArray<3,uint16_t> HoughAccumulator;
typedef HoughAccumulator::difference_type AccumulatorIdx;


struct TargetLocation
{
  Point2Df gpsMeters;
  Radian orientation;

  TargetLocation():gpsMeters(0, 0), orientation(0){}
  TargetLocation(double longitude, double latitude, Radian orientation) :
    gpsMeters(longitude, latitude), orientation(orientation){}
  TargetLocation(Point2Df gpsMeters, Radian orientation) :
    gpsMeters(gpsMeters), orientation(orientation){}
};

#endif
