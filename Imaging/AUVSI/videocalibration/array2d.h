#ifndef __ARRAY_2D_H
#define __ARRAY_2D_H

#include <iostream>
#include <ostream>

// a template class for a  
// dynamically initialized 2d array
// with boundary checking and an
// iterator (iterator not yet available.

template<class Ttype>
class Array2D
{
 public:
  Array2D():
    array(0),
    height(0),
    width(0)
    {
      // 0 
    }
  Array2D(unsigned int height, unsigned int width):
    array(0),
    height(height),
    width(width)
    {
      initialize();
    }

  Array2D(unsigned int height, unsigned int width, const Ttype & initVal):
    array(0),
    height(height),
    width(width)
    {
      initialize(initVal);
    }

  Array2D(const Array2D & rhs)
    {
      copyArray(rhs);
    }
  
  ~Array2D()
    {
      deleteArray();
    }

  Array2D & operator= (const Array2D & rhs)
    {
      copyArray(rhs);
      return *this;
    }

  inline void set(unsigned int row, unsigned int col, const Ttype & value)
  {
    if(col < width &&  row < height)
      {
	getItemRef(row,col) = value;
      }
    else
      {
	std::cerr << "Array2D::set() attempted to set out of range\n";
	std::cerr << "Array size: (" << height << "," << width << ")\n";
	std::cerr << "Attempted to set: (" << col << "," << row << ")\n";
      }
  }
  
  inline Ttype get(unsigned int row, unsigned int col) const
  {
    if( col < width &&  row < height)
      {
	return getItemRef(row,col);
      }
    else
      {
	std::cerr << "Array2D::get() attempted to query out of range\n";
	std::cerr << "Array size: (" << height << "," << width << ")\n";
	std::cerr << "Attempted to query: (" << col << "," << row << ")\n";
	return Ttype();
      }
    
  }

  void initialize()
    {
      if(width != 0 && height != 0)
	{
	  if(array)
	    {
	      delete [] array;
	    }
	  array = new Ttype[width * height];
	}
    }

  void initialize(unsigned int height, unsigned int width)
    {
      this->height = height;
      this->width  = width;
      initialize();
    }

  void initialize(const Ttype & initVal)
    {
      initialize();
      Ttype * pointer = array;
      unsigned int arraySize = height * width;
      for(unsigned int i = 0; i < arraySize; ++i)
	{
	  *pointer = initVal;
	  ++pointer;
	}
    }
  
  void initialize(unsigned int height, unsigned int width, 
		  const Ttype & initVal)
    {
      this->height = height;
      this->width = width;
      initialize(initVal);
    }

  void initialize(unsigned int topLeftRow, unsigned int topLeftCol,
		  unsigned int height, unsigned int width, 
		  const Ttype & initVal)
    {
      Ttype * pointer;
      unsigned int botRow = topLeftRow+height;
      unsigned int rightCol = topLeftCol + width;
      if( (topLeftRow + height) <= this->height &&
	  (topLeftCol + width)  <= this->width    )
	{
	  // the range is within bounds
	  for(unsigned int row = topLeftRow; row < botRow; ++row)
	    {	  
	      pointer = &getItemRef(row,topLeftCol);
	      for(unsigned int col = topLeftCol; col < rightCol; ++col)
		{
		  *pointer = initVal;
		  pointer++;
		}
	    }
	  
	}
      else
	{
	  // the range is out of bounds
	  std::cerr << "Array2D::set() attempted to initialize out of range\n";
	  std::cerr << "Array size: (" << this->height << "," << this->width << ")\n";
	  std::cerr << "Attempted to initialize topLeft: (" << topLeftRow << "-" << topLeftRow + height - 1 
		    << "," << topLeftCol << "-" << topLeftCol + width - 1 << ")\n";
	}
    }

  unsigned int getHeight()
    {
      return height;
    }
  
  unsigned int getWidth()
    {
      return width;
    }

void print(char * header = 0, char * rowDelim = "\n", char * colDelim = "\t")
    {
      Ttype * pointer = array;
      if(pointer)
	{
	  if(header)
	    std::cout << header << "\n";
	  for(unsigned int i = 0; i < height; ++i)
	    {
	      for(unsigned int j = 0; j < width; ++j) 
		{
		  std::cout << (*pointer) << colDelim;
		  pointer++;
		}
	      std::cout << rowDelim;
	    }
	}
      std::cout << "\n";
    }

 private:
  Ttype * array;
  unsigned int height;
  unsigned int width;

  inline Ttype & getItemRef(unsigned int row, unsigned int col) const
  {
    return array[row*width + col];
  }  

  void copyArray(const Array2D & rhs)
    {
      unsigned int arraySize;
      Ttype * thisPtr;
      Ttype * rhsPtr;
      if(array)
	{
	  deleteArray();
	}
      height = rhs.height;
      width = rhs.width;
      if(height == 0 || width == 0)
	{
	  array = 0;
	}
      else
	{
	  arraySize = height*width;
	  array = new Ttype[arraySize];
	  thisPtr = &array[0];
	  rhsPtr  = &rhs.array[0];
	  for(unsigned int i = 0; i < arraySize; ++i)
	    {
	      *thisPtr = *rhsPtr;
	      thisPtr++;
	      rhsPtr++;
	    }
	}
    }

  inline void deleteArray()
    {
      delete [] array;
    }
  
}; // end class



#endif
