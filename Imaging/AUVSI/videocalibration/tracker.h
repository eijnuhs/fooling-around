#ifndef __TRACKER_H_
#define __TRACKER_H_

#include <vector>
#include "trackable.h"
#include "robot.h"
#include <qobject.h>


/// Forward Declarations ///
class RunRegion;
class FrameBuffer;
class RegionList;
class FrameBufferWorld;
class TrackerConfig;
class Segmentation;
class TrackerViewer;
class Broadcaster;
class Ball;

class Tracker : public QObject
{
    public:
  Tracker(TrackerConfig * tcfg);
  ~Tracker();

  void processFrame(FrameBuffer * inFrame);
  // note: Regions store coordinates of pixels within the worldFrame.
  //       The inframe is the raw camera image.
  void addData(RegionList & regionList, FrameBuffer * inFrame, FrameBufferWorld * worldFrame);
  void setSegmentation(Segmentation * init_seg);
  void setBroadcaster(Broadcaster * init_bcast);
  std::vector<Trackable *> trackable;

 private:
  Segmentation * seg;
  Broadcaster * bcast;
  friend class TrackerConfig;
  friend class TrackerViewer;
  int updateBackgroundCount;
  Ball * theBall;
};

#endif
