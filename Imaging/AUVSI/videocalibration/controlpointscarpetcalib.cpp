// $Id: controlpointscarpetcalib.cpp,v 1.1.2.2 2004/10/08 14:27:26 cvs Exp $
//

using namespace std;

#include "rotationmatrix.h"
#include "calibration.h"
#include "controlpoints.h"
#include "controlpointscarpetcalib.h"
#include "geometry.h"

ControlPointsCarpetCalib::ControlPointsCarpetCalib (double fwidth,
						    double fheight)
{
  ControlPoint p(0.0,0.0,0.0,0.0,0.0,0.0);

  p.xw = 0.0;
  p.yw = 0.0;
  p.zw = 0.0;
  p.xm = 10.0;
  p.ym = fheight - 10.0;
  p.zm = 0.0;
  points.push_back (p);

  p.xw = 0.0;
  p.yw = 0.0;
  p.zw = 0.0;
  p.xm = fwidth - 10.0;
  p.ym = fheight - 10.0;
  p.zm = 0.0;
  points.push_back (p);

  p.xw = 0.0;
  p.yw = 0.0;
  p.zw = 0.0;
  p.xm = fwidth - 10.0;
  p.ym = 10.0;
  p.zm = 0.0;
  points.push_back (p);

  p.xw = 0.0;
  p.yw = 0.0;
  p.zw = 0.0;
  p.xm = 10.0;
  p.ym = 10.0;
  p.zm = 0.0;
  points.push_back (p);
}

void ControlPointsCarpetCalib::clamp(int width, int height)
{
  for(uint i = 0; i < points.size(); ++i)
    {
      points[i].xm = Geometry::clamp(0, (int)rint(points[i].xm), width - 1);
      points[i].ym = Geometry::clamp(0, (int)rint(points[i].ym), height - 1);
    }
}
