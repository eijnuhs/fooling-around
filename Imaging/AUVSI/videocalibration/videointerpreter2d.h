// $Id: videointerpreter2d.h,v 1.1.1.1.2.12 2005/01/27 02:33:46 cvs Exp $

#ifndef _VIDEO_INTERPRETER_2D
#define _VIDEO_INTERPRETER_2D

#include "camera.h"
#include "tsaiplanarcalibration.h"
#include <string>
#include "rotationmatrix.h"
#include "framebufferrgb32.h"
#include <qobject.h>
#include "framebufferworld.h"

#define MAP_DIMENSION 512

enum COLOUR_SPACES {
  CS_RGB = 0,
  CS_NRGB
 };

// Forward declarations
class VideoStream;
class RawPixel;
class Pixel;
class Mask;
class ContolPoint;
class VideoInterpreter2DConfig;

class VideoInterpreter2D : QObject
{
  Q_OBJECT

 public:
  VideoInterpreter2D( VideoStream * vs, std::string cameraFilename, std::string calibrationPointsFilename , std::string rotationMatrixFilename );
  VideoInterpreter2D( VideoStream * vs, VideoInterpreter2DConfig * vi2dc );
  ~VideoInterpreter2D( );
  void worldToScreenCoordinates( double x, double y, double z, int * ix, int * iy, bool useMap = true );
  void worldToScreenCoordinates( double x, double y, double z, double * dix, double * diy, bool useMap = true );
  void screenToWorldCoordinates( double const ix, double const iy, double const z, double * const x, double * const y, bool useMap = true );
  void screenToWorldCoordinates( unsigned int const ix, unsigned int const iy, double const z, double * const x, double * const y, bool useMap = true );
  bool interpolatePixel( FrameBuffer const * frame, double const x, double const y, double const z, RawPixel * pixel);
  bool interpolatePixelBlur( FrameBuffer const * frame, double const x, double const y, double const z, RawPixel * pixel);
  void interpolateFrame( FrameBuffer const * inFrame, FrameBuffer * outFrame);
  void interpolateFrame ( FrameBuffer const * inFrame, FrameBufferWorld * worldFrame, bool blur = false);
  void interpolateFrame ( FrameBuffer const * inFrame, FrameBufferWorld * worldFrame, unsigned int row, unsigned int col, unsigned int endRow, unsigned int endCol, bool blur = false);
  unsigned int getWidth( void ) const;
  unsigned int getHeight( void ) const;
  //  void createWorldView( FrameBuffer * frame, unsigned int * regionMap, unsigned int regionMapWidth, unsigned int regionMapHeight, unsigned int nsteps );
  void loadControlPoints(std::string filename);
  void updateWorldToScreenMap( void );
  void updateScreenToWorldMap( void );
  unsigned int getFrameHeight( void );
  unsigned int getFrameWidth( void );
  void loadRotation(std::string filename);
  void saveRotation(std::string filename);
  void resetControlPoints( void );
  std::string getRotationFilename( void );


  VideoStream * vs;
  TsaiPlanarCalibration calibration;
  RotationMatrix rotationMatrix;
  PlayingField field;

  Mask * mask;

  //  FrameBufferWorld wvFrame;

 signals:
  void newFrame( FrameBuffer * frame );

 private:
  void init();
  struct Mapping { double x; double y; }; 


  struct Mapping (*worldMap)[ MAP_DIMENSION ];
  struct Mapping *screenMap;

  double xFactor;
  double yFactor;

  unsigned int fwidth;
  unsigned int fheight;

};

#endif

