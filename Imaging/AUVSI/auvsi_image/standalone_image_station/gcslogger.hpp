#ifndef _GCSLOGGER_H
#define _GCSLOGGER_H

#include <iostream>
#include <string>
#include <fstream>
#include <exception>
#include "serialReader.h"
#include "sqlinterface.hpp"
#include "boost/date_time/posix_time/posix_time.hpp"
#include "boost/format.hpp"
#include "boost/program_options.hpp"
#include <cstring>
#include <vigra/tinyvector.hxx>
#include "telemetry.hpp"

using namespace boost::posix_time;
using namespace boost::gregorian;
using boost::format;
using namespace std;


const double gcsPI = 3.14159265;

/*!
    @const		HEADER_LENGTH
    @abstract   Number of bytes to receive before actual data is read
    @discussion This includes the START_OF_PACKET byte, the sequence number, and the RESPONSE_OK byte
*/
const int HEADER_LENGTH = 3;

/*!
    @const		MAX_NUM_FIELDS
    @abstract   The maximum number of fields possible to recieve in one packet.
    @discussion This is based on the Micropilot documentation which lists no more than fields that
				could possibly be requested
*/
const int MAX_NUM_FIELDS = 60;

/*!
    @const		BYTES_PER_FIELD
    @abstract   Number of bytes to desribe a field's value
    @discussion 4 bytes are needed to represent the 32 bit signed integer
*/
const int BYTES_PER_FIELD = 4;

/*!
    @const		BYTES_PER_ESCAPE
    @abstract   Number of bytes taken by escaped bytes
    @discussion 
*/
const int BYTES_PER_ESCAPE = 2;

/*!
    @const		END_LENGTH
    @abstract   Number of bytes to recieve after the data fields
    @discussion This includes the checksum and the END_OF_PACKET bytes
*/
const int END_LENGTH = 2;

/*!
    @const		MAX_MESSAGE_LENGTH
    @abstract   Number of bytes sent in the worst case
    @discussion 
*/
const int MAX_MESSAGE_LENGTH = (HEADER_LENGTH + MAX_NUM_FIELDS * BYTES_PER_FIELD * + END_LENGTH) * BYTES_PER_ESCAPE;

/*!
  @class		GCSLogger
  @abstract   Driver/wrapper class for serialReader.c
  @discussion Opens a serial connection, reads incoming stream, validates, interprets, and writes data to file/database
  using the supplied Writer. The Writer must expose a writeTelemetry(struct telemetry) function.
*/
template <class Writer>
class GCSLogger
{	
public:
  /*!
    @function   GCSLogger
    @abstract   Constructor
    @discussion Opens a file as a serial device, as well as a file for log output
    @param      fileName The file to open as a serial device
    @param		logFileName The file to open as a log file
  */
  GCSLogger(std::string fileName, std::string logFileName);
  /*!
    @function   ~GCSLogger
    @abstract   Destructor
    @discussion Closes all files
  */
  ~GCSLogger();
  /*!
    @function   setWind
    @abstract   Sets the windspeed and direction for heading estimation from track
  */
  void setWind( int degrees, int knots );
  /*!
    @function   recordNextPacket
    @abstract   Logs serial telemetry data to file
    @discussion Reads in a packet from the serial stream bounded by start and end bytes,
    validates the data, interprets the fields, and writes them to file
    with a timestamp
  */
  void recordNextPacket();
  /*!
    @function   mostRecentTelemetry
    @abstract   Gets the last telemetry packet read
    @discussion 
  */
  struct telemetry mostRecentTelemetry();
  /*!
    @function   okay
    @abstract   Confirms proper construction
    @discussion Returns true if raw log file and input device opened properly
  */
  bool okay();
  
private:
  Modem modem;
  std::ofstream logFile;
  bool filesOpened;	
  
  int longitude;
  int latitude;
  int pitch;
  int roll;
  int altitude;
  int as;
  int gs;
  int vsi;
  double heading;
  int track;
  int status;
  int throttle;
  char prevCounter;

  vigra::TinyVector<double, 2> windVector;

  struct telemetry lastT;
  
  Writer* writer;
  
  void writeRaw(std::ofstream* logFile, unsigned char * buffer, int len);
  bool analyzeTelemetry(unsigned char * message, int messageLength);
  bool analyzeFeedbackTelemetry(unsigned char * message, int messageLength);
  bool validChecksum(unsigned char * message, int begin, int end, unsigned char checksum);
  bool correctNumBytes(unsigned char * message, int length);
  bool correctNumBytesFeedback(unsigned char * message, int length);
  void unescape(unsigned char * message, int len, unsigned char * unescaped);
  void logTelemetry(unsigned char * message);
  void logFeedback(unsigned char * message);
};

template <class Writer>
GCSLogger<Writer>::GCSLogger(string fileName, string logFileName)
{
  longitude = 0;
  latitude = 0;
  pitch = 0;
  roll = 0;
  gs = 0;
  altitude = 0;
  vsi = 0;
  heading = 0;
  track = 0;
  status = 0;
  throttle = 0;
  prevCounter = 0;
	
  ModemParams params;

  params.baudrate = B9600;
  strcpy(params.modemDevice, fileName.c_str());
  filesOpened = true;
  
  if (openModem(&modem, params) <  0)
    {
      cout << "Error opening device...\n";
      filesOpened = false;
    }
  else
    {
      cout << "Device opened...\n";
    }
  
  logFile.open(logFileName.c_str(), ios::out | ios::app );
  cout << "Log file opened...\n";
 
  try
    {
      writer = new Writer();
    }
  catch (std::exception & e)
    {
      std::cerr << "Writer could not be created" << std::endl;
      throw e;
    }
}

template <class Writer>
GCSLogger<Writer>::~GCSLogger()
{
  if (!closeModem(&modem))
    {  
      cout << "Device closed.\n";
    }
  else
    {
      cout << "Error closing device.\n";
    }
  
  logFile.close();
  delete(writer);
}

template <class Writer>
void GCSLogger<Writer>::setWind( int direction, int knots )
{
  double windSpeedFps = 1.68780986 * knots;
  direction = direction - 180;
  if (direction < 0)
    {
      direction += 360;
    }
  double windDirection = direction * M_PI / 180;
  windVector[0] = windSpeedFps * cos(windDirection);
  windVector[1] = windSpeedFps * sin(windDirection);
}

template <class Writer>
void GCSLogger<Writer>::recordNextPacket()
{
  unsigned char message[MAX_MESSAGE_LENGTH];
  cout << "Getting next packet...\n";
  int len;
  len = readMessage(&modem, message, sizeof(message));
	
  bool valid = analyzeTelemetry(message, len);
  writeRaw(&logFile, message, len);
  if ( valid )
    {
      unsigned char unescaped[20];
      unescape(message, len, unescaped);
      logTelemetry(unescaped);
      cout << "Logged to file.\n";
    } 
  else if (analyzeFeedbackTelemetry(message, len))
    {
      unsigned char unescaped[20];
      unescape(message, len, unescaped);
      logFeedback(unescaped);
      cout << "Logged feedback.\n";
    }
  else
    {
      cout << "Invalid telemetry data.\n";
    }
}

template <class Writer>
void GCSLogger<Writer>::writeRaw(ofstream * logFile, unsigned char * buffer, int len)
{
  ptime now = microsec_clock::local_time();
  std::string dateString = to_iso_string(now);
  dateString += " >> ";
	
  *logFile << dateString;
	
  for (int i = 0; i < len; i++)
    {
      *logFile << (void*)((int)buffer[i]) << " ";
    }
  
  *logFile << "\n";
  (*logFile).flush();
}

template <class Writer>
bool GCSLogger<Writer>::okay()
{
  return filesOpened;
}

template <class Writer>
bool GCSLogger<Writer>::analyzeTelemetry(unsigned char * message, int messageLength)
{
  bool valid = true;
  
  if (messageLength < 20)
    {
      cout << "message too small\n";
      valid = false;
    }
  
  if (message[0] != START_OF_PACKET)
    {
      cout << "message missing start of packet\n";
      valid = false;
    }
  
  if (message[1] != RESPONSE_OK)
    {
      cout << "not a telemetry packet\n";
      valid = false;
    }
  
  if (message[messageLength-1] != END_OF_PACKET)
    {
      cout << "message missing end of packet\n";
      valid = false;
    }
  
  if (!validChecksum(message, 1, messageLength-3, message[messageLength-2]))
    {
      cout << "checksum INVALID\n";
      valid = false;
    }
  
  if ( !correctNumBytes(message, messageLength) )
    {
      cout << "incorrect number of bytes\n";
      valid = false;
    }
  
  return valid;
}

template <class Writer>
bool GCSLogger<Writer>::analyzeFeedbackTelemetry(unsigned char * message, int messageLength)
{
  bool valid = true;

  std::cout << "analyzing Telemetry " << std::endl;
  
  if (messageLength < 16)
    {
      cout << "feedback message too small\n";
      valid = false;
    }
  
  if (message[0] != START_OF_PACKET)
    {
      cout << "message missing start of packet\n";
      valid = false;
    }
  
  if (message[messageLength-1] != END_OF_PACKET)
    {
      cout << "message missing end of packet\n";
      valid = false;
    }
  
  if (!validChecksum(message, 1, messageLength-3, message[messageLength-2]))
    {
      cout << "checksum INVALID\n";
      valid = false;
    }
  
  if ( !correctNumBytesFeedback(message, messageLength) )
    {
      cout << "feedback message: incorrect number of bytes\n";
      valid = false;
    }
  
  return valid;
}

template <class Writer>
bool GCSLogger<Writer>::correctNumBytes(unsigned char * message, int length)
{
  bool correct = true;
  unsigned int escapeCount = 0;
	
  for (int i = 0; i < length; i++)
    {
      if (message[i] == ESCAPE_CHAR)
	{
	  escapeCount++;
	}
    }
  
  if (length - escapeCount != 20)
    {
      correct = false;
    }
  
  return correct;
  
}

template <class Writer>
bool GCSLogger<Writer>::correctNumBytesFeedback(unsigned char * message, int length)
{
  bool correct = true;
  unsigned int escapeCount = 0;
	
  for (int i = 0; i < length; i++)
    {
      if (message[i] == ESCAPE_CHAR)
	{
	  escapeCount++;
	}
    }

  std::cout << length << "\n";
  
  if (length - escapeCount != 21)
    {
      correct = false;
    }
  
  return correct;
  
}

template <class Writer>
bool GCSLogger<Writer>::validChecksum(unsigned char * message, int begin, int end, unsigned char checksum)
{
  unsigned char sum = 0;
  
  for (int i = begin; i <= end; i++)
    {
      sum += message[i];
    }
  
  return (sum == checksum);
}

template <class Writer>
void GCSLogger<Writer>::unescape(unsigned char * message, int len, unsigned char * unescaped)
{
  int index = 0;
  
  for (int i = 0; i < 20; i++)
    {
      if (message[index] != ESCAPE_CHAR)
	{
	  unescaped[i] = message[index++];
	}
      else
	{
	  index++; 
	  switch(message[index])
	    {
	    case ESCAPE_0:
	      unescaped[i] = CONVERT_0;
	      break;
	    case ESCAPE_1:
	      unescaped[i] = CONVERT_1;
	      break;
	    case ESCAPE_2:
	      unescaped[i] = CONVERT_2;
	      break;
	    }
	  index++;
	}
    }
}

template <class Writer>
void GCSLogger<Writer>::logTelemetry(unsigned char * message)
{
  unsigned char longitudeBytes[4];
  unsigned char latitudeBytes[4];
  unsigned char pitchByte;
  unsigned char rollByte;
  unsigned char asByte;
  unsigned char gsByte;
  unsigned char altitudeBytes[2];
  unsigned char vsiByte;
  unsigned char trackByte;
  unsigned char statusByte;
  unsigned char throttleByte;

  vigra::TinyVector<double, 2> trackVector;
  vigra::TinyVector<double, 2> headingVector;
	
  ptime now = microsec_clock::local_time();
	
  pitchByte = message[4];
  rollByte = message[5];
  
  switch(message[3])
    {
    case 0:
      std::cout << "telem_0\n";
      statusByte = message[8];
      status = statusByte;
      asByte = message[6];
      as = asByte * 4;
      gsByte = message[7];
      gs = gsByte * 4;
			
      break;
    case 1:
      std::cout << "telem_1\n";
      altitudeBytes[0] = message[6];
      altitudeBytes[1] = message[7];
      altitude = *(reinterpret_cast<short int*>(altitudeBytes)) * -8;
      throttleByte = message[8];
      throttle = throttleByte;
			
      break;
    case 2:
      std::cout << "telem_2\n";
      longitudeBytes[0] = message[6];
      longitudeBytes[1] = message[7];
      longitudeBytes[2] = message[8];
      longitudeBytes[3] = message[9];
      longitude = *(reinterpret_cast<int*>(longitudeBytes));
			
      break;
    case 3:
      std::cout << "telem_3\n";
      latitudeBytes[0] = message[6];
      latitudeBytes[1] = message[7];
      latitudeBytes[2] = message[8];
      latitudeBytes[3] = message[9];
      latitude = *(reinterpret_cast<int*>(latitudeBytes));
			
      break;
    case 4:
      std::cout << "telem_4\n";
      vsiByte = message[6];
      vsi = (char)vsiByte;
      trackByte = message[7];
      track = trackByte * 256;

      trackVector[0] = gs * cos(((double)track/100.0)*M_PI/180.0);
      trackVector[1] = gs * sin(((double)track/100.0)*M_PI/180.0);

      headingVector = trackVector - windVector;
      heading = atan2(headingVector[1], headingVector[0]);
      heading = (heading / M_PI) * 180;
      if (heading < 0)
	{
	  heading += 360;
	}

      break;
    default:
			
      break;
    };
  
  pitch = (char)pitchByte * 32;
  roll = (char)rollByte * 32;
  
  struct telemetry t(now,
		     (180 * (((double)longitude/100000000.0)/5.0) / M_PI), // longitude east
		     (180 * (((double)latitude/100000000.0)/5.0) / M_PI), // latitude north
		     (float)pitch/1024, // pitch in radians
		     (float)roll/1024, // roll in radians
		     as, // airspeed
		     gs, // groundspeed
		     altitude / -8, // altitude in feet
		     vsi, // vsi in fps
		     (double)heading/100.0, // heading approximation accounting for wind
		     (double)track/100.0, // the track in degrees
		     status);
  memcpy(&lastT, &t, sizeof(struct telemetry));
  
  writer->writeTelemetry(&lastT);
  
  return;
}

template <class Writer>
void GCSLogger<Writer>::logFeedback(unsigned char * message)
{
  ptime now = microsec_clock::local_time();

  printf("0x%02x", message[15]);
  if (prevCounter != message[15])
    {
      prevCounter = message[15];
      writer->writeSnapshot(message[15], now);
    }
  
  return;
}

template <class Writer>
telemetry GCSLogger<Writer>::mostRecentTelemetry()
{
  return lastT;
}


#endif
