#include "videointerpreter2dconfig.h"
#include "videointerpreter2d.h"
#include <iostream>


VideoInterpreter2DConfig::VideoInterpreter2DConfig() :
  Configuration("VideoInterpreter2D")
{

}

VideoInterpreter2DConfig::~VideoInterpreter2DConfig()
{

}

ControlPoints VideoInterpreter2DConfig::getControlPoints(VideoInterpreter2D * vi2d)
{
  ControlPoints cpList;
  int xw,yw, xm, ym;
  Node * temp;
  
  printf("getControlPoints\n");
  // get a vector of control point nodes
  std::vector<Node *> nodeList( getNodeList("ControlPoint") );
  
  for(unsigned int i = 0; i < nodeList.size(); ++i)
    {
      temp = config->findNode("x",nodeList[i]);
      xw = getInt(temp,0);
      temp = config->findNode("y",nodeList[i]);
      yw = getInt(temp,0); 
      vi2d->worldToScreenCoordinates ((double)xw, (double)yw, 0.0, &xm, &ym);  
      ControlPoint p(xw,yw,0,xm,ym,0);
      cpList.addPoint(p);
    }
  

  return cpList;
}

/*
VideoInterpreter2D::VideoInterpreter2D( VideoStream * vs, string cameraFilename, string calibrationPointsFilename, string rotationMatrixFilename ) :
  vs( vs ),
  calibration( cameraFilename, calibrationPointsFilename ),
  rotationMatrix( rotationMatrixFilename ),
  field( 2740.0, 1520.0, 200.0 ),
  worldMap( 0 ),
  screenMap( 0 ),
  mask(0)
{

  xFactor = ( 2 * field.margin + field.width ) / MAP_DIMENSION;
  yFactor = ( 2 * field.margin + field.height ) / MAP_DIMENSION;

  fwidth = vs->getVideoDevice()->getWidth();
  fheight = vs->getVideoDevice()->getHeight() * (vs->isInterlaced() ? 1 : 2);

#ifdef DEBUG_PTF
  std::cout << "vi2d viewer (w,h): ("<< fwidth << "," << fheight << ")\n";
#endif

  updateScreenToWorldMap();
  updateWorldToScreenMap();

  vs->setVideoInterpreter2D( this );
  // 1 pixel per cm 
  wvFrame.initialize( 640, 480, field );
  
  loadControlPoints( CTL_PTS_FILENAME );
  //  rotationMatrix.computeRotationMatrix(&calibration);

}
*/
