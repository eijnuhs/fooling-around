#include <iostream>
#include "auvsi_calibration.hpp"
#include <vigra/matrix.hxx>
#include <vigra/linear_algebra.hxx>

AuvsiCalibration* AuvsiCalibration::instance = 0;

void AuvsiCalibration::initialize(CameraType selector, int width, int height, std::string pointsFile)
{
  if (selector == LowRes && !lowResCalibration)
    {
      Camera* cam = new Camera(width,height);
      lowResCalibration = new TsaiPlanarCalibration(*cam, pointsFile);

      lowResTable = new vigra::TinyVector<double, 3>* [height];
      for (int i = 0; i < height; i++)
	{
	  lowResTable[i] = new vigra::TinyVector<double, 3> [width];
	}

      // initialize calibration table for lowres camera
      for (int row = 0; row < height; row++)
	{
	  for (int col = 0; col < width; col++)
	    {
	      double cameraX, cameraY;

	      // Get the projection into camera coordinates at distance 1000 from focal point
	      // x = col; y = row
	      lowResCalibration->imageCoordToCameraCoord(col, row, 1000, &cameraX, &cameraY);
	      lowResTable[row][col] = vigra::TinyVector<double, 3>(cameraX, cameraY, 1000);
	    }
	}
    }
  else if (selector == LowRes)
    {
      std::cout << "2nd initialization of LowRes camera calibration attempted." << std::endl;
    }
  if (selector == HighRes && !highResCalibration)
    {
      Camera* cam = new Camera(width,height);
      highResCalibration = new TsaiPlanarCalibration(*cam, pointsFile);

      highResTable = new vigra::TinyVector<double, 3>* [height];
      for (int i = 0; i < height; i++)
	{
	  highResTable[i] = new vigra::TinyVector<double, 3> [width];
	}

      // initialize calibration table for highres camera
      for (int row = 0; row < height; row++)
	{
	  for (int col = 0; col < width; col++)
	    {
	      double cameraX, cameraY;

	      // Get the projection into camera coordinates at distance 1000 from focal point
	      // x = col; y = row
	      highResCalibration->imageCoordToCameraCoord(col, row, 1000, &cameraX, &cameraY);
	      highResTable[row][col] = vigra::TinyVector<double, 3>(cameraX, cameraY, 1000);
	    }
	}

    }
  else if (selector == HighRes)
    {
      std::cout << "2nd initialization of HighRes camera calibration attempted." << std::endl;
    }
}

GpsCoord AuvsiCalibration::gpsCoordFromImageCoord(CameraType selector, int imageX, int imageY,
						  double roll, double pitch, double yaw, 
						  GpsCoord gpsPosition, double altitudeMetres)
{
  // Rotations passed relative to belly-down, nose north, wings level.
  // Must add pi to roll to make it relative to camera-upward
  roll += M_PI;
  // Also must reverse signs of pitch and yaw to make signage match micropilot.
  // (+ roll = right wing low, +yaw = clockwise from north)
  yaw *= -1;
  pitch *= -1;

  vigra::TinyVector<double, 3> unrotatedWorldCoord;

  // For clarity
  int row = imageY;
  int col = imageX;

  if ( selector == HighRes )
    {
      if (highResTable == 0)
	{
	  std::cerr << "AuvsiCalibration: highResTable not initialized" << std::endl;
	}
      unrotatedWorldCoord = highResTable[row][col];
    }
  else
    {
      if (lowResTable == 0)
	{
	  std::cerr << "AuvsiCalibration: lowResTable not initialized" << std::endl;
	}
      unrotatedWorldCoord = lowResTable[row][col];
    }

  vigra::Matrix<double> rYaw(3,3);
  vigra::Matrix<double> rPitch(3,3);
  vigra::Matrix<double> rRoll(3,3);

  // Rotation around z (vertical) axis
  rYaw(0,0) = cos(yaw); rYaw(0,1) = -sin(yaw); rYaw(0,2) = 0;
  rYaw(1,0) = sin(yaw); rYaw(1,1) =  cos(yaw); rYaw(1,2) = 0;
  rYaw(2,0) =        0; rYaw(2,1) =         0; rYaw(2,2) = 1;

  // Rotation around x (east-west) axis
  rPitch(0,0) = 1; rPitch(0,1) =          0; rPitch(0,2) =           0;
  rPitch(1,0) = 0; rPitch(1,1) = cos(pitch); rPitch(1,2) = -sin(pitch);
  rPitch(2,0) = 0; rPitch(2,1) = sin(pitch); rPitch(2,2) =  cos(pitch);
  
  // Rotation around y (north-south) axis
  rRoll(0,0) = cos(roll); rRoll(0,1) =  0; rRoll(0,2) = -sin(roll);
  rRoll(1,0) =         0; rRoll(1,1) =  1; rRoll(1,2) =          0;
  rRoll(2,0) = sin(roll); rRoll(2,1) =  0; rRoll(2,2) =  cos(roll);

  vigra::TinyVector<double, 3> rotatedWorldCoord = rYaw * rPitch * rRoll * unrotatedWorldCoord;
  
  vigra::TinyVector<double, 2> offsetInMetres = gpsPosition.inMetres();
  vigra::TinyVector<double, 3> positionInMetres;
  positionInMetres[0] = offsetInMetres[0];
  positionInMetres[1] = offsetInMetres[1];
  positionInMetres[2] = altitudeMetres;

  // Let positionInMetres be point P.
  // Let rotatedWorldCoord be vector v.
  // Solve for intersection of line "P + t*v" with the plane "z=0".
  
  double t = positionInMetres[2] / rotatedWorldCoord[2];
  
  double xInt = positionInMetres[0] + t * rotatedWorldCoord[0]; // east-west... longitude value
  double yInt = positionInMetres[1] + t * rotatedWorldCoord[1]; // north-south... latitude value

  // Must make this offset back into a gps coord for the return
  GpsCoord gps = GpsCoord::gpsCoordFromMetres( xInt, yInt );

  return gps;
}
