// $Id: region.h,v 1.1.1.1 2004/09/16 23:12:05 cvs Exp $
// Region segmentation 
// Jacky Baltes <jacky@cs.umanitoba.ca> Sat May 29 21:16:03 CDT 2004
//
//

#ifndef _REGION_H_
#define _REGION_H_

#include "controlpoints.h"
#include <list>
#include "pixel.h"
#include <ostream>

// Forward declarations

class SegmentationRegion;

class ScanLine
{
 public:
  ScanLine( ControlPoint start, ControlPoint end, Pixel pix );
  bool adjacent( ScanLine const & s, double threshold = 1.0 );

  ControlPoint start;
  ControlPoint end;
  double wLength;
  double mLength;

  SegmentationRegion * region;
  Pixel color;
};

class SegmentationRegion
{
 public:
  SegmentationRegion( );
  SegmentationRegion( ScanLine s );
  void merge( SegmentationRegion * s2 );

  bool attachScanLine( ScanLine const & s, double threshold );

  double xMExtension( void ) const;
  double yMExtension( void ) const;

  double xWExtension( void ) const;
  double yWExtension( void ) const;

  double mRatio( void ) const;
  double wRatio( void ) const;

  double bboxMArea( ) const;
  double bboxWArea( ) const;

  unsigned int numPoints;

  double sumMX;
  double sumMY;

  double sumWX;
  double sumWY;

  Pixel color;

  ControlPoint bboxMin, bboxMax;
  ControlPoint center;

  bool null;
  int num;

  friend std::ostream & operator<<( std::ostream & os, SegmentationRegion & r );

 private:
};


#endif
