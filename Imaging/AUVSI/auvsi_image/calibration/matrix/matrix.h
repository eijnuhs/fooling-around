/* matrix.h -- define types for matrices using Iliffe vectors
 *
 *************************************************************
 * HISTORY
 *
 * 02-Apr-95  Reg Willson (rgwillson@mmm.com) at 3M St. Paul, MN
 *      Rewrite memory allocation to avoid memory alignment problems
 *      on some machines.
 */

#ifndef _MATRIX_H_
#define _MATRIX_H_

typedef struct {
    int       lb1,
              ub1,
              lb2,
              ub2;
    char     *mat_sto;
    double  **el;
} dmat;

#ifdef __cplusplus
extern "C" {
#endif
  void      print_mat (dmat mat);
  dmat      newdmat (int rs, int re, int cs, int ce, int *error);
  int       matmul (dmat a, dmat b, dmat c);
  int       matcopy (dmat A, dmat RSLT);
  int       transpose (dmat A, dmat ATrans);
  double    matinvert (dmat a);
  int       solve_system (dmat M, dmat a, dmat b);
  void      freemat(dmat mat);
#ifdef __cplusplus
}
#endif

#endif
