#ifndef __TRACKER_VIEWER_H_
#define __TRACKER_VIEWER_H_
#include "framebufferviewer.h"

class Tracker;
class VideoInterpreter2D;

class TrackerViewer : public FrameBufferViewer
{
 public:
  TrackerViewer( QWidget * parent = 0, char const * name = 0 );
  ~TrackerViewer();
  void paintChild(QPainter & p);
  void setTracker(Tracker * tracker);
  void setVideoInterpreter(VideoInterpreter2D * init_vi2d);
 private:
  Tracker * tracker;
  VideoInterpreter2D * vi2d;
};

#endif

