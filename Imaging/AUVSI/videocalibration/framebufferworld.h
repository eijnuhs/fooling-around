
// $Id: framebufferworld.h,v 1.1.1.1.2.6 2005/01/26 06:48:26 cvs Exp $
// Jacky Baltes <jacky@cs.umanitoba.ca> Wed Jun  9 12:46:47 CDT 2004
//

#ifndef _FRAME_BUFFER_WORLD_H_
#define _FRAME_BUFFER_WORLD_H_

#include "framebufferrgb32x.h"
#include "playingfield.h"



class FrameBufferWorld : public FrameBufferRGB32X
{
 public:
  FrameBufferWorld( double mmperpix, PlayingField field);
  FrameBufferWorld( );
  bool bufferToWorldCoordinates( double x, double y, double * xw, double *yw ) const;
  bool bufferToWorldCoordinates( unsigned int x, unsigned int y, double * xw, double *yw ) const;
  bool worldToBufferCoordinates( double xw, double yw, unsigned int * x, unsigned int *y ) const;
  void initialize( double mmperpix, PlayingField field );
  double getMMPerPixel();
  
  double xOffset, yOffset;
  double mmPerPix;
  
  PlayingField field;
};

#endif

