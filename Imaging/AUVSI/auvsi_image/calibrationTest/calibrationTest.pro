TEMPLATE	= app
LANGUAGE	= C++

CVWRAP_LIB_PATH = ../../opencv_wrapper

INCLUDEPATH += ../../  ../../autogui/include /usr/local/include/boost-1_33_1 \
               /usr/include/opencv

LIBS += -L/sw/lib -L/usr/local/include -lautogui

include( $${CVWRAP_LIB_PATH}/cvwrapinclude.pro  )
!exists( $${CVWRAP_LIB_PATH}/cvwrapinclude.pro ){
  error( OpenCv Wrapper Library not found...It may be checked out from the sculpin cvs.)
}

include ( ../calibration/calibrationinclude.pro )


SOURCES	+= calibrationTest.cpp ../../auvsi_common/gps.cpp

QMAKE_CXXFLAGS += -ggdb -Wall 

macx {
  QMAKE_CXXFLAGS += -Wno-long-double
}

unix {
  UI_DIR = .ui
  MOC_DIR = .moc
  OBJECTS_DIR = .obj
  DEFINES += DEBUG_PTF
}

