#ifndef HELPER_FNS_H
#define HELPER_FNS_H

#include <umimage.hpp>
#include <string>

template<typename PixelType>
inline void doubleVertically(UmImage<PixelType> & m_image)
{
  typedef UmImage<PixelType> image_type;
  // shallow copy
  image_type image = m_image;

  m_image.resize(m_image.width(), m_image.height() * 2);
  typename image_type::traverser in = image.upperLeft();
  typename image_type::traverser out1 = m_image.upperLeft();
  typename image_type::traverser out2 = m_image.upperLeft();
  out2.y++;
  
  //    int i = 0;
  for(; in.y != image.lowerRight().y; in.y++, out1.y += 2, out2.y += 2)
    {
      //	std::cout << "copying row: " << i++ << "\n";
      memcpy(&(*out1), &(*in), image.widthStep());
      memcpy(&(*out2), &(*in), image.widthStep());
    }
}


#endif
