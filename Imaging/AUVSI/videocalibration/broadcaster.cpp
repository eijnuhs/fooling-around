#include "broadcaster.h"
#include "trackable.h"
#include "framebuffer.h"
#include <sys/time.h>
#include "framespersecond.h"
#include <sstream>

Broadcaster::Broadcaster(BroadcasterConfig * bcfg) : 
  QObject(),
  UDPBroadcaster(bcfg->getUnsigned("port",DEFAULT_BROADCAST_PORT),
		 (char *)bcfg->getString("ip_address", DEFAULT_IP_ADDR).c_str())
{
  gettimeofday (&t, 0);
}

Broadcaster::~Broadcaster()
{
  
}

void Broadcaster::setCameraPos(double cx, double cy, double cz)
{
  this->cx = cx;
  this->cy = cy;
  this->cz = cz;
}

void Broadcaster::newTrackable(std::vector<Trackable *> & trackable, FrameBuffer * frame)
{
  // build and broadcast the message.
  int timediff = FramesPerSecond::getTimeDiff(t, frame->t);
  
  std::ostringstream os;
  os << trackable.size() << ' ' << frame->absFrameNo << ' ' << timediff << std::endl;
  os << cx << ' ' << cy << ' ' << cz << std::endl;

  

  for (uint i = 0; i < trackable.size(); i++)
    {
      trackable[i]->output(os);
    }
  os << "\0";
  //  std::cout << "Buffer: " << os.str() << "\n";
  broadcast( os.str().c_str(), true);  //Broadcast a message

  memcpy(&t,&(frame->t),sizeof(struct timeval));
}
