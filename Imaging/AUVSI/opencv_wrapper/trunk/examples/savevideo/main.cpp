#include <qapplication.h>
#include "umimage.hpp"
#include "agmainwindow.hpp"
#include <boost/bind.hpp>
#include <iostream>
#include <qmenubar.h>
#include <videostream.h>
#include <vigra/rgbvalue.hxx>
#include "umimagegui.hpp"
#include "sampleprocessing.hpp"
#include <vector>
#include "v4l2control.h"

typedef vigra::RGBAValue<unsigned char> pixel_type;

int main(int argc, char ** argv)
{ 
  QApplication a(argc, argv);
  a.connect( &a, SIGNAL( lastWindowClosed() ), &a, SLOT( quit() ) );

  // initialize the configuration file
  if(!Configuration::init("cvwrap.conf"))
    {
      std::cout << "Config file error\n";
      exit(1);
    }
  SampleProcessing<pixel_type> b;
  VideoStream<pixel_type> vs;
  
  
  vs.connect(boost::ref(b));
  vs.startThread();
 

 
  
  AGMainWindow w;
  w.resize(1024,768);

  AGManager & gui = *w.getAGManager();

  VideoDevice * vd = vs.getVideoDevice();
  V4L2Device * vl2 = dynamic_cast<V4L2Device *>(vd);

  if(vl2)
    {
      std::cout << "got v4l2device\n";
      gui.pushGroup("V4L2");
      QWidget * parent = gui.getCurrentParent();
      V4L2Control * v4l2 = new V4L2Control(parent);
      v4l2->init(vl2);
      gui.popGroup();
    }


  
  addToGui(gui,b,"processing",100);
  

  w.show();
  int ret = a.exec();
  vs.stopThread();

  return ret;
}
