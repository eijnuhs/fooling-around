#ifndef _TELEMETRY_H
#define _TELEMETRY_H

#include <boost/date_time/posix_time/posix_time.hpp>
#include "gps.hpp"

struct telemetry
{
  boost::posix_time::ptime time;
  double longitude;
  double latitude;
  double pitch;
  double roll;
  int airspeed;
  int groundspeed;
  int altitude;
  int vsi;
  double heading;
  double track;
  int status;

  telemetry()
  {
  }

  telemetry(boost::posix_time::ptime _time, double _longitude = 0, double _latitude = 0, double _pitch = 0,
	    double _roll = 0, int _airspeed = 0, int _groundspeed = 0, int _altitude = 0, int _vsi = 0,
	    double _heading = 0, double _track = 0, int _status = 0) :
    time(_time), longitude(_longitude), latitude(_latitude), pitch(_pitch), roll(_roll), airspeed(_airspeed),
    groundspeed(_groundspeed), altitude(_altitude), vsi(_vsi), heading(_heading), track(_track),
    status(_status)
  {
  }

  GpsCoord gpsCoord()
  {
    return GpsCoord(longitude, latitude);
  }

};

#endif
