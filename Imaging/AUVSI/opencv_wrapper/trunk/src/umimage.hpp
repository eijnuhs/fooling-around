#ifndef UMIMAGE_HPP
#define UMIMAGE_HPP


#include <vigra/imageiterator.hxx>
#include <vigra/initimage.hxx>
#include <cv.h>
#include <boost/shared_array.hpp>
#include <boost/shared_ptr.hpp>
#include <string>
#include "umimagetraits.hpp"
#include "weakimage.hpp"
#include "boost/thread/mutex.hpp"
#include <umbotica/io/tostring.hpp>
#include <vigra/impex.hxx>

#define RUNTIME_CHECKS

#ifdef RUNTIME_CHECKS
#include <assert.h>
#endif

static std::string cvDepthString(int cvDepth)
{
    std::string retVal;
    switch(cvDepth)
      {
      case IPL_DEPTH_8U:
	retVal = "unsigned 8-bit";
	break;
      case IPL_DEPTH_8S:
	retVal =  "signed 8-bit";
	break;
      case IPL_DEPTH_16S:
	retVal = "signed 16-bit";
	break;
      case IPL_DEPTH_32S:
	retVal = "signed 32-bit";
	break;
      case IPL_DEPTH_32F:
	retVal = "single-precision floating point";
	break;
      case IPL_DEPTH_64F:
	retVal = "double-presision floating point";
	break;
      default:
	retVal = "unknown pixel depth";
    }
    return retVal;
}

template <class PIXELTYPE>
class UmImage
{
  public:

  /** the UmImage's pixel type
   */
  typedef PIXELTYPE value_type;
  
  /** the UmImage's pixel type
   */
  typedef PIXELTYPE PixelType;

  /** the UmImage's buffer pointer type
   */
  typedef boost::shared_array<PixelType> buffer_ptr_type;
  
  /** the UmImage's reference type (i.e. the
      return type of image[diff] and image(dx,dy))
  */
  typedef PIXELTYPE & reference;
  
  /** the UmImage's const reference type (i.e. the
      return type of image[diff] and image(dx,dy) when image is const)
  */
  typedef PIXELTYPE const & const_reference;
  
        /** the UmImage's pointer type
	 */
  typedef PIXELTYPE *       pointer;
  
  /** the UmImage's const pointer type
   */
  typedef PIXELTYPE const * const_pointer;

  /** the UmImage's 1D random access iterator
      (note: lower case 'iterator' is a STL compatible 1D random
      access iterator, don't confuse with capitalized Iterator)
  */
  
  typedef PIXELTYPE * ScanOrderIterator;
  
        /** the UmImage's 1D random access const iterator
            (note: lower case 'const_iterator' is a STL compatible 1D
            random access const iterator)
        */
  typedef PIXELTYPE const * const_iterator;
  
  
  /** the UmImage's 2D random access iterator ('traverser')
   */
  typedef vigra::ImageIterator<value_type> traverser;
  
  /** the UmImage's 2D random access const iterator ('const traverser')
   */
  typedef vigra::ConstImageIterator<value_type> const_traverser;
  
  /** the UmImage's difference type (argument type of image[diff])
   */
  typedef vigra::Diff2D difference_type;
  
  /** the UmImage's size type (result type of image.size())
   */
  typedef vigra::Size2D size_type;
  
  /** the UmImage's default accessor
   */
  typedef typename
  vigra::IteratorTraits<traverser>::DefaultAccessor Accessor;
  
  /** the UmImage's default const accessor
   */
  typedef typename
  vigra::IteratorTraits<const_traverser>::DefaultAccessor ConstAccessor;

  typedef umbotica::PixelTraits<PixelType> pixelTraits;

  typedef typename 
  pixelTraits::component_type component_type;

  typedef typename
  pixelTraits::RGBAccessor RGBAccessor;

  typedef typename
  pixelTraits::GrayAccessor GrayAccessor;

  typedef typename
  pixelTraits::RGBAAccessor RGBAAccessor;

  enum { NumberOfChannels = umbotica::PixelTraits<PixelType>::NumberOfChannels };
  enum { CvDepth =  umbotica::PixelTraits<PixelType>::CvDepth };
  enum { BytesPerPixel = umbotica::PixelTraits<PixelType>::BytesPerPixel };
  
  /** construct image of size w x h
   */
  UmImage(int w = 0, int h = 0, int stride = 0)
  {
    //    std::cout << "UmImage constructor: [" << w << "," << h << "], s:" << stride << "\n";
    // init the image with the width and height.
    // this aligns the vigra basic image view and IplImage structures
    // so that everyone is talking the same language.
    cv_init(w, h, stride, true);
  }
  
  /** construct image of size w x h from an existing buffer
   */
  UmImage(buffer_ptr_type buffer, int w = 0, int h = 0, int stride = 0)
  {
    //    std::cout << "UmImage buffer constructor: [" << w << "," << h << "], s:" << stride << "\n";
    // init the image with the width and height.
    // this aligns the vigra basic image view and IplImage structures
    // so that everyone is talking the same language.
    cv_init(w, h, stride, false);
    setBuffer(buffer);
  }
  
  /** assignment operator from a similarly-typed UmImage
   *  Shallow copy for now.
   */
  UmImage & operator=(UmImage & image)
  {
    cv_init(image.buffer(), image.width(), image.height(), image.stride());
    return *this;
  }

  // this will make a deep copy of the incoming image.
  template<class Pt>
  void copyFrom(const UmImage<Pt> & rhs)
  {
    resize(rhs.size());
    newBuffer();
    if(width() > 0 && height() > 0)
      {
	if(NumberOfChannels == 1)
	  {
	    vigra::copyImage(rhs.upperLeft(), rhs.lowerRight(), rhs.grayAccessor(), upperLeft(), GrayAccessor());
	  }
	else if(NumberOfChannels == 3)
	  {
	    vigra::copyImage(rhs.upperLeft(), rhs.lowerRight(), rhs.rgbaAccessor(), upperLeft(), RGBAccessor());
	  }
	else if(NumberOfChannels == 4)
	  {
	    vigra::copyImage(rhs.upperLeft(), rhs.lowerRight(), rhs.rgbaAccessor(), upperLeft(), RGBAAccessor());
	  }
	else
	  {
	    std::cerr << "UmImage passed unsupported image type\n";
	  }
      }
  }

  /** copy constructor
   *  Shallow copy for now.
   */
  UmImage(const UmImage<PIXELTYPE> & image)
  {
    cv_init(image.buffer(), image.width(), image.height(), image.stride());
  }

  /** set Image with const value
   */
  UmImage & init(value_type const & pixel)
  {
    initImage(upperLeft(), lowerRight(), accessor(), pixel);
    return *this;
  }
  
  /** width of Image
   */
  int width() const
  {
    return m_image.width;
  }
  
  /** height of Image
   */
  int height() const
  {
    return m_image.height;
  }
  
  /** stride of Image. 
      Memory offset between the start of two successive rows.
  */
    int stride() const
  {
    return m_stride;
  }
  
  /** size of Image
   */
  size_type size() const
  {
    return size_type(width(), height());
  }
  
  /** test whether a given coordinate is inside the image
   */
  bool isInside(difference_type const & d) const
  {
    return d.x >= 0 && d.y >= 0 &&
      d.x < width() && d.y < height();
  }
  
  /** access pixel at given location. <br>
      usage: <TT> value_type value = image[Diff2D(1,2)] </TT>
  */
  reference operator[](difference_type const & d)
  {
#ifdef RUNTIME_CHECKS
    assert((unsigned int)(d.y*stride() + d.x) < imageSize());
#endif
    return m_buffer[d.y*stride() + d.x];
  }
  
  /** read pixel at given location. <br>
      usage: <TT> value_type value = image[Diff2D(1,2)] </TT>
  */
  const_reference operator[](difference_type const & d) const
  {
#ifdef RUNTIME_CHECKS
    assert((d.y*stride() + d.x) < imageSize());
#endif
    return m_buffer[d.y*stride() + d.x];
  }
  
  /** access pixel at given location. <br>
      usage: <TT> value_type value = image(1,2) </TT>
  */
  reference operator()(int dx, int dy)
  {
#ifdef RUNTIME_CHECKS
    assert((dy*stride() + dx) < (int)imageSize());
#endif
    return m_buffer[dy*stride() + dx];
  }
  
  /** read pixel at given location. <br>
      usage: <TT> value_type value = image(1,2) </TT>
  */
  const_reference operator()(int dx, int dy) const
  {
#ifdef RUNTIME_CHECKS
    assert((dy*stride() + dx) < imageSize());
#endif
    return m_buffer[dy*stride() + dx];
  }
  
  /** access pixel at given location.
      Note that the 'x' index is the trailing index. <br>
      usage: <TT> value_type value = image[2][1] </TT>
  */
  pointer operator[](int dy)
  {
#ifdef RUNTIME_CHECKS
    assert(dy < height());
#endif
    return m_buffer.get() + dy*stride();
  }
  
  /** read pixel at given location.
      Note that the 'x' index is the trailing index. <br>
      usage: <TT> value_type value = image[2][1] </TT>
  */
  const_pointer operator[](int dy) const
  {
#ifdef RUNTIME_CHECKS
    assert(dy < height());
#endif
    return m_buffer.get() + dy*stride();
  }
  
  /** init 2D random access iterator poining to upper left pixel
   */
  traverser upperLeft()
  {
    return traverser(m_buffer.get(), stride());
  }
  
  /** init 2D random access iterator poining to
      pixel(width, height), i.e. one pixel right and below lower right
      corner of the image as is common in C/C++.
  */
  traverser lowerRight()
  {
    return upperLeft() + size();
  }
  
  /** init 2D random access const iterator poining to upper left pixel
   */
  const_traverser upperLeft() const
  {
    return const_traverser(m_buffer.get(), stride());
  }
  
  /** init 2D random access const iterator poining to
      pixel(width, height), i.e. one pixel right and below lower right
      corner of the image as is common in C/C++.
  */
  const_traverser lowerRight() const
  {
    return upperLeft() + size();
  }
  
  /** return default accessor
   */
  Accessor accessor()
  {
    return Accessor();
  }
  
  /** return default const accessor
   */
  ConstAccessor accessor() const
  {
    return ConstAccessor();
  }
  
  RGBAccessor rgbAccessor() const
  {
    return RGBAccessor();
  }

  RGBAAccessor rgbaAccessor() const
  {
    return RGBAAccessor();
  }

  GrayAccessor grayAccessor() const
  {
    return GrayAccessor();
  }
  
  bool isNull()
  {
    return m_buffer.get() == NULL;
  }
  
  int depth() const                  { return m_image.depth;     }
  unsigned int imageSize() const     { return m_image.imageSize; }
  unsigned int widthStep() const     { return m_image.widthStep; }
  std::string colorModel() const     { return m_image.colorModel;}
  std::string channelSeq() const     { return m_image.channelSeq;}
  int nChannels() const              { return m_image.nChannels; }
  buffer_ptr_type buffer() const     { return m_buffer;          } 

  /// Resize and allocate a new buffer
  /// if necessary
  void resize(unsigned int w, unsigned int h)
  {
    if((int)w != width() || (int)h != height())
      {
	cv_init(w,h,0,true);
      }
  }

  void resize(size_type s)
  {
    resize(s.width(), s.height());
    
  }
  std::string depth_str() const
  {
    return cvDepthString(CvDepth);
  }

  IplImage * cvImage(){return &m_image;}
  IplImage const * cvImage() const{return &m_image;}

  bool loadImage(std::string filename)
  {
    bool success = false;

    try{
    
      if(vigra::isImage(filename.c_str()))
	{
	  //	  std::cout << "Attepting to load image: " << filename << "\n";
	  vigra::ImageImportInfo info(filename.c_str());
	  cv_init(info.width(), info.height(), 0, true);
	  if(info.isGrayscale())
	    {	      
	      //	      std::cout << "loading a grayscale image\n";
	      vigra::importImage(info, upperLeft(), grayAccessor());
	      success = true;
	    }
	  else
	    {
	      //	      std::cout << "loading a color image\n";
	      if(NumberOfChannels == 1)
		{
		  // to ensure proper loading, we will load into a temp and copy the image.
		  typedef UmImage<vigra::RGBValue<uint8_t> > TempImageType;
		  TempImageType temp(info.width(),info.height(),0);
		  vigra::importImage(info, temp.upperLeft(), typename TempImageType::RGBAccessor());
		  copyFrom(temp);
		}
	      else
		{
		  vigra::importImage(info, upperLeft(), rgbAccessor());
		}
	      success = true;
	    }
	}
    }
    catch(const vigra::StdException e)
      {
	// catch any errors that might have occured and print their reason
	std::cerr << "UmImage::loadImage() error: " << e.what() << std::endl;
      }
    
    return success;
  }


private:
  void cv_init(boost::shared_array<PixelType> newBuffer, unsigned int w, unsigned int h, unsigned int s)
  {
    cv_init(w,h,s,false);
    setBuffer(newBuffer);
  }
  void cv_init(unsigned int w, unsigned int h, unsigned int s, bool allocateBuffer)
  {
    CvSize size;
    size.height = h;
    size.width = w;
    m_stride = (s == 0) ? w : s;
    int step = m_stride * BytesPerPixel;//sizeof(PixelType);
    //    m_image.widthStep = step;
    
    //   m_roi.xOffset = 0;
    //    m_roi.yOffset = 0;
    //    m_roi.width = w;
    //    m_roi.height = h;

    resetBuffer();
    cvInitImageHeader(&m_image, size , CvDepth, NumberOfChannels, IPL_ORIGIN_TL, 4);
    if(allocateBuffer)
      {
	newBuffer();
      }
    
    // CvInit clobbers the widthstep...
    // This may cause problems in the future...Keep your eye out for wierdness when
    // using opencv
#ifdef RUNTIME_CHECKS
    //    assert(m_image.widthStep == step);
    if(m_image.widthStep != step)
      std::cout << "Changing the widthstep from what OpenCv wanted.  OpenCv functions may not work!\n";
    
#endif

    m_image.widthStep = step;
    m_image.dataOrder = 0;
    m_image.roi = 0;

  }


  buffer_ptr_type newBuffer()
  {
    buffer_ptr_type newBuffer = buffer_ptr_type(new value_type[width() * height()]);
    setBuffer(newBuffer);
    return m_buffer;
  }

  void setBuffer(buffer_ptr_type newBuffer)
  {
    m_buffer = newBuffer;
    m_image.imageData = (char *)m_buffer.get();
    m_image.imageDataOrigin = m_image.imageData;
  }

  void resetBuffer()
  {
    m_buffer.reset();
    m_image.imageData = 0;
    m_image.imageDataOrigin = 0;
  }
  
  IplImage m_image;
  
  // IplROI has the following fields:
  // int coi     -- the channel of interest.
  // int xOffset -- the x offset to the start of the ROI
  // int yOffset -- the y offset to the start of the ROI
  // int width   -- the width of the ROI
  // int height  -- the height of the ROI
  //  IplROI m_roi;
  buffer_ptr_type m_buffer;
  unsigned int m_stride;

  friend class WeakImage;
  friend class UmImageLocker;
  boost::mutex m_mutex;
};


/********************************************************/
/*                                                      */
/*              argument object factories               */
/*                                                      */
/********************************************************/

template <class PixelType, class Accessor>
inline vigra::triple<typename UmImage<PixelType>::const_traverser, 
              typename UmImage<PixelType>::const_traverser, Accessor>
srcImageRange(UmImage<PixelType> const & img, Accessor a)
{
  return vigra::triple<typename UmImage<PixelType>::const_traverser, 
    typename UmImage<PixelType>::const_traverser, 
    Accessor>(img.upperLeft(),
	      img.lowerRight(),
	      a);
}

template <class PixelType, class Accessor>
inline vigra::pair<typename UmImage<PixelType>::const_traverser, Accessor>
srcImage(UmImage<PixelType> const & img, Accessor a)
{
    return vigra::pair<typename UmImage<PixelType>::const_traverser, 
                Accessor>(img.upperLeft(), a);
}

template <class PixelType, class Accessor>
inline vigra::triple<typename UmImage<PixelType>::traverser, 
              typename UmImage<PixelType>::traverser, Accessor>
destImageRange(UmImage<PixelType> & img, Accessor a)
{
  return vigra::triple<typename UmImage<PixelType>::traverser, 
    typename UmImage<PixelType>::traverser, 
    Accessor>(img.upperLeft(),
	      img.lowerRight(),
	      a);
}

template <class PixelType, class Accessor>
inline vigra::pair<typename UmImage<PixelType>::traverser, Accessor>
destImage(UmImage<PixelType> & img, Accessor a)
{
    return vigra::pair<typename UmImage<PixelType>::traverser, 
                Accessor>(img.upperLeft(), a);
}

template <class PixelType, class Accessor>
inline vigra::pair<typename UmImage<PixelType>::const_traverser, Accessor>
maskImage(UmImage<PixelType> const & img, Accessor a)
{
    return vigra::pair<typename UmImage<PixelType>::const_traverser, 
                Accessor>(img.upperLeft(), a);
}

/****************************************************************/

template <class PixelType>
inline vigra::triple<typename UmImage<PixelType>::const_traverser, 
              typename UmImage<PixelType>::const_traverser, 
              typename UmImage<PixelType>::ConstAccessor>
srcImageRange(UmImage<PixelType> const & img)
{
    return vigra::triple<typename UmImage<PixelType>::const_traverser, 
                  typename UmImage<PixelType>::const_traverser, 
                  typename UmImage<PixelType>::ConstAccessor>(img.upperLeft(),
                                                                     img.lowerRight(),
                                                                     img.accessor());
}

template <class PixelType>
inline vigra::pair< typename UmImage<PixelType>::const_traverser, 
             typename UmImage<PixelType>::ConstAccessor>
srcImage(UmImage<PixelType> const & img)
{
    return vigra::pair<typename UmImage<PixelType>::const_traverser, 
                typename UmImage<PixelType>::ConstAccessor>(img.upperLeft(), 
                                                                   img.accessor());
}

template <class PixelType>
inline vigra::triple< typename UmImage<PixelType>::traverser, 
               typename UmImage<PixelType>::traverser, 
               typename UmImage<PixelType>::Accessor>
destImageRange(UmImage<PixelType> & img)
{
    return vigra::triple<typename UmImage<PixelType>::traverser, 
                  typename UmImage<PixelType>::traverser, 
                  typename UmImage<PixelType>::Accessor>(img.upperLeft(),
                                                                img.lowerRight(),
                                                                img.accessor());
}

template <class PixelType>
inline vigra::pair< typename UmImage<PixelType>::traverser, 
             typename UmImage<PixelType>::Accessor>
destImage(UmImage<PixelType> & img)
{
    return vigra::pair<typename UmImage<PixelType>::traverser, 
                typename UmImage<PixelType>::Accessor>(img.upperLeft(), 
                                                              img.accessor());
}

template <class PixelType>
inline vigra::pair< typename UmImage<PixelType>::const_traverser, 
             typename UmImage<PixelType>::ConstAccessor>
maskImage(UmImage<PixelType> const & img)
{
    return vigra::pair<typename UmImage<PixelType>::const_traverser, 
                typename UmImage<PixelType>::ConstAccessor>(img.upperLeft(), 
                                                                   img.accessor());
}


inline std::ostream & operator << (std::ostream & oss, const IplImage & image)
{
  using umbotica::s_tuple;
  using umbotica::arr;
  oss << "IplImage: " << s_tuple(image.width, image.height) << " x " << image.nChannels << " x " << cvDepthString(image.depth) << " = imageSize: " << image.imageSize << "\n";
  oss << "alphaChannel: " << image.alphaChannel << "  widthStep: " << image.widthStep << "\n";
  oss << "Header: nSize: " << image.nSize << " ID: " << image.ID << "\n";
  oss << "colorModel: " << arr(image.colorModel, 4) << "  channel Seq: " << arr(image.channelSeq, 4) << "\n";
  oss << "data order: " << ((image.dataOrder == 0) ? "interleaved. " : "separate. ") << " origin: " << ((image.origin == 0) ? "top-left" : "bottom-left") << "\n";
  oss << "align: " << image.align << "  ROI ptr: " << (void *)image.roi << "  mask ROI ptr: " << (void *) image.maskROI << "\n";
  oss << "imageId ptr: " << (void *) image.imageId << "  tileInfo: " << (void*) image.tileInfo << "\n";
  oss << "buffer ptr: " << (void *) image.imageData << "  BorderMode: " << arr(image.BorderMode,4) << "  BorderConst: " << arr(image.BorderConst,4) << "\n";
  oss << "imageDataOrigin ptr: " << (void *) image.imageDataOrigin << "\n";
  return oss;
}

template<class PixelType>
std::ostream &operator<<(std::ostream & oss, const UmImage<PixelType> & image)
{
  using umbotica::s_tuple;
  IplImage const & cvImage = image.cvImage();
  oss << "UmImage<" << typeid(PixelType).name() << "> " << s_tuple(image.width(), image.height()) << " stride: " << image.stride() << "\n";
  oss << "this: " << (void*)&image << "  IplImage *: " << &image.cvImage() << "\n";
  oss << cvImage;
  return oss;
}


#endif /* UMIMAGE_HPP */
