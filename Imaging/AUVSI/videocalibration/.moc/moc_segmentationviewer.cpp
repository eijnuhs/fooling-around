/****************************************************************************
** SegmentationViewer meta object code from reading C++ file 'segmentationviewer.h'
**
** Created: Fri Feb 17 15:31:14 2006
**      by: The Qt MOC ($Id: qt/moc_yacc.cpp   3.3.4   edited Jan 21 18:14 $)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#undef QT_NO_COMPAT
#include "../segmentationviewer.h"
#include <qmetaobject.h>
#include <qapplication.h>

#include <private/qucomextra_p.h>
#if !defined(Q_MOC_OUTPUT_REVISION) || (Q_MOC_OUTPUT_REVISION != 26)
#error "This file was generated using the moc from 3.3.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

const char *SegmentationViewer::className() const
{
    return "SegmentationViewer";
}

QMetaObject *SegmentationViewer::metaObj = 0;
static QMetaObjectCleanUp cleanUp_SegmentationViewer( "SegmentationViewer", &SegmentationViewer::staticMetaObject );

#ifndef QT_NO_TRANSLATION
QString SegmentationViewer::tr( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "SegmentationViewer", s, c, QApplication::DefaultCodec );
    else
	return QString::fromLatin1( s );
}
#ifndef QT_NO_TRANSLATION_UTF8
QString SegmentationViewer::trUtf8( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "SegmentationViewer", s, c, QApplication::UnicodeUTF8 );
    else
	return QString::fromUtf8( s );
}
#endif // QT_NO_TRANSLATION_UTF8

#endif // QT_NO_TRANSLATION

QMetaObject* SegmentationViewer::staticMetaObject()
{
    if ( metaObj )
	return metaObj;
    QMetaObject* parentObject = FrameBufferViewer::staticMetaObject();
    static const QUParameter param_slot_0[] = {
	{ "mode", &static_QUType_QString, 0, QUParameter::In }
    };
    static const QUMethod slot_0 = {"setMode", 1, param_slot_0 };
    static const QUParameter param_slot_1[] = {
	{ "threshold", &static_QUType_int, 0, QUParameter::In }
    };
    static const QUMethod slot_1 = {"setThreshold", 1, param_slot_1 };
    static const QUParameter param_slot_2[] = {
	{ "threshold", &static_QUType_int, 0, QUParameter::In }
    };
    static const QUMethod slot_2 = {"setIntThreshold", 1, param_slot_2 };
    static const QUParameter param_slot_3[] = {
	{ "p", &static_QUType_ptr, "QPainter", QUParameter::InOut },
	{ "r", &static_QUType_ptr, "RunRegion", QUParameter::In }
    };
    static const QUMethod slot_3 = {"drawRegion", 2, param_slot_3 };
    static const QUMethod slot_4 = {"printName", 0, 0 };
    static const QUMethod slot_5 = {"saveBackground", 0, 0 };
    static const QMetaData slot_tbl[] = {
	{ "setMode(const QString&)", &slot_0, QMetaData::Public },
	{ "setThreshold(int)", &slot_1, QMetaData::Public },
	{ "setIntThreshold(int)", &slot_2, QMetaData::Public },
	{ "drawRegion(QPainter&,const RunRegion&)", &slot_3, QMetaData::Public },
	{ "printName()", &slot_4, QMetaData::Public },
	{ "saveBackground()", &slot_5, QMetaData::Public }
    };
    metaObj = QMetaObject::new_metaobject(
	"SegmentationViewer", parentObject,
	slot_tbl, 6,
	0, 0,
#ifndef QT_NO_PROPERTIES
	0, 0,
	0, 0,
#endif // QT_NO_PROPERTIES
	0, 0 );
    cleanUp_SegmentationViewer.setMetaObject( metaObj );
    return metaObj;
}

void* SegmentationViewer::qt_cast( const char* clname )
{
    if ( !qstrcmp( clname, "SegmentationViewer" ) )
	return this;
    return FrameBufferViewer::qt_cast( clname );
}

bool SegmentationViewer::qt_invoke( int _id, QUObject* _o )
{
    switch ( _id - staticMetaObject()->slotOffset() ) {
    case 0: setMode((const QString&)static_QUType_QString.get(_o+1)); break;
    case 1: setThreshold((int)static_QUType_int.get(_o+1)); break;
    case 2: setIntThreshold((int)static_QUType_int.get(_o+1)); break;
    case 3: drawRegion((QPainter&)*((QPainter*)static_QUType_ptr.get(_o+1)),(const RunRegion&)*((const RunRegion*)static_QUType_ptr.get(_o+2))); break;
    case 4: printName(); break;
    case 5: saveBackground(); break;
    default:
	return FrameBufferViewer::qt_invoke( _id, _o );
    }
    return TRUE;
}

bool SegmentationViewer::qt_emit( int _id, QUObject* _o )
{
    return FrameBufferViewer::qt_emit(_id,_o);
}
#ifndef QT_NO_PROPERTIES

bool SegmentationViewer::qt_property( int id, int f, QVariant* v)
{
    return FrameBufferViewer::qt_property( id, f, v);
}

bool SegmentationViewer::qt_static_property( QObject* , int , int , QVariant* ){ return FALSE; }
#endif // QT_NO_PROPERTIES
