"""Hello, and welcome to the source code of Gorillas.py. This program is meant to be very well documented so that a
novice programmer can follow along. This program was written by Al Sweigart as a companion for his free, Creative
Commons-licensed book "Invent Your Own Computer Games with Python", which is available in full at:

        http://inventwithpython.com

Feel free to email the author with any programming questions at al@inventwithpython.com

This program seeks to replicate gorillas.bas, a Qbasic program that was popular in the 1990s. By reading
through the comments, you can learn how a simple Python game with the Pygame engine is put together.

The comments will generally come _after_ the lines of code they describe.

If you like this, then check out inventwithpython.com to read the book (which has similar game projects) for free!

The Pygame documentation is pretty good, and can be found at http://www.pygame.org/docs

Unfortunately there is no sound with this game.

"""
import numpy
import pygame, sys, time, random, math, copy
#import matplotlib.pyplot as plt
from pygame.locals import *
"""We'll import quite a few modules for this game. "pygame" has all the graphics & game-related functions that the
Pygame game engine provides. "sys" has the exit() function. "time" has the sleep() function. "random" has the randint()
function, and "math" contains the pi constant."""


"""All of the variables below in CAPS LETTERS are constants, that is, they are only supposed to be read and not
modified. (There's nothing to keep the program from modifying them, but it's just a convention programmers use.
The constants are a bit more descriptive than just using the numbers by themselves. And if you ever want to change
some value (such as the size of the explosions or the color of the gorillas), you only have to change it in one
place."""


SCR_WIDTH = 640
SCR_HEIGHT = 350
FPS = 40
GAME_CLOCK = pygame.time.Clock()
"""Here are several constants we will use in the game. The original Qbasic game had a screen size of 640x350, so we'll
use that as our screen size. We will use a single global Clock object to handle some of the timing stuff in all our
functions, and generally have FPS set to 30 (except when we want to set it to something else.

Constants are useful because you can just change the value in one place, and it will be used throughout the program.

Try experimenting with different values for these global constants."""

BUILDING_COLORS = ((173, 170, 173), (0, 170, 173), (173, 0, 0))
LIGHT_WINDOW = (255, 255, 82)
DARK_WINDOW = (82, 85, 82)
SKY_COLOR = (0, 0, 173)
GOR_COLOR = (255, 170, 82)
BAN_COLOR = (255, 255, 82)
EXPLOSION_COLOR = (255, 0, 0)
SUN_COLOR = (255, 255, 0)
DARK_RED_COLOR = (173, 0, 0)
BLACK_COLOR = (0, 0, 0)
WHITE_COLOR = (255, 255, 255)
GRAY_COLOR = (173, 170, 173)
"""Here are a bunch of colors. Pygame uses a tuple of three integers to specify a color. The integers are for the
amount of Red, Blue, and Green (in order) in the color. This is known as an RGB value.

BUILDING_COLORS will hold a tuple of these RGB tuples and represent the different colors the buildings can be."""

BUILD_EXPLOSION_SIZE = int(SCR_HEIGHT / 50)
GOR_EXPLOSION_SIZE = 30
"""BUILD_EXPLOSION_SIZE holds the size of an explosion when a banana hits a building, and GOR_EXPLOSION_SIZE is the size
when it hits a gorilla."""

SUN_X = 300
SUN_Y = 10
"""The position of the sun in the sky."""

pygame.init()
GAME_FONT = pygame.font.SysFont(None, 20)
"""The pygame.init() function needs to be called before calling any of the Pygame functions.
We will use the default system font at a size of 20 points."""

# orientation of the banana:
RIGHT = 0
UP = 1
LEFT = 2
DOWN = 3
"""Some constants for the direction the banana (or anything else) faces."""

# gorilla arms drawing types
BOTH_ARMS_DOWN = 0
LEFT_ARM_UP = 1
RIGHT_ARM_UP = 2
"""Some constants for which of the three gorilla sprites to use: both arms down, left arm up, or right arm up."""

""" Some globals from pygame for ease of writing function """
skyline_surf = None
build_coords = None
win_surface = None

"""The following multiline strings are used with the makeSurfaceFromASCII() function. It's basically a way of
generating Surfaces other than using the drawing functions or including graphic files along with this .py file.

Try experimenting by changing the strings. The first and last line are ignored (so you don't have to deal with
indentation issues in the string)."""

STAR_ASCII = """


   XX  XX
    XXXX
  XXXXXXXX
    XXXX
   XX  XX
"""

GOR_DOWN_ASCII = """

          XXXXXXXX
          XXXXXXXX
         XX      XX
         XXXXXXXXXX
         XXX  X  XX
          XXXXXXXX
          XXXXXXXX
           XXXXXX
      XXXXXXXXXXXXXXXX
   XXXXXXXXXXXXXXXXXXXXXX
  XXXXXXXXXXXX XXXXXXXXXXX
 XXXXXXXXXXXXX XXXXXXXXXXXX
 XXXXXXXXXXXX X XXXXXXXXXXX
XXXXX XXXXXX XXX XXXXX XXXXX
XXXXX XXX   XXXXX   XX XXXXX
XXXXX   XXXXXXXXXXXX   XXXXX
 XXXXX  XXXXXXXXXXXX  XXXXX
 XXXXX  XXXXXXXXXXXX  XXXXX
  XXXXX XXXXXXXXXXXX XXXXX
   XXXXXXXXXXXXXXXXXXXXXX
       XXXXXXXXXXXXX
     XXXXXX     XXXXXX
     XXXXX       XXXXX
    XXXXX         XXXXX
    XXXXX         XXXXX
    XXXXX         XXXXX
    XXXXX         XXXXX
    XXXXX         XXXXX
     XXXXX       XXXXX
"""

GOR_LEFT_ASCII = """
   XXXXX
  XXXXX   XXXXXXXX
 XXXXX    XXXXXXXX
 XXXXX   XX      XX
XXXXX    XXXXXXXXXX
XXXXX    XXX  X  XX
XXXXX     XXXXXXXX
 XXXXX    XXXXXXXX
 XXXXX     XXXXXX
  XXXXXXXXXXXXXXXXXXXX
   XXXXXXXXXXXXXXXXXXXXXX
      XXXXXXXX XXXXXXXXXXX
      XXXXXXXX XXXXXXXXXXXX
      XXXXXXX X XXXXXXXXXXX
      XXXXXX XXX XXXXX XXXXX
      XXX   XXXXX   XX XXXXX
        XXXXXXXXXXXX   XXXXX
        XXXXXXXXXXXX  XXXXX
        XXXXXXXXXXXX  XXXXX
        XXXXXXXXXXXX XXXXX
       XXXXXXXXXXXXXXXXXX
       XXXXXXXXXXXXX
     XXXXXX     XXXXXX
     XXXXX       XXXXX
    XXXXX         XXXXX
    XXXXX         XXXXX
    XXXXX         XXXXX
    XXXXX         XXXXX
    XXXXX         XXXXX
     XXXXX       XXXXX
"""

GOR_RIGHT_ASCII = """
                    XXXXX
          XXXXXXXX   XXXXX
          XXXXXXXX    XXXXX
         XX      XX   XXXXX
         XXXXXXXXXX    XXXXX
         XXX  X  XX    XXXXX
          XXXXXXXX     XXXXX
          XXXXXXXX    XXXXX
           XXXXXX     XXXXX
      XXXXXXXXXXXXXXXXXXXX
   XXXXXXXXXXXXXXXXXXXXXX
  XXXXXXXXXXXX XXXXXXX
 XXXXXXXXXXXXX XXXXXXX
 XXXXXXXXXXXX X XXXXXX
XXXXX XXXXXX XXX XXXXX
XXXXX XXX   XXXXX   XX
XXXXX   XXXXXXXXXXXX
 XXXXX  XXXXXXXXXXXX
 XXXXX  XXXXXXXXXXXX
  XXXXX XXXXXXXXXXXX
   XXXXXXXXXXXXXXXXX
       XXXXXXXXXXXXX
     XXXXXX     XXXXXX
     XXXXX       XXXXX
    XXXXX         XXXXX
    XXXXX         XXXXX
    XXXXX         XXXXX
    XXXXX         XXXXX
    XXXXX         XXXXX
     XXXXX       XXXXX
"""

BAN_RIGHT_ASCII = """
     XX
    XXX
   XXX
   XXX
   XXX
   XXX
   XXX
    XXX
     XX
"""

BAN_LEFT_ASCII = """
XX
XXX
 XXX
 XXX
 XXX
 XXX
 XXX
XXX
XX
"""

BAN_UP_ASCII = """
XX     XX
XXXXXXXXX
 XXXXXXX
  XXXXX
"""

BAN_DOWN_ASCII = """
  XXXXX
 XXXXXXX
XXXXXXXXX
XX     XX
"""

SUN_NORMAL_ASCII = """
                    X
                    X
            X       X       X
             X      X      X
             X      X      X
     X        X     X     X        X
      X        X XXXXXXX X        X
       XX      XXXXXXXXXXX      XX
         X  XXXXXXXXXXXXXXXXX  X
          XXXXXXXXXXXXXXXXXXXXX
  X       XXXXXXXXXXXXXXXXXXXXX       X
   XXXX  XXXXXXXXXXXXXXXXXXXXXXX  XXXX
       XXXXXXXXXX XXXXX XXXXXXXXXX
        XXXXXXXX   XXX   XXXXXXXX
        XXXXXXXXX XXXXX XXXXXXXXX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
        XXXXXXXXXXXXXXXXXXXXXXXXX
        XXXXXXXXXXXXXXXXXXXXXXXXX
       XXXXXX XXXXXXXXXXXXX XXXXXX
   XXXX  XXXXX  XXXXXXXXX  XXXXX  XXXX
  X       XXXXXX  XXXXX  XXXXXX       X
          XXXXXXXX     XXXXXXXX
         X  XXXXXXXXXXXXXXXXX  X
       XX      XXXXXXXXXXX      XX
      X        X XXXXXXX X        X
     X        X     X     X        X
             X      X      X
             X      X      X
            X       X       X
                    X
                    X
"""

SUN_SHOCKED_ASCII = """
                    X
                    X
            X       X       X
             X      X      X
             X      X      X
     X        X     X     X        X
      X        X XXXXXXX X        X
       XX      XXXXXXXXXXX      XX
         X  XXXXXXXXXXXXXXXXX  X
          XXXXXXXXXXXXXXXXXXXXX
  X       XXXXXXXXXXXXXXXXXXXXX       X
   XXXX  XXXXXXXXXXXXXXXXXXXXXXX  XXXX
       XXXXXXXXXX XXXXX XXXXXXXXXX
        XXXXXXXX   XXX   XXXXXXXX
        XXXXXXXXX XXXXX XXXXXXXXX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
        XXXXXXXXXXXXXXXXXXXXXXXXX
        XXXXXXXXXXXXXXXXXXXXXXXXX
       XXXXXXXXXXXXXXXXXXXXXXXXXXX
   XXXX  XXXXXXXXX     XXXXXXXXX  XXXX
  X       XXXXXXX       XXXXXXX       X
          XXXXXXX       XXXXXXX
         X  XXXXXX     XXXXXX  X
       XX      XXXXXXXXXXX      XX
      X        X XXXXXXX X        X
     X        X     X     X        X
             X      X      X
             X      X      X
            X       X       X
                    X
                    X
"""

"""
These settings is for convience of the learning agent
SPEEDUP speeds up the game
DISPLAY show the learning progress in-game
VERBOSE output bunch of information regarding the learning progress
ACCUM_TRACE RL Traces ie how far back does the learning occurs

"""
SPEEDUP = True
DISPLAY = True
VERBOSE = False
ACCUM_TRACE = True


def terminate():
    """Calls both the pygame.quit() and sys.exit() functions, to end the program. (I found that not calling
    pygame.quit() before sys.exit() can mess up IDLE sometimes."""
    pygame.quit()
    sys.exit()

def makeSurfaceFromASCII(ascii, fgColor=(255,255,255), bgColor=(0,0,0)):
    """Returns a new pygame.Surface object that has the image drawn on it as specified by the ascii parameter.
    The first and last line in ascii are ignored. Otherwise, any X in ascii marks a pixel with the foreground color
    and any other letter marks a pixel of the background color. The Surface object has a width of the widest line
    in the ascii string, and is always rectangular."""

    """Try experimenting with this function so that you can specify more than two colors. (Pass a dict of
    ascii letters and RGB tuples, say."""
    ascii = ascii.split('\n')[1:-1]
    width = max([len(x) for x in ascii])
    height = len(ascii)
    surf = pygame.Surface((width, height))
    surf.fill(bgColor)

    pArr = pygame.PixelArray(surf)
    for y in range(height):
        for x in range(len(ascii[y])):
            if ascii[y][x] == 'X':
                pArr[x][y] = fgColor
    return surf

GOR_DOWN_SURF    = makeSurfaceFromASCII(GOR_DOWN_ASCII,    GOR_COLOR,      SKY_COLOR)
GOR_LEFT_SURF    = makeSurfaceFromASCII(GOR_LEFT_ASCII,    GOR_COLOR,      SKY_COLOR)
GOR_RIGHT_SURF   = makeSurfaceFromASCII(GOR_RIGHT_ASCII,   GOR_COLOR,      SKY_COLOR)
BAN_RIGHT_SURF   = makeSurfaceFromASCII(BAN_RIGHT_ASCII,   BAN_COLOR,      SKY_COLOR)
BAN_LEFT_SURF    = makeSurfaceFromASCII(BAN_LEFT_ASCII,    BAN_COLOR,      SKY_COLOR)
BAN_UP_SURF      = makeSurfaceFromASCII(BAN_UP_ASCII,      BAN_COLOR,      SKY_COLOR)
BAN_DOWN_SURF    = makeSurfaceFromASCII(BAN_DOWN_ASCII,    BAN_COLOR,      SKY_COLOR)
SUN_NORMAL_SURF  = makeSurfaceFromASCII(SUN_NORMAL_ASCII,  SUN_COLOR,      SKY_COLOR)
SUN_SHOCKED_SURF = makeSurfaceFromASCII(SUN_SHOCKED_ASCII, SUN_COLOR,      SKY_COLOR)
STAR_SURF        = makeSurfaceFromASCII(STAR_ASCII,        DARK_RED_COLOR, BLACK_COLOR)

assert GOR_DOWN_SURF.get_size() == GOR_LEFT_SURF.get_size() == GOR_RIGHT_SURF.get_size()
"""Create the pygame.Surface objects from the ASCII strings."""

sunRect = pygame.Rect(SUN_X, SUN_Y, SUN_NORMAL_SURF.get_width(), SUN_NORMAL_SURF.get_height())
"""sunRect will be a global value so we'll always know where the sun is."""

def drawText(text, surfObj, x, y, fgcol, bgcol, pos='left'):
    """A generic function to draw a string to a pygame.Surface object at a certain x,y location. This returns
    a pygame.Rect object which describes the area the string was drawn on.

    If the pos parameter is "left", then the x,y parameter specifies the top left corner of the text rectangle.
    If the pos parameter is "center", then the x,y parameter specifies the middle top point of the text rectangle."""

    textobj = GAME_FONT.render(text, 1, fgcol, bgcol) # creates the text in memory (it's not on a surface yet).
    textrect = textobj.get_rect()

    if pos == 'left':
        textrect.topleft = (x, y)
    elif pos == 'center':
        textrect.midtop = (x, y)
    surfObj.blit(textobj, textrect) # draws the text onto the surface
    """Remember that the text will only appear on the screen if you pass the pygame.Surface object that was
    returned from the call to pygame.display.set_mode(), and only after pygame.display.update() is called."""
    return textrect

def getModCase(s, mod):
    """Checks the state of the shift and caps lock keys, and switches the case of the s string if needed."""
    if bool(mod & KMOD_RSHIFT or mod & KMOD_LSHIFT) ^ bool(mod & KMOD_CAPS):
        return s.swapcase()
    else:
        return s

def inputMode(prompt, screenSurf, x, y, fgcol, bgcol, maxlen=12, allowed=None, pos='left', cursor='_', cursorBlink=False):
    """Takes control of the program when called. This function displays a prompt on the screen (the "prompt" string)
    parameter) on the screenSurf surface at the x, y coordinates. The text is in the fgcol color with a bgcol color
    background. You can optionally specify maxlen for a maximum length of the user's response. "allowed" is a string
    of allowed characters (if the player can only type numbers, say) and ignores all other keystrokes. The "pos"
    parameter can either be the string "left" (where the x, y coordinates refer to the top left corner of the text
    box) or "center" (where the x, y coordinates refer to the top center of the text box).

    "cursor" is an optional character that is used for a cursor to show where the next letter will go. If "cursorBlink"
    is True, then this cursor character will blink on and off.

    The returned value is a string of what the player typed in, or None if the player pressed the Esc key.

    Note that the player can only press Backspace to delete characters, they cannot use the arrow keys to move the
    cursor."""
    inputText = ''
    """inputText will store the text of what the player has typed in so far."""
    done = False
    cursorTimestamp = time.time()
    cursorShow = cursor
    while not done:
        """We will keep looping until the player has pressed the Esc or Enter key."""

        if cursor and cursorBlink and time.time() - 1.0 > cursorTimestamp:
            if cursorShow == cursor:
                cursorShow = '   '
            else:
                cursorShow = cursor
            cursorTimestamp = time.time()

        for event in pygame.event.get():
            if event.type == QUIT:
                terminate()
            elif event.type == KEYUP:
                if event.key == K_ESCAPE:
                    return None
                elif event.key == K_RETURN:
                    done = True
                    if cursorShow:
                        cursorShow = '   '
                elif event.key == K_BACKSPACE:
                    if len(inputText):
                        drawText(prompt + inputText + cursorShow, screenSurf, textrect.left, textrect.top, bgcol, bgcol, 'left')
                        inputText = inputText[:-1]
                else:
                    if len(inputText) >= maxlen or event.key not in range(0,257) or (allowed is not None and chr(event.key) not in allowed):
                        continue
                    if event.key >= 32 and event.key < 128:
                        inputText += getModCase(chr(event.key), event.mod)

        textrect = drawText(prompt + cursorShow, screenSurf, x, y, fgcol, bgcol, pos)
        drawText(prompt + inputText + cursorShow, screenSurf, textrect.left, textrect.top, fgcol, bgcol, 'left')
        pygame.display.update()
        GAME_CLOCK.tick(FPS)
    return inputText

def nextBananaShape(orient):
    """Returns the next banana shape in the sequence of 0, 1, 2, 3, then 0 again. (These correspond to the RIGHT, UP,
    LEFT, and DOWN variables."""
    if orient + 1 == 4:
        return 0
    else:
        return orient + 1

def drawBanana(screenSurf, orient, x, y):
    """Draws the banana shape to the screenSurf surface with its top left corner at the x y coordinate provided.
    "orient" is one of the RIGHT, UP, LEFT, or DOWN values (which are the integers 0 to 3 respectively)"""
    if orient == DOWN:
        screenSurf.blit(BAN_DOWN_SURF, (x, y))
    elif orient == UP:
        screenSurf.blit(BAN_UP_SURF, (x, y))
    elif orient == LEFT:
        screenSurf.blit(BAN_LEFT_SURF, (x, y))
    elif orient == RIGHT:
        screenSurf.blit(BAN_RIGHT_SURF, (x, y))


def drawSun(screenSurf, shocked=False):
    """Draws the sun sprite onto the screenSurf surface. If shocked is True, then use the shocked-looking face,
    otherwise use the normal smiley face. This function does not call python.display.update()"""
    if shocked:
        screenSurf.blit(SUN_SHOCKED_SURF, (SUN_X, SUN_Y))
    else:
        screenSurf.blit(SUN_NORMAL_SURF, (SUN_X, SUN_Y))


def drawGorilla(screenSurf, x, y, arms=BOTH_ARMS_DOWN):
    """Draws the gorilla sprite onto the screenSurf surface at a specific x, y coordinate. The x,y coordinate
    is for the top left corner of the gorilla sprite. Note that all three gorilla surfaces are the same size."""

    if arms == BOTH_ARMS_DOWN:
        gorSurf = GOR_DOWN_SURF
    elif arms == LEFT_ARM_UP:
        gorSurf = GOR_LEFT_SURF
    elif arms == RIGHT_ARM_UP:
        gorSurf = GOR_RIGHT_SURF
    """Above we choose which surface object we will use to draw the gorilla, depending on the "arms" parameter.
    The call to screenSurf.blit() will draw the surface onto the screen (but it won't show up on the screen until
    pygame.display.update() is called."""

    screenSurf.blit(gorSurf, (x, y))

def makeCityScape():
    """This function creates and returns a new cityscape of various buildings on a pygame.Surface object and returns
    this surface object."""

    screenSurf = pygame.Surface((SCR_WIDTH, SCR_HEIGHT)) # first make the new surface the same size of the screen.
    screenSurf.fill(SKY_COLOR) # fill in the surface with the background sky color

    """We will choose an upward, downward, valley "v" curve, or hilly "^" curve for the slope of the buildings.
    Half of the time we will choose the valley slope shape, while the remaining three each have a 1/6 chance of
    being choosen. The slope also determines the height of the first building, which is stored in newHeight."""
    slope = 1 #random.randint(1, 6)
    if slope == 1:
        slope = 'upward'
        newHeight = 15
    elif slope == 2:
        slope = 'downward'
        newHeight = 130
    elif slope >= 3 and slope <= 5:
        slope = 'v'
        newHeight = 15
    else:
        slope = '^'
        newHeight = 130

    bottomLine = 335 # the bottom line of the buildings. We want some space for the wind arrow to go
    heightInc = 10 #10 # a baseline for how much buildings grow or shrink compared to the last building
    defBuildWidth = 37 # default building width, also judges how wide buildings can be
    #randomHeightDiff = 120 # about how much buildings grow or shrink
    windowWidth = 4 # the width of each window in pixels
    windowHeight = 7 # the height of each window in pixels
    windowSpacingX = 10 # how many pixels apart each window's left edge is
    windowSpacingY = 15 # how many pixels apart each window's top edge is
    gHeight = 25 # (I'm not sure what this suppoes to be in the original Qbasic code, but I copied it anyway)
    # (There also was a maxHeight variable in the original Qbasic, but I don't think it did anything so I left it out.)

    buildingCoords = [] # a list of (left, top) coords of each building, left to right

    x = 2 # x refers to the top left corner of the current building being drawn

    while x < SCR_WIDTH - heightInc: # SCR_WIDTH 640
        # In this loop we keep drawing new buildings until we run out of space on the screen.

        # First the slope type determines if the building should grow or shrink.
        if slope == 'upward':
            newHeight += heightInc
        elif slope == 'downward':
            newHeight -= heightInc
        elif slope == 'v':
            if x > SCR_WIDTH / 2:
                newHeight -= (2 * heightInc)
                # For valley slopes, buildings shrink on the left half of the screen...
            else:
                newHeight += (2 * heightInc)
                # ...and grow on the right half.
        else:
            if x > SCR_WIDTH / 2:
                newHeight += (2 * heightInc)
                # For hilley slopes, buildings grow on the left half of the screen...
            else:
                newHeight -= (2 * heightInc)
                # ...and shrink on the right half.

        # Get the new building's width.
        buildWidth = defBuildWidth #+ random.randint(0, defBuildWidth)
        if buildWidth + x > SCR_WIDTH:
            buildWidth = SCR_WIDTH - x -2

        # Get the new building's height
        buildHeight = 100 #newHeight + random.randint(heightInc, randomHeightDiff)

        # Check if the height is too high.
        if bottomLine - buildHeight <= gHeight:
            buildHeight = gHeight

        # Randomly select one of the building colors.
        buildingColor = BUILDING_COLORS[random.randint(0, len(BUILDING_COLORS)-1)]

        # Draw the building
        pygame.draw.rect(screenSurf, buildingColor, (x+1, bottomLine - (buildHeight+1), buildWidth-1, buildHeight-1))

        buildingCoords.append( (x, bottomLine - buildHeight) )

        # Draw the windows
        for winx in range(3, buildWidth - windowSpacingX + windowWidth, windowSpacingX):
            for winy in range(3, buildHeight - windowSpacingY, windowSpacingY):
                if random.randint(1, 4) == 1:
                    winColor = DARK_WINDOW
                else:
                    winColor = LIGHT_WINDOW
                pygame.draw.rect(screenSurf, winColor, (x + 1 + winx, (bottomLine - buildHeight) + 1 + winy, windowWidth, windowHeight))

        x += buildWidth

    # We want to return both the surface object we've drawn the buildings on, and the coordinates of each building.
    return screenSurf, buildingCoords

def placeGorillas(buildCoords):
    """Using the buildingCoords value returned from makeCityScape(), we want to place the gorillas on the left and right
    side of the screen on the second or third building from the edge."""

    gorPos = [] # item 0 is for (left, top) of player one, item 1 is for player two.
    xAdj = int(GOR_DOWN_SURF.get_rect().width / 2)
    yAdj = GOR_DOWN_SURF.get_rect().height

    for i in range(0,2): # place first and then second player

        # place 1st gorillas on 2nd building
        if i == 0:
            buildNum = 1#random.randint(1,2)
        else:
            #TODO change starting position
            # Place angry gorrilla randomly on the last 3 building on the right
            buildNum = random.randint(len(buildCoords)-3, len(buildCoords)-1)

        buildWidth = buildCoords[buildNum][0] - buildCoords[buildNum-1][0]
        gorPosX = buildCoords[buildNum][0] + int(buildWidth / 2) - xAdj
        gorPosY = buildCoords[buildNum][1] - yAdj - 1
        gorPos.append([gorPosX,gorPosY])

    # The format of the gorPos list is [(p1 x, p1 y), (p2 x, p2 y)]
    return gorPos

def waitForPlayerToPressKey():
    """Calling this function will pause the program until the user presses a key. The key is returned."""
    while True:
        key = checkForKeyPress()
        if key:
            return key

def checkForKeyPress():
    """Calling this function will check if a key has recently been pressed. If so, the key is returned.
    If not, then False is returned. If the Esc key was pressed, then the program terminates."""
    for event in pygame.event.get():
        if event.type == QUIT:
            terminate()
        if event.type == KEYUP:
            if event.key == K_ESCAPE: # pressing escape quits
                terminate()
            return event.key
    return False

def showStartScreen(screenSurf):
    """Draws the starting introductory screen to screenSurf, with red stars rotating around the border. This screen
    remains until the user presses a key."""
    vertAdj = 0
    horAdj = 0
    while not checkForKeyPress():
        screenSurf.fill(BLACK_COLOR)

        drawStars(screenSurf, vertAdj, horAdj)
        vertAdj += 1
        if vertAdj == 4: vertAdj = 0
        horAdj += 12
        if horAdj == 84: horAdj = 0
        """The stars on the sides of the screen move 1 pixel each iteration through this loop and reset every 4
        pixels. The stars on the top and bottom of the screen move 12 pixels each iteration and reset every 84 pixels."""

        drawText('P  y  t  h  o  n     G  O  R  I  L  L  A  S', screenSurf, SCR_WIDTH / 2, 50, WHITE_COLOR, BLACK_COLOR, pos='center')
        drawText('Your mission is to hit your opponent with the exploding', screenSurf, SCR_WIDTH / 2, 110, GRAY_COLOR, BLACK_COLOR, pos='center')
        drawText('banana by varying the angle and power of your throw, taking', screenSurf, SCR_WIDTH / 2, 130, GRAY_COLOR, BLACK_COLOR, pos='center')
        drawText('into account wind speed, gravity, and the city skyline.', screenSurf, SCR_WIDTH / 2, 150, GRAY_COLOR, BLACK_COLOR, pos='center')
        drawText('The wind speed is shown by a directional arrow at the bottom', screenSurf, SCR_WIDTH / 2, 170, GRAY_COLOR, BLACK_COLOR, pos='center')
        drawText('of the playing field, its length relative to its strength.', screenSurf, SCR_WIDTH / 2, 190, GRAY_COLOR, BLACK_COLOR, pos='center')
        drawText('Press any key to continue', screenSurf, SCR_WIDTH / 2, 300, GRAY_COLOR, BLACK_COLOR, pos='center')

        pygame.display.update()
        GAME_CLOCK.tick(FPS)

def showGameOverScreen(screenSurf, p1name, p1score, p2name, p2score):
    """Draws the game over screen to screenSurf, showing the players' names and scores. This screen has rotating
    red stars too, and hangs around until the user presses a key."""
    p1score = str(p1score)
    p2score = str(p2score)
    vertAdj = 0
    horAdj = 0
    while not checkForKeyPress():
        screenSurf.fill(BLACK_COLOR)

        drawStars(screenSurf, vertAdj, horAdj)
        vertAdj += 1
        if vertAdj == 4: vertAdj = 0
        horAdj += 12
        if horAdj == 84: horAdj = 0
        """The stars on the sides of the screen move 1 pixel each iteration through this loop and reset every 4
        pixels. The stars on the top and bottom of the screen move 12 pixels each iteration and reset every 84 pixels."""

        drawText('GAME OVER!', screenSurf, SCR_WIDTH / 2, 120, GRAY_COLOR, BLACK_COLOR, pos='center')
        drawText('Score:', screenSurf, SCR_WIDTH / 2, 155, GRAY_COLOR, BLACK_COLOR, pos='center')
        drawText(p1name, screenSurf, 225, 170, GRAY_COLOR, BLACK_COLOR)
        drawText(p1score, screenSurf, 395, 170, GRAY_COLOR, BLACK_COLOR)
        drawText(p2name, screenSurf, 225, 185, GRAY_COLOR, BLACK_COLOR)
        drawText(p2score, screenSurf, 395, 185, GRAY_COLOR, BLACK_COLOR)
        drawText('Press any key to continue', screenSurf, SCR_WIDTH / 2, 298, GRAY_COLOR, BLACK_COLOR, pos='center')

        pygame.display.update()
        GAME_CLOCK.tick(FPS)

def drawStars(screenSurf, vertAdj, horAdj):
    """This function draws the red stars on the border of screenSurf."""
    for i in range(16):
        # draw top row of stars
        screenSurf.blit(STAR_SURF, (2 + (((3 - vertAdj) + i * 4) * STAR_SURF.get_width()), 3))
        # draw bottom row of stars
        screenSurf.blit(STAR_SURF, (2 + ((vertAdj + i * 4) * STAR_SURF.get_width()), SCR_HEIGHT - 7 - STAR_SURF.get_height()))

    for i in range(4):
        # draw left column of stars going down
        screenSurf.blit(STAR_SURF, (5, 6 + STAR_SURF.get_height() + (horAdj + i * 84)))
        # draw right column of stars going up
        screenSurf.blit(STAR_SURF, (SCR_WIDTH - 5 - STAR_SURF.get_width(), (SCR_HEIGHT - (6 + STAR_SURF.get_height() + (horAdj + i * 84)))))



def showSettingsScreen(screenSurf):
    """This is the screen that lets the user type in their name and settings for the game."""
    p1name = None
    p2name = None
    points = None
    gravity = None

    screenSurf.fill(BLACK_COLOR)

    while p1name is None:
        p1name = inputMode("Name of Player 1 (Default = 'Player 1'):  ", screenSurf, SCR_WIDTH / 2 - 146, 50, GRAY_COLOR, BLACK_COLOR, maxlen=10, pos='left', cursorBlink=True)
    if p1name == '':
        p1name = 'Player 1'

    while p2name is None:
        p2name = inputMode("Name of Player 2 (Default = 'Player 2'):  ", screenSurf, SCR_WIDTH / 2 - 146, 80, GRAY_COLOR, BLACK_COLOR, maxlen=10, pos='left', cursorBlink=True)
    if p2name == '':
        p2name = 'Player 2'

    while points is None:
        points = inputMode("Play to how many total points (Default = 3)?  ", screenSurf, SCR_WIDTH / 2 - 155, 110, GRAY_COLOR, BLACK_COLOR, maxlen=6, allowed='0123456789', pos='left', cursorBlink=True)
    if points == '':
        points = 3
    else:
        points = int(points)

    while gravity is None:
        gravity = inputMode("Gravity in Meters/Sec (Earth = 9.8)?  ", screenSurf, SCR_WIDTH / 2 - 150, 140, GRAY_COLOR, BLACK_COLOR, maxlen=6, allowed='0123456789.', pos='left', cursorBlink=True)
    if gravity == '':
        gravity = 9.8
    else:
        gravity = float(gravity)

    drawText('--------------', screenSurf, SCR_WIDTH / 2 -10, 170, GRAY_COLOR, BLACK_COLOR, pos='center')
    drawText('V = View Intro', screenSurf, SCR_WIDTH / 2 -10, 200, GRAY_COLOR, BLACK_COLOR, pos='center')
    drawText('P = Play Game', screenSurf, SCR_WIDTH / 2 -10, 230, GRAY_COLOR, BLACK_COLOR, pos='center')
    drawText('Your Choice?', screenSurf, SCR_WIDTH / 2 -10, 260, GRAY_COLOR, BLACK_COLOR, pos='center')
    pygame.display.update()

    key = waitForPlayerToPressKey()
    while chr(key) != 'v' and chr(key) != 'p':
        key = waitForPlayerToPressKey()

    return p1name, p2name, points, gravity, chr(key) # returns 'v' or 'p'

def showIntroScreen(screenSurf, p1name, p2name):
    """This is the screen that plays if the user selected "view intro" from the starting screen."""
    screenSurf.fill(SKY_COLOR)
    drawText('P  y  t  h  o  n     G  O  R  I  L  L  A  S', screenSurf, SCR_WIDTH / 2, 15, WHITE_COLOR, SKY_COLOR, pos='center')
    drawText('STARRING:', screenSurf, SCR_WIDTH / 2, 55, WHITE_COLOR, SKY_COLOR, pos='center')
    drawText('%s AND %s' % (p1name, p2name), screenSurf, SCR_WIDTH / 2, 115, WHITE_COLOR, SKY_COLOR, pos='center')

    x = 278
    y = 175

    for i in range(2):
        drawGorilla(screenSurf, x-13, y, RIGHT_ARM_UP)
        drawGorilla(screenSurf, x+47, y, LEFT_ARM_UP)
        pygame.display.update()

        time.sleep(2)

        drawGorilla(screenSurf, x-13, y, LEFT_ARM_UP)
        drawGorilla(screenSurf, x+47, y, RIGHT_ARM_UP)
        pygame.display.update()

        time.sleep(2)

    for i in range(4):
        drawGorilla(screenSurf, x-13, y, LEFT_ARM_UP)
        drawGorilla(screenSurf, x+47, y, RIGHT_ARM_UP)
        pygame.display.update()

        time.sleep(0.3)

        drawGorilla(screenSurf, x-13, y, RIGHT_ARM_UP)
        drawGorilla(screenSurf, x+47, y, LEFT_ARM_UP)
        pygame.display.update()

        time.sleep(0.3)


def getShot(screenSurf, p1name, p2name, playerNum):
    """getShot() is called when we want to get the angle and velocity from the player."""
    pygame.draw.rect(screenSurf, SKY_COLOR, (0, 0, 200, 50))
    pygame.draw.rect(screenSurf, SKY_COLOR, (550, 0, 00, 50))

    drawText(p1name, screenSurf, 2, 2, WHITE_COLOR, SKY_COLOR)
    drawText(p2name, screenSurf, 538, 2, WHITE_COLOR, SKY_COLOR)

    if playerNum == 1:
        x = 2
    else:
        x = 538

    angle = ''
    while angle == '':
        angle = inputMode('Angle:  ', screenSurf, x, 18, WHITE_COLOR, SKY_COLOR, maxlen=3, allowed='0123456789')
    if angle is None: terminate()
    angle = int(angle)

    velocity = ''
    while velocity == '':
        velocity = inputMode('Velocity:  ', screenSurf, x, 34, WHITE_COLOR, SKY_COLOR, maxlen=3, allowed='0123456789')
    if velocity is None: terminate()
    velocity = int(velocity)

    # Erase the user's input
    drawText('Angle:   ' + str(angle), screenSurf, x, 2, SKY_COLOR, SKY_COLOR)
    drawText('Velocity:   ' + str(angle), screenSurf, x, 2, SKY_COLOR, SKY_COLOR)
    pygame.display.update()

    if playerNum == 2:
        angle = 180 - angle

    return (angle, velocity)

def displayScore(screenSurf, oneScore, twoScore):
    """Draws the score on the screenSurf surface."""
    drawText(str(oneScore) + '>Score<' + str(twoScore), screenSurf, 270, 310, WHITE_COLOR, SKY_COLOR, pos='left')

def getShotCoords(angle,velocity, wind, gravity, gor1pos, t):
    velocity = velocity
    angle = angle / 180.0 * math.pi
    initXVel = math.cos(angle) * velocity
    initYVel = math.sin(angle) * velocity

    # initial position to banana
    startx = gor1pos[0]
    starty = gor1pos[1]
    bananaShape = 1
    starty -= getBananaRect(0, 0, bananaShape).height + BAN_UP_SURF.get_size()[1]


    x = startx + (initXVel * t) + (0.5 * (wind / 5) * t**2)
    y = starty + ((-1 * (initYVel * t)) + (0.5 * gravity * t**2))

    return x,y

''' Some globals used for plotting spinny banana '''
banana_shape = UP
def plotBanana(x,y,gorPos):
    global banana_shape
    gorWidth, gorHeight = GOR_DOWN_SURF.get_size()
    gor1rect = pygame.Rect(gorPos[0][0], gorPos[0][1], gorWidth, gorHeight)
    gor2rect = pygame.Rect(gorPos[1][0], gorPos[1][1], gorWidth, gorHeight)
    bananaRect = getBananaRect(x, y, banana_shape)
    # rotation
    if banana_shape == UP:
        bananaSurf = BAN_UP_SURF
        bananaRect.left -= 2
        bananaRect.top += 2
    elif banana_shape == DOWN:
        bananaSurf = BAN_DOWN_SURF
        bananaRect.left -= 2
        bananaRect.top += 2
    elif banana_shape == LEFT:
        bananaSurf = BAN_LEFT_SURF
    elif banana_shape == RIGHT:
        bananaSurf = BAN_RIGHT_SURF

    banana_shape = nextBananaShape(banana_shape)
    srcPixArray = pygame.PixelArray(skyline_surf)
    if y > 0:
        if sunRect.collidepoint(x, y):
            # banana has hit the sun, so draw the "shocked" face.
            sunHit = True
            # draw the appropriate sun face
            drawSun(win_surface, shocked=sunHit)

        if bananaRect.colliderect(gor1rect):
            # banana has hit player 1
            """Note that we draw the explosion on the screen (on screenSurf) and on the separate skyline surface (on skylineSurf).
            This is done so that bananas won't hit the sun or any text and accidentally think they've hit something. We also want
            the skylineSurf surface object to keep track of what chunks of the buildings are left."""
            doExplosion(win_surface, skyline_surf, bananaRect.centerx, bananaRect.centery, explosionSize=int(GOR_EXPLOSION_SIZE*2/3), speed=0.005)
            doExplosion(win_surface, skyline_surf, bananaRect.centerx, bananaRect.centery, explosionSize=GOR_EXPLOSION_SIZE, speed=0.005)
            drawSun(win_surface)
            return 'gorilla1'
        elif bananaRect.colliderect(gor2rect):
            # banana has hit player 2
            doExplosion(win_surface, skyline_surf, bananaRect.centerx, bananaRect.centery, explosionSize=int(GOR_EXPLOSION_SIZE*2/3), speed=0.005)
            doExplosion(win_surface, skyline_surf, bananaRect.centerx, bananaRect.centery, explosionSize=GOR_EXPLOSION_SIZE, speed=0.005)
            win_surface.fill(SKY_COLOR, bananaRect) # erase banana
            drawSun(win_surface)
            return 'gorilla2'
        elif collideWithNonColor(srcPixArray, win_surface, bananaRect, SKY_COLOR):
            # banana has hit a building
            doExplosion(win_surface, skyline_surf, bananaRect.centerx, bananaRect.centery)
            win_surface.fill(SKY_COLOR, bananaRect) # erase banana
            drawSun(win_surface)
            return 'building'
        #elif contactGorrilla(gor1rect,gor2rect) or gorPos[0][0]+gorWidth > gorPos[1][0]:
        #    return "collide"
    """Pygame doesn't let us blit a surface while there is a pixel array of it existing, so we delete it."""
    del srcPixArray
    win_surface.blit(bananaSurf, (bananaRect.topleft))
    # OUT OF PLAY
    if x >= SCR_WIDTH - 10 or x <= 3 or y >= SCR_HEIGHT:
        win_surface.fill(SKY_COLOR, bananaRect) # erase banana
        return "miss"
    return ""

def throwPosUP(screenSurf, gor1):

    # drawing
    gorImg = LEFT_ARM_UP
    drawGorilla(screenSurf, gor1[0], gor1[1], gorImg)

def throwPosDOWN(screenSurf, gor1):

    # drawing
    gorImg = BOTH_ARMS_DOWN
    drawGorilla(screenSurf, gor1[0], gor1[1], gorImg)


''' Original Plot Shot '''
def plotShot(screenSurf, skylineSurf, angle, velocity, playerNum, wind, gravity, gor1, gor2):
    # startx and starty is the upper left corner of the gorilla.
    angle = angle / 180.0 * math.pi
    initXVel = math.cos(angle) * velocity
    initYVel = math.sin(angle) * velocity
    gorWidth, gorHeight = GOR_DOWN_SURF.get_size()
    gor1rect = pygame.Rect(gor1[0], gor1[1], gorWidth, gorHeight)
    gor2rect = pygame.Rect(gor2[0], gor2[1], gorWidth, gorHeight)


    if playerNum == 1:
        gorImg = LEFT_ARM_UP
    else:
        gorImg = RIGHT_ARM_UP
    """The player 1 gorilla on the left uses his left arm to throw, the player 2 gorilla on the right uses his
    right arm to throw."""

    if playerNum == 1:
        startx = gor1[0]
        starty = gor1[1]
    elif playerNum == 2:
        startx = gor2[0]
        starty = gor2[1]

    drawGorilla(screenSurf, startx, starty, gorImg)
    pygame.display.update()
    time.sleep(0.3)
    drawGorilla(screenSurf, startx, starty, BOTH_ARMS_DOWN)
    pygame.display.update()
    """Draw the gorilla throwing the banana."""

    bananaShape = UP

    if playerNum == 2:
        startx += GOR_DOWN_SURF.get_size()[0]

    starty -= getBananaRect(0, 0, bananaShape).height + BAN_UP_SURF.get_size()[1]

    impact = False
    bananaInPlay = True

    t = 1.0
    sunHit = False

    while not impact and bananaInPlay:
        x = startx + (initXVel * t) + (0.5 * (wind / 5) * t**2)
        y = starty + ((-1 * (initYVel * t)) + (0.5 * gravity * t**2))
        """This is basically the equation that describes the banana's arc."""

        if x >= SCR_WIDTH - 10 or x <= 3 or y >= SCR_HEIGHT:
            bananaInPlay = False

        bananaRect = getBananaRect(x, y, bananaShape)
        if bananaShape == UP:
            bananaSurf = BAN_UP_SURF
            bananaRect.left -= 2
            bananaRect.top += 2
        elif bananaShape == DOWN:
            bananaSurf = BAN_DOWN_SURF
            bananaRect.left -= 2
            bananaRect.top += 2
        elif bananaShape == LEFT:
            bananaSurf = BAN_LEFT_SURF
        elif bananaShape == RIGHT:
            bananaSurf = BAN_RIGHT_SURF

        bananaShape = nextBananaShape(bananaShape)

        srcPixArray = pygame.PixelArray(skylineSurf)
        if bananaInPlay and y > 0:

            if sunRect.collidepoint(x, y):
                # banana has hit the sun, so draw the "shocked" face.
                sunHit = True

            # draw the appropriate sun face
            drawSun(screenSurf, shocked=sunHit)

            if bananaRect.colliderect(gor1rect):
                # banana has hit player 1

                """Note that we draw the explosion on the screen (on screenSurf) and on the separate skyline surface (on skylineSurf).
                This is done so that bananas won't hit the sun or any text and accidentally think they've hit something. We also want
                the skylineSurf surface object to keep track of what chunks of the buildings are left."""
                doExplosion(screenSurf, skylineSurf, bananaRect.centerx, bananaRect.centery, explosionSize=int(GOR_EXPLOSION_SIZE*2/3), speed=0.005)
                doExplosion(screenSurf, skylineSurf, bananaRect.centerx, bananaRect.centery, explosionSize=GOR_EXPLOSION_SIZE, speed=0.005)
                drawSun(screenSurf)
                return 'gorilla1'
            elif bananaRect.colliderect(gor2rect):
                # banana has hit player 2
                doExplosion(screenSurf, skylineSurf, bananaRect.centerx, bananaRect.centery, explosionSize=int(GOR_EXPLOSION_SIZE*2/3), speed=0.005)
                doExplosion(screenSurf, skylineSurf, bananaRect.centerx, bananaRect.centery, explosionSize=GOR_EXPLOSION_SIZE, speed=0.005)
                screenSurf.fill(SKY_COLOR, bananaRect) # erase banana
                drawSun(screenSurf)
                return 'gorilla2'
            elif collideWithNonColor(srcPixArray, screenSurf, bananaRect, SKY_COLOR):
                # banana has hit a building
                doExplosion(screenSurf, skylineSurf, bananaRect.centerx, bananaRect.centery)
                screenSurf.fill(SKY_COLOR, bananaRect) # erase banana
                drawSun(screenSurf)
                return 'building'

        del srcPixArray
        """Pygame doesn't let us blit a surface while there is a pixel array of it existing, so we delete it."""

        screenSurf.blit(bananaSurf, (bananaRect.topleft))
        pygame.display.update()
        time.sleep(0.02)

        screenSurf.fill(SKY_COLOR, bananaRect) # erase banana

        t += 0.1 # go forward in the plot.
    drawSun(screenSurf)
    return 'miss'


def victoryDance(screenSurf, x, y):
    """Given the x,y coordinates of the topleft corner of the gorilla sprite, this goes through
    the victory dance routine of the gorilla where they start waving their arms in the air."""
    for i in range(4):
        screenSurf.blit(GOR_LEFT_SURF, (x, y))
        pygame.display.update()
        time.sleep(0.3)
        screenSurf.blit(GOR_RIGHT_SURF, (x, y))
        pygame.display.update()
        time.sleep(0.3)


def collideWithNonColor(pixArr, surfObj, rect, color):
    """This checks the area (described by "rect") on pixArr (a pixel array derived from the surfObj surface object)
    if it has any pixels that are not the color specified by the "color" parameter. This function is used to detect
    if the banana has hit any non-sky colored parts (which means a gorilla or a building)."""
    rightSide = min(rect.right, SCR_WIDTH)
    bottomSide = min(rect.bottom, SCR_HEIGHT)

    for x in range(rect.left, rightSide):
        for y in range(rect.top, bottomSide):
            if surfObj.unmap_rgb(pixArr[x][y]) != color:
                return True
    return False


def getBananaRect(x, y, shape):
    if shape == UP:
        return pygame.Rect((x, y), BAN_UP_SURF.get_size())
    if shape == DOWN:
        return pygame.Rect((x, y), BAN_DOWN_SURF.get_size())
    if shape == LEFT:
        return pygame.Rect((x, y), BAN_LEFT_SURF.get_size())
    if shape == RIGHT:
        return pygame.Rect((x, y), BAN_RIGHT_SURF.get_size())

def getWind():
    """Randomly determine what the wind speed and direction should be for this game."""
    wind = random.randint(5, 15)
    if random.randint(0, 1):
        wind *= -1
    wind = 0 # wind is nothing now
    return wind

def drawWind(screenSurf, wind):
    """Draws the wind arrow on the screenSurf object at the bottom of the screen. The "wind" parameter comes from
    a call to getWind()."""
    if wind != 0:
        wind *= 3
        pygame.draw.line(screenSurf, EXPLOSION_COLOR, (int(SCR_WIDTH / 2), SCR_HEIGHT - 5), (int(SCR_WIDTH / 2) + wind, SCR_HEIGHT - 5))
        # draw the arrow end
        if wind > 0: arrowDir = -2
        else:        arrowDir = 2
        pygame.draw.line(screenSurf, EXPLOSION_COLOR, (int(SCR_WIDTH / 2) + wind, SCR_HEIGHT - 5), (int(SCR_WIDTH / 2) + wind + arrowDir, SCR_HEIGHT - 5 - 2))
        pygame.draw.line(screenSurf, EXPLOSION_COLOR, (int(SCR_WIDTH / 2) + wind, SCR_HEIGHT - 5), (int(SCR_WIDTH / 2) + wind + arrowDir, SCR_HEIGHT - 5 + 2))

paused_for = 0
def doExplosion(screenSurf, skylineSurf, x, y, explosionSize=BUILD_EXPLOSION_SIZE, speed=0.05):
    for r in range(1, explosionSize):
        pygame.draw.circle(screenSurf, EXPLOSION_COLOR, (x, y), r)
        pygame.draw.circle(skylineSurf, EXPLOSION_COLOR, (x, y), r)
        #pygame.display.update()
        #paused_for = speed/2.0
        #time.sleep(paused_for)

    for r in range(explosionSize, 1, -1):
        pygame.draw.circle(screenSurf, SKY_COLOR, (x, y), explosionSize)
        pygame.draw.circle(skylineSurf, SKY_COLOR, (x, y), explosionSize)
        pygame.draw.circle(screenSurf, EXPLOSION_COLOR, (x, y), r)
        pygame.draw.circle(skylineSurf, EXPLOSION_COLOR, (x, y), r)
        #pygame.display.update()
        #paused_for = speed/2.0
        #time.sleep(paused_for)

    pygame.draw.circle(screenSurf, SKY_COLOR, (x, y), 2)
    pygame.draw.circle(skylineSurf, SKY_COLOR, (x, y), 2)

def contactGorrilla(gorPos):
    gorWidth, gorHeight = GOR_DOWN_SURF.get_size()
    gor1rect = pygame.Rect(gorPos[0][0], gorPos[0][1], gorWidth, gorHeight)
    gor2rect = pygame.Rect(gorPos[1][0], gorPos[1][1], gorWidth, gorHeight)
    return gor1rect.colliderect(gor2rect) or (gorPos[0][0] + gorWidth > gorPos[1][0])

# Quick hack to get things done quick
class Gorilla:
    DEFAULT_SPEED = 0.01   # pixel/microsec
    DEFAULT_ACCEL = 0.0001   # pixel/microsec
    ''' Good range for accel is 0.000001 to 0.000001'''

    def __init__(self):
        ''' Fix speed and acceleration for now '''
        self.speed = Gorilla.DEFAULT_SPEED
        self.accel = Gorilla.DEFAULT_ACCEL
        self.pos= None # only the X

    def resetSpeed(self,noisy=False):
        noise = 0
        ''' Include some noise to make the game harder '''
        if noisy == True:
            ''' Keep variance small for the game to be realistic '''
            sigma = 0.1
            noise = abs(sigma * numpy.random.randn())
        self.speed = Gorilla.DEFAULT_SPEED + noise
        self.accel = Gorilla.DEFAULT_ACCEL

    def setPos(self,pos):
        self.pos = pos

    def updateGorrillaMovement(self,time_passed):
        dist = self.speed*time_passed + (1/2)*self.accel*time_passed**2
        self.speed = self.speed + self.accel*time_passed
        self.pos = self.pos - dist
        return self.pos
    

#################################################
### (START) Actor Critic SANSA - AC-S (START) ###
#################################################
    
#######################################
#####''' MDP related stuff ''' ########
#######################################
class Environment:
    def __init__(self,gorPos=None):
        self.wind       = getWind()
        self.opponent   = Gorilla()
        self.gorPos     = gorPos
        # banana related
        self.bananaInPlay = False
        # use for plotting banana in play
        self.bananaIteration = 0
        self.angle = None
        self.power = None
        # State of the environment

    def getState(self):
        return State(self.opponent.pos,self.opponent.speed,self.bananaInPlay)

    def resetEnv(self,buildCoords):
        self.gorPos     = placeGorillas(buildCoords)
        self.wind       = getWind()
        self.opponent.setPos(self.gorPos[1][0])
        self.opponent.resetSpeed(True)

        self.bananaInPlay = False
        self.bananaIteration = 0
        self.angle = None
        self.power = None

    def moveOpponent(self,time_passed):
        self.gorPos[1][0] = self.opponent.updateGorrillaMovement(time_passed)

    def run(self,time_passed):

        outcome = ""
        # move banana if in play
        if self.bananaInPlay:
            self.bananaIteration = self.bananaIteration + 1
            if self.bananaIteration < 20:
                throwPosUP(win_surface, self.gorPos[0])
            else:
                throwPosDOWN(win_surface, self.gorPos[0])
            # coordinates of Banana
            x,y = getShotCoords(self.angle, self.power, self.wind, 9.8, self.gorPos[0],0.1*self.bananaIteration)
            outcome = plotBanana(x,y,self.gorPos)

            if outcome is not "":
                self.bananaIteration = 0
                self.bananaInPlay = False

        if contactGorrilla(self.gorPos):
            outcome = "collide"
        # move gorilla
        self.moveOpponent(time_passed)
        return outcome

    # Action stuff goes here
    # (None) == wait otherwise throw
    def do(self,a):
        if a != (None,None):
            self.bananaInPlay = True
            self.angle = a[0]
            self.power = a[1]

    def getReward(self,outcome):
        reward = None
        if outcome == "collide":
            reward = -3 
        elif outcome == "gorilla1":
            reward = -2 # not possible to happen
        elif outcome == "gorilla2":
            reward = 10
        elif outcome == "building":
            reward = -2
        elif outcome == "miss":
            reward = -2 #-0.5
        elif outcome == "":
            reward = 0
        return reward

class Episode:
    def __init__(self,env):
        self.states = []
        self.actions= []
        self.rewards= []
        self.env = env

    def currentState(self):
        return self.states[len(self.states)-1]

    def prevState(self):
        if len(self.states) > 1:
            return self.states[len(self.states)-2]
        return None


    def resetEpisode(self):
        self.states=[]
        pos   = self.env.opponent.pos
        speed = self.env.opponent.speed
        ''' init first state '''
        self.states.append(State(pos,speed,False))

    def run(self):
        currentState.chooseAction()

    def returnReward(self):
        pass

    def addState(self,state):
        self.states.append(state)

class State:
    def __init__(self,pos,speed,bananaInPlay):
        self.pos            = pos
        self.speed          = speed
        self.bananaInPlay   = bananaInPlay


    def getActions(self):
        if self.bananaInPlay:
            return [0] # wait = 0
        else:
            return range(Actions.numActions)
    def getContinousActions(self):
        if self.bananaInPlay:
            return [0] # wait = 0
        else:
            # if throw is selected,
            return [0,1] # 0 = wait, 1 = throw

    def getStateVar(self):
        return (self.pos,self.speed,self.bananaInPlay)

    def __str__(self):
        return  str(self.pos)+", "+ \
                str(self.speed)+", "+ \
                str(self.bananaInPlay)

class TileCoder:
    '''Tilings stuff'''
    ''' This is only written for 2D tiles '''
    # TODO - Move these somewhere...too many constants
    numTilings  = 20
    nGrids      = 30
    posRange    = (0,640)
    spdRange    = (0,0.4)
    posRangeN    = posRange[1] - posRange[0]
    spdRangeN    = spdRange[1] - spdRange[0] # cast spd to integer range to minimize floating point error

    rowWidth    = posRangeN/nGrids
    colWidth    = spdRangeN/nGrids

    ''' Only 2 state variables '''
    numRowTile = nGrids + 1 # +1 is to have extra to shift tiles by
    numColTile = nGrids + 1 # RL book pg 206 for more info
    numTilesIdx= numRowTile * numColTile * numTilings # total number of tiles used in binary vector

    rowShiftBy  = rowWidth/numTilings
    colShiftBy  = colWidth/numTilings

    def __init__(self):
        pass

    def tilecode(self,x1, x2):
        # Check out of bounds error

        if  not (TileCoder.posRange[0] <= x1 <= TileCoder.posRange[1] and \
            TileCoder.spdRange[0] <= x2 <= TileCoder.spdRange[1]):
            x1 = 0
            x2 = 0.4
            #raise Exception("State variables out of bounds ("+str(x1)+","+str(x2)+")")

        tilecodeIndices = [0.0]*TileCoder.numTilings

        for i in xrange(TileCoder.numTilings):
            idx1 = math.floor((x1+TileCoder.rowShiftBy*i)/TileCoder.rowWidth)
            idx2 = math.floor((x2+TileCoder.colShiftBy*i)/TileCoder.colWidth)
            offset = TileCoder.numRowTile * TileCoder.numColTile * i
            idxTile = idx1*TileCoder.numRowTile + idx2 + offset
            tilecodeIndices[i] = int(idxTile)
        return tilecodeIndices

##########################
### The learning agent ###
##########################
### Agent will ignore bananaInPlay since they are not "interesting" ###


class Agent:


    # for continous agent
    ANGLE_FIXED = 45  # fixed angle
    epsVar = 1
    beta = 1
    beta1 = 1

    def __init__(self,env,alpha=0.1,gamma=0.9,lambd=0.9,epsilon=0.01):
        self.env = env
        self.tileCoder = TileCoder()
        self.epsilon = epsilon
        self.alpha = alpha
        self.lambd = lambd
        self.gamma = gamma


        ''' Foreach a, we have a set of tile codings for Q(s,a)  '''
        self.thetas = [None]*Actions.numActions

        self.etrace = [None]*Actions.numActions
        for a in xrange(Actions.numActions):
            # representation of state action space
            self.thetas[a] = numpy.zeros(self.tileCoder.numTilesIdx)
            self.etrace[a] = numpy.zeros(self.tileCoder.numTilesIdx)

         # for continous action; we fix angle, and vary on power for simplicity
         # 40 for initial power
        self.actorThetas = numpy.ones(self.tileCoder.numTilesIdx) * 40/TileCoder.numTilings
        self.actorVar = numpy.ones(self.tileCoder.numTilesIdx) * 2/TileCoder.numTilings

    def resetEpisode(self):
        self.etrace = [None]*Actions.numActions
        for a in xrange(Actions.numActions):
            self.etrace[a] = numpy.zeros(self.tileCoder.numTilesIdx)


    def learn(self,lasts,lasta,r,s,a):
        self.learnSarsa(lasts,lasta,r,s,a)


    def learnDiscrete(self,lasts,lasta,r,s,a):
        self.learnSarsa(lasts,lasta,r,s,a)

    def learnContinous(self,lasts,lasta,r,s,a):
        self.learnModSarsa(lasts,lasta,r,s,a)


    def computeQ(self,state,a):
        tcodes = self.tileCoder.tilecode(state.pos,state.speed)
        qVal = 0
        for tc in tcodes:
            qVal = self.thetas[a][tc] + qVal
        return qVal

    def computeU(self,state):
        tcodes = self.tileCoder.tilecode(state.pos,state.speed)
        uVal = 0
        vVal = 0
        for tc in tcodes:
            uVal = self.actorThetas[tc] + uVal
            uVal = self.actorVar[tc] + uVal
        return uVal,vVal

    def chooseAction(self,state):
        action = self.chooseDiscreteAction(state)
        return action

    def chooseDiscreteAction(self,state):
        "Chooses next action"
        '''
            tcodes are common between actions
            Not considering joint actions
        '''
        availActions = state.getActions()
        qValues = [None for i in range(Actions.numActions)]
        for a in availActions:
            qValues[a] = self.computeQ(state,a)		# compute action values
        # choose epsilon greedy
        chooseA = self.egreedy(availActions,qValues)

        if len(availActions) != 1 and VERBOSE == True:
            print "Qvals:",qValues,"Choose:",chooseA
        # print "Agent chose action", chooseA, "qValues", qValues
        return Actions.actions[chooseA]

    def chooseContinousAction(self,state):
        "Chooses next action"
        '''
            tcodes are common between actions
            Not considering joint actions
        '''

        availActions = state.getContinousActions()
        qValues = [None for i in range(Actions.numContActions)]
        for a in availActions:
            qValues[a] = self.computeQ(state,a)	# compute action values
        # choose discrete based on epsilon greedy
        chooseA = self.egreedy(availActions,qValues)

        if len(availActions) != 1 and VERBOSE == True:
            print "Qvals:",qValues,"Choose:",chooseA

        if chooseA == 0: # wait is chosen
            return Actions.contActions[chooseA]

        # decide to throw, compute base on parameters
        mu,var = self.computeU(state)
        #print "MU:",mu,Agent.epsVar
        power = random.gauss(mu, var)
        # print "Agent chose action", chooseA, "qValues", qValues
        return (Agent.ANGLE_FIXED,power)

    def egreedy(self,availActions,qValues):
        eps = self.epsilon
        if random.random() > eps:
            m = max(qValues)
            actionIdx = [i for i, j in enumerate(qValues) if j == m]
            return random.choice(actionIdx)
        else:
            return random.choice(availActions)

    def randomAction(self,state):
        return random.choice(state.getActions())

    def randomContAction(self,state):
        a = None,None # none indicate wait

        if random.random() > 0.9:
            power   = random.randint(20,60)
            angle   = random.randint(30,60)
            a = angle,power
        return a

    def learnSarsa(self,lasts, lasta, r, s, a):
        # choose greedy
        #print "Sarsa:",lasts,lasta,r,s,a
        if lasts == None or lasta ==None:
            return

        lasta =  Actions.actions.index(lasta)
        a =  Actions.actions.index(a)

        tc = self.tileCoder.tilecode(lasts.pos,lasts.speed)

        # update trace
        for ai in range(Actions.numActions):
            self.etrace[ai] = self.gamma * self.lambd * self.etrace[ai]

        global ACCUM_TRACE
        for i in tc:
            if ACCUM_TRACE == True:
                self.etrace[lasta][i] = self.etrace[lasta][i] + 1 # acculmating trace
            else:
                self.etrace[lasta][i] = 1 # replacing traces

        tdError = r - self.computeQ(lasts,lasta) + self.gamma * self.computeQ(s,a)
        amt = tdError * (self.alpha/self.tileCoder.numTilings)

        for ai in range(Actions.numActions):
            self.thetas[ai] = self.thetas[ai] + amt * self.etrace[ai]

    def learnModSarsa(self,lasts, lasta, r, s, a):
        # choose greedy
        #print "Sarsa:",lasts,lasta,r,s,a
        if lasts == None or lasta ==None:
            return

        angle,power = lasta
        if lasta == (None,None):
            lasta = 0
        else:
            lasta = 1

        if a == (None,None):
            a = 0
        else:
            a = 1

        tc = self.tileCoder.tilecode(lasts.pos,lasts.speed)

        # update trace
        for ai in range(Actions.numContActions):
            self.etrace[ai] = self.gamma * self.lambd * self.etrace[ai]

        for i in tc:
            self.etrace[lasta][i] = self.etrace[lasta][i] + 1 # acculmating trace

        tdError = r - self.computeQ(lasts,lasta) + self.gamma * self.computeQ(s,a)
        amt = tdError * (self.alpha/self.tileCoder.numTilings)

        for ai in range(Actions.numContActions):
            self.thetas[ai] = self.thetas[ai] + amt * self.etrace[ai]

        # only update if we find something good
        if power != None:
            #print "--TD--:",tdError
            if tdError > 0:
                #before = 0
                #after = 0
                for i in tc:
                    #before = before + self.actorThetas[i]
                    self.actorThetas[i] = self.actorThetas[i] + Agent.beta*(power/TileCoder.numTilings - self.actorThetas[i])
                    self.actorVar[i]    = self.actorVar[i] * 0.5
            else:
                for i in tc:
                    self.actorVar[i]    = self.actorVar[i] * 1.2


class Actions:
    ''' discrete actions '''
    ''' wait and throw with some angle velocity '''
    ''' (ANGLE,POWER), (0,0) or (None,None) = wait '''
    actions = [(None,None),(45,46),(45,36)]
    contActions = [(None,None),(-1,-1)]
    numActions = len(actions)
    # in this case (-1,-1) means throw
    numContActions = len(contActions)

    ''' Continous Actions '''
    minAngle = 40
    minPower = 30
    maxAngle = 60
    maxPower = 60

##########################################
''' END -- MDP RL related stuff -- END '''
##########################################

def doRun(numEpisodes=100,alpha=0.1,gamma=0.9,lambd=0.9,epsilon=0.01,continous=False):
    print "---------- START OF RUN ----------"
    ActionType = "Discete" if continous == False else "Continous"
    print "---------- (",ActionType,") ----------"
    env = Environment();
    agent = Agent(env,alpha,gamma,lambd,epsilon);
    global skyline_surf, build_coords,win_surface, paused_for
    win_surface = pygame.display.set_mode((SCR_WIDTH, SCR_HEIGHT), 0, 32)
    """winSurface, being the surface object returned by pygame.display.set_mode(), will be drawn to the screen
    every time pygame.display.update() is called."""

    # Uncomment either of the following lines to put the game into full screen mode.
    ##winSurface = pygame.display.set_mode((SCR_WIDTH, SCR_HEIGHT), pygame.FULLSCREEN, 32)
    ##pygame.display.toggle_fullscreen()
    pygame.display.set_caption('Gorillas.py')
    # START SCREEN
    #showStartScreen(win_surface)
    #p1name, p2name, winPoints, gravity, nextScreen = showSettingsScreen(win_surface)
    p1name, p2name, winPoints, gravity, nextScreen = ['p1','p2','1','9.8','p']

    skyline_surf, build_coords = makeCityScape() # Note that the city skyline goes on skylineSurf, not winSurface.
    env.resetEnv(build_coords)

    p1score = 0
    p2score = 0

    epd = Episode(env);
    epd.resetEpisode()
    """ For TESTING...REMOVE """

    #while t < numOfEpisodes:
     #   t = t + 1
      #  resetEpisode()
       # startEpisode()

    t = 0
    lasta = None
    lasts = None
    s = None
    a = None
    totalReward = 0
    lastReward = 0
    outcome = ""
    run = 0
    rewardsFull = []
    rewards = []

    while run < numEpisodes:
        # Check if terminal

        if outcome in ["collide","gorilla1","gorilla2"]:
            if continous:
                agent.learnModSarsa(lasts,lasta,lastReward,s,a)
            else:
                agent.learnSarsa(lasts,lasta,lastReward,s,a)
            agent.resetEpisode()
            #print "Sarsa:",lasts,lasta,lastReward,s,a
            if VERBOSE == True:
                print "REWARD = ",totalReward
                print "#Run:",run,"  #Visits:",t
                print "-- End of Episode:",run,"--"
            t = 0
            epd.addState(s)
            skyline_surf, build_coords = makeCityScape()
            env.resetEnv(build_coords)
            epd.resetEpisode()
            lasts = None
            lasta = None
            a = None
            s = None
            outcome = ""
            totalReward = 0
            run = run + 1
            rewardsFull.append(rewards)
            rewards = []
            if run <= numEpisodes:
                continue


        t = t + 1
        # Limit frame speed to 30 FPS (microsec)
        if SPEEDUP == False:
            time_passed = GAME_CLOCK.tick(FPS)
        else:
            time_passed = 25
        #

        # End the game when user click on X
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                exit_game()

        # drawing
        if DISPLAY == True:
            win_surface.blit(skyline_surf, (0,0))
            drawGorilla(win_surface, env.gorPos[0][0], env.gorPos[0][1], 0)
            drawGorilla(win_surface, env.gorPos[1][0], env.gorPos[1][1], 0)
            drawWind(win_surface, env.wind) # Don't need wind in this game
            drawSun(win_surface)
            displayScore(win_surface, p1score, p2score)

        # interested in states with more than 1 action
        if s != None and s.bananaInPlay == False :
            lasts = s
            lasta = a

        s = env.getState()
        epd.addState(s)
        # Agent choose action base on current state

        if continous:
            a = agent.chooseContinousAction(s)
        else:
            a = agent.chooseDiscreteAction(s)

        # Agent learning
        if s.bananaInPlay == False:
            if continous:
                agent.learnContinous(lasts,lasta,lastReward,s,a)
            else:
                agent.learnDiscrete(lasts,lasta,lastReward,s,a)
            #print "Sarsa:",lasts,lasta,lastReward,s,a

        #if s.bananaInPlay == False :
        #    print "St:",s,a

        #agent.learn(lasts,lasta,lastReward,s,a)
        # doing action at the current state
        env.do(a)
        outcome = env.run(time_passed)
        lastReward = env.getReward(outcome)
        rewards.append(lastReward)
        totalReward = totalReward + lastReward

        if VERBOSE == True:
            if outcome != "" or s.bananaInPlay == False:
                print t,":",s, '--->',a,'(',lastReward,') -- ',outcome

        # score for visual
        if outcome == "collide" or outcome == "gorilla1":
            p2score = p2score + 1
        elif outcome == "gorilla2":
            p1score = p1score + 1

        # Update all drawings
        if DISPLAY == True:
            pygame.display.update()
    return rewardsFull


''' Handle game exit '''
def exit_game():
    sys.exit()

def doTrial(numTrials,numEpisodes=100,alpha=0.1,gamma=0.9,lambd=0.9,epsilon=0.01,continous=False):
    results = []
    for i in range(numTrials):
        print " -- Trial",i+1,"--"
        result = doRun(numEpisodes,alpha,gamma,lambd,epsilon,continous)
        print "Reward:",getRewardTrial(result)
        results.append(result)
    return results

def getRewardTrial(result,epds=None):
    ''' result is 1 trial '''
    if epds == None:
        epds = len(result)
    total = 0.0
    for i in range(epds):
        total = total + sum(result[i])
    return total,total/epds

def getAvgTrialReward(result):
    numTrials = len(result)
    numEpds = len(result[0])

    avgList = []
    for j in range(numEpds):
        total = 0.0
        for i in range(numTrials):
            total = total + sum(result[i][j])
        avgList.append(total/numTrials)
    return avgList


def main():
    global ACCUM_TRACE
    numTrial = 30
    numEpd = 150

    alpha=0.1
    gamma=0.9
    lambd=0.9
    epsilon=0.01
    continous=False

    #result = doTrial(numTrial,numEpd)
    #print getAvgTrialReward(result)
    lines = ['-','--']
    alphas = [0.05,0.1,0.6]
    lmbds = [0.1,0.2,0.3,0.4,0.5,0.6,0.7]
    lmbds = numpy.arange(0,1.1,0.1)
    alphasR = numpy.arange(0,1.1,0.1)

    '''
    p = []
    for v in alphasR:
        result = doTrial(numTrial,numEpd,lambd=0.5,alpha=v,continous=True)
        avgR = getAvgTrialReward(result)
        p.append(sum(avgR[len(avgR)-10:len(avgR)-1])/10)
    plt.plot(alphasR,p,lines[0],label=str(ACCUM_TRACE))
    #ACCUM_TRACE = False


    for x in range(2):
        p = []
        for v in lmbds:
            result = doTrial(numTrial,numEpd,lambd=v,alpha=0.1,continous=True)
            avgR = getAvgTrialReward(result)
            p.append(sum(avgR[len(avgR)-10:len(avgR)-1])/10)
        plt.plot(lmbds,p,lines[x],label=str(ACCUM_TRACE))
        ACCUM_TRACE = False


    for v in alphas:
        print "v:",v
        result = doTrial(numTrial,numEpd,lambd=0.9,alpha=v,continous=False)
        avgRewards = [0]
        avgRewards.extend(getAvgTrialReward(result))
        print avgRewards
        plt.plot(range(0,numEpd+1),avgRewards,label=str(v))
        #ACCUM_TRACE = False
    '''

    result1 = doTrial(numTrial,numEpd,lambd=0.5,alpha=0.1,continous=True)
    avgRewards = [0]
    avgRewards.extend(getAvgTrialReward(result1))
    plt.plot(range(0,numEpd+1),avgRewards,label="T")
    result2 = doTrial(numTrial,numEpd,lambd=0.5,alpha=0.1,continous=False)
    avgRewards = [0]
    avgRewards.extend(getAvgTrialReward(result2))
    plt.plot(range(0,numEpd+1),avgRewards,label="F")

    plt.legend(loc=4)
    #plt.savefig("plot.png")
    plt.show()
    exit_game()


if __name__ == '__main__':
    #tc = TileCoder()
    main()

