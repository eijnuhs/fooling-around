#ifndef _FRAME_BUFFER_WORLD_MOTION_H_
#define _FRAME_BUFFER_WORLD_MOTION_H_

#include "framebufferworld.h"
#include "array2d.h"
#include <string>
#include <vector>


// the number of cells storing the median intensity per dimension
#define CELL_DIM 8 

class QPainter;
class Run;

class FrameBufferWorldMotion : public FrameBufferWorld
{
 public:
  FrameBufferWorldMotion( double mmperpix, PlayingField field);
  FrameBufferWorldMotion();
  ~FrameBufferWorldMotion( );
  
  void initialize( double mmperpix, PlayingField field);
  void addBackgroundData(FrameBuffer * inFrame);
  void addBackgroundData(std::vector<Run> & unused, FrameBuffer * inFrame);
  void removeBackground(FrameBufferWorld * inFrame, FrameBufferWorld * outFrame, RawPixel removed = RawPixel(0,0,0));  
  void paintChild(QPainter & p);
  unsigned int getAvgIntensity(unsigned int row, unsigned int col);
  bool loadData(std::string filePrefix);
  void saveData(std::string filePrefix);

  void setVarianceThreshold(unsigned int vThreshold);
  void setIntensityThreshold(unsigned int iThreshold);

  unsigned int getVarianceThreshold();
  unsigned int getIntensityThreshold();
  void updateVarianceThresholds();
  void updateIntensityThresholds();

    
  Array2D<unsigned int> pixelVariance;
  Array2D<unsigned int> medianIntensity;

 private:
  bool initialized;

  bool loadFromFile(std::string filename);
  void saveToFile(std::string filename);
  void saveImage(std::string filename);
  bool loadImage(std::string filename);
  void initCMask();

  Array2D<unsigned int> varianceThresholds;
  Array2D<unsigned int> intensityThresholds;
  unsigned int globalVThreshold;
  unsigned int globalIThreshold;
  int cm[3][3]; // convolution mask
  int divisor;  // mask divisor
};

#endif
