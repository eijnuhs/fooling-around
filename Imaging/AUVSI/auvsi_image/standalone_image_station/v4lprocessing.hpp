#ifndef _VIDEO_PROCESSING_H_
#define _VIDEO_PROCESSING_H_

#include "umimage.hpp"
#include "umimagelocker.hpp"
#include "umimagegui.hpp"
#include <boost/bind.hpp>
#include <agmanager.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <configuration.h>
#include <umbotica/math/math.hpp>
#include <umimagetypedefs.hpp>
#include <umbotica/string_routines.hpp>
using namespace boost::posix_time;
#include "boost/filesystem/operations.hpp" // includes boost/filesystem/path.hpp
#include "boost/filesystem/fstream.hpp"    // ditto
namespace fs = boost::filesystem;
#include <helper_fns.hpp>

class VideoProcessing;

inline void addToGui(AGManager & gui, VideoProcessing & b, std::string name, int updateMsec);

class V4lProcessing
{
public:
  typedef vigra::RGBAValue<unsigned char> pixel_type;
  typedef UmImage<pixel_type> image_type;
  V4lProcessing(ImageVector & q, std::string _imageFolder):m_capture(false), m_filename("Not Capturing"),
							   imageFolder(_imageFolder), m_q(q)
  {
    Configuration cfg("V4lProcessing");
  }

  // the function call operator that allows us to pass this object in
  // place of a function pointer to the VideoStream.
  void operator()(image_type & image)
  {
    if(!image.isNull())
      {
	ptime timestamp = microsec_clock::local_time();
	UmImageLocker lock1(m_image);
	// shallow copy.
	m_image = image;
	if(m_capture)
	  {
	    // Get the timestamp and build the filename.  
	    // The replacement accounts for a deficiency in the vigra import/export
	    // code that leaves it unable to deal with extra dots in the filename 
	    // when loading files.
	    std::string imageName = imageFolder + "/" + 
	      umbotica::replace(to_iso_string(timestamp),'.','_') + ".jpg";
	    
	    // doubleVertically(m_image);
	    exportImage(m_image.upperLeft(), m_image.lowerRight(), m_image.rgbAccessor() ,vigra::ImageExportInfo(imageName.c_str()));
	    m_q.push_back(imageName);
	    m_filename = "Capturing: " + imageName;
	  }
      }
      
  }
  image_type & getImage(){ return m_image; }
  bool getCaptureOn(){return m_capture;}
  void setCaptureOn(bool on)
  {
    if(!on)
      {
	m_filename = "Not Capturing";
      }
    else
      {
	// 
	ptime timestamp = microsec_clock::local_time();
	std::string m_path = umbotica::replace(to_iso_string(timestamp),'.','_') + "/";
      }
    m_capture = on; 
  }
  std::string capturing(){return m_filename;}

private:
  image_type m_image;
  bool m_capture;
  std::string m_filename;
  std::string m_path;
  std::string imageFolder;

  // the queue used for inter-thread communication.
  ImageVector & m_q;
  friend void addToGui(AGManager & gui, V4lProcessing & b, std::string name, int updateMsec);
};


inline void addToGui(AGManager & gui, V4lProcessing & b, std::string name, int updateMsec)
{
  gui.pushGroup(name, false);
  gui.pushGroup("raw", false);
  gui.setWindowSize(640,480);
  addToGui(gui, b.getImage(), "Raw Image", updateMsec,false);
  gui.popGroup();
  // add the capture checkbox.
  gui.nextLine();
  gui.add("Capture", b.m_capture);
  gui.add("State: ", b.m_filename, true);
  gui.setWindowTimer(200);
  gui.popGroup();
}



#endif
