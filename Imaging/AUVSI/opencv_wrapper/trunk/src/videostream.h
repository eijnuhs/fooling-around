#ifndef _VIDEOSTREAM_H_
#define _VIDEOSTREAM_H_

#include "videodevice.h"
#include <string>
#include <sys/time.h>
#include <boost/function.hpp>
#include "umimage.hpp"
#include "v4l2device.h"
#include <iostream>
#include "videostreamconfig.h"
#include <boost/thread.hpp>
#include "imageformats.hpp"
#include "weakimage.hpp"

template<class PIXELTYPE>
class VideoStream
{
 public:
  VideoStream( std::string driver, 
	       std::string devname, 
	       std::string input, 
	       std::string standard, 
	       int fps, int width, 
	       int height, std::string format,
	       bool interlaced);
  
  VideoStream();
  virtual ~VideoStream();
  VideoDevice * getVideoDevice() const;
  
  void connect(boost::function<void(UmImage<PIXELTYPE>&)> slot);
  void startThread();
  void stopThread();
  double getFramesPerSecond();
 private: 
  void initialize( std::string driver, std::string name, std::string input, std::string standard, int fps, int width, int height, std::string format, bool interlaced);
  void initialize();
  void processingLoop();
  
  volatile bool m_done;
  VideoDevice * m_device;
  boost::function<void(UmImage<PIXELTYPE>&)> m_signal;
  boost::thread * m_thread;
  std::string driver;
  std::string name;
  std::string input;
  std::string standard;
  int fps;
  int width;
  int height;
  std::string format;
  bool interlaced;
};


template<class PIXELTYPE>
inline VideoStream<PIXELTYPE>::VideoStream( std::string driver, std::string name, 
				 std::string input, std::string standard, 
				 int fps, int width, int height, std::string format, bool interlaced ) : 
  m_done( false ), m_device( 0 ), m_thread(NULL), driver(driver),name(name),input(input),standard(standard),fps(fps),width(width),height(height), format(format)
{

}

template<class PIXELTYPE>
inline VideoStream<PIXELTYPE>::VideoStream() : m_done( false ), m_device( 0 ), m_thread(NULL)
{
  VideoStreamConfig vsc;
  driver = vsc.getString("driver", "V4L2");
  name = vsc.getString("name", "/dev/v4l/video0");
  input = vsc.getString("input", "Composite0");
  standard = vsc.getString("standard", "NTSC");
  fps = vsc.getUnsigned("fps", 30, 10);
  width = vsc.getUnsigned("width", 640, 10);
  height = vsc.getUnsigned("height", 240, 10);
  format = vsc.getString("format", "rgb24");
  interlaced = vsc.getInt("interlaced", 0);

}

template<class PIXELTYPE>
VideoStream<PIXELTYPE>::~VideoStream()
{
  stopThread();
}

template<class PIXELTYPE>
inline void VideoStream<PIXELTYPE>::initialize()
{
  initialize(driver,name,input,standard,fps,width,height, format, interlaced); 
}

template<class PIXELTYPE>
inline void VideoStream<PIXELTYPE>::initialize( std::string driver, std::string name, std::string input, std::string standard, int fps, int width, int height, std::string format, bool interlaced)
{
  // discern the driver from the string
  ImageFormats::ImageFormat fmt = ImageFormats::UNSUPPORTED;
  for(int i = 0; i < ImageFormats::COUNT; ++i)
    {
      if(format == ImageFormats::STRINGS[i])
	{
	  fmt = (ImageFormats::ImageFormat)i;
	  break;
	}
    }

  if(fmt != ImageFormats::UNSUPPORTED)
    {
      if ( driver == "V4L2" )
	{
	  uint32_t v4l2_fmt = ImageFormats::V4L2[fmt];
	  if(v4l2_fmt != umbotica::V4L2Traits<PIXELTYPE>::Format)
	    {
	      std::cout << "The specified driver format does not match the pixel type.  You can expect less than optimal performance\n";
	      std::cout << "Current Driver format: " << ImageFormats::getFormatString(fmt) << ".\n";
	      std::cout << "Current Pixel format: " << ImageFormats::getFormatString((ImageFormats::ImageFormat)umbotica::V4L2Traits<PIXELTYPE>::ImageFormat) << std::endl;
	    }
	  m_device = new V4L2Device( name, input, standard, fps, width, height, fmt, interlaced);
	  if ( m_device == 0 )
	    {
	      std::cerr << "ERROR: unable to create V4L2 video device";
	      perror(":");
	      std::exit( EXIT_FAILURE );
	    }
	  else if ( m_device->getError() )
	    {
	      std::cerr << "ERROR: V4L2 video device creation failed, error code = " << endl;
	      perror(":");
	      std::exit( EXIT_FAILURE );
	    }
	}
      else
	{
	  std::cerr << "ERROR: Unsupported device driver specified: " << driver << std::endl;	  
	}
    }
  else
    {
      std::cerr << "ERROR: Unsupported format specified: " << format << std::endl;
    }
}

template<class PIXELTYPE>
inline void VideoStream<PIXELTYPE>::startThread(void)
{
  
  if(m_thread != NULL)
    {
      std::cerr << "Videostream Error: A capture thread is already started.\n";
    }
  else if(!m_signal)
    {
      std::cerr << "Videostream Error: Will not start thread without a callback\n";
    }
  else
    {
      // start a thread.
      // initialize the device
      initialize();
      if(m_device->getError())
	{
	  std::cerr << "Videostream Error: unable to open video driver\n";
	}
      else
	{
	  m_done = false;
	  std::cout << "Starting the capture thread\n";
	  m_thread = new boost::thread(boost::bind(&VideoStream<PIXELTYPE>::processingLoop,this));
	}
    }
}

template<class PIXELTYPE>
inline void VideoStream<PIXELTYPE>::stopThread()
{
  if(m_thread != NULL)
    {
      std::cerr << "Stopping Thread\n";
      m_done = true;
      m_thread->join();
      m_done = false;
      delete m_thread;
      m_thread = 0;
      std::cerr << "m_done: " << m_done << "\n";
    }
}

template<class PIXELTYPE>
inline VideoDevice * VideoStream<PIXELTYPE>::getVideoDevice() const
{
  return m_device;
}

template<class PIXELTYPE>
inline void VideoStream<PIXELTYPE>::connect(boost::function<void(UmImage<PIXELTYPE>&)> slot)
{
  m_signal = slot;
}

template<class PIXELTYPE>
inline void VideoStream<PIXELTYPE>::processingLoop()
{
  std::cerr << "Called start THread\n";

  m_done = false;
  UmImage<PIXELTYPE> frame;
  WeakImage temp;
  if(m_signal)
    {
      while(!m_done)
	{
	  try
	    {
	      m_device->startCapture( );
	      while( ! m_done )
		{
		  temp = m_device->nextFrame();
		  temp.packUmImage(frame);
		  //frame = m_device->nextFrame();
		  m_signal(frame);
		  m_device->releaseCurrentBuffer();
		}
	    }
	  catch(std::exception const & e)
	    {
	      m_device->stopCapture( );
	      std::cout << "Videostream caught exception: " + std::string(e.what()) << "\n";
	      initialize();
	      m_device->startCapture( );      
	    }
	}
    }
  std::cerr << "Stopping capture\n";
  m_device->stopCapture( );
}

#endif
