#ifndef __VISITED_MAP_H_
#define __VISITED_MAP_H_
#include "videocalibration.h"


class FrameBuffer;

class VisitedMap
{
 public:
  VisitedMap(unsigned int init_height, unsigned int init_width);
  VisitedMap(FrameBuffer * frame);
  VisitedMap(FrameBuffer * frame, list< sPoint > & scanlines);
  VisitedMap();
  ~VisitedMap();
  VisitedMap(const VisitedMap & rhs);
  VisitedMap & operator= (const VisitedMap & rhs);
  void setVisited(unsigned int x, unsigned int y, bool visited = true);
  bool isVisited(unsigned int x, unsigned int y) const;
  void initialize(FrameBuffer * frame);
  void initialize(FrameBuffer * frame, list< sPoint > & scanlines);

 private:
  bool * map;
  unsigned int height;
  unsigned int width;

  bool & getItem(unsigned int x, unsigned int y);
  void initMap(bool initialValue = false);
  void copyMap(const VisitedMap & rhs);
};
#endif

