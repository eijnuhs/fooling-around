#include "debugviewer.h"
#include "videointerpreter2d.h"
#include <qstring.h>
#include "paintbox.h"
#include "histogramviewer.h"
#include "featurecircle.h"
#include "framebuffer.h"
#include "mainwindow.h"
#include "geometry.h"
#include <qtextedit.h> 


DebugViewer::DebugViewer( QWidget * parent, char const * name ) :
  FrameBufferViewer(parent,name),
  point(0.0, 0.0, 0.0, 0.0, 0.0, 0.0),
  bot(0),
  vi2d(0),
  initialized(false),
  histview(0),
  state(0)
{
  // 0
}

void DebugViewer::initialize(DebugViewerConfig * dbc)
{
  if(dbc)
    {
      double botRadius = dbc->getDouble("botRadius", 90.0);
      double botHeight = dbc->getDouble("botHeight", 10.0);
      bot = new Robot("debugBot", botHeight, botRadius, 10, 0);
    }
}

DebugViewer::~DebugViewer()
{
  delete bot;
}

void DebugViewer::mousePressEvent(QMouseEvent * e)
{
  int xm, ym;
  if(vi2d && initialized && e->button() == Qt::LeftButton)
    {
      // set the control point to the new location
      widgetToScreenCoordinates(e->x(), e->y(), &xm, &ym);
      point.xm = (double)xm;
      point.ym = (double)ym;
      updateControlPoint();
    }
}

void DebugViewer::setVideoInterpreter(VideoInterpreter2D * v)
{
  vi2d = v;
}

void DebugViewer::updateControlPoint()
{
  if(vi2d)
    {
      vi2d->screenToWorldCoordinates(point.xm,point.ym,0.0,&point.xw,&point.yw);
    }
}


void DebugViewer::newFrame(FrameBuffer * frame)
{
  if(!initialized)
    {
      point.xm = (double)(frame->height / 2);
      point.ym = (double)(frame->width / 2);
      updateControlPoint();
      initialized = true;
    }
  if(isVisible() && vi2d && bot)
    {
      bot->initForFrame();
      // we try to find a robot in the location of the control point.
      if(bot->findRobot(point.xw, point.yw, frame, &subtype, &theta) && vi2d)
	{
	  vi2d->worldToScreenCoordinates(point.xw,point.yw,0.0,&point.xm,&point.ym);
	  bot->setPosition(point.xw, point.yw, theta, frame->t);
	}
      if(histview)
	{
	  histview->newHistogram(bot->hist);
	  histview->thresholds[0] = bot->black;
	  histview->thresholds[1] = bot->white;
	}
      
    }  
  FrameBufferViewer::newFrame(frame);
}
void DebugViewer::paintChild(QPainter & p)
{
  if(state == 0)
    {
      int wgx,wgy, sx2, sy2, wgx2, wgy2, sx, sy;
      QString label;
      //  RawPixel pixel;
      label = QString("DebugBot%1 %2%").arg(subtype).arg(bot->getPercentFound() * 100);
      screenToWidgetCoordinates( point.xm, point.ym, &wgx, &wgy );
      PaintBox::drawPoint( p, wgx, wgy,  7, label, 20,Qt::blue,Qt::cyan);
      
      // paint theta. 
      vi2d->worldToScreenCoordinates(point.xw + (bot->getRadius() * cos(theta)), point.yw + (bot->getRadius() * sin(theta)), 0.0, &sx2,&sy2);
      screenToWidgetCoordinates( sx2, sy2, &wgx2, &wgy2 );
      p.setPen(QPen(Qt::red, 2));
      p.drawLine(wgx,wgy,wgx2,wgy2);
      
      // paint the bounding box.
      vi2d->worldToScreenCoordinates(point.xw - bot->getRadius(), point.yw - bot->getRadius(), 0.0, &sx,&sy);
      vi2d->worldToScreenCoordinates(point.xw + bot->getRadius(), point.yw + bot->getRadius(), 0.0, &sx2,&sy2);
      screenToWidgetCoordinates( sx, sy, &wgx, &wgy );
      screenToWidgetCoordinates( sx2, sy2, &wgx2, &wgy2);
      p.drawRect(wgx,wgy,wgx2-wgx, wgy2-wgy);
    }
  
  // update the text edit with the intensity strips.
  QString s(bot->getIntensityStripOutput());
  mainWindow->txeDebugText->setText( s + QString("\nblack: %1  white: %2").arg(bot->black).arg(bot->white));
  
}

void DebugViewer::setHistogramViewer(HistogramViewer * hv)
{
  histview = hv;
  if(histview && bot)
    {
      histview->thresholds.push_back(bot->black);
      histview->thresholds.push_back(bot->white);
    }
}

void DebugViewer::updateQImageFromFrameBuffer( FrameBuffer * frame )
{

if(frame)
  {
  FrameBufferIterator fbit(frame);
  int sy,sx;
  RawPixel pixel;
  // now paint the interpolated circles
  for( unsigned int i = 0; i < C_NUM; ++i)
    {
      for(unsigned int j = 0; j < bot->features[i]->getSize(); ++j)
	{
	  // get the screen location
	  FeatureCircle & fc(*bot->features[i]);
	  vi2d->worldToScreenCoordinates(point.xw + fc[j].column, point.yw + fc[j].row, 0.0, &sx,&sy, false);
       	//   if(bot->intensityArrays[i][j] == Robot::R_BLACK)
// 	    pixel = RawPixel(0,0,0);
// 	  else if(bot->intensityArrays[i][j] == Robot::R_WHITE)
// 	    pixel = RawPixel(255,255,255);
// 	  else
// 	    pixel = RawPixel(0,255,0);
	  if(bot->medianIntensities[j] == Robot::R_BLACK)
	    pixel = RawPixel(0,0,0);
	  else if(bot->medianIntensities[j] == Robot::R_WHITE)
	    pixel = RawPixel(255,255,255);
	  else
	    pixel = RawPixel(0,0,255);
	  
	  if(!frame->interlaced)
	    sy = sy / 2;
	  Geometry::clamp(0,sy,frame->height - 1);
	  Geometry::clamp(0,sx,frame->width - 1);
	  fbit.goPosition(sy,sx);
	  fbit.setPixel(pixel);
	  //frame->setPixelAt(sy,sx,pixel);
	} 
    }
  
  if(state == 0) 
    FrameBufferViewer::updateQImageFromFrameBuffer(frame);
  else
    {
      //      std::cout << "zoom\n";
      // paint a zoomed in version of the current robot
      int sx,sy,sx2,sy2,width,height;
      FrameBufferRGB32 f;
      vi2d->worldToScreenCoordinates(point.xw - bot->getRadius() * 2.0, point.yw - bot->getRadius() * 2.0, 0.0, &sx,&sy);
      vi2d->worldToScreenCoordinates(point.xw + bot->getRadius() * 2.0, point.yw + bot->getRadius() * 2.0, 0.0, &sx2,&sy2);
      if(!frame->interlaced)
	{
	  sy /= 2;
	  sy2 /= 2;
	}
      sx = Geometry::clamp(0,sx,frame->width - 1);
      sy = Geometry::clamp(0,sy,frame->height - 1);
      sx2 = Geometry::clamp(0,sx2,frame->width - 1);
      sy2 = Geometry::clamp(0,sy2,frame->height - 1);
      if(sx > sx2)
	{
	  width = sx;
	  sx = sx2;
	  sx2 = width;
	}
      if(sy > sy2)
	{
	  width = sy;
	  sy = sy2;
	  sy2 = width;
	}
      width = sx2 - sx;
      height = sy2 - sy;
      //      std::cout << "(" << sy << "," << sx << ")\n";
      //      std::cout << "(" << sy2 << "," << sx2 << ")\n";
      if(width > 0 && height > 0)
	{
	  //	  std::cout << "all good\n";
	  f.initialize(FrameBuffer::RGB32, width,height);
	  FrameBufferIterator fbit2(&f);
	  for(int i = 0; i < height; ++i)
	    {
	      fbit2.goPosition(i,0);
	      fbit.goPosition(sy + i,sx);
	      for(int j = 0; j < width; ++j)
		{
		  fbit.getPixel(&pixel);
		  fbit2.setPixel(pixel);
		  fbit2.goRight();
		  fbit.goRight();
		}
	    }
	  FrameBufferViewer::updateQImageFromFrameBuffer(&f);
	}
      else
	{
	  FrameBufferViewer::updateQImageFromFrameBuffer(frame);
	}
    }
  }
}

void DebugViewer::mouseDoubleClickEvent ( QMouseEvent * e )
{
  std::cout << "DOuble Click\n";
  if(e->button() == Qt::RightButton)
    {
      state ^= 1;
      std::cout << "Changing State: " << state << "r\n";
    }
  
}
