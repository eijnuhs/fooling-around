// $Id: calibration.h,v 1.1.1.1.2.1 2004/10/14 15:12:57 cvs Exp $
// A Baseclass for different calibration methods.

#ifndef _CALIBRATION_H_
#define _CALIBRATION_H_

#include <math.h>
#include "controlpoints.h"
#include "camera.h"
#include <string>
#include <list>
#include "square.h"

class Calibration
{
public:
  Calibration ();
  Calibration ( std::string calibrationPointsFilename );
  virtual ~ Calibration ();
  virtual void loadCalibrationData(std::string filename);
  virtual void loadCalibrationPoints (std::string filename);
  virtual void generateCalibrationPoints (void);
  virtual void recalibrate(void) = 0;

  virtual void worldCoordToImageCoord (double xw, double yw, double zw, double *Xf, double *Yf) = 0;
  virtual void imageCoordToWorldCoord (double Xfd, double Yfd, double zw, double *xw, double *yw) = 0;
  virtual void worldCoordToCameraCoord (double xw, double yw, double zw, double *xc, double *yc, double *zc) = 0;
  virtual void cameraCoordToWorldCoord (double xc, double yc, double zc, double *xw, double *yw, double *zw) = 0;
  virtual void imageCoordToCameraCoord (double Xfd, double Yfd, double zc, double *xc, double *yc) = 0;
  struct ControlPoint *Calibration::controlPoint (int x, int y);

  void newCalibrationPoints (ControlPoints points);
  void newCalibrationPoints (std::list< Square > squares);
  void saveCalibration (std::string filename);

  void calcErrorStats ( void );
  std::string getPointsFile( void );

  Camera camera;

protected:
  ControlPoints points;
  std::string pointsFile;
};
#endif
