/**
 * <p>Title: UDPBroadcaster</p>
 * <p>Description: UDPBroadcaster class</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: Xiao-Wen Terry Liu</p>
 * @author Xiao-Wen Terry Liu
 * @version 1.02
 */


#ifndef UDPBROADCASTER_H
#define UDPBROADCASTER_H

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define DEFAULT_BROADCAST_PORT  6363
#define DEFAULT_IP_ADDR "255.255.255.255"

class UDPBroadcaster {
public:
  //constructor that accepts a new port, new IP address
  UDPBroadcaster(unsigned int servPort = DEFAULT_BROADCAST_PORT,
		 char * servIP = DEFAULT_IP_ADDR );
  UDPBroadcaster::~UDPBroadcaster( );
  
  void setServPort(unsigned int servPort);     //Sets the server port
  unsigned int getServPort();                 //Returns the server port
  void printServPort();                       //Prints the server port
  void setServIP(char* new_ip_addr);          //Sets the IP to transmit to
  char* getServIP();                          //Return the IP address to transmit to
  
  int broadcast( const char* incoming_msg, bool hide_output = false );  //Broadcast a message
 private:
  unsigned int servPort;                      //The server port to connect to
  char* servIP;                               //The IP address to send to
  int sd;
  struct sockaddr_in remoteServAddr;  //our address, remote server address
}; //end class UDPBroadcaster

#endif
