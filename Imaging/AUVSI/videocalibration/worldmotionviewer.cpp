#include "worldmotionviewer.h"
#include "framebufferworldmotion.h"
#include <qpainter.h>
#include <qwmatrix.h>


WorldMotionViewer::WorldMotionViewer( QWidget * parent, char const * name ) :
  FrameBufferViewer(parent,name)
{
  // mode = WMV_VARIANCE;
  //   mode = WMV_MEAN;
   mode = WMV_NONE;
}

WorldMotionViewer::~WorldMotionViewer(){}

void WorldMotionViewer::paintChild(QPainter & p)
{
  
   FrameBufferWorldMotion * f;
   if((f = dynamic_cast<FrameBufferWorldMotion *>(frame)) != NULL)
     {
       int cellwidth = width() / CELL_DIM;
       int cellheight = height() / CELL_DIM;
       int offset = (cellheight + cellwidth) / 4;
       p.setPen(Qt::red);
      
     
       if(mode == WMV_MEAN)
 	{
 	  for(int i = 0; i < CELL_DIM; ++i)
 	    for(int j = 0; j < CELL_DIM; ++j)
 	      {
		
 		p.drawText((cellwidth * i) + offset, 
 			   (cellheight * j)  + offset, 
 			   QString::number(f->medianIntensity.get(j,i)));
 	      }
 	}
       //       else if (mode == WMV_VARIANCE)
// 	  {
// 	  for(int i = 0; i < BRIGHTNESS_DIM; ++i)
// 	    for(int j = 0; j < BRIGHTNESS_DIM; ++j)
// 	      {
		
// 		p.drawText((cellwidth * i) + offset, 
// 			   (cellheight * j)  + offset, 
// 			   QString::number(f->variance[j][i]));
// 	      }
// 	}
      
     }
  

}


void WorldMotionViewer::newFrame(FrameBuffer * f)
{
  //  std::cout << "NewFrame\n";
  FrameBufferViewer::newFrame(f);
}
