#include "trackerconfig.h"
#include "trackable.h"
#include "colourmap.h"
#include "robot.h"
#include "spot.h"
#include "ball.h"
#include "tobject.h"

TrackerConfig::TrackerConfig(ColourMap * cm) :
  Configuration("Tracker"),
  cmap(cm),
  ball(0),
  loadedBotList(false)
{
  //nothing

}

TrackerConfig::~TrackerConfig()
{
  // nothing
}

Ball * TrackerConfig::getBall()
{
  if(!loadedBotList)
    getBotList();

  return ball;
}


std::vector<Trackable *> TrackerConfig::getBotList()
{
  Node * temp;
  std::string type;
  double height, radius;
  std::string name;
  int subtype;
  Trackable * item;
  std::vector<Trackable *> retlist;
  std::vector<Node *> nodeList(getNodeList("Trackable"));
  std::string colourName;
  //  temp = getNode("mm_per_pixel", "Segmentation");
  //double mmPerPix = getDouble(temp, 15);
  double mmPerPix = getDouble("mm_per_pixel", 15);

  std::cout << "Tracker mm_per_pix: " << mmPerPix << "\n";
  
  std::cout << "Getting trackable objects\n";
  for(unsigned int i = 0; i < nodeList.size(); ++i)
    {
      temp = getNode("type",nodeList[i]);
      type = getString(temp, "Unknown");
      temp = getNode("name",nodeList[i]);
      name = getString(temp,"Unnamed");
      temp = getNode("height",nodeList[i]);
      height = getDouble(temp, 0.0);
      temp = getNode("subtype",nodeList[i]);
      subtype = getInt(temp,-1);
      temp = getNode("radius",nodeList[i]);
      radius = getDouble(temp, 90.0);

      std::cout.flush();
      std::cout << "Trackable object: " << name << "  r:" << radius << " h:" << height << " st:" << subtype << "\n";
      
      if(type == "Robot")
	{
	  item = new Robot(name,height,radius,mmPerPix, subtype);
	  std::cout << "Creating a new robot: " << name << "," << subtype << "\n";
	}
      else if(type == "Ball")
	{
	  if(!ball)
	    {
	      temp = config->findNode("colour", nodeList[i]);
	      colourName = getString(temp, "purple");
	      if(cmap)
		{
		  ball = new Ball(name, height, radius, mmPerPix, cmap->getColour(colourName));
		  item = ball;
		}
	    }
	  else
	    {
	      std::cerr << "TrackerConfig found two balls.  Only one ball is allowed\n";
	    }
	}
      else if(type == "Spot")
	{
	  temp = config->findNode("colour", nodeList[i]);
	  colourName = getString(temp, "purple");
	  if(cmap)
	    {
	      item = new Spot(name, height, radius, mmPerPix, cmap->getColour(colourName));
	      std::cout << "Creating new spot: " << name << " with colour " << colourName << "\n";      
	    }
	  else
	    {
	      std::cerr << "Cannot create spot without colour map.\n";
	    }
	}
      else if(type == "TObject")
	{
	  item = new TObject(name, height, radius, mmPerPix ); 
	  std::cout << "Creating new tobject: " << name << "\n";      
	}
      else
	{
	  std::cerr << "Unknown type: " << type << "\n";
	}
      
      if(item)
	retlist.push_back(item);
      
      item = 0;      
    }
  loadedBotList = true;
  return retlist;

}

