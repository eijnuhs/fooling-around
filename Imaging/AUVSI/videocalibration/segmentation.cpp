// $Id: segmentation.cpp,v 1.1.1.1.2.31 2005/02/06 00:44:02 cvs Exp $
//

#include "segmentation.h"
#include "videointerpreter2d.h"
#include "framebuffer.h"
#include <inttypes.h>
#include "pixel.h"
#undef min
#undef max
#include <iostream>
#include "assert.h"
#include "controlpoints.h"
#include "region.h"
#include <qmutex.h>
#include "mask.h"
#include "framebufferrgb565.h"
#include "framebufferworld.h"
#include "imageprocessing.h"
#include <linux/videodev.h>
#include "segmentationconfig.h"
#include "framespersecond.h"
#include "robot.h"

#define MM_PER_PIXEL 10
#define MM_PER_PIXEL_WV 10

#define PIX_LB 10
#define PIX_UB 60

#define NO_FILE_PREFIX "__noNe__"
#define DEFAULT_FILE_PREFIX "backimg_"

// static double dminim( double a, double b );
// static double dmaxim( double a, double b );


Segmentation::Segmentation( VideoInterpreter2D * vi2d, SegmentationConfig * scfg) : 
  vi2d( vi2d ), 
  blurring( false ), 
  filterRegions( false ),
  mode(0)
{
  std::string temp;

  mmppix = scfg->getUnsigned("mm_per_pixel", MM_PER_PIXEL);
  mmppixwv = scfg->getUnsigned("mm_per_pixel_wv", MM_PER_PIXEL_WV);
  setIntThreshold(scfg->getUnsigned("intThreshold",0));
  
  setThreshold(scfg->getUnsigned("threshold",500));
  backFilePrefix = scfg->getString("backgroundFiles",NO_FILE_PREFIX);
  motionFrameTop.initialize( mmppix, vi2d->field );
  motionFrameBottom.initialize( mmppix, vi2d->field );

  if(backFilePrefix == NO_FILE_PREFIX)
    {
      // initialize the motion frames blank
      backFilePrefix = DEFAULT_FILE_PREFIX;
    }
  else
    {
      // load the motion frames from disk
      temp = backFilePrefix + "top";
      motionFrameTop.loadData(temp);
      temp = backFilePrefix + "bot";
      motionFrameBottom.loadData(temp);
    }

  ipFrame.initialize(mmppix, vi2d->field );

  segmentedFrame.initialize( mmppix, vi2d->field );

  wvFrame.initialize( mmppixwv , vi2d->field );

#ifdef DEBUG
  std::cout << "Segmentation::Segmentation segmentedFrame width " << segmentedFrame.width << ", height " << segmentedFrame.height << std::endl;
#endif
}


int Segmentation::getMode()
{
  return mode;
}
int Segmentation::getThreshold()
{
  return motionFrameTop.getVarianceThreshold();
}
int Segmentation::getIntThreshold()
{
  return motionFrameTop.getIntensityThreshold();
}

void Segmentation::interpolateFrame( FrameBuffer * inFrame )
{
  // initialize and interpolate the frame.
  //  memset( ipFrame.buffer, 0x25, ipFrame.frameSize );
  vi2d->interpolateFrame( inFrame, &ipFrame, blurring);
  memcpy(&timestamp, &(inFrame->t),sizeof(struct timeval));
}

void
Segmentation::segmentFrame( FrameBuffer * inFrame )
{
  // unsigned int noPix;

  if(FramesPerSecond::getTimeDiff(timestamp, inFrame->t) != 0)
    {
      interpolateFrame(inFrame);
      // std::cout << "interpolating frame from segmentFrame()\n";
    }

  QMutexLocker qml(&mutex);
  
  // initialize the world frame
  //  memset( wvFrame.buffer, 0x00, wvFrame.frameSize );
  
  // send the raw frame to the display.
  emit newInterpolatedFrame(&ipFrame);
  
  //  std::cout << "Field: " << inFrame->fieldNo << "\n";
  
  // initialize the segmented frame
  //  memset( segmentedFrame.buffer, 0x00, segmentedFrame.frameSize );  
  if(mode == SM_SEGMENTATION)
    {
      // remove the background and segment the frame
      if(inFrame->fieldNo == V4L2_FIELD_BOTTOM)
	{
	  motionFrameBottom.removeBackground(&ipFrame, &segmentedFrame);
	  emit newBackgroundFrame(&motionFrameBottom);
	  //	  std::cout << "Bottom Field Remove\n";
	}
      else
	{
	  motionFrameTop.removeBackground(&ipFrame, &segmentedFrame);
	  emit newBackgroundFrame(&motionFrameTop);
	  //	  std::cout << "Top Field Remove\n";
	}

      regionList.createRegions(&segmentedFrame);
      emit newSegmentedFrame( (FrameBuffer *) & segmentedFrame);

      // transfer all regions of the proper size to the world frame
      //      for(uint i = 0; i < regionList.size(); ++i)
      //	{
      //	  noPix = regionList[i].numPixels;
      //	  if(noPix > PIX_LB && noPix < PIX_UB)
      //	    {
      //	      addRegionToWorldFrame(regionList[i],inFrame);
	      // std::cout << "adding region: " << regionList[i] << "\n";
      //	    }
      //	}
    }
  else if (mode == SM_MOTION)
    {
      interpolateFrame(inFrame);
      regionList.clear();
      if(inFrame->fieldNo == V4L2_FIELD_BOTTOM)
	{
	  motionFrameBottom.addBackgroundData(&ipFrame);
	  emit newBackgroundFrame(&motionFrameBottom);
	  //	  std::cout << "Bottom Field Add data\n";
	}
      else
	{
	  motionFrameTop.addBackgroundData(&ipFrame);

	  emit newBackgroundFrame(&motionFrameTop);
	  //	  std::cout << "Top Field Add data\n";
	}

    }

  // createWorldFrame( inFrame, &segmentedFrame, &wvFrame );
  // emit newFrame( (FrameBuffer * ) & wvFrame );
}

#if 0
void
Segmentation::segmentFrame( FrameBuffer * inFrame )
{
#ifdef DEBUG
  std::cout << "Segmentation::segmentFrame called, ";
  std::cout << "inFrame->buffer: " << (void*)inFrame->buffer << "\n";   
  std::cout << "blurring: " << blurring <<"\n";
#endif
  mutex.lock();

  memset( ipFrame.buffer, 0x25, ipFrame.frameSize );
  vi2d->interpolateFrame( inFrame, &ipFrame);
  
  memset( segmentedFrame.buffer, 0x00, segmentedFrame.frameSize );
  
  stats.numScanLines = 0;
  stats.numFilteredRegions = 0;
  stats.numSmallRegions = 0;
  segmentWorldFrame( &ipFrame, &segmentedFrame );
  createWorldFrame( inFrame, &segmentedFrame, &wvFrame );
  mutex.unlock();
  emit newFrame( (FrameBuffer * ) & wvFrame );
}
#endif
 


void
Segmentation::setThreshold( unsigned int threshold )
{
  //  this->threshold = threshold;
  motionFrameTop.setVarianceThreshold(threshold);
  motionFrameBottom.setVarianceThreshold(threshold);
}
void
Segmentation::setIntThreshold( unsigned int threshold )
{
  //  this->intThreshold = threshold;
  motionFrameTop.setIntensityThreshold(threshold);
  motionFrameBottom.setIntensityThreshold(threshold);
}


///////// DEPRECATED /////////////
void Segmentation::addRegionToWorldFrame(RunRegion & region, FrameBuffer * inFrame)
{
  double wx,wy;
  unsigned int wfx,wfy; // the world frame coordinates of the region's centre.
  int pixelRadius = (int)rint(BOT_RADIUS / mmppixwv);
  //segmentedFrame, wvFrame.
  unsigned int startRow,endRow,startCol,endCol;
  
  // get the centre of the region
  segmentedFrame.bufferToWorldCoordinates(region.getCOMXD(),region.getCOMYD(),&wx,&wy);
  wvFrame.worldToBufferCoordinates(wx,wy,&wfx,&wfy);

  // set up the box to draw.
  wfx = Geometry::clamp(pixelRadius,wfx,wvFrame.width - pixelRadius - 1);
  wfy = Geometry::clamp(pixelRadius,wfy,wvFrame.height - 1 - pixelRadius);
  startRow = wfy - pixelRadius;
  endRow   = wfy + pixelRadius;
  startCol = wfx - pixelRadius;
  endCol   = wfx + pixelRadius;

  // Interpolate the box
  vi2d->interpolateFrame (inFrame , &wvFrame, startRow,startCol, endRow, endCol,false);

  // draw the centre.
  
  wvFrame.setPixelAt(wfy,wfx,RawPixel(0,0,255));
  


#ifdef DEBUG
  std::cout << "region com: (" << region.getCOMYI() << "," << region.getCOMXI() << ")\n";
  std::cout << "region BBC: (" << region.getBBCentreYI() << "," << region.getBBCentreXI() << ")\n"; 
  std::cout << "world coords: (" << wy << "," << wx << ")\n";
  std::cout << "world coords1: (" << wy1 << "," << wx1 << ")\n";
  std::cout << "world coords2: (" << wy2 << "," << wx2 << ")\n";
  std::cout << "buffer coords: (" << wfy << "," << wfx << ")\n";
#endif
  
}



void Segmentation::setMode(int mode)
{
  this->mode = mode;
  
}



void Segmentation::saveBackground()
{
  std::string temp;
  temp = backFilePrefix + "top";
  motionFrameTop.saveData(temp);
  temp = backFilePrefix + "bot";
  motionFrameBottom.saveData(temp);
}

void Segmentation::updateWVFrame()
{
  emit newFrame( (FrameBuffer * ) & wvFrame );
}

void Segmentation::updateMotionFrame(std::vector<Run> & unused, int fieldNo)
{
  if(fieldNo == V4L2_FIELD_BOTTOM)
    {
      motionFrameBottom.addBackgroundData(unused, &ipFrame);
      //      std::cout << "Bottom Field incremental add motion data\n";
    }
  else
    {
      motionFrameTop.addBackgroundData(unused, &ipFrame);
      //      std::cout << "Top Field incremental add motion data\n";
    }
  
}






















//////////////////////////////////////////////////////////////
///////////////// DEPRECATED /////////////////////////////////
//////////////////////////////////////////////////////////////
#if 0

bool Segmentation::setBlurring( bool flag )
{
  blurring = flag;
  return blurring;
}

bool 
Segmentation::setFilterRegions( bool flag )
{
  filterRegions = flag;
  return filterRegions;
}


void Segmentation::setMinLength(unsigned int minL)
{
  minLength = minL;
}

void Segmentation::setMaxLength(unsigned int maxL)
{
  maxLength = maxL;
}

void Segmentation::setMMPerPixel(unsigned int mm)
{
  if(mm == 0)
    mmPerPixel = 1;
  else
    mmPerPixel = mm;
}


static double
dminim( double a, double b )
{
  return a < b ? a : b;
}

static double
dmaxim( double a, double b )
{
  return a > b ? a : b;
}



void
Segmentation::segmentWithMotion( FrameBuffer * frame, FrameBuffer * outFrame)
{
  unsigned int avgIntensity = 0;
  unsigned int prevIntensity = 0;
  unsigned int intensity;
  //  Pixel avg( 0, 0, 0 );
  //  Pixel pPixel, pixel;
  //  Pixel nPixel;

  FrameBufferIterator inFrameIter( frame );
  FrameBufferIterator outFrameIter( outFrame );
  
  unsigned int prevIndex;
  for( unsigned int i = 0; i < frame->height; i++ )
    {
      inFrameIter.goRow( i );
      avgIntensity = inFrameIter.getIntensity();
      prevIndex = 0;

      for( unsigned int j = 1; j < frame->width; j++, inFrameIter.goRight( ) )
	{
	  if ( ( blurring ) && ( j > 2 ) && ( j < frame->width - 2 ) && ( i > 2 ) && ( i < frame->height - 2 ) )
	    {
	      intensity = inFrameIter.getBlurred3x3Intensity();
	      //void * pptr = inFrameIter.getPixelPtr( );
	      //frame->getBlurred3x3Pixel( pptr, & pixel );
	    }
	  else
	    {
	      intensity = inFrameIter.getIntensity();
	      //inFrameIter.getPixel( & pixel );
	    }
	  //	  min.setMinimum( pixel );
	  //	  max.setMaximum( pixel );
	  bool endOfEdge = false;




	  //double diff = pixel.diffMax( pPixel );
	  int diff = prevIntensity - intensity;
	  unsigned int udiff = diff * diff;
	  
	  if ( udiff > threshold )
	    {
	      endOfEdge = true;
	    }

	  if ( ( endOfEdge ) || ( j >= frame->width - 1 ) )
	    {
	      unsigned int rend;
	      if ( j >= frame->width - 1 )
		{
		  rend = frame->width;
		}
	      else
		{
		  rend = j;
		}

#ifdef XX_DEBUG
	      std::cout << "Segmentation::segmentWithMotion found scanline in row " << i << " from " << prevIndex << " to " << j << " diff " << diff << " frame->width - 1 " << frame->width - 1 << std::endl;
#endif
	      unsigned int length = rend - prevIndex;
	      if ( length >= 1 && length <= 3 )
		{
		  stats.numScanLines++;
		  FrameBufferIterator segmentIter( outFrame, i, prevIndex );

		  for( unsigned int k = prevIndex; k < rend; k++, segmentIter.goRight( ) )
		    {
		      if ( k == ( ( rend - 1 + prevIndex ) / 2 ) )
			{
			  segmentIter.setPixel( RawPixel( 255, length, 0 ) );
			}
		      else
			{
			  segmentIter.setPixel( RawPixel( 0, 0, avgIntensity/length ) );
			}
		    }
		}
	      // avg = pixel;
	      avgIntensity = intensity;
	      prevIndex = j;
	    }
	  else
	    {
	      //avg += pixel;
	      avgIntensity += intensity;
	    }
	  prevIntensity = intensity;
	  //	  pPixel = pixel;    
	}
    }  
    
}


void
Segmentation::segmentWorldFrame( FrameBuffer * frame, FrameBuffer * outFrame )
{
  
  Pixel avg( 0, 0, 0 );
  Pixel pPixel, pixel;
  Pixel nPixel;

  FrameBufferIterator inFrameIter( frame );
  FrameBufferIterator outFrameIter( outFrame );
  
  unsigned int prevIndex;
  for( unsigned int i = 0; i < frame->height; i++ )
    {
      inFrameIter.goRow( i );
      inFrameIter.getPixel( & avg );
      prevIndex = 0;

      for( unsigned int j = 1; j < frame->width; j++, inFrameIter.goRight( ) )
	{
	  if ( ( blurring ) && ( j > 2 ) && ( j < frame->width - 2 ) && ( i > 2 ) && ( i < frame->height - 2 ) )
	    {
	      void * pptr = inFrameIter.getPixelPtr( );
	      frame->getBlurred3x3Pixel( pptr, & pixel );
	    }
	  else
	    {
	      inFrameIter.getPixel( & pixel );
	    }
	  //	  min.setMinimum( pixel );
	  //	  max.setMaximum( pixel );
	  bool isEdge = false;

	  double diff = pixel.diffMax( pPixel );
	  if ( diff > threshold )
	    {
	      isEdge = true;
	    }

	  if ( ( isEdge ) || ( j >= frame->width - 1 ) )
	    {
	      unsigned int rend;
	      if ( j >= frame->width - 1 )
		{
		  rend = frame->width;
		}
	      else
		{
		  rend = j;
		}

#ifdef XX_DEBUG
	      std::cout << "Segmentation::segmentFrame found scanline in row " << i << " from " << prevIndex << " to " << j << " diff " << diff << " frame->width - 1 " << frame->width - 1 << std::endl;
#endif
	      unsigned int length = rend - prevIndex;
	      if ( length >= 1 && length <= 3 )
		{
		  stats.numScanLines++;
		  nPixel = avg / static_cast<double>( rend - prevIndex );
		  FrameBufferIterator segmentIter( outFrame, i, prevIndex );

		  for( unsigned int k = prevIndex; k < rend; k++, segmentIter.goRight( ) )
		    {
		      if ( k == ( ( rend - 1 + prevIndex ) / 2 ) )
			{
			  segmentIter.setPixel( RawPixel( 255, length, 0 ) );
			}
		      else
			{
			  segmentIter.setPixel( RawPixel( 0, 0, nPixel.blue ) );
			}
		    }
		}
	      avg = pixel;
	      prevIndex = j;
	    }
	  else
	    {
	      avg += pixel;
	    }
	  pPixel = pixel;    
	}
    }
}

void
Segmentation::createWorldFrame( FrameBuffer const * frame, FrameBufferWorld const * inFrame, FrameBufferWorld * outFrame )
{
  FrameBufferIterator rFIter( const_cast< FrameBufferWorld * > ( inFrame ) );
  Pixel pixel;
  Pixel nPixel;
  FrameBufferWorld sFrame;

  memset( outFrame->buffer, 0x00, outFrame->frameSize );
  for( unsigned int i = 0 ; i < inFrame->height; i++ )
    {
      rFIter.goRow( i );
      for( unsigned int j = 0; j < inFrame->width; j++, rFIter.goRight( ) )
	{
	  rFIter.getPixel( & pixel );
	  if ( ( pixel.red > 0.0 ) && ( pixel.green == 1 ) )
	    {
	      nPixel = pixel;
	      double xi, yi;
	      inFrame->bufferToWorldCoordinates( j, i, &xi, &yi );

	      unsigned int minx = outFrame->width - 1;
	      unsigned int miny = outFrame->height - 1;
	      unsigned int maxx = 1;
	      unsigned int maxy = 1;

	      int length = pixel.green;
	      double const xInc = 15.0;
	      double const xExt = 45.0;
	      double const yInc = 15.0;
	      double const yExt = 15.0;
	      
	      for( double xx = dmaxim( xi - length * xExt, -inFrame->field.margin ); xx < dminim( xi + length * xExt, inFrame->field.width + inFrame->field.margin); xx = xx + xInc )
		{
		  for( double yy = dmaxim( yi - yExt, - inFrame->field.margin ); yy < dminim( yi + yExt, inFrame->field.height + inFrame->field.margin); yy = yy + yInc )
		    {
		      unsigned int x, y;
		      outFrame->worldToBufferCoordinates( xx, yy, &x, &y );
		      if ( outFrame->isValidPoint( x, y ) )
			{
			  if ( x < minx )
			    {
			      minx = x;
			    }
			  if ( y < miny )
			    {
			      miny = y;
			    }
			  if ( x > maxx )
			    {
			      maxx = x;
			    }
			  if ( y > maxy )
			    {
			      maxy = y;
			    }
			  vi2d->interpolatePixel( frame, xx, yy, 0.0, &nPixel );

			  if ( ( yy == yi ) && ( xx == xi ) )
			    {
			      static_cast< FrameBuffer *>( outFrame )->setPixel( y, x, RawPixel(0, 255, 255 ) );
			    }
			  else
			    {
			      static_cast< FrameBuffer *>( outFrame )->setPixel( y, x, nPixel );
			    }
			}
		    }
		}
	      if ( ( minx + 2 < maxx ) && ( miny + 2 < maxy ) )
		{
		  FrameBufferWorld sFrame;
		  sFrame.width = maxx - minx + 1;
		  sFrame.height = maxy - miny + 1;
		  sFrame.bytesPerLine = outFrame->bytesPerLine;
		  sFrame.bytesPerPixel = outFrame->bytesPerPixel;
		  sFrame.buffer = outFrame->buffer + miny * outFrame->bytesPerLine + minx * outFrame->bytesPerPixel;
		  sFrame.frameSize = sFrame.height * sFrame.bytesPerLine;
		  //		  sFrame.drawBorder( RawPixel( 0, 255, 0 ) );
		  //		  sFrame.drawCenter( RawPixel( 255, 0, 0 ) );
		  //		  ImageProcessing::highlightFeatures( & sFrame, threshold );
		  
		  double circleConfidence;
		  unsigned int xc;
		  unsigned int yc;
		  if ( ( circleConfidence = ImageProcessing::detectCircle( &sFrame, RawPixel( 200, 200, 200 ), 2, &xc, &yc ) ) >= 0.4 )
		    {
		      ((FrameBuffer &)sFrame).setPixel( yc, xc, RawPixel( 0, 0, 255 ) );
		      //sFrame.drawBorder( RawPixel( 255, 255, 10 ) );
		    }
		  else
		    {
		      //		      ((FrameBuffer &)sFrame).fill( RawPixel( 50, 50, 50 ) );
		    }
		}
	    }
	}
    }
}


void Segmentation::runLengthEncode(FrameBuffer * frame)
{
  Run run;
  unsigned int row, col;
  FrameBufferIterator fbit(frame);
  runList.clear();

  //  std::cout << "rle enter\n";
  for(row = 0; row < frame->height; ++row)
    {
      fbit.goPosition(row,0);
      // initialize the first run.
      run.row = row;
      run.col = 0;
      run.colour = fbit.getIntensity();
      fbit.goRight();
      for(col = 1; col < frame->width; ++col)
	{
	  if(fbit.getIntensity() == 0 && run.colour != 0)
	    {
	      // store the run
	      run.length = col - run.col;
	      runList.push_back(run);
	      //  std::cout << "storing: " << run;
	      // initialize a new run
	      run.col = col;
	      run.colour = fbit.getIntensity();
	    }
	  else if(fbit.getIntensity() != 0 && run.colour == 0)
	    {
	      // start a run
	      run.col = col;
	      run.colour = fbit.getIntensity();
	    }
	  fbit.goRight();
	} // end for each pixel in a row
      // store end-of-row runs
      if(run.colour != 0)
	{
	  run.length = frame->width - run.col;
	  runList.push_back(run);
	  // std::cout << "storing: " << run;
	} // end if

    } // end for each row.
  
  //  std::cout << "rle exit\n";
}


// grow regions from the list of runs.
void Segmentation::runRegionMerge()
{
  unsigned int aIdx, bIdx; // indexes used to build the regions
  unsigned int size;

  // std::cout << "=========================\n=======================\n";
  
  aIdx = bIdx = 0;
  size = runList.size();
  regionList.clear();
  parent.clear();

  if(size > 0)
    newRunRegion(0);

  
  // std::cout << "rrm: runs: " << size << "\n";

  // std::cout << "=========================\n=======================\n";
  while(bIdx < size)
    {
      // std::cout << "loop head\n";
      // std::cout << "A: " << runList[aIdx];
      // std::cout << "B: " << runList[bIdx];
      if(runList[bIdx].row <= runList[aIdx].row)
	{
	  // std::cout << "B is on the same row as A\n";
	  // std::cout << "A: " << aIdx << "  B: " << bIdx << "\n";
	  // B is on the same row as A
	  // increment B.
	  bIdx++;
	}
      else if(runList[aIdx].row < runList[bIdx].row - 1)
	{
	  // std::cout << "A is more than one row behind B\n";
	  // std::cout << "A: " << aIdx << "  B: " << bIdx << "\n";
	  // A is more than one row behind B.
	  // increment A
	  aIdx++;
	  if(runList[aIdx].parent == -1)
	    newRunRegion(aIdx);
	}
      else
	{
	  if(runList[bIdx].isBelow(runList[aIdx]))
	    {
	      // std::cout << "B is below A\n";
	      // std::cout << "A: " << aIdx << "  B: " << bIdx << "\n";
	      // join the regions if they overlap
	      // the region with the youngest parent wins.
	      if(runList[bIdx].parent == -1)
		{
		  // std::cout << "B has no parent\n";
		  // std::cout << "A: " << aIdx << "  B: " << bIdx << "\n";
		  // B has no parent.  absorb B
		  regionList[parent[runList[aIdx].parent]].absorb(runList[bIdx]);
		  runList[bIdx].parent = runList[aIdx].parent;
		}
	      else if(runList[aIdx].parent < runList[bIdx].parent)
		{
		  // std::cout << "A absorbs B's parent\n";
		  // std::cout << "A: " << aIdx << "  B: " << bIdx << "\n";

		  // A absorbs B's parent region
		  regionList[parent[runList[aIdx].parent]].absorb(regionList[parent[runList[bIdx].parent]]);
		  // kill B's region
		  regionList[parent[runList[bIdx].parent]].numPixels = 0;
		  // B's parent region is replaced with A's
		  parent[runList[bIdx].parent] = parent[runList[aIdx].parent];
		}
	      else if(runList[aIdx].parent > runList[bIdx].parent)
		{
		  // std::cout << "B absorbs A's parent\n";
		  // std::cout << "A: " << aIdx << "  B: " << bIdx << "\n";

		  // B absorbs A's parent region
		  regionList[parent[runList[bIdx].parent]].absorb(regionList[parent[runList[bIdx].parent]]);
		  // kill A's region
		  regionList[parent[runList[aIdx].parent]].numPixels = 0;
		  // A's parent region is replaced with B's
		  parent[runList[aIdx].parent] = parent[runList[bIdx].parent];
		} // note, if the parents are the same, do nothing!
	    }
	  
	  if(runList[aIdx].colMax() > runList[bIdx].colMax())
	    {
	      // std::cout << "A's colmax > B's\nIncrement B\n";
	      bIdx++;
	    }
	  else
	    {
	      // std::cout << "B's colmax <= A's\nIncrement A\n";
	      aIdx++;
	      if(runList[aIdx].parent == -1)
		newRunRegion(aIdx);
	    }
	}
      // std::cout << "loop tail\n";
    }  

  // process the last row.
  while(aIdx < size)
    {
      if(runList[aIdx].parent == -1)
	newRunRegion(aIdx);
      aIdx++;
    }

}
void Segmentation::newRunRegion(unsigned int runIdx)
{
  // creates a new region using double indirect addressing.
  RunRegion r(runList[runIdx]);
  regionList.push_back(r);  
  parent.push_back(regionList.size() - 1);
  runList[runIdx].parent = parent.size() - 1;
  // std::cout << "newRunRegion(" << runIdx << "): " << runList[runIdx];
  // std::cout << r;
  // std::cout << runList[parent[runList[runIdx].parent]];
}

#endif
