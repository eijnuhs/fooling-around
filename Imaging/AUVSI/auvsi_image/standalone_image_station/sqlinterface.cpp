#include "sqlinterface.hpp"
#include "telemetry.hpp"
#include <iostream>
#include <exception>
#include <vector>
#include <cstdlib>
#include <boost/format.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

SqlInterface::SqlInterface() {

  try
    {
      con = new mysqlpp::Connection("auvsi", "localhost", "root", "", true);
      if (con->connected())
	{
	  std::cout << "Connected to db.\n";
	}
      else
	{
	  std::cout << "Could not connect to db.\n";
	}
    }
  catch (mysqlpp::BadQuery& er)
    {
      std::cerr << "Bad Query Error: " << er.what() << "\n";
    }
  catch (std::exception & er)
    {
      std::cerr << "Standard Error: " << er.what() << "\n";
      throw er;
    }
}

SqlInterface::~SqlInterface() {
  if (con->connected())
    {
      con->close();
      std::cout << "Connection closed.\n";
    }
  delete(con);
}

void SqlInterface::writeSnapshot(char snapshotNumber, boost::posix_time::ptime snapshotTime)
{
  std::stringstream queryString("", std::ios::out);
  queryString << "INSERT INTO snapshots VALUES (";
  queryString << (int)snapshotNumber << ", ";
  queryString << "'" << boost::posix_time::to_iso_string(snapshotTime).substr(0,15) << "',";
  queryString << (snapshotTime).time_of_day().fractional_seconds() << ");";

  std::cout << queryString.str() << std::endl;

  mysqlpp::Query q(con, true);

  try 
    {
    q.exec(queryString.str());
    }
  catch (mysqlpp::BadQuery& er)
    {
      std::cerr << "Error: " << er.what() << "\n";
    }
  catch (std::exception & er)
    {
      std::cerr << "Error: " << er.what() << "\n";
    }
}

void SqlInterface::writeTelemetry(struct telemetry* t)
{
  //std::cout << "SqlInterface::writeTelemetry: got data to write\n";

  std::stringstream queryString("", std::ios::out);
  queryString << "INSERT INTO telemetry VALUES (";
  queryString << "'" << boost::posix_time::to_iso_string(t->time).substr(0,15) << "',";
  queryString << (t->time).time_of_day().fractional_seconds() << ",";
  queryString << boost::format("%f") % t->longitude << ","; //boost::format for easy precision enforcement
  queryString << boost::format("%f") % t->latitude << ",";
  queryString << t->pitch << ",";
  queryString << t->roll << ",";
  queryString << t->airspeed << ",";
  queryString << t->groundspeed << ",";
  queryString << t->altitude << ",";
  queryString << t->vsi << ",";
  queryString << t->heading << ",";
  queryString << t->track << ",";
  queryString << t->status << ");";

  // std::cout << queryString.str() << "\n";

  mysqlpp::Query q(con, true);

  try 
    {
    q.exec(queryString.str());
    }
  catch (mysqlpp::BadQuery& er)
    {
      std::cerr << "Error: " << er.what() << "\n";
    }
  catch (std::exception & er)
    {
      std::cerr << "Error: " << er.what() << "\n";
    }

}


void SqlInterface::writeSmoothed(struct telemetry* t)
{
  // DELETE ENTRY IF ALREADY IN SMOOTHED DB
  std::stringstream deleteQuery("", std::ios::out);
  deleteQuery << "DELETE FROM smoothed WHERE ";
  deleteQuery << "TIME='" << boost::posix_time::to_iso_string(t->time).substr(0, 15) << "' AND ";
  deleteQuery << "MICROSECONDS=" << (t->time).time_of_day().fractional_seconds() << ";";

  //std::cout << deleteQuery.str() << "\n";

  mysqlpp::Query q0(con, true);
  try 
    {
    q0.exec(deleteQuery.str());
    }
  catch (mysqlpp::BadQuery& er)
    {
      std::cerr << "Delete Error: " << er.what() << "\n";
    }
  catch (std::exception & er)
    {
      std::cerr << "Delete Error: " << er.what() << "\n";
    }
    
  // WRITE FRESHLY SMOOTHED DATA
  std::stringstream queryString("", std::ios::out);
  queryString << "INSERT INTO smoothed (TIME, MICROSECONDS, LONGITUDE, LATITUDE, PITCH, ROLL, AIRSPEED, GROUNDSPEED, ALTITUDE, VSI, HEADING, TRACK, STATUS ) VALUES (";
  queryString << "'" << boost::posix_time::to_iso_string(t->time).substr(0, 15) << "',";
  queryString << (t->time).time_of_day().fractional_seconds() << ",";
  queryString << boost::format("%f") % t->longitude << ","; // boost::format for easy precision enforcement
  queryString << boost::format("%f") % t->latitude << ",";
  queryString << t->pitch << ",";
  queryString << t->roll << ",";
  queryString << t->airspeed << ",";
  queryString << t->groundspeed << ",";
  queryString << t->altitude << ",";
  queryString << t->vsi << ",";
  queryString << t->heading << ",";
  queryString << t->track << ",";
  queryString << t->status << ");";

  // std::cout << queryString.str() << "\n";

  mysqlpp::Query q(con, true);

  try 
    {
    q.exec(queryString.str());
    }
  catch (mysqlpp::BadQuery& er)
    {
      std::cerr << "Write Error: " << er.what() << "\n";
      std::cout << "Query \"" << queryString.str() << "\"\n";
    }
  catch (std::exception & er)
    {
      std::cerr << "Write Error: " << er.what() << "\n";
      std::cout << "Query \"" << queryString.str() << "\"\n";
    }
}

boost::posix_time::ptime SqlInterface::snapshotTimeCorrector(int snapshotNumber, boost::posix_time::ptime original)
{
  mysqlpp::Result result;

  mysqlpp::Query query(con, true);

  std::stringstream queryString("", std::ios::out);

  queryString << "SELECT time, microseconds ";
  queryString << "FROM snapshots WHERE ";
  queryString << "(number = '" << snapshotNumber << "');";

  std::cout << queryString.str() << std::endl;
  
  query << queryString.str();
  
  try
    {
      result = query.store();
    }
  catch (mysqlpp::BadQuery& er)
    {
      std::cerr << "Error: " << er.what() << "\n";
    }
  catch (std::exception & er)
    {
      std::cerr << "Error: " << er.what() << "\n";
    }


  boost::posix_time::ptime resultPTime;
  if (result.size() != 0)
    {
      int resultMicro = atoi(result.at(0).raw_data(1));

      std::stringstream resultIsoTimeString;
      std::string resultTimeString(result.at(0).raw_data(0));
      
      if ( resultTimeString.find(":") == std::string::npos )
	{
	  std::string resultIsoTimeString(resultTimeString.substr(0,8) + "T" + resultTimeString.substr(8));
	  resultPTime = boost::posix_time::from_iso_string(resultIsoTimeString);
	}
      else
	{
	  resultPTime = boost::posix_time::time_from_string(resultTimeString);
	}

      resultPTime += boost::posix_time::microsec(resultMicro);
      std::cout << "Improved time using snapshot time" << "\n";
    }
  else
    {
      resultPTime = original;
      std::cout << "Could not improve original Time" << "\n";
    }

  return resultPTime;
}


boost::posix_time::ptime SqlInterface::snapshotTimeCorrector(boost::posix_time::ptime inaccurateTime)
{
  mysqlpp::Result result;

  mysqlpp::Query query(con, true);

  // window of time to select telemetry
  boost::posix_time::ptime lowerBound;
  boost::posix_time::ptime upperBound;
  std::string lowerBoundString;
  std::string upperBoundString;

  lowerBound = inaccurateTime + boost::posix_time::seconds(-1);
  upperBound = inaccurateTime + boost::posix_time::seconds(1);
  upperBoundString = boost::posix_time::to_iso_string(upperBound);
  lowerBoundString = boost::posix_time::to_iso_string(lowerBound);

  std::string lowerWhole;
  std::string lowerFractional;
  std::string upperWhole;
  std::string upperFractional;

  int decimalIndex = lowerBoundString.find_first_of('.');
  if (decimalIndex == -1)
    {
      lowerWhole = lowerBoundString;
      lowerFractional = "0";
    }
  else
    {
      lowerWhole = lowerBoundString.substr(0, decimalIndex);
      lowerFractional = lowerBoundString.substr(decimalIndex+1, lowerBoundString.length() - decimalIndex);
    }

  decimalIndex = upperBoundString.find_first_of('.');
  if (decimalIndex == -1)
    {
      upperWhole = upperBoundString;
      upperFractional = "0";
    }
  else
    {
      upperWhole = upperBoundString.substr(0, decimalIndex);
      upperFractional = upperBoundString.substr(decimalIndex+1, upperBoundString.length() - decimalIndex);
    }

  std::string lowerWholeFinal = "";
  int tIndex = lowerWhole.find_first_of('T');
  lowerWholeFinal += lowerWhole.substr(0, tIndex);
  lowerWholeFinal += lowerWhole.substr(tIndex+1, lowerWhole.length() - tIndex);
  std::string upperWholeFinal = "";
  tIndex = upperWhole.find_first_of('T');
  upperWholeFinal += upperWhole.substr(0, tIndex);
  upperWholeFinal += upperWhole.substr(tIndex+1, upperWhole.length() - tIndex);

  std::stringstream queryString("", std::ios::out);

  queryString << "SELECT time, microseconds ";
  queryString << "FROM snapshots WHERE ";
  queryString << "(time = '" << lowerWholeFinal << "' AND microseconds >= '" << lowerFractional << "') ";
  queryString << "OR (time = '" << upperWholeFinal << "' AND microseconds <= '" << upperFractional << "') ";
  queryString << "OR (time > '" << lowerWholeFinal << "' AND time < '" << upperWholeFinal << "');";

  //std::cout << queryString.str() << "\n";
  
  query << queryString.str();
  
  try
    {
      result = query.store();
    }
  catch (mysqlpp::BadQuery& er)
    {
      std::cerr << "Error: " << er.what() << "\n";
    }
  catch (std::exception & er)
    {
      std::cerr << "Error: " << er.what() << "\n";
    }


  boost::posix_time::ptime resultPTime;
  if (result.size() != 0)
    {
      int resultMicro = atoi(result.at(0).raw_data(1));

      std::stringstream resultIsoTimeString;
      std::string resultTimeString(result.at(0).raw_data(0));
      
      if ( resultTimeString.find(":") == std::string::npos )
	{
	  std::string resultIsoTimeString(resultTimeString.substr(0,8) + "T" + resultTimeString.substr(8));
	  resultPTime = boost::posix_time::from_iso_string(resultIsoTimeString);
	}
      else
	{
	  resultPTime = boost::posix_time::time_from_string(resultTimeString);
	}

      resultPTime += boost::posix_time::microsec(resultMicro);
      std::cout << "Improved time using snapshot time" << "\n";
    }
  else
    {
      resultPTime = inaccurateTime;
      std::cout << "Could not improve inaccurate time" << "\n";
    }

  return resultPTime;
}

std::vector<struct telemetry> SqlInterface::getNeighbors(boost::posix_time::ptime target, int secondsMinus, int secondsPlus)
{
  std::vector<struct telemetry> neighbors;
  
  mysqlpp::Result result;

  mysqlpp::Query query(con, true);

  // window of time to select telemetry
  boost::posix_time::ptime lowerBound;
  boost::posix_time::ptime upperBound;
  std::string lowerBoundString;
  std::string upperBoundString;

  lowerBound = target + boost::posix_time::seconds(secondsMinus);
  upperBound = target + boost::posix_time::seconds(secondsPlus);
  upperBoundString = boost::posix_time::to_iso_string(upperBound);
  lowerBoundString = boost::posix_time::to_iso_string(lowerBound);

  std::string lowerWhole;
  std::string lowerFractional;
  std::string upperWhole;
  std::string upperFractional;

  int decimalIndex = lowerBoundString.find_first_of('.');
  if (decimalIndex == -1)
    {
      lowerWhole = lowerBoundString;
      lowerFractional = "0";
    }
  else
    {
      lowerWhole = lowerBoundString.substr(0, decimalIndex);
      lowerFractional = lowerBoundString.substr(decimalIndex+1, lowerBoundString.length() - decimalIndex);
    }

  decimalIndex = upperBoundString.find_first_of('.');
  if (decimalIndex == -1)
    {
      upperWhole = upperBoundString;
      upperFractional = "0";
    }
  else
    {
      upperWhole = upperBoundString.substr(0, decimalIndex);
      upperFractional = upperBoundString.substr(decimalIndex+1, upperBoundString.length() - decimalIndex);
    }

  std::string lowerWholeFinal = "";
  int tIndex = lowerWhole.find_first_of('T');
  lowerWholeFinal += lowerWhole.substr(0, tIndex);
  lowerWholeFinal += lowerWhole.substr(tIndex+1, lowerWhole.length() - tIndex);
  std::string upperWholeFinal = "";
  tIndex = upperWhole.find_first_of('T');
  upperWholeFinal += upperWhole.substr(0, tIndex);
  upperWholeFinal += upperWhole.substr(tIndex+1, upperWhole.length() - tIndex);

  std::stringstream queryString("", std::ios::out);

  queryString << "SELECT time, microseconds, longitude, latitude, altitude, pitch, roll, heading, track, airspeed ";
  queryString << "FROM telemetry WHERE ";
  queryString << "(time = '" << lowerWholeFinal << "' AND microseconds >= '" << lowerFractional << "') ";
  queryString << "OR (time = '" << upperWholeFinal << "' AND microseconds <= '" << upperFractional << "') ";
  queryString << "OR (time > '" << lowerWholeFinal << "' AND time < '" << upperWholeFinal << "');";

  //std::cout << queryString.str() << "\n";
  
  query << queryString.str();
  
  try
    {
      result = query.store();
    }
  catch (mysqlpp::BadQuery& er)
    {
      std::cerr << "Error: " << er.what() << "\n";
    }
  catch (std::exception & er)
    {
      std::cerr << "Error: " << er.what() << "\n";
    }

  for (unsigned int i = 0; i < result.size(); i++)
    {
      int resultMicro = atoi(result.at(i).raw_data(1));
      double resultlongitude = atof(result.at(i).raw_data(2));
      double resultlatitude = atof(result.at(i).raw_data(3));
      int resultaltitude = atoi(result.at(i).raw_data(4));
      double resultpitch = atof(result.at(i).raw_data(5));
      double resultroll = atof(result.at(i).raw_data(6));
      double resultheading = atof(result.at(i).raw_data(7));
      double resulttrack = atof(result.at(i).raw_data(8));
      int resultAirspeed = atoi(result.at(i).raw_data(9));

      std::stringstream resultIsoTimeString;
      std::string resultTimeString(result.at(i).raw_data(0));
      boost::posix_time::ptime resultPTime;
      
      if ( resultTimeString.find(":") == std::string::npos )
	{
	  std::string resultIsoTimeString(resultTimeString.substr(0,8) + "T" + resultTimeString.substr(8));
	  resultPTime = boost::posix_time::from_iso_string(resultIsoTimeString);
	}
      else
	{
	  resultPTime = boost::posix_time::time_from_string(resultTimeString);
	}

      resultPTime += boost::posix_time::microsec(resultMicro);
      
      struct telemetry t(resultPTime, resultlongitude,
			 resultlatitude, resultpitch,
			 resultroll, resultAirspeed,
			 0, resultaltitude,
			 0, resultheading,
			 resulttrack, 0);

      neighbors.push_back(t);
    }


  return neighbors;
}

