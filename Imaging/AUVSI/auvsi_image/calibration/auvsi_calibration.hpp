// Singleton wrapper class for the calibration code.
// Manages the two camera calibration objects (video and high res digital).
// Prevents two places (base station and image station) from accidentally using two different calibrations.

#ifndef _AUVSI_CALIBRATION_H
#define _AUVSI_CALIBRATION_H

#include <string>
#include "auvsi_common/gps.hpp"
#include "vigra/tinyvector.hxx"
#include "calibration.h"
#include "tsaiplanarcalibration.h"

typedef enum {
  LowRes,
  HighRes,
  NumCameras
} CameraType;

class AuvsiCalibration
{
public:
  static AuvsiCalibration* Instance()
  {
    if (!instance)
      {
	instance = new AuvsiCalibration;
      }
    return instance;
  }

  void initialize(CameraType selector, int width, int height, std::string pointsFile);
  
  // The roll, pitch, and yaw angles must be provided in radians.  is
  // assumed that with 0 degrees roll, pitch and yaw, that the
  // aircraft is positioned belly-down, with the nose of the aircraft
  // pointed north. Altitude must be specified in metres.
  GpsCoord gpsCoordFromImageCoord(CameraType selector, int imageX, int imageY, double roll, double pitch, double yaw,
				  GpsCoord position, double altitudeMetres);

private:
  inline AuvsiCalibration() {
    lowResCalibration = 0;
    highResCalibration = 0;
  }; // Prevent client construction
  inline AuvsiCalibration(const AuvsiCalibration&) {}; // Prevent client copying

  static AuvsiCalibration* instance; // the singleton instance
  TsaiPlanarCalibration* lowResCalibration;
  TsaiPlanarCalibration* highResCalibration;

  vigra::TinyVector<double, 3>** lowResTable; // 2d array
  vigra::TinyVector<double, 3>** highResTable; // 2d array

};

#endif
