#include "spot.h"
#include "colourdefinition.h"
#include "framebuffer.h"
#include "framebufferworld.h"
#include "run.h"
#include "framebufferrgb32.h"
#include "videointerpreter2d.h"


// private:
// ColourDefinition * colour;
// RegionList regions;

Spot::Spot(std::string name, double height, double radius, double mmPerPix, ColourDefinition * cd, enum Trackable::TrackableTypes classtype) :
  Trackable(classtype,name,height,radius,mmPerPix),
  colour(cd)
{
  // 0 
}
Spot::~Spot()
{
  
}

bool Spot::isMe(RunRegion & region, FrameBuffer * inFrame, FrameBufferWorld * worldFrame, unsigned int avgIntensity, bool updatePos)
{
  double lrx,lry,wx,wy;
  bool success = false;
  if(region.numPixels < maxPixels && region.numPixels > minPixels && colour)
    {
      //      std::cout << "attempting to match spot with colour: ";
      //      Pixel p = region.getAvgColour();
      //      p.writeTo(std::cout);
      //      std::cout << "\n";

      if((success = colour->isMatch(region.getAvgColour())))
	{
	  lrx = region.getCOMXD();
	  lry = region.getCOMYD();
	  worldFrame->bufferToWorldCoordinates(lrx,lry,&wx,&wy);

	  if(updatePos)
	    setPosition(wx,wy, 0.0, inFrame->t); 
	  //	   std::cout << "success!\n";
	}
    }
  return success;
}
// methods required by trackable.
bool Spot::isMe(RunRegion & region, FrameBuffer * inFrame, FrameBufferWorld * worldFrame, unsigned int avgIntensity)
{
  return isMe(region, inFrame, worldFrame, avgIntensity, true);
}
bool Spot::trackMe(FrameBuffer * inFrame, FrameBufferWorld * worldFrame)
{
   bool success = false;
   double wx,wy;
   double pixelDim = (Trackable::getRadius() * 8.0) / (double)Trackable::scratchpad()->height;
   unsigned int lrx,lry;
   // check if the centre of the object is black in the interpolated frame;
   Trackable::predictPosition(wx,wy,inFrame->t);
   if(!worldFrame->worldToBufferCoordinates(wx,wy,&lrx,&lry))
     {
       if(worldFrame->getIntensity(lry,lrx))
	 {
	       if(success = findMe(inFrame, pixelDim, &wx,&wy))
		 {
		   setPosition(wx,wy, 0.0, inFrame->t); 
		   Trackable::maskObject(worldFrame);  
		 }
	 }
     }

  return success;
}


bool Spot::findMe(FrameBuffer * inFrame, double pixelDim, double * rwx, double * rwy, WorldRegion * theRegion)
{
  double px,py, offset;
  double wx,wy, lrx, lry;
  unsigned int maxPix;
  int maxIdx;
  FrameBuffer * scratch = Trackable::scratchpad();
  Pixel p;
  RawPixel black(0,0,0);
  bool success = false;
  bool onRun = false;

  //  std::cout << "Spot::findMe()\n";
  
  if(scratch && inFrame && colour && getPercentFound() > 0.01)
    {
      // std::cout << "scratchpad: (" << scratch->height << "," << scratch->width << ")\n";
      //std::cout << "pixelDim: " << pixelDim << "\n";
      offset = -(((double)(scratch->height) * pixelDim)/2.0);
      Trackable::predictPosition(px,py,inFrame->t);
      FrameBufferIterator fbit(scratch);
      wy = py + offset;
      // make sure the predicted position is on the field
      if (Trackable::vi2d->interpolatePixel( inFrame, px, py, 0.0, &p))
	{
	  for(unsigned int row = 0; row < scratch->height; ++row)
	    {
	      wx = px + offset;
	      fbit.goPosition(row,0);
	      onRun = false;
	      for(unsigned int col = 1; col < scratch->width; ++col)
		{
		  //	      std::cout << row << "," << col << "-->" << wy << "," << wx << "\n";
		  Trackable::vi2d->interpolatePixel( inFrame, wx, wy, 0.0, &p);
		  p.update();
		  if(p.intensity() != 0 && colour->isMatch(p))
		    {
		      fbit.setPixel(p);
		      onRun = true;
		    }
		  else if(onRun)
		    { // this is an unsophisticated region bulking algorithm
		      fbit.setPixel(p);
		      onRun = false;
		      //		      std::cout << "bulking\n";
		    }
		  else
		    {
		      fbit.setPixel(black);
		    }
		  wx += pixelDim;
		  fbit.goRight();
		}
	      wy += pixelDim;
	    }
	  regionList.createRegions(scratch);
	  maxPix = 0;
	  maxIdx = -1;
	  for(uint i = 0; i < regionList.size(); ++i)
	    {
	      if(regionList[i].numPixels > maxPix)
		{
		  maxPix = regionList[i].numPixels;
		  maxIdx = i;
		}
	    }

	  if(maxIdx != -1)
	    {
	      success = true;
	      lrx = regionList[maxIdx].getCOMXD();
	      lry = regionList[maxIdx].getCOMYD();
	      
	      //      std::cout << "region: (" << lrx << "," << lry << ")\n";
	      *rwx = px + offset + (lrx * pixelDim);
	      *rwy = py + offset + (lry * pixelDim);
	      //	      std::cout << "predicted: (" << px << "," << py << ")\n";
	      //	      std::cout << "offset: (" << offset << "," << offset << ")\n";
	      //	      std::cout << "world: (" << wx << "," << wy << ")\n";
	      //	      std::cout << "found spot in scratchpad!\n";
	      fbit.goPosition((int)floor(lry), (int)floor(lrx));
	      fbit.setPixel(RawPixel(255,0,0));
	      
	      if(theRegion)
		*theRegion = WorldRegion(*rwx, *rwy, regionList[maxIdx], pixelDim);
	    

	    }
	  else
	    {
	      fbit.goPosition(1,1);
	      fbit.setPixel(RawPixel(0,0,255));
	    }  
	}
      //      else
      //	{
	  //	  std::cout << "Not found on field: (" << px << "," << py << "\n";
      //	}
    }
  return success;
}
