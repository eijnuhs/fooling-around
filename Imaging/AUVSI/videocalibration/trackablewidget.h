#ifndef __TRACKABLE_WIDGET_H_
#define __TRACKABLE_WIDGET_H_

#include <qtable.h>
#include <vector>
#include <qwidget.h>
////// Forward Declarations //////
class Trackable;
class FrameBuffer;

class FoundTableItem : public QTableItem
{
 public:
  FoundTableItem(QTable * table);
  ~FoundTableItem();
  
  void setFound(double init_found);
  void paint ( QPainter * p, const QColorGroup & cg, const QRect & cr, bool selected );
 private:
  int found;
};

class TrackableWidget : public QTable
{
  Q_OBJECT
 public:
  TrackableWidget(QWidget * parent = 0, const char * name = 0);
  ~TrackableWidget();
  void setTrackable(std::vector<Trackable *> & init_trackable);
 public slots:
  void updateDisplay();
 signals:
 void newScratchFrame(FrameBuffer *);
 private:
  std::vector<Trackable *> trackable;
  std::vector<FoundTableItem *> found;
};

#endif
