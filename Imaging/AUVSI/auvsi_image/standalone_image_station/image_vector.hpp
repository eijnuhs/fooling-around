#ifndef IMAGE_VECTORR_H
#define IMAGE_VECTORR_H

#include <boost/thread/mutex.hpp>
#include <boost/thread/condition.hpp>
#include <string>
#include <smoother.hpp>
#include <sqlinterface.hpp>
#include <deque>
#include <telemetry.hpp>

// the thread-safe image queue used for communication
// between image capture, telemetry and pattern recognition.

class ImageVector
{
public:
  ImageVector();
  ~ImageVector();

  void push_back(std::string filename);
  // the bool allows us to pop a filename off the
  // queue only if there is available smoothed telemetry
  // data available.  If there is not, the fn will return 
  // false and retFilename will be unchanged.
  bool getNext(std::string & retFilename, struct telemetry & retTelemetry, int increment = 1);
  
  void signalAll();

  unsigned int size();
  void setIdx(unsigned int idx);
  unsigned int idx();

private:
  boost::mutex m_mutex;
  std::vector<std::string> m_vector;
  boost::condition m_condition;
  Smoother<SqlInterface> smoother;
  unsigned int m_idx;
};


#endif
