#ifndef __VIDEO_CALIBRATION_H_
#define __VIDEO_CALIBRATION_H_

#include "square.h"
#include <list>
#include "controlpoints.h"
#include "controlpointscarpetcalib.h"
#include <functional>
#include "matrix/matrix.h"
#include <string>
#include <qobject.h>
#include <qmutex.h>
#include "tsaiplanarcalibration.h"
#include "colourdefinition.h"

using namespace std;

////// Forward Declarations /////
class VideoInterpreter2D;
class FrameBuffer;

struct GET_entry
{
  double xmin, ymin, xmax, ymax;
  double islope;
};

struct sPoint
{
  int xMin, xMax;
  int y;
};


class VideoCalibration 
{
public:
  VideoCalibration (VideoInterpreter2D * vi2d);
  ~VideoCalibration ();
  bool preprocessFrame(FrameBuffer * frame);
  void setIntensityMin(int iMin);
  void setIntensityMax(int iMax);
  void setPixelMin(int pMin);
  void setPixelMax(int pMax);
  void setState(std::string state);
  void enableProcessing(bool enable);
  void setControlPoints(int height, int width);
  ControlPoint *controlPoint (int sx, int sy);
  void setFeatureX(double init_featureX);
  void setFeatureY(double init_featureY);
  void setExternalFill(bool init_fillOn);
  std::string getCalibrationFilename( void );
  void saveCalibration(std::string filename);
  void loadCalibration(std::string filename);
  std::string getCalibrationErrorMessage( void );

  void addColour(int mx, int my);


  ControlPoints & getPoints();
  list <Square> & getSquares();
  QMutex mutex;


  ColourDefinition colour;

private:
  void calcScanLinesPolygon (struct GET_entry edgePoints[], int n, list< struct sPoint > & retList);//*
  void insertGET (struct GET_entry &new_edge, list < struct GET_entry >*GET); //*
  void addEdgesToAET (list < struct GET_entry >&GET, int scanline, list < struct GET_entry >*AET);//*
  void removeEdgesFromAET (int scanline, list < struct GET_entry >*AET); //*
  void updateEdgesInAET (list < struct GET_entry >&AET);
  bool lessThanAET (const struct GET_entry e1, const struct GET_entry e2);
  void assignBlockCoordinates (list < Square > &squares, Camera camera, bool interlaced);
  int assignLine (list < Square > &squares,  double x1, double y1,  double x2, double y2,  bool horizontal, int block_x, int block_y); //*
  int solveProjectionMatrix (list < Square > &squares, double rtMatrix[3][3]); //*
  void computeBlockCoordinates (list < Square > &squares, double rtMatrix[3][3]);//*
  void computeBlockCoordinates (list < Square > &squares, TsaiPlanarCalibration calib);
  void findSquares(FrameBuffer * frame, list <struct sPoint> scanlines);
  void recalibrate(FrameBuffer * frame);
  void updateColour(FrameBuffer * frame);

  bool preprocessEnabled;

  VideoInterpreter2D * vi2d;
  std::string state;
  int minPixels;
  int maxPixels;
  int minIntensity;
  int maxIntensity;
  list < Square > squares;
  double featureX;
  double featureY;
  bool fillOn;
  bool initCalibration;


  struct AET_Compare
  {
    bool operator () (const struct GET_entry & e1,
		      const struct GET_entry & e2)
    {
      if (e1.xmin < e2.xmin)
	return true;
      else
	return false;
    }
  }
  comp;

  class AET_EdgeDone:public unary_function < struct GET_entry, bool >
  {
    int scanLine;
  public:
      explicit AET_EdgeDone (const int &y)
    {
      scanLine = y;
    }
    bool operator () (const GET_entry & e1) const
    {
      return (int) rint (e1.ymax) < scanLine;
    }
  };

  ControlPointsCarpetCalib * points;

  int mousex, mousey;
};



#endif

