// $Id: camera.h,v 1.1.1.1.2.1 2005/01/23 05:24:37 cvs Exp $
//

#ifndef _CAMERA_H_
#define _CAMERA_H_

#include <string>

class Camera
{
public:
  Camera( int width = 640, int height = 480 );
  Camera( std::string cameraFilename );

  void init(int width, int height);

  void imageCoordToSensorCoord( double x, double y, double *xs, double *ys );
  void sensorCoordToImageCoord( double xs, double ys, double *x, double *y );

  double Ncx;
  double Nfx;
  double dx;
  double dy;
  double dpx;
  double dpy;
  double Cx;
  double Cy;
  double sx;
};

#endif
