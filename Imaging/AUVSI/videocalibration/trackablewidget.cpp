#include "trackablewidget.h"
#include <qpainter.h>
#include <qpen.h>
#include <qheader.h>
#include <qrect.h>
#include "trackable.h"
#include <iostream>
#include <qpalette.h> 

FoundTableItem::FoundTableItem(QTable * table) :
  QTableItem(table,QTableItem::Never),
  found(0)
{
  // 0
}
FoundTableItem::~FoundTableItem()
{
  // 0
}
  
void FoundTableItem::setFound(double init_found)
{
  found = (int)(init_found * 100);

  setText(QString("%1%").arg(found));
}
void FoundTableItem::paint ( QPainter * p, const QColorGroup & cg, const QRect & cr, bool selected )
{
//   int wid;
//   QColorGroup g( cg );
    
//   g.setColor( QColorGroup::Base, red );
//   QRect cellRect = table()->cellRect(row(), col());
//   wid = (cellRect.width() * found) / 100;
//   cellRect.setWidth((cellRect.width() * found) / 100);
//   QTableItem::paint( p, g, cellRect, selected );


  QTableItem::paint(p,cg,cr,selected);
  QRect cellRect = table()->cellRect(row(), col());
  p->setBrush(Qt::white);
  p->drawRect(cellRect);
  p->setBrush(Qt::green);
  cellRect.setWidth((cellRect.width() * found) / 100);
  p->drawRect(cellRect);
  p->setPen(Qt::black);
  p->drawText (cellRect, Qt::AlignHCenter, text());

  
}

TrackableWidget::TrackableWidget(QWidget * parent, const char * name) :
  QTable(0,8,parent,name)
{
  QHeader * columns;
  int widthstep = width()/11;
  setSelectionMode(QTable::NoSelection);
  setReadOnly(true);
  // create the rows/columns and set the formatting.

  columns = horizontalHeader();
  columns->setLabel(0,"Name",widthstep * 2);
  columns->setLabel(1,"Type",widthstep);
  columns->setLabel(2,"x",widthstep);
  columns->setLabel(3,"y",widthstep);
  columns->setLabel(4,"dx",widthstep);
  columns->setLabel(5,"dy",widthstep);
  columns->setLabel(6,"theta",widthstep);
  columns->setLabel(7,"Found",width() - widthstep * 8);

}
TrackableWidget::~TrackableWidget()
{

}

void TrackableWidget::setTrackable(std::vector<Trackable *> & init_trackable)
{
  FoundTableItem * foundPtr;
  setNumRows ( (int)init_trackable.size() );  
  QString typeLabel;
  

  for(uint i = 0; i < init_trackable.size(); ++i)
    {
      if(init_trackable[i])
	{
	  trackable.push_back(init_trackable[i]);
	  typeLabel = QString("%1::%2").arg(Trackable::TYPE_NAMES[trackable[i]->getType()]).arg(trackable[i]->getSubtype());
	  setText(i,0,trackable[i]->getName());
	  setText(i,1,typeLabel);
	  // create the foundtableitem.
	  foundPtr = new FoundTableItem(this);
	  setItem( i,7, foundPtr );
	  found.push_back(foundPtr);
	  setRowHeight(i,15);
	}
      else
	{
	  std::cerr << "Trackable item " << i << " was null!\n";
	}
    }
  updateDisplay();
}

void TrackableWidget::updateDisplay()
{
  double wx = 0.0, wy = 0.0;
  if(isVisible())
    {
      Trackable * t;
      // run through the list of trackable items updating each row/col.
      for(uint i = 0; i < trackable.size(); ++i)
	{
	  t = trackable[i];
	  
	  if(t->getFound())
	    {
	      t->getAdjustedCoordinates(&wx,&wy);
	      setText(i,2,QString::number(wx,'f',2));
	      setText(i,3,QString::number(wy,'f',2));
	      setText(i,4,QString::number(t->getDx(),'f',2));
	      setText(i,5,QString::number(t->getDy(),'f',2));
	      setText(i,6,QString::number(t->getTheta(),'f',2));
	      if(QTable::currentRow() == (int)i)
		{
		  FrameBuffer * f = t->scratchpad();
		  if(f)
		    emit newScratchFrame(f);
		  else
		    std::cerr << "No Scratchpad: " << t->getName() << "\n";
		}
	    }
	  found[i]->setFound(t->getPercentFound());
	  updateCell(i,7);

	}
    }
}

