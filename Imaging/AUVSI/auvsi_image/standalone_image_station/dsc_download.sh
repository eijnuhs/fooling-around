#!/bin/bash
# Switching to root and mounting the digital camera

ROOT_UID=0
E_WRONG_USER=65

if [ "$UID" -ne "$ROOT_UID" ]
then
    echo;
    echo "You must be root to run this script.";
    echo;
    exit $E_WRONG_USER
fi

if [ $# -ne 1 ];
then
    echo "Usage: ./dsc_download download_destination_folder";
    echo;
    exit 1;
fi

mount /dev/sda1 /usb &> /dev/null

if [ "$?" != 0 ];
then
    echo "Error mounting device.";
    echo "Check dmesg.";
    echo "Are you connected to the front USB ports?";
    echo "Is the camera turned on?"
    exit 1;
fi

DESTINATION_FOLDER=$1

if [ -a "$DESTINATION_FOLDER" ];
then
    echo "Folder already exists.";
    echo;
    exit 1;
fi

mkdir "$DESTINATION_FOLDER"

echo "Downloading files...";

cp -r /usb/DCIM/101MSDCF/* "$DESTINATION_FOLDER";

echo "Setting up permissions...";
chmod 755 "$DESTINATION_FOLDER";
chmod 644 "$DESTINATION_FOLDER"/*;

umount /usb

if [ "$?" != 0 ];
then
    echo "Error unmounting device.";
else
    echo "Device unmounted.";
fi