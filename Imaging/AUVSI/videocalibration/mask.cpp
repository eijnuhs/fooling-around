// $Id: mask.cpp,v 1.1.1.1 2004/09/16 23:12:05 cvs Exp $
// Jacky Baltes <jacky@cs.umanitoba.ca> Fri Jun  4 11:35:38 CDT 2004
//

#include "mask.h"
#include "playingfield.h"
#include "videointerpreter2d.h"
#include <assert.h>

#ifdef XXDEBUG
#undef min
#undef max
#include <iostream>
#endif

Mask::Mask( VideoInterpreter2D * vi2d )
{
  /* Find the left edge */
  
  unsigned int fheight = vi2d->getHeight();
  unsigned int fwidth = vi2d->getWidth();

  xMin = new unsigned int [ fheight ];
  xMax = new unsigned int [ fheight ];

  assert( ( xMin != 0 ) && ( xMax != 0 ) );

  for( unsigned int i = 0; i < fheight; i++ )
    {
      xMin[ i ] = fwidth - 1;
      for( unsigned int j = 0; j < fwidth; j++ )
	{
	  double xr, yr;
	  vi2d->screenToWorldCoordinates( j, i, 0.0, & xr, & yr );
	  if ( vi2d->field.isInside( xr, yr ) )
	    {
	      xMin[ i ] = j;
	      break;
	    }
	  else 
	    {
	      vi2d->screenToWorldCoordinates( j, i, 150.0, & xr, & yr );
	      if ( vi2d->field.isInside( xr, yr ) )
		{
		  xMin[ i ] = j;
		  break;
		}
	    }
	}

      xMax[ i ] = 0;
      for( unsigned int j = fwidth - 1; j < fwidth; j-- )
	{
	  double xr, yr;
	  vi2d->screenToWorldCoordinates( j, i, 0.0, & xr, & yr );
	  if ( vi2d->field.isInside( xr, yr ) )
	    {
	      xMax[ i ] = j;
	      break;
	    }
	  else 
	    {
	      vi2d->screenToWorldCoordinates( j, i, 150.0, & xr, & yr );
	      if ( vi2d->field.isInside( xr, yr ) )
		{
		  xMax[ i ] = j;
		  break;
		}
	    }
	}
    }
}

unsigned int
Mask::getMin( unsigned int row ) const
{
  return xMin[ row ];
}

unsigned int
Mask::getMax( unsigned int row ) const
{
  return xMax[ row ];
}

