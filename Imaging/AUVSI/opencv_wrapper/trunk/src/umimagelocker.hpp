#ifndef _UM_IMAGE_LOCKER_H_
#define _UM_IMAGE_LOCKER_H_

#include "umimage.hpp"
#include <boost/thread/mutex.hpp>

class UmImageLocker
{
public:
  template<class PixelType>
  UmImageLocker(UmImage<PixelType> & image) : m_lock(image.m_mutex){}
  template<class PixelType>
  UmImageLocker(UmImage<PixelType> & image, bool lock_condition) : m_lock(image.m_mutex, lock_condition){}
  ~UmImageLocker(){unlock();}
  
  void unlock()
  {
    if(m_lock.locked())
      m_lock.unlock();
  }
  bool locked()
  {
    return m_lock.locked();
  }
private:
  boost::mutex::scoped_lock m_lock;
};

#endif

