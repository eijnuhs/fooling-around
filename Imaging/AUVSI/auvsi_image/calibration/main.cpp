#include "tsaiplanarcalibration.h"
#include <string>
#include <iostream>
#include "auvsi_calibration.hpp"
#include "gps.hpp"

// Just an example of using the calibration code.

int main( int argc, char** argv )
{
  // A file to output camera data to
  std::string camFile("camera.dat");
  // A file with the image coord to world coord matchings
  std::string pointsFile("tennispoints.dat");

  if ( argc != 7 )
    {
      std::cout << "usage: " << std::endl;
      std::cout << "./a.out [longitude] [latitude] [altitude] [roll] [pitch] [yaw]" << std::endl;
    }
  else
    {
      double longitude = atof( argv[1] );
      double latitude = atof( argv[2] );
      double altitude = atof( argv[3] );
      double roll = atof( argv[4] );
      double pitch = atof( argv[5] );
      double yaw = atof( argv[6] );

      // Initialize our calibration with the data file
      std::cout << "initializing camera calibration...";
      std::cout.flush();
      AuvsiCalibration::Instance()->initialize(HighRes, 2032, 1524, pointsFile);
      std::cout << "done" << std::endl;
      
      GpsCoord transformPos = AuvsiCalibration::Instance()->gpsCoordFromImageCoord( HighRes, 0, 0,
										    roll, pitch, yaw, 
										    GpsCoord( longitude, latitude ),
										    altitude );

      std::cout << "TopLeft: " << transformPos << "\n";

      transformPos = AuvsiCalibration::Instance()->gpsCoordFromImageCoord( HighRes, 2031, 1523,
										    roll, pitch, yaw, 
										    GpsCoord( longitude, latitude ),
										    altitude );

      std::cout << "BottomRight: " << transformPos << "\n";
    }

  return 0;
}
