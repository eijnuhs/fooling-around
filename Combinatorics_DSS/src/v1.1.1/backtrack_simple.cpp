/*
 * Backtracking algorithm 
 * Goal is to look for matrix with the following properties
 * 1) Every pair columns have to have a 10 and 01 row
 * 2) Every triple have 100 or 010 or 001 row
 * 
 * The structure of the matrix is structured as 
 * a single row of integers.
 * 
 * Hueristic used
 * - By value  Num(n) > Num(n-1)
 * - By weight Weight(n) >= Weight(n-1);
 * - by weight test (refer to code below)
 * - by checking against previous solution
 * 
 * Progress:
 * Found the bug why it show 2 isomorphic matrix.
 * Its due to the tree is transverse is too deep to detect a weight change
 * */
#include "backtrack_simple.h"

#define DEBUG 0

int START 		=1;
int STOP  		=120;
int N_SIZE 		=7;
int ONE_SOLUTION=0;
/*
 * Make use of a column farm to 
 * */

using namespace std;

enum {ADDONE =0, WEIGHTED};

DSS_Matrix::DSS_Matrix(unsigned int rows)
{
	N					= rows;
	c_index				= 0;
	P 					= (1 << rows) - 2;	//all 0s and 1s are ignored 
	col_farm 			= NULL;
	weighted_col_farm 	= NULL;
	matrix				= NULL;
	solutions			= vector<unsigned int*>();
	start				= time(NULL);
	elapsed				= 0;
	stringstream fn;
	fn << "results_" << N << ".txt";
	saveTo 				= fn.str();
	createMatrix(rows);
	found = 0;
	
}

/*
 * Iterative Backtracking algorithm. Only hunts for M thus
 * 1 solution is enough
 * 
 * */
void
DSS_Matrix::backtrack_iterative(){
	
}

/*
 * Recursive Backtracking algorithm
 * !!! Naive approach !!!
 * 
 * */
void
DSS_Matrix::backtrack_recursive(int type){
	switch (type){
		case ADDONE:
			for(unsigned int first = 1;first<P;first=(first << 1) | 1){
				matrix[0]=first;
				cout << "Doing weight " << first << endl;
				backtrack_recursive_ADDONE(1);
			}
			break;
		case WEIGHTED:
			for(unsigned int first = 7;first<P;first=(first << 1) | 1){
				matrix[0]=first;
				
				stringstream info;
				info << "Doing weight " << bitsSet(first) << endl;
				cout << info.str();
				saveData(info.str(),NULL,saveTo);
				info.str("");
				
				start = time(NULL);
				backtrack_recursive_WEIGHTED(1);
				elapsed = time(NULL) - start;
								
				info << "Total Time for weight "<< bitsSet(first)  
					 << ": " << elapsed/60 << " mins" << endl;
				saveData(info.str(),NULL,saveTo);
				cout << info.str();				
			}
			break;
	}
}
/*
 * Backtracking recursion using weights lexigraphically
 * */
void 
DSS_Matrix::backtrack_recursive_WEIGHTED(unsigned int col_index){
	//found solution
	if(col_index == M){
		if((found%10) == 0) printMatrix();	
		found++;
		addSolution(); //add solution to list
		stringstream info;
		info << "found in " << (time(NULL) - start)/60 << " mins" << endl;
		saveData(info.str(),matrix, saveTo);		
		return;
	}
	
	//continue searching
	unsigned int start_index = 0;
	unsigned int prev_c 	= matrix[col_index-1];
	unsigned int prev_weight= bitsSet(prev_c) - 1;
	unsigned int weight 	= prev_weight;
		
	unsigned int pc = 0;
	for(unsigned int i = start_index;i < P && weight < N-1; i++){
		unsigned int c = weighted_col_farm[weight][i];
		//end of a weight column, go to next weight column
		if(c == 0){	
			weight++;
			i=0;
			continue;
		}
		/****** hueristic validation *******/
		//if(c<=prev_c)					continue; //over pruning
		if(weight == prev_weight && c <= prev_c) 				continue;
		if(weightChangeCheck(prev_weight,weight,c,col_index))	continue; 
		if(weightTest(pc,c,col_index))  						continue;
		
		//matrix checking
		if(isValidMatrix(c,col_index)){
			matrix[col_index] = c;
			backtrack_recursive_WEIGHTED(col_index+1);
			pc = c;			
			if(ONE_SOLUTION && found > 0) return; //return only 1 solution
		}			
	}
}

/*
 * Recursive backtrack linearly adding one
 * */
void 
DSS_Matrix::backtrack_recursive_ADDONE(unsigned int col_index){
	//found solution
	if(col_index == M){
		found ++;
		addSolution();
		if(found <= 10){
			printMatrix();
		}
		return;
	}
	//continue searching
	//int prev_weight = bitsSet(matrix[col_index-1]);
	unsigned int prev_i = 0;
	unsigned int start = matrix[col_index-1] + 1;
	for(unsigned int i = start;i < P; i = i+1){
		//hueristics
		if(weightTest(prev_i,i,col_index)) continue; 
				
		//checking
		if(isValidMatrix(i,col_index)){
			prev_i = i;
			matrix[col_index] = i;
			backtrack_recursive_ADDONE(col_index+1);
			//return only 1 solution
			if(ONE_SOLUTION && found > 0) return;
		}		
	}
}

unsigned int**
DSS_Matrix::generateWeightedColumn(){
	//dellocate memory	
	if(weighted_col_farm != NULL){
		int n_cols = sizeof(weighted_col_farm);
		for( int i = 0 ; i < n_cols; i++ )
			delete[] weighted_col_farm[i];
		delete[] weighted_col_farm;
	}
	
	//columns assigned by weight
	weighted_col_farm = new unsigned int *[N-1];
	if (weighted_col_farm == NULL) return NULL;
	
	for (unsigned int i = 0; i < N-1; i++){
		weighted_col_farm[i] = new unsigned int[P];
		//initialized to 0
		for(unsigned int j = 0;j < P;j++){
			weighted_col_farm[i][j] = 0;
		}
	}
		
	//keep track of last index
	int last_index[N-1];
	for(unsigned int i = 0;i<N-1;i++){
			last_index[i] = 0;
	}
	//simple sorting into bins
	unsigned int n,w;
	for(unsigned int i=0;i<P;i++){
		n = i+1;
		w = bitsSet(n);
		weighted_col_farm[w-1][last_index[w-1]++] = n;		
	}
	
	return weighted_col_farm;
}
/*
 * Generate all the possible columns given N this allows 
 * forward checking and constraint progation to take place
 * 
 * (!!! not implement in this version !!!)
 * Skips repeated columns for example 1100 and 0011 since rows are 
 * interchangeable 
 * */
void
DSS_Matrix::generateColumnFarm()
{
	//dellocate memory	
	if(col_farm != NULL){
		int n_cols = sizeof(col_farm);
		for( int i = 0 ; i < n_cols; i++ )
			delete[] col_farm[i];
		delete[] col_farm;
	}
	
	//allocate new memory
	col_farm = new unsigned int *[M];
	if (col_farm != NULL) {
		for (unsigned int i = 0; i < M; i++) {
			col_farm[i] = new unsigned int[P];
		}
	}
	
	//initializing the column farms.
	for(unsigned int i = 0; i < M ; i++){
		resetColumnFarm(i);
	}
}
/*
 * Generate individual columns
 * */
void 
DSS_Matrix::resetColumnFarm(unsigned int index){
	if(col_farm == NULL)
		return;
	
	if(DEBUG == 1){
		cout << "farm index " << index << " resetting" << endl;
	}
	
	// reset column farm index
	for(unsigned int j = 0; j < P; j++){
		col_farm[index][j] = j+1;
	}
}
/*
 * Creates the matrix with appropiate column size
 * */
void
DSS_Matrix::createMatrix(unsigned int col_size)
{
	M = col_size;
	found = 0;
	if(matrix != NULL)
		delete[] matrix;
	if(solutions.size())
		solutions.clear();
		
	//new matrix
	matrix 			= new unsigned int[M];	
	
	//initialize to zero
	for(unsigned int i = 0;i < M;i++)	matrix[i] = 0;
}


void 
DSS_Matrix::printMatrix(){
	cout << toString(matrix) << endl;
}
/*
 * Prints matrix in either numeric integers or full matrix
 * */
string 
DSS_Matrix::toString(unsigned int * m)
{
	if(m == NULL)
		return NULL;

	ostringstream ss;
	ss << "Solution #" << found << endl;
	char buffer[64];
	ss << "- ";
	for(unsigned int i=0;i<M;i++){
		sprintf (buffer, "%4d ",i+1);
		ss << buffer;
		//printf("%4d ",i+1);
	}
	ss<< "-" << endl;
	
	ss << "n ";
	for(unsigned int i=0;i<M;i++){
		sprintf (buffer, "%4d ",m[i]);
		ss << buffer;
		//printf("%4d ",m[i]);
	}
	ss << endl;
	
	for(unsigned int i=0;i<N;i++){
		ss << "[ ";
		int v, w = 0;
		for(unsigned int j=0;j<M;j++){
			v = m[j]>>i & 1;
			w += v;
			sprintf(buffer,"%4d ",v);
			ss << buffer;
			//printf("%4d ",v);
		}
		ss <<"] "<< w << " " << i+1 << endl;		
	}
	ss << "w ";
	for(unsigned int i=0;i<M;i++){
		sprintf(buffer,"%4d ",bitsSet(m[i]));
		ss << buffer;
		//printf("%4d ",bitsSet(m[i]));
	}	
	ss << endl ;
	string str = ss.str();
	return str;
}

//Testing functions

/*
 * This assumes the current matrix is correct and only test the new
 * column input. Returns true if matrix smaller than 3
 * 
 * test_matrix 		the matrix to test
 * input			the new column input to test
 * col				the column index
 **/
bool 
DSS_Matrix::isValidMatrix(unsigned int input,unsigned int col_index){
	if(DEBUG == 1)
		cout << "current index " << col_index << endl;
	
	//test pair
	for(unsigned int i = 0; i < col_index; i++){
		if(checkPair(matrix[i],input) == false)
			return false;
	}
	if(DEBUG == 1)
		cout << "test pair ok" << endl;
	
	//test triple
	for(unsigned int i = 0; col_index > 0 && i < col_index-1; i++){
		for(unsigned int j = i+1; j < col_index; j++){
			if(checkTriple(matrix[i],matrix[j],input) == false)
				return false;
			
		}
	}
	if(DEBUG == 1)
		cout << "test triple ok" << endl;
	return true;	
}
/* (NOT SURE IF THIS IS ACCURATE)
 * When there is a change in weight between col n and col n-1
 * The following check is performed against the last solution
 * 
 * It checks the weight of current matrix and last solution.
 * Rejects the value if current columns weights and 
 * previous solution are the same.
 * 
 * NOTE: if I disable weight change check, solutions would be left out
 * solutions but solutions of different weights will be return
 * */
bool
DSS_Matrix::weightChangeCheck(unsigned int prev_w,unsigned int w,unsigned int c,unsigned int c_index){
	if(solutions.size() == 0)
		return false;
		
	//weight change check
	if(prev_w >= w)	return false;
	
	unsigned int * lastSolution = solutions.back();	
	//checking the current column index
	int s,m;
	s = bitsSet(lastSolution[c_index]);
	m = bitsSet(c);
	if(s!=m) 		return false;
	//checking the rest of the matrix
	for(int i=c_index-1;i>=0;i--){
		s = bitsSet(lastSolution[i]);
		m = bitsSet(matrix[i]);
		if(s!=m)	return false;
	}
	return true;
}
/*
 * Comparing weights of 0s and 1s of one column to another
 * p			previous value tried
 * c			current value
 * col_index 	current index
 * */
bool 
DSS_Matrix::weightTest(unsigned int p,unsigned int c,unsigned int col_index){
	unsigned int prevColWeight1,prevColWeight0,currColWeight1,currColWeight0;
	for(int i = col_index-1;i >= 0;i--){
		prevColWeight1 = bitsSet(matrix[i] & p);
		prevColWeight0 = bitsSet(matrix[i] | p);
		currColWeight1 = bitsSet(matrix[i] & c);
		currColWeight0 = bitsSet(matrix[i] | c);
		if(prevColWeight1 != currColWeight1 || prevColWeight0 != currColWeight0){
			return false;			
		}
	}
	return true;	
}

bool 
DSS_Matrix::isValidMatrix()
{
	return testRulePair(matrix) && testRuleTriple(matrix);
}

/* Simple search through every pair 
 * This uses bitwise operations
 * */
bool 
DSS_Matrix::checkPair(unsigned int i, unsigned int j){
	unsigned a = i & j;
	if(a != i && a != j)
		return true;
	else 
		return false;
}
bool
DSS_Matrix::checkTriple(unsigned int i, unsigned int j, unsigned int k)
{
	unsigned a = i ^ j ^ k ^ (i & j & k);
	if(a != 0)
		return true;
	else 
		return false;
}
bool 
DSS_Matrix::testRulePair(unsigned int * test_matrix)
{
	for(unsigned int i = 0; i < M-1; i++){
		for(unsigned int j = i+1; j < M; j++){
			if(checkPair(test_matrix[i],test_matrix[j]) == false)
				return false;
		}
	}
	return true;
}
bool 
DSS_Matrix::testRuleTriple(unsigned int * test_matrix)
{
	for(unsigned int i = 0; i < M-2; i++){
		for(unsigned int j = i+1; j < M-1; j++){
			for(unsigned int k = j+1; k < M; k++){
				if(checkTriple(test_matrix[i],test_matrix[j],test_matrix[k]) == false)
					return false;
			}
		}
	}
	return true;
}	
void 
DSS_Matrix::setMatrix(unsigned int * new_matrix,unsigned int col_size, unsigned int row_size)
{
	resetMatrix(col_size,row_size);
	matrix = new_matrix;
}

void 
DSS_Matrix::resetMatrix(unsigned int col_size, unsigned int row_size)
{
	if(matrix != NULL)
		delete[] matrix;
	
	matrix 		= NULL;	
	M 			= col_size;
	N 			= row_size;
	c_index 	= col_size;
	P 			= (1 << row_size) -2;	//all 0s and 1s are ignored 
	found 		= 0;
}

unsigned int
DSS_Matrix::solutionFound(){
	return found;
}

void 
DSS_Matrix::saveData( string info){
	saveData(info,NULL);
}

void 
DSS_Matrix::saveData( string info, unsigned int * mat){
	saveData(info,mat, saveTo);
}

void 
DSS_Matrix::saveData( string info, unsigned int * mat, std::string filename )
	{
		ofstream file;
		file.open (filename.c_str(),ios::app);
		if(!file.is_open()) {
			cout << "fail to save to " << filename << endl;
			return;
		}
		
		if(mat != NULL)
			file << toString(mat)<< endl;		
		if( !info.empty())		
			file << info;	
		file.close();
					
	
	}

/* Functions used for debugging */
void
DSS_Matrix::printMatrixSize(){
	cout 	<< "M = " << M << " N = " << N << endl;
}
void
DSS_Matrix::setMValue(unsigned int m){
	M = m;
}
/* END OF DEBUGGING FUNCTIONS */

void 
DSS_Matrix::addSolution(){
	//Do a deep copy
	unsigned int * sol = new unsigned int[M];
	for(unsigned int i=0;i<M;i++)	sol[i] = matrix[i];
	solutions.push_back(sol);
}

int 
main(int argc, char* argv[]){
	
	opterr = 0;
	int c;

	while ((c = getopt (argc, argv, "f:t:n:o")) != -1)
	switch (c){
	case 'n':
		N_SIZE = atoi(optarg);
		break;
	case 'f':
		START = atoi(optarg);
		break;
	case 't':
		STOP = atoi(optarg);
		break;
	case 'o':
		ONE_SOLUTION = 1;
		break;
	case '?':
		if (optopt == 'n' || optopt == 't' || optopt == 'f')
			fprintf (stderr, "Option -%c requires an argument.\n", optopt);
		else if (isprint (optopt))
			fprintf (stderr, "Unknown option `-%c'.\n", optopt);
		else
			fprintf (stderr,
			"Unknown option character `\\x%x'.\n",
			optopt);
		return 1;
	default:
		return 1;
	}
	
	//START BACKTRACK
	stringstream info;
	DSS_Matrix *bt = new DSS_Matrix(N_SIZE);
	bt->generateWeightedColumn();
	int i;
	for(i = START;i < STOP;i++){
		bt->createMatrix(i);
		bt->printMatrixSize();
		info.str("");
		info << "M = " << i << " N = " << N_SIZE << endl;
		bt->saveData(info.str());
		bt->backtrack_recursive(WEIGHTED);
		info.str("");
		info << "Total solution found " << bt->solutionFound() << endl
			 << "--------------------------------------------" << endl;
			 
		cout << info.str();
		bt->saveData(info.str());
		if(bt->solutionFound() == 0) {
			break;
		}
	}
	info.str("");
	info << "Largest M FOR N = " << N_SIZE << " is " << i -1 << endl;
	bt->saveData(info.str());
	cout << info.str() << endl;
	
}

