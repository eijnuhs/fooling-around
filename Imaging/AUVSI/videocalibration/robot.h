#ifndef __ROBOT_H_
#define __ROBOT_H_

#include <string>
#include "trackable.h"
#include "histogram.h"
#include <vector>

#define C_NUM 3
#define BOT_RADIUS 38.1

//// Forward Declarations ////
class RunRegion;
class FeatureCircle;
class RegionList;

class Robot : public Trackable
{
 public:
  Robot(std::string name, double height, double radius, double mmPerPix, int subtype);
  ~Robot();
  
  bool isMe(RunRegion & region, FrameBuffer * inFrame, 
	    FrameBufferWorld * worldFrame, unsigned int avgIntensity);
  bool trackMe(FrameBuffer * inFrame, FrameBufferWorld * worldFrame);

  // colour definitions
  enum R_Colours{
    R_BLACK = 0,
    R_OTHER = 1,
    R_WHITE = 2,
    R_SIZE
  };

  static void Robot::breakUpBigRegion(FrameBuffer * inFrame, FrameBufferWorld * worldFrame, std::vector<Robot *> & notFound, RegionList & regionList, unsigned int regionIdx);

 private:
  // private methods used to identify a bot.
  void paintDebugFrame(double wxc, double wyc, FeatureCircle & feature, unsigned int intensities[]);
  void colourThreshold(unsigned int min, unsigned int max, unsigned int avg, unsigned int intensities[]);
  void interpolateStrip(double wxc, double wyc, FeatureCircle & feature, unsigned int intensities[], unsigned int & min, unsigned int & max, unsigned int & avg, FrameBuffer * inFrame);
  bool identifyRobot(unsigned int colours[], int cellColours[6], double & theta, double & wx, double & wy);
  bool findRobot(double & wx, double & wy, FrameBuffer * inFrame, int * retSubtype, double * retTheta);
  void adjustCentre(int start, int end, double & wx, double & wy);
  void addMeToScratchpad(FrameBuffer * inFrame);

  std::string getIntensityStripOutput();

  // instance variables
  unsigned int ** intensityArrays;
  unsigned int * medianIntensities;
  Histogram hist;
  unsigned int black, white;

  // the maximum and minimum H-W ratio 
  double minRatio;
  double maxRatio;
  
  FeatureCircle * features[C_NUM];
  
  friend class DebugViewer;

};

#endif
