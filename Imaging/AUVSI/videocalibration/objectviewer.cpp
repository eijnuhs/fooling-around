// $Id: objectviewer.cpp,v 1.1.1.1.2.1 2004/12/28 19:30:05 cvs Exp $
// Jacky Baltes jacky@cs.umanitoba.ca Tue May 25 17:58:28 CDT 2004
//

#include "objectviewer.h"
#include <mainwindow.h>
#include <qcombobox.h>
#include <iostream>
#include "pixel.h"
#include "framebuffer.h"

ObjectViewer::ObjectViewer( QWidget * parent, char const * name ) : FrameBufferViewer( parent, name )
{
  image = 0;
}

ObjectViewer::~ObjectViewer( )
{
  if ( image != 0 )
    {
      delete image;
    }
}


void ObjectViewer::updateQImageFromFrameBuffer( FrameBuffer * frame )
{
  QString mode = mainWindow->comboBox3->currentText();
  Pixel colors[] = 
    {
      Pixel( 255, 128, 0 ),
      Pixel( 255, 0, 0 ),
      Pixel( 0, 255, 0 ),
      Pixel( 0, 0, 255 ),
      Pixel( 255, 255, 0 ),
      Pixel(   0, 255, 255 ),
      Pixel( 255, 0, 255 ),
      Pixel( 0, 0, 0 ),
      Pixel( 255, 255, 255 )
    };

  checkImageWidthAndHeight( frame );

  FrameBufferIterator fIter( frame );
  Pixel pixel;
  for( unsigned int i = 0; i < frame->height; i++ )
    {
      uint32_t * p = (uint32_t *) image->scanLine( i );
      fIter.goRow( i );
      for( unsigned int j = 0; j < frame->width; j++, fIter.goRight() )
	{
#if 0
	  if ( ( i > 1 ) && ( i < frame->height - 1 ) && ( j > 1 ) && ( j < frame->width - 1 ) )
	    {
	      fIter.getBlurred3x3Pixel( & pixel );
	    }
	  else
	    {
		  fIter.getPixel( & pixel );
	    }
#else
	  fIter.getPixel( & pixel );
#endif
	  //	      static_cast<RawPixel>(pixel).adjustContrast( (RawPixel)(frame->getMinimum()), (RawPixel)(frame->getMaximum() ) );
	  //	      pixel.update();
	  Pixel d;
	  int minIndex = 0;
	  unsigned int max = pixel.max( );
	  unsigned int avg = ( pixel.red + pixel.green + pixel.blue ) / 3;
	  unsigned int range = max - avg;

	  if ( ( pixel.red == max ) && ( pixel.red > 128) && ( 10 * pixel.green <= 7 * max ) && ( 10 * pixel.blue <= 7 * max ) )
	    {
	      d = Pixel( 255, 0, 0 );
	    }
	  else if ( ( pixel.green  == max ) && ( pixel.green > 128 ) && ( 10 * pixel.red <= 7 * max ) && ( 10 * pixel.blue <= 7 * max ) )
	    {
	      d = Pixel( 0, 255, 0 );
	    }
	  else if ( ( pixel.blue == max ) && ( pixel.blue > 128 ) && ( 10 * pixel.red <= 7 * max ) && ( 10 * pixel.green <= 7 * max ) )
	    {
	      d = Pixel( 0, 0, 255 );
	    }
	  else if ( ( pixel.blue == max ) && ( pixel.blue > 128 ) && ( 10 * pixel.red <= 7 * max ) && ( 10 * pixel.green > 8 * max ) )
	    {
	      d = Pixel( 0, 255, 255 );
	    }
	  else
	    {
	      d = Pixel( max, max, max );
	    }
	  Pixel s;
	  
	  if ( mode == "None" )
	    {
	      s = pixel;
	    }
	  else if ( mode == "All" )
	    {
	      s = d;
	    }
	  else if ( mode == "Ball" ) 
	    {
	      if ( minIndex == 0 )
		{
		  s = Pixel( 0, 255, 128 );
		}
	      else if ( minIndex == 1 ) 
		{
		  s = Pixel( 0, 250, 128 );
		}
	      else
		{
		  s = pixel;
		}
	    }
	  QRgb rgb = qRgb( s.red, s.green, s.blue );
	  * p++ = rgb;
	}
    }
}

void ObjectViewer::printName()
{
  std::cout << "ObjectViewer\n";
}
