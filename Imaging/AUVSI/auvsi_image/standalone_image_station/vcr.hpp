#ifndef _VCR_HPP
#define _VCR_HPP

#include <iostream>
#include <fstream>
#include <vector>
#include <exception>
#include <umimage.hpp>
#include <umimagetypedefs.hpp>
#include <umimagegui.hpp>
#include <agmouse.hpp>
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/condition.hpp>
#include <umbotica/threads/threadwrapper.hpp>
#include <qslider.h>
#include <qinputdialog.h>
#include <qfiledialog.h>
#include <qstring.h>
#include <string>
#include <algorithm>
#include "boost/filesystem/path.hpp"
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/fstream.hpp"
#include <boost/date_time/posix_time/posix_time.hpp>
#include <umbotica/string_routines.hpp>
#include <umbotica/math/geometry.hpp>
#include "image_vector.hpp"
#include "telemetry.hpp"
#include "configuration.h"
#include "smoother.hpp"
#include "auvsi_calibration.hpp"
#include "helper_fns.hpp"


class Vcr : public umbotica::threads::ThreadWrapper
{
public:
#if defined(__APPLE__)
  QuickTimeProcessing::image_type image;
#else
  V4lProcessing::image_type image;
#endif

  QSlider * slider;
  std::string timeString;
  std::string gpsLongString;
  std::string gpsLatString;
  bool fakeTelemetry; // used to display an image even without telemetry data

  Vcr(ImageVector & vec, std::vector<GpsCoord> & roil, std::ofstream & _roiFile) : image_vec(vec), roiList(roil), roiFile(_roiFile)
  {
    previousValue = 0;
    paused = true;
    fakeTelemetry = false;
  }

  ~Vcr()
  {
  }

  void processing()
  {
    UmImageLocker lock(image);
    std::string filename;
    bool updateImage = false;

    if ( !paused && image_vec.size() > 0 && (image_vec.getNext(filename, telem, 5) || fakeTelemetry) )
      {	
	updateImage = true;
	if (slider->value() != previousValue)
	  {
	    image_vec.setIdx(slider->value());
	  }
      }
    else if ( paused && image_vec.size() > 0)
      {
	if (slider->value() != previousValue)
	  {
	    image_vec.setIdx(slider->value());
	    if ( image_vec.getNext(filename, telem, 0) || fakeTelemetry )
	      {
		updateImage = true;
	      }
	  }
      }

    if ( updateImage )
      {
	shapes.clear();
	image.loadImage(filename);
	doubleVertically(image);

	// draw the compass north arrow
	double theta = telem.track * M_PI / 180.0;
	double y = cos(theta - M_PI) * 30;
	double x = -sin(theta) * 30;
	
	AGColor color(255, 0, 0);
	AGShape imageCentre;
	imageCentre.circle(lowResWidth / 2, lowResHeight / 2, 10, color);
	if ( !fakeTelemetry )
	  {
	    AGShape northVector;
	    northVector.line(lowResWidth / 2, lowResHeight / 2, lowResWidth / 2 + (int)x, lowResHeight / 2 + (int)y, color, 2);
	    AGShape northLabel;
	    northLabel.text("N", lowResWidth / 2 + (int)(x * 1.2), lowResHeight / 2 + (int)(y * 1.2), color, QFont("Times New Roman") );
	    
	    shapes.push_back(imageCentre);
	    shapes.push_back(northVector);
	    shapes.push_back(northLabel);
	  }
	else
	  {
	    AGShape cross1;
	    cross1.line(lowResWidth / 2 - 30, lowResHeight / 2 - 30, lowResWidth / 2 + 30, lowResHeight / 2 + 30, color, 2);
	    AGShape cross2;
	    cross2.line(lowResWidth / 2 - 30, lowResHeight / 2 + 30, lowResWidth / 2 + 30, lowResHeight / 2 - 30, color, 2);
	    shapes.push_back(cross1);
	    shapes.push_back(cross2);
	  }

	if ( paused && !fakeTelemetry )
	  {
	    GpsCoord gpsTL = AuvsiCalibration::Instance()->gpsCoordFromImageCoord( LowRes, 0, 0,
										   telem.roll, telem.pitch, telem.track * M_PI / 180.0,
										   GpsCoord( telem.longitude, telem.latitude ),
										   telem.altitude * 0.3048 );
	    GpsCoord gpsTR = AuvsiCalibration::Instance()->gpsCoordFromImageCoord( LowRes, lowResWidth - 1, 0,
										   telem.roll, telem.pitch, telem.track * M_PI / 180.0,
										   GpsCoord( telem.longitude, telem.latitude ),
										   telem.altitude * 0.3048 );
	    GpsCoord gpsBR = AuvsiCalibration::Instance()->gpsCoordFromImageCoord( LowRes, lowResWidth - 1, lowResHeight - 1,
										   telem.roll, telem.pitch, telem.track * M_PI / 180.0,
										   GpsCoord( telem.longitude, telem.latitude ),
										   telem.altitude * 0.3048 );
	    GpsCoord gpsBL = AuvsiCalibration::Instance()->gpsCoordFromImageCoord( LowRes, 0, lowResHeight - 1,
										   telem.roll, telem.pitch, telem.track * M_PI / 180.0,
										   GpsCoord( telem.longitude, telem.latitude ),
										   telem.altitude * 0.3048 );
	    std::vector<GpsCoord> bounds;
	    bounds.push_back(gpsTL);
	    bounds.push_back(gpsTR);
	    bounds.push_back(gpsBR);
	    bounds.push_back(gpsBL);
	    AGShape tlLabel;
	    AGShape trLabel;
	    AGShape brLabel;
	    AGShape blLabel;
	    tlLabel.text(std::string() + gpsTL.toICAOString(), 0, 10, color, QFont("Times New Roman") );
	    trLabel.text(std::string() + gpsTR.toICAOString(), lowResWidth - 110, 10, color, QFont("Times New Roman") );
	    brLabel.text(std::string() + gpsBR.toICAOString(), lowResWidth - 110, lowResHeight - 2, color, QFont("Times New Roman") );
	    blLabel.text(std::string() + gpsBL.toICAOString(), 0, lowResHeight - 2, color, QFont("Times New Roman") );
	    shapes.push_back(tlLabel);
	    shapes.push_back(trLabel);
	    shapes.push_back(brLabel);
	    shapes.push_back(blLabel);
	    
	    std::vector<GpsCoord>::const_iterator it = roiList.begin();
	    for ( ; it != roiList.end(); it++ )
	      {
		if ( umbotica::geometry::isInside( bounds, (*it) ) )
		  {
		    double minDistance = __DBL_MAX__;
		    int targetRow = 0;
		    int targetCol = 0;
		    int row = 0;
		    int col = 0;
		    
		    // wierd inefficient search for location to draw region of interest
		    for (row = 0; row < lowResHeight; row+=5)
		      {
			for (col = 0; col < lowResWidth; col+=5)
			  {
			    // gps is still in metres
			    GpsCoord gpsTest = AuvsiCalibration::Instance()->gpsCoordFromImageCoord( LowRes, col, row,
												     telem.roll, telem.pitch, telem.track * M_PI / 180.0,
												     GpsCoord( telem.longitude, telem.latitude ),
												     telem.altitude * 0.3048 );
			    double longdiff = gpsTest.longitude() - it->longitude();
			    double latdiff = gpsTest.latitude() - it->latitude();
			    
			    double distance = sqrt(longdiff * longdiff + latdiff * latdiff);

			    if (distance < minDistance)
			      {
				minDistance = distance;
				targetRow = row;
				targetCol = col;
			      }
			  }
		      }

		    AGShape roiCircle;
		    roiCircle.circle(targetCol, targetRow, 3, color, true);
		    shapes.push_back(roiCircle);
		  }
	      }
	  }
	else if ( paused && fakeTelemetry )
	  {
	    AGShape tlLabel;
	    AGShape trLabel;
	    AGShape brLabel;
	    AGShape blLabel;
	    tlLabel.text("Fake Telemetry", 0, 10, color, QFont("Times New Roman") );
	    trLabel.text("Fake Telemetry", lowResWidth - 110, 10, color, QFont("Times New Roman") );
	    brLabel.text("Fake Telemetry", lowResWidth - 110, lowResHeight - 2, color, QFont("Times New Roman") );
	    blLabel.text("Fake Telemetry", 0, lowResHeight - 2, color, QFont("Times New Roman") );
	    shapes.push_back(tlLabel);
	    shapes.push_back(trLabel);
	    shapes.push_back(brLabel);
	    shapes.push_back(blLabel);
	  }

	filename = filename.substr(filename.find("/")+1, filename.find(".")-filename.find("/")-1);
	boost::posix_time::ptime pt = boost::posix_time::from_iso_string(filename);
	std::stringstream timeStringStream;
	timeStringStream << pt;
	timeString = timeStringStream.str();
	slider->setMinValue(0);
	slider->setMaxValue(image_vec.size());
	slider->setValue(image_vec.idx());
	previousValue = slider->value();
      }
  }

  void pause()
  {
    paused = true;
  }
  
  void jump(int jumpAmount)
  {
    paused = true;
    slider->setValue(slider->value() + jumpAmount);
  }

  void play()
  {
    paused = false;
  }

  void addToROI()
  {
    roiList.push_back(gpsClick);
    std::cout << "Added ROI: " << gpsClick.latitudeString() << ", " << gpsClick.longitudeString() << std::endl;
    roiFile << gpsClick.longitude() << " " << gpsClick.latitude() << std::endl;
  }

  std::vector<AGShape> getShapes()
  {
    return shapes;
  }

  void mouseEvent(AGPoint point, int flags)
  {
    int eventType = flags & AG::EventTypeMask;
    int button = flags & AG::MouseButtonMask;
    int keyButton = flags & AG::KeyButtonMask;

    if ( button == AG::LeftButton && !fakeTelemetry )
      {
	gpsClick = AuvsiCalibration::Instance()->gpsCoordFromImageCoord( LowRes, point.x, point.y,
										  telem.roll, telem.pitch, telem.track * M_PI / 180.0,
										  GpsCoord( telem.longitude, telem.latitude ),
										  telem.altitude * 0.3048 );
	gpsLongString = gpsClick.longitudeString();
	gpsLatString = gpsClick.latitudeString();
      }
  }

private:
  bool paused;
  ImageVector & image_vec;
  int previousValue;
  struct telemetry telem;
  GpsCoord gpsClick;
  std::vector<GpsCoord> & roiList;
  std::vector<AGShape> shapes;
  std::ofstream & roiFile;

};

#endif
