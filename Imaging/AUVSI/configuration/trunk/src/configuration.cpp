#include "configuration.h"
#include <iostream>


Configuration::Configuration(std::string init_name)
{
  name = init_name;
}

Configuration::~Configuration(){}

Node * Configuration::getRootNode(std::string name)
{
  if(config)
    {
      return config->findNode(name.c_str());
    }
  else
    {
      std::cerr << "Configuration::getRootNode() attempt to get node before initialization.\n";
      return 0;
    }
}

Node * Configuration::getRootNode()
{
  return getRootNode(name); 
}

bool Configuration::init(std::string name)
{
  std::cout << "Parsing configuration file "  << name << "\n";
  config = new DOTCONFDocument(DOTCONFDocument::CASEINSENSETIVE);
  if(config->setContent(name.c_str()) == -1){
    std::cerr << "Configuration::init configuration file error\n";
    config = 0;
    return false;
  }
  
  return true;
}

DOTCONFDocument * Configuration::config = 0;


Node * Configuration::getNode(std::string name, Node * parent)
{
  Node * node = 0;
  if(parent)
    {
      // get the requested branch
      node = config->findNode(name.c_str(), parent);
      if(node == 0)
	{
	  std::cout << this->name << " unable to get value for " << name << "\n";
	}
    }

  return node; 
}
Node * Configuration::getNode(std::string name, std::string rootName)
{
  return getNode(name, getRootNode(rootName));
}
Node * Configuration::getNode(std::string  name)
{
  return getNode(name, this->name);
}

int Configuration::getInt(Node * node, int defaultValue, int base, bool * success)
{
  bool completed = false;
  int result = defaultValue;
  const char * val = node ? node->getValue() : 0;
  if(val)
    {
      int temp = (int)strtol(val,0,base);
      if(!errno)
	{
	  result = temp;
	  completed = true;
	}
      else
	errno = 0;
    }
  
  if(success)
    {
      *success = completed;
    }
  
  if(!completed)
    {
      if(node)
	std::cout << this->name << " unable to get value from " << node->getName() << "\n";
    }

  return result;
}

int Configuration::getInt(std::string name, int defaultValue, int base, bool * success)
{
  return getInt(getNode(name), defaultValue, base, success);
}


std::string Configuration::getString(std::string name, std::string defaultValue, bool * success)
{
  return getString(getNode(name), defaultValue, success);
}

std::string Configuration::getString(Node * node, std::string defaultValue, bool * success)
{
  bool completed = false;
  std::string parsed = defaultValue;
  const char * val = node ? node->getValue() : 0;
  if(val)
    {
      parsed = val;
      completed = true;
    }
    
  
  if(success)
    {
      *success = completed;
    }

  if(!completed)
    {
      if(node)
	std::cout << this->name << " unable to get value from " << node->getName() << "\n";
    }


  return parsed;

}

unsigned int Configuration::getUnsigned(Node * node, unsigned int defaultValue, int base, bool * success)
{
  bool completed = false;
  unsigned int result = defaultValue;
  const char * val = node ? node->getValue() : 0;
  if(val)
    {
      unsigned int temp = (unsigned int)strtoul(val,0,base);
      if(!errno)
	{
	  result = temp;
	  completed = true;
	}
     else
	errno = 0;
    }
  
  if(success)
    {
      *success = completed;
    }

  if(!completed)
    {
      if(node)
	std::cout << this->name << " unable to get value from " << node->getName() << "\n";
    }


  return result;
}
unsigned int Configuration::getUnsigned(std::string  name, unsigned int defaultValue, int base, bool * success)
{
  return getUnsigned(getNode(name), defaultValue, base, success);
}

double Configuration::getDouble(Node * node, double defaultValue, bool * success)
{
  bool completed = false;
  double result = defaultValue;
  const char * val = node ? node->getValue() : 0;
  if(val)
    {
      double temp = strtod(val,0);
      if(!errno)
	{
	  result = temp;
	  completed = true;
	}
      else
	errno = 0;
    }
  
  if(success)
    {
      *success = completed;
    }

  if(!completed)
    {
      if(node)
	std::cout << this->name << " unable to get value from " << node->getName() << "\n";
    }

  return result;
}
double Configuration::getDouble(std::string  name, double defaultValue, bool * success)
{
  return getDouble(getNode(name), defaultValue, success);
}

std::vector<Node *> Configuration::getNodeList(std::string name, Node * parent)
{
  std::vector<Node *> nodeList;
  parent = parent ? parent : getRootNode();
  Node * node = 0;
  
  if(parent)
    {
      while( (node = config->findNode(name.c_str(),parent,node)) != 0)
      {
	nodeList.push_back(node);
      }
    }

  return nodeList;
}


