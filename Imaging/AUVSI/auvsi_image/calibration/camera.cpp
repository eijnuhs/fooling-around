// $Id: camera.cpp,v 1.1.1.1.2.1 2005/01/23 05:24:37 cvs Exp $
//

using namespace std;

#include "camera.h"
#include <iostream>
#include <fstream>
#include <stdlib.h>

Camera::Camera (int width, int height)
{
  init(width, height);
//   Ncx = (double) width;
//   Nfx = (double) width;
//   dx = 0.015;
//   dy = 0.015;
//   dpx = dx * Ncx / Nfx;
//   dpy = dy;
//   Cx = ((double) width) / 2.0;
//   Cy = ((double) height) / 2.0;
//   sx = 1.00;
}

Camera::Camera (std::string filename)
{
#ifdef DEBUG
  cout << "Camera::Camera( std::string " << filename << ") called" << endl;
#endif
  ifstream fin ( filename.c_str() );

  if (!fin)
    {
      cerr << "Camera::Camera(" << filename << ") failed\n";
      init(640,480);
      return;
    }

  fin >> Ncx >> Nfx >> dx >> dy >> dpx >> dpy;
  fin >> Cx >> Cy >> sx;
  if (!fin)
    {
      cerr << "Camera::Camera(" << filename << ") load failed\n";
      exit ( EXIT_FAILURE );
    }
#ifdef DEBUG
  cout << "Camera::Camera( const char * " << filename << "), Ncx " << this->Ncx << ", Nfx " << Nfx << ", dpx " << dpx << ", dpy " << dpy << endl;
#endif
}

void Camera::init(int width, int height)
{
  Ncx = (double) width;
  Nfx = (double) width;
  dx = 0.015;
  dy = 0.015;
  dpx = dx * Ncx / Nfx;
  dpy = dy;
  Cx = ((double) width) / 2.0;
  Cy = ((double) height) / 2.0;
  sx = 1.00;
}

void
Camera::imageCoordToSensorCoord (double x, double y, double *xs, double *ys)
{
  double dx = x - Cx;
  double dy = y - Cy;
  double xd = dpx * dx / sx;
  double yd = dpy * dy;

  *xs = xd;
  *ys = yd;
}

void
Camera::sensorCoordToImageCoord (double xs, double ys, double *x, double *y)
{
  double dx, dy;

  dx = xs / dpx * sx;
  dy = ys / dpy;

  *x = dx + Cx;
  *y = dy + Cy;
}
