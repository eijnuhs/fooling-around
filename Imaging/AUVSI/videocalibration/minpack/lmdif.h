#ifndef _LMDIF_H_
#define _LMDIF_H_

#ifdef __cplusplus
extern "C" {
#endif
#include "f2c.h"

/* Subroutine */ 
int lmdif_(S_fp fcn, integer *m, integer *n, doublereal *x, 
	   doublereal *fvec, doublereal *ftol, doublereal *xtol, doublereal *
	   gtol, integer *maxfev, doublereal *epsfcn, doublereal *diag, integer *
	   mode, doublereal *factor, integer *nprint, integer *info, integer *
	   nfev, doublereal *fjac, integer *ldfjac, integer *ipvt, doublereal *
	   qtf, doublereal *wa1, doublereal *wa2, doublereal *wa3, doublereal *
	   wa4);

#ifdef __cplusplus
	}
#endif

#endif
