// $Id: videostream.cpp,v 1.1.1.1.2.17 2004/12/28 19:30:05 cvs Exp $
//

#include "filevideostream.h"
#include <iostream>
#include "framebufferrgb565.h"
#include "segmentation.h"
#include "videointerpreter2d.h"
#include "videocalibration.h"
#include "videostreamconfig.h"
#include "framespersecond.h"
#include "tracker.h"
#include <qstringlist.h>
//#include <umbotica/string_routines.hpp>
#include <sys/time.h>
#include <qimage.h>
#include <boost/filesystem/convenience.hpp>

#ifdef DEBUG_PTF
#include <iostream>
using namespace std;
#endif

namespace fs = boost::filesystem;

FileVideoStream::FileVideoStream(std::string path) : done( false ), videoInterpreter2D( 0 ), tracker(0), path(path)
{
  // load and sort the image file names
  if(fs::exists(path))
    {
      fs::directory_iterator i = fs::directory_iterator(path);
      fs::directory_iterator end = fs::directory_iterator();
      
      bool hw_init = false;

      QStringList formatlist = QImage::inputFormatList();
      
      QStringList::iterator fl = formatlist.begin(),flend = formatlist.end();
      if(fl != flend)
	{
	  std::cout << "Formats accepted: " << (*fl).ascii();
	  fl++;
	
	  for( ; fl != flend; fl++)
	    {
	      std::cout << ", " << (*fl).ascii();
	    }
	  std::cout << std::endl;
	}

      for (; i != end; ++i)
	{
	  std::cout << "Testing file: " << i->native_directory_string() << std::endl;
	  if(i->has_leaf())
	    {
	      std::cout << "extension: " << extension(*i) << std::endl;
	      QStringList format = formatlist.grep(extension(*i).c_str(), false);
	      if(format.size() > 0)
		{
		  // make sure the image is of an accepted type...
		  image_files.push_back(*i);
		  if(!hw_init)
		    {
		      QImage temp;
		      temp.load(i->native_directory_string().c_str());
		      m_height = temp.height();
		      m_width = temp.width();
		      hw_init = true;
		    }
		}
	      else if(extension(*i) == "ppm")
		{
		  image_files.push_back(*i);
		  if(!hw_init)
		    {
		      FrameBufferRGB32 frame;
		      frame.inFromPPM(i->native_directory_string());
		      m_height = frame.height;
		      m_width = frame.width;
		      hw_init = true;
		    }
		}
	    }
	}
      
      sort(image_files.begin(), image_files.end());
      std::cout << "accepted image files: " << image_files.size() << std::endl;
    }

}

void FileVideoStream::sendFPSupdate()
{
  struct timeval now;
  double timeDiff;
  double fps;

  if ( gettimeofday( & now, 0  ) != 0 )
    {
      std::cerr << "FileVideoStream::updateFPS gettimeofday failed\n";
      perror( "FileVideoStream::updateFPS gettimeofday ");
    }

  timeDiff = FramesPerSecond::getTimeDiff(prev,now);

  memcpy( & prev, & now, sizeof( struct timeval ) );
  //  fps = (fps * 0.8) + ((1000000.0/timeDiff)*0.2);
  fps = (1000000.0/timeDiff);
  emit fpsUpdate(fps);
}
  

void FileVideoStream::run( void )
{
  FrameBufferRGB32 frame;
  QImage temp_frame;
  done = false;

  std::vector<boost::filesystem::path>::iterator f_it = image_files.begin();

  while(f_it != image_files.end() && ! done )
    {
      frame.mutex->lock();
      // load the next frame with the next image.
      temp_frame.load(f_it->native_directory_string());
      
      //  printName();
      //  std::cout << "FBV update QImage: " << (void *)frame << "\n";
      frame.initialize(FrameBuffer::RGB32 , temp_frame.width(), temp_frame.height() );      
      
      FrameBufferIterator fIter( &frame );
      for( unsigned int i = 0; i < frame.height; i++ )
	{
	  fIter.goRow( i );
	  for( unsigned int j = 0; j < frame.width; j++, fIter.goRight() )
	    {
	      QRgb pix = temp_frame.pixel(j,i);
	      RawPixel pixel(qRed(pix), qGreen(pix), qBlue(pix));
	      fIter.setPixel( pixel ,0);
	    }
	}
	      
    
      frame.mutex->unlock();

      vCalibration->preprocessFrame( & frame );

      emit newFrame( & frame );
      sendFPSupdate();
      // modulus increment the image iterator
      f_it++;
      if(f_it == image_files.end())
	f_it = image_files.begin();
      
    }

}


unsigned int FileVideoStream::width(){ return m_width;}
unsigned int FileVideoStream::height(){return m_height;}

bool 
FileVideoStream::isInterlaced( void )
{
  return false;
}

// void
// FileVideoStream::setSegmentation( Segmentation * seg )
// {
//   segmentation = seg;
// }

// Segmentation *
// FileVideoStream::getSegmentation( void  )
// {
//   return segmentation;
// }


VideoDevice *
FileVideoStream::getVideoDevice() const
{
  return NULL;
}

void
FileVideoStream::setVideoInterpreter2D( VideoInterpreter2D * vi2d )
{
  videoInterpreter2D = vi2d;
}

VideoCalibration * 
FileVideoStream::getVideoCalibration( void )
{
  return vCalibration;
}
void 
FileVideoStream::setVideoCalibration(VideoCalibration * vCalibration )
{
#ifdef DEBUG_PTF
  std::cout << "FileVideoStream::VideoCalibrationViewer::setVideoCalibration\n";
  std::cout << "vCal = " << (void *) vCalibration << std::endl;
#endif
  this->vCalibration = vCalibration;
}


void FileVideoStream::setTracker(Tracker * trak)
{
  tracker = trak;
}
