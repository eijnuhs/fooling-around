// $Id: framebufferviewer.cpp,v 1.1.1.1.2.17 2005/02/01 19:40:00 cvs Exp $
// A QT widget that displays a FrameBuffer
// Jacky Baltes <jacky@cs.umanitoba.ca> Tue Oct 21 21:46:10 CDT 2003

#include "framebufferviewer.h"
#include <qevent.h>
#include <sstream>
#include "framebuffer.h"
#include <qpainter.h>
#include <qcolor.h>
#include <iostream>
#include <time.h>
#include <inttypes.h>
#include "pixel.h"
#include <mainwindow.h>
#include <qstatusbar.h>
#include <qlcdnumber.h>
#include <qfiledialog.h>
#include <math.h>

using namespace std;

FrameBufferViewer::FrameBufferViewer( QWidget * parent, char const * name )
  : QWidget( parent, name ), 
    image( 0 ),
    frame(0) ,
    needRedraw(false)
{
  setWFlags( WNoAutoErase );

  gettimeofday( & prev, 0 );

  QWidget * mparent = gotoParent( this, 5 );
  mainWindow = dynamic_cast<Form1 *>(mparent);
  assert( mainWindow != 0 );

  // hookup the redraw timer
  QObject::connect( ( (QObject const *) FrameBufferViewer::redrawTimer  ), SIGNAL( timeout() ),( ( QObject const * ) ( this ) ), SLOT( redraw() ) );
}

FrameBufferViewer::~FrameBufferViewer()
{
}

void FrameBufferViewer::newFrame( FrameBuffer * frame )
{
  
  this->frame = frame;

  needRedraw = true;
  // this code made obsolete by the redraw signal.
//   if ( isVisible() )
//     {
//       this->updateQImageFromFrameBuffer( frame );
//       update( 0, 0, frame->width, frame->height );
//     }
}

void FrameBufferViewer::paintEvent( QPaintEvent * e)
{
#ifdef XXDEBUG
  std::cout << "FrameBufferViewer::paintEvent called" << endl;
#endif
  e = 0; // kill warning;
  if ( image != 0 )
    {
      QPixmap pm( size() );

      QPainter p;
      
      p.begin( &pm, this );
#ifdef SAFE_DRAW
      // mutex.lock();
      QMutexLocker qml(&mutex);
      p.drawImage( QRect( 0, 0, width(), height() ), *image );
      //      mutex.unlock();
#else
      p.drawImage( QRect( 0, 0, width(), height() ), *image );
#endif

      //mainWindow->lcdFramesPerSec->display(fps);

      this->paintChild(p);

      p.end();
      bitBlt( this, 0, 0, &pm );
    }
}

void FrameBufferViewer::paintChild(QPainter & p) 
{
  p.worldMatrix(); // kill warning
}


void FrameBufferViewer::updateQImageFromFrameBuffer( FrameBuffer * frame )
{
  RawPixel pixel;
  //  printName();
  //  std::cout << "FBV update QImage: " << (void *)frame << "\n";
  
  if ( ( frame->type == FrameBuffer::RGB32 ) || ( frame->type == FrameBuffer::RGB565 ) || ( frame->type == FrameBuffer::WORLD_FRAME ) )
    {
      checkImageWidthAndHeight( frame );
      FrameBufferIterator fIter( frame );
#ifdef SAFE_DRAW
      // mutex.lock();
      QMutexLocker qm(&mutex);
#endif
      //      if(frame.fieldNo == V4L2_FIELD_BOTTOM)
      for( unsigned int i = 0; i < frame->height; i++ )
	{
	  uint32_t * p = (uint32_t *) image->scanLine( i );
	  fIter.goRow( i );
	  for( unsigned int j = 0; j < frame->width; j++, fIter.goRight() )
	    {
	      fIter.getPixel( & pixel );
	      *p++ = qRgb( pixel.red, pixel.green, pixel.blue );
	    }
	}
      
    }
  else
    {
      std::cerr << "updateQImageFromFrameBuffer: frame->bytesPerPixel " << frame->bytesPerPixel << " type " << frame->type << " is not supported\n";
      if ( image != 0 )
	{
	  delete image;
	}
      image = 0;
      return;
    }

}



void FrameBufferViewer::checkImageWidthAndHeight( FrameBuffer * frame )
{
  if ( ( image == 0 ) || ( frame->width != static_cast<unsigned int>(image->width()) ) || ( frame->height != static_cast<unsigned int>(image->height()) ) || ( image->depth() != 32 ) )
    {
      if ( image != 0 ) 
	{
	  delete image;
	}
      image = new QImage( frame->width, frame->height, 32 );
    }
}

QWidget * FrameBufferViewer::gotoParent( QWidget * w, int level ) const
{
  QWidget * result = 0;

  if ( w == 0 )
    {
      result = 0;
    }
  else
    {
      if ( level <= 0 )
	{
	  result = w;
	}
      else
	{
	  result = gotoParent( w->parentWidget(), level - 1 );
	}
    }
  return result;
}

void
FrameBufferViewer::mousePressEvent( QMouseEvent * e )
{
  displayColourInfo(e);
}

void
FrameBufferViewer::mouseMoveEvent( QMouseEvent * e )
{
  displayColourInfo(e);
}

void FrameBufferViewer::mouseDoubleClickEvent ( QMouseEvent * e )
{
  // save the current frame.
  if(e->state() == Qt::ControlButton && e->button() == Qt::LeftButton)
    {
      // get the filename.
      QString s = QFileDialog::getSaveFileName(QString::null, "PNG file (*.png)", mainWindow, "Save Image", "Choose a filename to save under" );
      if(!s.isNull() && !s.isEmpty())
	{
	  // save the QImage.
	  image->save(s,"PNG", 100);
	}
    }

#ifdef DEBUG_PTF
  std::cout << "FrameBufferViewer::mouseDoubleClickEvent\n";
  std::cout << "state: " << e->state() << "\n";
  std::cout << "button: " << e->button() << "\n";
#endif
}

void
FrameBufferViewer::displayColourInfo(QMouseEvent * e)
{
  int x = e->x();
  int y = e->y();

  if ( ( image != 0 ) && ( x >= 0 ) && ( y < width() ) && ( y >= 0 ) && ( y < height() ) )
    {
      double scaleX = static_cast< double> ( image->width() ) / static_cast< double> ( width() );
      double scaleY = static_cast< double> ( image->height() ) / static_cast< double> ( height() );
      
      int showX = static_cast<int>( scaleX * static_cast<double>( x ) );
      int showY = static_cast<int>( scaleY * static_cast<double>( y ) );
      
      uint8_t *p = (uint8_t *)( ( uint32_t * ) image->scanLine( showY ) + showX );
      int intensity = (int)( 0.299 * (*(p+2)) + 0.587 *  (*(p+1)) + 0.114 *  (*(p+0)));
      QString s = QString("R:%1 G:%2 B:%3 Intensity:%4").arg(*(p + 2) ).arg(*(p+1) ).arg(*(p+0) ).arg(intensity);
      
      mainWindow->statusBar()->message( s, 1000 );
    }

}

void 
FrameBufferViewer::screenToWidgetCoordinates( int sx, int sy, int * wx, int * wy )
{
  // assume that the image is resized to fill the widget.
  screenToWidgetCoordinates( (double)sx, (double) sy, wx, wy );
}

void FrameBufferViewer::screenToWidgetCoordinates( double sx, double sy, int * wx, int * wy )
{
  *wx =(int) rint( ((double)QWidget::width()  / (double)frame->width )   * sx) ;
  *wy =(int) rint( ((double)QWidget::height() / (double)(frame->height * (frame->interlaced ? 1.0 : 2.0) )  * sy)) ;

#ifdef XXDEBUG
  cout << "FrameBufferViewer::screenToWidgetCoordinates\n";
  if(frame->interlaced)
    cout << "interlaced frame\n";
  else
    cout << " not interlaced frame\n";
  cout << "Widget w,h:  (" << QWidget::width() << "," << QWidget::height() << ")\n";
  cout << "Frame w,h:   (" << frame->width << "," << frame->height << ")\n";
  cout << "Screen: x,y: (" << sx << "," << sy << ")\n";
  cout << "Widget: x,y: (" << *wx << "," << *wy << ")\n";
#endif

}
void 
FrameBufferViewer::widgetToScreenCoordinates( int wx, int wy, int * sx, int * sy )
{
  // assume that the image is resized to fill the widget.
  *sx = (int) rint(((double)frame->width / (double)QWidget::width()) * (double)wx ) ;
  *sy = (int) (( (double) frame->height * (frame->interlaced ? 1.0 : 2.0) / (double) QWidget::height() ) * (double)wy);
 

#ifdef XXDEBUG
  cout << "FrameBufferViewer::widgetToScreenCoordinates\n";
  if(frame->interlaced)
    cout << "interlaced frame\n";
  else
    cout << " not interlaced frame\n";
  cout << "Widget w,h:  (" << QWidget::width() << "," << QWidget::height() << ")\n";
  cout << "Frame w,h:   (" << frame->width << "," << frame->height << ")\n";
  cout << "Screen: x,y: (" << *sx << "," << *sy << ")\n";
  cout << "Widget: x,y: (" << wx << "," << wy << ")\n";
#endif

}

void FrameBufferViewer::frameToWidgetCoordinates( int sx, int sy, int * wx, int * wy )
{
  frameToWidgetCoordinates( (double)sx, (double) sy, wx, wy );
}
void FrameBufferViewer::frameToWidgetCoordinates( double sx, double sy, int * wx, int * wy )
{
  *wx =(int) rint( ((double)QWidget::width()  / (double)frame->width )  * sx) ;
  *wy =(int) rint( ((double)QWidget::height() / (double)frame->height)  * sy) ;
}
void FrameBufferViewer::widgetToFrameCoordinates( int wx, int wy, int * sx, int * sy )
{
  *sx = (int) rint(((double)frame->width / (double) QWidget::width()  ) * (double)wx) ;
  *sy = (int) (( (double) frame->height  / (double) QWidget::height() ) * (double)wy);
}

void FrameBufferViewer::screenToFrameCoordinates( int sx, int sy, int * fx, int * fy )
{
  *fx = sx;
  if(!frame->interlaced)
    {
      *fy = sy / 2;
    }
  else
    {
      *fy = sy;
    }
}
void FrameBufferViewer::frameToScreenCoordinates( int fx, int fy, int * sx, int * sy )
{
  *sx = fx;
  if(!frame->interlaced)
    {
      *sy = fy*2;
    }
  else
    {
      *sy = fy;
    }
}

void FrameBufferViewer::startTimer( int msec )
{
  redrawTimer->start(msec);
}

void FrameBufferViewer::redraw()
{
  if(frame && isVisible() && needRedraw)
    {
      //      std::cout << "locking in redraw: " << (void*)frame << "\n";
      //      printName();
      //      std::cout << "this: " << (void *)this << "\n";
      QMutexLocker qml(frame->mutex);
      this->updateQImageFromFrameBuffer( frame );
      //      std::cout << "unlocking in redraw" << (void*)&frame << "\n";
      update();
      needRedraw = false;
    }
}

void FrameBufferViewer::printName()
{
  std::cout << "FrameBufferViewer\n";
}

QTimer * FrameBufferViewer::redrawTimer = new QTimer();
