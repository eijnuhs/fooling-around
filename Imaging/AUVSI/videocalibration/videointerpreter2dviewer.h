// $Id: videointerpreter2dviewer.h,v 1.1.1.1.2.8 2005/02/01 19:40:00 cvs Exp $
// A QT widget that displays a FrameBuffer
// Jacky Baltes <jacky@cs.umanitoba.ca> Tue Oct 21 21:46:10 CDT 2003

#ifndef _VIDEO_INTERPRETER_2D_VIEWER_H_
#define _VIDEO_INTERPRETER_2D_VIEWER_H_

#include <qwidget.h>
#include <qevent.h>
#include "framebufferviewer.h"
#include <qimage.h>
#include <time.h>
#include "videointerpreter2d.h"
#include <mainwindow.h>

class VideoInterpreter2DViewer : public FrameBufferViewer
{
  Q_OBJECT

 public: 
  VideoInterpreter2DViewer( QWidget * parent = 0, char const * name = 0 );
  ~VideoInterpreter2DViewer();
  void paintChild( QPainter & p);
  void setVideoInterpreter2D( VideoInterpreter2D * vi2d );
  void mousePressEvent(QMouseEvent * e);
  void mouseMoveEvent(QMouseEvent * e);
  void mouseReleaseEvent(QMouseEvent * e);
  void updateQImageFromFrameBuffer( FrameBuffer * frame );
  void printName();
 public slots:
 virtual void newFrame( FrameBuffer * );
 void loadRotation();
 void saveRotation();
 void resetControlPoints();
 private:
  void showScreenPoint( QPainter & p, double x, double y, std::string label );  
  void drawRotationMatrix( QPainter & p );
  void drawMask( QPainter & p );
  void drawControlPoints( QPainter & p );
  void updateControlPoint(int xm, int ym);
  static QWidget * gotoParent( QWidget *, int level );

  VideoInterpreter2D * vi2d;
  ControlPoint * grabbed;
  Form1 * mainWindow;
};

#endif


