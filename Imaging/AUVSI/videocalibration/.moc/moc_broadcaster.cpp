/****************************************************************************
** Broadcaster meta object code from reading C++ file 'broadcaster.h'
**
** Created: Fri Feb 17 15:31:23 2006
**      by: The Qt MOC ($Id: qt/moc_yacc.cpp   3.3.4   edited Jan 21 18:14 $)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#undef QT_NO_COMPAT
#include "../broadcaster.h"
#include <qmetaobject.h>
#include <qapplication.h>

#include <private/qucomextra_p.h>
#if !defined(Q_MOC_OUTPUT_REVISION) || (Q_MOC_OUTPUT_REVISION != 26)
#error "This file was generated using the moc from 3.3.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

const char *Broadcaster::className() const
{
    return "Broadcaster";
}

QMetaObject *Broadcaster::metaObj = 0;
static QMetaObjectCleanUp cleanUp_Broadcaster( "Broadcaster", &Broadcaster::staticMetaObject );

#ifndef QT_NO_TRANSLATION
QString Broadcaster::tr( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "Broadcaster", s, c, QApplication::DefaultCodec );
    else
	return QString::fromLatin1( s );
}
#ifndef QT_NO_TRANSLATION_UTF8
QString Broadcaster::trUtf8( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "Broadcaster", s, c, QApplication::UnicodeUTF8 );
    else
	return QString::fromUtf8( s );
}
#endif // QT_NO_TRANSLATION_UTF8

#endif // QT_NO_TRANSLATION

QMetaObject* Broadcaster::staticMetaObject()
{
    if ( metaObj )
	return metaObj;
    QMetaObject* parentObject = QObject::staticMetaObject();
    static const QUParameter param_slot_0[] = {
	{ "trackable", &static_QUType_ptr, "std::vector<Trackable*>", QUParameter::InOut },
	{ "frame", &static_QUType_ptr, "FrameBuffer", QUParameter::In }
    };
    static const QUMethod slot_0 = {"newTrackable", 2, param_slot_0 };
    static const QMetaData slot_tbl[] = {
	{ "newTrackable(std::vector<Trackable*>&,FrameBuffer*)", &slot_0, QMetaData::Public }
    };
    metaObj = QMetaObject::new_metaobject(
	"Broadcaster", parentObject,
	slot_tbl, 1,
	0, 0,
#ifndef QT_NO_PROPERTIES
	0, 0,
	0, 0,
#endif // QT_NO_PROPERTIES
	0, 0 );
    cleanUp_Broadcaster.setMetaObject( metaObj );
    return metaObj;
}

void* Broadcaster::qt_cast( const char* clname )
{
    if ( !qstrcmp( clname, "Broadcaster" ) )
	return this;
    if ( !qstrcmp( clname, "UDPBroadcaster" ) )
	return (UDPBroadcaster*)this;
    return QObject::qt_cast( clname );
}

bool Broadcaster::qt_invoke( int _id, QUObject* _o )
{
    switch ( _id - staticMetaObject()->slotOffset() ) {
    case 0: newTrackable((std::vector<Trackable*>&)*((std::vector<Trackable*>*)static_QUType_ptr.get(_o+1)),(FrameBuffer*)static_QUType_ptr.get(_o+2)); break;
    default:
	return QObject::qt_invoke( _id, _o );
    }
    return TRUE;
}

bool Broadcaster::qt_emit( int _id, QUObject* _o )
{
    return QObject::qt_emit(_id,_o);
}
#ifndef QT_NO_PROPERTIES

bool Broadcaster::qt_property( int id, int f, QVariant* v)
{
    return QObject::qt_property( id, f, v);
}

bool Broadcaster::qt_static_property( QObject* , int , int , QVariant* ){ return FALSE; }
#endif // QT_NO_PROPERTIES
