#ifndef UMBOTICA_PROFILE_STAT_HPP
#define UMBOTICA_PROFILE_STAT_HPP

#include <umbotica/threads/null_mutex.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <sstream>
#include <numeric>
#include <boost/function.hpp>
#include <boost/bind.hpp>
#include <iomanip>

namespace pt = boost::posix_time;

namespace umbotica { namespace stats {
#ifdef UMBOTICA_NO_STATISTICS

  class ProfileNodeBase
  {
  public:
    ProfileNodeBase(std::string) {}
    virtual ~ProfileNodeBase(){}
    virtual pt::time_duration total() = 0;
    std::string name() const { return "no statistics"; }
    virtual std::pair<std::string, pt::time_duration> internal_results(unsigned int indent_number, unsigned int longest_sibling_name) const = 0;
  };


  
  class ProfileNode : public ProfileNodeBase
  {
  public:
    ProfileNode(std::string name, ProfileNode * parent = NULL) : ProfileNodeBase(name)
    { }
    
    // accumulate the total time of the node's children
    pt::time_duration total()
    {
      return pt::time_duration();
    }

    void add_child(ProfileNodeBase *)
    {  }

    std::pair<std::string, pt::time_duration> internal_results(unsigned int, unsigned int) const
    {
      return std::pair<std::string, pt::time_duration>();
    }

  protected:
    std::pair<std::string, pt::time_duration> collect_child_output(unsigned int) const
    {
      return std::pair<std::string, pt::time_duration>();
    }

  };


  class profile : public umbotica::stats::RuntimeStat, public ProfileNode
  {
  public:
    template<typename STAT_TRACKER_T>
    profile(std::string category, std::string name, STAT_TRACKER_T * statTrack) :
      RuntimeStat(category, name, statTrack),
      ProfileNode(name)
    { }
    
    std::string results() const
    {
      return "No statistics";
    };
    
  };


 
  // forward declaration.
  class ScopedProfiler;
   
  template<typename MUTEX_T = umbotica::threads::null_mutex>
  class ProfileNodeLeaf : public ProfileNodeBase
  {
  public:
    typedef MUTEX_T mutex_t;
    typedef typename mutex_t::scoped_lock lock_t;
 
    ProfileNodeLeaf(std::string name, ProfileNode * parent = NULL) : ProfileNodeBase(name)
    {  }

    pt::time_duration total()
    {
      return pt::time_duration();
    }

    friend class ScopedProfiler;
  protected:
    std::pair<std::string, pt::time_duration> internal_results(unsigned int, unsigned int) const
    {
      return std::pair<std::string, pt::time_duration>();
    }


  private:
    void add_time_elapsed(pt::time_duration const &)
    { }

  };


  class ScopedProfiler
  {
  public:
    template<typename MUTEX_T>
    ScopedProfiler(ProfileNodeLeaf<MUTEX_T>) 
    { }
    
    ~ScopedProfiler(){exit();}

    void exit()
    {  }

  };


#else
  class ProfileNodeBase
  {
  public:
    ProfileNodeBase(std::string name) : m_name(name){}
    virtual ~ProfileNodeBase(){}
    virtual pt::time_duration total() = 0;
    std::string name() const { return m_name; }
    virtual std::pair<std::string, pt::time_duration> internal_results(unsigned int indent_number, unsigned int longest_sibling_name) const = 0;
  private:
    std::string m_name;
  };


  
  class ProfileNode : public ProfileNodeBase
  {
  public:
    ProfileNode(std::string name, ProfileNode * parent = NULL) : ProfileNodeBase(name), m_longest_child_name(0)
    {
      if(parent)
	parent->add_child(this);
    }
    
    // accumulate the total time of the node's children
    pt::time_duration total()
    {
      pt::time_duration t;
      std::vector<ProfileNodeBase *>::iterator c_p = m_children.begin(), c_end = m_children.end();
      for(; c_p != c_end; c_p++)
	t += (*c_p)->total();
      return t;
    }

    void add_child(ProfileNodeBase * child)
    {
      m_children.push_back(child);
      m_longest_child_name = std::max(m_longest_child_name, child->name().size());
    }

    std::pair<std::string, pt::time_duration> internal_results(unsigned int indent_number, unsigned int longest_sibling_name) const
    {
      // return my total plus the list of my children.
      std::pair<std::string, pt::time_duration> c_result;
      c_result = collect_child_output(indent_number);

      // now produce my output...
      std::ostringstream out; 
      if(indent_number > 0)
	out << std::setfill('\t') << std::setw(indent_number) << "";
      out << std::setiosflags(std::ios::left) << std::setfill(' ') 
	  << std::setw(longest_sibling_name + 2) << (name() + ": ") << c_result.second;
      
      c_result.first = out.str() + "\n" + c_result.first;
      return c_result;
    }

  protected:
    std::pair<std::string, pt::time_duration> collect_child_output(unsigned int indent_number) const
    {
      std::pair<std::string, pt::time_duration> c_result, c_temp;
      // collect the output from child nodes, accumulating the total time
      std::vector<ProfileNodeBase *>::const_iterator c_p = m_children.begin(), c_end = m_children.end();
      for(; c_p != c_end; c_p++)
	{
	  c_temp = (*c_p)->internal_results(indent_number + 1, m_longest_child_name);
	  c_result.first = c_result.first + c_temp.first + "\n";
	  c_result.second += c_temp.second;
	}
      return c_result;
    }

  private:
    std::vector<ProfileNodeBase *> m_children; 
    unsigned int m_longest_child_name;
  };


  class Profile : public umbotica::stats::RuntimeStat, public ProfileNode
  {
  public:
    template<typename STAT_TRACKER_T>
    Profile(std::string category, std::string name, STAT_TRACKER_T * statTrack) :
      RuntimeStat(category, name, statTrack),
      ProfileNode(name)
    { }
    
    std::string results() const
    {
      std::ostringstream out;
      std::pair<std::string, pt::time_duration> result = collect_child_output(0);
      out << result.second << "\n";
      return  out.str() + result.first;
    };
    
  };


 
  // forward declaration.
  class ScopedProfiler;
   
  template<typename MUTEX_T = umbotica::threads::null_mutex>
  class ProfileNodeLeaf : public ProfileNodeBase
  {
  public:
    typedef MUTEX_T mutex_t;
    typedef typename mutex_t::scoped_lock lock_t;
 
    ProfileNodeLeaf(std::string name, ProfileNode * parent = NULL) : ProfileNodeBase(name), m_elapsed()
    { 
      if(parent)
	parent->add_child(this);
    }

    pt::time_duration total()
    {
      lock_t lock(m_mutex);
      return m_elapsed;
    }

    friend class ScopedProfiler;
  protected:
    std::pair<std::string, pt::time_duration> internal_results(unsigned int indent_number, unsigned int longest_sibling_name) const
    {
      lock_t lock(m_mutex);
      // produce my output...
      std::ostringstream out; 
      if(indent_number > 0)
	out << std::setfill('\t') << std::setw(indent_number) << '\t';
      
      out << std::setiosflags(std::ios::left) << std::setfill(' ') 
	  << std::setw(longest_sibling_name + 2) << (name() + ": ") << m_elapsed;
      
                  
      return std::make_pair(out.str(),m_elapsed);
    }


  private:
    void add_time_elapsed(pt::time_duration const & time)
    {
      lock_t lock(m_mutex);
      m_elapsed += time;
    }

    mutable mutex_t m_mutex;
    pt::time_duration m_elapsed;

  };


  class ScopedProfiler
  {
  public:
    template<typename MUTEX_T>
    ScopedProfiler(ProfileNodeLeaf<MUTEX_T> & profiler) : 
      m_started(pt::microsec_clock::local_time()), 
      m_add_elapsed (boost::bind(&ProfileNodeLeaf<MUTEX_T>::add_time_elapsed,&profiler,_1)),
      m_did_exit(false)
    { }
    
    ~ScopedProfiler(){exit();}

    void exit()
    {
      if(!m_did_exit)
	{
	  m_add_elapsed(pt::microsec_clock::local_time() - m_started);
	  m_did_exit = true;
	}
    }

    pt::ptime m_started;
    boost::function<void(pt::time_duration)> m_add_elapsed;
    bool m_did_exit;
  };

#endif //#ifdef UMBOTICA_NO_STATISTICS

}} // namespace umbotica::stats

#endif 
