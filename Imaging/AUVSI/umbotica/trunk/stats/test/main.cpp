#include <iostream>
//#define UMBOTICA_NO_STATISTICS

#include <umbotica/stats/runtime_stat_tracker.hpp>
#include <umbotica/stats/percentage_stat.hpp>
#include <umbotica/stats/sum_stat.hpp>
#include <umbotica/stats/profile_stat.hpp>

using namespace umbotica::stats;



RuntimeStatTracker<> theTracker;
Profile the_time("Time", "Total Time", &theTracker);
ProfileNode init("initialization",&the_time);
ProfileNode runtime("runtime",&the_time);
ProfileNode recursion("recursion",&the_time);
ProfileNodeLeaf<> rec_time("inner recursion",&recursion);
ProfileNodeLeaf<> rec_out("outer recursion",&recursion);
ProfileNodeLeaf<> b_time("breakfast",&init);
ProfileNodeLeaf<> l_time("lunch",&init);
ProfileNodeLeaf<> m_time("meals",&init);
ProfileNodeLeaf<> br_time("breakfast",&runtime);
ProfileNodeLeaf<> lr_time("lunch",&runtime);
ProfileNodeLeaf<> mr_time("meals",&runtime);


void wait(unsigned int n)
{
  double i = n;
  for(unsigned int j = 0; j < n; j++)
    {
      int k = (int)i;
      k += 1;
      i += (double)k * 1.1;
    }
}

void recursive_test(int i)
{
  ScopedProfiler c(rec_time);
  if(i > 0)
    {
      wait(i);
      recursive_test(i - 1);
    }
}

int main(int argc, char ** argv)
{
  std::cout << "Hello stats!" << std::endl;


  ScopedProfiler a(b_time);
  Percentage<> breakfast("Food", "Breakfast", &theTracker);
  a.exit();
  
  ScopedProfiler b(l_time);
  Percentage<> lunch;
  b.exit();
 
  ScopedProfiler c(m_time);
  Sum<double> meals("Food", "Total Meals", &theTracker);
  c.exit();
  

  
  theTracker.add_stat("Food", "Lunch",&lunch);

  ScopedProfiler d(br_time);
  breakfast["Cereal"] += 200;
  breakfast["Coffee"] += 20; 
  d.exit();
  ScopedProfiler f(mr_time);
  meals += 1;
  f.exit();

  ScopedProfiler e(lr_time);
  lunch["Roti"] += 4030;
  lunch["Cookies"] += 30;
  lunch["Water"];
  lunch.insert("Coffee");
  e.exit();
  ScopedProfiler g(mr_time);
  meals += 1.5;
  g.exit();

  ScopedProfiler h(rec_out);
  recursive_test(10);
  h.exit();
  

  std::cout << theTracker;
  
}

