#include "boost/filesystem/operations.hpp" // includes boost/filesystem/path.hpp
#include "boost/filesystem/fstream.hpp"    // ditto

#include <iostream>
#include <fstream>
#include <boost/lexical_cast.hpp>
#include <cv.h>
#include <string>
#include <vector>
#include <tostring.hpp>

namespace fs = boost::filesystem;

std::ostream & operator<<(std::ostream & out, CvPoint3D32f & pt)
{
  out << umbotica::tuple(pt.x,pt.y,pt.z);
  return out;
}

std::ostream & operator<<(std::ostream & out, CvPoint2D32f & pt)
{
  out << umbotica::tuple(pt.x,pt.y);
  return out;
}


std::istream & operator>>(std::istream & in, CvPoint3D32f & pt)
{
  in >> pt.x >> pt.y >> pt.z;
  return in;
}

std::istream & operator>>(std::istream & in, CvPoint2D32f & pt)
{
  in >> pt.x >> pt.y;
  return in;
}


int main(int argc, char**argv)
{
  fs::directory_iterator p;//iteratively load image files from the directory
  fs::directory_iterator endIterator;//points to past-the-end of the images, used to tell when we got the last one
  fs::path imageDir;//path to the directory holding the files
  std::vector<int> numPoints;
  std::vector<CvPoint2D32f> points;
  std::vector<CvPoint3D32f> objectPts;

  if(argc != 2)
    {
      std::cout << "Usage is \"ergocv directory\"\n";
      exit(1);
    }
  
  imageDir = argv[1];
  if(!fs::exists(imageDir))
    {
      std::cout << "Directory " << argv[1] << "does not exist\n";
      std::cout << "Usage is \"ergocv directory\"\n";
      exit(1);
    }


  p = fs::directory_iterator(imageDir);

  try
    {
      // for each file in the directory
      for( ; p != endIterator; p++)
	{
	  std::cout << "Processing file \"" << p->leaf() << "\"\n"; 
	  if(p->leaf().size() == 0 || p->leaf()[0] == '.')
	    {
	      std::cout << "Invalid filename (probably .svn). Skipping file...\n";
	      continue;
	    }
	  std::ifstream in;
	  in.open(p->native_directory_string().c_str());
	  int idx = numPoints.size();
	  numPoints.push_back(0);
	  CvPoint2D32f p2d;
	  CvPoint3D32f p3d;
	  //	  std::string discard;
	  //	  in >> discard;

	  while(!in.eof())
	    {
	      in >> p3d;
	      in >> p2d;
	      numPoints[idx]++;
	      std::cout << "Point[" << numPoints[idx] << "] " << p2d << " ==> " << p3d << "\n";
	      points.push_back(p2d);
	      objectPts.push_back(p3d);
	    }
	  if(numPoints[idx] == 0)
		  numPoints.pop_back();
	}
    }
  catch(std::exception const & e)
    {
      std::cout << "Error parsing file: " << p->native_directory_string() << ": " << e.what() << "\n";
    }


  // now move everything to fixed sized arrays and calculate the caliberation:
  //    std::vector<int> numPoints;
  int numpts[numPoints.size()];
  //  std::vector<CvPoint2D32f> points;
  CvPoint2D32f pts[points.size()];
  //  std::vector<CvPoint3D32f> objectPts;
  CvPoint3D32f obj[objectPts.size()];

  for(int i = 0; i < numPoints.size(); i++)
    {
      numpts[i] = numPoints[i];
    }

  for(int i = 0; i < points.size(); i++)
    {
      pts[i] = points[i];
    }
  for(int i = 0; i < objectPts.size(); i++)
    {
      obj[i] = objectPts[i];
    }

  float distortion[4];
  float matrix[9];
  CvSize imageSize = {640,480};

  float transVects32f[3 * numPoints.size()];
  float rotMatrs32f[9 * numPoints.size()];


  cvCalibrateCamera( numPoints.size(), (int*)numpts, imageSize,(CvPoint2D32f*)pts, obj,
		     distortion, matrix,
		     transVects32f, rotMatrs32f,0);

  //    std::cout << "Distortion\n";
  std::cout << "\n";
    std::cout << "k1 " << distortion[0] << "\n";
    std::cout << "k2 " << distortion[1] << "\n";
    std::cout << "p1 " << distortion[2] << "\n";
    std::cout << "p2 " << distortion[3] << "\n";

    std::cout << "\n";
    /*    [ fx 0 cx; 0 fy cy; 0 0 1 ] */
//    std::cout << "Matrix\n";
    std::cout << "intrinsic00: " << matrix[0] << "\n";
    std::cout << "intrinsic01 " << matrix[1] << "\n";
    std::cout << "intrinsic02 " << matrix[2] << "\n";

    std::cout << "intrinsic10 " << matrix[3] << "\n";
    std::cout << "intrinsic11 " << matrix[4] << "\n";
    std::cout << "intrinsic12 " << matrix[5] << "\n";
    std::cout << "intrinsic20 " << matrix[6] << "\n";
    std::cout << "intrinsic21 " << matrix[7] << "\n";
    std::cout << "intrinsic22 " << matrix[8] << "\n";

}
