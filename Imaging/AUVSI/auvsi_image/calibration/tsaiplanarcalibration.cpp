// $Id: tsaiplanarcalibration.cpp,v 1.1.1.1.2.10 2005/02/01 19:40:00 cvs Exp $

using namespace std;

#include <fstream>
#include "calibration.h"
#include "tsaiplanarcalibration.h"
#include <math.h>
#include <stdlib.h>
#include <errno.h>
#include "matrix/matrix.h"
#include "controlpoints.h"
#include <iostream>
#include "minpack/lmdif.h"

class TsaiPlanarCalibration * TsaiPlanarCalibration::lmdif_this = 0;

TsaiPlanarCalibration::TsaiPlanarCalibration (int width, int height):
  Calibration()
{
  this->camera = Camera( width, height );
}

TsaiPlanarCalibration::TsaiPlanarCalibration (Camera const camera, string pointsFilename ):
  Calibration ()
{
  this->camera = camera;
  loadCalibrationPoints( pointsFilename );
  recalibrate( );
}

TsaiPlanarCalibration::TsaiPlanarCalibration (Camera init_camera):
  Calibration ()
{
  this->camera = init_camera;

}


TsaiPlanarCalibration::TsaiPlanarCalibration( string cameraFilename, string calibrationPointsFilename ) : Calibration()
{
  cout << "TsaiPlanarCalibration::TsaiPlanarCalibration( " << cameraFilename << "," << calibrationPointsFilename << ")" << ", Ncx " << camera.Ncx << ", Nfx " << camera.Nfx << ", dpx " << camera.dpx << ", dpy " << camera.dpy << ", Sx " << camera.sx << endl;

  this->camera = Camera( cameraFilename );

  cout << "camera info: Ncx " << camera.Ncx << ", Nfx " << camera.Nfx << ", dpx " << camera.dpx << ", dpy " << camera.dpy << ", Sx " << camera.sx << endl;

  loadCalibrationPoints( calibrationPointsFilename );
  recalibrate( );
}

double TsaiPlanarCalibration::getTx(){return cc.Tx;};			/* [mm]          */
double TsaiPlanarCalibration::getTy(){return cc.Ty;};			/* [mm]          */
double TsaiPlanarCalibration::getTz(){return cc.Tz;};			/* [mm]          */
double TsaiPlanarCalibration::getRx(){return cc.Rx;};			/* [rad]         */
double TsaiPlanarCalibration::getRy(){return cc.Ry;};			/* [rad]         */
double TsaiPlanarCalibration::getRz(){return cc.Rz;};

void
TsaiPlanarCalibration::loadCalibrationData (string filename)
{
  ifstream fin (filename.c_str ());

#ifdef DEBUG
  cout << "TsaiPlanarCalibration::loadCalibrationData called" << endl;
#endif

  if (!fin)
    {
      cerr << "TsaiPlanarCalibration::loadCalibrationData " << filename <<
	" failed\n";
      exit (1);
    }

  fin >> camera.Ncx >> camera.Nfx >> camera.dx >> camera.dy >> camera.
    dpx >> camera.dpy;
  fin >> camera.Cx >> camera.Cy >> camera.sx;

  fin >> cc.f >> cc.kappa1;
  fin >> cc.Tx >> cc.Ty >> cc.Tz;
  fin >> cc.Rx >> cc.Ry >> cc.Rz;

  applyRPYTransform ();

  fin >> cc.p1 >> cc.p2;
  generateCalibrationData ();
}

void
TsaiPlanarCalibration::cameraCoordToWorldCoord (double xc, double yc,
						double zc, double *xw,
						double *yw, double *zw)
{
  double common_denominator;

  /* these equations were found by simply inverting the previous routine using Macsyma */

  common_denominator = ((cc.r1 * cc.r5 - cc.r2 * cc.r4) * cc.r9 +
			(cc.r3 * cc.r4 - cc.r1 * cc.r6) * cc.r8 +
			(cc.r2 * cc.r6 - cc.r3 * cc.r5) * cc.r7);

  *xw = ((cc.r2 * cc.r6 - cc.r3 * cc.r5) * zc +
	 (cc.r3 * cc.r8 - cc.r2 * cc.r9) * yc +
	 (cc.r5 * cc.r9 - cc.r6 * cc.r8) * xc +
	 (cc.r3 * cc.r5 - cc.r2 * cc.r6) * cc.Tz +
	 (cc.r2 * cc.r9 - cc.r3 * cc.r8) * cc.Ty +
	 (cc.r6 * cc.r8 - cc.r5 * cc.r9) * cc.Tx) / common_denominator;

  *yw = -((cc.r1 * cc.r6 - cc.r3 * cc.r4) * zc +
	  (cc.r3 * cc.r7 - cc.r1 * cc.r9) * yc +
	  (cc.r4 * cc.r9 - cc.r6 * cc.r7) * xc +
	  (cc.r3 * cc.r4 - cc.r1 * cc.r6) * cc.Tz +
	  (cc.r1 * cc.r9 - cc.r3 * cc.r7) * cc.Ty +
	  (cc.r6 * cc.r7 - cc.r4 * cc.r9) * cc.Tx) / common_denominator;

  *zw = ((cc.r1 * cc.r5 - cc.r2 * cc.r4) * zc +
	 (cc.r2 * cc.r7 - cc.r1 * cc.r8) * yc +
	 (cc.r4 * cc.r8 - cc.r5 * cc.r7) * xc +
	 (cc.r2 * cc.r4 - cc.r1 * cc.r5) * cc.Tz +
	 (cc.r1 * cc.r8 - cc.r2 * cc.r7) * cc.Ty +
	 (cc.r5 * cc.r7 - cc.r4 * cc.r8) * cc.Tx) / common_denominator;
}

void
TsaiPlanarCalibration::worldCoordToCameraCoord (double xw, double yw, double zw, double *xc, double *yc, double *zc)
{
  *xc = cc.r1 * xw + cc.r2 * yw + cc.r3 * zw + cc.Tx;
  *yc = cc.r4 * xw + cc.r5 * yw + cc.r6 * zw + cc.Ty;
  *zc = cc.r7 * xw + cc.r8 * yw + cc.r9 * zw + cc.Tz;
}

void
TsaiPlanarCalibration::imageCoordToCameraCoord (double const Xfd, double const Yfd, double const zc, double * const xc, double * const yc)
{
  double Xd, Yd, Xu, Yu;

  /* convert from image to distorted sensor coordinates */
  Xd = camera.dpx * (Xfd - camera.Cx) / camera.sx;
  Yd = camera.dpy * (Yfd - camera.Cy);

  /* convert from distorted sensor to undistorted sensor plane coordinates */
  distortedToUndistortedSensorCoord (Xd, Yd, &Xu, &Yu);

  /* Convert to camera coordinates (simply the inverse operation to 
     finding the image coordinates, however we must specify z */

  *xc = Xu * zc / cc.f;
  *yc = Yu * zc / cc.f;

};

void
TsaiPlanarCalibration::imageCoordToWorldCoord (double const Xfd, double const Yfd, double const zw, double * const xw, double * const yw)
{
  double Xd, Yd, Xu, Yu, common_denominator;

  /* convert from image to distorted sensor coordinates */
  Xd = camera.dpx * (Xfd - camera.Cx) / camera.sx;
  Yd = camera.dpy * (Yfd - camera.Cy);

  /* convert from distorted sensor to undistorted sensor plane coordinates */
  distortedToUndistortedSensorCoord (Xd, Yd, &Xu, &Yu);

  /* calculate the corresponding xw and yw world coordinates   */
  /* (these equations were derived by simply inverting         */
  /* the perspective projection equations using Macsyma)       */
  common_denominator = ((cc.r1 * cc.r8 - cc.r2 * cc.r7) * Yu +
			(cc.r5 * cc.r7 - cc.r4 * cc.r8) * Xu -
			cc.f * cc.r1 * cc.r5 + cc.f * cc.r2 * cc.r4);

  *xw = (((cc.r2 * cc.r9 - cc.r3 * cc.r8) * Yu +
	  (cc.r6 * cc.r8 - cc.r5 * cc.r9) * Xu -
	  cc.f * cc.r2 * cc.r6 + cc.f * cc.r3 * cc.r5) * zw +
	 (cc.r2 * cc.Tz - cc.r8 * cc.Tx) * Yu +
	 (cc.r8 * cc.Ty - cc.r5 * cc.Tz) * Xu -
	 cc.f * cc.r2 * cc.Ty + cc.f * cc.r5 * cc.Tx) / common_denominator;

  *yw = -(((cc.r1 * cc.r9 - cc.r3 * cc.r7) * Yu +
	   (cc.r6 * cc.r7 - cc.r4 * cc.r9) * Xu -
	   cc.f * cc.r1 * cc.r6 + cc.f * cc.r3 * cc.r4) * zw +
	  (cc.r1 * cc.Tz - cc.r7 * cc.Tx) * Yu +
	  (cc.r7 * cc.Ty - cc.r4 * cc.Tz) * Xu -
	  cc.f * cc.r1 * cc.Ty + cc.f * cc.r4 * cc.Tx) / common_denominator;
}

// This returns a vector that points from the focal point of the
// camera at time of calibration to the point on the carpet that it
// was matched to at time of calibration.
void
TsaiPlanarCalibration::imageCoordToCarpetVector (double xi, double yi, double *cx, double *cy, double *cz)
{
  double xw;
  double yw;
  double zw = 0;
  imageCoordToWorldCoord( xi, yi, 0, &xw, &yw);

  *cx = cc.r1 * xw + cc.r2 * yw + cc.r3 * zw + cc.Tx;
  *cy = cc.r4 * xw + cc.r5 * yw + cc.r6 * zw + cc.Ty;
  *cz = cc.r7 * xw + cc.r8 * yw + cc.r9 * zw + cc.Tz;

  return;
}

void
TsaiPlanarCalibration::worldCoordToImageCoord (double xw, double yw,
					       double zw, double *Xf,
					       double *Yf)
{
  double xc, yc, zc, Xu, Yu, Xd, Yd;

  /* convert from world coordinates to camera coordinates */
  xc = cc.r1 * xw + cc.r2 * yw + cc.r3 * zw + cc.Tx;
  yc = cc.r4 * xw + cc.r5 * yw + cc.r6 * zw + cc.Ty;
  zc = cc.r7 * xw + cc.r8 * yw + cc.r9 * zw + cc.Tz;

  /* convert from camera coordinates to undistorted sensor plane coordinates */
  Xu = cc.f * xc / zc;
  Yu = cc.f * yc / zc;

  /* convert from undistorted to distorted sensor plane coordinates */
  undistortedToDistortedSensorCoord (Xu, Yu, &Xd, &Yd);

  /* convert from distorted sensor plane coordinates to image coordinates */
  *Xf = Xd * camera.sx / camera.dpx + camera.Cx;
  *Yf = Yd / camera.dpy + camera.Cy;
}

void
TsaiPlanarCalibration::distortedToUndistortedImageCoord (double Xfd,
							 double Yfd,
							 double *Xfu,
							 double *Yfu)
{
  double Xd, Yd, Xu, Yu;

  /* convert from image to sensor coordinates */
  Xd = camera.dpx * (Xfd - camera.Cx) / camera.sx;
  Yd = camera.dpy * (Yfd - camera.Cy);

  /* convert from distorted sensor to undistorted sensor plane coordinates */
  distortedToUndistortedSensorCoord (Xd, Yd, &Xu, &Yu);

  /* convert from sensor to image coordinates */
  *Xfu = Xu * camera.sx / camera.dpx + camera.Cx;
  *Yfu = Yu / camera.dpy + camera.Cy;
}

void
TsaiPlanarCalibration::undistortedToDistortedImageCoord (double Xfu,
							 double Yfu,
							 double *Xfd,
							 double *Yfd)
{
  double Xu, Yu, Xd, Yd;

  /* convert from image to sensor coordinates */
  Xu = camera.dpx * (Xfu - camera.Cx) / camera.sx;
  Yu = camera.dpy * (Yfu - camera.Cy);

  /* convert from undistorted sensor to distorted sensor plane coordinates */
  undistortedToDistortedSensorCoord (Xu, Yu, &Xd, &Yd);

  /* convert from sensor to image coordinates */
  *Xfd = Xd * camera.sx / camera.dpx + camera.Cx;
  *Yfd = Yd / camera.dpy + camera.Cy;
}

void
TsaiPlanarCalibration::distortedToUndistortedSensorCoord (double Xd,
							  double Yd,
							  double *Xu,
							  double *Yu)
{
  double distortion_factor;

  /* convert from distorted to undistorted sensor plane coordinates */
  distortion_factor = 1 + cc.kappa1 * (SQR (Xd) + SQR (Yd));
  *Xu = Xd * distortion_factor;
  *Yu = Yd * distortion_factor;
}


void
TsaiPlanarCalibration::undistortedToDistortedSensorCoord (double Xu,
							  double Yu,
							  double *Xd,
							  double *Yd)
{
  const double SQRT3 = 1.732050807568877293527446341505872366943;

  double Ru, Rd, lambda, c, d, Q, R, D, S, T, sinT, cosT;

  if (((Xu == 0) && (Yu == 0)) || (cc.kappa1 == 0))
    {
      *Xd = Xu;
      *Yd = Yu;
      return;
    }

  Ru = hypot (Xu, Yu);		/* SQRT(Xu*Xu+Yu*Yu) */

  c = 1 / cc.kappa1;
  d = -c * Ru;

  Q = c / 3;
  R = -d / 2;
  D = CUB (Q) + SQR (R);

  if (D >= 0)
    {				/* one real root */
      D = SQRT (D);
      S = CBRT (R + D);
      T = CBRT (R - D);
      Rd = S + T;

      if (Rd < 0)
	{
	  Rd = SQRT (-1 / (3 * cc.kappa1));
	  cerr <<
	    "\nWarning: undistorted image point to distorted image point mapping limited by\n";
	  cerr << "         maximum barrel distortion radius of " << Rd <<
	    "\n";
	  cerr << "         (Xu = " << Xu << ", Yu = " << Yu << ") -> (Xd = "
	    << Xu * Rd / Ru << ", Yd = " << Yu * Rd / Ru << "\n\n";
	}
    }
  else
    {				/* three real roots */
      D = SQRT (-D);
      S = CBRT (hypot (R, D));
      T = atan2 (D, R) / 3;
      SINCOS (T, sinT, cosT);

      /* the larger positive root is    2*S*cos(T)                   */
      /* the smaller positive root is   -S*cos(T) + SQRT(3)*S*sin(T) */
      /* the negative root is           -S*cos(T) - SQRT(3)*S*sin(T) */

      Rd = -S * cosT + SQRT3 * S * sinT;	/* use the smaller positive root */
    }

  lambda = Rd / Ru;

  *Xd = Xu * lambda;
  *Yd = Yu * lambda;
}

/***********************************************************************\
* Routines for coplanar camera calibration	 			*
* Taken from Reg Wilson's stuff. We assimilated his source here         *
* for the videoserver. Jacky Baltes <j.baltes@auckland.ac.nz>           *
\***********************************************************************/
void
TsaiPlanarCalibration::ccComputeXdYdAndRSquared (void)
{
  double Xd_, Yd_;

#ifdef XX_DEBUG
  cout << "TsaiPlanarCalibration::ccComputeXdYdAndRSquared( void )" << 
    ", camera.dpx " << camera.dpx << ", camera.dpy " << camera.dpy << ", camera.sx " << camera.sx << endl;
#endif

  for (int i = 0; i < points.size (); i++)
    {
      struct ControlPoint p = points.getPoint (i);
      Xd[i] = Xd_ = camera.dpx * (p.xm - camera.Cx) / camera.sx;	/* [mm] */
      Yd[i] = Yd_ = camera.dpy * (p.ym - camera.Cy);	/* [mm] */
      r_squared[i] = SQR (Xd_) + SQR (Yd_);	/* [mm^2] */
#ifdef XX_DEBUG
      cout << "TsaiPlanarCalibration::ccComputeXdYdAndRSquared( void )" << ", Xd[ " << i << "]=" << Xd[i] << ", Yd[" << i << "]=" << Yd[i] << endl;
#endif     
    }
}


void
TsaiPlanarCalibration::ccComputeU (void)
{
  dmat M, a, b;

  M = newdmat( 0, points.size () - 1, 0, 4, &errno );
  if (errno)
    {
      cerr << "cc compute U: unable to allocate matrix M\n";
      exit (-1);
    }

  a = newdmat( 0, 4, 0, 0, &errno );
  if (errno)
    {
      cerr << "cc compute U: unable to allocate vector a\n";
      exit (-1);
    }

  b = newdmat( 0, points.size () - 1, 0, 0, &errno );
  if (errno)
    {
      cerr << "cc compute U: unable to allocate vector b\n";
      exit (-1);
    }

  for (int i = 0; i < points.size (); i++)
    {
      struct ControlPoint p = points.getPoint (i);
      M.el[i][0] = Yd[i] * p.xw;
      M.el[i][1] = Yd[i] * p.yw;
      M.el[i][2] = Yd[i];
      M.el[i][3] = -Xd[i] * p.xw;
      M.el[i][4] = -Xd[i] * p.yw;
      b.el[i][0] = Xd[i];
    }

  if (solve_system (M, a, b))
    {
      cerr << "cc compute U: unable to solve system  Ma=b\n";
      //      exit (-1);
    }
  else
    {
      U[0] = a.el[0][0];
      U[1] = a.el[1][0];
      U[2] = a.el[2][0];
      U[3] = a.el[3][0];
      U[4] = a.el[4][0];
    }

  freemat (M);
  freemat (a);
  freemat (b);
}


void
TsaiPlanarCalibration::ccComputeTxAndTy (void)
{
  int i, far_point;

  double Tx,
    Ty,
    Ty_squared,
    x, y, Sr, r1p, r2p, r4p, r5p, r1, r2, r4, r5, distance, far_distance;

  r1p = U[0];
  r2p = U[1];
  r4p = U[3];
  r5p = U[4];

  /* first find the square of the magnitude of Ty */
  if ((fabs (r1p) < EPSILON) && (fabs (r2p) < EPSILON))
    Ty_squared = 1 / (SQR (r4p) + SQR (r5p));
  else if ((fabs (r4p) < EPSILON) && (fabs (r5p) < EPSILON))
    Ty_squared = 1 / (SQR (r1p) + SQR (r2p));
  else if ((fabs (r1p) < EPSILON) && (fabs (r4p) < EPSILON))
    Ty_squared = 1 / (SQR (r2p) + SQR (r5p));
  else if ((fabs (r2p) < EPSILON) && (fabs (r5p) < EPSILON))
    Ty_squared = 1 / (SQR (r1p) + SQR (r4p));
  else
    {
      Sr = SQR (r1p) + SQR (r2p) + SQR (r4p) + SQR (r5p);
      Ty_squared = (Sr - sqrt (SQR (Sr) - 4 * SQR (r1p * r5p - r4p * r2p))) /
	(2 * SQR (r1p * r5p - r4p * r2p));
    }

  /* find a point that is far from the image center */
  far_distance = 0;
  far_point = 0;
  for (i = 0; i < points.size (); i++)
    if ((distance = r_squared[i]) > far_distance)
      {
	far_point = i;
	far_distance = distance;
      }

  /* now find the sign for Ty */
  /* start by assuming Ty > 0 */
  Ty = sqrt (Ty_squared);
  r1 = U[0] * Ty;
  r2 = U[1] * Ty;
  Tx = U[2] * Ty;
  r4 = U[3] * Ty;
  r5 = U[4] * Ty;

  struct ControlPoint p = points.getPoint (far_point);
  x = r1 * p.xw + r2 * p.yw + Tx;
  y = r4 * p.xw + r5 * p.yw + Ty;

  /* flip Ty if we guessed wrong */
  if ((SIGNBIT (x) != SIGNBIT (Xd[far_point])) ||
      (SIGNBIT (y) != SIGNBIT (Yd[far_point])))
    Ty = -Ty;

  /* update the calibration constants */
  cc.Tx = U[2] * Ty;
  cc.Ty = Ty;
}


void
TsaiPlanarCalibration::ccComputeR (void)
{
  double r1, r2, r3, r4, r5, r6, r7, r8, r9;

  r1 = U[0] * cc.Ty;
  r2 = U[1] * cc.Ty;
  r3 = sqrt (1 - SQR (r1) - SQR (r2));

  r4 = U[3] * cc.Ty;
  r5 = U[4] * cc.Ty;
  r6 = sqrt (1 - SQR (r4) - SQR (r5));
  if (!SIGNBIT (r1 * r4 + r2 * r5))
    r6 = -r6;

  /* use the outer product of the first two rows to get the last row */
  r7 = r2 * r6 - r3 * r5;
  r8 = r3 * r4 - r1 * r6;
  r9 = r1 * r5 - r2 * r4;

  /* update the calibration constants */
  cc.r1 = r1;
  cc.r2 = r2;
  cc.r3 = r3;
  cc.r4 = r4;
  cc.r5 = r5;
  cc.r6 = r6;
  cc.r7 = r7;
  cc.r8 = r8;
  cc.r9 = r9;

  /* fill in cc.Rx, cc.Ry and cc.Rz */
  solveRPYTransform ();
}


void
TsaiPlanarCalibration::ccComputeApproximateFAndTz (void)
{
  int i;

  dmat M, a, b;

  M = newdmat (0, points.size () - 1, 0, 1, &errno);
  if (errno)
    {
      cerr << "cc compute apx: unable to allocate matrix M\n";
      exit (-1);
    }

  a = newdmat (0, 1, 0, 0, &errno);
  if (errno)
    {
      cerr << "cc compute apx: unable to allocate vector a\n";
      exit (-1);
    }

  b = newdmat (0, points.size () - 1, 0, 0, &errno);
  if (errno)
    {
      cerr << "cc compute apx: unable to allocate vector b\n";
      exit (-1);
    }

  for (i = 0; i < points.size (); i++)
    {
      struct ControlPoint p = points.getPoint (i);
      M.el[i][0] = cc.r4 * p.xw + cc.r5 * p.yw + cc.Ty;
      M.el[i][1] = -Yd[i];
      b.el[i][0] = (cc.r7 * p.xw + cc.r8 * p.yw) * Yd[i];
    }

  if (solve_system (M, a, b))
    {
      cerr << "cc compute apx: unable to solve system  Ma=b\n";
      //      exit (-1);
      return;
    }

  /* update the calibration constants */
  cc.f = a.el[0][0];
  cc.Tz = a.el[1][0];
  cc.kappa1 = 0.0;		/* this is the assumption that our calculation was made under */

  freemat (M);
  freemat (a);
  freemat (b);
}


/************************************************************************/
int
TsaiPlanarCalibration::ccComputeExactFAndTzError (int *m_ptr, int *n_ptr, double *params, double *err)
/* pointer to number of points to fit */
/* pointer to number of parameters */
/* vector of parameters */
/* vector of error from data */
{
  double f, Tz, kappa1, xc, yc, zc, Xu_1, Yu_1, Xu_2, Yu_2, distortion_factor;

  f = params[0];
  Tz = params[1];
  kappa1 = params[2];

  for (int i = 0; i < points.size(); i++)
    {
      struct ControlPoint p = points.getPoint (i);
      /* convert from world coordinates to camera coordinates */
      /* Note: zw is implicitly assumed to be zero for these (coplanar) calculations */
      xc = cc.r1 * p.xw + cc.r2 * p.yw + cc.Tx;
      yc = cc.r4 * p.xw + cc.r5 * p.yw + cc.Ty;
      zc = cc.r7 * p.xw + cc.r8 * p.yw + Tz;

      /* convert from camera coordinates to undistorted sensor coordinates */
      Xu_1 = f * xc / zc;
      Yu_1 = f * yc / zc;

      /* convert from distorted sensor coordinates to undistorted sensor coordinates */
      distortion_factor = 1 + kappa1 * (SQR (Xd[i]) + SQR (Yd[i]));
      Xu_2 = Xd[i] * distortion_factor;
      Yu_2 = Yd[i] * distortion_factor;

      /* record the error in the undistorted sensor coordinates */
      err[i] = hypot (Xu_1 - Xu_2, Yu_1 - Yu_2);
    }
  return 0;
}


void
TsaiPlanarCalibration::ccComputeExactFAndTz (void)
{
  int const NPARAMS = 3;

  /* Parameters needed by MINPACK's lmdif() */

  int m = points.size ();
  int n = NPARAMS;
  double x[NPARAMS];
  double *fvec;
  double ftol = REL_SENSOR_TOLERANCE_ftol;
  double xtol = REL_PARAM_TOLERANCE_xtol;
  double gtol = ORTHO_TOLERANCE_gtol;
  int maxfev = MAXFEV;
  double epsfcn = EPSFCN;
  double diag[NPARAMS];
  int mode = MODE;
  double factor = FACTOR;
  int nprint = 0;
  int info;
  int nfev;
  double *fjac;
  int ldfjac = m;
  int ipvt[NPARAMS];
  double qtf[NPARAMS];
  double wa1[NPARAMS];
  double wa2[NPARAMS];
  double wa3[NPARAMS];
  double *wa4;

  /* allocate some workspace */
  if ((fvec =
       (double *) calloc ((unsigned int) m,
			  (unsigned int) sizeof (double))) == NULL)
    {
      cerr << "calloc: Cannot allocate workspace fvec\n";
      exit (-1);
    }

  if ((fjac =
       (double *) calloc ((unsigned int) m * n,
			  (unsigned int) sizeof (double))) == NULL)
    {
      cerr << "calloc: Cannot allocate workspace fjac\n";
      exit (-1);
    }

  if ((wa4 =
       (double *) calloc ((unsigned int) m,
			  (unsigned int) sizeof (double))) == NULL)
    {
      cerr << "calloc: Cannot allocate workspace wa4\n";
      exit (-1);
    }

  /* use the current calibration constants as an initial guess */
  x[0] = cc.f;
  x[1] = cc.Tz;
  x[2] = cc.kappa1;

  /* define optional scale factors for the parameters */
  if (mode == 2)
    {
      for (int i = 0; i < NPARAMS; i++)
	diag[i] = 1.0;		/* some user-defined values */
    }

  //  std::cout << "TsaiPlanarCalibration::ccComputeExactFAndTz &m=" << &m << ",&n="<< &n << '\n';

  if ( lmdif_this == 0 )
    {
      lmdif_this = this;
    }
  else
    {
      std::cerr << "Mutex on lmdif_this failed!\n";
      exit( 23 );
    }
  /* perform the optimization */
  lmdif_( (S_fp) &TsaiPlanarCalibration::wrapperCcComputeExactFAndTzError,
	  (integer *)&m, (integer *)&n, (doublereal *)x, (doublereal *)fvec, 
	  (doublereal *)&ftol, (doublereal *)&xtol, 
	  (doublereal *)&gtol, (integer *)&maxfev, (doublereal *)&epsfcn,
	  (doublereal *)diag, (integer *)&mode, (doublereal *)&factor, 
	  (integer *)&nprint, (integer *)&info, (integer *)&nfev, 
	  (doublereal *)fjac, (integer *)&ldfjac,
	  (integer *)ipvt, (doublereal *)qtf, (doublereal *)wa1, 
	  (doublereal *)wa2, (doublereal *)wa3, (doublereal *)wa4);

  lmdif_this = 0;

  /* update the calibration constants */
  cc.f = x[0];
  cc.Tz = x[1];
  cc.kappa1 = x[2];

  //  std::cout << "TsaiPlanarCalibration::ccThreeParmOptimization Tz=" << cc.Tz << " f=" << cc.f << " kappa1=" << cc.kappa1 << '\n';

  /* release allocated workspace */
  free (fvec);
  free (fjac);
  free (wa4);

#ifdef DEBUG
  /* print the number of function calls during iteration */
  cerr << "info: " << info << ",nfev: " << nfev << "\n\n";
#endif
}


/************************************************************************/
void
TsaiPlanarCalibration::ccThreeParmOptimization (void)
{
  for (int i = 0; i < points.size (); i++)
    {
      struct ControlPoint p = points.getPoint (i);

      if (p.zw)
	{
	  cerr <<
	    "error - coplanar calibration tried with data outside of Z plane\n\n";
	  exit (-1);
	}
    }
  ccComputeXdYdAndRSquared ();

  ccComputeU ();

  ccComputeTxAndTy ();

  ccComputeR ();

  ccComputeApproximateFAndTz ();

  //  std::cout << "TsaiPlanarCalibration::ccThreeParmOptimization Tz=" << cc.Tz << " f=" << cc.f << " kappa1=" << cc.kappa1 << '\n';

  if (cc.f < 0)
    {
      /* try the other solution for the orthonormal matrix */
      cc.r3 = -cc.r3;
      cc.r6 = -cc.r6;
      cc.r7 = -cc.r7;
      cc.r8 = -cc.r8;
      solveRPYTransform ();

      ccComputeApproximateFAndTz ();

      if (cc.f < 0)
	{
	  cerr << "error - possible handedness problem with data\n";
	  exit (-1);
	}
    }

  ccComputeExactFAndTz ();
}


/************************************************************************/
void
TsaiPlanarCalibration::ccRemoveSensorPlaneDistortionFromXdAndYd (void)
{
  double Xu, Yu;

  for (int i = 0; i < points.size (); i++)
    {
      distortedToUndistortedSensorCoord (Xd[i], Yd[i], &Xu, &Yu);
      Xd[i] = Xu;
      Yd[i] = Yu;
      r_squared[i] = SQR (Xu) + SQR (Yu);
    }
}


/************************************************************************/
int
TsaiPlanarCalibration::ccFiveParmOptimizationWithLateDistortionRemovalError (int *m_ptr, int *n_ptr, double *params, double *err)
				/* pointer to number of points to fit */
				/* pointer to number of parameters */
				/* vector of parameters */
				/* vector of error from data */
{
  int i;

  double f, Tz, kappa1, xc, yc, zc, Xu_1, Yu_1, Xu_2, Yu_2, distortion_factor;

  /* in this routine radial lens distortion is only taken into account */
  /* after the rotation and translation constants have been determined */

  f = params[0];
  Tz = params[1];
  kappa1 = params[2];

  camera.Cx = params[3];
  camera.Cy = params[4];

  ccComputeXdYdAndRSquared ();

  ccComputeU ();

  ccComputeTxAndTy ();

  ccComputeR ();

  ccComputeApproximateFAndTz ();

  if (cc.f < 0)
    {
      /* try the other solution for the orthonormal matrix */
      cc.r3 = -cc.r3;
      cc.r6 = -cc.r6;
      cc.r7 = -cc.r7;
      cc.r8 = -cc.r8;
      solveRPYTransform ();

      ccComputeApproximateFAndTz ();

      if (cc.f < 0)
	{
	  cerr << "error - possible handedness problem with data\n";
	  exit (-1);
	}
    }

  for (i = 0; i < points.size (); i++)
    {
      struct ControlPoint p = points.getPoint (i);
      /* convert from world coordinates to camera coordinates */
      /* Note: zw is implicitly assumed to be zero for these (coplanar) calculations */
      xc = cc.r1 * p.xw + cc.r2 * p.yw + cc.Tx;
      yc = cc.r4 * p.xw + cc.r5 * p.yw + cc.Ty;
      zc = cc.r7 * p.xw + cc.r8 * p.yw + Tz;

      /* convert from camera coordinates to undistorted sensor coordinates */
      Xu_1 = f * xc / zc;
      Yu_1 = f * yc / zc;

      /* convert from distorted sensor coordinates to undistorted sensor coordinates */
      distortion_factor = 1 + kappa1 * r_squared[i];
      Xu_2 = Xd[i] * distortion_factor;
      Yu_2 = Yd[i] * distortion_factor;

      /* record the error in the undistorted sensor coordinates */
      err[i] = hypot (Xu_1 - Xu_2, Yu_1 - Yu_2);
    }
  return 0;
}


void
TsaiPlanarCalibration::ccFiveParmOptimizationWithLateDistortionRemoval (void)
{
  const int NPARAMS = 5;

  /* Parameters needed by MINPACK's lmdif() */

  int m = points.size ();
  int n = NPARAMS;
  double x[NPARAMS];
  double *fvec;
  double ftol = REL_SENSOR_TOLERANCE_ftol;
  double xtol = REL_PARAM_TOLERANCE_xtol;
  double gtol = ORTHO_TOLERANCE_gtol;
  int maxfev = MAXFEV;
  double epsfcn = EPSFCN;
  double diag[NPARAMS];
  int mode = MODE;
  double factor = FACTOR;
  int nprint = 0;
  int info;
  int nfev;
  double *fjac;
  int ldfjac = m;
  int ipvt[NPARAMS];
  double qtf[NPARAMS];
  double wa1[NPARAMS];
  double wa2[NPARAMS];
  double wa3[NPARAMS];
  double *wa4;

  /* allocate some workspace */
  if ((fvec =
       (double *) calloc ((unsigned int) m,
			  (unsigned int) sizeof (double))) == NULL)
    {
      cerr << "calloc: Cannot allocate workspace fvec\n";
      exit (-1);
    }

  if ((fjac =
       (double *) calloc ((unsigned int) m * n,
			  (unsigned int) sizeof (double))) == NULL)
    {
      cerr << "calloc: Cannot allocate workspace fjac\n";
      exit (-1);
    }

  if ((wa4 =
       (double *) calloc ((unsigned int) m,
			  (unsigned int) sizeof (double))) == NULL)
    {
      cerr << "calloc: Cannot allocate workspace wa4\n";
      exit (-1);
    }

  /* use the current calibration constants as an initial guess */
  x[0] = cc.f;
  x[1] = cc.Tz;
  x[2] = cc.kappa1;
  x[3] = camera.Cx;
  x[4] = camera.Cy;

  /* define optional scale factors for the parameters */
  if (mode == 2)
    {
      for (int i = 0; i < NPARAMS; i++)
	diag[i] = 1.0;		/* some user-defined values */
    }

  /* perform the optimization */
  if ( lmdif_this == 0 )
    {
      lmdif_this = this;
    }
  else
    {
      std::cerr << "Mutex on lmdif_this failed!\n";
      exit( 23 );
    }
  lmdif_( (S_fp) &TsaiPlanarCalibration::wrapperCcFiveParmOptimizationWithLateDistortionRemovalError,
	  (integer *)&m, (integer *)&n, (doublereal *)x, (doublereal *)fvec, 
	  (doublereal *)&ftol, (doublereal *)&xtol, 
	  (doublereal *)&gtol, (integer *)&maxfev, (doublereal *)&epsfcn,
	  (doublereal *)diag, (integer *)&mode, (doublereal *)&factor, 
	  (integer *)&nprint, (integer *)&info, (integer *)&nfev, 
	  (doublereal *)fjac, (integer *)&ldfjac,
	  (integer *)ipvt, (doublereal *)qtf, (doublereal *)wa1, 
	  (doublereal *)wa2, (doublereal *)wa3, (doublereal *)wa4);

  lmdif_this = 0;

  /* update the calibration and camera constants */
  cc.f = x[0];
  cc.Tz = x[1];
  cc.kappa1 = x[2];
  camera.Cx = x[3];
  camera.Cy = x[4];

  /* release allocated workspace */
  free (fvec);
  free (fjac);
  free (wa4);

#ifdef XXDEBUG
  /* print the number of function calls during iteration */
  cerr << "info: " << info << ",nfev: " << nfev << "\n";
#endif
}


/************************************************************************/
int
TsaiPlanarCalibration::ccFiveParmOptimizationWithEarlyDistortionRemovalError (int *m_ptr, int *n_ptr, double *params, double *err)
				/* pointer to number of points to fit */
				/* pointer to number of parameters */
				/* vector of parameters */
				/* vector of error from data */
{
  double f, Tz, xc, yc, zc, Xu_1, Yu_1, Xu_2, Yu_2;

  /* in this routine radial lens distortion is taken into account */
  /* before the rotation and translation constants are determined */
  /* (this assumes we have the distortion reasonably modelled)    */

  f = params[0];
  Tz = params[1];

  cc.kappa1 = params[2];
  camera.Cx = params[3];
  camera.Cy = params[4];

  ccComputeXdYdAndRSquared ();

  /* remove the sensor distortion before computing the translation and rotation stuff */
  ccRemoveSensorPlaneDistortionFromXdAndYd ();

  ccComputeU ();

  ccComputeTxAndTy ();

  ccComputeR ();

  /* we need to do this just to see if we have to flip the rotation matrix */
  ccComputeApproximateFAndTz ();

  if (cc.f < 0)
    {
      /* try the other solution for the orthonormal matrix */
      cc.r3 = -cc.r3;
      cc.r6 = -cc.r6;
      cc.r7 = -cc.r7;
      cc.r8 = -cc.r8;
      solveRPYTransform ();

      ccComputeApproximateFAndTz ();

      if (cc.f < 0)
	{
	  cerr << "error - possible handedness problem with data\n";
	  exit (-1);
	}
    }

  /* now calculate the squared error assuming zero distortion */
  for (int i = 0; i < points.size (); i++)
    {
      struct ControlPoint p = points.getPoint (i);
      /* convert from world coordinates to camera coordinates */
      /* Note: zw is implicitly assumed to be zero for these (coplanar) calculations */
      xc = cc.r1 * p.xw + cc.r2 * p.yw + cc.Tx;
      yc = cc.r4 * p.xw + cc.r5 * p.yw + cc.Ty;
      zc = cc.r7 * p.xw + cc.r8 * p.yw + Tz;

      /* convert from camera coordinates to undistorted sensor coordinates */
      Xu_1 = f * xc / zc;
      Yu_1 = f * yc / zc;

      /* convert from distorted sensor coordinates to undistorted sensor coordinates  */
      /* (already done, actually)                                                      */
      Xu_2 = Xd[i];
      Yu_2 = Yd[i];

      /* record the error in the undistorted sensor coordinates */
      err[i] = hypot (Xu_1 - Xu_2, Yu_1 - Yu_2);
    }
  return 0;
}


void
TsaiPlanarCalibration::ccFiveParmOptimizationWithEarlyDistortionRemoval (void)
{
  const int NPARAMS = 5;

  /* Parameters needed by MINPACK's lmdif() */

  int m = points.size ();
  int n = NPARAMS;
  double x[NPARAMS];
  double *fvec;
  double ftol = REL_SENSOR_TOLERANCE_ftol;
  double xtol = REL_PARAM_TOLERANCE_xtol;
  double gtol = ORTHO_TOLERANCE_gtol;
  int maxfev = MAXFEV;
  double epsfcn = EPSFCN;
  double diag[NPARAMS];
  int mode = MODE;
  double factor = FACTOR;
  int nprint = 0;
  int info;
  int nfev;
  double *fjac;
  int ldfjac = m;
  int ipvt[NPARAMS];
  double qtf[NPARAMS];
  double wa1[NPARAMS];
  double wa2[NPARAMS];
  double wa3[NPARAMS];
  double *wa4;

  /* allocate some workspace */
  if ((fvec =
       (double *) calloc ((unsigned int) m,
			  (unsigned int) sizeof (double))) == NULL)
    {
      cerr << "calloc: Cannot allocate workspace fvec\n";
      exit (-1);
    }

  if ((fjac =
       (double *) calloc ((unsigned int) m * n,
			  (unsigned int) sizeof (double))) == NULL)
    {
      cerr << "calloc: Cannot allocate workspace fjac\n";
      exit (-1);
    }

  if ((wa4 =
       (double *) calloc ((unsigned int) m,
			  (unsigned int) sizeof (double))) == NULL)
    {
      cerr << "calloc: Cannot allocate workspace wa4\n";
      exit (-1);
    }

  /* use the current calibration and camera constants as a starting point */
  x[0] = cc.f;
  x[1] = cc.Tz;
  x[2] = cc.kappa1;
  x[3] = camera.Cx;
  x[4] = camera.Cy;

  /* define optional scale factors for the parameters */
  if (mode == 2)
    {
      for (int i = 0; i < NPARAMS; i++)
	diag[i] = 1.0;		/* some user-defined values */
    }

  /* perform the optimization */
  if ( lmdif_this == 0 )
    {
      lmdif_this = this;
    }
  else
    {
      std::cerr << "Mutex on lmdif_this failed!\n";
      exit( 23 );
    }

  lmdif_( (S_fp) &TsaiPlanarCalibration::wrapperCcFiveParmOptimizationWithEarlyDistortionRemovalError,
	  (integer *)&m, (integer *)&n, (doublereal *)x, (doublereal *)fvec, 
	  (doublereal *)&ftol, (doublereal *)&xtol, 
	  (doublereal *)&gtol, (integer *)&maxfev, (doublereal *)&epsfcn,
	  (doublereal *)diag, (integer *)&mode, (doublereal *)&factor, 
	  (integer *)&nprint, (integer *)&info, (integer *)&nfev, 
	  (doublereal *)fjac, (integer *)&ldfjac,
	  (integer *)ipvt, (doublereal *)qtf, (doublereal *)wa1, 
	  (doublereal *)wa2, (doublereal *)wa3, (doublereal *)wa4);

  lmdif_this = 0;
  /* update the calibration and camera constants */
  cc.f = x[0];
  cc.Tz = x[1];
  cc.kappa1 = x[2];
  camera.Cx = x[3];
  camera.Cy = x[4];

  /* release allocated workspace */
  free (fvec);
  free (fjac);
  free (wa4);

#ifdef XXDEBUG
  /* print the number of function calls during iteration */
  cerr << "info: " << info << ",nfev: " << nfev << "\n\n";
#endif
}


/************************************************************************/
int
TsaiPlanarCalibration::ccNicOptimizationError (int *m_ptr, int *n_ptr, double *params, double *err)
				/* pointer to number of points to fit */
				/* pointer to number of parameters */
				/* vector of parameters */
				/* vector of error from data */
{
  double xc,
    yc,
    zc,
    Xd_,
    Yd_,
    Xu_1,
    Yu_1,
    Xu_2,
    Yu_2,
    distortion_factor,
    Rx,
    Ry,
    Rz, Tx, Ty, Tz, kappa1, f, r1, r2, r4, r5, r7, r8, sa, sb, sg, ca, cb, cg;

  Rx = params[0];
  Ry = params[1];
  Rz = params[2];
  Tx = params[3];
  Ty = params[4];
  Tz = params[5];
  kappa1 = params[6];
  f = params[7];

  SINCOS (Rx, sa, ca);
  SINCOS (Ry, sb, cb);
  SINCOS (Rz, sg, cg);
  r1 = cb * cg;
  r2 = cg * sa * sb - ca * sg;
  r4 = cb * sg;
  r5 = sa * sb * sg + ca * cg;
  r7 = -sb;
  r8 = cb * sa;

  for (int i = 0; i < points.size (); i++)
    {
      struct ControlPoint p = points.getPoint (i);
      /* convert from world coordinates to camera coordinates */
      /* Note: zw is implicitly assumed to be zero for these (coplanar) calculations */
      xc = r1 * p.xw + r2 * p.yw + Tx;
      yc = r4 * p.xw + r5 * p.yw + Ty;
      zc = r7 * p.xw + r8 * p.yw + Tz;

      /* convert from camera coordinates to undistorted sensor plane coordinates */
      Xu_1 = f * xc / zc;
      Yu_1 = f * yc / zc;

      /* convert from 2D image coordinates to distorted sensor coordinates */
      Xd_ = camera.dpx * (p.xm - camera.Cx) / camera.sx;
      Yd_ = camera.dpy * (p.ym - camera.Cy);

      /* convert from distorted sensor coordinates to undistorted sensor plane coordinates */
      distortion_factor = 1 + kappa1 * (SQR (Xd_) + SQR (Yd_));
      Xu_2 = Xd_ * distortion_factor;
      Yu_2 = Yd_ * distortion_factor;

      /* record the error in the undistorted sensor coordinates */
      err[i] = hypot (Xu_1 - Xu_2, Yu_1 - Yu_2);
    }
  return 0;
}


void
TsaiPlanarCalibration::ccNicOptimization (void)
{
  const int NPARAMS = 8;
  /* Parameters needed by MINPACK's lmdif() */

  int m = points.size ();
  int n = NPARAMS;
  double x[NPARAMS];
  double *fvec;
  double ftol = REL_SENSOR_TOLERANCE_ftol;
  double xtol = REL_PARAM_TOLERANCE_xtol;
  double gtol = ORTHO_TOLERANCE_gtol;
  int maxfev = MAXFEV;
  double epsfcn = EPSFCN;
  double diag[NPARAMS];
  int mode = MODE;
  double factor = FACTOR;
  int nprint = 0;
  int info;
  int nfev;
  double *fjac;
  int ldfjac = m;
  int ipvt[NPARAMS];
  double qtf[NPARAMS];
  double wa1[NPARAMS];
  double wa2[NPARAMS];
  double wa3[NPARAMS];
  double *wa4;

  /* allocate some workspace */
  if ((fvec =
       (double *) calloc ((unsigned int) m,
			  (unsigned int) sizeof (double))) == NULL)
    {
      cerr << "calloc: Cannot allocate workspace fvec\n";
      exit (-1);
    }

  if ((fjac =
       (double *) calloc ((unsigned int) m * n,
			  (unsigned int) sizeof (double))) == NULL)
    {
      cerr << "calloc: Cannot allocate workspace fjac\n";
      exit (-1);
    }

  if ((wa4 =
       (double *) calloc ((unsigned int) m,
			  (unsigned int) sizeof (double))) == NULL)
    {
      cerr << "calloc: Cannot allocate workspace wa4\n";
      exit (-1);
    }

  /* use the current calibration and camera constants as a starting point */
  x[0] = cc.Rx;
  x[1] = cc.Ry;
  x[2] = cc.Rz;
  x[3] = cc.Tx;
  x[4] = cc.Ty;
  x[5] = cc.Tz;
  x[6] = cc.kappa1;
  x[7] = cc.f;

  /* define optional scale factors for the parameters */
  if (mode == 2)
    {
      for (int i = 0; i < NPARAMS; i++)
	diag[i] = 1.0;		/* some user-defined values */
    }

  /* perform the optimization */
  if ( lmdif_this == 0 )
    {
      lmdif_this = this;
    }
  else
    {
      std::cerr << "Mutex on lmdif_this failed!\n";
      exit( 23 );
    }

  lmdif_( (S_fp) &TsaiPlanarCalibration::wrapperCcNicOptimizationError,
	  (integer *)&m, (integer *)&n, (doublereal *)x, (doublereal *)fvec, 
	  (doublereal *)&ftol, (doublereal *)&xtol, 
	  (doublereal *)&gtol, (integer *)&maxfev, (doublereal *)&epsfcn,
	  (doublereal *)diag, (integer *)&mode, (doublereal *)&factor, 
	  (integer *)&nprint, (integer *)&info, (integer *)&nfev, 
	  (doublereal *)fjac, (integer *)&ldfjac,
	  (integer *)ipvt, (doublereal *)qtf, (doublereal *)wa1, 
	  (doublereal *)wa2, (doublereal *)wa3, (doublereal *)wa4);
  lmdif_this = 0;

  /* update the calibration and camera constants */
  cc.Rx = x[0];
  cc.Ry = x[1];
  cc.Rz = x[2];
  applyRPYTransform ();

  cc.Tx = x[3];
  cc.Ty = x[4];
  cc.Tz = x[5];
  cc.kappa1 = x[6];
  cc.f = x[7];

  /* release allocated workspace */
  free (fvec);
  free (fjac);
  free (wa4);

#ifdef XXDEBUG
  /* print the number of function calls during iteration */
  cerr << "info: " << info << ",nfev: " << nfev << "\n\n";
#endif
}


/************************************************************************/
int
TsaiPlanarCalibration::ccFullOptimizationError (int *m_ptr, int *n_ptr, double *params, double *err)
				/* pointer to number of points to fit */
				/* pointer to number of parameters */
				/* vector of parameters */
				/* vector of error from data */
{
  double xc,
    yc,
    zc,
    Xd_,
    Yd_,
    Xu_1,
    Yu_1,
    Xu_2,
    Yu_2,
    distortion_factor,
    Rx,
    Ry,
    Rz,
    Tx,
    Ty, Tz, kappa1, f, Cx, Cy, r1, r2, r4, r5, r7, r8, sa, sb, sg, ca, cb, cg;

  Rx = params[0];
  Ry = params[1];
  Rz = params[2];
  Tx = params[3];
  Ty = params[4];
  Tz = params[5];
  kappa1 = params[6];
  f = params[7];
  Cx = params[8];
  Cy = params[9];

  SINCOS (Rx, sa, ca);
  SINCOS (Ry, sb, cb);
  SINCOS (Rz, sg, cg);
  r1 = cb * cg;
  r2 = cg * sa * sb - ca * sg;
  r4 = cb * sg;
  r5 = sa * sb * sg + ca * cg;
  r7 = -sb;
  r8 = cb * sa;

  for (int i = 0; i < points.size (); i++)
    {
      struct ControlPoint p = points.getPoint (i);
      /* convert from world coordinates to camera coordinates */
      /* Note: zw is implicitly assumed to be zero for these (coplanar) calculations */
      xc = r1 * p.xw + r2 * p.yw + Tx;
      yc = r4 * p.xw + r5 * p.yw + Ty;
      zc = r7 * p.xw + r8 * p.yw + Tz;

      /* convert from camera coordinates to undistorted sensor plane coordinates */
      Xu_1 = f * xc / zc;
      Yu_1 = f * yc / zc;

      /* convert from 2D image coordinates to distorted sensor coordinates */
      Xd_ = camera.dpx * (p.xm - Cx) / camera.sx;
      Yd_ = camera.dpy * (p.ym - Cy);

      /* convert from distorted sensor coordinates to undistorted sensor plane coordinates */
      distortion_factor = 1 + kappa1 * (SQR (Xd_) + SQR (Yd_));
      Xu_2 = Xd_ * distortion_factor;
      Yu_2 = Yd_ * distortion_factor;

      /* record the error in the undistorted sensor coordinates */
      err[i] = hypot (Xu_1 - Xu_2, Yu_1 - Yu_2);
    }
  return 0;
}


void
TsaiPlanarCalibration::ccFullOptimization (void)
{
  const int NPARAMS = 10;

  /* Parameters needed by MINPACK's lmdif() */

  int m = points.size ();
  int n = NPARAMS;
  double x[NPARAMS];
  double *fvec;
  double ftol = REL_SENSOR_TOLERANCE_ftol;
  double xtol = REL_PARAM_TOLERANCE_xtol;
  double gtol = ORTHO_TOLERANCE_gtol;
  int maxfev = MAXFEV;
  double epsfcn = EPSFCN;
  double diag[NPARAMS];
  int mode = MODE;
  double factor = FACTOR;
  int nprint = 0;
  int info;
  int nfev;
  double *fjac;
  int ldfjac = m;
  int ipvt[NPARAMS];
  double qtf[NPARAMS];
  double wa1[NPARAMS];
  double wa2[NPARAMS];
  double wa3[NPARAMS];
  double *wa4;

  /* allocate some workspace */
  if ((fvec =
       (double *) calloc ((unsigned int) m,
			  (unsigned int) sizeof (double))) == NULL)
    {
      cerr << "calloc: Cannot allocate workspace fvec\n";
      exit (-1);
    }

  if ((fjac =
       (double *) calloc ((unsigned int) m * n,
			  (unsigned int) sizeof (double))) == NULL)
    {
      cerr << "calloc: Cannot allocate workspace fjac\n";
      exit (-1);
    }

  if ((wa4 =
       (double *) calloc ((unsigned int) m,
			  (unsigned int) sizeof (double))) == NULL)
    {
      cerr << "calloc: Cannot allocate workspace wa4\n";
      exit (-1);
    }

  /* use the current calibration and camera constants as a starting point */
  x[0] = cc.Rx;
  x[1] = cc.Ry;
  x[2] = cc.Rz;
  x[3] = cc.Tx;
  x[4] = cc.Ty;
  x[5] = cc.Tz;
  x[6] = cc.kappa1;
  x[7] = cc.f;
  x[8] = camera.Cx;
  x[9] = camera.Cy;

  /* define optional scale factors for the parameters */
  if (mode == 2)
    {
      for (int i = 0; i < NPARAMS; i++)
	diag[i] = 1.0;		/* some user-defined values */
    }

  /* perform the optimization */
  if ( lmdif_this == 0 )
    {
      lmdif_this = this;
    }
  else
    {
      std::cerr << "Mutex on lmdif_this failed!\n";
      exit( 23 );
    }

  lmdif_( (S_fp) &TsaiPlanarCalibration::wrapperCcFullOptimizationError,
	  (integer *)&m, (integer *)&n, (doublereal *)x, (doublereal *)fvec, 
	  (doublereal *)&ftol, (doublereal *)&xtol, 
	  (doublereal *)&gtol, (integer *)&maxfev, (doublereal *)&epsfcn,
	  (doublereal *)diag, (integer *)&mode, (doublereal *)&factor, 
	  (integer *)&nprint, (integer *)&info, (integer *)&nfev, 
	  (doublereal *)fjac, (integer *)&ldfjac,
	  (integer *)ipvt, (doublereal *)qtf, (doublereal *)wa1, 
	  (doublereal *)wa2, (doublereal *)wa3, (doublereal *)wa4);
  lmdif_this = 0;

  /* update the calibration and camera constants */
  cc.Rx = x[0];
  cc.Ry = x[1];
  cc.Rz = x[2];
  applyRPYTransform ();

  cc.Tx = x[3];
  cc.Ty = x[4];
  cc.Tz = x[5];
  cc.kappa1 = x[6];
  cc.f = x[7];
  camera.Cx = x[8];
  camera.Cy = x[9];

  /* release allocated workspace */
  free (fvec);
  free (fjac);
  free (wa4);

#ifdef XXDEBUG
  /* print the number of function calls during iteration */
  cerr << "info: " << info << ",nfev: " << nfev << "\n\n";
#endif
}

/***********************************************************************\
* This routine solves for the roll, pitch and yaw angles (in radians)	*
* for a given orthonormal rotation matrix (from Richard P. Paul,        *
* Robot Manipulators: Mathematics, Programming and Control, p70).       *
* Note 1, should the rotation matrix not be orthonormal these will not  *
* be the "best fit" roll, pitch and yaw angles.                         *
* Note 2, there are actually two possible solutions for the matrix.     *
* The second solution can be found by adding 180 degrees to Rz before   *
* Ry and Rx are calculated.                                             *
\***********************************************************************/
void
TsaiPlanarCalibration::solveRPYTransform (void)
{
  double sg, cg;

  cc.Rz = atan2 (cc.r4, cc.r1);

  SINCOS (cc.Rz, sg, cg);

  cc.Ry = atan2 (-cc.r7, cc.r1 * cg + cc.r4 * sg);

  cc.Rx = atan2 (cc.r3 * sg - cc.r6 * cg, cc.r5 * cg - cc.r2 * sg);
}


/***********************************************************************\
* This routine simply takes the roll, pitch and yaw angles and fills in	*
* the rotation matrix elements r1-r9.					*
\***********************************************************************/
void
TsaiPlanarCalibration::applyRPYTransform (void)
{
  double sa, ca, sb, cb, sg, cg;

  SINCOS (cc.Rx, sa, ca);
  SINCOS (cc.Ry, sb, cb);
  SINCOS (cc.Rz, sg, cg);

  cc.r1 = cb * cg;
  cc.r2 = cg * sa * sb - ca * sg;
  cc.r3 = sa * sg + ca * cg * sb;
  cc.r4 = cb * sg;
  cc.r5 = sa * sb * sg + ca * cg;
  cc.r6 = ca * sb * sg - cg * sa;
  cc.r7 = -sb;
  cc.r8 = cb * sa;
  cc.r9 = ca * cb;
}

void
TsaiPlanarCalibration::generateCalibrationData (void)
{
  cout << "TPC::generateCalibrationData called\n";
  const double off_x = 1000.0;
  const double off_y = 1000.0;
  const double dist_x = 166.34;
  const double dist_y = 159.22;

  double xw, yw;
  double xm, ym;

  for (int i = 0; i < 3; i++)
    {
      for (int j = 0; j < 4; j++)
	{
	  xw = off_x + dist_x * j;
	  yw = off_y + dist_y * i;

	  worldCoordToImageCoord (xw, yw, 0.0, &xm, &ym );
	  points.addPoint ( ControlPoint( xw, yw, 0.0, xm, ym, 0.0 ) );
	}
    }

  cerr << "TsaiPlanarCalibration::generateCalibrationData:\n";
  for (int i = 0; i < 12; i++)
    {
      struct ControlPoint p = points.getPoint (i);
      cerr << "ControlPoint p = \n";
      cerr << "p.xw=" << p.xw << "\n";
      cerr << "p.yw=" << p.yw << "\n";
      cerr << "p.zw=" << p.zw << "\n";
      cerr << "p.xm=" << p.xm << "\n";
      cerr << "p.ym=" << p.ym << "\n";
      cerr << "p.zm=" << p.zm << "\n";
    }

}

void
TsaiPlanarCalibration::coplanarCalibrationWithFullOptimization (void)
{
  /* start with a 3 parameter (Tz, f, kappa1) optimization */
  ccThreeParmOptimization ();

#ifdef DEBUG
  std::cout << "Tz=" << cc.Tz << " f=" << cc.f << " kappa1=" << cc.kappa1 << '\n';
#endif

  /* do a 5 parameter (Tz, f, kappa1, Cx, Cy) optimization */
  ccFiveParmOptimizationWithLateDistortionRemoval ();

  /* do a better 5 parameter (Tz, f, kappa1, Cx, Cy) optimization */
  ccFiveParmOptimizationWithEarlyDistortionRemoval ();

  /* do a full optimization minus the image center */
  ccNicOptimization ();

  /* do a full optimization including the image center */
  ccFullOptimization ();
}

void
TsaiPlanarCalibration::recalibrate (void)
{
#ifdef DEBUG
  cout << "TsaiPlanarCalibration::recalibrate() called\n";
  for (vector < struct ControlPoint >::const_iterator it =
       points.points.begin (); it != points.points.end (); ++it)
    {
      struct ControlPoint point = *it;
      cout << "Point " << point.xw << "," << point.yw << "," << point.
	zw << " " << point.xm << "," << point.ym << endl;
    }
  cout << "camera info: Ncx " << camera.Ncx << ", Nfx " << camera.Nfx << ", dpx " << camera.dpx << ", dpy " << camera.dpy << ", Sx " << camera.sx << endl;
#endif
  coplanarCalibrationWithFullOptimization ();

  calcErrorStats ();
#ifdef DEBUG
  cout << "Calibration Errors: mean=" << mean << ", stddev=" << stddev <<
    ",max=" << max << ",sse=" << sse << "\n";
#endif
}

void
TsaiPlanarCalibration::calcErrorStats (void)
{
  double xc,
    yc,
    zc,
    Xu,
    Yu,
    Xd,
    Yd,
    t,
    distortion_factor,
    error, squared_error, max_error = 0, sum_error = 0, sum_squared_error = 0;

  if (points.size () < 1)
    {
      mean = stddev = max = sse = 0;
      return;
    }

  for (int i = 0; i < points.size (); i++)
    {
      struct ControlPoint p = points.getPoint (i);
      /* determine the position of the 3D object space point in camera coordinates */
      worldCoordToCameraCoord (p.xw, p.yw, p.zw, &xc, &yc, &zc);

      /* convert the measured 2D image coordinates into distorted sensor coordinates */
      Xd = camera.dpx * (p.xm - camera.Cx) / camera.sx;
      Yd = camera.dpy * (p.ym - camera.Cy);

      /* convert from distorted sensor coordinates into undistorted sensor plane coordinates */
      distortion_factor = 1 + cc.kappa1 * (SQR (Xd) + SQR (Yd));
      Xu = Xd * distortion_factor;
      Yu = Yd * distortion_factor;

      /* find the magnitude of the distance (error) of closest approach */
      /* between the undistorted line of sight and the point in 3 space */
      t =
	(xc * Xu + yc * Yu + zc * cc.f) / (SQR (Xu) + SQR (Yu) + SQR (cc.f));
      squared_error =
	SQR (xc - Xu * t) + SQR (yc - Yu * t) + SQR (zc - cc.f * t);
      error = sqrt (squared_error);
      sum_error += error;
      sum_squared_error += squared_error;
      max_error = MAX (max_error, error);
    }

  mean = sum_error / points.size ();
  max = max_error;
  sse = sum_squared_error;

  if (points.size () == 1)
    stddev = 0;
  else
    stddev =
      sqrt ((sum_squared_error -
	     SQR (sum_error) / points.size ()) / (points.size () - 1));
}

int 
TsaiPlanarCalibration::wrapperCcComputeExactFAndTzError( int * m_ptr, int * n_ptr, double * params, double * err )
{
  TsaiPlanarCalibration * p = lmdif_this;
  return p->ccComputeExactFAndTzError( m_ptr, n_ptr, params, err );
}

int TsaiPlanarCalibration::wrapperCcFiveParmOptimizationWithLateDistortionRemovalError( int * m_ptr, int * n_ptr, double * params, double * err )
{
  TsaiPlanarCalibration * p = lmdif_this;
  return p->ccFiveParmOptimizationWithLateDistortionRemovalError( m_ptr, n_ptr, params, err );
}

int TsaiPlanarCalibration::wrapperCcFiveParmOptimizationWithEarlyDistortionRemovalError(int * m_ptr, int * n_ptr, double * params, double * err )
{
  TsaiPlanarCalibration * p = lmdif_this;
  return p->ccFiveParmOptimizationWithEarlyDistortionRemovalError( m_ptr, n_ptr, params, err );
}

int TsaiPlanarCalibration::wrapperCcNicOptimizationError(int * m_ptr, int * n_ptr, double * params, double * err )
{
  TsaiPlanarCalibration * p = lmdif_this;
  return p->ccNicOptimizationError( m_ptr, n_ptr, params, err );
}

int TsaiPlanarCalibration::wrapperCcFullOptimizationError(int * m_ptr, int * n_ptr, double * params, double * err )
{
  TsaiPlanarCalibration * p = lmdif_this;
  return p->ccFullOptimizationError( m_ptr, n_ptr, params, err );
}

double TsaiPlanarCalibration::getMaxError( void )
{
  calcErrorStats ();
  return max;
}

std::string TsaiPlanarCalibration::getErrors( void )
{
  char buffer[512];
  calcErrorStats ();
  
  sprintf(buffer, "Calibration Errors: mean = %lf. stddev = %lf, max = %lf. sse = %lf\n",mean,stddev,max,sse);
  
  return std::string(buffer);
}

void TsaiPlanarCalibration::printCameraData()
{
  cout << "camera info: Ncx " << camera.Ncx << ", Nfx " << camera.Nfx << ", dpx " << camera.dpx << ", dpy " << camera.dpy << ", Sx " << camera.sx << endl;
  for(int i = 0; i < points.size(); i++)
    {
      cout << points.points[i] << endl;
    }
}
