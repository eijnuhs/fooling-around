#include "ball.h"
#include "framebuffer.h"
#include "framebufferworld.h"

#define VAL_LB 0.05

Ball::Ball(std::string name, double height, double radius, double mmPerPix, ColourDefinition * cd) :
  Spot(name, height, radius, mmPerPix, cd, Trackable::BALL)
{
  idealArea = M_PI * radius * radius;
  std::cout << "Ball: r: " << radius << "  idealArea: " << idealArea << "\n";
  Trackable::scratchpad()->initialize( FrameBuffer::RGB32, 60, 60 );
}
Ball::~Ball()
{
  // 0
}
  
// methods required by trackable.
bool Ball::isMe(RunRegion & region, FrameBuffer * inFrame, FrameBufferWorld * worldFrame, unsigned int avgIntensity)
{
  t = inFrame->t;
  double lrx,lry,wx,wy;
  if(Spot::isMe(region, inFrame, worldFrame, avgIntensity, false))
    {
      lrx = region.getCOMXD();
      lry = region.getCOMYD();
      worldFrame->bufferToWorldCoordinates(lrx,lry,&wx,&wy);
      regions.push_back(WorldRegion(wx,wy,region,worldFrame->mmPerPix));
    }
  return false;
}

bool Ball::trackMe(FrameBuffer * inFrame, FrameBufferWorld * worldFrame)
{
  t = inFrame->t;
  WorldRegion wr;
  double wx,wy;
  double pixelDim = (Trackable::getRadius() * 8.0) / (double)Trackable::scratchpad()->height;
  if(findMe(inFrame, pixelDim, &wx,&wy, &wr))
    addRegion(wr);

  return false;
}

void Ball::trackingComplete()
{
  // now is the time to sort through the world regions
  // to find the most appropriate ball.
  if(regions.size() > 0)
    {
      int bestIdx = 0;
      double bestValue = regions[0].value; 
      for(uint i = 1; i < regions.size(); ++i)
	{
	  if(regions[i].value > bestValue)
	    {
	      bestIdx = i;
	      bestValue = regions[i].value;
	    }
	}
      if(bestIdx != -1)
	setPosition(regions[bestIdx].wx,regions[bestIdx].wy, 0.0, t); 
    }
}

//bool findMe(FrameBuffer * inFrame, double pixelDim, double * wx, double * wy, WorldRegion * theRegion = 0);

void Ball::addRegion(double wx, double wy, const RunRegion & r, double mmPerPix)
{
  WorldRegion wr(wx,wy,r,mmPerPix);
  addRegion(wr);
}

void Ball::addRegion(WorldRegion wr)
{
  // compute value.
  double value = 1 - (fabs(idealArea - wr.area) / idealArea);
  double ratio = (wr.hwRatio > 1.0) ? (1.0/wr.hwRatio ) : wr.hwRatio;
  if(value < 0) 
    wr.value = 0;
  else
    wr.value = value * ratio;

  if(wr.value > VAL_LB) // set a lower bound on agreeing this is the ball.
    {
      regions.push_back(wr);
      //std::cout << "Adding Region at (" << wr.wy << "," << wr.wx << ") hw: " << wr.hwRatio << " area: " << wr.area << " value: " << wr.value << "\n";
    }
}

void Ball::initForFrame()
{
  regions.clear();
  Trackable::initForFrame();
  
}
