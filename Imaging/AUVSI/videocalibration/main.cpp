#include <qapplication.h>
#include "mainwindow.h"
#include "videostream.h"
#include "videocalibration.h"
#include "videocalibrationviewer.h"
#include "v4l2device.h"
#include "v4l2control.h"
#include "framespersecond.h"
#include "videostreamconfig.h"
#include <string>
#include "videointerpreter2d.h"
#include "videointerpreter2dconfig.h"
#include "videointerpreter2dviewer.h"


#ifdef DEBUG_PTF
#include <iostream>
using namespace std;
#endif

int main( int argc, char ** argv )
{
    QApplication a( argc, argv );
    Form1 w;
    int ret = 0;
    std::string configFile = "vc.conf";
    
    // create configuration objects
    VideoStreamConfig vsc;
    VideoInterpreter2DConfig vi2dc;

    // look for a file name on the command line
    for(int i = 2; i < a.argc(); i++)
      {
	if(strcmp(a.argv()[i-1],"-c") == 0)
	   configFile = a.argv()[i];
      }

    // initialize the configuration file
    if(!Configuration::init(configFile))
      {
	exit(1);
      }

    // initialize processing objects
    FramesPerSecond fps;
    std::cout << "creating video stream\n";
    VideoStream vs( &vsc );
    std::cout << "creating video interpreter\n";
    VideoInterpreter2D vi2d( &vs, &vi2dc );
    std::cout << "creating video calibration manager\n";
    VideoCalibration vc( & vi2d);

    vs.setVideoCalibration( &vc ); 

    // associate GUI objects and processing objects.
    w.videoCalibrationViewer->setVideoCalibration(& vc );
     
    // initialize the V4L2 Control sliders
    V4L2Device * vDevice = dynamic_cast<V4L2Device *>(vs.getVideoDevice());
    if(vDevice)
      {
	w.v4L2Control->init(vDevice);
      }
    else
      {
	w.v4L2Control->hideParent();
      }

    // display the GUI
    w.show();
 
    // connect signals and slots.
    a.connect( &a, SIGNAL( lastWindowClosed() ), &a, SLOT( quit() ) );
    QObject::connect( ( (QObject const *) & vs ), SIGNAL( newFrame( FrameBuffer * ) ),( ( QObject const * ) ( w.v4l2ControlPreview ) ), SLOT( newFrame( FrameBuffer * ) ) );
    QObject::connect( ( (QObject const *) & vs ), SIGNAL( newFrame( FrameBuffer * ) ),( ( QObject const * ) ( w.videoCalibrationViewer ) ), SLOT( newFrame( FrameBuffer * ) ) );
    QObject::connect( ( (QObject const *) & fps ), SIGNAL( updateFPS(double) ),( ( QObject const * ) ( w.lcdFramesPerSec ) ), SLOT( display(double) ) );
    QObject::connect( ( (QObject const *) & vs ), SIGNAL( fpsUpdate( double ) ),( ( QObject const * ) ( &fps ) ), SLOT(recieveFPS(double) ) );

    // start the capture thread.
    std::cout << "starting video stream\n";
    vs.start();
    // start the redraw timer (in milliseconds)
    FrameBufferViewer::startTimer(100);
    ret = a.exec();
    // end the capture thread
    vs.done = true;
    vs.wait();

    return ret;
}
