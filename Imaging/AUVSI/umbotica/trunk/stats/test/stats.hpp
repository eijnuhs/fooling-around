#ifndef FRANTIC_STATS_HPP
#define FRANTIC_STATS_HPP

#include <string>
#include <map>
#include <iostream>
#include <sstream>
#include <frantic/null_mutex.hpp>

#define PRETTY_BAR_WIDTH 40

namespace frantic { namespace stats {

  // namespace forward declarations
  class runtime_stat;
  class runtime_stat_tracker;

}}

// function prototypes:
std::ostream & operator<<(std::ostream & stream, frantic::stats::StatisticsTracker const & stat);
std::ostream & operator<<(std::ostream & stream, frantic::stats::Statistic const & stat);


#ifdef FRANTIC_NO_STATISTICS

namespace frantic { namespace stats

  class StatisticsTracker
  {
  public:
    void addStatistic(std::string const & , std::string const & , frantic::stats::Statistic *){}
  };


  class Statistic
  {
  public:
    Statistic(std::string , std::string , frantic::stats::StatisticsTracker * ){}
    Statistic(){};
    virtual ~Statistic(){}
    virtual std::string results() const
    {
      std::cout << "No statistics\n";
    }
  };


}} // namespace frantic::stats

inline std::ostream & operator<<(std::ostream & stream, frantic::stats::Statistic const & stat)
{
  stream << stat.results();
  return stream;
}


inline std::ostream & operator<<(std::ostream & stream, frantic::stats::StatisticsTracker const &)
{
  stream << "No Statistics\n";
  return stream;
}



#else // #ifdef FRANTIC_NO_STATISTICS

namespace frantic { namespace stats {

  template<typename MUTEX_T = frantic::threads::NullMutex>
  class StatisticsTracker
  {
  public:
    typedef MUTEX_T mutex_t;
    typedef typename mutex_t::scoped_lock lock_t;
    StatisticsTracker(){}
    virtual ~StatisticsTracker(){}

    void addStatistic(std::string const & category, 
                      std::string const & statName, 
                      frantic::stats::Statistic * stat)
    {
      lock_t lock(m_mutex);
      m_stats[category][statName] = stat;
    }  

    friend std::ostream & ::operator<<(std::ostream & stream, frantic::stats::StatisticsTracker const & stat);

  private:
    std::map<std::string, std::map<std::string, frantic::stats::Statistic * > > m_stats;
    mutable mutex_t m_mutex;
  };


  class Statistic
  {
  public:
    Statistic(std::string category, 
              std::string name, 
              frantic::stats::StatisticsTracker * statTracker)
    {
      if(statTracker)
	{
	  statTracker->addStatistic(category, name, this);
	}
    }

    Statistic(){};
    
    virtual ~Statistic(){}

    virtual std::string results() const = 0;
  };


}} // namespace frantic::stats

inline std::ostream & operator<<(std::ostream & stream, frantic::stats::Statistic const & stat)
{
  stream << stat.results();
  return stream;
}


inline std::ostream & operator<<(std::ostream & stream, frantic::stats::StatisticsTracker const & stat)
{
  frantic::stats::StatisticsTracker::lock_t lock(stat.m_mutex);

  // Iterate through the categories outputting the statistics under each.
  std::map<std::string, std::map<std::string, frantic::stats::Statistic * > >::const_iterator cat_it = stat.m_stats.begin();

  for( ; cat_it != stat.m_stats.end(); cat_it++)
    {
      // output the header.
      std::string title = cat_it->first;
      stream << title << std::endl;
      for(unsigned int i = 0; i < std::max<unsigned int>(PRETTY_BAR_WIDTH,title.size()); i++) stream << "-";

      stream << std::endl;

      // output the contents of the category.
      std::map<std::string, frantic::stats::Statistic * >::const_iterator cont_it = cat_it->second.begin();
      for( ; cont_it != cat_it->second.end(); cont_it++)
	{
	  stream << cont_it->first << ": " << (*(cont_it->second)) << std::endl;
	}

      // output the footer.
      for(unsigned int i = 0; i < std::max<unsigned int>(PRETTY_BAR_WIDTH,title.size()); i++) stream << "-";
      stream << std::endl;

    }

  return stream;
}

#endif // ifdef FRANTIC_NO_STATISTICS

#endif
