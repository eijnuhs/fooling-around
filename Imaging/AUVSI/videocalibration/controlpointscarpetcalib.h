// $Id: controlpointscarpetcalib.h,v 1.1.2.2 2004/10/08 14:27:26 cvs Exp $
//

#ifndef _CONTROLPOINTS_CARPET_CALIB_H_
#define _CONTROLPOINTS_CARPET_CALIB_H_

#include "controlpoints.h"
#include "calibration.h"

class ControlPointsCarpetCalib:public ControlPoints
{
public:
  ControlPointsCarpetCalib (double fwidth, double fheight);
  void clamp(int width, int height);

};

#endif
