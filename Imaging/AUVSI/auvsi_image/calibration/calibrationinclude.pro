CWD = $$system("pwd")
message( Including the calibration library from path $$CWD)
DEPENDPATH += $${CWD}
INCLUDEPATH += $${CWD}

SOURCES += auvsi_calibration.cpp calibration.cpp camera.cpp controlpoints.cpp square.cpp tsaiplanarcalibration.cpp matrix/matrix.c

LIBS += -L$${CWD}/minpack -lminpak
