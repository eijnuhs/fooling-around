#ifndef BACKTRACK_CMBTR_H
#define BACKTRACK_CMBTR_H
	
#include <string>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <vector>
#include <list>
#include <fstream>

#include <unistd.h>
#include <time.h>

/*
 * Backtracking Algorithm to look for matrix with 2 properties
 * 1) Every pair column must both have 
 * 		10 row and 
 * 		01 row
 * 2) Every triple columns must have 
 * 		100 row
 * 		010 row
 * 		001 row
 * */
class DSS_Matrix
{

public:
	DSS_Matrix(unsigned int N = 3);
	~DSS_Matrix();
	
	void backtrack_recursive(int type);
	void backtrack_recursive_ADDONE(unsigned int index);
	void backtrack_recursive_WEIGHTED(unsigned int index);
	void backtrack_iterative();
	void createMatrix(unsigned int col_size);
	void resetMatrix(unsigned int col_size, unsigned int row_size);
	void generateColumnFarm();
	unsigned int** generateWeightedColumn();
	void printMatrix();	
	std::string toString(unsigned int * m);	
	void printMatrixSize();
	void setMValue(unsigned int M);
	void setMatrix(unsigned int * matrix, unsigned int col_size, unsigned int row_size);
	bool isValidMatrix();
	bool isValidMatrix(unsigned int input,unsigned int col_index);
	unsigned int solutionFound();
	
	int bitsSet(int n){
		unsigned i, c = n;
		for (i = 0; c; i++) c ^= c & -c;
		return i;
	}
	
	void saveData( std::string info);
	void saveData( std::string info, unsigned int * mat);
	void saveData( std::string info,unsigned int * mat, std::string filename );
		
private:
	
	unsigned int N;			// MAX size of row
	unsigned int M;			// MAX size of column
	unsigned int P;			// number of possible columns
	unsigned int c_index; 	// the size of current matrix
	unsigned int ** col_farm;
	unsigned int ** weighted_col_farm;
	unsigned int * matrix;
	unsigned int found;
	time_t start;
	time_t elapsed;
	std::string saveTo;
	std::vector<unsigned int*> solutions;
	
	void resetColumnFarm(unsigned int index);
	void addSolution();
		
	bool weightTest(unsigned int w,unsigned int i,unsigned int col_index);
	bool weightChangeCheck(unsigned int prev_w,unsigned int w,unsigned int c,unsigned int c_index);
	
	bool testRulePair(unsigned int * matrix);
	bool testRuleTriple(unsigned int * matrix);
	bool checkPair(unsigned int i, unsigned int j);
	bool checkTriple(unsigned int i, unsigned int j, unsigned int k);
};

#endif
