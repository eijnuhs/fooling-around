#include <qapplication.h>
#include "umimage.hpp"
#include "agmainwindow.hpp"
#include <boost/bind.hpp>
#include <iostream>
#include <vigra/rgbvalue.hxx>
#include "umimagegui.hpp"
#include <vector>
#include "agref.hpp"
#include <string>
#include <tostring.hpp>
#include <umimagetypedefs.hpp>
#include <vigra/convolution.hxx> // gaussian gradient
#include <threadwrapper.hpp>
#include "boost/filesystem/operations.hpp" // includes boost/filesystem/path.hpp
#include "boost/filesystem/fstream.hpp"    // ditto
#include <agdockwindow.hpp>
#include "vigra/transformimage.hxx"
#include "vigra/inspectimage.hxx"
#include "vigra/flatmorphology.hxx"
#include "vigra/basicgeometry.hxx"
#include <cvaux.h>
#include <algorithm>
#include <qmessagebox.h>
#include <agshapes.hpp>


namespace fs = boost::filesystem;

std::ostream & operator<<(std::ostream & out, CvPoint3D32f & pt)
{
  out << umbotica::tuple(pt.x,pt.y,pt.z);
  return out;
}

std::ostream & operator<<(std::ostream & out, CvPoint2D32f & pt)
{
  out << umbotica::tuple(pt.x,pt.y);
  return out;
}


std::istream & operator>>(std::istream & in, CvPoint3D32f & pt)
{
  in >> pt.x >> pt.y >> pt.z;
  return in;
}

std::istream & operator>>(std::istream & in, CvPoint2D32f & pt)
{
  in >> pt.x >> pt.y;
  return in;
}


bool pointLessThanX(CvPoint2D32f a, CvPoint2D32f b)
{
  return a.x < b.x;
}

bool pointLessThanY(CvPoint2D32f a, CvPoint2D32f b)
{
  return a.y < b.y;
}

class Processing : public umbotica::ThreadWrapper
{
  private:  
    fs::directory_iterator imageFinder;//iteratively load image files from the directory
    fs::directory_iterator endIterator;//points to past-the-end of the images, used to tell when we got the last one
    fs::path imageDir;//path to the directory holding the files

    bool imRunning;//false if we already called die()
    
  public:
    //Select a directory that contains the calibration images.  Returns false if that
    //directory couldn't be found

  Processing() : brightness(1.0), contrast(6), m_zParam(15)
  { 
    memStorage = cvCreateMemStorage();
    std::cout << "\a";
    calibFilter.SetCameraCount(1);
    double params[3] = {6,8,1};
    calibFilter.SetEtalon(CV_CALIB_ETALON_CHESSBOARD, (double*)params);

  }
  ~Processing(){cvReleaseMemStorage(&memStorage);}

    bool setImageDir(std::string newImageDir)
    {
      bool result = true;//false if imageDir doesn't exist
      
      imageDir = newImageDir;
      if (!fs::exists(imageDir))
        result = false;

      return result;
    }//setImageDir()
    
    void init()
    {
      imRunning = true;
      imageFinder = fs::directory_iterator(imageDir);
      numPoints.clear();
      points.clear();
    }//init()
  
//     void processing()
//     {      
//       // ***** 1) Find next image. *****
//       if (imageFinder != endIterator)
//       {
// 	//        std::cout << "Image Name: " <<  imageFinder->leaf() << std::endl;

//         // ***** 2) image.loadImage(filename); *****
// 	std::cout << "Processing image: " << imageFinder->native_directory_string() << "\n";
// 	image.loadImage(imageFinder->native_directory_string());
// 	int idx = numPoints.size();
// 	numPoints.push_back(0);
// 	vigra::RGBValue<unsigned char> testColor(255,0,0);
// 	int intermediateCount = 0;
// 	for(int row = 0; row < image.height(); row++)
// 	  {
// 	    for(int col = 0; col < image.width(); col++)
// 	      {
		
// 		if(image[row][col].red() > 253)
// 		  {
// 		    intermediateCount++;
// 		    std::cout << "Found the color at pixel: " << tuple(col,row) << "\n";
// 		    numPoints[idx]++;
// 		    CvPoint2D32f point = {(double)col, (double)row};
// 		    points.push_back(point);
// 		    if(intermediateCount == 8)
// 		      {
// 			std::sort(points.end() - 8, points.end(),pointLessThanX);
// 			//			points.pop_back();
// 			//			points.pop_back();
// 			//			points.pop_back();
// 			//			numPoints[idx] -= 3;
// 			intermediateCount = 0;
// 		      }
// 		  }  
// 	      }
// 	  }
	
// 	if(numPoints[idx] == 0)
// 	  numPoints.pop_back();
	
// 	std::cout << "Number of corners: " << numPoints[idx] << "\n";
// 	for(unsigned int i = 0; i < points.size(); i++)
// 	  std::cout << tuple<double>(points[i].x,points[i].y) << "\n";

//         imageFinder++;
//       }//if imageFinder
//       else
//       {
//         if (imRunning)
//         {
//           std::cout << "No more images, dying..." << std::endl;
//           die();
//         }//if imRunning

//       }//else imageFin
//   }//processing()

  void processing()
  {

    try{

      if (imageFinder != endIterator)
	{
	  count_buffer = 0;
	  
	  std::cout << "Processing file \"" << imageFinder->leaf() << "\"\n"; 
	  if(imageFinder->leaf().size() == 0 || imageFinder->leaf()[0] == '.')
	    {
	      std::cout << "Invalid filename (probably .svn). Skipping file...\n";
	      return;
	    }
	  std::ifstream in;
	  in.open(imageFinder->native_directory_string().c_str());

	  CvPoint2D32f p2d;
	  CvPoint3D32f p3d;
	  //	  std::string discard;
	  //	  in >> discard;

	  while(!in.eof())
	    {
	      in >> p3d;
	      in >> p2d;
	      count_buffer++;
	      std::cout << "Point[" << numPoints[idx] << "] " << p2d << " ==> " << p3d << "\n";
	      point2d_buffer.push_back(p2d);
	      point3d_buffer.push_back(p3d);
	    }
	  
	  // convert these to shapes
	  shapes.clear();
	  for(int i = 0; i < count_buffer; i++)
	    {
	      AGShape shape;
	      shape.circle(point2d_buffer[i].x, point2d_buffer[i].y
	    }

	  //	  QMessageBox::question ( QWidget * parent, const QString & caption, const QString & text, const QString & button0Text = QString::null, const QString & button1Text = QString::null, const QString & button2Text = QString::null, int defaultButtonNumber = 0, int escapeButtonNumber = -1 ) 

	  if(numPoints[idx] == 0)
	    numPoints.pop_back();
	  
	  int idx = numPoints.size();
	  numPoints.push_back(0);
	}
    }
    catch(std::exception const & e)
    {
      std::cout << "Error parsing file: " << p->native_directory_string() << ": " << e.what() << "\n";
    }


  }
  
  void calibrate()
  {
    CvSize imageSize = {640,240};
    CvPoint3D32f objectPts[points.size()];
    float transVects32f[3 * numPoints.size()];
    float rotMatrs32f[9 * numPoints.size()];


    std::cerr << "Calibrating...numImages: " << numPoints.size() << " num Points: " << points.size() << "==" << numPoints[0] << "\n";
    
    int np[numPoints.size()];
    for(unsigned int i = 0; i < numPoints.size(); i++)
      np[i] = numPoints[i];

    CvPoint2D32f pts[points.size()];
    for(unsigned int i = 0; i < points.size(); i++)
      pts[i] = points[i];
    //    for(int i = 0; i < 400; i++)
    //      distortion32f[i] = cameraMatrix32f[i] = transVects32f[i] = rotMatrs32f[i] = 0.0f;

    int l = 0;
    for(unsigned int i = 0; i < points.size() / 48; i++)
      {
	for(int j = 0; j < 6; j++)
	  for(int k = 0; k < 8; k++)
	    {
	      objectPts[l].x = k;
	      objectPts[l].y = j;
	      objectPts[l].z = m_zParam;
	      l++;
	    }
      }

    cvCalibrateCamera( numPoints.size(), (int*)np, imageSize,(CvPoint2D32f*)pts, objectPts,
 				camera.distortion, camera.matrix,
 				transVects32f, rotMatrs32f,0);
    std::cout << "Distortion\n";
    std::cout << "k1: " << camera.distortion[0] << "\n";
    std::cout << "k2: " << camera.distortion[1] << "\n";
    std::cout << "p1: " << camera.distortion[2] << "\n";
    std::cout << "p2: " << camera.distortion[3] << "\n";

    /*    [ fx 0 cx; 0 fy cy; 0 0 1 ] */
    std::cout << "Matrix\n";
    std::cout << "fx: " << camera.matrix[0] << "\n";
    std::cout << "0: " << camera.matrix[1] << "\n";
    std::cout << "cx: " << camera.matrix[2] << "\n";

    std::cout << "0: " << camera.matrix[3] << "\n";
    std::cout << "fy: " << camera.matrix[4] << "\n";
    std::cout << "cy: " << camera.matrix[5] << "\n";

    std::cout << "0: " << camera.matrix[6] << "\n";
    std::cout << "0: " << camera.matrix[7] << "\n";
    std::cout << "1: " << camera.matrix[8] << "\n";

    std::cerr << "Distortion\n";
    std::cerr << "k1: " << camera.distortion[0] << "\n";
    std::cerr << "k2: " << camera.distortion[1] << "\n";
    std::cerr << "p1: " << camera.distortion[2] << "\n";
    std::cerr << "p2: " << camera.distortion[3] << "\n";

    /*    [ fx 0 cx; 0 fy cy; 0 0 1 ] */
    std::cerr << "Matrix\n";
    std::cerr << "fx: " << camera.matrix[0] << "\n";
    std::cerr << "0: " << camera.matrix[1] << "\n";
    std::cerr << "cx: " << camera.matrix[2] << "\n";

    std::cerr << "0: " << camera.matrix[3] << "\n";
    std::cerr << "fy: " << camera.matrix[4] << "\n";
    std::cerr << "cy: " << camera.matrix[5] << "\n";

    std::cerr << "0: " << camera.matrix[6] << "\n";
    std::cerr << "0: " << camera.matrix[7] << "\n";
    std::cerr << "1: " << camera.matrix[8] << "\n";




    UmRGB24 temp;
    temp.copyFrom(image);

    cvUnDistortOnce( temp.cvImage(), image.cvImage(),
		     camera.matrix,
		     camera.distortion,
		     1 );

  }

  void cleanup()
  {
    // calibrate and print results
    imageFinder = endIterator;  
//     if (imageFinder != NULL)
//     {
//       delete imageFinder;
//       imageFinder = NULL;
//     }//if imageFinder
  }//cleanup()

  UmRGB24 image;

  float brightness, contrast;
  int radius;

  std::vector<int> numPoints;
  std::vector<CvPoint2D32f> point2d;
  std::vector<CvPoint3D32f> point3d;

  struct CvCamera camera; // cvaux.h
  int m_zParam;
  
  int count_buffer;
  std::vector<CvPoint2D32f> point2d_buffer;
  std::vector<CvPoint3D32f> point3d_buffer;
  std::vector<AgShape> shapes;


CvMemStorage * memStorage;
CvCalibFilter calibFilter;
};

int main(int argc, char ** argv)
{ 
  QApplication a(argc, argv);
  a.connect( &a, SIGNAL( lastWindowClosed() ), &a, SLOT( quit() ) );
  Processing p;
  AGMainWindow w;
  w.resize(1024,768);
  int ret = 0;
  
  AGManager & gui = *w.getAGManager();
  
  // the autogui is a little rough around the edges...File
  // dialog requires a getter as well as a setter.

  // add the image with a one second update time.
  gui.pushGroup("log");
  gui.addLogWindow(std::cout);
  gui.setWindowSize(1024,10);
  gui.popGroup();

  gui.pushGroup();
  addToGui(gui, p.image, "Image", 500);
  gui.setWindowSize(1024,768);
  gui.addButton("Start Thread", boost::bind(&Processing::startThread, &p));
  gui.addButton("calibrate", boost::bind(&Processing::calibrate, &p));
  gui.addInt("zParam", p.m_zParam,0,2000,1);
  gui.popGroup();

  w.show();

  //  AGDockWindow * imageW = gui.getWindow(0);
  //  imageW->setFixedExtentHeight(1024);

  

if (p.setImageDir("../savevideo/cb"))
  {
    p.startThread();
    ret = a.exec();
    p.stopThread();
  }//if setImageDir
  else
    std::cout << "Image directory not found" << std::endl;

  std::cout << "Halting normally" << std::endl;
  
  return ret;
}
