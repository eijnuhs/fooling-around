#include "v4l2control.h"
#include <linux/videodev.h>
#include "v4l2device.h"
#include <v4l2controlitem.h>
#include <qvbox.h>

#ifdef DEBUG_PTF
#include <iostream>
using namespace std;
#endif

V4L2Control::V4L2Control(QWidget * parent, const char * name) :
  QVGroupBox(parent, name),
  videoDevice(0)
{
  // hello!
  setTitle("V4L2 Control");

}

void V4L2Control::init(V4L2Device * videoDevice)
{
#ifdef DEBUG
  cout << "V4L2Control::init() called\n"; 
#endif

  // make sure the widget has not already been initialized.
  // and the passed videodevice exists.
  if(this->videoDevice == 0 && videoDevice)
    {
      this->videoDevice = videoDevice;
      struct v4l2_queryctrl qc;
      //  from V4L2_CID_BASE to V4L2_CID_LASTP1
      for(unsigned int i = V4L2_CID_BASE; i < V4L2_CID_LASTP1; ++i)
	{
	  qc.id = i;
    
	  int errorCode = videoDevice->queryControl(&qc);
	  // query control and add control if it is available.
	  if (errorCode == 0 && qc.type == V4L2_CTRL_TYPE_INTEGER)
	    {
	       new V4L2ControlItem(videoDevice, qc, this);
	    }
#ifdef DEBUG
	  cout << "Querying Contol: " << i << endl;
	  cout << "qc.id: " << qc.id << endl;
	  cout << "qc.type: " << qc.type << endl;
	  cout << "qc.name: " << qc.name << endl;
	  cout << "Error Code: " << errorCode << endl;
#endif

	}
    }
  
}

void V4L2Control::hideParent()
{
  QWidget * parent = QWidget::parentWidget();
  if(parent)
    {
      parent->hide();
#ifdef DEBUG_PTF
      std::cout << "Hiding parent: " << parent->name() << std::endl;
#endif
    }
}




