TEMPLATE	= app
LANGUAGE	= C++

macx {
LIBS += -lautogui -L/sw/lib -lboost_date_time -lboost_filesystem -lboost_thread -lmysqlpp \
        -L/usr/local/mysql/lib -lmysqlclient -lexif -lintl -lgd -bind_at_load
INCLUDEPATH += ./ /usr/local/include/boost-1_33_1 ../../autogui/include ../../auvsi_common /usr/local/mysql/include /sw/include
}

!macx {
LIBS += -lautogui -lboost_date_time -lboost_filesystem -lboost_thread -lmysqlpp -lmysqlclient -lexif -lgd
INCLUDEPATH += ./ /usr/local/include ../../autogui/include ../../auvsi_common /usr/include \
     /usr/include/mysql
}

DEPENDPATH += ./ ../../auvsi_common ../../

HEADERS += gps.hpp \
           telemetry.hpp \
           smoother.hpp \
           localregression.hpp \
           sqlinterface.hpp \
           helper_fns.hpp \
           umbotica/math/radian.hpp

SOURCES	+= main.cpp \
           gps.cpp \
           sqlinterface.cpp \
           umbotica/math/radian.cpp

include( ../../opencv_wrapper/cvwrapinclude.pro )
include( ../../umbotica/utilsinclude.pro )
include( ../calibration/calibrationinclude.pro )
include( ../../configuration/configurationinclude.pro )

QMAKE_CC=g++


QMAKE_CXXFLAGS = -ggdb

macx {
QMAKE_CXXFLAGS += -Wno-long-double
}

unix {
  UI_DIR = .ui
  MOC_DIR = .moc
  OBJECTS_DIR = .obj
}
