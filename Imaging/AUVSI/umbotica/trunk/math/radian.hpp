#ifndef RADIAN_H
#define RADIAN_H

#include <iostream>

#ifndef M_PI
#    define M_PI     3.14159265358979323846
#endif

#ifndef M_SQRT2
#    define M_SQRT2  1.41421356237309504880
#endif


#ifndef M_2PI
#    define M_2PI    6.28318530717958647692
#endif 

#ifndef DEG_PER_RAD
#    define DEG_PER_RAD 57.29577951308232
#endif


// a class representing a radian angle between
// -PI and PI.
class Radian
{
public:
  Radian(double angle = 0.0);
  // from a normal vector
  Radian(double nx, double ny);
  virtual ~Radian();
  
  // assignment
  Radian & operator=(Radian const & rhs);
  Radian & operator=(double const rhs);

  bool operator<(Radian const & rhs) const ;
  bool operator<(double const rhs) const ;
  bool operator<=(Radian const & rhs) const ;
  bool operator<=(double const rhs) const ;
  bool operator>(Radian const & rhs) const ;
  bool operator>(double const rhs) const ;
  bool operator>=(Radian const & rhs) const ;
  bool operator>=(double const rhs) const ;
  bool operator==(Radian const & rhs) const ;
  bool operator==(double const rhs) const ;
  bool operator!=(Radian const & rhs) const ;
  bool operator!=(double const rhs) const ;


  // add or subtract an angle.
  Radian & operator+=(Radian const & rhs);
  Radian & operator+ (Radian const & rhs) const ;
  Radian & operator-=(Radian const & rhs);
  Radian & operator- (Radian const & rhs) const ;
  // times a scalar.
  Radian & operator*=(double const rhs);
  Radian & operator* (double const rhs) const ;
  Radian & operator/=(double const rhs);
  Radian & operator/ (double const rhs) const ;
  
  double dbl() const ;
  double magnitude() const;

  // conversion to degrees.
  double degrees() const;
  // to a normal vector
  void normalVector(double & nx, double & ny) const ;
  // to a normal vector.  PointType requires the interface:
  // constructor(double x, double y)
  template<typename VectorType>
  VectorType normalVector()  const ;
  
  // static utility functions
  static double angleMod(double angle);
  static double angleDiff(double angle1, double angle2);
  static double radiansToDegrees(Radian rad);
  static Radian degreesToRadians(double degrees);
  static Radian vectorToAngle(double vx, double vy);
private:

  double m_angle;
};


bool operator< (double const lhs, Radian const & rhs);
bool operator<=(double const lhs, Radian const & rhs);
bool operator> (double const lhs, Radian const & rhs);
bool operator>=(double const lhs, Radian const & rhs);
bool operator==(double const lhs, Radian const & rhs);
bool operator!=(double const lhs, Radian const & rhs);


template<typename VectorType>
VectorType Radian::normalVector() const 
{
  double nx,ny;
  normalVector(nx,ny);
  return VectorType(nx,ny);
}

std::ostream & operator<<(std::ostream & stream, Radian const & rhs);

#endif
