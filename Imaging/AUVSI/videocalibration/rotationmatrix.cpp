// $Id: rotationmatrix.cpp,v 1.1.1.1.2.10 2005/01/23 05:24:37 cvs Exp $
//

using namespace std;

#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "matrix/matrix.h"
#include "rotationmatrix.h"
#include "controlpoints.h"
#include <sstream>
#include "playingfield.h"
#include "calibration.h"


RotationMatrix::RotationMatrix ( string filename )
{
  int myerrno;
  ControlPoints pts;

  rotM = newdmat (0, 2, 0, 2, &myerrno);
  if (myerrno)
    {
      cerr << "RotationMatrix::RotationMatrix unable to allocate rotM\n";
      exit (12);
    }

  rotMi = newdmat (0, 2, 0, 2, &myerrno);

  if (myerrno)
    {
      cerr << "RotationMatrix::RotationMatrix unable to allocate rotMi\n";
      exit (12);
    }

  if (filename == "")
    {
      setToIdentity();
    }
  else
    {
      loadMatrix(filename);
    }



}

RotationMatrix::~RotationMatrix ()
{
  freemat (rotM);
  freemat (rotMi);
}

RotationMatrix::operator  const char *()
{
  stringstream
    out;

  out << rotM.el[0][0] << " " << rotM.el[0][1] << " " << rotM.
    el[0][2] << endl;
  out << rotM.el[1][0] << " " << rotM.el[1][1] << " " << rotM.
    el[1][2] << endl;
  out << '\000';

  return (out.str ().c_str ());
}

void
RotationMatrix::updateInverse ()
{
  matcopy (rotM, rotMi);
  matinvert (rotMi);
};

void
RotationMatrix::rotate (double x, double y, double *ox, double *oy)
{
  *ox = rotM.el[0][0] * x + rotM.el[0][1] * y + rotM.el[0][2];
  *oy = rotM.el[1][0] * x + rotM.el[1][1] * y + rotM.el[1][2];
};

void
RotationMatrix::rotatei (double x, double y, double *ox, double *oy)
{
  *ox = rotMi.el[0][0] * x + rotMi.el[0][1] * y + rotMi.el[0][2];
  *oy = rotMi.el[1][0] * x + rotMi.el[1][1] * y + rotMi.el[1][2];
};

struct ControlPoint *
RotationMatrix::controlPoint (int x, int y)
{
  struct ControlPoint *p;
  p = points.selectPoint (x, y);

  return p;
}

struct ControlPoint *
RotationMatrix::controlPoint (int index)
{
  struct ControlPoint *p = 0;

  if ((index >= 0) && (index < 4))
    {
      p = (ControlPoint *) points.getPointRef (index);
    }
  return p;
}

void
RotationMatrix::computeRotationMatrix (Calibration * calib)
{
#ifdef XXDEBUG
  std::cout << "RotationMatrix::computeRotationMatrix\n";
  std::cout << "Before compute.\n";
  printMatrix();
#endif


  int numControlPoints = points.size();
  int myerrno;

  dmat M, a, b;

  M = newdmat (0, (numControlPoints * 2) - 1, 0, 5, &myerrno);
  if (myerrno)
    {
      cerr << "RotationMatrix::computeMatrix unable to allocate M\n";
      exit (12);
    }

  a = newdmat (0, 5, 0, 0, &myerrno);
  if (myerrno)
    {
      cerr << "RotationMatrix::computeMatrix unable to allocate a\n";
      exit (12);
    }

  b = newdmat (0, (numControlPoints * 2) - 1, 0, 0, &myerrno);
  if (myerrno)
    {
      cerr << "RotationMatrix::computeMatrix unable to allocate b\n";
      exit (12);
    }

  for (int i = 0; i < numControlPoints; i++)
    {
      double tx, ty;
      ControlPoint p = points.getPoint (i);


#ifdef XXDEBUG
      std::cout << "RotationMatrix::computeRotationMatrix (" << p.xw << ',' << p.yw << ")=(" << p.xm << ',' << p.ym << ")\n";
#endif
      calib->imageCoordToWorldCoord (p.xm, p.ym, 0, &tx, &ty);
#ifdef XXDEBUG
      std::cout << "tx,ty: (" << tx << ',' << ty  << ")\n";
#endif
      M.el[2 * i][0] = tx;
      M.el[2 * i][1] = ty;
      M.el[2 * i][2] = 1.0;

      M.el[2 * i][3] = 0.0;
      M.el[2 * i][4] = 0.0;
      M.el[2 * i][5] = 0.0;

      M.el[2 * i + 1][0] = 0.0;
      M.el[2 * i + 1][1] = 0.0;
      M.el[2 * i + 1][2] = 0.0;

      M.el[2 * i + 1][3] = tx;
      M.el[2 * i + 1][4] = ty;
      M.el[2 * i + 1][5] = 1.0;

      b.el[2 * i][0] = points.getPoint(i).xw;
      b.el[2 * i + 1][0] = points.getPoint(i).yw;
    }

  if (solve_system (M, a, b))
    {
      cerr << "RotationMatrix::computeMatrix: unable to solve system  Ma=b\n";
    }

  for (int i = 0; i < 2; i++)
    {
      for (int j = 0; j < 3; j++)
	{
	  rotM.el[i][j] = a.el[i * 3 + j][0];
	}
    }
  rotM.el[2][0] = 0.0;
  rotM.el[2][1] = 0.0;
  rotM.el[2][2] = 1.0;

  updateInverse ();

  freemat (M);
  freemat (b);
  freemat (a);
#ifdef XXDEBUG
  std::cout << "After compute.\n";
  printMatrix();
#endif
}

void
RotationMatrix::setControlPoints (ControlPoints & points)
{
  this->points = points;
  int numControlPoints = points.size();

  for (int i = 0; i < numControlPoints; i++)
    {
      double tx, ty;
      struct ControlPoint * p = points.getPointRef (i);
      // calib->worldCoordToImageCoord (p->Xw, p->Yw, 0, &tx, &ty);
#ifdef XXDEBUG
      std::cout << "RotationMatrix::setControlPoints (" << p->xw << ',' << p->yw << ")=(" << p->xm << ',' << p->ym  << ")\n";
#endif
      p->xm = tx;
      p->ym = ty;
    }

}

void RotationMatrix::printMatrix( void )
{
      for (int i = 0; i < 2; i++)
	{
	  for (int j = 0; j < 3; j++)
	    {
	      std::cout << rotM.el[i][j] << " ";
	    }
	  std::cout << std::endl;
	}
}

void RotationMatrix::loadMatrix(std::string filename)
{
  this->filename = filename;
  FILE *fd = fopen (filename.c_str (), "r");
  if (NULL == fd)
    {
      cerr << "RotationMatrix:: rotationMatrix file error";
      setToIdentity();
      return;
    }
  
#ifdef XXDEBUG
  printf ("ROTATION MATRIX FILE=%s\n", filename.c_str ());
#endif
  
  for (int i = 0; i < 2; i++)
    {
      for (int j = 0; j < 3; j++)
	{
	  if (fscanf (fd, "%lf", &rotM.el[i][j]) != 1)
	    {
	      fprintf (stderr, "Unable to load rotation matrix %d %d\n", i, j);
	      exit (1);
	    }
	}
    }
  fclose (fd);
  
  rotM.el[2][0] = 0.0;
  rotM.el[2][1] = 0.0;
  rotM.el[2][2] = 1.0;
  updateInverse ();
}
void RotationMatrix::saveMatrix(std::string filename)
{
  this->filename = filename;
  FILE *fd = fopen (filename.c_str (), "w");
  if (NULL == fd)
    {
      cerr << "RotationMatrix:: rotationMatrix file error";
      return;
    }
  
#ifdef XXDEBUG
  printf ("ROTATION MATRIX FILE=%s\n", filename.c_str ());
#endif 
  //     fprintf(fd,"matrix ");
  for (int i = 0; i < 2; i++)
    {
      for (int j = 0; j < 3; j++)
	{
	  if (fprintf (fd, "%lf ", rotM.el[i][j]) < 0 )
	    {
	      fprintf (stderr, "Unable to save rotation matrix %d %d\n",
		       i, j);
	      exit (1);
	    }
	}
      //      fprintf(fd,"\n");
    }
  fclose (fd);
}

void RotationMatrix::setToIdentity()
{
  for(uint i = 0; i < 3; i++)
    for(uint j = 0; j < 3; j++)
      if(i == j)
	{
	  rotM.el[i][j] = 1.0;
	  rotMi.el[i][j] = 1.0;
	}
      else
	{
	  rotM.el[i][j] = 0.0;
	  rotMi.el[i][j] = 0.0;
	}

}
