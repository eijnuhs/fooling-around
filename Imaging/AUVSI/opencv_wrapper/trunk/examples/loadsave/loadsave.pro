TEMPLATE	= app
LANGUAGE	= C++

SOURCES	+= main.cpp

#include the umimage gui files.
UMIMAGEGUI = 1

AUTOGUI_LIB_PATH = ../../../autogui
UTILS_INCLUDE_PATH = ../../../utils
CVWRAP_LIB_PATH = ../../

!exists( .firstqmake ){
system(cat ../../README)
system(touch .firstqmake)
}

include( $${CVWRAP_LIB_PATH}/cvwrapinclude.pro  )
!exists( $${CVWRAP_LIB_PATH}/cvwrapinclude.pro ){
  error( OpenCv Wrapper Library not found...It may be checked out from the sculpin cvs.)
}

include( $${AUTOGUI_LIB_PATH}/autoguiinclude.pro)
!exists($${AUTOGUI_LIB_PATH}/autoguiinclude.pro){
  error( AutoGui library not found...It may be checked out from the sculpin cvs.)
}

MACHINENAME = $$(MACHINE)

equals(MACHINENAME, sancho) {
QMAKE_CXXFLAGS = -ggdb -lpthread
} else {
QMAKE_CXXFLAGS = -ggdb -pthread
}

unix {
  UI_DIR = .ui
  MOC_DIR = .moc
  OBJECTS_DIR = .obj
  DEFINES += DEBUG_PTF
}
