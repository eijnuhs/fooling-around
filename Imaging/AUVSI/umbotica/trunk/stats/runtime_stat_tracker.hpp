#ifndef UMBOTICA_STATS_HPP
#define UMBOTICA_STATS_HPP

#include <string>
#include <map>
#include <iostream>
#include <sstream>
#include "../threads/null_mutex.hpp"

#define PRETTY_BAR_WIDTH 40

namespace umbotica { namespace stats {

  // namespace forward declarations
  class RuntimeStat;
  template<typename MUTEX_T>
  class RuntimeStatTracker;

}}

// function prototypes:
template<typename MUTEX_T>
std::ostream & operator<<(std::ostream & stream, umbotica::stats::RuntimeStatTracker<MUTEX_T> const & stat);
std::ostream & operator<<(std::ostream & stream, umbotica::stats::RuntimeStat const & stat);


#ifdef UMBOTICA_NO_STATISTICS

namespace umbotica { namespace stats {

  template<typename MUTEX_T = umbotica::threads::null_mutex>
  class RuntimeStatTracker
  {
  public:
    void add_stat(std::string const & , std::string const & , umbotica::stats::RuntimeStat *){}
  };


  class RuntimeStat
  {
  public:
    template<typename STAT_TRACKER_T>
    RuntimeStat(std::string , std::string , STAT_TRACKER_T * ){}
    RuntimeStat(){};
    virtual ~RuntimeStat(){}
    virtual std::string results() const
    {
      return "No statistics\n";
    }
  };


}} // namespace umbotica::stats

inline std::ostream & operator<<(std::ostream & stream, umbotica::stats::RuntimeStat const & stat)
{
  stream << stat.results();
  return stream;
}

template<typename MUTEX_T>
inline std::ostream & operator<<(std::ostream & stream, umbotica::stats::RuntimeStatTracker<MUTEX_T> const &)
{
  stream << "No RuntimeStats\n";
  return stream;
}



#else // #ifdef UMBOTICA_NO_STASTICS

namespace umbotica { namespace stats {

  template<typename MUTEX_T = umbotica::threads::null_mutex>
  class RuntimeStatTracker
  {
  public:
    typedef MUTEX_T mutex_t;
    typedef typename mutex_t::scoped_lock lock_t;
    RuntimeStatTracker(){}
    virtual ~RuntimeStatTracker(){}

    void add_stat(std::string const & category, 
                  std::string const & statName, 
                  umbotica::stats::RuntimeStat * stat)
    {
      lock_t lock(m_mutex);
      m_stats[category][statName] = stat;
    }  

    friend std::ostream & ::operator<< <MUTEX_T>(std::ostream & stream, umbotica::stats::RuntimeStatTracker<MUTEX_T> const & stat);

  private:
    std::map<std::string, std::map<std::string, umbotica::stats::RuntimeStat * > > m_stats;
    mutable mutex_t m_mutex;
  };


  class RuntimeStat
  {
  public:
    template<class STAT_TRACKER_T>
    RuntimeStat(std::string category,
                 std::string name,
                 STAT_TRACKER_T * statTracker)
    {
      if(statTracker)
	{
	  statTracker->add_stat(category, name, this);
	}
    }

    RuntimeStat(){};
    
    virtual ~RuntimeStat(){}

    virtual std::string results() const = 0;
  };


}} // namespace umbotica::stats

inline std::ostream & operator<<(std::ostream & stream, umbotica::stats::RuntimeStat const & stat)
{
  stream << stat.results();
  return stream;
}


template<typename MUTEX_T>
inline std::ostream & operator<<(std::ostream & stream, umbotica::stats::RuntimeStatTracker<MUTEX_T> const & stat)
{
  typename umbotica::stats::RuntimeStatTracker<MUTEX_T>::lock_t lock(stat.m_mutex);

  // Iterate through the categories outputting the statistics under each.
  std::map<std::string, std::map<std::string, umbotica::stats::RuntimeStat * > >::const_iterator cat_it = stat.m_stats.begin();

  for( ; cat_it != stat.m_stats.end(); cat_it++)
    {
      // output the header.
      std::string title = cat_it->first;
      stream << title << std::endl;
      for(unsigned int i = 0; i < std::max<unsigned int>(PRETTY_BAR_WIDTH,title.size()); i++) stream << "-";

      stream << std::endl;

      // output the contents of the category.
      std::map<std::string, umbotica::stats::RuntimeStat * >::const_iterator cont_it = cat_it->second.begin();
      for( ; cont_it != cat_it->second.end(); cont_it++)
	{
	  stream << cont_it->first << ": " << (*(cont_it->second)) << std::endl << std::endl;
	}

      // output the footer.
      for(unsigned int i = 0; i < std::max<unsigned int>(PRETTY_BAR_WIDTH,title.size()); i++) stream << "-";
      stream << std::endl;

    }

  return stream;
}

#endif // ifdef UMBOTICA_NO_STATISTICS

#endif
