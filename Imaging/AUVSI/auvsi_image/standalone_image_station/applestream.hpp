#ifndef _APPLESTREAM_HPP
#define _APPLESTREAM_HPP

#if defined (__APPLE__)
#include "cameraTool.h"
#include "quicktimeProcessing.hpp"
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/condition.hpp>
#include <umbotica/threads/threadwrapper.hpp>
#include <iostream>
#include <umimage.hpp>
#include <umimagetypedefs.hpp>
#include "boost/filesystem/path.hpp"
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/fstream.hpp"
#include <boost/date_time/posix_time/posix_time.hpp>

class LiveStream : public umbotica::threads::ThreadWrapper
{
public:
  QuickTimeProcessing::image_type image;
  QuickTimeProcessing videoProcessing;
  std::string savePath;
  ImageVector & image_vec;
  
  LiveStream(std::string savePath_, ImageVector & vec) : savePath(savePath_), image_vec(vec)
  {
    if ( !fs::exists(savePath) )
      {
	std::cout << "Creating directory: " << savePath << std::endl;
	fs::create_directory(savePath);
      }
    videoProcessing.initialize();
    videoProcessing.startCamera();
    if (videoProcessing.stillRunning())
      {
	cout << "Camera running." << endl;
      }
  }

  ~LiveStream()
  {
    videoProcessing.stopCamera();
    if (!videoProcessing.stillRunning())
      {
	cout << "Camera stopped." << endl;
      }
  }

  void stopCamera()
  {
    videoProcessing.stopCamera();
  }
  
  void startCamera()
  {
    videoProcessing.startCamera();
  }

  void processing()
  {
    UmImageLocker lock(image);
    image.copyFrom(videoProcessing.getFrame());
    ptime timestamp = microsec_clock::local_time();
    std::string imageName = savePath + "/" + umbotica::replace(to_iso_string(timestamp),'.','_') + ".jpg";
    exportImage(image.upperLeft(), image.lowerRight(), image.rgbAccessor() ,vigra::ImageExportInfo(imageName.c_str()));
    image_vec.push_back(imageName);
  }

};
#endif

#endif
