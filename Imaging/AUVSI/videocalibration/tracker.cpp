#include "tracker.h"
#include "trackerconfig.h"
#include "segmentation.h"
#include "broadcaster.h"
#include <iostream>
#include "ball.h"

#define BACKGROUND_DELAY 240

Tracker::Tracker(TrackerConfig * tcfg) :
  seg(0),
  bcast(0),
  updateBackgroundCount(0),
  theBall(0)
{
  if(tcfg)
    {
      trackable = tcfg->getBotList();
      theBall = tcfg->getBall();

    }
}

Tracker::~Tracker()
{
  for(unsigned int i = 0; i < trackable.size(); ++i)
    {
      delete trackable[i];
    }
}

void Tracker::setSegmentation(Segmentation * init_seg)
{
  seg = init_seg;
  if(seg)
    {
      Trackable::setDebugFrame(&seg->wvFrame);
    }
}


void Tracker::processFrame(FrameBuffer * inFrame)
{
  double wx,wy; // world x and y
  unsigned int lrx,lry; // lo-res x and y
  int regIdx; // region index
  bool found;
  Trackable * current = 0;
  std::vector<unsigned int> unmatched;
  std::vector<unsigned int> regionIdx;
  //  std::cout << "Tracker::processFrame(FrameBuffer * inFrame)\n";

  seg->interpolateFrame(inFrame);
  //  std::cout << "Tracker initial pass\n";
  // before segmenting the frame give objects a chance to track themselves
  // and erase themselves from the interpolated frame so they don't show up
  // in the motion detection.
  for(unsigned int i = 0; i < trackable.size(); ++i)
    {
      current = trackable[i];
      // decrement found count.
      current->initForFrame();
      current->trackMe(inFrame, &seg->ipFrame);
//       if(current->trackMe(inFrame, &seg->ipFrame))
// 	{
// 	  std::cout << current->getName() << " found on initial pass\n";
// 	}
            
    }
  
  // segment the frame.
  //  seg->wvFrame.mutex->lock();
  seg->segmentFrame(inFrame);

//   std::cout << "Region List: \n";
//   for(uint i = 0; i < seg->regionList.size(); ++i)
//     {
//       std::cout << seg->regionList[i];
//     }
  
//  std:: cout << "Tracker: first pass\n";
  // first pass:
  // for each trackable item
  for(unsigned int i = 0; i < trackable.size(); ++i)
    {
      current = trackable[i];
      
     // if it is valid
      if(!current->getFound())
	{
	  if(current->getPercentFound() > 0.0)
	    {
	      //	      std::cout << "Attempting to find " << current->getName() << "\n";
	      // project position based on timestamp
	      current->predictPosition(wx,wy,inFrame->t);
	      // translate the coordinates.
	      seg->segmentedFrame.worldToBufferCoordinates(wx,wy,&lrx,&lry);
	      // check the region that appears in this spot.  
	      regIdx = seg->regionList.getParentRegionIdx(lry,lrx);
	      
	      if(regIdx == -1 ||
		 !current->isMe(seg->regionList[regIdx], inFrame, &seg->segmentedFrame, seg->motionFrameTop.getAvgIntensity(lry,lrx)))
		{
		  unmatched.push_back(i);
		}
// 	      else
// 		{
// 		  std::cout << "Found on first pass: " << *current;
// 		  std::cout << seg->regionList[regIdx];
// 		}
	    }
	  else
	    {
	      unmatched.push_back(i);
	    }      
	}
    }

  //  std:: cout << "Tracker: unmatched: " << unmatched.size() << "\n";

  // second pass:
  // get the indicies of regions that we need to look at
  for(unsigned int i = 0; i < seg->regionList.size(); ++i)
    {
      if(!seg->regionList[i].used)
	{
	  regionIdx.push_back(i);
	  //	  std::cout << "umatched region[" << i << "]: " << seg->regionList[i];
	}
      
    }  

  //  std:: cout << "Tracker: unmatched regions: " << regionIdx.size() << "\n";

  std::vector<Robot *> notFoundBots;
  Robot * r;
  // if there are unmatched trackable objects,
  // run through each trying to find a matching region.
  for(unsigned int i = 0; i < unmatched.size(); ++i)
    {
      found = false;
      for(unsigned int j = 0; j < regionIdx.size() && !found; ++j)
	{
	  if(!seg->regionList[regionIdx[j]].used)
	    {
	      lrx = seg->regionList[regionIdx[j]].getCOMXI();
	      lry = seg->regionList[regionIdx[j]].getCOMYI();
	      found = trackable[unmatched[i]]->isMe(seg->regionList[regionIdx[j]], 
						   inFrame, &seg->segmentedFrame, 
						   seg->motionFrameTop.getAvgIntensity(lry,lrx));
	      // DEBUG
// 	      if ( found )
// 		std::cout << *trackable[unmatched[i]] << " found on second pass: " << seg->regionList[regionIdx[j]] << "\n";

	    }
	}
      if(!found && (r = dynamic_cast<Robot *>(trackable[unmatched[i]])))
	{
	  //	  std::cout << "collected a notFoundBot: " << r->getSubtype() << "\n";
	  notFoundBots.push_back(r);
	}
    }
  //  seg->wvFrame.mutex->unlock();

  // final pass for robots:
  for(uint j = 0; j < regionIdx.size(); ++j)
    Robot::breakUpBigRegion(inFrame, &seg->segmentedFrame, notFoundBots, seg->regionList, regionIdx[j]);


  // this call allows the ball to
  // decide where it is based
  // on data computed in the above passes
  if(theBall)
    theBall->trackingComplete();

  seg->updateWVFrame();
  if(bcast)
    bcast->newTrackable(trackable, inFrame);

  // Disabled until we have a better idea
    
  if(updateBackgroundCount++ >= BACKGROUND_DELAY)
    {
      
      std::vector<Run> unused(seg->regionList.getUnusedRuns());
      seg->updateMotionFrame(unused, inFrame->fieldNo);

      // there are two motion frames (one for the top field of the image
      // one for the bottom field) so we want to update both
      if(updateBackgroundCount > BACKGROUND_DELAY)
	updateBackgroundCount = 0;
    }
  

}

void Tracker::setBroadcaster(Broadcaster * init_bcast)
{
  bcast = init_bcast;
}




