// $Id: framebufferworld.cpp,v 1.1.1.1.2.5 2005/01/23 05:24:37 cvs Exp $
// Jacky Baltes <jacky@cs.umanitoba.ca> Wed Jun  9 12:46:47 CDT 2004
//

#include "framebufferworld.h"
#include "playingfield.h"

FrameBufferWorld::FrameBufferWorld( double mmperpix, PlayingField field ) : 
  FrameBufferRGB32X( ),
  mmPerPix(mmperpix)
{
  initialize( mmperpix, field );
}

FrameBufferWorld::FrameBufferWorld( ) : FrameBufferRGB32X( )
{
  
}

double FrameBufferWorld::getMMPerPixel()
{
  return mmPerPix;
}

void
FrameBufferWorld::initialize( double mmperpix, PlayingField field )
{
  if(mmperpix != 0)
    {
      mmPerPix = mmperpix;
      width = (unsigned int)(field.getFullWidth() / mmperpix);
      height = (unsigned int)(field.getFullHeight() / mmperpix);
      FrameBufferRGB32X::initialize( WORLD_FRAME, width, height );
      memset(buffer, 0x00, frameSize);
      this->field = field;
      xOffset = field.margin / mmPerPix;
      yOffset = field.margin / mmPerPix;
    }
}

bool
FrameBufferWorld::bufferToWorldCoordinates( unsigned int x, unsigned int y, double * xw, double *yw ) const
{
  bool result = true;
  // set (0,0) to the lower left hand corner
  y = this->height - y - 1;

  if ( static_cast<unsigned int>( x ) < width ) 
    {
      if ( static_cast<unsigned int>( y ) < height ) 
	{
	  * xw = ( static_cast<double>( x - xOffset ) * mmPerPix );
	  * yw = ( static_cast<double>( y - yOffset ) * mmPerPix );
	  
	  if ( ( *xw >= -field.margin ) && ( *xw <= field.width + field.margin ) )
	    {
	      if ( ( *yw >= -field.margin ) && ( *yw <= field.width + field.margin ) )
		{
		  result = false;
		}
	    }
	}
    }
  return result;
}

bool
FrameBufferWorld::bufferToWorldCoordinates( double x, double y, double * xw, double *yw ) const
{
  bool result = true;
  // set (0,0) to the lower left hand corner
  y = this->height - y - 1;

  if ( static_cast<unsigned int>( x ) < width ) 
    {
      if ( static_cast<unsigned int>( y ) < height ) 
	{
	  * xw = ( static_cast<double>( x - xOffset ) * mmPerPix );
	  * yw = ( static_cast<double>( y - yOffset ) * mmPerPix );
	  
	  if ( ( *xw >= -field.margin ) && ( *xw <= field.width + field.margin ) )
	    {
	      if ( ( *yw >= -field.margin ) && ( *yw <= field.width + field.margin ) )
		{
		  result = false;
		}
	    }
	}
    }
  return result;
}



bool FrameBufferWorld::worldToBufferCoordinates( double xw, double yw, unsigned int * x, unsigned int *y ) const
{
  bool result = true;
  // set (0,0) to the lower left hand corner
  if ( ( xw >= -field.margin ) && ( xw <= field.width + field.margin ) )
    {
      if ( ( yw >= -field.margin ) && ( yw <= field.height + field.margin ) )
	{
	  *x = static_cast< unsigned int > ( xw / mmPerPix + xOffset );
	  *y = this->height - static_cast< unsigned int > ( yw / mmPerPix + yOffset ) - 1;
	  
	  if ( ( *x < width ) && ( *y < height ) )
	    {
	      result = false;
	    }
	}
    }
  return result;
}



