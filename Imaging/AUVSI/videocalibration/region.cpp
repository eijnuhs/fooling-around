// Region segmentation 
// Jacky Baltes <jacky@cs.umanitoba.ca> Sat May 29 21:16:03 CDT 2004
//
//

#include "region.h"
#include <math.h>
#include <list>
#include "geometry.h"
#include <iostream>
#include "assert.h"
#include "controlpoints.h"

ScanLine::ScanLine( ControlPoint start, ControlPoint end, Pixel pix ) : start( start ), end( end ), color( pix )
{
  mLength = hypot( start.xm - end.xm, start.ym - end.ym );
  wLength = hypot( start.xw - end.xw, start.yw - end.yw );
}

bool
ScanLine::adjacent( ScanLine const & s, double threshold )
{
  bool result = false;

  if ( ( start.ym == end.ym ) && ( s.start.ym == s.end.ym ) )
    {
      if ( fabs( start.ym - s.start.ym ) <= threshold )
	{
	  double x11,x12,x21,x22;

	  if ( start.xm < start.ym )
	    {
	      x11 = start.xm;
	      x12 = end.xm;
	    }
	  else
	    {
	      x11 = end.xm;
	      x12 = start.xm;
	    }

	  if ( s.start.xm < s.start.ym )
	    {
	      x21 = s.start.xm;
	      x22 = s.end.xm;
	    }
	  else
	    {
	      x21 = s.end.xm;
	      x22 = s.start.xm;
	    }
	
	  double left, right;
	  // left = max( x11, x21 )
	  if ( x11 > x21 )
	    {
	      left = x11;
	    }
	  else
	    {
	      left = x21;
	    }
	  
	  // right = min( x21, x22 )
	  if ( x21 < x22 )
	    {
	      right = x21;
	    }
	  else
	    {
	      right = x22;
	    }
	  if ( left <= right )
	    {
	      result = true;
	    }
	  
#ifdef XX_DEBUG
	  std::cout << "ScanLine::adjacent( [" << start.xm << "," << start.ym << "],[" << end.xm << "," << end.ym << "] and [" << s.start.xm << "," << s.start.ym << "],[" << s.end.xm << "," << s.end.ym << "] ) returns " << result << " left " << left << ", right " << right << std::endl;
#endif
	}
    }
  else
    {
      std::cerr << "ScanLine::adjacentFast unable to attach line segment" << std::endl;
    }
  return result;
}


SegmentationRegion::SegmentationRegion( ) : numPoints( 0 ), sumMX( 0.0 ), sumMY( 0.0 ), sumWX( 0.0 ), sumWY( 0.0 ), bboxMin( 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 ), bboxMax( 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 ), center( 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 ), null ( true ), num( 0 )
{
}

SegmentationRegion::SegmentationRegion( ScanLine const s ) : sumWX( 0.0 ), sumWY( 0.0 ), bboxMin( 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 ), bboxMax( 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 ), center( 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 ), null ( false ), num( 0 )
{
  color = s.color;

  numPoints = static_cast< unsigned int > ( s.end.xm - s.start.xm );
  sumMX = s.end.xw * ( s.end.xw + 1 ) / 2.0 - s.start.xw *( s.start.xw - 1 ) / 2.0;
  sumMY = numPoints * s.start.yw;

  double minMX, maxMX;
  Geometry::minMax( s.start.xm, s.end.xm, & minMX, & maxMX );
  double minMY, maxMY;
  Geometry::minMax( s.start.ym, s.end.ym, & minMY, & maxMY );

  double minWX, maxWX;
  Geometry::minMax( s.start.xw, s.end.xw, & minWX, & maxWX );
  double minWY, maxWY;
  Geometry::minMax( s.start.yw, s.end.yw, & minWY, & maxWY );

  bboxMin = ControlPoint( minWX, minWY, 0.0, minMX, minMY, 0.0 );
  bboxMax = ControlPoint( maxWX, maxWY, 0.0, maxMX, maxMY, 0.0 );

  center = ControlPoint( s.start.xw + 0.5 * ( s.end.xw - s.start.xw ), s.start.yw + 0.5 * ( s.end.yw - s.start.yw ), 0.0, s.start.xm + 0.5 * ( s.end.xm - s.start.xm ), s.start.ym + 0.5 * ( s.end.ym - s.start.ym ), 0.0 );
}

void
SegmentationRegion::merge( SegmentationRegion * s2 )
{
  if ( ! s2->null ) 
    {
      Pixel & p1 = color;
      Pixel & p2 = s2->color;
      double n1 = numPoints;
      double n2 = s2->numPoints;
      double areaSum = n1 + n2;
      
      color = p1 * ( n1 / areaSum )  + p2 * ( n2 / areaSum );
      
      double minWX;
      Geometry::minMax( bboxMin.xw, s2->bboxMin.xw, & minWX, (double*)0 );
      double minWY;
      Geometry::minMax( bboxMin.yw, s2->bboxMin.yw, & minWY, (double*)0 );

      double maxWX;
      Geometry::minMax( bboxMax.xw, s2->bboxMax.xw, (double*)0, & maxWX );
      double maxWY;
      Geometry::minMax( bboxMax.yw, s2->bboxMax.yw, (double*)0, & maxWY );
      
      double minMX;
      Geometry::minMax( bboxMin.xm, s2->bboxMin.xm, & minMX, (double*)0 );
      double minMY;
      Geometry::minMax( bboxMin.ym, s2->bboxMin.ym, & minMY, (double*)0 );
      
      double maxMX;
      Geometry::minMax( bboxMax.xm, s2->bboxMax.xm, (double*)0, & maxMX );
      double maxMY;
      Geometry::minMax( bboxMax.ym, s2->bboxMax.ym, (double*)0, & maxMY );
      
      bboxMin = ControlPoint( minWX, minWY, 0.0, minMX, minMY, 0.0 );
      bboxMax = ControlPoint( maxWX, maxWY, 0.0, maxMX, maxMY, 0.0 );
      
      sumWX = sumWX + s2->sumWX;
      sumWY = sumWY + s2->sumWY;

      sumMX = sumMX + s2->sumMX;
      sumMY = sumMY + s2->sumMY;

      numPoints = static_cast< unsigned int > ( areaSum );
      center = ControlPoint( sumWX/areaSum, sumWY/areaSum, 0.0, sumMX/areaSum, sumMY/areaSum, 0.0 );
      
#ifdef XX_DEBUG
      std::cout << "SegmentationRegion::merge of " << area1 / areaSum << "*p1(" << p1.red << "," << p1.green << "," << p1.blue << ") and " << area2 / areaSum << "*p2(" << p2.red << "," << p2.green << "," << p2.blue << ") results in color(" << color.red << "," << color.green << "," << color.blue << std::endl; 
      std::cout << "SegmentationRegion::merge " << numPoints << ", bboxAreaM " << bboxMArea() << std::endl;
#endif
    }
#if 1 
  null = false;
#endif
}

double
SegmentationRegion::bboxMArea( ) const
{
  assert( bboxMax.xm >= bboxMin.xm );
  assert( bboxMax.ym >= bboxMin.ym );

  double xd = xMExtension( );
  double yd = yMExtension( );

  return ( xd ) * ( yd );
}

double
SegmentationRegion::bboxWArea( ) const
{
  assert( bboxMax.xw >= bboxMin.xw );
  assert( bboxMax.yw >= bboxMin.yw );

  double xWExt = xWExtension( );
  double yWExt = yWExtension( );
  
  return ( xWExt ) * ( yWExt );  
}

double
SegmentationRegion::xMExtension( void ) const
{
  return bboxMax.xm - bboxMin.xm + 1;
}

double
SegmentationRegion::yMExtension( void ) const
{
  return bboxMax.ym - bboxMin.ym + 1;
}

double
SegmentationRegion::xWExtension( void ) const
{
  return bboxMax.xw - bboxMin.xw + 1;
}

double
SegmentationRegion::yWExtension( void ) const
{
  return bboxMax.yw - bboxMin.yw + 1;
}

double
SegmentationRegion::mRatio( void ) const
{
  double xe = xMExtension( );
  double ye = yMExtension( );

  return xe/ ye;
}

double
SegmentationRegion::wRatio( void ) const
{
  double xe = xWExtension( );
  double ye = yWExtension( );

  return xe/ye;
}

std::ostream &
operator<<( std::ostream & os, SegmentationRegion & r )
{
  os << "Region at " << r.center << " WExt(" << r.xWExtension() << "," << r.yWExtension() << ", bbox " << r.bboxMin << "," << r.bboxMax;
  return os;
}
