/****************************************************************************
** FileVideoStream meta object code from reading C++ file 'filevideostream.h'
**
** Created: Fri Feb 17 16:07:47 2006
**      by: The Qt MOC ($Id: qt/moc_yacc.cpp   3.3.4   edited Jan 21 18:14 $)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#undef QT_NO_COMPAT
#include "../filevideostream.h"
#include <qmetaobject.h>
#include <qapplication.h>

#include <private/qucomextra_p.h>
#if !defined(Q_MOC_OUTPUT_REVISION) || (Q_MOC_OUTPUT_REVISION != 26)
#error "This file was generated using the moc from 3.3.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

const char *FileVideoStream::className() const
{
    return "FileVideoStream";
}

QMetaObject *FileVideoStream::metaObj = 0;
static QMetaObjectCleanUp cleanUp_FileVideoStream( "FileVideoStream", &FileVideoStream::staticMetaObject );

#ifndef QT_NO_TRANSLATION
QString FileVideoStream::tr( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "FileVideoStream", s, c, QApplication::DefaultCodec );
    else
	return QString::fromLatin1( s );
}
#ifndef QT_NO_TRANSLATION_UTF8
QString FileVideoStream::trUtf8( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "FileVideoStream", s, c, QApplication::UnicodeUTF8 );
    else
	return QString::fromUtf8( s );
}
#endif // QT_NO_TRANSLATION_UTF8

#endif // QT_NO_TRANSLATION

QMetaObject* FileVideoStream::staticMetaObject()
{
    if ( metaObj )
	return metaObj;
    QMetaObject* parentObject = QObject::staticMetaObject();
    static const QUParameter param_signal_0[] = {
	{ "frame", &static_QUType_ptr, "FrameBuffer", QUParameter::In }
    };
    static const QUMethod signal_0 = {"newFrame", 1, param_signal_0 };
    static const QUParameter param_signal_1[] = {
	{ "fps", &static_QUType_double, 0, QUParameter::In }
    };
    static const QUMethod signal_1 = {"fpsUpdate", 1, param_signal_1 };
    static const QMetaData signal_tbl[] = {
	{ "newFrame(FrameBuffer*)", &signal_0, QMetaData::Public },
	{ "fpsUpdate(double)", &signal_1, QMetaData::Public }
    };
    metaObj = QMetaObject::new_metaobject(
	"FileVideoStream", parentObject,
	0, 0,
	signal_tbl, 2,
#ifndef QT_NO_PROPERTIES
	0, 0,
	0, 0,
#endif // QT_NO_PROPERTIES
	0, 0 );
    cleanUp_FileVideoStream.setMetaObject( metaObj );
    return metaObj;
}

void* FileVideoStream::qt_cast( const char* clname )
{
    if ( !qstrcmp( clname, "FileVideoStream" ) )
	return this;
    if ( !qstrcmp( clname, "QThread" ) )
	return (QThread*)this;
    return QObject::qt_cast( clname );
}

#include <qobjectdefs.h>
#include <qsignalslotimp.h>

// SIGNAL newFrame
void FileVideoStream::newFrame( FrameBuffer* t0 )
{
    if ( signalsBlocked() )
	return;
    QConnectionList *clist = receivers( staticMetaObject()->signalOffset() + 0 );
    if ( !clist )
	return;
    QUObject o[2];
    static_QUType_ptr.set(o+1,t0);
    activate_signal( clist, o );
}

// SIGNAL fpsUpdate
void FileVideoStream::fpsUpdate( double t0 )
{
    activate_signal( staticMetaObject()->signalOffset() + 1, t0 );
}

bool FileVideoStream::qt_invoke( int _id, QUObject* _o )
{
    return QObject::qt_invoke(_id,_o);
}

bool FileVideoStream::qt_emit( int _id, QUObject* _o )
{
    switch ( _id - staticMetaObject()->signalOffset() ) {
    case 0: newFrame((FrameBuffer*)static_QUType_ptr.get(_o+1)); break;
    case 1: fpsUpdate((double)static_QUType_double.get(_o+1)); break;
    default:
	return QObject::qt_emit(_id,_o);
    }
    return TRUE;
}
#ifndef QT_NO_PROPERTIES

bool FileVideoStream::qt_property( int id, int f, QVariant* v)
{
    return QObject::qt_property( id, f, v);
}

bool FileVideoStream::qt_static_property( QObject* , int , int , QVariant* ){ return FALSE; }
#endif // QT_NO_PROPERTIES
