// $Id: videoviewer.cpp,v 1.1.1.1.2.3 2004/12/28 19:30:05 cvs Exp $
// A QT widget that displays various video information including normalized
// RGB
// Jacky Baltes <jacky@cs.umanitoba.ca> Tue Oct 21 21:46:10 CDT 2003

#include "videoviewer.h"
#include "framebufferviewer.h"
#include <qwidget.h>
#include <qcombobox.h>
#include <iostream>
#include <qcheckbox.h>
#include "framebufferrgb565.h"
#include "imageprocessing.h"

VideoViewer::VideoViewer( QWidget * parent, char const * name ) : FrameBufferViewer( parent, name )
{
}

void VideoViewer::updateQImageFromFrameBuffer( FrameBuffer * inFrame )
{
  if(true)
    {
      FrameBufferViewer::updateQImageFromFrameBuffer( inFrame );
      return;
    }

  QString mode = mainWindow->comboBox2->currentText();
  Pixel min = inFrame->getMinimum( );
  Pixel max = inFrame->getMaximum( );

  FrameBufferRGB565 frame = * ( ( FrameBufferRGB565 * ) inFrame );
  frame.owner = false;

  bool subsampled = false;

  if ( mainWindow->checkBox4_2->isChecked() )
    {
      frame.bytesPerPixel = inFrame->bytesPerPixel * 4;
      frame.bytesPerLine = inFrame->bytesPerLine * 4;
      frame.width = inFrame->width / 4;
      frame.height = inFrame->height / 4;
      subsampled = true;
    }

  if ( mode == "RGB" )
    {
      FrameBufferViewer::updateQImageFromFrameBuffer( &frame );
    }
  else
    {
      Pixel pixel;

      if ( ( frame.type == FrameBuffer::RGB32 ) || ( frame.type == FrameBuffer::RGB565 ) )
	{
	  checkImageWidthAndHeight( & frame );
	  FrameBufferIterator fIter( & frame );
	  for( unsigned int i = 0; i < frame.height; i++ )
	    {
	      uint32_t * p = (uint32_t *) image->scanLine( i );
	      
	      fIter.goRow( i );
	      for( unsigned int j = 0; j < frame.width; j++, fIter.goRight() )
		{
		  if ( mainWindow->checkBox4->isChecked() && ( j > 0 ) && ( j <  frame.width - 1 ) && ( i > 0 ) && ( i < frame.height - 1 ) )
		    {
		      fIter.getBlurred3x3Pixel( & pixel );
		    }
		  else
		    {
		      fIter.getPixel( & pixel );
		    }
		  if ( mode == "NRGB" )
		    {
		      *p++ = qRgb( pixel.red_ratio, pixel.green_ratio, pixel.blue_ratio );
		    }
		  else if ( mode == "DRGB" )
		    {
		      *p++ = qRgb( ( pixel.red_green + 255 ) / 2, ( pixel.red_blue + 255 ) / 2, ( pixel.green_blue + 255 ) / 2 );
		    }
		  else if ( mode == "HSI" )
		    {
		      double hue = pixel.hue( );
		      RawPixel s( hue );
		      *p++ = qRgb( s.red, s.green, s.blue );
		    }
		  else if ( mode == "CRGB" )
		    {
		      int red = ( pixel.red * 255 ) / ( max.red - min.red + 1);
		      int green = ( pixel.green * 255 ) / ( max.green - min.green + 1);
		      int blue = ( pixel.blue * 255 ) / ( max.blue - min.blue + 1);
		      *p++ = qRgb( red, green, blue );
		    }
		  else if ( mode == "Gray" )
		    {
		      *p++ = qRgb( static_cast< int > ( pixel.intensity( ) ), static_cast< int > ( pixel.intensity( ) ), static_cast< int > (  pixel.intensity( ) ) );
		    }
		  else if ( mode == "Convol" )
		    {
		      if ( ( j > 2 ) && ( j <  frame.width - 3 ) && ( i > 2 ) && ( i < frame.height - 3 ) )
			{
			  double convolutionMatrix[ 5 ][ 5 ] =
			    {
			      {  0.00,  0.00,  0.00,  0.00,  0.00 },
			      {  0.00,  0.33,  0.33,  0.33,  0.00 },
			      {  0.00,  0.33,  0.33,  0.33,  0.00 },
			      {  0.00,  0.33,  0.33,  0.33,  0.00 },
			      {  0.00,  0.00,  0.00,  0.00,  0.00 }
			    };

			  RawPixel s;
			  fIter.convolution5x5( convolutionMatrix, 1, 0, & s );
			  *p++ = qRgb( s.red, s.green, s.blue );
			}
		      else
			{
			  *p++ = qRgb( 0, 0, 0 );
			}
		    }
		  else if ( mode == "Mosaic" )
		    {
		      if ( ( j > 2 ) && ( j <  frame.width - 3 ) && ( i > 2 ) && ( i < frame.height - 3 ) )
			{
			  RawPixel s;
			  if ( subsampled )
			    {
			      inFrame->getMosaicedPixel( i * 4, j * 4, & s  );
			    }
			  else
			    {
			      inFrame->getMosaicedPixel( i, j, & s  );
			    }
			  *p++ = qRgb( s.red, s.green, s.blue );
			}
		      else
			{
			  *p++ = qRgb( 0, 0, 0 );
			}
		    }
		  else if ( mode == "Median" )
		    {
		      if ( ( j > 1 ) && ( j <  frame.width - 1 ) && ( i > 1 ) && ( i < frame.height - 1 ) )
			{
			  fIter.getMedianPixel( static_cast<RawPixel *> ( & pixel ) );
			  *p++ = qRgb( pixel.red, pixel.green, pixel.blue );
			}
		      else
			{
			  *p++ = qRgb( 0, 0, 0 );
			}
		    }
		  else if ( mode == "Roberts" )
		    {
		      if ( ( i > 1 ) && ( j > 1 ) )
			{
			  RawPixel tl, t, bl;
			  fIter.getPixel( & tl, -frame.bytesPerLine - frame.bytesPerPixel );
			  fIter.getPixel( & t, -frame.bytesPerLine );
			  fIter.getPixel( & bl, -frame.bytesPerPixel );

			  RawPixel r1 = ( ( tl * -1.0 ) + pixel ) / 2.0;
			  RawPixel r2 = ( ( bl * -1.0 ) + t ) / 2.0;
			  RawPixel r3 = r1 + r2;
			  *p++ = qRgb( r3.red, r3.green, r3.blue );
			}
		      else
			{
			  *p++ = qRgb( pixel.red, pixel.green, pixel.blue );
			}
		    }
		  else if ( mode == "Max" )
		    {
		      *p++ = qRgb( pixel.max( ), pixel.max( ), pixel.max( ) );
		    }
		  else if ( mode == "Max Filter" )
		    {
		      if ( ( j > 2 ) && ( j <  frame.width - 3 ) && ( i > 2 ) && ( i < frame.height - 3 ) )
			{
			  double convolutionMatrix[ 5 ][ 5 ] =
			    {
			      {  0.00,  0.00,  0.00,  0.00,  0.00 },
			      {  0.00,  0.00,  1.00,  0.00,  0.00 },
			      {  0.00,  0.00,  1.00,  0.00,  0.00 },
			      {  0.00,  0.00,  0.00,  0.00,  0.00 },
			      {  0.00,  0.00,  0.00,  0.00,  0.00 }
			    };

			  RawPixel s;
			  fIter.convolution5x5( convolutionMatrix, 2.0, 0, & s );
			  *p++ = qRgb( s.red, s.green, s.blue );
			}
		    }
		  else if ( mode == "Edge Remove" )
		    {
		      Pixel pPixel = pixel;

		      if ( j > 1 )
			{
			  fIter.getPixel( & pPixel, - frame.bytesPerPixel );
			}

		      double diff = pixel.diffMax( pPixel );
		      if ( diff > 5000 )
			{
			  pixel = pPixel;
			}
		      *p++ = qRgb( pixel.max( ), pixel.max( ), pixel.max( ) );
		      if ( diff < 2500 )
			{
			  pPixel = pixel;
			}
		    }
		  else if ( mode == "Features" )
		    {
		      FeatureDetector edgeInfo;

		      RawPixel pixels[ 3 ][ 3 ];

		      if ( ( j > 1 ) && ( j < frame.width -1 ) && ( i > 1 ) && ( i < frame.height - 1 ) )
			{
			  fIter.getPixel( & pixels[ 0 ][ 0 ],  - frame.bytesPerLine - frame.bytesPerPixel );
			  fIter.getPixel( & pixels[ 0 ][ 1 ],    - frame.bytesPerLine );
			  fIter.getPixel( & pixels[ 0 ][ 2 ], - frame.bytesPerLine + frame.bytesPerPixel );
			  fIter.getPixel( & pixels[ 1 ][ 0 ],                        - frame.bytesPerPixel );
			  pixels[ 1 ][ 1 ] = pixel;
			  fIter.getPixel( & pixels[ 1 ][ 2 ],                       + frame.bytesPerPixel );
			  fIter.getPixel( & pixels[ 2 ][ 0 ],  + frame.bytesPerLine - frame.bytesPerPixel );
			  fIter.getPixel( & pixels[ 2 ][ 1 ],  + frame.bytesPerLine );
			  fIter.getPixel( & pixels[ 2 ][ 2 ],  + frame.bytesPerLine + frame.bytesPerPixel );
			}
		      double threshold = 2000.0;
		      enum ImageProcessing::FeatureType f = ImageProcessing::classifyFeature( pixels, threshold, 0 );
		      RawPixel spixel;
		      switch( f )
			{
			case ImageProcessing::NONE: 
			  spixel = Pixel( pixel.max(), pixel.max(), pixel.max() );
			  break;
			case ImageProcessing::VERT_UP:
			  spixel = Pixel( 255, 0, 0 );
			  break;
			case ImageProcessing::VERT_DOWN:
			  spixel = Pixel( 128, 0, 0 );
			  break;
			case ImageProcessing::HORIZ_UP:
			  spixel = Pixel( 0, 255, 0 );
			  break;
			case ImageProcessing::HORIZ_DOWN:
			  spixel = Pixel( 0, 128, 0 );
			  break;
			case ImageProcessing::ANGLE45_UP:
			  spixel = Pixel( 0, 0, 255 );
			  break;
			case ImageProcessing::ANGLE45_DOWN:
			  spixel = Pixel( 0, 0, 128 );
			  break;
			case ImageProcessing::ANGLE135_UP:
			  spixel = Pixel( 0, 255, 255 );
			  break;
			case ImageProcessing::ANGLE135_DOWN:
			  spixel = Pixel( 0, 128, 128 );
			  break;
			}
		      *p++ = qRgb( spixel.red, spixel.green, spixel.blue );
		    }
		}
	    }
	  //	  std::cerr << "VideoViewer::updateQImageFromFrameBuffer: red " << max.red << "-" <<  min.red << " green " << max.green << "-" <<  min.green << " blue " << max.blue << "-" << min.blue << std::endl;
	}
      else
	{
	  std::cerr << "VideoViewer::updateQImageFromFrameBuffer: frame.bytesPerPixel " << frame.bytesPerPixel << " type " << frame.type << " is not supported\n";
	  if ( image != 0 )
	    {
	      delete image;
	    }
	  image = 0;
	}
    }
}



void VideoViewer::printName()
{
  std::cout << "VideoViewer\n";
}
