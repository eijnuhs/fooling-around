#ifndef _LOCALREGRESSION_H
#define _LOCALREGRESSION_H

#include <vigra/matrix.hxx>
#include <vigra/array_vector.hxx>
#include <vigra/linear_algebra.hxx>
#include <vector>
#include <cmath>

using namespace vigra::linalg;

//////////////////////////////////////////////
// Class TrainingExample
//  - Holds data for use in the linear
//    regression smoother
//////////////////////////////////////////////
template <typename X, typename Y>
class TrainingExample
{
public:
  TrainingExample(X _x, Y _y) : x(_x), y(_y) { }
  ~TrainingExample() { }
  X x;
  Y y;
};


template <typename X, typename Y>
class LRSmoother
{
public:
  /////////////////////////////////////////////
  // Smoother Constructor
  // Accepts the degree of the smoothing
  // polynomial
  /////////////////////////////////////////////
  LRSmoother(int _degree) : degree(_degree) { };
  ~LRSmoother() { };

  /////////////////////////////////////////////
  // Calculates a polynomial of best fit
  // and returns its value at the given target
  /////////////////////////////////////////////
  inline Y getSmoothed(X target, std::vector<TrainingExample<X,Y> > examples);

private:
  int degree;
  inline double kernel(X target, X example);
};

template<typename X, typename Y>
Y LRSmoother<X, Y>::getSmoothed(X target, std::vector<TrainingExample<X,Y> > examples)
{
  Y returnVal;
  vigra::Matrix<double> u(examples.size(), degree+1);
  for (int i = 0; i < (int)(examples.size()); i++)
    {
      for (int j = 0; j < degree+1; j++)
	{
	  u(i,j) = (double)(pow(examples[i].x, j));
	}
    }
  vigra::Matrix<double> y(examples.size(), 1);
  for (int i = 0; i < (int)(examples.size()); i++)
    {
      y(i, 0) = (double)(examples[i].y);
    }
  vigra::Matrix<double> k(examples.size(), examples.size());
  for (int i = 0; i < (int)(examples.size()); i++)
    {
      k(i,i) = sqrt(kernel(examples[i].x, target));
      //std::cout << "kernel of (" << target << ", " << examples[i].x << "): ";
      //std::cout << k(i,i) << "\n";
    }

  TemporaryMatrix<double> ku = mmul(k, u);
  TemporaryMatrix<double> ky = mmul(k, y);
  
  TemporaryMatrix<double> kut = transpose(ku);
  TemporaryMatrix<double> kutku = mmul(kut, ku);
  TemporaryMatrix<double> b = mmul(kut, ky);

  Matrix<double> res(rowCount(b), 1);

  linearSolve(kutku, b, res);
  
  returnVal = 0;
  for (int i = 0; i < degree+1; i++)
    {
      returnVal += (Y)(res(i, 0) * pow(target, i));
    }

  return returnVal;
}

template<typename X, typename Y>
double LRSmoother<X, Y>::kernel(X target, X example)
{
  double distance = fabs(target - example);
  
  return (1.0 / pow(M_E , 150*distance * 150*distance));
}

#endif
