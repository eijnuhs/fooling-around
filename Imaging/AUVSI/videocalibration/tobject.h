#ifndef _TOBJECT_H_
#define _TOBJECT_H_

#include "framebufferrgb32.h"
#include "run.h"
#include <string>
#include "worldregion.h"

class TObject : public Trackable 
{
 public:
  TObject(std::string name, double height, double radius, double mmPerPix, enum Trackable::TrackableTypes classtype = Trackable::TOBJECT);
  ~TObject();
  
  // methods required by trackable.
  bool isMe(RunRegion & region, FrameBuffer * inFrame, FrameBufferWorld * worldFrame, unsigned int avgIntensity);
  bool isMe(RunRegion & region, FrameBuffer * inFrame, FrameBufferWorld * worldFrame, unsigned int avgIntensity, bool updatePos);
  bool trackMe(FrameBuffer * inFrame, FrameBufferWorld * worldFrame);

  void output (std::ostream & os);
  
 protected:
  // attempts to use the scratchpad to find the tobject at
  // this location. wx, wy and region must be objects
  // passed in
  bool findMe(FrameBuffer * inFrame, double pixelDim, double * wx, double * wy, WorldRegion * theRegion = 0);
 private:
  RegionList regionList;
};
#endif

