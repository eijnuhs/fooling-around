/*!
    @header		serialReader.h
    @abstract   C functions for reading serial data from Micropilot GCS
*/

#ifndef _SERIALREADER_H
#define _SERIALREADER_H

#ifdef __CPLUSPLUS
extern "C"
{
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <strings.h>
#include <unistd.h>

/*!
    @defined	ESCAPE_CHAR
    @abstract   Escape character in GCS protocol
*/
#define ESCAPE_CHAR       ( 0xfd ) // should be 0xfd

/*!
    @defined	ESCAPE_X
	@abstract   Escaped bytes
	@discussion If found following ESCAPE_CHAR, they will be replaced with the equivilant CONVERT_X
*/
#define ESCAPE_0          ( 0x00 )
#define ESCAPE_1          ( 0x01 )
#define ESCAPE_2          ( 0x02 )
	
/*!
	@defined	CONVERT_X
	@abstract   Converted bytes
	@discussion Replacements for ESCAPE_X bytes
*/
#define CONVERT_0         ( 0xfd )
#define CONVERT_1         ( 0xfe )
#define CONVERT_2         ( 0xff )
	
/*!
	@defined	START_OF_PACKET
	@abstract   Reserved start-of-packet byte
*/
#define START_OF_PACKET   ( 0xff ) // should be 0xff

/*!
	@defined	END_OF_PACKET
	@abstract   Reserved end-of-packet byte
*/
#define END_OF_PACKET     ( 0xfe ) // should be 0xfe
	
/*!
	@defined	RESPONSE_OK
	@abstract   Indicates an OK response
*/
#define RESPONSE_OK       ( 0x00 ) // should be 0x00

/*!
    @struct		ModemParams
    @abstract   Modem parameters
    @discussion Holds parameters to pass to openModem()
    @field      baudrate A standard baud rate defined in sys/termios.h
	@field		modemDevice Name of the device to open
*/
typedef struct
{
	int baudrate;
	char modemDevice[32];
} ModemParams;

/*!
    @struct		Modem
    @abstract   Holds modem state information
    @discussion Passed to the openModem() and closeModem() calls. Will be filled
				with state info during the open() call. Pass the same Modem struct
				to the closeModem() call to restore device to its original state.
    @field      fd The file descriptor.
	@field		oldtio Stores the original termios struct while device is open
	@field		newtio The customized termios struct
*/
typedef struct
{
	int fd;
	struct termios oldtio;
	struct termios newtio;
} Modem;

/*!
    @function   openModem
    @abstract   Opens a modem device
    @discussion Opens a modem device using the specifed parameters
    @param      modem Pointer to a Modem struct that will be filled with state information
	@param		params ModemParams struct indicating file name and baud-rate
    @result     Returns 1 if open was successful, -1 otherwise.
*/

int openModem(Modem * modem, ModemParams params);
/*!
    @function   readMessage
    @abstract   Reads packet from serial stream into the passed buffer
    @discussion Will start reading at next START_OF_PACKET byte and will read until either
				END_OF_PACKET byte is encountered, or the passed maxLength is exceeded.
    @param      modem Pointer to a Modem struct that has previously been passed to openModem()
	@param		buffer Pointer to a char array to fill with the serial input
	@param		maxLength The maximum number of bytes to read before returning.
    @result     Returns the number of bytes stored in buffer.
*/

int readMessage(Modem const* modem, unsigned char * buffer, int maxLength);
/*!
    @function   closeModem
    @abstract   Closes modem and restores default state
    @discussion Will close the file descriptor in the passed Modem struct, and restore the
				system termios struct to its original state.
    @param      modem A pointer to a Modem struct opened previously with openModem()
    @result     Returns 0 if successful, otherwise, returns the error code resulting from the system close() call.
*/
int closeModem(Modem * modem);

#ifdef __CPLUSPLUS
}
#endif

#endif
