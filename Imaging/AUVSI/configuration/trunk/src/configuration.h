#ifndef __CONFIGURATION_H_
#define __CONFIGURATION_H_
#include <string>
#include <vector>
#include <qobject.h>
#include <dotconf++/dotconfpp.h>
#include <boost/lexical_cast.hpp>
#include <iostream>

// typedef to avoid typing
typedef const DOTCONFDocumentNode Node;

// pure virtual superclass that allows access to 
// a static configuration object.  Subclasses will
// have access to the root node of their name.
class Configuration
{
 public:
  Configuration(std::string init_name = "No_Name");
  virtual ~Configuration();
  
  // initializes the static config object with the specified file
  static bool init(std::string  filename); 
  
  // methods used to safely get a value with a type.
  int getInt(std::string  name, int defaultValue, int base = 10, bool * success = 0);
  std::string getString(std::string  name, std::string defaultValue, bool * success = 0);
  unsigned int getUnsigned(std::string  name, unsigned int defaultValue, int base = 10, bool * success = 0);
  double getDouble(std::string  name, double defaultValue, bool * success = 0);  

  template<typename T>
  T get(std::string name, T defaultValue, bool * success = 0);

 protected:
  // gets the section root node
  Node * getRootNode();
  // gets the requested branch.
  Node * getNode(std::string name);
  Node * getNode(std::string name, std::string rootName);
  Node * getNode(std::string name, Node * parent);
  // allows configuration objects to get other top level objects.
  Node * getRootNode(std::string name);
  int getInt(Node * node, int defaultValue, int base = 10, bool * success = 0);
  std::string getString(Node * node, std::string defaultValue, bool * success = 0);
  unsigned int getUnsigned(Node * node, unsigned int defaultValue, int base = 10, bool * success = 0);
  double getDouble(Node * node, double defaultValue, bool * success = 0);
  
  // gets a list of nodes with parent "parent" (or root if null) 
  // that match the name.
  std::vector<Node *> getNodeList(std::string name, Node * parent = 0);

  ////// Static Members //////////
  // the static configuration object
  static DOTCONFDocument * config;

 private:
  std::string name;

};


// a templated get function that will reduce code and offer better error messages.
template<typename T>
T Configuration::get(std::string name, T defaultValue, bool * success)
{
  Node * node = getNode(name);
  T result = defaultValue;
  bool completed = false;
  if(!node)
    {
      std::cerr << "Configuration error:  No node named \"" << name << "\".\n";
    }
  else
    {
      char const * val = node->getValue();
      if(!val)
	{
	  std::cerr << "Configuration error: No value for node \"" << name << "\".\n";
	}
      else
	{
	  try
	    {
	      result = boost::lexical_cast<T>(val);
	      completed = true;
	    }
	  catch(const boost::bad_lexical_cast & e)
	    {
	      std::cerr << "Configuration error: Unable to cast value. Node: \"" << name << "\", value: \"" << val << "\".\n";
	    }
	}
    }
  
  if(success)
    {
      *success = completed;
    }
  
  if(!completed)
    {
      if(node)
	std::cerr << this->name << " unable to get value from " << node->getName() << "\n";
    }

  return result;

}

#endif
