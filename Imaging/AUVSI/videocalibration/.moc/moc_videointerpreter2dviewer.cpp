/****************************************************************************
** VideoInterpreter2DViewer meta object code from reading C++ file 'videointerpreter2dviewer.h'
**
** Created: Fri Feb 17 15:31:10 2006
**      by: The Qt MOC ($Id: qt/moc_yacc.cpp   3.3.4   edited Jan 21 18:14 $)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#undef QT_NO_COMPAT
#include "../videointerpreter2dviewer.h"
#include <qmetaobject.h>
#include <qapplication.h>

#include <private/qucomextra_p.h>
#if !defined(Q_MOC_OUTPUT_REVISION) || (Q_MOC_OUTPUT_REVISION != 26)
#error "This file was generated using the moc from 3.3.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

const char *VideoInterpreter2DViewer::className() const
{
    return "VideoInterpreter2DViewer";
}

QMetaObject *VideoInterpreter2DViewer::metaObj = 0;
static QMetaObjectCleanUp cleanUp_VideoInterpreter2DViewer( "VideoInterpreter2DViewer", &VideoInterpreter2DViewer::staticMetaObject );

#ifndef QT_NO_TRANSLATION
QString VideoInterpreter2DViewer::tr( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "VideoInterpreter2DViewer", s, c, QApplication::DefaultCodec );
    else
	return QString::fromLatin1( s );
}
#ifndef QT_NO_TRANSLATION_UTF8
QString VideoInterpreter2DViewer::trUtf8( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "VideoInterpreter2DViewer", s, c, QApplication::UnicodeUTF8 );
    else
	return QString::fromUtf8( s );
}
#endif // QT_NO_TRANSLATION_UTF8

#endif // QT_NO_TRANSLATION

QMetaObject* VideoInterpreter2DViewer::staticMetaObject()
{
    if ( metaObj )
	return metaObj;
    QMetaObject* parentObject = FrameBufferViewer::staticMetaObject();
    static const QUParameter param_slot_0[] = {
	{ 0, &static_QUType_ptr, "FrameBuffer", QUParameter::In }
    };
    static const QUMethod slot_0 = {"newFrame", 1, param_slot_0 };
    static const QUMethod slot_1 = {"loadRotation", 0, 0 };
    static const QUMethod slot_2 = {"saveRotation", 0, 0 };
    static const QUMethod slot_3 = {"resetControlPoints", 0, 0 };
    static const QMetaData slot_tbl[] = {
	{ "newFrame(FrameBuffer*)", &slot_0, QMetaData::Public },
	{ "loadRotation()", &slot_1, QMetaData::Public },
	{ "saveRotation()", &slot_2, QMetaData::Public },
	{ "resetControlPoints()", &slot_3, QMetaData::Public }
    };
    metaObj = QMetaObject::new_metaobject(
	"VideoInterpreter2DViewer", parentObject,
	slot_tbl, 4,
	0, 0,
#ifndef QT_NO_PROPERTIES
	0, 0,
	0, 0,
#endif // QT_NO_PROPERTIES
	0, 0 );
    cleanUp_VideoInterpreter2DViewer.setMetaObject( metaObj );
    return metaObj;
}

void* VideoInterpreter2DViewer::qt_cast( const char* clname )
{
    if ( !qstrcmp( clname, "VideoInterpreter2DViewer" ) )
	return this;
    return FrameBufferViewer::qt_cast( clname );
}

bool VideoInterpreter2DViewer::qt_invoke( int _id, QUObject* _o )
{
    switch ( _id - staticMetaObject()->slotOffset() ) {
    case 0: newFrame((FrameBuffer*)static_QUType_ptr.get(_o+1)); break;
    case 1: loadRotation(); break;
    case 2: saveRotation(); break;
    case 3: resetControlPoints(); break;
    default:
	return FrameBufferViewer::qt_invoke( _id, _o );
    }
    return TRUE;
}

bool VideoInterpreter2DViewer::qt_emit( int _id, QUObject* _o )
{
    return FrameBufferViewer::qt_emit(_id,_o);
}
#ifndef QT_NO_PROPERTIES

bool VideoInterpreter2DViewer::qt_property( int id, int f, QVariant* v)
{
    return FrameBufferViewer::qt_property( id, f, v);
}

bool VideoInterpreter2DViewer::qt_static_property( QObject* , int , int , QVariant* ){ return FALSE; }
#endif // QT_NO_PROPERTIES
