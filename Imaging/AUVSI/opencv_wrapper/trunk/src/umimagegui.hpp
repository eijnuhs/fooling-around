#ifndef _UM_IMAGE_GUI_H_
#define _UM_IMAGE_GUI_H_

#include <qimage.h>
#include "umimage.hpp"
#include "agmanager.hpp"
#include "umaccessors.hpp"
#include <boost/thread/mutex.hpp>
#include <umbotica/io/tostring.hpp>
#include "umimagelocker.hpp"
#include "umimageagcontainer.hpp"
//#include <agimagecontainer.hpp>
#include <boost/shared_ptr.hpp>
using umbotica::s_tuple;

template<class ImageType>
struct Updator{
  typedef typename ImageType::traverser iterator;
  typedef typename iterator::difference_type difference_type;
  typedef typename ImageType::RGBAccessor RGBAccessor;
  typedef typename ImageType::PixelType pixel_type;

  Updator(ImageType & image) : m_image(image)
  {
    m_container.reset(new UmImageAgContainer<pixel_type>());
    m_container->setImage(m_image);      
  }
  
  boost::shared_ptr<AGImageContainer> operator()()
  {
    UmImageLocker lock(m_image);
    m_container->setImage(m_image);
    return boost::dynamic_pointer_cast<AGImageContainer>(m_container);
  }
  
  boost::shared_ptr<UmImageAgContainer<pixel_type> > m_container;
  ImageType & m_image;
};

template<class PIXELTYPE>
inline void addToGui(AGManager & gui, UmImage<PIXELTYPE> & image, boost::function<std::vector<AGShape>()> shapeCallback, 
		     boost::function<void(AGPoint,int)> mouseEventHandler,
		     std::string name, int updateMs, bool showName = false)
{
  //  std::cout << "add to gui Image: " << (void *)&image << "\n";
  Updator<UmImage<PIXELTYPE> > upper(image);
  gui.pushGroup(name, showName);
  gui.addFrameBuffer(name, upper, shapeCallback, mouseEventHandler, updateMs); 
  gui.popGroup();
}



template<class PIXELTYPE>
inline void addToGui(AGManager & gui, UmImage<PIXELTYPE> & image, std::string name, int updateMs, bool showName = false)
{
  addToGui(gui,image, boost::function<std::vector<AGShape>()>(), boost::function<void(AGPoint,int)>(), name, updateMs, showName);
}

template<class PIXELTYPE>
inline void addToGui(AGManager & gui, UmImage<PIXELTYPE> & image, boost::function<std::vector<AGShape>()> shapeCallback, std::string name, int updateMs, bool showName = false)
{
  addToGui(gui,image, shapeCallback, boost::function<void(AGPoint,int)>(), name, updateMs, showName);
}


template<class PIXELTYPE>
inline void addToGuiMouse(AGManager & gui, UmImage<PIXELTYPE> & image, boost::function<void(AGPoint,int)> mouseEventHandler , std::string name, int updateMs, bool showName = false)
{
  addToGui(gui,image, boost::function<std::vector<AGShape>()>(), mouseEventHandler, name, updateMs, showName);
}



#endif
