#include "serialReader.h"

int openModem( Modem * modem, ModemParams params )
{
  
  int openSuccess;
  
  openSuccess = 1;
  
  printf("openning modem\n");
#if defined(__APPLE__)
  modem->fd = open(params.modemDevice, O_RDONLY | O_NOCTTY | O_NONBLOCK );
#else
  modem->fd = open(params.modemDevice, O_RDONLY | O_NOCTTY );
#endif
  printf("done\n");
  if (modem->fd < 0)
    {
      perror(params.modemDevice);
      openSuccess = -1;
    }
  else
    {
      tcgetattr(modem->fd, &(modem->oldtio));
      
      bzero(&(modem->newtio), sizeof(modem->newtio));
#if defined(__APPLE__)
      cfmakeraw(&(modem->newtio));
      cfsetspeed(&(modem->newtio), params.baudrate);
#endif
      modem->newtio.c_cflag = params.baudrate | CS8 | CLOCAL | CREAD;
      modem->newtio.c_iflag = IGNPAR;
      modem->newtio.c_oflag = 0;
      modem->newtio.c_lflag = 0;
      
      modem->newtio.c_cc[VTIME] = 0;
      modem->newtio.c_cc[VMIN] = 1;
      
      tcflush(modem->fd, TCIFLUSH);
      if ( tcsetattr(modem->fd, TCSANOW, &(modem->newtio)) )
	{
	  perror("Error setting modem attributes");
	}
    }
  
  return openSuccess;
}

int readMessage( Modem const *modem, unsigned char * buffer, int maxLength )
{
  int buffIndex;
  unsigned char c;

  do
    {
      if (read(modem->fd, &c, 1) == 1)
	{ 
	  printf("not SOP: %x\n", c);
	}
      else
	{
	  printf("no data on line...\n");
	}
    }
  while (c != START_OF_PACKET);

  //printf("START_OF_PACKET\n");

  buffIndex = 0;
  do
    {
      buffer[buffIndex++] = c;
      read(modem->fd, &c, 1);
      //printf("<< %x\n", c);
    }
  while ((buffIndex < maxLength) && (c != END_OF_PACKET));

  if (buffIndex < maxLength)
    {
      buffer[buffIndex++] = c;
    }

  //printf("END_OF_PACKET\n");
  
  return buffIndex;
}

int closeModem( Modem * modem )
{
  int closeSuccess;

  tcsetattr(modem->fd, TCSANOW, &(modem->oldtio));
  closeSuccess = close(modem->fd);
  modem->fd = 0;

  return closeSuccess;
}
