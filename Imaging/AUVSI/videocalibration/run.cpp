#include "run.h"
#include "framebuffer.h"
#include <math.h>

Run::Run(unsigned int row, unsigned int col,
	 RawPixel colour, unsigned int length, 
	 int parent) :
  row(row),
  col(col),
  colour(colour),
  length(length),
  parent(parent)
{
  // 0
}

Run::~Run(){}

unsigned int Run::getCOMX() const
{
  
  return (((col << 1) + length - 1)*length) >> 1;

}
unsigned int Run::getCOMY() const
{
  return row * length;
}

std::ostream & operator<<(std::ostream & stream, const Run & run)
{
  stream << "run(" << run.row << "," << run.col << ") ";
  stream << "length: " << run.length << "  colour: ";
  stream << (RawPixel)run.colour << "   parent: " << run.parent << "\n";
  return stream;
}

unsigned int Run::colMax() const
{
  return col + length - 1;
}

bool Run::isBelow(const Run & run)
{
  bool result = false;
  int offset;  
  if(row == run.row + 1)
    {
      offset = run.col - col;
      if(offset > 0)
	{
	  if((int)length > offset)
	    result = true;
	}
      else
	{
	  if((int)run.length > -offset)
	    result = true;
	}
    }

  //  std::cout << "is this below that?" << (result ? "true" : "false") << "\n";
  //  std::cout << "this: " << *this;
  //  std::cout << "that: " << run;

  return result;
}

// returns zero if the run contains the point, 
// -ve if the point occurs before the run.
// +ve if the point occurs after the run.
int Run::containsPoint(unsigned int pRow, unsigned int pCol)
{
  int rVal = 0;
  int offset;
  
  //  std::cout << "Run::containsPoint(" << pRow << "," << pCol << ")\n";
  //  std::cout << *this;
  if(pRow != row)
    {
      rVal = pRow - row;
      //      std:: cout << "The point is not on the same row";
    }
  else 
    {
      offset = (pCol - col);
      if(offset < 0 || offset >= (int)length)
	{
	  rVal = offset;
	  //	  std::cout << "the point is on the row but before or after: " << offset << "\n";
	}
      else
	{
	  //	  rVal = 0;
	  //	  std::cout << "run found\n";
	}
    }
  //  std::cout << "rVal: " << rVal << "\n";
  return rVal;
}

int Run::overlaps(RunPoint p){
  return containsPoint(p.row,p.col);
}

// returns zero if the run contains the point, 
// -ve if the point occurs before the run.
// +ve if the point occurs after the run.
int Run::overlaps(Run run)
{
  int result;
  int offset;  
  if(row == run.row)
    {
      offset = run.col - col;
      if(offset > 0)
	{
	  if((int)length > offset)
	    result = 0;
	  else
	    result = 1;
	}
      else
	{
	  if((int)run.length > -offset)
	    result = 0;
	  else
	    result = -1;
	}
    }
  else
    {
      result = run.row - row;
    }
  return result;
}
int Run::overlaps(unsigned int row)
{
  return row - this->row;
}

RunRegion::RunRegion(unsigned int xMin, unsigned int xMax, 
		     unsigned int yMin, unsigned int yMax,
		     unsigned int numPixels) :
  xMin(xMin),
  xMax(xMax),
  yMin(yMin),
  yMax(yMax),
  numPixels(numPixels),
  used(false),
  type(Trackable::UNKNOWN),
  subtype(-1),
  theta(0.0),
  centreOfMassX(0),
  centreOfMassY(0)
{
  // 0
  //  std::cout << "creating region: " << *this;
}

RunRegion::RunRegion(const Run & run)
{
  yMin = yMax = run.row;
  xMin = run.col;
  xMax = run.col + run.length - 1;
  numPixels = run.length;
  centreOfMassX = run.getCOMX();
  centreOfMassY = run.getCOMY();
  used = false;
  type = Trackable::UNKNOWN;
  subtype = -1;
  theta = 0;
  colour = run.colour;
  //  std::cout << "creating region from run: " << *this;
}

void RunRegion::absorb(RunRegion & r)
{
  if(&r != this)
    {
      //      std::cout << *this;
      //      std::cout << "absorbing " << r;
      xMin = (xMin < r.xMin) ? xMin : r.xMin;
      xMax = (xMax > r.xMax) ? xMax : r.xMax;
      yMin = (yMin < r.yMin) ? yMin : r.yMin;
      yMax = (yMax > r.yMax) ? yMax : r.yMax;
      numPixels += r.numPixels;
      centreOfMassX += r.centreOfMassX;
      centreOfMassY += r.centreOfMassY;
      colour += r.colour;
      r.numPixels = 0;
      r.used = true;
    }
//   else
//    {
//       std::cout << "Attempted to merge region with itself\n";
//    }

}

void RunRegion::absorb(const Run & r)
{
  //  std::cout << *this;
  //  std::cout << "absorbing " << r;

  xMin = (xMin < r.col) ? xMin : r.col;
  xMax = (xMax > r.colMax()) ? xMax : r.colMax();
  yMin = (yMin < r.row) ? yMin : r.row;
  yMax = (yMax > r.row) ? yMax : r.row;
  numPixels += r.length;
  centreOfMassX += r.getCOMX();
  centreOfMassY += r.getCOMY();
  colour += r.colour;
}

std::ostream & operator<<(std::ostream & stream, const RunRegion & r)
{
  stream << "RunRegion (" << r.yMin << "," << r.xMin << ")-(" 
	 << r.yMax << "," << r.xMax << ") size: " << r.numPixels 
	 << "  used: " << r.used << "  type:" << r.type 
	 << "\n"; 
  return stream;
}

int RunRegion::getCOMXI() const
{
  return (int)rint(getCOMXD());
}
double RunRegion::getCOMXD() const
{
  return centreOfMassX / (double)numPixels;
}
int RunRegion::getCOMYI() const
{
  return (int)rint(getCOMYD());
}
double RunRegion::getCOMYD() const
{
  return centreOfMassY / (double)numPixels;
}


RegionList::RegionList(){}
RegionList::~RegionList(){}

void RegionList::createRegions(FrameBuffer * frame, unsigned int row, 
			       unsigned int col, unsigned int height, 
			       unsigned int width)
{
  runLengthEncode(frame,row,col,width,height);
  runRegionMerge();
}

void RegionList::createRegions(FrameBuffer * frame)
{
  createRegions(frame,0,0,frame->width, frame->height);
}

int RegionList::getParentRegionIdx(unsigned int row, unsigned int col)
{

  int retVal = binarySearchRunList(RunPoint(row,col));
  //  std::cout << "Result: " << retVal << "\n";
  if(retVal != -1)
    {
      //      std::cout << runList[idx];
      retVal = parent[runList[retVal].parent];
      //      std::cout << (*this)[retVal];
    }
  return retVal;
}


std::vector<Run> RegionList::getRegionRunsOnRow(unsigned int row, unsigned int parentIdx)
{
  std::vector<Run> retList;
  Run r; // a run covering the area requested.
  int rowIdx;
  if(parentIdx < size())
    {
      r.row = row;
      r.col = (*this)[parentIdx].xMin;
      r.length = (*this)[parentIdx].xMax - r.col;
      
      rowIdx = binarySearchRunList(row);
      if(rowIdx != -1)
	{
	  while(rowIdx < (int)runList.size() && runList[rowIdx].row == row)
	    {
	      if(parent[runList[rowIdx].parent] == (int)parentIdx)
		{
		  retList.push_back(runList[rowIdx]);
		}
	      rowIdx++;
	    }
	}

    }
  else
    {
      std::cerr << "RegionList::getRunsOnRow() passed parentIdx out of range.\n";
    }

  return retList;
}

template<class T>
int RegionList::binarySearchRunList(T rhs)
{
  // run a binary search through the run list to find the parent idx
  int startIdx;
  int endIdx;
  int idx;
  int compareVal;
  int retVal = -1;

  if(runList.size() > 0)
    {
      startIdx = 0;
      endIdx = runList.size() - 1;
      idx = (startIdx+endIdx) >> 1;
      //      std::cout << "start: " << startIdx << "  end: " << endIdx << "  idx: " << idx << "\n";
      compareVal = runList[idx].overlaps(rhs);
      while(compareVal != 0 && startIdx < endIdx)
	{
	  if(compareVal < 0)
	    endIdx = idx - 1;
	  else
	    startIdx = idx + 1;
	  idx = (startIdx+endIdx) >> 1;
	  //	  std::cout << "start: " << startIdx << "  end: " << endIdx << "  idx: " << idx << "\n";
	  compareVal = runList[idx].overlaps(rhs);
	}
      
      if(compareVal == 0)
	retVal = idx;

    }
  
  return retVal;  
}

void RegionList::runLengthEncode(FrameBuffer * frame,unsigned int startRow, unsigned int startCol, unsigned int height, unsigned int width)
{
  Run run;
  RawPixel colour;
  unsigned int row, col, xEnd, yEnd;
  bool runStarted;
  FrameBufferIterator fbit(frame);
  runList.clear();
  xEnd = startCol + width;
  yEnd = startRow + height;

  //  std::cout << "rle enter\n";
  for(row = startRow; row < yEnd; ++row)
    {
      fbit.goPosition(row,0);
      // initialize the first run.
      run.row = row;
      run.col = startCol;
      //      run.colour = fbit.getIntensity();
      runStarted = (fbit.getIntensity() != 0);
      fbit.getPixel(&colour);
      run.colour = colour;
      fbit.goRight();
      for(col = startCol + 1; col < xEnd; ++col)
	{
	  if(fbit.getIntensity() == 0 && runStarted)
	    {
	      // store the run
	      run.length = col - run.col;
	      runList.push_back(run);
	      //  std::cout << "storing: " << run;
	      // initialize a new "zero" run
	      // (a run of black pixels)
	      // run.col = col;
	      // run.colour = fbit.getIntensity();
	      runStarted = false;
	    }
	  else if(fbit.getIntensity() != 0 )
	    {
	      fbit.getPixel(&colour);
	      if(!runStarted)
		{
		  // start a run
		  run.col = col;
		  //run.colour = fbit.getIntensity();
		  run.colour = colour;
		  runStarted = true;
		}
	      else
		{
		  run.colour += colour;
		}
	    }
	  fbit.goRight();
	} // end for each pixel in a row
      // store end-of-row runs
      if(runStarted)
	{
	  run.length = frame->width - run.col;
	  runList.push_back(run);
	  // std::cout << "storing: " << run;
	} // end if

    } // end for each row.
  
  //  std::cout << "rle exit\n";

}


// grow regions from the list of runs.
void RegionList::runRegionMerge()
{
  unsigned int aIdx, bIdx; // indexes used to build the regions
  unsigned int size;
  RegionList & regionList = *this;
  // std::cout << "=========================\n=======================\n";
  
  aIdx = bIdx = 0;
  size = runList.size();
  regionList.clear();
  parent.clear();

  if(size > 0)
    newRunRegion(0);

  
  // std::cout << "rrm: runs: " << size << "\n";

  // std::cout << "=========================\n=======================\n";
  while(bIdx < size)
    {
      // std::cout << "loop head\n";
      // std::cout << "A: " << runList[aIdx];
      // std::cout << "B: " << runList[bIdx];
      if(runList[bIdx].row <= runList[aIdx].row)
	{
	  // std::cout << "B is on the same row as A\n";
	  // std::cout << "A: " << aIdx << "  B: " << bIdx << "\n";
	  // B is on the same row as A
	  // increment B.
	  bIdx++;
	}
      else if(runList[aIdx].row < runList[bIdx].row - 1)
	{
	  // std::cout << "A is more than one row behind B\n";
	  // std::cout << "A: " << aIdx << "  B: " << bIdx << "\n";
	  // A is more than one row behind B.
	  // increment A
	  aIdx++;
	  if(runList[aIdx].parent == -1)
	    newRunRegion(aIdx);
	}
      else
	{
	  if(runList[bIdx].isBelow(runList[aIdx]))
	    {
	      // std::cout << "B is below A\n";
	      // std::cout << "A: " << aIdx << "  B: " << bIdx << "\n";
	      // join the regions if they overlap
	      // the region with the youngest parent wins.
	      if(runList[bIdx].parent == -1)
		{
		  // std::cout << "B has no parent\n";
		  // std::cout << "A: " << aIdx << "  B: " << bIdx << "\n";
		  // B has no parent.  absorb B
		  regionList[parent[runList[aIdx].parent]].absorb(runList[bIdx]);
		  runList[bIdx].parent = runList[aIdx].parent;
		}
	      else if(runList[aIdx].parent < runList[bIdx].parent)
		{
		  // std::cout << "A absorbs B's parent\n";
		  // std::cout << "A: " << aIdx << "  B: " << bIdx << "\n";

		  // A absorbs B's parent region
		  regionList[parent[runList[aIdx].parent]].absorb(regionList[parent[runList[bIdx].parent]]);
		  // B's parent region is replaced with A's
		  parent[runList[bIdx].parent] = parent[runList[aIdx].parent];
		}
	      else if(runList[aIdx].parent > runList[bIdx].parent)
		{
		  // std::cout << "B absorbs A's parent\n";
		  // std::cout << "A: " << aIdx << "  B: " << bIdx << "\n";

		  // B absorbs A's parent region
		  regionList[parent[runList[bIdx].parent]].absorb(regionList[parent[runList[bIdx].parent]]);
		  // A's parent region is replaced with B's
		  parent[runList[aIdx].parent] = parent[runList[bIdx].parent];
		} // note, if the parents are the same, do nothing!
	    }
	  
	  if(runList[aIdx].colMax() > runList[bIdx].colMax())
	    {
	      // std::cout << "A's colmax > B's\nIncrement B\n";
	      bIdx++;
	    }
	  else
	    {
	      // std::cout << "B's colmax <= A's\nIncrement A\n";
	      aIdx++;
	      if(runList[aIdx].parent == -1)
		newRunRegion(aIdx);
	    }
	}
      // std::cout << "loop tail\n";
    }  

  // process the last row.
  while(aIdx < size)
    {
      if(runList[aIdx].parent == -1)
	newRunRegion(aIdx);
      aIdx++;
    }

}
void RegionList::newRunRegion(unsigned int runIdx)
{
  // creates a new region using double indirect addressing.
  RunRegion r(runList[runIdx]);
  this->push_back(r);  
  parent.push_back(this->size() - 1);
  runList[runIdx].parent = parent.size() - 1;
  // std::cout << "newRunRegion(" << runIdx << "): " << runList[runIdx];
  // std::cout << r;
  // std::cout << runList[parent[runList[runIdx].parent]];
}


double RunRegion::getHWRatio() const
{
  return (double)getHeight() / (double)getWidth();
}
unsigned int RunRegion::getHeight() const
{
  return yMax - yMin;
}
unsigned int RunRegion::getWidth() const
{
  return xMax - xMin;
}

double RunRegion::getAvgCentreX() const
{
  return (getBBCentreX() + getCOMXD()) / 2.0;
}
double RunRegion::getAvgCentreY() const
{
  return (getBBCentreY() + getCOMYD()) / 2.0;
}
int RunRegion::getAvgCentreXI() const
{
  return (int)rint(getAvgCentreX());
}
int RunRegion::getAvgCentreYI() const
{
  return (int)rint(getAvgCentreY());
}
double RunRegion::getBBCentreX() const
{
  return (double)(xMin + xMax) / 2.0;
}

double RunRegion::getBBCentreY() const
{
  return (double)(yMin + yMax) / 2.0;
}

int RunRegion::getBBCentreXI() const
{
  return (int)rint(getBBCentreX());
}

int RunRegion::getBBCentreYI() const
{
  return (int)rint(getBBCentreY());
}


std::vector<Run> RegionList::getUnusedRuns()
{
  std::vector<Run> unused;
  for(uint i = 0; i < runList.size(); ++i)
    {
      if(!(*this)[runList[i].parent].used)
	{
	  unused.push_back(runList[i]);
	}
    }
  return unused;
}


Pixel RunRegion::getAvgColour()
{
  return Pixel(colour.red / numPixels, colour.green / numPixels, colour.blue / numPixels );
}




