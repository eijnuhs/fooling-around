/****************************************************************************
** ColourMapFrameBufferViewer meta object code from reading C++ file 'colourmapframebufferviewer.h'
**
** Created: Fri Feb 17 15:31:30 2006
**      by: The Qt MOC ($Id: qt/moc_yacc.cpp   3.3.4   edited Jan 21 18:14 $)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#undef QT_NO_COMPAT
#include "../colourmapframebufferviewer.h"
#include <qmetaobject.h>
#include <qapplication.h>

#include <private/qucomextra_p.h>
#if !defined(Q_MOC_OUTPUT_REVISION) || (Q_MOC_OUTPUT_REVISION != 26)
#error "This file was generated using the moc from 3.3.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

const char *ColourMapFrameBufferViewer::className() const
{
    return "ColourMapFrameBufferViewer";
}

QMetaObject *ColourMapFrameBufferViewer::metaObj = 0;
static QMetaObjectCleanUp cleanUp_ColourMapFrameBufferViewer( "ColourMapFrameBufferViewer", &ColourMapFrameBufferViewer::staticMetaObject );

#ifndef QT_NO_TRANSLATION
QString ColourMapFrameBufferViewer::tr( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "ColourMapFrameBufferViewer", s, c, QApplication::DefaultCodec );
    else
	return QString::fromLatin1( s );
}
#ifndef QT_NO_TRANSLATION_UTF8
QString ColourMapFrameBufferViewer::trUtf8( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "ColourMapFrameBufferViewer", s, c, QApplication::UnicodeUTF8 );
    else
	return QString::fromUtf8( s );
}
#endif // QT_NO_TRANSLATION_UTF8

#endif // QT_NO_TRANSLATION

QMetaObject* ColourMapFrameBufferViewer::staticMetaObject()
{
    if ( metaObj )
	return metaObj;
    QMetaObject* parentObject = FrameBufferViewer::staticMetaObject();
    static const QUMethod slot_0 = {"nextColour", 0, 0 };
    static const QUMethod slot_1 = {"previousColour", 0, 0 };
    static const QUMethod slot_2 = {"addColour", 0, 0 };
    static const QUMethod slot_3 = {"loadColours", 0, 0 };
    static const QUMethod slot_4 = {"saveColours", 0, 0 };
    static const QMetaData slot_tbl[] = {
	{ "nextColour()", &slot_0, QMetaData::Public },
	{ "previousColour()", &slot_1, QMetaData::Public },
	{ "addColour()", &slot_2, QMetaData::Public },
	{ "loadColours()", &slot_3, QMetaData::Public },
	{ "saveColours()", &slot_4, QMetaData::Public }
    };
    static const QUParameter param_signal_0[] = {
	{ "name", &static_QUType_QString, 0, QUParameter::In }
    };
    static const QUMethod signal_0 = {"newName", 1, param_signal_0 };
    static const QUParameter param_signal_1[] = {
	{ "c", &static_QUType_varptr, "\x0a", QUParameter::In }
    };
    static const QUMethod signal_1 = {"newColour", 1, param_signal_1 };
    static const QMetaData signal_tbl[] = {
	{ "newName(const QString&)", &signal_0, QMetaData::Public },
	{ "newColour(const QColor&)", &signal_1, QMetaData::Public }
    };
    metaObj = QMetaObject::new_metaobject(
	"ColourMapFrameBufferViewer", parentObject,
	slot_tbl, 5,
	signal_tbl, 2,
#ifndef QT_NO_PROPERTIES
	0, 0,
	0, 0,
#endif // QT_NO_PROPERTIES
	0, 0 );
    cleanUp_ColourMapFrameBufferViewer.setMetaObject( metaObj );
    return metaObj;
}

void* ColourMapFrameBufferViewer::qt_cast( const char* clname )
{
    if ( !qstrcmp( clname, "ColourMapFrameBufferViewer" ) )
	return this;
    return FrameBufferViewer::qt_cast( clname );
}

// SIGNAL newName
void ColourMapFrameBufferViewer::newName( const QString& t0 )
{
    activate_signal( staticMetaObject()->signalOffset() + 0, t0 );
}

#include <qobjectdefs.h>
#include <qsignalslotimp.h>

// SIGNAL newColour
void ColourMapFrameBufferViewer::newColour( const QColor& t0 )
{
    if ( signalsBlocked() )
	return;
    QConnectionList *clist = receivers( staticMetaObject()->signalOffset() + 1 );
    if ( !clist )
	return;
    QUObject o[2];
    static_QUType_varptr.set(o+1,&t0);
    activate_signal( clist, o );
}

bool ColourMapFrameBufferViewer::qt_invoke( int _id, QUObject* _o )
{
    switch ( _id - staticMetaObject()->slotOffset() ) {
    case 0: nextColour(); break;
    case 1: previousColour(); break;
    case 2: addColour(); break;
    case 3: loadColours(); break;
    case 4: saveColours(); break;
    default:
	return FrameBufferViewer::qt_invoke( _id, _o );
    }
    return TRUE;
}

bool ColourMapFrameBufferViewer::qt_emit( int _id, QUObject* _o )
{
    switch ( _id - staticMetaObject()->signalOffset() ) {
    case 0: newName((const QString&)static_QUType_QString.get(_o+1)); break;
    case 1: newColour((const QColor&)*((const QColor*)static_QUType_ptr.get(_o+1))); break;
    default:
	return FrameBufferViewer::qt_emit(_id,_o);
    }
    return TRUE;
}
#ifndef QT_NO_PROPERTIES

bool ColourMapFrameBufferViewer::qt_property( int id, int f, QVariant* v)
{
    return FrameBufferViewer::qt_property( id, f, v);
}

bool ColourMapFrameBufferViewer::qt_static_property( QObject* , int , int , QVariant* ){ return FALSE; }
#endif // QT_NO_PROPERTIES
