#ifndef __TRACKABLE_H_
#define __TRACKABLE_H_


/* a pure virtual superclass that 
 * represents a trackable item
 */

#include<string>
#include<iostream>
#include "framebufferrgb32.h"

// setting this to zero will
// disable the use of the 
// scratchpad framebuffer
// for trackable object that
// don't absoultely need
// to use it.
#define USE_SCRATCHPAD 1

// the size of the array that tracks the percent found
#define FA_SIZE 100

// the size of the array tracking velocity
#define VEL_AVG_SIZE 10

//// Forward Declarations ////
class RunRegion;
class FrameBuffer;
class FrameBufferWorld;
class VideoInterpreter2D;

#define MM_PER_METER 1000.0
#define MICRO_SEC_PER_SEC 1000000.0

#define M_PER_SEC 1000.0

class Trackable
{
 public:
  
  // the enumerated constant of types and a 
  // parallel string array of the names
  enum TrackableTypes{
    ROBOT = 0,
    BALL,
    SPOT,
    TOBJECT,
    UNKNOWN,
    TT_SIZE
  };
  static const std::string TYPE_NAMES[TT_SIZE];
  
  // a static method to generate Trackable objects
  // This job has been moved to the TrackerConfig object;
  //  static Trackable * createTrackable(std::string type, std::string name, double height, double radius, int subtype = -1);
  static void setVideoInterpreter(VideoInterpreter2D * init_vi2d);
  static void setDebugFrame(FrameBufferWorld * init_debugFrame);

  Trackable(enum TrackableTypes type, std::string name, double height, double radius, double mmPerPix, int subtype = -1);
  virtual ~Trackable();

  inline enum TrackableTypes getType(){return type;}
  inline int getSubtype(){return subtype;}
  inline double getX(){return x;}
  inline double getY(){return y;}
  inline double getDx(){return dx;}
  inline double getDy(){return dy;}
  inline double getTheta(){return theta;}
  inline double getHeight(){return height;}
  virtual double getRadius(){return radius;}
  inline std::string getName(){return name;}
  inline bool getFound(){return found;}
  inline double getPercentFound(){return percentFound;}
  inline FrameBufferRGB32 * scratchpad(){return & m_scratchpad; }


  // coordinates adjusted for height.
  void getAdjustedCoordinates(double * x, double * y);


  void getScreenCoordinates(int * x, int * y);
  virtual void output (std::ostream & os);

  friend std::ostream & operator<<(std::ostream & stream, const Trackable & track);

  // this method decrements the percentfound and kills the valid flag.
  // it should be called every frame.
  virtual void initForFrame();

  // This method updates the x,y,theta but also the 
  // dx,dy based on the timestamp.  A new timestamp is grabbed.
  virtual void setPosition(double x, double y, double theta, struct timeval time);
  virtual void setPosition(double x, double y, struct timeval time);

  // predicts the position based on the timestamp, position and
  // velocity.
  virtual void predictPosition(double &x, double &y, struct timeval time);

  
  // virtual method
  // determines if a region is the trackable object.
  // if it is, the method should call the setPosition method
  // to update the data.
  // note: Regions store coordinates of pixels within the worldFrame.
  //       The inframe is the raw camera image.
  //       The static class variable debugFrame is a FrameBufferWorld object
  //       which may be used to print debugging information.
  // The vi2d static class object is the VideoInterpreter2D object that 
  // trackable objects may use to translate coordinates.
  virtual bool isMe(RunRegion & region, FrameBuffer * inFrame, FrameBufferWorld * worldFrame, unsigned int avgIntensity) = 0;

  // virtual method
  // allows a trackable item to try to track itself just with velocity and the world frame.  Items may then erase themselves
  // from the world frame.  This is called before motion detection.  Subclasses are not required to use this.
  virtual bool trackMe(FrameBuffer *, FrameBufferWorld *){return false;}
 protected:

  virtual void maskObject(FrameBufferWorld * worldFrame);
  void setSubtype(int setSubtype);
  virtual void initMinAndMax(double mmPerPix);

  enum TrackableTypes type;

  int subtype;
  std::string name;
  double x,y;
  double dx,dy;
  double theta;
  double height;
  double percentFound;
  double radius;
  bool found; // found this round.
  struct timeval timestamp;
  bool everFound;
  FrameBufferRGB32 m_scratchpad;

  // static class members available to subclasses.
  static VideoInterpreter2D * vi2d;
  static FrameBufferWorld * debugFrame;
  unsigned int minPixels;
  unsigned int maxPixels;
  
  bool foundArray[FA_SIZE];
  uint foundIdx;
  friend class Tracker;

 private:

};


#endif

