// $Id: segmentationviewer.h,v 1.1.1.1.2.5 2004/12/28 19:30:05 cvs Exp $
//

#ifndef _SEGMENTATION_VIEWER_H_
#define _SEGMENTATION_VIEWER_H_

#include "framebufferviewer.h"
#include <mainwindow.h>

// 
class Segmentation;
class QPaintEvent;
class RunRegion;
class QPainter;

class SegmentationViewer : public FrameBufferViewer
{
  Q_OBJECT
 public:
  SegmentationViewer( QWidget * parent = 0, char const * name = 0);
  void setSegmentation( Segmentation * seg );
  virtual void paintChild( QPainter & p );
  virtual void mousePressEvent( QMouseEvent * e );
 public slots:
  void setMode(const QString & mode);
  void setThreshold(int threshold);
  //  void enableFiltering(bool enabled);
  //  void enableBlur(bool enable);
  //  void setMMPerPixel(int mm);
  //  void setMaxLength(int maxL);
  //  void setMinLength(int minL);
  //  void newFrame();
  void setIntThreshold(int threshold); // intensity threshold
  void drawRegion(QPainter & p, const RunRegion & r);
  void printName();
  void saveBackground();

 private:
  void showRegionInfo( void ) const;
  void showRegions( void );
  
  Segmentation * segmentation;
  int showX, showY;
  
};

#endif
