/************************************************************************/
/*                                                                      */
/*               Copyright 1998-2002 by Ullrich Koethe                  */
/*       Cognitive Systems Group, University of Hamburg, Germany        */
/*                                                                      */
/*    This file is part of the VIGRA computer vision library.           */
/*    ( Version 1.3.2, Jan 27 2005 )                                    */
/*    You may use, modify, and distribute this software according       */
/*    to the terms stated in the LICENSE file included in               */
/*    the VIGRA distribution.                                           */
/*                                                                      */
/*    The VIGRA Website is                                              */
/*        http://kogs-www.informatik.uni-hamburg.de/~koethe/vigra/      */
/*    Please direct questions, bug reports, and contributions to        */
/*        koethe@informatik.uni-hamburg.de                              */
/*                                                                      */
/*  THIS SOFTWARE IS PROVIDED AS IS AND WITHOUT ANY EXPRESS OR          */
/*  IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED      */
/*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. */
/*                                                                      */
/************************************************************************/


#ifndef VIGRA_BGRVALUE_HXX
#define VIGRA_BGRVALUE_HXX

#include <cmath>    // abs(double)
#include <cstdlib>  // abs(int)
#include "vigra/config.hxx"
#include "vigra/numerictraits.hxx"
#include "vigra/accessor.hxx"
#include "vigra/tinyvector.hxx"

namespace vigra {

/********************************************************/
/*                                                      */
/*                      BGRValue                        */
/*                                                      */
/********************************************************/

/** \brief Class for a single BGR value.

    This class contains three values (of the specified type) that represent
    red, green, and blue color channels. There are three possibilities
    to access these values: accessor functions (\ref red(), \ref green(),
    \ref blue()), index operator (operator[](dx), where 0 is red,
    1 is green and 2 is blue) and iterator (STL-compatible random access
    iterator that references the three colors in turn). The latter two
    methods, together with the necessary embedded typedefs, ensure
    compatibility of a BGRValue with a STL vector.

    \ref BGRValueOperators "Arithmetic operations" are defined as component-wise applications of these
    operations. Addition, subtraction, and multiplication of two BGRValues
    (+=, -=, *=, +, -, *, unary -), multiplication and division of an
    BGRValue with a double, and NumericTraits/PromoteTraits are defined,
    so that BGRValue fulfills the requirements of a \ref LinearAlgebra.

    A number of \ref BGRValueAccessors "accessors" are provided
    that support access to BGRValues as a whole, to a selected
    color component, or to the luminance value.

    <b>\#include</b> "<a href="bgrvalue_8hxx-source.html">vigra/bgrvalue.hxx</a>"<br>
    Namespace: vigra
*/
template <class VALUETYPE>
class BGRValue
: public TinyVector<VALUETYPE, 3>
{
    typedef TinyVector<VALUETYPE, 3> Base;
  public:
        /** STL-compatible definition of valuetype
        */
    typedef VALUETYPE value_type;
        /** STL-compatible definition of iterator
        */
    typedef typename TinyVector<VALUETYPE, 3>::iterator iterator;
        /** STL-compatible definition of const iterator
        */
    typedef typename TinyVector<VALUETYPE, 3>::const_iterator const_iterator;

        /** Construct from explicit color values
        */
    BGRValue(value_type red, value_type green, value_type blue)
    : Base(blue, green, red)
    {}

        /** Construct gray value
        */
    BGRValue(value_type gray)
    : Base(gray, gray, gray)
    {}

        /** Construct from another sequence (must have length 3!)
        */
    template <class Iterator>
    BGRValue(Iterator i, Iterator end)
    : Base(i[0], i[1], i[2])
    {}

        /** Default constructor (sets all components to 0)
        */
    BGRValue()
    : Base(0, 0, 0)
    {}

#if !defined(TEMPLATE_COPY_CONSTRUCTOR_BUG)

    BGRValue(BGRValue const & r)
    : Base(r)
    {}

    BGRValue & operator=(BGRValue const & r)
    {
        Base::operator=(r);
        return *this;
    }

#endif // TEMPLATE_COPY_CONSTRUCTOR_BUG


        /** Copy constructor.
        */
    template <class U>
    BGRValue(BGRValue<U> const & r)
    : Base(r)
    {}

        /** Copy assignment.
        */
    template <class U>
    BGRValue & operator=(BGRValue<U> const & r)
    {
        Base::operator=(r);
        return *this;
    }

        /** construct from TinyVector
        */
    BGRValue(TinyVector<value_type, 3> const & r)
    : Base(r)
    {}

        /** assign TinyVector.
        */
    BGRValue & operator=(TinyVector<value_type, 3> const & r)
    {
        Base::operator=(r);
        return *this;
    }

        /** Unary negation (construct BGRValue with negative values)
        */
    BGRValue operator-() const
    {
        return BGRValue(-red(), -green(), -blue());
    }

        /** Access red component.
        */
    value_type & red() { return (*this)[2]; }

        /** Access green component.
        */
    value_type & green() { return (*this)[1]; }

        /** Access blue component.
        */
    value_type & blue() { return (*this)[0]; }

        /** Get red component.
        */
    value_type const & red() const { return (*this)[2]; }

        /** Get green component.
        */
    value_type const & green() const { return (*this)[1]; }

        /** Get blue component.
        */
    value_type const & blue() const { return (*this)[0]; }

        /** Calculate luminance.
        */
    value_type luminance() const {
         return detail::RequiresExplicitCast<value_type>::cast(0.3*red() + 0.59*green() + 0.11*blue()); }

        /** Calculate magnitude.
        */
    typename NumericTraits<VALUETYPE>::RealPromote
    magnitude() const {
         return VIGRA_CSTD::sqrt(
            (typename NumericTraits<VALUETYPE>::RealPromote)squaredMagnitude());
    }

        /** Calculate squared magnitude.
        */
    typename NumericTraits<VALUETYPE>::Promote
    squaredMagnitude() const {
         return red()*red() + green()*green() + blue()*blue();
    }

        /** Set red component. The type <TT>V</TT> of the passed
            in <TT>value</TT> is automatically converted to <TT>VALUETYPE</TT>.
        */
    template <class V>
    void setRed(V value) { (*this)[2] = detail::RequiresExplicitCast<value_type>::cast(value); }

        /** Set green component.The type <TT>V</TT> of the passed
            in <TT>value</TT> is automatically converted to <TT>VALUETYPE</TT>.
        */
    template <class V>
    void setGreen(V value) { (*this)[1] = detail::RequiresExplicitCast<value_type>::cast(value); }

        /** Set blue component.The type <TT>V</TT> of the passed
            in <TT>value</TT> is automatically converted to <TT>VALUETYPE</TT>.
        */
    template <class V>
    void setBlue(V value) { (*this)[0] = detail::RequiresExplicitCast<value_type>::cast(value); }


    template <class V>
    void setRGB(V r, V g, V b) 
    { 
        (*this)[2] = detail::RequiresExplicitCast<value_type>::cast(r); 
        (*this)[1] = detail::RequiresExplicitCast<value_type>::cast(g); 
        (*this)[0] = detail::RequiresExplicitCast<value_type>::cast(b); 
    }
};

/********************************************************/
/*                                                      */
/*                     BGRValue Comparison              */
/*                                                      */
/********************************************************/

/** \addtogroup BGRValueOperators Functions for BGRValue

    \brief <b>\#include</b> "<a href="bgrvalue_8hxx-source.html">vigra/bgrvalue.hxx</a>

    These functions fulfill the requirements of a Linear Algebra.
    Return types are determined according to \ref BGRValueTraits.

    Namespace: vigra
    <p>

 */
//@{
    /// component-wise equal
template <class V1, class V2>
inline
bool
operator==(BGRValue<V1> const & l, BGRValue<V2> const & r)
{
    return (l.red() == r.red()) &&
       (l.green() == r.green()) &&
       (l.blue() == r.blue());
}

    /// component-wise not equal
template <class V1, class V2>
inline
bool
operator!=(BGRValue<V1> const & l, BGRValue<V2> const & r)
{
    return (l.red() != r.red()) ||
       (l.green() != r.green()) ||
       (l.blue() != r.blue());
}


//@}

/********************************************************/
/*                                                      */
/*                      BGRValue-Traits                 */
/*                                                      */
/********************************************************/

/** \page BGRValueTraits Numeric and Promote Traits of BGRValue
    The numeric and promote traits for BGRValues follow
    the general specifications for \ref NumericPromotionTraits.
    They are implemented in terms of the traits of the basic types by
    partial template specialization:

    \code

    template <class T>
    struct NumericTraits<BGRValue<T> >
    {
        typedef BGRValue<typename NumericTraits<T>::Promote> Promote;
        typedef BGRValue<typename NumericTraits<T>::RealPromote> RealPromote;

        typedef typename NumericTraits<T>::isIntegral isIntegral;
        typedef VigraFalseType isScalar;

        // etc.
    };

    template <class T1, class T2>
    struct PromoteTraits<BGRValue<T1>, BGRValue<T2> >
    {
        typedef BGRValue<typename PromoteTraits<T1, T2>::Promote> Promote;
    };
    \endcode

    <b>\#include</b> "<a href="bgrvalue_8hxx-source.html">vigra/bgrvalue.hxx</a>"<br>
    Namespace: vigra

*/

#if !defined(NO_PARTIAL_TEMPLATE_SPECIALIZATION)

template <class T>
struct NumericTraits<BGRValue<T> >
{
    typedef BGRValue<T> Type;
    typedef BGRValue<typename NumericTraits<T>::Promote> Promote;
    typedef BGRValue<typename NumericTraits<T>::RealPromote> RealPromote;
    typedef BGRValue<typename NumericTraits<T>::ComplexPromote> ComplexPromote;
    typedef T ValueType; 
    
    typedef typename NumericTraits<T>::isIntegral isIntegral; 
    typedef VigraFalseType isScalar; 
    typedef VigraFalseType isOrdered;
    typedef VigraFalseType isComplex; 

    static BGRValue<T> zero() {
        return BGRValue<T>(NumericTraits<T>::zero());
    }
    static BGRValue<T> one() {
        return BGRValue<T>(NumericTraits<T>::one());
    }
    static BGRValue<T> nonZero() {
        return BGRValue<T>(NumericTraits<T>::nonZero());
    }

    static Promote toPromote(BGRValue<T> const & v) {
        return Promote(v);
    }
    static RealPromote toRealPromote(BGRValue<T> const & v) {
        return RealPromote(v);
    }
    static BGRValue<T> fromPromote(Promote const & v) {
        return BGRValue<T>(NumericTraits<T>::fromPromote(v.red()),
                           NumericTraits<T>::fromPromote(v.green()),
                           NumericTraits<T>::fromPromote(v.blue()));
    }
    static BGRValue<T> fromRealPromote(RealPromote const & v) {
        return BGRValue<T>(NumericTraits<T>::fromRealPromote(v.red()),
                           NumericTraits<T>::fromRealPromote(v.green()),
                           NumericTraits<T>::fromRealPromote(v.blue()));
    }
};

template <class T1, class T2>
struct PromoteTraits<BGRValue<T1>, BGRValue<T2> >
{
    typedef BGRValue<typename PromoteTraits<T1, T2>::Promote> Promote;
};

template <class T>
struct PromoteTraits<BGRValue<T>, double >
{
    typedef BGRValue<typename NumericTraits<T>::RealPromote> Promote;
};

template <class T>
struct PromoteTraits<double, BGRValue<T> >
{
    typedef BGRValue<typename NumericTraits<T>::RealPromote> Promote;
};

#else // NO_PARTIAL_TEMPLATE_SPECIALIZATION

#define BGRVALUE_NUMTRAITS(T) \
template<>\
struct NumericTraits<BGRValue<T> >\
{\
    typedef BGRValue<T> Type; \
    typedef BGRValue<NumericTraits<T>::Promote> Promote; \
    typedef BGRValue<NumericTraits<T>::RealPromote> RealPromote; \
    typedef BGRValue<NumericTraits<T>::ComplexPromote> ComplexPromote; \
    typedef T ValueType; \
    \
    typedef NumericTraits<T>::isIntegral isIntegral; \
    typedef VigraFalseType isScalar; \
    typedef VigraFalseType isOrdered; \
    typedef VigraFalseType isComplex; \
    \
    static BGRValue<T> zero() { \
        return BGRValue<T>(NumericTraits<T>::zero()); \
    }\
    static BGRValue<T> one() { \
        return BGRValue<T>(NumericTraits<T>::one()); \
    }\
    static BGRValue<T> nonZero() { \
        return BGRValue<T>(NumericTraits<T>::nonZero()); \
    }\
    \
    static Promote toPromote(BGRValue<T> const & v) { \
        return Promote(v); \
    }\
    static RealPromote toRealPromote(BGRValue<T> const & v) { \
        return RealPromote(v); \
    }\
    static BGRValue<T> fromPromote(Promote const & v) { \
        BGRValue<T> res;\
        BGRValue<T>::iterator d = res.begin();\
        Promote::const_iterator s = v.begin();\
        for(; d != res.end(); ++d, ++s)\
            *d = NumericTraits<T>::fromPromote(*s);\
        return res;\
    }\
    static BGRValue<T> fromRealPromote(RealPromote const & v) {\
        BGRValue<T> res;\
        BGRValue<T>::iterator d = res.begin();\
        RealPromote::const_iterator s = v.begin();\
        for(; d != res.end(); ++d, ++s)\
            *d = NumericTraits<T>::fromRealPromote(*s);\
        return res;\
    }\
};

#define BGRVALUE_PROMTRAITS1(type1) \
template<> \
struct PromoteTraits<BGRValue<type1>, BGRValue<type1> > \
{ \
    typedef BGRValue<PromoteTraits<type1, type1>::Promote> Promote; \
    static Promote toPromote(BGRValue<type1> const & v) { \
        return static_cast<Promote>(v); } \
};

#define BGRVALUE_PROMTRAITS2(type1, type2) \
template<> \
struct PromoteTraits<BGRValue<type1>, BGRValue<type2> > \
{ \
    typedef BGRValue<PromoteTraits<type1, type2>::Promote> Promote; \
    static Promote toPromote(BGRValue<type1> const & v) { \
        return static_cast<Promote>(v); } \
    static Promote toPromote(BGRValue<type2> const & v) { \
        return static_cast<Promote>(v); } \
};

BGRVALUE_NUMTRAITS(unsigned char)
BGRVALUE_NUMTRAITS(int)
BGRVALUE_NUMTRAITS(float)
BGRVALUE_NUMTRAITS(double)
BGRVALUE_PROMTRAITS1(unsigned char)
BGRVALUE_PROMTRAITS1(int)
BGRVALUE_PROMTRAITS1(float)
BGRVALUE_PROMTRAITS1(double)
BGRVALUE_PROMTRAITS2(float, unsigned char)
BGRVALUE_PROMTRAITS2(unsigned char, float)
BGRVALUE_PROMTRAITS2(int, unsigned char)
BGRVALUE_PROMTRAITS2(unsigned char, int)
BGRVALUE_PROMTRAITS2(int, float)
BGRVALUE_PROMTRAITS2(float, int)
BGRVALUE_PROMTRAITS2(double, unsigned char)
BGRVALUE_PROMTRAITS2(unsigned char, double)
BGRVALUE_PROMTRAITS2(int, double)
BGRVALUE_PROMTRAITS2(double, int)
BGRVALUE_PROMTRAITS2(double, float)
BGRVALUE_PROMTRAITS2(float, double)

#undef BGRVALUE_NUMTRAITS
#undef BGRVALUE_PROMTRAITS1
#undef BGRVALUE_PROMTRAITS2

#endif // NO_PARTIAL_TEMPLATE_SPECIALIZATION


/********************************************************/
/*                                                      */
/*                      BGRValue-Arithmetic             */
/*                                                      */
/********************************************************/

/** \addtogroup BGRValueOperators
 */
//@{
    /// componentwise add-assignment
template <class V1, class V2>
inline
BGRValue<V1> &
operator+=(BGRValue<V1> & l, BGRValue<V2> const & r)
{
    l.red() += r.red();
    l.green() += r.green();
    l.blue() += r.blue();
    return l;
}

    /// componentwise subtract-assignment
template <class V1, class V2>
inline
BGRValue<V1> &
operator-=(BGRValue<V1> & l, BGRValue<V2> const & r)
{
    l.red() -= r.red();
    l.green() -= r.green();
    l.blue() -= r.blue();
    return l;
}

    /// componentwise multiply-assignment
template <class V1, class V2>
inline
BGRValue<V1> &
operator*=(BGRValue<V1> & l, BGRValue<V2> const & r)
{
    l.red() *= r.red();
    l.green() *= r.green();
    l.blue() *= r.blue();
    return l;
}

    /// componentwise scalar multiply-assignment
template <class V>
inline
BGRValue<V> &
operator*=(BGRValue<V> & l, double r)
{
    l.red() *= r;
    l.green() *= r;
    l.blue() *= r;
    return l;
}

    /// componentwise scalar divide-assignment
template <class V>
inline
BGRValue<V> &
operator/=(BGRValue<V> & l, double r)
{
    l.red() /= r;
    l.green() /= r;
    l.blue() /= r;
    return l;
}

using VIGRA_CSTD::abs;

    /// component-wise absolute value
template <class T>
inline
BGRValue<T> abs(BGRValue<T> const & v) {
    return BGRValue<T>(abs(v.red()), abs(v.green()),  abs(v.blue()));
}



    /// component-wise addition
template <class V1, class V2>
inline
typename PromoteTraits<BGRValue<V1>, BGRValue<V2> >::Promote
operator+(BGRValue<V1> const & r1, BGRValue<V2> const & r2)
{
    typename PromoteTraits<BGRValue<V1>, BGRValue<V2> >::Promote res(r1);

    res += r2;

    return res;
}

    /// component-wise subtraction
template <class V1, class V2>
inline
typename PromoteTraits<BGRValue<V1>, BGRValue<V2> >::Promote
operator-(BGRValue<V1> const & r1, BGRValue<V2> const & r2)
{
    typename PromoteTraits<BGRValue<V1>, BGRValue<V2> >::Promote res(r1);

    res -= r2;

    return res;
}

    /// component-wise multiplication
template <class V1, class V2>
inline
typename PromoteTraits<BGRValue<V1>, BGRValue<V2> >::Promote
operator*(BGRValue<V1> const & r1, BGRValue<V2> const & r2)
{
    typename PromoteTraits<BGRValue<V1>, BGRValue<V2> >::Promote res(r1);

    res *= r2;

    return res;
}

    /// component-wise left scalar multiplication
template <class V>
inline
typename NumericTraits<BGRValue<V> >::RealPromote
operator*(double v, BGRValue<V> const & r)
{
    typename NumericTraits<BGRValue<V> >::RealPromote res(r);

    res *= v;

    return res;
}

    /// component-wise right scalar multiplication
template <class V>
inline
typename NumericTraits<BGRValue<V> >::RealPromote
operator*(BGRValue<V> const & r, double v)
{
    typename NumericTraits<BGRValue<V> >::RealPromote res(r);

    res *= v;

    return res;
}

    /// component-wise scalar division
template <class V>
inline
typename NumericTraits<BGRValue<V> >::RealPromote
operator/(BGRValue<V> const & r, double v)
{
    typename NumericTraits<BGRValue<V> >::RealPromote res(r);

    res /= v;

    return res;
}

    /// cross product
template <class V1, class V2>
inline
typename PromoteTraits<BGRValue<V1>, BGRValue<V2> >::Promote
cross(BGRValue<V1> const & r1, BGRValue<V2> const & r2)
{
    typedef typename PromoteTraits<BGRValue<V1>, BGRValue<V2> >::Promote
            Res;
    return  Res(r1.green()*r2.blue() - r1.blue()*r2.green(),
                r1.blue()*r2.red() - r1.red()*r2.blue(),
                r1.red()*r2.green() - r1.green()*r2.red());
}

    /// dot product
template <class V1, class V2>
inline
typename PromoteTraits<V1, V2>::Promote
dot(BGRValue<V1> const & r1, BGRValue<V2> const & r2)
{
    return r1.red()*r2.red() + r1.green()*r2.green() + r1.blue()*r2.blue();
}

using VIGRA_CSTD::ceil;

    /** Apply ceil() function to each BGR component.
    */
template <class V>
inline
BGRValue<V>
ceil(BGRValue<V> const & r)
{
    return BGRValue<V>(ceil(r.red()),
                       ceil(r.green()),
                       ceil(r.blue()));
}

using VIGRA_CSTD::floor;

    /** Apply floor() function to each BGR component.
    */
template <class V>
inline
BGRValue<V>
floor(BGRValue<V> const & r)
{
    return BGRValue<V>(floor(r.red()),
                       floor(r.green()),
                       floor(r.blue()));
}

//@}



} // namespace vigra

#endif // VIGRA_BGRVALUE_HXX
