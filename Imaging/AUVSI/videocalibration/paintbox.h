// A class with some static draw routines
#ifndef _PAINT_BOX_H_
#define _PAINT_BOX_H_

#include <qpainter.h>
class ControlPoint;
class ControlPoints;


class PaintBox
{
  
  public:
  static void drawPoint( QPainter & p, int xm, int ym, uint size = 5, const QColor & pointColor = Qt::red);
  static void drawPoint( QPainter & p, int xm, int ym,  uint size, QString label, int labelOffset = 0, const QColor & pointColor = Qt::red,
                         const QColor & labelColor = Qt::yellow); 
  static void drawLabel( QPainter & p, int xm, int ym, QString label, int labelOffset = 0, 
                         const QColor & labelColor = Qt::yellow);

  static void drawControlPoints(QPainter & p, ControlPoints & points);
  static void drawControlPoint(QPainter & p, ControlPoint & p);
 
 };

#endif
 

