#include "paintbox.h"
#include <qstring.h>
#include "controlpoints.h"

void PaintBox::drawPoint( QPainter & p, int xm, int ym, uint size, const QColor & pointColor)
{
  uint half_size = size / 2;
  p.fillRect( xm - half_size, ym - half_size, size, size, pointColor );
}
  
void PaintBox::drawPoint( QPainter & p, int xm, int ym, 
                       uint size, QString label,
                       int labelOffset, const QColor & pointColor,
                       const QColor & labelColor)   
{
  PaintBox::drawPoint(p, xm, ym, size, pointColor);
  PaintBox::drawLabel(p,xm,ym,label,labelOffset, labelColor);
}

void PaintBox::drawLabel( QPainter & p, int xm, int ym, QString label, int labelOffset, 
                       const QColor & labelColor)
{
  p.setPen( labelColor );
  p.drawText( xm - labelOffset, ym - labelOffset, label );  
}
                       
void PaintBox::drawControlPoint( QPainter & p, ControlPoint & point )
{
  QString label = "(" + QString::number(point.xw) + ", " + QString::number(point.yw) + ")";
  PaintBox::drawPoint(p, (int)point.xm, (int)point.ym, 7, label, 10,Qt::red, Qt::blue);
}
  
  
void PaintBox::drawControlPoints( QPainter & p, ControlPoints & cps )
{
  for(int i = 0; i < cps.size(); ++i)
    PaintBox::drawControlPoint(p, cps.points[i]);
}

