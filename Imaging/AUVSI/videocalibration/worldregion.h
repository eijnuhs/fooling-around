#ifndef _WORLD_REGION_H_
#define _WORLD_REGION_H_

class WorldRegion{
 public:
  WorldRegion():wx(0),wy(0),hwRatio(0),area(0),value(0){}
  WorldRegion(double wx, double wy, const RunRegion & rr, double pixelDim) :
    wx(wx), wy(wy), hwRatio(rr.getHWRatio()), value(0.0)
    {
      area = rr.numPixels * pixelDim * pixelDim;
    }
  
  double wx,wy,hwRatio,area;
  double value; // the "goodness" of the region.
};

#endif /* _WORLD_REGION_H_ */
