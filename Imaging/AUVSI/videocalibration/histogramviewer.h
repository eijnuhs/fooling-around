#ifndef _HISTOGRAM_VIEWER_H_
#define _HISTOGRAM_VIEWER_H_

#include <qwidget.h>
#include "histogram.h"
#include <vector>


class HistogramViewer : public QWidget
{
  Q_OBJECT
 public:
  HistogramViewer( QWidget * parent = 0, const char * name = 0, WFlags f = 0 );
  ~HistogramViewer();
  void paintEvent ( QPaintEvent * );
  std::vector<unsigned int> thresholds;
 public slots:
 void newHistogram(const Histogram & hist);
 void setMax(int i);
 

 
 private:
  Histogram hist;
  unsigned int max;
};


#endif

