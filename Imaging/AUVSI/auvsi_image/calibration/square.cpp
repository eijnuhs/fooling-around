// $Id: square.cpp,v 1.1.1.1 2004/09/16 23:12:05 cvs Exp $
//

using namespace std;

#include "square.h"
#include <string>
#include <sstream>

Square::Square ()
{
  min_x = 0;
  max_x = 0;
  min_y = 0;
  max_y = 0;
  num_pixels = 0;
  median_x = 0.0;
  median_y = 0.0;
  avg_x = 0.0;
  avg_y = 0.0;
  x = 0.0;
  y = 0.0;
  memset (pixmap, 0, sizeof (pixmap));
  xi = 0.0;
  yi = 0.0;
  xr = 0.0;
  yr = 0.0;
  block_x = -1;
  block_y = -1;
}

Square::operator const char *()
{
  stringstream out;

  out << "Square at "
    << "(" << x << "," << y << ")"
    << "\n"
    << "   real="
    << "(" << xr << "," << yr << ")"
    << "\n"
    << "   image="
    << "(" << xi << "," << yi << ")"
    << "\n"
    << "   block="
    << "(" << block_x << "," << block_y << ")" << "\n" << '\000';

  return (out.str ().c_str ());
}
