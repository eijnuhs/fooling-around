#ifndef _SALIENCY_H_
#define _SALIENCY_H_

//FFTW includes.
#include "fftw3.h"

// OpenCV includes.
#include <cv.h>
#include <highgui.h>

// C++ includes.
#include <algorithm>
#include <iostream>
#include <sstream>
#include <string>
#include <cmath>
#include <complex>

class Saliency{

public:
	Saliency(IplImage* img);
	~Saliency();
	int numFound();
	IplImage* getSaliencyMap();
	IplImage* getInputImage();
	IplImage** getOutputImages();
	bool findObjects();
	void setInput(IplImage* img);


private:
	IplImage* inImg;
	IplImage* saliencyMap;
	IplImage* fftInput;
	IplImage** outImgs;
	CvMat* diskKernel;

	fftw_complex	*data_in;
	fftw_complex	*data_out;
    fftw_complex    *fft;
	fftw_complex    *ifft;   
    fftw_plan       plan_f;
    fftw_plan       plan_b;
	
	int numOutputs;
	int fftInputSize;

	void initFFTW();
	void fft2();
	void ifft2();
	double* logAmplitude(fftw_complex* in);
	double* phase(fftw_complex* in);
	double* spectralResidual(double* in);
	IplImage* doSaliencyMap(double* specResidual,double* fftPhase);
	
	//segmentation
	void adaptiveFilter(IplImage* img);
	void diskSmooth(IplImage* img);
	int segmentObjects(IplImage* img,int threshold);
	IplImage** extractSegments(IplImage* img);

	void prepareImage();
	double* abs_complex(fftw_complex* in);
	double abs_complex(double x, double i);
	uchar imageMax(IplImage* img);
	CvPoint findHighestIntensity(IplImage * img);
	CvPoint findHighestIntensity(IplImage * img,uchar max);
	
	inline int round(double d){
		return int(floor(d + 0.5));
	}
	

};

static int RESIZE_X = 64;
static int RESIZE_Y = 64;
static int SEG_THRESHOLD = 20;
static double SCALE_FACTOR = 0.5;
//disk kernel
static double arr [] = { 
	0.000 ,   0.003,    0.110,    0.172,    0.110,    0.003,    0.000,
    0.003  ,  0.245  ,  0.354  ,  0.354 ,   0.354 ,   0.245 ,   0.003,
    0.110 ,   0.354  ,  0.354  ,  0.354 ,   0.354  ,  0.354 ,   0.110,
    0.172 ,   0.354  ,  0.354 ,   0.354 ,   0.354  ,  0.354 ,   0.172,
    0.110 ,   0.354  ,  0.354  ,  0.354 ,   0.354  ,  0.354  ,  0.110,
    0.003,    0.245  ,  0.354 ,   0.354 ,   0.354  ,  0.245  ,  0.003,
	0.000,    0.003,    0.110,    0.172 ,   0.110,    0.003,    0.000
};

#endif