#ifndef __BROADCASTER_H_
#define __BROADCASTER_H_

#include <qobject.h>
#include <vector>
#include "configuration.h"
#include "udpbroadcaster.h"
class Trackable;
class FrameBuffer;

class BroadcasterConfig : public Configuration
{
 public:
  BroadcasterConfig() : Configuration("Network"){}
  ~BroadcasterConfig(){}
};

class Broadcaster : public QObject, public UDPBroadcaster
{
  Q_OBJECT
 public:
  Broadcaster(BroadcasterConfig * bcfg);
  ~Broadcaster();
  void setCameraPos(double cx, double cy, double cz);

 public slots:
   void  newTrackable(std::vector<Trackable *> & trackable, FrameBuffer * frame);

 private: 
 struct timeval t;
 double cx,cy,cz;
};



#endif

