#include <qapplication.h>
#include "umimage.hpp"
#include "agmainwindow.hpp"
#include <boost/bind.hpp>
#include <iostream>
#include <vigra/rgbvalue.hxx>
#include "umimagegui.hpp"
#include <vector>
#include "agref.hpp"
#include <string>
#include <tostring.hpp>
#include <umimagetypedefs.hpp>
#include <vigra/convolution.hxx> // gaussian gradient
#include <threadwrapper.hpp>
#include "boost/filesystem/operations.hpp" // includes boost/filesystem/path.hpp
#include "boost/filesystem/fstream.hpp"    // ditto
#include <agdockwindow.hpp>
#include "vigra/transformimage.hxx"
#include "vigra/inspectimage.hxx"
#include "vigra/flatmorphology.hxx"

namespace fs = boost::filesystem;

class Processing : public umbotica::ThreadWrapper
{
  private:  
    fs::directory_iterator imageFinder;//iteratively load image files from the directory
    fs::directory_iterator endIterator;//points to past-the-end of the images, used to tell when we got the last one
    fs::path imageDir;//path to the directory holding the files

    bool imRunning;//false if we already called die()
    
  public:
    //Select a directory that contains the calibration images.  Returns false if that
    //directory couldn't be found

  Processing() : brightness(1.0), contrast(1.0){}
    bool setImageDir(std::string newImageDir)
    {
      bool result = true;//false if imageDir doesn't exist
      
      imageDir = newImageDir;
      if (!fs::exists(imageDir))
        result = false;

      return result;
    }//setImageDir()
    
    void init()
    {
      imRunning = true;
      imageFinder = fs::directory_iterator(imageDir);
    }//init()
  
    void processing()
    {      
      // ***** 1) Find next image. *****
      if (imageFinder != endIterator)
      {
        std::cout << "Image Name: " <<  imageFinder->leaf() << std::endl;

        // ***** 2) image.loadImage(filename); *****
	process.loadImage(imageFinder->native_directory_string());

	vigra::FindMinMax<float> minmax;
	vigra::inspectImage(process.upperLeft(), process.lowerRight(), process.grayAccessor(), minmax);

	vigra::transformImage(process.upperLeft(), process.lowerRight(), process.grayAccessor(), process.upperLeft(), process.grayAccessor(),
			      vigra::BrightnessContrastFunctor<float>(brightness, contrast, minmax.min, minmax.max));

	UmGrey8 temp;
	temp.copyFrom(process);
	vigra::discErosion (temp.upperLeft(), temp.lowerRight(), temp.grayAccessor(), process.upperLeft(), process.grayAccessor() , radius);


	////////// END PREPROCESSING ////////////////
	image.copyFrom(process);



	UmGrey8 image_2 = image;
	image.resize(image.width(), image.height() * 2);
	UmGrey8::traverser in = image_2.upperLeft();
	UmGrey8::traverser out1 = image.upperLeft();
	UmGrey8::traverser out2 = image.upperLeft();
	out2.y++;
	
	//    int i = 0;
	for(; in.y != image_2.lowerRight().y; in.y++, out1.y += 2, out2.y += 2)
	  {
	    //	std::cout << "copying row: " << i++ << "\n";
	    memcpy(&(*out1), &(*in), image_2.widthStep());
	    memcpy(&(*out2), &(*in), image_2.widthStep());
	  }
      

	// I just added this sleep to see the pictures flip.
	sleep(1);
	
        // ***** 3) find chessboard corners *****
	CvSize etalonSize = {6,8};
	CvPoint2D32f corners[48];
	int cornerCount = 0;
	CvMemStorage * memStorage = cvCreateMemStorage();
	temp.resize(image.size());
	cvFindChessBoardCornerGuesses( image.cvImage(), temp.cvImage(), memStorage, etalonSize, corners, &cornerCount );
	std::cout << "found " << cornerCount << " corners\n";
        for(int i = 0; i < cornerCount; ++i)
	  {
	    std::cout << "Corner: " << umbotica::tuple(corners[i].x,corners[i].y) << "\n";
	  }
	// ***** 4) store info for calibration. *****
	cvReleaseMemStorage(&memStorage);
        
        imageFinder++;
      }//if imageFinder
      else
      {
        if (imRunning)
        {
          std::cout << "No more images, dying..." << std::endl;
          die();
        }//if imRunning
      }//else imageFinder
  }//processing()
  
  void cleanup()
  {
    // calibrate and print results
    imageFinder = endIterator;  
//     if (imageFinder != NULL)
//     {
//       delete imageFinder;
//       imageFinder = NULL;
//     }//if imageFinder
  }//cleanup()

  UmGrey8 image, temp;
  UmGreyF process;
  float brightness, contrast;
  int radius;
};

int main(int argc, char ** argv)
{ 
  QApplication a(argc, argv);
  a.connect( &a, SIGNAL( lastWindowClosed() ), &a, SLOT( quit() ) );
  Processing p;
  AGMainWindow w;
  w.resize(1024,768);
  int ret = 0;
  
  AGManager & gui = *w.getAGManager();
  
  // the autogui is a little rough around the edges...File
  // dialog requires a getter as well as a setter.

  // add the image with a one second update time.
  gui.pushGroup("log");
  gui.addLogWindow(std::cout);
  gui.setWindowSize(1024,10);
  gui.popGroup();

  gui.pushGroup();
  addToGui(gui, p.image, "Image", 500);
  gui.setWindowSize(1024,768);
  gui.addButton("Start Thread", boost::bind(&Processing::startThread, &p));
  gui.addFloat("Brightness: ", p.brightness, 0.0, 10000.0, 0.1, false);
  gui.addFloat("Contrast: ", p.contrast, 0.0, 10000.0, 0.1, false);
  gui.addInt("Radius: ", p.radius, 0, 10, 1, false);
  gui.popGroup();

  w.show();

  //  AGDockWindow * imageW = gui.getWindow(0);
  //  imageW->setFixedExtentHeight(1024);

  if (p.setImageDir("/home/auvsi11/auvsi/opencv_wrapper/examples/savevideo/checkerboard_reduced"))
  {
    p.startThread();
    ret = a.exec();
    p.stopThread();
  }//if setImageDir
  else
    std::cout << "Image directory not found" << std::endl;

  std::cout << "Halting normally" << std::endl;
  
  return ret;
}
