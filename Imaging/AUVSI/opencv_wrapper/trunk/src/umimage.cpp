//#include "grfmts.h"
#include "umimage.hpp"
#include "cvimagefilters.h"
#include <boost/shared_ptr.hpp>
#include <iostream>

// The cv image import/export filters
static CvImageFilters st_filterFactory;

UmImage::UmImage(unsigned int width, unsigned int height)
{
  // this sets the roi pointing at our member to control access and memory.
  m_image.roi = & m_roi;
  init(width, height, depth, channels);
}

UmImage::~UmImage()
{
  // nothing for now...We're going to try to keep everything
  // on the stack and images data in smart pointers.
  // cross your fingers.
}

void UmImage::init(unsigned int width, unsigned int height, int depth, int channels, bool allocateBuffer)
{
  CvSize size;
  size.height = height;
  size.width = width;
  cvInitImageHeader(&m_image, size , depth, channels, 0, 8);
  resetBuffer();
  if(allocateBuffer)
    {
      newBuffer();
    }
}

// is this unduly dangerous?
UmImage::operator const IplImage () const
{
  return m_image;
}


std::string UmImage::depth_str() const
{
  std::string retVal;
  switch(m_image.depth)
    {
    case IPL_DEPTH_8U:
      retVal = "unsigned 8-bit";
      break;
	
    case IPL_DEPTH_8S:
      retVal =  "signed 8-bit";
      break;
    case IPL_DEPTH_16S:
      retVal = "signed 16-bit";
      break;
    case IPL_DEPTH_32S:
      retVal = "signed 32-bit";
      break;
    case IPL_DEPTH_32F:
      retVal = "single-precision floating point";
      break;
    case IPL_DEPTH_64F:
      retVal = "double-presision floating point";
      break;
    default:
      retVal = "unknown pixel depth";
    }
  return retVal;
}
int UmImage::depth() const                  { return m_image.depth;     }
unsigned int UmImage::nChannels() const     { return m_image.nChannels; }
unsigned int UmImage::width()  const        { return m_image.width;     }
unsigned int UmImage::height() const        { return m_image.height;    }
unsigned int UmImage::imageSize() const     { return m_image.imageSize; }
unsigned int UmImage::widthStep() const     { return m_image.widthStep; }
std::string UmImage::colorModel() const     { return m_image.colorModel;}
std::string UmImage::channelSeq() const     { return m_image.channelSeq;}
//CvRect roi() const;
//void setROI(CvRect roi);
//void setROI(unsigned int x, unsigned int y, unsigned int width, unsigned int height);

buffer_ptr UmImage::newBuffer()
{
  m_buffer = buffer_ptr(new uint8_t[imageSize()]);
  m_image.imageData = (char *)m_buffer.get();
  return m_buffer;
}

void UmImage::resetBuffer()
{
  m_buffer.reset();
  m_image.imageData = 0;
}

bool UmImage::loadImage(std::string filename, UmImage::FileLoadingOptions option)
{
  std::cout << "loading " << filename << std::endl;
 
    // verify the filename
    if( filename.size() == 0 )
      {
	std::cerr << "UmImage::loadImage error: passed empty filename\n";
	return false;
      }

    boost::shared_ptr<GrFmtReader> reader(st_filterFactory.FindReader( filename.c_str() ));
    if( reader.get() == NULL )
      {
	std::cerr << "UmImage::loadImage error: filename sports unsupported type extension: " << filename << "\n";
	return false;
      }

    if( !reader->ReadHeader() )
      {
	std::cerr << "UmImage::loadImage error: unable to read file header: " << filename << "\n";
	return false;
      }

    bool isColor = option > 0 || (option < 0 && reader->IsColor());
    init(reader->GetWidth(), reader->GetHeight(), IPL_DEPTH_8U, isColor ? 3 : 1);
    newBuffer();
    if( !reader->ReadData( (unsigned char*)(m_image.imageData),
			   m_image.widthStep, isColor ))
      {
	std::cerr << "UmImage::loadImage error: image file read error: " << filename << "\n";
	resetBuffer();
	return false;
      }
    
    int cvErr = cvGetErrStatus();
    if( cvErr < 0 )
      {
        resetBuffer();
	std::cerr << "UmImage::loadImage error: opencv returned error code: " << cvErr << "\n";
	return false;
      }
    
    return true;
}


bool UmImage::saveImage(std::string filename)
{
    int origin = 0;
    int did_flip = 0;
    int channels = nChannels();

    // verify the filename
    if( filename.size() == 0 )
      {
	std::cerr << "UmImage::saveImage error: passed empty filename\n";
	return false;
      }

    origin = m_image.origin;

    if( channels != 1 && channels != 3 )
      {
	std::cerr << "UmImage::saveImage error: unsupported number of channels: " << channels << "\n";
	return false;
      }

    boost::shared_ptr<GrFmtWriter> writer(st_filterFactory.FindWriter( filename.c_str() ));
    if( writer.get() == NULL )
    {
      std::cerr << "UmImage::saveImage error: could not find a filter for the specified extension\n";
      return false;
    }

    if( origin )
      {
	cvFlip( &m_image, NULL, 0 );
	did_flip = 1;
      }

    if( !writer->WriteImage( (const uchar *)m_image.imageData, m_image.widthStep, m_image.width,
			     m_image.height, channels > 1 ))
      {
	std::cerr <<  "UmImage::saveImage error: could not save the image\n" ;
	return false;
      }

    if( origin && did_flip )
        cvFlip( &m_image, NULL, 0);

    int cvErr = cvGetErrStatus();
    if( cvErr < 0 )
      {
        resetBuffer();
	std::cerr << "UmImage::loadImage error: opencv returned errorcode: " << cvErr << "\n";
	return false;
      }
    
    return true;
}


uint8_t UmImage::getPixel(unsigned int x, unsigned int y, unsigned int channel) const
{
  return (m_buffer.get() + (y * widthStep()))[x * nChannels() + channel];
}

buffer_ptr UmImage::buffer(){return m_buffer;}


