#include "saliencyDetection.h"

Saliency::Saliency(IplImage* img){
	inImg			= img;
	saliencyMap		= NULL;
	fftInput		= NULL;
	outImgs			= NULL;
	numOutputs		= 0;
	fftInputSize	= 0;

	prepareImage();
	initFFTW();
	
	//initialize the diskKernel
	diskKernel = cvCreateMat(7,7,CV_64FC1);
	diskKernel->data.db = arr;
}

Saliency::~Saliency(){
	cvReleaseImage(&inImg);
	cvReleaseImage(&saliencyMap);

	fftw_destroy_plan( plan_f );
    fftw_destroy_plan( plan_b );
    fftw_free( data_in );
	fftw_free( data_out );
    fftw_free( fft );
    fftw_free( ifft );	
}
/*
	Initialise the image variables
*/
void Saliency::prepareImage(){

	//reduce the size of the input.
	fftInput = cvCreateImage(cvSize(RESIZE_X,RESIZE_Y),IPL_DEPTH_8U,1);
	cvResize( inImg, fftInput);	
	fftInputSize = fftInput->width * fftInput->height;
	
}
/* number of objects found */
int Saliency::numFound(){
	return numOutputs;
}
/* return the saliency map */
IplImage* Saliency::getSaliencyMap(){
	return saliencyMap;
}
/* return the input image */
IplImage* Saliency::getInputImage(){
	return inImg;
}
/* return the salient objects images */
IplImage** Saliency::getOutputImages(){
	return outImgs;
}
/* change the input image */
void Saliency::setInput(IplImage* img){
	//cleaning up 
	if(inImg != NULL){
		cvReleaseImage( &fftInput );
		cvReleaseImage( &saliencyMap);
		for(int i = 0 ; i < numOutputs ; ++i){
			cvReleaseImage( &outImgs[i] );
		}
		delete outImgs;
		numOutputs = 0;

	}
	inImg = img;
	prepareImage();
}

/*
	Initialize FFTW.
*/
void Saliency::initFFTW(){
	
	//memory allocation
	data_in = ( fftw_complex* )fftw_malloc( sizeof( fftw_complex ) * fftInputSize );
	data_out= ( fftw_complex* )fftw_malloc( sizeof( fftw_complex ) * fftInputSize );
    fft     = ( fftw_complex* )fftw_malloc( sizeof( fftw_complex ) * fftInputSize );
    ifft    = ( fftw_complex* )fftw_malloc( sizeof( fftw_complex ) * fftInputSize );

	//generating plans
	plan_f = fftw_plan_dft_2d( fftInput->height , fftInput->width, data_in, fft,  FFTW_FORWARD,  FFTW_ESTIMATE );
    plan_b = fftw_plan_dft_2d( fftInput->height , fftInput->width, data_out,ifft, FFTW_BACKWARD, FFTW_ESTIMATE );
}

/*
Performs the saliency detection.
Then applies the adaptive filtering, and finally segmenting each image blobs.
Return true if salient images are found else false
*/
bool Saliency::findObjects(){

	//Saliency Detection
	fft2();
	double* logAmp			= logAmplitude(fft);
	double* fftPhase		= phase(fft);
	double* specResidual	= spectralResidual(logAmp);
	saliencyMap				= doSaliencyMap(specResidual,fftPhase);
	IplImage *tempImg		= cvCloneImage(saliencyMap);
	
	//Adaptive Filtering
	adaptiveFilter(tempImg);
	
	//Image segmentation
	numOutputs = segmentObjects(tempImg,SEG_THRESHOLD);
	outImgs = extractSegments(tempImg);

	//free resources
	cvReleaseImage(&tempImg);
	delete logAmp;
	delete fftPhase;
	delete specResidual;

	return numOutputs;
}
/* 
This adaptive filtering is based on the highest intensity
and the average intensity in the image. 
It is more restrictive than the ones offered by OpenCV.
*/
void Saliency::adaptiveFilter(IplImage* img){
	int size	= img->width * img->height;
	int sum		= 0;
	int average = 0;
	int filter	= 0;
	
	//scale filtering with respect to the highest intensity
	uchar maxVal	= imageMax(img);
	uchar scale	=  (uchar) (maxVal * SCALE_FACTOR);

	//averaging of all filter values
	int k = 0;
	for(int i = 0; i < size; ++i){
		uchar c = img->imageData[i];
		if(c > 0){
			sum += c;	
			k++;
		}
	}
	average = sum/k;
	
	//intensity to reduce
	filter = min(255,average + scale);
	for(int i = 0 ; i < size; ++i){
		img->imageData[i] = max(0, min(255,(uchar)img->imageData[i] - filter));
	}
}
/* Computes the saliency map */
IplImage* Saliency::doSaliencyMap(double* specResidual, double* fftPhase){
	
	//preparing the input data for inverse fft
	for(int i = 0;i<fftInputSize;i++){
		std::complex<double> item = std::complex<double>(specResidual[i],fftPhase[i]);
		item = exp(item);
		data_out[i][0] = std::real(item);
		data_out[i][1] = std::imag(item);
	}

	// inverse DFT
	ifft2();
	
	//squaring the result to increase variance
	double size = fftInput->width * fftInput->height;
	for(int i = 0;i<fftInputSize;++i){
		ifft[i][0] = ifft[i][0]*ifft[i][0];
	}
	
	//normalizing the image with intensity between min and max as 0.0 to 1.0
	double maxVal = 0x0;
	double minVal = 0xffff;
	for(int i = 0 ; i < size; ++i){
		if(ifft[i][0] > maxVal)
			maxVal = ifft[i][0];
		if(ifft[i][0] < minVal)
			minVal = ifft[i][0];
	}
	maxVal -= minVal;

	//create intensity range
	for(int i = 0 ; i < size; ++i){
		ifft[i][0] = ifft[i][0] - minVal;
		ifft[i][0] = ifft[i][0] / maxVal;
	}
	
	//create saliency map image
	IplImage* img = cvCreateImage(cvSize(fftInput->width,fftInput->height),IPL_DEPTH_8U,1);
	for(int i = 0 ; i < size ; ++i){
		img->imageData[i] = ( uchar )(ifft[i][0] * 255);
	}

	//apply post-processing smoothing filter
	diskSmooth(img);
	
	return img;
}

/*
This is a rounded disk smoothing kernel to offer
a more circular smoothing filter compared to regular
square filter kernels
*/
void Saliency::diskSmooth(IplImage* img){
	cvFilter2D(img,img,diskKernel);
}

/*
Segment the image into segments with markers with a threshold value
The first segment will have marker value of 255
and the next will be 254 and so on.
Returns the number segments found.

*/
int Saliency::segmentObjects(IplImage* img,int threshold){
	int nMarker= 0;
	int marker = 255;
		
	CvPoint pt = findHighestIntensity(img);
	uchar highInt = img->imageData[pt.y * img->widthStep + pt.x];
	while(highInt > threshold){
		//Flood every connected points with respect to the seed point
		cvFloodFill(img,pt,cvScalar(marker-nMarker),cvScalar(highInt-1),cvScalar(0),NULL,CV_FLOODFILL_FIXED_RANGE);
		++nMarker;
		pt = findHighestIntensity(img,marker-nMarker);
		highInt = img->imageData[pt.y * img->widthStep + pt.x];
	}
	return nMarker;
}

/*
	Extract the number of segments in an image
*/
IplImage** Saliency::extractSegments(IplImage* img){
	int marker	= 255;
	int nMarker	= 0;
	int width	= img->width;
	int step	= img->widthStep;
	int height	= img->height;
	int size	= width * height;
	uchar c;
	
	int maxX[255], maxY[255], minX[255],minY[255];
	do{
		maxX[nMarker] = -1;
		maxY[nMarker] = -1;
		minX[nMarker] = -1;
		minY[nMarker] = -1;
		//finding min and max Y values
		for(int i = 0 ; i < size ; ++i){
			c = img->imageData[i];
			if(c == marker){	
				maxY[nMarker] = i;
				if(minY[nMarker] == -1){
					minY[nMarker] = i;
				}
			}
		}
		
		//finding min and max X values
		for(int i = 0 ; i < width ; ++i){
			for(int j = 0 ; j < height ; ++j){
				c = img->imageData[i + j * width];
				if(c == marker){	
					maxX[nMarker] = i + j * width;
					if(minX[nMarker] == -1){
						minX[nMarker] = i + j * width;
					}
				}
			}
		}
		
		//next marker
		marker = marker - 1;
		++nMarker;
	}while (maxX[nMarker-1] != minX[nMarker-1] && maxY[nMarker-1] != minY[nMarker-1]);
	--nMarker;

	//extracting each blobs from original image
	IplImage** img_objs = new IplImage*[nMarker];
	for(int i = 0 ; i < nMarker ; i ++){
		CvRect roi;
		IplImage *result;
		double scaleX, scaleY;
		int x1,x2,y1,y2;

		//-1 and +1 to increase the returned image size
		//mins
		x1 = max(0,(minX[i]%width				-1));
		y1 = max(0,(round(minY[i]/height)		-1));
		//maxs
		x2 = min(width,(maxX[i]%width			+1));
		y2 = min(height,(round(maxY[i]/height)	+1));
		
		//scale the image back to original input size
		scaleX = inImg->width/(double)RESIZE_X;
		scaleY = inImg->height/(double)RESIZE_Y;
		
		roi.x = round(x1 * scaleX);
		roi.y = round(y1 * scaleY);
		roi.width	=  round(x2 * scaleX) - round(x1 * scaleX);
		roi.height	=  round(y2 * scaleY) - round(y1 * scaleY);

		//extracting sub image
		cvSetImageROI(inImg,roi);
		result = cvCreateImage( cvSize(roi.width, roi.height),
		inImg->depth, inImg->nChannels );
		cvCopy(inImg,result);
		cvResetImageROI(inImg);

		img_objs[i] = result;
	}

	return img_objs;
}

/* return the point with the highest intensity*/
CvPoint Saliency::findHighestIntensity(IplImage * img){
	return findHighestIntensity(img,0xff);
}

/* return the point with the highest intensity with a limit */
CvPoint Saliency::findHighestIntensity(IplImage * img , uchar limit){
	int width	= img->width;
	int height	= img->height;
	int step	= img->widthStep;
	int size	= width * height;
	int x	= 0;
	int y	= 0;
	uchar c, maxVal = 0;

	for(int i = 0; i < size ; ++i){
		c = img->imageData[i];
		if(c > maxVal && c <= limit){
			maxVal = c;
			x = i % width;
			y = i / height;
		}
	}

	return cvPoint(x,y);
}

/* computing FFT using FFTW*/
void Saliency::fft2(){
	
	int width  		= fftInput->width;
    int height		= fftInput->height;
	int step		= fftInput->widthStep;
	uchar* img_data = ( uchar* ) fftInput->imageData;
	
	/* load img1's data to fftw input */
	double intensity = 0xff;
	int i,j,k;
    for( i = 0, k = 0 ; i < height ; i++ ) {
        for( j = 0 ; j < width ; j++ ) {
            data_in[k][0] = ( double )(img_data[i * step + j]/intensity);
            data_in[k][1] = 0.0;
            k++;
        }
    }
	
	/* perform FFT */
    fftw_execute( plan_f );
}
void Saliency::ifft2(){
	/* perform IFFT */
    fftw_execute( plan_b );
	
	/* normalizing the result of ifft */
	double size = fftInputSize;
	for(int i = 0;i < fftInputSize; ++i){
		ifft[i][0] = ifft[i][0]/size;
	}
}
/* Computing Spectral Residual */
double* Saliency::spectralResidual(double* in){

	double* specRes = new double[fftInputSize];
	double* temp	= new double[fftInputSize];
	int widthstep	= fftInput->widthStep;
	int height		= fftInput->height;
	double sum,average;
	
	//perform averaging convolution
	double div = 9.0;
	for(int i = 0;i<fftInputSize;++i){
		average =  in[i];
		if(	i > widthstep &&  
			i < (height-1) * widthstep &&
			i		%widthstep != 0 &&
			(i+1)	%widthstep != 0){
			sum = 0;
			sum +=	in[i-widthstep-1] + in[i-widthstep] + in[i-widthstep+1];
			sum +=	in[i-1] + in[i] + in[i+1];
			sum +=	in[i+widthstep-1] + in[i+widthstep] + in[i+widthstep+1];
			average = sum/div;
		}
		temp[i] = average;
	}

	for(int i = 0;i<fftInputSize;++i){
		specRes[i] = in[i] - temp[i];
	}
	delete temp;
	return specRes;
}
/* Computing the log Amplitude fft */
double* Saliency::logAmplitude(fftw_complex* in){

	double *data = abs_complex(in);
	
	for(int i = 0;i < fftInputSize;++i){
		data[i] = log(data[i]);
	}
	
	return data;
}
/* Computing the angle of the fft */
double* Saliency::phase(fftw_complex* in){
	
	double* out = new double[fftInputSize];
	for(int i = 0;i < fftInputSize;++i){
		out[i] = atan2(in[i][1],in[i][0]);		
	}
	return out;
	
}
/* performs absolute of complex numbers with array input*/
double* Saliency::abs_complex(fftw_complex* in){
	
	double* out = new double[fftInputSize];
	for(int i = 0;i < fftInputSize ;++i){
		out[i] = abs_complex(in[i][0],in[i][1]);
	}
	return out;
}

/* performs absolute of complex numbers */
double Saliency::abs_complex(double x, double i){
	return sqrt(x*x + i*i);
}

/* returns the highest intensity of an image*/
uchar Saliency::imageMax(IplImage* img){
	int size = img->width*img->height;
	uchar maxVal = 0x0;
	for(int i = 0 ; i < size; ++i){
		if(img->imageData[i] > maxVal)
			maxVal = img->imageData[i];
		
	}
	return maxVal;
}

int main( int argc, char** argv ){
    
    // Check if an input image is passed
    if (argc <= 1) {
        std::out <<  << std::endl;
        return -1;
    }
    inImage = argv[1]

	IplImage* img1 = cvLoadImage( inImage, CV_LOAD_IMAGE_GRAYSCALE ); // TODO: Path Error Checking.
	
	Saliency* sal = new Saliency(img1);
	sal->findObjects();

	
	IplImage* map = sal->getSaliencyMap();
	IplImage** outs = sal->getOutputImages();
	cvNamedWindow("A");
	cvShowImage("A",sal->getSaliencyMap());
	
	for(int i = 0; i < sal->numFound(); ++i){
		std::stringstream out;
		out << i;
		cvNamedWindow(out.str().c_str());
		cvShowImage(out.str().c_str(),outs[i]);
	}	
    
    bool done = false;
	while(!done)
	{	
		//WAIT TIME
		int keyCode = cvWaitKey(1);
	
		if('q' == (char)keyCode || (char) keyCode == 27)
		{
				done = true;
		}
	}
    
	cvReleaseImage(&img1);
    return 0;
}
