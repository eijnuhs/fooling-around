#include "radian.hpp"
#include <math.h>

Radian::Radian(double angle) : m_angle(Radian::angleMod(angle))
{
  // Nothing
}

// from a normal vector
Radian::Radian(double nx, double ny) : m_angle(atan2(ny,nx))
{
  // Nothing
}

Radian::~Radian()
{
  // Nothing
}

double Radian::dbl() const
{
  return m_angle;
}
  
// add or subtract an angle.
Radian & Radian::operator+=(Radian const & rhs)
{
  m_angle = angleMod(m_angle + rhs.m_angle);
  return *this;
}
Radian & Radian::operator+ (Radian const & rhs) const
{
  Radian angle(m_angle);
  return angle += rhs;
}
Radian & Radian::operator-=(Radian const & rhs)
{
  m_angle = angleMod(m_angle - rhs.m_angle);
  return *this;
}
Radian & Radian::operator- (Radian const & rhs) const
{
  Radian angle(m_angle);
  return angle -= rhs;
}
// times a scalar.
Radian & Radian::operator*=(double const rhs)
{
  m_angle = angleMod(m_angle * rhs);
  return *this;
}
Radian & Radian::operator* (double const rhs) const
{
  Radian angle(m_angle);
  return angle *= rhs;
}
Radian & Radian::operator/=(double const rhs)
{
  m_angle = angleMod(m_angle / rhs);
  return *this;
}
Radian & Radian::operator/ (double const rhs) const
{
  Radian angle(m_angle);
  return angle /= rhs;
}


// conversion to degrees.
double Radian::degrees() const
{
  return radiansToDegrees(m_angle);
}


// static utility functions
double Radian::angleMod(double angle)
{
  return angle - (M_2PI * rint(angle / (M_2PI) ));
}
double Radian::angleDiff(double angle1, double angle2)
{
  return angleMod(angle1 - angle2);
}
double Radian::radiansToDegrees(Radian rad)
{
  return rad.dbl() * DEG_PER_RAD;
}
Radian Radian::degreesToRadians(double degrees)
{
  return degrees / DEG_PER_RAD;
}

double Radian::magnitude() const
{
  return m_angle > 0 ? m_angle : -m_angle;
}

void Radian::normalVector(double & nx, double & ny)  const 
{
  nx = cos(m_angle);
  ny = sin(m_angle);
}

Radian Radian::vectorToAngle(double vx, double vy)
{
  return atan2(vy,vx);
}

std::ostream & operator<<(std::ostream & stream, Radian const & rhs)
{
  stream << rhs.dbl();
  return stream;
}

Radian & Radian::operator=(Radian const & rhs)
{
  m_angle = rhs.m_angle;
  return *this;
}
Radian & Radian::operator=(double const rhs)
{
  m_angle = rhs;
  return *this;
}

bool Radian::operator<(Radian const & rhs) const { return m_angle < rhs.m_angle;}
bool Radian::operator<(double const rhs) const { return m_angle < rhs; }
bool Radian::operator<=(Radian const & rhs) const { return m_angle <= rhs.m_angle;}
bool Radian::operator<=(double const rhs) const { return m_angle <= rhs; }
bool Radian::operator>(Radian const & rhs) const { return m_angle > rhs.m_angle;}
bool Radian::operator>(double const rhs) const { return m_angle > rhs; }
bool Radian::operator>=(Radian const & rhs) const { return m_angle >= rhs.m_angle;}
bool Radian::operator>=(double const rhs) const { return m_angle >= rhs; }
bool Radian::operator==(Radian const & rhs) const { return m_angle == rhs.m_angle;}
bool Radian::operator==(double const rhs) const { return m_angle == rhs; }
bool Radian::operator!=(Radian const & rhs) const { return m_angle != rhs.m_angle;}
bool Radian::operator!=(double const rhs) const { return m_angle!= rhs; }

bool operator< (double const lhs, Radian const & rhs){return lhs < rhs.dbl();}
bool operator<=(double const lhs, Radian const & rhs){return lhs <= rhs.dbl();}
bool operator> (double const lhs, Radian const & rhs){return lhs > rhs.dbl();}
bool operator>=(double const lhs, Radian const & rhs){return lhs >= rhs.dbl();}
bool operator==(double const lhs, Radian const & rhs){return lhs == rhs.dbl();}
bool operator!=(double const lhs, Radian const & rhs){return lhs != rhs.dbl();}

