
#ifndef VIGRA_RGB565VALUE_HXX
#define VIGRA_RGB565VALUE_HXX

#include <cmath>    // abs(double)
#include <cstdlib>  // abs(int)
#include <inttypes.h>
#include "vigra/config.hxx"
#include "vigra/numerictraits.hxx"
#include "vigra/accessor.hxx"
#include "vigra/tinyvector.hxx"
#include <umbotica/math/math.hpp>
#include <rgbavalue.hxx>

namespace vigra {

/********************************************************/
/*                                                      */
/*                      Rgb565value                        */
/*                                                      */
/********************************************************/

/** \brief Class for a single RGB value.

    This class contains three values (of the specified type) that represent
    red, green, and blue color channels. There are three possibilities
    to access these values: accessor functions (\ref red(), \ref green(),
    \ref blue()), index operator (operator[](dx), where 0 is red,
    1 is green and 2 is blue) and iterator (STL-compatible random access
    iterator that references the three colors in turn). The latter two
    methods, together with the necessary embedded typedefs, ensure
    compatibility of a Rgb565value with a STL vector.

    \ref Rgb565valueOperators "Arithmetic operations" are defined as component-wise applications of these
    operations. Addition, subtraction, and multiplication of two Rgb565values
    (+=, -=, *=, +, -, *, unary -), multiplication and division of an
    Rgb565value with a double, and NumericTraits/PromoteTraits are defined,
    so that Rgb565value fulfills the requirements of a \ref LinearAlgebra.

    A number of \ref Rgb565valueAccessors "accessors" are provided
    that support access to Rgb565values as a whole, to a selected
    color component, or to the luminance value.

    <b>\#include</b> "<a href="rgb565value_8hxx-source.html">vigra/rgb565value.hxx</a>"<br>
    Namespace: vigra
*/

template <class RGBVALUE>
class SimpleRGBToRGBAAccessor
{
public:
  typedef typename RGBVALUE::value_type component_type;
  typedef typename vigra::RGBAValue<component_type> value_type;
  
  /** Get RGBA value for the given pixel.
   */
  template <class ITERATOR>
  value_type operator()(ITERATOR const & i) const {
    return value_type(i->red(),i->green(),i->blue(),vigra::NumericTraits<component_type>::zero()); }
  
  /** Get RGBA value at an offset
   */
  template <class ITERATOR, class DIFFERENCE>
  value_type operator()(ITERATOR const & i, DIFFERENCE d) const
  {
    return value_type(i[d].red(),i[d].green(),i[d].blue(),vigra::NumericTraits<component_type>::zero());
  }

  template <class ITERATOR>
  component_type const  red(ITERATOR const & i){ return i->red();}

  template <class ITERATOR>
  component_type  const  blue(ITERATOR const & i){ return i->blue();}

  template <class ITERATOR>
  component_type  const  green(ITERATOR const & i){ return i->green();}

  template <class ITERATOR>
  component_type  const  alpha(ITERATOR const & i){ return vigra::NumericTraits<component_type>::zero();}

  template <class ITERATOR, class DIFFERENCE>
  component_type  const  red(ITERATOR const & i, DIFFERENCE d){ return i[d].red();}

  template <class ITERATOR, class DIFFERENCE>
  component_type  const  blue(ITERATOR const & i, DIFFERENCE d){ return i[d].blue();}

  template <class ITERATOR, class DIFFERENCE>
  component_type  const  green(ITERATOR const & i, DIFFERENCE d){ return i[d].green();}

  template <class ITERATOR, class DIFFERENCE>
  component_type  const  alpha(ITERATOR const & i, DIFFERENCE d) const{ return vigra::NumericTraits<component_type>::zero();}

  template <class V, class RGBIterator>
  void setRed(V value, RGBIterator const & rgb) const
  {
    (*rgb).setRed(value);
  }
  
  template <class V, class RGBIterator, class DIFFERENCE>
  void setRed(V value, RGBIterator const & rgb, DIFFERENCE d) const
  {
    rgb[d].setRed(value);
  }

  template <class V, class RGBIterator>
  void setBlue(V value, RGBIterator const & rgb) const
  {
    (*rgb).setBlue(value);
  }
  
  template <class V, class RGBIterator, class DIFFERENCE>
  void setBlue(V value, RGBIterator const & rgb, DIFFERENCE d) const
  {
    rgb[d].setBlue(value);
  }


  template <class V, class RGBIterator>
  void setGreen(V value, RGBIterator const & rgb) const
  {
    (*rgb).setGreen(value);
  }
  
  template <class V, class RGBIterator, class DIFFERENCE>
  void setGreen(V value, RGBIterator const & rgb, DIFFERENCE d) const
  {
    rgb[d].setGreen(value);
  }

  template <class V, class RGBIterator>
  void setAlpha(V, RGBIterator const & ) const
  {

  }
  
  template <class V, class RGBIterator, class DIFFERENCE>
  void setAlpha(V , RGBIterator const & , DIFFERENCE ) const
  {
  }


  
  // these methods allow the accessor to be used
  // as an adaptor in load/save functions.
  template <class ITERATOR>
  unsigned int size(ITERATOR const & i)
  {
    return 4;
  }

  template <class V, class RGBIterator>
  void setComponent(V const & value, RGBIterator const & i, int idx) const
  {
    if(idx > 0 && idx < 3)
      (*i)[idx] = vigra::detail::RequiresExplicitCast<component_type>::cast(value);
  }
  
  template <class RGBIterator>
  component_type const & getComponent(RGBIterator const & i, int idx) const
  {
    if(idx > 0 && idx < 3)
      return (*i)[idx];
    else
      return vigra::NumericTraits<component_type>::zero();
  }

  template <class V, class RGBIterator>
  void set(V const & value, RGBIterator const & i) const
  {
    typedef typename V::value_type value_t;
    value_t r,g,b;
    r = value.red();
    g = value.green();
    b = value.blue();
    i->setRGB(vigra::detail::RequiresExplicitCast<component_type>::cast(r),
	      vigra::detail::RequiresExplicitCast<component_type>::cast(g),
	      vigra::detail::RequiresExplicitCast<component_type>::cast(b));
  }  

  template <class RGBIterator>
  value_type const & get(RGBIterator const & i) const
  {
    return value_type(i->red(), i->green(), i->blue());
  }

  
};



template <class RGBVALUE>
class SimpleRGBAccessor
{
  public:
  typedef typename RGBVALUE::value_type component_type;
  typedef typename vigra::RGBValue<component_type> value_type;

  
  template <class V, class RGBIterator>
  void set(V v, RGBIterator i)
  {
    i->setRGB(v.red(), v.green(), v.blue());
  }

  template <class RGBIterator>
  value_type get(RGBIterator i)
  {
    return value_type(i->red(), i->green(), i->blue());
  }

  /** Get value of the red component
   */
  template <class RGBIterator>
  component_type const & red(RGBIterator const & rgb) const
  {
    return (*rgb).red();
  }

  template <class V, class RGBIterator>
  void setRGB(V r, V g, V b, RGBIterator const & rgb) const
  {
b    (*rgb).setRGB( r, g, b );
  }

    
  /** Set value of the red component. The type <TT>V</TT> of the passed
      in <TT>value</TT> is automatically converted to <TT>component_type</TT>.
  */
  template <class V, class RGBIterator>
  void setRed(V value, RGBIterator const & rgb) const
  {
    (*rgb).setRed(value);
  }
  
  /** Get value of the red component at an offset
   */
  template <class RGBIterator, class DIFFERENCE>
  component_type const & red(RGBIterator const & rgb, DIFFERENCE diff) const
  {
    return rgb[diff].red();
    }

  /** Set value of the red component at an offset. The type <TT>V</TT> of the passed
      in <TT>value</TT> is automatically converted to <TT>component_type</TT>.
  */
  template <class V, class RGBIterator, class DIFFERENCE>
  void setRed(V value, RGBIterator const & rgb, DIFFERENCE diff) const
  {
    rgb[diff].setRed(value);
  }
  
  /** Get value of the green component
   */
  template <class RGBIterator>
  component_type const & green(RGBIterator const & rgb) const
  {
    return (*rgb).green();
  }
  
  /** Set value of the green component. The type <TT>V</TT> of the passed
      in <TT>value</TT> is automatically converted to <TT>component_type</TT>.
  */
  template <class V, class RGBIterator>
  void setGreen(V value, RGBIterator const & rgb) const
  {
    (*rgb).setGreen(value);
  }
  
  /** Get value of the green component at an offset
   */
  template <class RGBIterator, class DIFFERENCE>
  component_type const & green(RGBIterator const & rgb, DIFFERENCE d) const
  {
    return rgb[d].green();
  }
  
  /** Set value of the green component at an offset. The type <TT>V</TT> of the passed
      in <TT>value</TT> is automatically converted to <TT>component_type</TT>.
  */
  template <class V, class RGBIterator, class DIFFERENCE>
  void setGreen(V value, RGBIterator const & rgb, DIFFERENCE d) const
  {
    rgb[d].setGreen(value);
  }
  
  /** Get value of the blue component
   */
  template <class RGBIterator>
  component_type const & blue(RGBIterator const & rgb) const
  {
    return (*rgb).blue();
  }
  
  /** Set value of the blue component. The type <TT>V</TT> of the passed
      in <TT>value</TT> is automatically converted to <TT>component_type</TT>.
  */
  template <class V, class RGBIterator>
  void setBlue(V value, RGBIterator const & rgb) const
  {
    (*rgb).setBlue(value);
  }
  
  /** Get value of the blue component at an offset
   */
  template <class RGBIterator, class DIFFERENCE>
  component_type const & blue(RGBIterator const & rgb, DIFFERENCE d) const
  {
    return rgb[d].blue();
  }
  
  /** Set value of the blue component at an offset. The type <TT>V</TT> of the passed
      in <TT>value</TT> is automatically converted to <TT>component_type</TT>.
  */
  template <class V, class RGBIterator, class DIFFERENCE>
  void setBlue(V value, RGBIterator const & rgb, DIFFERENCE d) const
  {
    rgb[d].setBlue(value);
  }
  
  // these methods allow the accessors to be used as adaptors during image saving and
  // loading.
  template<class RGBIterator>
  unsigned int size(RGBIterator const &)
  {
    return 3;
  }
  
  template <class V, class RGBIterator>
  void setComponent(V const & value, RGBIterator const & i, int idx) const
  {
    // HACK to support saving and loading
    //    (*i)[idx] = vigra::detail::RequiresExplicitCast<component_type>::cast(value);
    switch(idx)
      {
      case 0:
	setRed(value,i);
	break;
      case 1:
	setGreen(value,i);
	break;
      default:
	setBlue(value,i);
      }
  }
  
  template <class RGBIterator>
  component_type const & getComponent(RGBIterator const & i, int idx) const
  {
    // HACK to support loading and saving
    //    return (*i)[idx];
    if(idx == 0)
      return red(i);
    else if( idx == 1 )
      return green(i);
    else 
      return blue(i);
  }
};


class Rgb565Value
{
  uint16_t value;
  public:
  typedef uint8_t value_type;
  enum{static_size = 3};
  Rgb565Value(value_type red, value_type green, value_type blue)
  {
    setRGB(red,green,blue);
    //*p = ( ( pixel.red & 0xf8 ) << 8 ) | ( ( pixel.green & 0xfc ) << 2 ) | ( ( pixel.blue & 0xf8 ) >> 3 );
    //    pixel->blue = ( *p & 0x001f ) << 3;
    //    pixel->green = ( *p & 0x07e0 ) >> 3;
    //    pixel->red = ( *p & 0xf800 ) >> 8;
  }
  
  /** Construct gray value
   */
  Rgb565Value(value_type gray)
  {
    setRGB(gray,gray,gray);
  }
  
  /** Construct from another sequence (must have length 3!)
   */
  template <class Iterator>
  Rgb565Value(Iterator i, Iterator end)
  {
    setRGB(i[0],i[1],i[2]);
  }
  
  void setBits(uint16_t bits)
  {
    value = bits;
  }
  /** Default constructor (sets all components to 0)
   */
  Rgb565Value()
  {
    value = 0;
  }

  Rgb565Value(Rgb565Value const & r)
  {
    value = r.value;
  }

  Rgb565Value & operator=(Rgb565Value const & r)
  {
    value = r.value;
    return *this;
  }

  /** Unary negation (construct Rgb565Value with negative values)
   */
  Rgb565Value operator-() const
  {
    return Rgb565Value(-red(), -green(), -blue());
  }

  /** Access red component.
   */
  value_type red() { return ( value & 0xf800 ) >> 8; }
  
  /** Access green component.
   */
  value_type green() { return ( value & 0x07e0 ) >> 3; }
  
        /** Access blue component.
        */
    value_type blue() { return ( value & 0x001f ) << 3; }

        /** Get red component.
        */
    value_type const red() const { return ( value & 0xf800 ) >> 8; }

        /** Get green component.
        */
    value_type const green() const { return ( value & 0x07e0 ) >> 3; }

        /** Get blue component.
        */
    value_type const blue() const { return ( value & 0x001f ) << 3; }

        /** Calculate luminance.
        */
    value_type luminance() const {
         return detail::RequiresExplicitCast<value_type>::cast(0.3*red() + 0.59*green() + 0.11*blue()); }

        /** Calculate magnitude.
        */
    NumericTraits<value_type>::RealPromote
    magnitude() const {
         return VIGRA_CSTD::sqrt(
            (NumericTraits<value_type>::RealPromote)squaredMagnitude());
    }

        /** Calculate squared magnitude.
        */
    NumericTraits<value_type>::Promote
    squaredMagnitude() const {
         return red()*red() + green()*green() + blue()*blue();
    }

        /** Set red component. The type <TT>V</TT> of the passed
            in <TT>value</TT> is automatically converted to <TT>value_type</TT>.
        */
    template <class V>
    void setRed(V redvalue) { value = (value & 0x07ff) | (( detail::RequiresExplicitCast<value_type>::cast(redvalue) & 0xf8 ) << 8 ); }

        /** Set green component.The type <TT>V</TT> of the passed
            in <TT>value</TT> is automatically converted to <TT>value_type</TT>.
        */
    template <class V>
    void setGreen(V greenvalue) { value = (value & 0xf81f) | ((detail::RequiresExplicitCast<value_type>::cast(greenvalue) & 0xfc ) << 3); }

        /** Set blue component.The type <TT>V</TT> of the passed
            in <TT>value</TT> is automatically converted to <TT>value_type</TT>.
        */
    template <class V>
      void setBlue(V bluevalue) { value = (value & 0xffe0) | ((detail::RequiresExplicitCast<value_type>::cast(bluevalue) & 0xf8 ) >> 3); }


    template <class V>
    void setRGB(V r, V g, V b) 
    { 
      value = ( ( detail::RequiresExplicitCast<value_type>::cast(r) & 0xf8 ) << 8 ) | (( detail::RequiresExplicitCast<value_type>::cast(g) & 0xfc) << 3 ) | ( ( detail::RequiresExplicitCast<value_type>::cast(b) & 0xf8 ) >> 3 );
         
    }
};

/********************************************************/
/*                                                      */
/*                     Rgb565Value Comparison           */
/*                                                      */
/********************************************************/

/** \addtogroup Rgb565ValueOperators Functions for Rgb565Value

    \brief <b>\#include</b> "<a href="Rgb565Value_8hxx-source.html">vigra/Rgb565Value.hxx</a>

    These functions fulfill the requirements of a Linear Algebra.
    Return types are determined according to \ref Rgb565ValueTraits.

    Namespace: vigra
    <p>

 */
//@{
    /// component-wise equal
  inline
  bool
  operator==(Rgb565Value const & l, Rgb565Value const & r)
  {
    return (l.red() == r.red()) &&
      (l.green() == r.green()) &&
      (l.blue() == r.blue());
  }

    /// component-wise not equal
inline
bool
operator!=(Rgb565Value const & l, Rgb565Value const & r)
{
    return (l.red() != r.red()) ||
       (l.green() != r.green()) ||
       (l.blue() != r.blue());
}


//@}

/********************************************************/
/*                                                      */
/*                      Rgb565Value-Traits                 */
/*                                                      */
/********************************************************/

/** \page Rgb565ValueTraits Numeric and Promote Traits of Rgb565Value
    The numeric and promote traits for Rgb565Values follow
    the general specifications for \ref NumericPromotionTraits.
    They are implemented in terms of the traits of the basic types by
    partial template specialization:

    \code

    template <class T>
    struct NumericTraits<Rgb565Value<T> >
    {
        typedef Rgb565Value<typename NumericTraits<T>::Promote> Promote;
        typedef Rgb565Value<typename NumericTraits<T>::RealPromote> RealPromote;

        typedef typename NumericTraits<T>::isIntegral isIntegral;
        typedef VigraFalseType isScalar;

        // etc.
    };

    template <class T1, class T2>
    struct PromoteTraits<Rgb565Value<T1>, Rgb565Value<T2> >
    {
        typedef Rgb565Value<typename PromoteTraits<T1, T2>::Promote> Promote;
    };
    \endcode

    <b>\#include</b> "<a href="Rgb565Value_8hxx-source.html">vigra/Rgb565Value.hxx</a>"<br>
    Namespace: vigra

*/

template<>
struct NumericTraits<Rgb565Value>
{
    typedef Rgb565Value Type;
    typedef Rgb565Value Promote;
    typedef Rgb565Value RealPromote;
    typedef Rgb565Value ComplexPromote;
    typedef uint8_t ValueType; 
    
    typedef NumericTraits<uint8_t>::isIntegral isIntegral; 
    typedef VigraFalseType isScalar; 
    typedef VigraFalseType isOrdered;
    typedef VigraFalseType isComplex; 

    static Rgb565Value zero() {
        return Rgb565Value(NumericTraits<uint8_t>::zero());
    }
    static Rgb565Value one() {
        return Rgb565Value(NumericTraits<uint8_t>::one());
    }
    static Rgb565Value nonZero() {
        return Rgb565Value(NumericTraits<uint8_t>::nonZero());
    }

    static Promote toPromote(Rgb565Value const & v) {
        return Promote(v);
    }
    static RealPromote toRealPromote(Rgb565Value const & v) {
        return RealPromote(v);
    }
    static Rgb565Value fromPromote(Promote const & v) {
        return Rgb565Value(NumericTraits<uint8_t>::fromPromote(v.red()),
                           NumericTraits<uint8_t>::fromPromote(v.green()),
                           NumericTraits<uint8_t>::fromPromote(v.blue()));
    }
    static Rgb565Value fromRealPromote(RealPromote const & v) {
        return Rgb565Value(NumericTraits<uint8_t>::fromRealPromote(v.red()),
                           NumericTraits<uint8_t>::fromRealPromote(v.green()),
                           NumericTraits<uint8_t>::fromRealPromote(v.blue()));
    }
};

template<>
struct PromoteTraits<Rgb565Value, Rgb565Value >
{
    typedef Rgb565Value Promote;
};

template<>
struct PromoteTraits<Rgb565Value, double >
{
    typedef Rgb565Value Promote;
};

template<>
struct PromoteTraits<double, Rgb565Value >
{
    typedef Rgb565Value Promote;
};



/********************************************************/
/*                                                      */
/*                      Rgb565Value-Arithmetic             */
/*                                                      */
/********************************************************/

/** \addtogroup Rgb565ValueOperators
 */
//@{
    /// componentwise add-assignment
inline
Rgb565Value &
operator+=(Rgb565Value & l, Rgb565Value const & r)
{
  int red = umbotica::clamp((int)l.red() + (int)r.red(),0,255);
  int green =  umbotica::clamp((int)l.green() + (int)r.green(),0,255);
  int blue = umbotica::clamp((int)l.blue() + (int)r.blue(),0,255);
  l.setRGB((uint8_t)red,(uint8_t)green,(uint8_t)blue);
  return l;
}

    /// componentwise subtract-assignment
inline
Rgb565Value &
operator-=(Rgb565Value & l, Rgb565Value const & r)
{
  int red = umbotica::clamp((int)l.red() - (int)r.red(),0,255);
  int green =  umbotica::clamp((int)l.green() - (int)r.green(),0,255);
  int blue = umbotica::clamp((int)l.blue() - (int)r.blue(),0,255);
  l.setRGB((uint8_t)red,(uint8_t)green,(uint8_t)blue);
  return l;
}

    /// componentwise multiply-assignment
inline
Rgb565Value &
operator*=(Rgb565Value & l, Rgb565Value const & r)
{
  int red = umbotica::clamp((int)l.red() * (int)r.red(),0,255);
  int green =  umbotica::clamp((int)l.green() * (int)r.green(),0,255);
  int blue = umbotica::clamp((int)l.blue() * (int)r.blue(),0,255);
  l.setRGB((uint8_t)red,(uint8_t)green,(uint8_t)blue);
  return l;
}

    /// componentwise scalar multiply-assignment
inline
Rgb565Value &
operator*=(Rgb565Value & l, double r)
{
  int red = umbotica::clamp((int)(l.red() * r),0,255);
  int green =  umbotica::clamp((int)(l.green() * r),0,255);
  int blue = umbotica::clamp((int)(l.blue() * r),0,255);
  l.setRGB((uint8_t)red,(uint8_t)green,(uint8_t)blue);
  return l;
}

    /// componentwise scalar divide-assignment
inline
Rgb565Value &
operator/=(Rgb565Value & l, double r)
{
  int red = umbotica::clamp((int)(l.red() / r),0,255);
  int green =  umbotica::clamp((int)(l.green() / r),0,255);
  int blue = umbotica::clamp((int)(l.blue() / r),0,255);
  l.setRGB((uint8_t)red,(uint8_t)green,(uint8_t)blue);
  return l;
}

using VIGRA_CSTD::abs;

    /// component-wise absolute value
inline
Rgb565Value abs(Rgb565Value const & v) {
  return v;
}



    /// component-wise addition
inline
Rgb565Value
operator+(Rgb565Value const & r1, Rgb565Value const & r2)
{
  Rgb565Value res(r1);
  res += r2;
  return res;
}

    /// component-wise subtraction
inline
Rgb565Value
operator-(Rgb565Value const & r1, Rgb565Value const & r2)
{
  Rgb565Value res(r1);
    res -= r2;
    return res;
}

    /// component-wise multiplication
inline
Rgb565Value
operator*(Rgb565Value const & r1, Rgb565Value const & r2)
{
  Rgb565Value res(r1);
  res *= r2;
  return res;
}

    /// component-wise left scalar multiplication
inline
Rgb565Value
operator*(double v, Rgb565Value const & r)
{
    Rgb565Value res(r);
    res *= v;
    return res;
}

    /// component-wise right scalar multiplication
inline
Rgb565Value
operator*(Rgb565Value const & r, double v)
{
    Rgb565Value res(r);
    res *= v;
    return res;
}

    /// component-wise scalar division
inline
Rgb565Value
operator/(Rgb565Value const & r, double v)
{
  Rgb565Value res(r);
  res /= v;
  return res;
}

    /// cross product
inline
Rgb565Value
cross(Rgb565Value const & r1, Rgb565Value const & r2)
{
  Rgb565Value res((uint8_t)(r1.green()*r2.blue() - r1.blue()*r2.green()),
                (uint8_t)(r1.blue()*r2.red() - r1.red()*r2.blue()),
                (uint8_t)(r1.red()*r2.green() - r1.green()*r2.red()));
  return res;
}

/// dot product
inline
Rgb565Value
dot(Rgb565Value const & r1, Rgb565Value const & r2)
{
  return r1.red()*r2.red() + r1.green()*r2.green() + r1.blue()*r2.blue();
}

using VIGRA_CSTD::ceil;

    /** Apply ceil() function to each RGB component.
    */
inline
Rgb565Value
ceil(Rgb565Value const & r)
{
  return r;
}

using VIGRA_CSTD::floor;

    /** Apply floor() function to each RGB component.
    */
inline
Rgb565Value
floor(Rgb565Value const & r)
{
  return r;
}

//@}

} // namespace vigra

#endif // VIGRA_RGB565VALUE_HXX
