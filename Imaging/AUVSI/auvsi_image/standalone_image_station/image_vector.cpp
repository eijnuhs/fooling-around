#include <umbotica/math/math.hpp>
#include <umbotica/error.hpp>
#include <umbotica/string_routines.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include "image_vector.hpp"
#include <iostream>

using namespace boost::posix_time;

ImageVector::ImageVector() : m_idx(0)
{

}

ImageVector::~ImageVector(){}

void ImageVector::push_back(std::string filename)
{
  boost::mutex::scoped_lock lock(m_mutex);
  m_vector.push_back(filename);
  m_condition.notify_one();
}
  // the bool allows us to pop a filename off the
  // queue only if there is available smoothed telemetry
  // data available.  If there is not, the fn will return 
  // false and retFilename will be unchanged.
bool ImageVector::getNext(std::string & retFilename, struct telemetry & retTelemetry, int increment)
{
  bool success = false;
  boost::mutex::scoped_lock lock(m_mutex);
 
  if(m_vector.size() <= 2)
    {
      std::cout << "ImageVector empty, waiting.\n";
      m_condition.wait(lock);
    }

  if(m_vector.size() > 2)
    {
      m_idx = umbotica::clamp<unsigned int>(m_idx, 0, m_vector.size() - 2);
      std::string filename = m_vector[m_idx];
      try
	{
	  // get the timestamp from the filename.  The replacement is necessary to deal with a
	  // shortcoming in the vigra import/export code that cannot deal with files with more
	  // than one dot.  The file has the ptime dots replaced with underscores.
	  std::string tempString = umbotica::replace(umbotica::basefilename(filename),'_','.');
	  int slashIndex = tempString.find_last_of("/");
	  tempString = tempString.substr(slashIndex + 1, tempString.length() - slashIndex);

	  //std::cout << "temp string: " << tempString << "\n";
	  ptime timestamp = from_iso_string(tempString);
	  //std::cout << "time stamp: " << timestamp << "\n";

	  // Get the telemetry from the timestamp
	  if(smoother.getSmoothed(timestamp, &retTelemetry, Fresh))
	    {
	      success = true;
	    }
	  retFilename = filename;
	}
      catch( const std::exception & e )
	{
	  std::cout << "Exception in ImageQueue::getNext(): " << e.what() << "\n";
	}
    }
  m_idx = umbotica::clamp<int>(m_idx + increment,0,m_vector.size() - 1);
  return success;
}

void ImageVector::signalAll()
{
  boost::mutex::scoped_lock lock(m_mutex);
  m_condition.notify_all();
}

unsigned int ImageVector::size()
{
  boost::mutex::scoped_lock lock(m_mutex);
  return m_vector.size();
}

void ImageVector::setIdx(unsigned int idx)
{
  boost::mutex::scoped_lock lock(m_mutex);
  m_idx = umbotica::clamp<int>(idx,0,m_vector.size());
}

unsigned int ImageVector::idx()
{
  return m_idx;
}
