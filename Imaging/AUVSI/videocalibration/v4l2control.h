#ifndef __V4L2_CONTROL_H
#define __V4L2_CONTROL_H
#include <qvgroupbox.h>

/////// Forward Decalration /////////
class V4L2Device;

class V4L2Control : public QVGroupBox
{
 public:
  V4L2Control(QWidget * parent = 0, const char * name = 0 );
  void init(V4L2Device * videoDevice);
  void hideParent();
 private:
  V4L2Device * videoDevice;
};

#endif





