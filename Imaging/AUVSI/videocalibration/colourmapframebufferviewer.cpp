#include "colourmapframebufferviewer.h"
#include "colourdefinition.h"
#include "colourmap.h"
#include <qstring.h>
#include <qpainter.h>
#include <qfiledialog.h>
#include <qinputdialog.h>
#include "framebuffer.h"

ColourMapFrameBufferViewer::ColourMapFrameBufferViewer( QWidget * parent, char const * name) :
  FrameBufferViewer(parent, name),
  cMap(0)
{
  // 0
}


ColourMapFrameBufferViewer::~ColourMapFrameBufferViewer()
{
  // 0
}

void ColourMapFrameBufferViewer::updateQImageFromFrameBuffer( FrameBuffer * frame )
{
  Pixel pixel;
  //  printName();
  //  std::cout << "CMFBV update QImage: " << (void *)frame << "\n";
  
  ColourDefinition * colour = 0;
  if(cMap)
    {
      colour = cMap->getSelectedColour();
    }
  else
    {
      std::cerr << "ColourMapFrameBufferViewer has no ColourMap\n";
    }
  
  if ( ( frame->type == FrameBuffer::RGB32 ) || ( frame->type == FrameBuffer::RGB565 ) || ( frame->type == FrameBuffer::WORLD_FRAME ) )
    {
      checkImageWidthAndHeight( frame );
      FrameBufferIterator fIter( frame );
#ifdef SAFE_DRAW
      // mutex.lock();
      QMutexLocker qm(&mutex);
#endif
      //      if(frame.fieldNo == V4L2_FIELD_BOTTOM)
      for( unsigned int i = 0; i < frame->height; i++ )
	{
	  uint32_t * p = (uint32_t *) image->scanLine( i );
	  fIter.goRow( i );
	  for( unsigned int j = 0; j < frame->width; j++, fIter.goRight() )
	    {
	      fIter.getPixel( & pixel );
	      if(colour && colour->isMatch(pixel))
		*p++ = qRgb(255,0,0);
	      else
		*p++ = qRgb( pixel.red, pixel.green, pixel.blue );
	    }
	}
      
    }
  else
    {
      std::cerr << "updateQImageFromFrameBuffer: frame->bytesPerPixel " << frame->bytesPerPixel << " type " << frame->type << " is not supported\n";
      if ( image != 0 )
	{
	  delete image;
	}
      image = 0;
      return;
    }

}


void ColourMapFrameBufferViewer::mousePressEvent( QMouseEvent * e)
{
  if(cMap && e->button() == Qt::RightButton)
    {
      cMap->resetSelected();
    }
  else
    {
      updateColour(e);
    }
}

void ColourMapFrameBufferViewer::mouseMoveEvent( QMouseEvent * e )
{
  updateColour(e);
}

void ColourMapFrameBufferViewer::updateColour(QMouseEvent * e)
{
  if(frame && e && cMap)
    {
      ColourDefinition * cd = cMap->getSelectedColour();
      Pixel p;
      int fx,fy;
      QMutexLocker(frame->mutex);
      widgetToFrameCoordinates( e->x(), e->y(), &fx, &fy );
      frame->getPixel(fy,fx, &p);
      p.update();
      cd->addPixel(p);
      updateView();

      //      RawPixel rp(p);
      //      std::cout << "updateColour(QMouseEvent * e) colour: " << rp << "\n";
    }
  
}

void ColourMapFrameBufferViewer::setColourMap(ColourMap * cm)
{
  cMap = cm;
  updateView();
}



void ColourMapFrameBufferViewer::nextColour()
{
  if(cMap)
    {
      cMap->selectNext();
      updateView();
    }
}
void ColourMapFrameBufferViewer::previousColour()
{
  if(cMap)
    {
      cMap->selectPrevious();
      updateView();
    }
}

void ColourMapFrameBufferViewer::updateView()
{
  if(cMap)
    {
      QString s(cMap->getSelectedName().c_str());
      RawPixel rp(cMap->getSelectedAvgColour());
      QColor c(rp.red, rp.green, rp.blue);
      std::cout << "updateView(): colour" << rp << "\n";

      emit newName(s);
      emit newColour(c);
    }
}

void ColourMapFrameBufferViewer::addColour()
{
  if(cMap)
    {
      // get the colour name;
      QString s = QInputDialog::getText ( "Ergo::Input Dialog.", "Please enter the new colour name.");
      // add it to the map
      if(!s.isNull() && !s.isEmpty())
	{
	  cMap->addColour(s.ascii());
	  updateView();
	}
    }
}

void ColourMapFrameBufferViewer::loadColours()
{
  if(cMap)
    {
      QString s = QFileDialog::getOpenFileName(QString(cMap->getLastFilename()) , "Configuration files (*.dat *.cfg)", this, "open file dialog", "Choose a file to open" );
      if(!s.isNull() && !s.isEmpty())
	{
	  cMap->loadFromFile(s.ascii());
	}
    }
}    

void ColourMapFrameBufferViewer::saveColours()
{
  if(cMap)
    {
      QString s = QFileDialog::getSaveFileName( QString(cMap->getLastFilename()), "Configuration files (*.dat *.cfg)", this, "save file dialog", "Choose a filename to save under" );
      if(!s.isNull() && !s.isEmpty())
	{
	  cMap->saveToFile(s.ascii());
	}
      
    }
}


