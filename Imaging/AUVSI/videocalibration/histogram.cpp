#include "histogram.h"
#include <iostream>


Histogram::Histogram(unsigned int init_width, unsigned int init_val) :
  counts(0),
  width(0),
  max(0),
  total(0),
  lowerIdx(0),
  upperIdx(0),
  upperAndLowerInit(false)
{
  initialize(init_width, init_val);
}

Histogram::~Histogram()
{
  delete [] counts;
}

void Histogram::initialize(unsigned int initWidth, unsigned int init_val)
{
  delete [] counts;
  counts = new unsigned int[initWidth];
  width = initWidth;
  reset(init_val);
}
void Histogram::reset(unsigned int init_val)
{
  memset(counts, init_val, width * sizeof(unsigned int));
  max = init_val;
  total = init_val * width;
  if(init_val > 0)
    {
      lowerIdx = 0;
      upperIdx = width - 1;
      upperAndLowerInit = true;
    }
  else
    {
      upperAndLowerInit = false;
    }
}
void Histogram::increment(unsigned int i)
{
  if(i < width)
    {
      counts[i]++;
      if(counts[i] > max)
	max = counts[i];
      total++;
      if(!upperAndLowerInit)
	{
	  upperIdx = lowerIdx = i;
	  upperAndLowerInit = true;
	}
      else
	{
	  upperIdx = (i > upperIdx) ? i : upperIdx;
	  lowerIdx = (i < lowerIdx) ? i : lowerIdx;
	}
    }
  else
    {
      std::cerr << "Histogram::increment attempt to increment out of range.\n";
      std::cerr << "width: " << width << "  i: " << i << "\n";
    }
}

unsigned int Histogram::get(unsigned int i) const
{
  unsigned int retVal = 0;
  if(i < width)
    {
      retVal = counts[i];
    }
  else
    {
      std::cerr << "Histogram::get attempt to get out of range.\n";
      std::cerr << "width: " << width << "  i: " << i << "\n";
    }
  return retVal;
}
void Histogram::set(unsigned int i, unsigned int value)
{
  if(i < width)
    {
      total -= counts[i];
      counts[i] = value;
      total += value;
      if(value > max)
	max = counts[i];
    }
  else
    {
      std::cerr << "Histogram::set attempt to set out of range.\n";
      std::cerr << "width: " << width << "  i: " << i << "\n";
    }
}

unsigned int Histogram::getMax() const
{
  return max;
}

unsigned int Histogram::size() const
{
  return width;
}


Histogram::Histogram(const Histogram & rhs)
{
  copy(rhs);
}
Histogram & Histogram::operator=(const Histogram & rhs)
{
  copy(rhs);
  return *this;
}

void Histogram::copy(const Histogram & rhs)
{
  initialize(rhs.size());
  for(unsigned int i = 0; i < width; ++i)
    {
      counts[i] = rhs.counts[i];
    }
  max = rhs.getMax();
  total = rhs.getTotal();
  upperIdx = rhs.getUpperIdx();
  lowerIdx = rhs.getLowerIdx();
  upperAndLowerInit = rhs.upperAndLowerInit;
}

unsigned int Histogram::getWeighted(unsigned int i) const
{
  return counts[i] * i;
}

unsigned int Histogram::getTotal() const
{
  return total;
}
unsigned int Histogram::getLowerThreshold(unsigned int percent) const
{
  unsigned int threshold = 0; 
  unsigned int amount;
  unsigned int count;

  if( percent == 0)
    threshold = getLowerIdx();
  else if (percent >= 100)
    threshold = getUpperIdx();
  else if( getTotal() > 0)
    {
      amount = (percent * total) / 100;
      count = 0;
      threshold = getLowerIdx() - 1;
      while ( count < amount )
	{
	  count += get(++threshold);
	}
    }

  return threshold;
}
unsigned int Histogram::getUpperThreshold(unsigned int percent) const
{
  unsigned int threshold = 0; 
  unsigned int amount;
  unsigned int count;

  if( percent == 0)
    threshold = getLowerIdx();
  else if (percent >= 100)
    threshold = getUpperIdx();
  else if( getTotal() > 0)
    {
      amount = (percent * total) / 100;
      count = 0;
      threshold = getUpperIdx() + 1;
      while ( count < amount )
	{
	  count += get(--threshold);
	}
    }

  return threshold;
}

unsigned int Histogram::getLowerIdx() const
{
  if(upperAndLowerInit)
    return lowerIdx;
  else
    return 0;
}
unsigned int Histogram::getUpperIdx() const
{
  if(upperAndLowerInit)
    return upperIdx;
  else
    return width - 1;
}
unsigned int Histogram::getRange() const
{
  if(upperAndLowerInit)
    return upperIdx - lowerIdx + 1;
  else
    return 0;
}




