#ifndef UM_IMAGE_AG_CONTAINER_H
#define UM_IMAGE_AG_CONTAINER_H

#include <agimagecontainer.hpp>
#include "umimagetraits.hpp"
#include "umimage.hpp"
#include <umbotica/io/tostring.hpp>
#include <umbotica/math/math.hpp>



using umbotica::clamp;

template<class PIXELTYPE>
class UmImageAgContainer : public AGImageContainer
{
public:
  typedef PIXELTYPE pixel_type;
  typedef UmImage<pixel_type> image_type;
  typedef typename umbotica::PixelTraits<pixel_type>::component_type component_type;
  UmImageAgContainer();
  ~UmImageAgContainer();  
  
  void setImage(const image_type & image);
  unsigned int height();
  unsigned int width();
  std::string imageType();
  std::string red(unsigned int row, unsigned int col);
  std::string green(unsigned int row, unsigned int col);
  std::string blue(unsigned int row, unsigned int col);
  std::string alpha(unsigned int row, unsigned int col);
  std::string otherInfo(unsigned int row, unsigned int col);
  void rgbValue(unsigned int row, unsigned int col, int & red, int & green, int & blue);
  void allInfo(unsigned int row, unsigned int col, std::string & red, std::string & green, std::string & blue, std::string & alpha, std::string & info, int & r, int & g, int & b);
private:
  image_type m_image;
  typename image_type::traverser m_iterator;
  typename image_type::RGBAAccessor m_accessor;
};



template<typename PIXELTYPE>
UmImageAgContainer<PIXELTYPE>::UmImageAgContainer() :
  m_accessor() 
{
  //  std::cout << "UMIAGContainer(): \n" << m_image << "\n";
}

template<typename PIXELTYPE>
UmImageAgContainer<PIXELTYPE>::~UmImageAgContainer(){}

template<typename PIXELTYPE>
void UmImageAgContainer<PIXELTYPE>::setImage(const image_type & image)
{
  // make a deep copy of the image.
  m_image.copyFrom(image); 
  m_iterator = m_image.upperLeft();
}

template<typename PIXELTYPE>
inline unsigned int 
UmImageAgContainer<PIXELTYPE>::height()
{
  return m_image.height();
}
template<typename PIXELTYPE>
inline unsigned int
UmImageAgContainer<PIXELTYPE>::width()
{
  return m_image.width();
}
template<typename PIXELTYPE>
inline std::string 
UmImageAgContainer<PIXELTYPE>::imageType()
{
  return "UmImage<" + umbotica::PixelTraits<pixel_type>::description() + ">";
}

template<typename PIXELTYPE>
inline std::string 
UmImageAgContainer<PIXELTYPE>::red(unsigned int row, unsigned int col)
{
  row = clamp<unsigned int>(row,0,m_image.height() - 1);
  col = clamp<unsigned int>(col,0,m_image.width() - 1);
  typename image_type::traverser i = m_iterator + vigra::Diff2D(col,row);
  return boost::lexical_cast<std::string>(m_accessor.red(i));
}

template<typename PIXELTYPE>
inline std::string
UmImageAgContainer<PIXELTYPE>::green(unsigned int row, unsigned int col)
{
  row = clamp<unsigned int>(row,0,m_image.height() - 1);
  col = clamp<unsigned int>(col,0,m_image.width() - 1);

  typename image_type::traverser i = m_iterator + vigra::Diff2D(col,row);
  return boost::lexical_cast<std::string>(m_accessor.green(i));
}

template<typename PIXELTYPE>
inline std::string 
UmImageAgContainer<PIXELTYPE>::blue(unsigned int row, unsigned int col)
{
  row = clamp<unsigned int>(row,0,m_image.height() - 1);
  col = clamp<unsigned int>(col,0,m_image.width() - 1);

  typename image_type::traverser i = m_iterator + vigra::Diff2D(col,row);
  return boost::lexical_cast<std::string>(m_accessor.blue(i));
}

template<typename PIXELTYPE>
inline std::string 
UmImageAgContainer<PIXELTYPE>::alpha(unsigned int, unsigned int)
{
  //  row = clamp<unsigned int>(row,0,m_image.height() - 1);
  //  col = clamp<unsigned int>(col,0,m_image.width() - 1);

  // FIX ME!
  return "fix me";
  //  m_iterator(row,col);
  //  return boost::lexical_cast<std::string>(m_accessor.alpha(m_iterator));
}

template<typename PIXELTYPE>
inline std::string 
UmImageAgContainer<PIXELTYPE>::otherInfo(unsigned int, unsigned int)
{
  return std::string();
}

template<typename PIXELTYPE>
inline void
UmImageAgContainer<PIXELTYPE>::rgbValue(unsigned int row, unsigned int col, int & red, int & green, int & blue)
{
  row = clamp<unsigned int>(row,0,m_image.height() - 1);
  col = clamp<unsigned int>(col,0,m_image.width() - 1);
 
  typename image_type::traverser i = m_iterator + vigra::Diff2D(col,row);


  // debug:
  //  using umbotica::tuple;
  //  std::cout << "rgbValue" << tuple(row,col) << "\n:";
  //  std::cout << "iterator pos: " << tuple(i.x, i.y) << "\n";
  //  std::cout << "Blue: " << vigra::detail::RequiresExplicitCast<uint8_t>::cast(m_accessor.blue(i)) << "\n";
  //  std::cout << "Green: " << vigra::detail::RequiresExplicitCast<uint8_t>::cast(m_accessor.green(i)) << "\n";
  // std::cout << "Red: " << vigra::detail::RequiresExplicitCast<uint8_t>::cast(m_accessor.red(i)) << "\n";
  // end DUBUG
  blue = vigra::detail::RequiresExplicitCast<int>::cast(m_accessor.blue(i));
  green = vigra::detail::RequiresExplicitCast<int>::cast(m_accessor.green(i));
  red = vigra::detail::RequiresExplicitCast<int>::cast(m_accessor.red(i));
}

template<typename PIXELTYPE>
inline void UmImageAgContainer<PIXELTYPE>::allInfo(unsigned int row, unsigned int col, std::string & red, std::string & green, std::string & blue, std::string & alpha, std::string & info, int & r, int & g, int & b)
{
  row = clamp<unsigned int>(row,0,m_image.height() - 1);
  col = clamp<unsigned int>(col,0,m_image.width() - 1);
  typename image_type::traverser i = m_iterator + vigra::Diff2D(col,row);
  red = umbotica::FormatTraits<typename vigra::NumericTraits<component_type>::isIntegral >::format(m_accessor.red(i));
  green = umbotica::FormatTraits<typename vigra::NumericTraits<component_type>::isIntegral >::format(m_accessor.green(i));
  blue = umbotica::FormatTraits<typename vigra::NumericTraits<component_type>::isIntegral >::format(m_accessor.blue(i));
  alpha = "";//boost::lexical_cast<std::string>(m_accessor.red(i));
  info = "";
  b = vigra::detail::RequiresExplicitCast<int>::cast(m_accessor.blue(i));
  g = vigra::detail::RequiresExplicitCast<int>::cast(m_accessor.green(i));
  r = vigra::detail::RequiresExplicitCast<int>::cast(m_accessor.red(i));

}


#endif
