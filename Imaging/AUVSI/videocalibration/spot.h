#ifndef _SPOT_H_
#define _SPOT_H_

#include "framebufferrgb32.h"
#include "run.h"
#include <string>
#include "worldregion.h"

class ColourDefinition;

class Spot : public Trackable 
{
 public:
  Spot(std::string name, double height, double radius, double mmPerPix, ColourDefinition * cd, enum Trackable::TrackableTypes classtype = Trackable::SPOT);
  ~Spot();
  
  // methods required by trackable.
  bool isMe(RunRegion & region, FrameBuffer * inFrame, FrameBufferWorld * worldFrame, unsigned int avgIntensity);
  bool isMe(RunRegion & region, FrameBuffer * inFrame, FrameBufferWorld * worldFrame, unsigned int avgIntensity, bool updatePos);
  bool trackMe(FrameBuffer * inFrame, FrameBufferWorld * worldFrame);
  
 protected:
  // attempts to use the scratchpad to find the spot at
  // this location. wx, wy and region must be objects
  // passed in
  bool findMe(FrameBuffer * inFrame, double pixelDim, double * wx, double * wy, WorldRegion * theRegion = 0);
 private:
  ColourDefinition * colour;
  RegionList regionList;
};
#endif

