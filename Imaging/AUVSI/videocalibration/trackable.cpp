#include "trackable.h"
#include "framespersecond.h"
#include <iostream>
#include "robot.h"
#include "videointerpreter2d.h"
#define SP_DIM 30

const std::string Trackable::TYPE_NAMES[TT_SIZE] = {"Robot","Ball","Spot","TObject","Unknown"};

Trackable::Trackable(enum TrackableTypes type, std::string name, double height, double radius, double mmPerPixel, int subtype) :
  type(type),
  subtype(subtype),
  name(name),
  height(height),
  radius(radius),
  minPixels(255),
  maxPixels(0)
{
  x = y = dx = dy = theta = percentFound = 0;
  everFound = found = false;
  initMinAndMax(mmPerPixel);
  m_scratchpad.initialize( FrameBuffer::RGB32, SP_DIM, SP_DIM );
  for(uint i = 0; i < FA_SIZE; ++i)
    {
      foundArray[i] = false;
    }
  foundIdx = 0;
  
}

Trackable::~Trackable()
{
  // 0
}


void Trackable::setSubtype(int setSubtype)
{
  subtype = setSubtype;
}

void Trackable::setPosition(double x, double y, struct timeval time)
{
  setPosition(x,y,0,time);
}

void Trackable::setPosition(double x, double y, double theta, struct timeval time)
{
  double timediff; // timediff is in microseconds.

  timediff = (double)FramesPerSecond::getTimeDiff(timestamp, time);
  //  std::cout << "Trackable::setPosition: timediff: " << timediff << "\n";
  if(timediff > 0)
    {
      dx = ((x - this->x) / timediff) * M_PER_SEC;
      dy = ((y - this->y) / timediff) * M_PER_SEC;  
      // std::cout << "x, x'-->dx: " << this->x << "," << x << "-->" << dx << "\n";
      //      std::cout << "y, y'-->dy: " << this->y << "," << y << "-->" << dy << "\n";
    }
  this->x = x;
  this->y = y;
  this->theta = theta;

  memcpy(&timestamp, &time,sizeof(struct timeval));

  found = true;
  everFound = true;

  foundArray[foundIdx] = true;
  percentFound += 0.01;
  //  percentFound += 0.02;
  //  if(percentFound > 1)
  //  percentFound = 1;

//   std::cout << "set position " << name << " (" << x << "," << y << 
//     ") vel: (" << dx << "," << dy << ") time: " << timediff << "\n";
    
    
}

void Trackable::predictPosition(double &x, double &y, struct timeval time)
{
  
  double timediff;

  timediff = (double)FramesPerSecond::getTimeDiff(timestamp,time);

  x = this->x + (dx * timediff / MICRO_SEC_PER_SEC);
  y = this->y + (dy * timediff / MICRO_SEC_PER_SEC);
  
//   std::cout << "predict position: " << name << " (" << this->x <<
//     "," << this->y << ") --" << timediff << "--> (" << x << "," << 
//     y << ")\n";
  
}

// a static factory method for creating new Trackable object
/*
Trackable * Trackable::createTrackable(std::string typeName, std::string name, double height, double radius, int subtype)
{
  enum TrackableTypes type = UNKNOWN;
  Trackable * newItem = 0;

  // set the type.
  for(int i = 0; i < TT_SIZE; i++)
    {
      if(typeName == TYPE_NAMES[i])
	{
	  type = (TrackableTypes)i;
	  break;
	}
    }

  // create the new object.
  switch(type)
    {
    case ROBOT:
      newItem = new Robot(name,height,radius,subtype);
      std::cout << "Creating a new robot: " << name << "," << subtype << "\n";
      break;
    case BALL:
      std::cerr << "Trackable::createTrackable: Ball not implemented yet: " << name << "\n";
      break;
    case SPOT:
      std::cerr << "Trackable::createTrackable: Spot not implemented yet: " << name << "\n";      
      break;
    default:
      std::cerr << "Trackable::createTrackable: unknown type: " << typeName << "\n";
      break;
    }

  return newItem;
}
*/

void Trackable::setVideoInterpreter(VideoInterpreter2D * init_vi2d)
{
  vi2d = init_vi2d;
}

void Trackable::setDebugFrame(FrameBufferWorld * init_debugFrame)
{
  debugFrame = init_debugFrame;
}

void Trackable::initForFrame()
{
  
  //  percentFound -= .01;
  //  if(percentFound < 0.0)
  //   percentFound = 0.0;
  found = false;
  foundIdx = (foundIdx + 1) % FA_SIZE;
  if(foundArray[foundIdx])
    {
      percentFound -= .01;
      foundArray[foundIdx] = false;
    }
  //  std::cout << "initForFrame(): " << name << " " << percentFound <<"\n";
}

VideoInterpreter2D * Trackable::vi2d = 0;
FrameBufferWorld * Trackable::debugFrame = 0;


std::ostream & operator<<(std::ostream & stream, const Trackable & track)
{
  stream << track.name << " (" << track.x << "," << track.y << ") vel: (" << 
    track.dx << "," << track.dy << ") theta: " << track.theta << " found:" <<
    track.percentFound << "\n";

  return stream;
}

void Trackable::getScreenCoordinates(int * sx, int * sy)
{
  Trackable::vi2d->worldToScreenCoordinates( x, y, 0, sx, sy );
}

// Note: it is inconvienient to account for height during 
// tracking because it uses a low-resolution interpolated 
// image.  So, we adjust height only at the output stage.

void Trackable::output (std::ostream & os)
{ 
  double wx=0.0,wy=0.0,vx = 0.0, vy = 0.0;
  vx = isnan (dx) ? 0.0 : dx;
  vy = isnan (dy) ? 0.0 : dy;
  
  if(everFound)
    getAdjustedCoordinates(&wx,&wy);

  os << (int)type << ' ' << name << ' ' << (found ? "Found" : "NoFnd") << ' '
    << wx << ' ' << wy << ' ' << height << ' '
    << theta << ' ' << vx << ' ' << vy << "\n";
}

void Trackable::getAdjustedCoordinates(double * x, double * y)
{
  double sx,sy; // screen coordinates

  if(found)
    {
      Trackable::vi2d->worldToScreenCoordinates(this->x,this->y,0.0,&sx,&sy,false);
      Trackable::vi2d->screenToWorldCoordinates(sx,sy,height,x,y, false);
    }

}

void Trackable::initMinAndMax(double mmPerPix)
{
  if(mmPerPix > 0)
    {
      minPixels = (Geometry::circlePixelArea(radius, mmPerPix) * 40)/ 100;
      maxPixels = (Geometry::squarePixelArea(radius * 2, mmPerPix) * 250) / 100;

      //      std::cout << "mmperpix: " << mmPerPix << "\n";
      //      std::cout << "area: " << area << "\n";
      //      std::cout << "bbarea: " << bbarea << "\n";
      std::cout << "Min and max pixels for object " << Trackable::getName() << " with radius " << radius << ": (" << minPixels << "," << maxPixels << ")\n";
      std::cout << "mmPerPix: " << mmPerPix << "\n";
    }
}

void Trackable::maskObject(FrameBufferWorld * worldFrame)
{
  double wx = getX();
  double wy = getY();
  unsigned int lrx,lry;
  double sx,sy,ex,ey;
  unsigned int startX,endX,startY,endY;
  sx = (wx - getRadius());
  sy = (wy - getRadius());
  ex = (wx + getRadius());
  ey = (wy + getRadius());
  
  worldFrame->worldToBufferCoordinates(sx,sy,&startX, &startY);
  worldFrame->worldToBufferCoordinates(ex,ey,&endX, &endY);
  
  Geometry::minMax(startX,endX,&startX,&endX);
  Geometry::minMax(startY,endY,&startY,&endY);

  endY++;
  endX++;

  // now I should erase myself from the world frame.
  if(!worldFrame->worldToBufferCoordinates(wx, wy, &lrx, &lry ))
    {
      //		  std::cout << "Erasing object from interpolated frame\n";
      startX = Geometry::clamp(0,startX, worldFrame->width);
      endX = Geometry::clamp(0,endX, worldFrame->width);
      startY = Geometry::clamp(0,startY, worldFrame->height);
      endY = Geometry::clamp(0,endY, worldFrame->height);
      
      //       std::cout << "buffer: start: (" << startX << "," << startY << ") end: (" 
      //	 << endX << "," << endY << ")\n";
      //  std::cout << "world: start: (" << sx << "," << sy << ") end: (" 
      //		 << ex << "," << ey << ")\n";
      
      FrameBufferIterator fbit(worldFrame);
      for(unsigned int i = startY; i < endY; i++)
	{
	  fbit.goPosition(i, startX);
	  for(unsigned int j = startX; j < endX; j++)
	    {
	      fbit.setPixel(RawPixel(0,0,0));
	      fbit.goRight();
	    }
	}
    }
}
