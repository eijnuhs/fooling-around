#include <iostream>
#include "quicktimeProcessing.hpp"
#include "boost/shared_array.hpp"
#include <string>

QuickTimeProcessing::QuickTimeProcessing()
{
  camera = NULL;
  width = 640;
  height = 480;
  fps = 0;
}

QuickTimeProcessing::~QuickTimeProcessing()
{

  if ( camera )
    {
      if ( camera->stillRunning() )
	{
	  camera->stopCamera();
	}
      
      camera->closeCamera();
      delete camera;
    }
}

int QuickTimeProcessing::initialize()
{
  int failure = 0;

  if ( !camera )
    {
      camera = cameraTool::findCamera();
      
      if ( camera )
	{
	  if ( camera->initCamera( width, height, true ) )
	    {
	      width = camera->getWidth();
	      height = camera->getHeight();
	      fps = camera->getFps();
	    }
	  else
	    {
	      delete camera;
	      camera = NULL;
	      failure = 1;
	    }
	}
      else
	{
	  failure = 1;
	}
    }
  else
    {
      failure = 1;
    }

  return failure;
}

void QuickTimeProcessing::startCamera()
{
  if (camera)
    {
      camera->startCamera();
    }
}

void QuickTimeProcessing::stopCamera()
{
  if (camera)
    {
      camera->stopCamera();
    }
}

QuickTimeProcessing::image_type QuickTimeProcessing::getFrame()
{
  unsigned char * framebuffer = 0;

  while ( !(framebuffer = camera->getFrame()) ); 

  image_type image;
  image.resize(width, height);

  for (int row = 0; row < height; row++)
    {
      for (int col = 0; col < width; col++)
	{
	  image[row][col] = RGB24Pixel(framebuffer[row*width*3 + col*3+0],
				       framebuffer[row*width*3 + col*3+1],
				       framebuffer[row*width*3 + col*3+2]);
	}
    }

  return image;
}

int QuickTimeProcessing::getWidth()
{
  return width;
}

int QuickTimeProcessing::getHeight()
{
  return height;
}

int QuickTimeProcessing::getFPS()
{
  return fps;
}

bool QuickTimeProcessing::stillRunning()
{
  bool running;

  if ( camera )
    {
      running = camera->stillRunning();
    }
  else
    {
      running = false;
    }

  return running;
}
