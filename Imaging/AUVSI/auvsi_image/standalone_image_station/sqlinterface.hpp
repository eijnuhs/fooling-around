#ifndef _SQLINTERFACE_H
#define _SQLINTERFACE_H

#include <mysql++/mysql++.h>
#include <boost/date_time/posix_time/posix_time.hpp>
#include "telemetry.hpp"
#include <vector>

class SqlInterface
{
public:
  // Constructor opens connection - no error handling yet
  SqlInterface();
  ~SqlInterface();

  // writes telemetry to the snapshot table in the database
  void writeSnapshot(char snapshotNumber, boost::posix_time::ptime snapshotTime);

  // writes telemetry to the telemetry table in the database
  void writeTelemetry(struct telemetry* t);

  // writes telemetry to the smoothed table
  void writeSmoothed(struct telemetry* t);


  // returns the time in the snapshots database table that is closest to
  // the inaccurate time in the exif
  boost::posix_time::ptime snapshotTimeCorrector(int snapshotNumber, boost::posix_time::ptime original);
  boost::posix_time::ptime snapshotTimeCorrector(boost::posix_time::ptime inaccurateTime);

  // gets the neighboring telemetry within the given boundaries
  std::vector<struct telemetry> getNeighbors(boost::posix_time::ptime, int minus, int plus);

private:
  mysqlpp::Connection* con;
};

#endif
