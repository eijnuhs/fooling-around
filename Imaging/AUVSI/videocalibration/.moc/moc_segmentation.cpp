/****************************************************************************
** Segmentation meta object code from reading C++ file 'segmentation.h'
**
** Created: Fri Feb 17 15:31:12 2006
**      by: The Qt MOC ($Id: qt/moc_yacc.cpp   3.3.4   edited Jan 21 18:14 $)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#undef QT_NO_COMPAT
#include "../segmentation.h"
#include <qmetaobject.h>
#include <qapplication.h>

#include <private/qucomextra_p.h>
#if !defined(Q_MOC_OUTPUT_REVISION) || (Q_MOC_OUTPUT_REVISION != 26)
#error "This file was generated using the moc from 3.3.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

const char *Segmentation::className() const
{
    return "Segmentation";
}

QMetaObject *Segmentation::metaObj = 0;
static QMetaObjectCleanUp cleanUp_Segmentation( "Segmentation", &Segmentation::staticMetaObject );

#ifndef QT_NO_TRANSLATION
QString Segmentation::tr( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "Segmentation", s, c, QApplication::DefaultCodec );
    else
	return QString::fromLatin1( s );
}
#ifndef QT_NO_TRANSLATION_UTF8
QString Segmentation::trUtf8( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "Segmentation", s, c, QApplication::UnicodeUTF8 );
    else
	return QString::fromUtf8( s );
}
#endif // QT_NO_TRANSLATION_UTF8

#endif // QT_NO_TRANSLATION

QMetaObject* Segmentation::staticMetaObject()
{
    if ( metaObj )
	return metaObj;
    QMetaObject* parentObject = QObject::staticMetaObject();
    static const QUParameter param_signal_0[] = {
	{ "frame", &static_QUType_ptr, "FrameBuffer", QUParameter::In }
    };
    static const QUMethod signal_0 = {"newFrame", 1, param_signal_0 };
    static const QUParameter param_signal_1[] = {
	{ "frame", &static_QUType_ptr, "FrameBuffer", QUParameter::In }
    };
    static const QUMethod signal_1 = {"newSegmentedFrame", 1, param_signal_1 };
    static const QUParameter param_signal_2[] = {
	{ "frame", &static_QUType_ptr, "FrameBuffer", QUParameter::In }
    };
    static const QUMethod signal_2 = {"newInterpolatedFrame", 1, param_signal_2 };
    static const QUParameter param_signal_3[] = {
	{ "frame", &static_QUType_ptr, "FrameBuffer", QUParameter::In }
    };
    static const QUMethod signal_3 = {"newBackgroundFrame", 1, param_signal_3 };
    static const QMetaData signal_tbl[] = {
	{ "newFrame(FrameBuffer*)", &signal_0, QMetaData::Public },
	{ "newSegmentedFrame(FrameBuffer*)", &signal_1, QMetaData::Public },
	{ "newInterpolatedFrame(FrameBuffer*)", &signal_2, QMetaData::Public },
	{ "newBackgroundFrame(FrameBuffer*)", &signal_3, QMetaData::Public }
    };
    metaObj = QMetaObject::new_metaobject(
	"Segmentation", parentObject,
	0, 0,
	signal_tbl, 4,
#ifndef QT_NO_PROPERTIES
	0, 0,
	0, 0,
#endif // QT_NO_PROPERTIES
	0, 0 );
    cleanUp_Segmentation.setMetaObject( metaObj );
    return metaObj;
}

void* Segmentation::qt_cast( const char* clname )
{
    if ( !qstrcmp( clname, "Segmentation" ) )
	return this;
    return QObject::qt_cast( clname );
}

#include <qobjectdefs.h>
#include <qsignalslotimp.h>

// SIGNAL newFrame
void Segmentation::newFrame( FrameBuffer* t0 )
{
    if ( signalsBlocked() )
	return;
    QConnectionList *clist = receivers( staticMetaObject()->signalOffset() + 0 );
    if ( !clist )
	return;
    QUObject o[2];
    static_QUType_ptr.set(o+1,t0);
    activate_signal( clist, o );
}

// SIGNAL newSegmentedFrame
void Segmentation::newSegmentedFrame( FrameBuffer* t0 )
{
    if ( signalsBlocked() )
	return;
    QConnectionList *clist = receivers( staticMetaObject()->signalOffset() + 1 );
    if ( !clist )
	return;
    QUObject o[2];
    static_QUType_ptr.set(o+1,t0);
    activate_signal( clist, o );
}

// SIGNAL newInterpolatedFrame
void Segmentation::newInterpolatedFrame( FrameBuffer* t0 )
{
    if ( signalsBlocked() )
	return;
    QConnectionList *clist = receivers( staticMetaObject()->signalOffset() + 2 );
    if ( !clist )
	return;
    QUObject o[2];
    static_QUType_ptr.set(o+1,t0);
    activate_signal( clist, o );
}

// SIGNAL newBackgroundFrame
void Segmentation::newBackgroundFrame( FrameBuffer* t0 )
{
    if ( signalsBlocked() )
	return;
    QConnectionList *clist = receivers( staticMetaObject()->signalOffset() + 3 );
    if ( !clist )
	return;
    QUObject o[2];
    static_QUType_ptr.set(o+1,t0);
    activate_signal( clist, o );
}

bool Segmentation::qt_invoke( int _id, QUObject* _o )
{
    return QObject::qt_invoke(_id,_o);
}

bool Segmentation::qt_emit( int _id, QUObject* _o )
{
    switch ( _id - staticMetaObject()->signalOffset() ) {
    case 0: newFrame((FrameBuffer*)static_QUType_ptr.get(_o+1)); break;
    case 1: newSegmentedFrame((FrameBuffer*)static_QUType_ptr.get(_o+1)); break;
    case 2: newInterpolatedFrame((FrameBuffer*)static_QUType_ptr.get(_o+1)); break;
    case 3: newBackgroundFrame((FrameBuffer*)static_QUType_ptr.get(_o+1)); break;
    default:
	return QObject::qt_emit(_id,_o);
    }
    return TRUE;
}
#ifndef QT_NO_PROPERTIES

bool Segmentation::qt_property( int id, int f, QVariant* v)
{
    return QObject::qt_property( id, f, v);
}

bool Segmentation::qt_static_property( QObject* , int , int , QVariant* ){ return FALSE; }
#endif // QT_NO_PROPERTIES
