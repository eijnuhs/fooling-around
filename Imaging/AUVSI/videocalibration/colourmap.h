#ifndef _COLOUR_MAP_H_
#define _COLOUR_MAP_H_

#include <map>
#include "colourdefinition.h"
#include <string>
#include "configuration.h"
#include "pixel.h"

class ColourMapConfig : public Configuration
{
 public:
  ColourMapConfig() : Configuration("Colours") {}
  ~ColourMapConfig(){}
};

typedef std::map<std::string, ColourDefinition> CMAP_TYPE;

// a list of colours.
class ColourMap 
{
 public:
  ColourMap(std::string filename = "");
  ColourMap(ColourMapConfig * cmc);
  ~ColourMap();

  void addColour(std::string colourName);
  void updateSelected(const Pixel & p);
  void resetSelected();
  void selectNext();
  void selectPrevious();
  std::string getSelectedName();
  ColourDefinition * getSelectedColour();
  ColourDefinition * getColour(const std::string & colourName);
  RawPixel getSelectedAvgColour();
  std::string getLastFilename();

  bool loadFromFile(std::string filename);
  bool saveToFile(std::string filename);
 private:
  void reset();
  CMAP_TYPE cMap;
  CMAP_TYPE::iterator selected;
  std::string lastFilename;
};

#endif
