#ifndef UMBOTICA_GEOMETRY_H
#define UMBOTICA_GEOMETRY_H

#include "../error.hpp"
#include <cmath>
#include <vector>

namespace umbotica { 
  namespace geometry {
    using namespace std;

    struct point2d
    {
      float x;
      float y;
    };
    
    // This function takes a vector of vertices.  It returns true if
    // the query point is surrounded by the edge joining the vertices,
    // false otherwise. Type T must expose an operator [] with T[0]
    // and T[1] giving 2D coordinates of a point. The bounding shape
    // can't have "star" like vertices. Regular polygons,
    // parallelagrams, trapazoids are all okay.
    template<typename T>
    inline bool isInside(vector<T> vertices, T queryPoint)
    {
      bool inside = true;

      point2d previousVertex;
      point2d thisVertex;
      point2d nextVertex;

      for ( int i = 0; i < (int)vertices.size(); ++i )
	{
	  int previous = i - 1;
	  int next = i + 1;
	  if ( previous < 0 )
	    {
	      previous = vertices.size() - 1;
	    }
	  if ( next == (int)vertices.size() )
	    {
	      next = 0;
	    }
	  
	  previousVertex.x = vertices[previous][0];
	  previousVertex.y = vertices[previous][1];
	  thisVertex.x = vertices[i][0];
	  thisVertex.y = vertices[i][1];
	  nextVertex.x = vertices[next][0];
	  nextVertex.y = vertices[next][1];
	  
	  point2d prevEdge;
	  prevEdge.x = previousVertex.x - thisVertex.x;
	  prevEdge.y = previousVertex.y - thisVertex.y;
	  point2d nextEdge;
	  nextEdge.x = nextVertex.x - thisVertex.x;
	  nextEdge.y = nextVertex.y - thisVertex.y;
	  point2d queryVector;
	  queryVector.x = queryPoint[0] - thisVertex.x;
	  queryVector.y = queryPoint[1] - thisVertex.y;

	  if ( prevEdge.x * nextEdge.x + prevEdge.y * nextEdge.y >
	       prevEdge.x * queryVector.x + prevEdge.y * queryVector.y )
	    {
	      inside = false;
	      break;
	    }
	  
	}
      
      return inside;
    } 
  }
}

#endif
