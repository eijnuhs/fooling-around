#ifndef _SAMPLE_PROCESSING_H_
#define _SAMPLE_PROCESSING_H_

#include "umimage.hpp"
#include "umimagelocker.hpp"
#include "umimagegui.hpp"
#include <boost/bind.hpp>
#include <agmanager.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <configuration.h>
#include <string_routines.hpp>
#include <string>
using namespace boost::posix_time;

template<class PIXELTYPE>
class SampleProcessing;

template<class PIXELTYPE>
inline void addToGui(AGManager & gui, SampleProcessing<PIXELTYPE> & b, std::string name, int updateMsec);

template<class PIXELTYPE>
class SampleProcessing
{
public:
  typedef UmImage<PIXELTYPE> image_type;
  SampleProcessing() : m_capture(false), m_filename("Not Capturing"), m_extension("jpg")
  {
    Configuration cfg("Saving");
    m_extension = cfg.getString("extension", "jpg");
  }

  // the function call operator that us to pass this object in
  // place of a function pointer to the VideoStream.
  void operator()(UmImage<PIXELTYPE> & image)
  {
    if(!image.isNull())
      {
	UmImageLocker lock1(m_image);
	// shallow copy.
	m_image = image;
	if(m_capture)
	  {
	    // save the image with a timestamp.
	    
	    // get the timestamp
	    ptime timestamp = microsec_clock::local_time();
	    // build the filename
    std::string imageName = "images/" + umbotica::replace(to_iso_string(timestamp),'.','_') + "." + m_extension;
	    exportImage(m_image.upperLeft(), m_image.lowerRight(), m_image.rgbAccessor() ,vigra::ImageExportInfo(imageName.c_str()));
	    m_filename = "Capturing: " + imageName;
	  }
      }
  }
  UmImage<PIXELTYPE> & getImage(){return m_image;}
  bool getCaptureOn(){return m_capture;}
  void setCaptureOn(bool on)
  {
    m_capture = on; 
    if(!on)
      {
	m_filename = "Not Capturing";
      }
  }
  std::string capturing(){return m_filename;}
  // this is a hack to account for the shortcomings
  // of the autogui.
  //  std::string setFile(std::string){}; 

private:
  UmImage<PIXELTYPE> m_image;
  bool m_capture;
  std::string m_filename;
  std::string m_extension;
  friend void addToGui<PIXELTYPE>(AGManager & gui, SampleProcessing<PIXELTYPE> & b, std::string name, int updateMsec);
};


template<class PIXELTYPE>
inline void addToGui(AGManager & gui, SampleProcessing<PIXELTYPE> & b, std::string name, int updateMsec)
{
  gui.pushGroup(name, false);
  gui.pushGroup("raw", false);
  gui.setWindowSize(640,480);
  addToGui(gui, b.getImage(), "Raw Image", updateMsec,true);
  gui.popGroup();
  // add the capture checkbox.
  gui.add("Capture", b.m_capture);
  gui.add("State: ", b.m_filename, true);
  gui.setWindowTimer(200);
  gui.popGroup();
}



#endif
