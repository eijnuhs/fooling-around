// $Id: videostream.h,v 1.1.1.1.2.7 2004/12/28 19:30:05 cvs Exp $
// Jacky Baltes <jacky@cs.umanitoba.ca>

#ifndef _VIDEOSTREAM_H_
#define _VIDEOSTREAM_H_

#include <qthread.h>
#include "videodevice.h"
#include <string>
#include <qobject.h>
#include "framebufferrgb32.h"
#include <sys/time.h>


// Forward declarations
class Segmentation;
class VideoInterpreter2D;
class VideoCalibration;
class VideoStreamConfig;
class Tracker;

class VideoStream : public QObject, public QThread
{
  Q_OBJECT

 public:
  VideoStream( std::string driver, std::string name, std::string input, std::string standard, int fps, int width, int height, int depth );
  VideoStream(VideoStreamConfig * vsc);

  void run( void );
  //  void setSegmentation( Segmentation * segmentation );
  Segmentation * getSegmentation( void );
  VideoDevice * getVideoDevice() const;
  VideoCalibration * getVideoCalibration( void );
  void setVideoCalibration(VideoCalibration * vCalibration );
  void setVideoInterpreter2D( VideoInterpreter2D * vi2d );
  void setTracker(Tracker * trak);
  void updateFPS( void );

  bool done;
  bool isInterlaced();
  static double framesPerSecond;

 signals:
  void newFrame( FrameBuffer * frame );
  void fpsUpdate(double fps);

 private: 
  void initialize( std::string driver, std::string name, std::string input, std::string standard, int fps, int width, int height, int depth );
  void sendFPSupdate();
  FrameBufferRGB32 ipFrame;
  VideoDevice * device;
  //  Segmentation * segmentation;
  VideoInterpreter2D * videoInterpreter2D;
  VideoCalibration * vCalibration;
  VideoStreamConfig * vsConfig;
  Tracker * tracker;
  struct timeval prev;
};

#endif
