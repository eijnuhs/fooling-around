#include "framespersecond.h"
#include <iostream>
#define FPS_UPDATE_INTERVAL 250

FramesPerSecond::FramesPerSecond() :
  QObject(),
  timer(this)
{

  QObject::connect( ( (QObject const *) &timer ), SIGNAL( timeout() ),( ( QObject const * ) (this ) ), SLOT( emitFPS() ) );  
  timer.start(FPS_UPDATE_INTERVAL);
}
FramesPerSecond::~FramesPerSecond(){}

void FramesPerSecond::recieveFPS(double init_fps) 
{ 
  mutex.lock();
  fps = (fps * 0.8) + (init_fps * 0.2) ;
  mutex.unlock();
}

void FramesPerSecond::emitFPS()
{
  mutex.lock();
  emit updateFPS(fps);
  mutex.unlock();
}

long FramesPerSecond::getTimeDiff(struct timeval early, struct timeval late, bool debug)
{
  long timeDiff;
  if ( late.tv_usec >= early.tv_usec )
    {
      timeDiff = (late.tv_usec - early.tv_usec) + (1000000 * ( late.tv_sec - early.tv_sec ));
    }
  else
    {
      timeDiff = late.tv_usec + 1000000 - early.tv_usec + 1000000 * ( late.tv_sec - early.tv_sec - 1);
    }

  if(debug)
    {
      if ( late.tv_usec >= early.tv_usec )
	{
	  std::cout << "usec: " << late.tv_usec << " - " << early.tv_usec << " = " << (late.tv_usec - early.tv_usec) << "\n";
	  std::cout << " sec: " << late.tv_sec << " - " << early.tv_sec << " = " << (late.tv_sec - early.tv_sec) << "\n";
	}
      else
	{
	  std::cout << "usec: " << late.tv_usec << " - " << early.tv_usec << " + 1000000  = " << (1000000 + late.tv_usec - early.tv_usec) << "\n";
	  std::cout << " sec: " << late.tv_sec << " - " << early.tv_sec << " - 1 = " << (late.tv_sec - early.tv_sec - 1) << "\n";
	}
      std::cout << " tot: " << timeDiff << "\n";
      std::cout << "ticks per second: " << (1000000 / timeDiff) << "\n";
    }
  

  return timeDiff;
}
