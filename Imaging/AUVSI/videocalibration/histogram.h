#ifndef _HISTOGRAM_H_
#define _HISTOGRAM_H_

class Histogram
{
 public:
  Histogram(unsigned int init_width = 256, unsigned int init_val = 0);
  Histogram(const Histogram & rhs);
  Histogram & operator=(const Histogram & rhs);
  ~Histogram();

  void initialize(unsigned int init_width, unsigned int init_val = 0);
  void reset(unsigned int init_val = 0);
  void increment(unsigned int i);
  unsigned int get(unsigned int i) const;
  void set(unsigned int i, unsigned int value);
  unsigned int getMax() const;
  unsigned int size() const;
  unsigned int getWeighted(unsigned int i) const;
  unsigned int getTotal() const;
  unsigned int getLowerThreshold(unsigned int percent) const;
  unsigned int getUpperThreshold(unsigned int percent) const;
  unsigned int getLowerIdx() const;
  unsigned int getUpperIdx() const;
  unsigned int getRange() const;
							
 private:
  void copy(const Histogram & rhs);
  unsigned int * counts;
  unsigned int width;
  unsigned int max;
  unsigned int total;
  unsigned int lowerIdx;
  unsigned int upperIdx;
  bool upperAndLowerInit;
};


#endif
