TEMPLATE	= app
LANGUAGE	= C++

CONFIG	+= qt warn_on debug

LIBS	+= minpack/libminpak.a matrix/matrix.o -ldotconfpp -lboost_filesystem

HEADERS	+= videodevice.h \
	v4l2device.h \
	videostream.h \
	framebuffer.h \
	framebufferviewer.h \
	calibration.h \
	camera.h \
	square.h \
	controlpoints.h \
	videointerpreter2d.h \
	tsaiplanarcalibration.h \
	rotationmatrix.h \
	playingfield.h \
	pixel.h \
	geometry.h \
	framebufferrgb565.h \
	framebufferrgb32.h \
	region.h \
	mask.h \
	framebufferworld.h \
	imageprocessing.h \
	paintbox.h \
	videocalibration.h \
	visitedmap.h \
	controlpointscarpetcalib.h \
	videocalibrationviewer.h \
	v4l2control.h \
	v4l2controlitem.h \
	framebufferrgb32x.h \
	framespersecond.h \
	configuration.h \
	videostreamconfig.h \
	videointerpreter2dconfig.h \
	array2d.h \
	run.h \
	colourdefinition.h \
	colourview.h \
	filevideodevice.h

SOURCES	+= main.cpp \
	v4l2device.cpp \
	videostream.cpp \
	videodevice.cpp \
	framebuffer.cpp \
	framebufferviewer.cpp \
	calibration.cpp \
	camera.cpp \
	square.cpp \
	controlpoints.cpp \
	videointerpreter2d.cpp \
	tsaiplanarcalibration.cpp \
	rotationmatrix.cpp \
	playingfield.cpp \
	pixel.cpp \
	geometry.cpp \
	framebufferrgb565.cpp \
	framebufferrgb32.cpp \
	region.cpp \
	mask.cpp \
	framebufferworld.cpp \
	imageprocessing.cpp \
	paintbox.cpp \
	videocalibration.cpp \
	visitedmap.cpp \
	controlpointscarpetcalib.cpp \
	videocalibrationviewer.cpp \
	v4l2control.cpp \
	v4l2controlitem.cpp \
	framebufferrgb32x.cpp \
	framespersecond.cpp \
	framebufferworldmotion.cpp \
	configuration.cpp \
	videostreamconfig.cpp \
	videointerpreter2dconfig.cpp \
	run.cpp \
	colourdefinition.cpp \
	colourview.cpp \
	filevideodevice.cpp

FORMS	= mainwindow.ui

unix {
  UI_DIR = .ui
  MOC_DIR = .moc
  OBJECTS_DIR = .obj
}

TEMPLATE	=app 
CONFIG	        += qt warn_on debug 
LANGUAGE	= C++

profile {
  QMAKE_CFLAGS          *= -p 
  QMAKE_CXXFLAGS          *= -p 
  QMAKE_LFLAGS          *= -p
}

release {
  QMAKE_CFLAGS          *= -O3 -funroll-all-loops -fomit-frame-pointer 
  QMAKE_CXXFLAGS          *= -O3 -funroll-all-loops -fomit-frame-pointer 
}

debug {
  QMAKE_CFLAGS    += -O0 -ggdb
  QMAKE_CXXFLAGS    += -O0 -ggdb
}

libminpack.target = minpack/libminpak.a
libminpack.commands = (cd minpack; gcc -Wall -O2 -c *.c; ar q libminpak.a *.o)
libminpack.depends = minpack/dpmpar.c minpack/enorm.c minpack/fdjac2.c minpack/lmdif.c minpack/lmpar.c minpack/qrfac.c minpack/qrsolv.c

matrix.target = matrix/matrix.o
matrix.commands = (cd matrix; gcc -Wall -O2 -c matrix.c -o matrix.o)
matrix.depends = matrix/matrix.c matrix/matrix.h


QMAKE_EXTRA_UNIX_TARGETS += libminpack matrix

PRE_TARGETDEPS	+= minpack/libminpak.a matrix/matrix.o



