#include "gps.hpp"
#include "vigra/accessor.hxx"
#include <cmath>
#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>
#include <umbotica/math/math.hpp>

GpsCoord GpsCoord::reference(0, 0);

GpsCoord::GpsCoord (double const & longitude, double const & latitude) : vigra::TinyVector<double, 2>(longitude, latitude)
{
}

GpsCoord::GpsCoord( std::string icaoCoordinateString )
{
  int degreesLatitude = boost::lexical_cast<int>(icaoCoordinateString.substr(0,2));
  int minutesLatitude = boost::lexical_cast<int>(icaoCoordinateString.substr(2,2));
  int secondsLatitude = boost::lexical_cast<int>(icaoCoordinateString.substr(4,2));
  if (icaoCoordinateString[6] == 'S') { degreesLatitude *= -1; }
  int degreesLongitude = boost::lexical_cast<int>(icaoCoordinateString.substr(7,3));
  int minutesLongitude = boost::lexical_cast<int>(icaoCoordinateString.substr(10,2));
  int secondsLongitude = boost::lexical_cast<int>(icaoCoordinateString.substr(12,2));

  (*this)[0] = degreesLongitude + (double)minutesLongitude/60.0 + (double)secondsLongitude/3600.0;
  (*this)[1] = degreesLatitude + (double)minutesLatitude/60.0 + (double)secondsLatitude/3600.0;

}

GpsCoord::~GpsCoord()
{
}

vigra::TinyVector<double, 2> GpsCoord::inMetres() const
{
  return distanceInMetres(reference);
}

GpsCoord GpsCoord::gpsCoordFromMetres( double metresEast, double metresNorth )
{
  return reference.fromMetres(metresEast, metresNorth);
}

double GpsCoord::longitude() const
{
  return (*this)[0];
}

double GpsCoord::latitude() const
{
  return (*this)[1];
}

void GpsCoord::setReference(GpsCoord _reference)
{
  reference = _reference;
}

// Gives a gps location from the current point in meters.
GpsCoord GpsCoord::fromMetres(double metresEast, double metresNorth) const
{
  double b1 = this->longitude() * M_PI / 180.0;
  double a1 = this->latitude() * M_PI / 180.0;

  double latitudeDifference = metresNorth / EARTH_RADIUS;
  double radiusAtLatitude = EARTH_RADIUS * cos(a1);
  double longitudeDifference = metresEast / radiusAtLatitude;

  double a2 = a1 + latitudeDifference;
  double b2 = b1 + longitudeDifference;

  double degreesLatitude = a2 * 180.0 / M_PI;
  double degreesLongitude = b2 * 180.0 / M_PI;

  return GpsCoord( degreesLongitude, degreesLatitude );
}

vigra::TinyVector<double, 2> GpsCoord::distanceInMetres(GpsCoord g) const
{
 //-----convert to radians
  double b2 = longitude() * M_PI / 180.0;
  double a2 = latitude() * M_PI / 180.0;

  double b1 = g.longitude() * M_PI / 180.0;
  double a1 = g.latitude() * M_PI / 180.0;

  double metersNorth = (a2 - a1) * EARTH_RADIUS;

  double radiusAtLatitude = EARTH_RADIUS * cos(a1);
  
  double metersEast = (b2 - b1) * radiusAtLatitude;

  vigra::TinyVector<double, 2> meters;
  meters[0] = metersEast;
  meters[1] = metersNorth;

  return meters;
}

double GpsCoord::straightLineDistance(GpsCoord g) const
{
  vigra::TinyVector<double, 2> offset = distanceInMetres(g);
  return sqrt(offset[0] * offset[0] + offset[1] * offset[1]);
}

std::string GpsCoord::longitudeString() const
{
  double longit = longitude();
  double degrees = trunc(longit);
  double minutes = fabs(longit - degrees) * 60;

  std::stringstream result;
  result << boost::lexical_cast<std::string>(fabs(degrees)) << "*" << boost::format("%f") % minutes << (longit < 0 ? "W" : "E");
  return result.str();
}

std::string GpsCoord::latitudeString() const
{
  double lat = latitude();
  double degrees = trunc(lat);
  double minutes = fabs(lat - degrees) * 60;

  std::stringstream result;
  result << boost::lexical_cast<std::string>(fabs(degrees)) << "*" << boost::format("%f") % minutes << (lat < 0 ? "S" : "N");
  return result.str();
}

std::string GpsCoord::toCompetitionString() const
{
  return latitudeString() + " " + longitudeString();
}
