#include "v4l2controlitem.h"
#include <qslider.h>
#include <qlabel.h>
#include <linux/videodev.h>
#include <sys/ioctl.h>
#include <qmessagebox.h>
#include <v4l2device.h>
#ifdef XXDEBUG
#include <iostream>
using namespace std;
#endif
#define LABEL_WIDTH 100


V4L2ControlItem::V4L2ControlItem(V4L2Device * vDev, struct v4l2_queryctrl qctl,  QWidget * parent, const char * name) :
  QHBox(parent, name)
{
  this->videoDevice = vDev;
  this->controlId = qctl.id;
  this->controlName = QString((const char *)qctl.name);

  // set the widget label at a fixed width
  QLabel * label = new QLabel(QString((const char *)qctl.name), this);
  label->setFixedWidth(LABEL_WIDTH);

  // create a slider for any integer controls.  Other controls are not yet implemented
  if(qctl.type == V4L2_CTRL_TYPE_INTEGER)
    {
      // add a slider
      QSlider * slider = new QSlider(qctl.minimum, qctl.maximum, qctl.step, qctl.default_value, Qt::Horizontal, this);

      // set the slider to the current value.
      struct v4l2_control vc;
      int errorCode;
      vc.id = (__u32)controlId;
      errorCode = videoDevice->getControl(&vc);
      if(errorCode == 0)
	{
	  slider->setValue(vc.value);
	}
      // connect the signal to the slot that handles slider moves.
      QObject::connect( slider, SIGNAL( valueChanged(int) ), this, SLOT( setV4LValue(int) ));
    }
  else
    {
       new QLabel("Control type not implemented.", this);
    }

}
void V4L2ControlItem::setV4LValue(int value)
{
#ifdef XXDEBUG
  cout << "V4L2ControlItem( " << controlName.ascii()  << " )::setV4LValue: " << value << endl;
#endif
  struct v4l2_control vc;
  int errorCode;
  vc.id = (__u32)controlId;
  vc.value = (__s32)value;
  errorCode = videoDevice->setControl(vc);
  if ( errorCode != 0)
    {
      QString warningMessage = QString("V4L2Control: Error setting %1 to value = %2.\nError Number: %3").arg(controlName).arg(value).arg(errorCode); 
      QMessageBox::warning (this, "V4L2Control Error.", warningMessage, QMessageBox::Ok,QMessageBox::NoButton, QMessageBox::NoButton);
    }
}


