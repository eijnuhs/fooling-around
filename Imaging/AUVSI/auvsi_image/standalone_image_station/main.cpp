#include <iostream>
#include <fstream>
#include <vector>
#include <exception>
#include <qapplication.h>
#include <agmainwindow.hpp>
#include <agmanager.hpp>
#include <agshapes.hpp>
#include <umimage.hpp>
#include <umimagetypedefs.hpp>
#include <agdockwindow.hpp>
#include <agref.hpp>
#include <umimagegui.hpp>
#include <agmouse.hpp>
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/condition.hpp>
#include <umbotica/threads/threadwrapper.hpp>
#include <qslider.h>
#include <qmessagebox.h>
#include <qinputdialog.h>
#include <qfiledialog.h>
#include <qstringlist.h>
#include <qstring.h>
#include <string>
#include <algorithm>
#include "boost/filesystem/path.hpp"
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/fstream.hpp"
#include <boost/date_time/posix_time/posix_time.hpp>
#include <libexif/exif-data.h>
#include <libexif/exif-entry.h>
#include <libexif/exif-content.h>
#include <umbotica/string_routines.hpp>
#include <umbotica/math/geometry.hpp>
#include "image_vector.hpp"
#include "telemetry.hpp"
#include "configuration.h"
#include "smoother.hpp"
#include "auvsi_calibration.hpp"
#include "helper_fns.hpp"
#include <umbotica/math/radian.hpp>
#include <vigra/tinyvector.hxx>
#include <limits>
#include <vigra/resizeimage.hxx>
#include <vigra/impex.hxx>
#include <qdom.h>
#include <qfile.h>
#include <gd.h>
#include <cstdio>
#include "umimageagcontainer.hpp"
#include "sqlinterface.hpp"

using namespace std;
namespace fs = boost::filesystem;
using namespace boost::posix_time;

int highResWidth;
int highResHeight;

struct HiResImage
{
  string name;
  vector<GpsCoord> bounds;
  bool telemAvailable;
  struct telemetry m_telem;
  boost::posix_time::ptime time;
};

struct TargetConfirmationPoint
{
  string id;
  GpsCoord location;
  int orientation; // rotation in degrees clockwise from north
  string thumbnail;
};

bool sortHiResImage(const HiResImage & first, const HiResImage & second)
{
  return first.time < second.time;
}

bool prepareOutputStream( ofstream & outStream, QWidget * parent, vector<struct TargetConfirmationPoint> & targetVector );  
GpsCoord rotatedGPSCoordFromImageCoord( int x, int y, struct telemetry telem );
vigra::TinyVector<int, 2> findCanvasFromGPS(GpsCoord query, telemetry telem, int xMin, int xMax, int yMin, int yMax);

class HiResViewer
{
public:
  int m_operator_id;
  UmRGB24 m_image;
  boost::shared_ptr<UmImageAgContainer<UmRGB24::PixelType> > imageContainer;
  UmRGB24 image;
  UmRGB24 bgImage;
  string clickedGPSLongitude; // used for display to the UI
  string clickedGPSLatitude; // used for display to the UI
  string clickedOrientationString; // used for display to the UI
  string targetID; // used to for UI interface
 
  HiResViewer( string _bgImagePath, double gTop, double gBottom,
	       double gLeft, double gRight, string thumbnailDirectory, int farmMode, QWidget* parent) 
    : gpsTop(gTop),
      gpsBottom(gBottom),
      gpsLeft(gLeft),
      gpsRight(gRight),
      bgImagePath(_bgImagePath),
      m_parent(parent),
      m_thumbnailDirectory(thumbnailDirectory),
      m_farmMode(farmMode)
  {
    UmImageLocker lock(bgImage);
    UmRGB24 temp;
    temp.loadImage( bgImagePath );
    bgImage.resize(200,200);
    vigra::resizeImageSplineInterpolation( srcImageRange(temp, temp.rgbAccessor()),
					   destImageRange(bgImage, bgImage.rgbAccessor()) );
    
    imageIndex = -1; // no image has been viewed yet.
    m_operator_id = 0;
    if (m_farmMode)
      {
	m_operator_id = getOperatorID();
      }
    prepareXMLDoc();

    imageContainer.reset(new UmImageAgContainer<UmRGB24::PixelType>());
    imageContainer->setImage(UmRGB24(10,10));
    
  }

  ~HiResViewer() {}

  vector<AGShape> getImageShapes()
  {
    return imageShapes;
  }

  vector<AGShape> getOverlayShapes()
  {
    return overlayShapes;
  }

  void updateGui()
  {
    imageShapes.clear();
    overlayShapes.clear();
    
    for (int imgIndex = 0; imgIndex < (int)imageList.size(); imgIndex++ )
      {
	if ( imgIndex == imageIndex && imageList[imgIndex].telemAvailable )
	  {
	    vector<GpsCoord> bounds = imageList[imgIndex].bounds;
	    
	    int canvasX = 0;
	    int canvasY = 0;
	    vector<int> corners;
	    
	    GpsCoord gpsTL = bounds[0];
	    GpsCoord gpsTR = bounds[1];
	    GpsCoord gpsBR = bounds[2];
	    GpsCoord gpsBL = bounds[3];
	    
	    gpsToBGCanvas(gpsTL, &canvasX, &canvasY);
	    corners.push_back(canvasX);
	    corners.push_back(canvasY);
	    gpsToBGCanvas(gpsTR, &canvasX, &canvasY);
	    corners.push_back(canvasX);
	    corners.push_back(canvasY);
	    gpsToBGCanvas(gpsBR, &canvasX, &canvasY);
	    corners.push_back(canvasX);
	    corners.push_back(canvasY);
	    gpsToBGCanvas(gpsBL, &canvasX, &canvasY);
	    corners.push_back(canvasX);
	    corners.push_back(canvasY);
	    gpsToBGCanvas(gpsTL, &canvasX, &canvasY);
	    corners.push_back(canvasX);
	    corners.push_back(canvasY);
	    
	    AGShape outline;
	    outline.multiLine( corners, AGColor(255,0,0) );
		
	    overlayShapes.push_back(outline);
	  } // if telem is available
      } // for each image


    if ( imageIndex != -1) // if showing an image
      {
	if (lastImageIndex != imageIndex)
	  {
	    UmImageLocker lock1(m_image);
	    m_image.loadImage( imageList[imageIndex].name );
	    imageContainer->setImage( m_image );
	
	  }
	lastImageIndex = imageIndex;

	AGShape tlLabel;
	AGShape trLabel;
	AGShape brLabel;
	AGShape blLabel;
	AGColor color(255,0,0);
	
	if ( imageList[imageIndex].telemAvailable )
	  {
	    tlLabel.text(string() + imageList[imageIndex].bounds[0].toCompetitionString(), 0, 10, color, QFont("Times New Roman") );
	    trLabel.text(string() + imageList[imageIndex].bounds[1].toCompetitionString(), highResWidth - 110, 10, color, QFont("Times New Roman") );
	    brLabel.text(string() + imageList[imageIndex].bounds[2].toCompetitionString(), highResWidth - 110, highResHeight - 2, color, QFont("Times New Roman") );
	    blLabel.text(string() + imageList[imageIndex].bounds[3].toCompetitionString(), 0, highResHeight - 2, color, QFont("Times New Roman") );

	    imageShapes.push_back(tlLabel);
	    imageShapes.push_back(trLabel);
	    imageShapes.push_back(brLabel);
	    imageShapes.push_back(blLabel);
	    
	    AGShape targetLine;
	    targetLine.line(m_click_point[0], m_click_point[1], m_drag_point[0], m_drag_point[1], color, 2);
	    imageShapes.push_back(targetLine);
	    
	    // show other targets
	    vector<TargetConfirmationPoint>::const_iterator target = targetVector.begin();
	    for ( ; target != targetVector.end(); target++ )
	      {
		if (umbotica::geometry::isInside(imageList[imageIndex].bounds, target->location))
		  {
		    vigra::TinyVector<int, 2> targetCanvasLocation = findCanvasFromGPS(target->location,
										       imageList[imageIndex].m_telem,
										       0, highResWidth - 1, 0, highResHeight -1);
		    AGShape targetCircle;
		    targetCircle.circle(targetCanvasLocation[0], targetCanvasLocation[1], 30, color, false);
		    imageShapes.push_back(targetCircle);
		    AGShape targetIDText;
		    if (targetCanvasLocation[0] < highResWidth / 2)
		      {
			targetIDText.text(target->id, targetCanvasLocation[0] + 35, targetCanvasLocation[1], color, QFont("Times New Roman", 24));
		      }
		    else
		      {
			targetIDText.text(target->id, targetCanvasLocation[0] - 120, targetCanvasLocation[1], color, QFont("Times New Roman", 24));
		      }
		    imageShapes.push_back(targetIDText);
		  }
		
	      }
	  }
	else
	  {
	    // Display X if there is no telemetry available.
	    AGShape cross1;
	    cross1.line(image.width() - 200, image.height() - 200, image.width() + 200, image.height() + 200, color, 3);
	    AGShape cross2;
	    cross2.line(image.width() - 200, image.height() + 200, image.width() + 200, image.height() - 200, color, 3);
	    imageShapes.push_back(cross1);
	    imageShapes.push_back(cross2);

	  }

      }
  }


  void loadImages( QWidget * parent )
  {
    QString result = QFileDialog::getExistingDirectory( QString::null, parent, 0, "Find High Resolution Images" );
    SqlInterface *timeCorrector = 0;

    string folder(result.ascii());

    cout << "opening folder " << folder << " for high res images..." << endl;

    imageList.clear();
    
    if ( fs::exists(folder) )
      {
	fs::directory_iterator i = fs::directory_iterator(folder);
	fs::directory_iterator end = fs::directory_iterator();
	
	try
	  {
	    timeCorrector = new SqlInterface();
	  }
	catch (std::exception & e)
	  {
	    cerr << "No connection to telemetry data source." << endl;
	    throw e;
	  }

	for (; i != end; i++)
	  {
	    HiResImage im;
	    ExifData* data;
	    ExifContent* content;
	    ExifEntry* entry;
	    
	    im.name = i->native_directory_string();
	    
	    if ( !(data = exif_data_new_from_file(im.name.c_str())) )
	      {
		cout << "Could not load exif data from " << im.name << endl;
	      }
	    else
	      {
		for (int id = 0; id < EXIF_IFD_COUNT; id++)
		  {
		    content = data->ifd[id];
		    // I forget why apple is different
		    if ( (entry = exif_content_get_entry( content, EXIF_TAG_DATE_TIME_ORIGINAL )) )
		      {
#if defined (__APPLE__)
			string timeString( exif_entry_get_value( entry ) );
#else 			
			char valueBuffer[30];
			string timeString( exif_entry_get_value( entry, valueBuffer, 30 ) );
#endif
			timeString = umbotica::replace_reverse( timeString, ':', '-', 2 );
			im.time = boost::posix_time::time_from_string( timeString );
			
			//don't correct now
			//im.time = timeCorrector->snapshotTimeCorrector(im.time);
			
			imageList.push_back(im);
			break;
		      }
		  }
	      }
	  }

	sort(imageList.begin(), imageList.end(), &sortHiResImage);

	for (int i = 0; i < (int)imageList.size(); i++)
	  {
	    // database is based at 1, not 0
	    cout << "uncorrected " << i << ": " << imageList[i].time << endl;
	    imageList[i].time = timeCorrector->snapshotTimeCorrector(i+1, imageList[i].time);
	    cout << "image " << i << ": " << imageList[i].time << endl;
	  }

	cout << "Found " << imageList.size() << " images." << endl;
	
	Smoother<SqlInterface> *telemSource = 0;
	
	try
	  {
	    telemSource = new Smoother<SqlInterface>();
	  }
	catch ( exception & e)
	  {
	    cerr << "Could not access telemetry source when loading hi res images" << endl;
	    exit(1);
	  }
	
	if (telemSource != 0)
	  {
	    for ( int img = 0; img < (int)imageList.size(); img++ )
	      {
		bool gotTelem = false;
		struct telemetry telem;
		gotTelem = telemSource->getSmoothed(imageList[img].time, &telem, Fresh); // get Fresh telemetry from db

		

		if (gotTelem)
		  {
		    GpsCoord gpsTL = rotatedGPSCoordFromImageCoord( 0, 0, telem );
		    GpsCoord gpsTR = rotatedGPSCoordFromImageCoord( highResWidth - 1, 0, telem );
		    GpsCoord gpsBR = rotatedGPSCoordFromImageCoord( highResWidth - 1, highResHeight - 1, telem );
		    GpsCoord gpsBL = rotatedGPSCoordFromImageCoord( 0, highResHeight - 1, telem );
		    
		    vector<GpsCoord> bounds;
		    bounds.push_back(gpsTL);
		    bounds.push_back(gpsTR);
		    bounds.push_back(gpsBR);
		    bounds.push_back(gpsBL);
		    
		    imageList[img].bounds = bounds;
		    imageList[img].m_telem = telem;

		    imageList[img].telemAvailable = true;
		  } 
		else
		  {
		    imageList[img].telemAvailable = false;
		    cout << "No telemetry available for HighRes image at time: " << imageList[img].time << endl;
		  }
	      } // end for each gpsPoint
	  } // if smoother was created
      } // end if images folder exists
    else
      {
	cout << "Folder does not exist." << endl;
      }
    
    if (imageList.size() != 0)
      {
	imageIndex = 0;
      }
    
    updateGui();

    if (timeCorrector)
      {
	delete(timeCorrector);
      }
  }
  
  void viewOverlay()
  {
    imageIndex = -1;
    updateGui();
  }

  void nextImage()
  {
    if (imageIndex != -1)
      {
	imageIndex++;
	if (imageIndex >= (int)imageList.size())
	  {
	    imageIndex = 0;
	  }
	updateGui();
      }
  }

  void previousImage()
  {
    if (imageIndex != -1)
      {
	imageIndex--;
	if (imageIndex < 0)
	  {
	    imageIndex = (int)imageList.size() - 1;
	  }
	updateGui();
      }
  }

  void fiveAhead()
  {
    if (imageIndex != -1)
      {
	imageIndex = (imageIndex+5) % (int)imageList.size();
	updateGui();
      }
  }

  void fiveBack()
  {
    if (imageIndex != -1)
      {
	imageIndex-=5;
	
	if (imageIndex < 0)
	  {
	    imageIndex = (int)imageList.size() + imageIndex;
	  }

	updateGui();
      }
  }

  // for display to UI
  int getCurrIndexPlusOne()
  {
    return imageIndex + 1;
  }

  int getTotal()
  {
    return imageList.size();
  }

  void confirmTarget()
  {
    int thumbnailSize = 150;
    if ( imageIndex != -1 )
      {
	struct TargetConfirmationPoint target;
	
	if (targetID.length() == 0)
	  {
	    QMessageBox::information(m_parent, "Confirm", "No target ID entered. Target not recorded.");
	  }
	else
	  {
	    target.id = targetID;
	    target.location = m_gpsClick;
	    target.orientation = m_orientation_degrees;
	    
	    FILE* fd = 0;

	    if ( !(fd = fopen(imageList[imageIndex].name.c_str(), "r")) )
	      {
		perror("Error opening input image file:");
	      }
	    else
	      {

		int xMin = umbotica::clamp(m_click_point[0], thumbnailSize, m_image.width() - thumbnailSize - 1);
		int yMin = umbotica::clamp(m_click_point[1], thumbnailSize, m_image.height() - thumbnailSize - 1);

		gdImagePtr hiResImage = gdImageCreateFromJpeg(fd);
		fclose(fd);
		
		if ( !(fd = fopen("temp.gd", "wb")) )
		  {
		    perror("Error opening temp image file for output:");
		  }
		gdImageGd2(hiResImage, fd, 0, GD2_FMT_COMPRESSED);
		fclose(fd);

		gdImageDestroy(hiResImage);
		if ( !(fd = fopen("temp.gd", "r")) )
		  {
		    perror ("Error opening temp image file for input:");
		  }
		hiResImage = gdImageCreateFromGd2Part(fd, xMin - thumbnailSize, yMin - thumbnailSize,
						      2 * thumbnailSize,
						      2 * thumbnailSize);

		std::cout << "HiResImage: " << hiResImage->sx << "x" << hiResImage->sy << endl;

		if (hiResImage == 0)
		  {
		    cout << "error creating partial image from gd2 temp" << endl;
		  }
		
		
		string thumbName;
		int seqNo = 0;
		do
		  {
		    thumbName = string(m_thumbnailDirectory + "/" +
				       target.id + "_" +
				       boost::lexical_cast<std::string>(seqNo++) + ".jpeg");
		  } while (fs::exists(thumbName));

		if ( !(fd = fopen(thumbName.c_str(), "wb")) )
		  {
		    perror("Error opening output image file:");
		  }
		else
		  {
		    gdImageJpeg(hiResImage, fd, 95);
		    gdImageDestroy(hiResImage);

		    target.thumbnail = thumbName;
		    fclose(fd);
		  }
	      }
	    
	    addToXML(target);
	    saveXML();

	    targetVector.push_back(target);
	    QMessageBox::information(m_parent, "Confirm", "Target recorded.");
	    //targetID.clear();
	    clickedGPSLongitude.clear();
	    clickedGPSLatitude.clear();
	    updateGui();
	  }
      }
  }

  void review()
  {
    cout << "Targets identified:" << endl;
    QDomElement root = m_xmldoc.documentElement();
    QDomNode targetNode = root.firstChild();
    while ( !targetNode.isNull() )
      {
	if ( targetNode.isElement() )
	  {
	    QDomElement targetElement = targetNode.toElement();
	    cout << targetElement.attribute("id") << endl;

	    int childCount = 0;
	    double latitudeAccumulator = 0;
	    double longitudeAccumulator = 0;
	    int accumulators[8];
	    for (int i = 0; i < 8; i++)
	      {
		accumulators[i] = 0;
	      }

	    vector<string> thumbnailList;
	    QDomNode confirmationNode = targetElement.firstChild();
	    while ( !confirmationNode.isNull() )
	      {
		if ( confirmationNode.isElement() )
		  {
		    childCount++;
		    QDomElement confirmationElement = confirmationNode.toElement();
		    longitudeAccumulator += boost::lexical_cast<double>(confirmationElement.attribute("longitude"));
		    latitudeAccumulator += boost::lexical_cast<double>(confirmationElement.attribute("latitude"));
		    double orient = boost::lexical_cast<double>(confirmationElement.attribute("orientation"));
		    
		    if (orient > 360 - 22.5 || orient <= 22.5)
		      {
			accumulators[0]++;
		      }
		    else if (orient > 22.5 && orient <= 67.5)
		      {
			accumulators[1]++;
		      }
		    else if (orient > 67.5 && orient <= 112.5)
		      {
			accumulators[2]++;
		      }
		    else if (orient > 112.5 && orient <= 157.5)
		      {
			accumulators[3]++;
		      }
		    else if (orient > 157.5 && orient <= 202.5)
		      {
			accumulators[4]++;
		      }
		    else if (orient > 202.5 && orient <= 247.5)
		      {
			accumulators[5]++;
		      }
		    else if (orient > 247.5 && orient <= 292.5)
		      {
			accumulators[6]++;
		      }
		    else
		      {
			accumulators[7]++;
		      }

		    string temp = confirmationElement.attribute("thumb");
		    thumbnailList.push_back(temp);
		  }
		confirmationNode = confirmationNode.nextSibling();
	      }
	    
	    GpsCoord avgLocation( longitudeAccumulator / childCount, latitudeAccumulator / childCount );
	    cout << "\tAverage location: " << avgLocation.toCompetitionString() << endl;
	    for (int i = 0; i < (int)thumbnailList.size(); i++)
	      {
		cout << "\t" << thumbnailList[i] << endl;
	      }
	  }
	targetNode = targetNode.nextSibling();
      }

    
  }

  void report()
  {
    if (m_farmMode)
      {
	report(string("/nfsshare/report.html"));
      }
    else
      {
	boost::posix_time::ptime reportTime = boost::posix_time::second_clock::local_time();
	    
	report(string("report-" + boost::posix_time::to_iso_string(reportTime) + ".html"));
	report(string("report.html"));
      }
  }
    

  

  void report(string outFileName)
  {
    boost::posix_time::ptime reportTime = boost::posix_time::second_clock::local_time();   
 
    fs::ofstream htmlOut( outFileName, ios::out | ios::trunc );

    htmlOut.precision(10);
    
    htmlOut << "<html>" << endl;
    htmlOut << "<head>" << endl;
    htmlOut << "<title>Target Summary</title>" << endl;
    htmlOut << "</head>" << endl;
    htmlOut << "<body>" << endl;
    htmlOut << "<h1>Target Summary: " << reportTime.date() << " - " << reportTime.time_of_day() << "</h1>" << endl;
    
    if (m_farmMode)
      {
	fs::directory_iterator di = fs::directory_iterator("/nfsshare/");
	fs::directory_iterator end = fs::directory_iterator();
	
	while ( di != end )
	  {
	    string currFileString = di->leaf();
	    
	    if ( currFileString.find("target", 0) != string::npos 
		 && currFileString.find("xml", 0) != string::npos )
	      {

		cout << "Processing " << currFileString << endl;
		QFile file( outFileName.c_str() );
		if ( !file.open( IO_ReadOnly ) )
		  {
		    QMessageBox::warning(m_parent, "Load XML", "Could not open file. You should restart.");
		  }
		if ( !m_xmldoc.setContent( &file ) )
		  {
		    QMessageBox::warning(m_parent, "Load XML", "Could not load data from file. You should restart.");
		  }
		file.close();
		
		// TODO: load
	      }
	    di++;
	  }
	
	outputXMLToHTMLStream(htmlOut);
      }
    else
      {
	outputXMLToHTMLStream(htmlOut);
      }
 
    htmlOut << "</body>" << endl;
    htmlOut << "</html>" << endl;
  }

  void outputXMLToHTMLStream(fs::ofstream & htmlOut)
  {
    QDomElement root = m_xmldoc.documentElement();
    QDomNode targetNode = root.firstChild();
    while ( !targetNode.isNull() )
      {
	if ( targetNode.isElement() )
	  {
	    QDomElement targetElement = targetNode.toElement();
	    htmlOut << "<h4>" << targetElement.attribute("id") << "</h4>" << endl;
	    
	    int childCount = 0;
	    double latitudeAccumulator = 0;
	    double longitudeAccumulator = 0;
	    int accumulators[8];
	    for (int i = 0; i < 8; i++)
	      {
		accumulators[i] = 0;
	      }

	    vector<string> thumbnailList;
	    QDomNode confirmationNode = targetElement.firstChild();
	    while ( !confirmationNode.isNull() )
	      {
		if ( confirmationNode.isElement() )
		  {
		    childCount++;
		    QDomElement confirmationElement = confirmationNode.toElement();
		    longitudeAccumulator += boost::lexical_cast<double>(confirmationElement.attribute("longitude"));
		    latitudeAccumulator += boost::lexical_cast<double>(confirmationElement.attribute("latitude"));
		    double orient = boost::lexical_cast<double>(confirmationElement.attribute("orientation"));
		    
		    if (orient > 360 - 22.5 || orient <= 22.5)
		      {
			accumulators[0]++;
		      }
		    else if (orient > 22.5 && orient <= 67.5)
		      {
			accumulators[1]++;
		      }
		    else if (orient > 67.5 && orient <= 112.5)
		      {
			accumulators[2]++;
		      }
		    else if (orient > 112.5 && orient <= 157.5)
		      {
			accumulators[3]++;
		      }
		    else if (orient > 157.5 && orient <= 202.5)
		      {
			accumulators[4]++;
		      }
		    else if (orient > 202.5 && orient <= 247.5)
		      {
			accumulators[5]++;
		      }
		    else if (orient > 247.5 && orient <= 292.5)
		      {
			accumulators[6]++;
		      }
		    else
		      {
			accumulators[7]++;
		      }

		    string temp = confirmationElement.attribute("thumb");
		    thumbnailList.push_back(temp);
		  }
		confirmationNode = confirmationNode.nextSibling();
	      }
	    
	    GpsCoord avgLocation( longitudeAccumulator / childCount, latitudeAccumulator / childCount );
	    htmlOut << "<p>Location: " << avgLocation.toCompetitionString() << "</p>" << endl;
	    
	    int maxAccumulator = -1;
	    int maxVal = 0;
	    for (int curr = 0; curr < 8; curr++)
	      {
		if (accumulators[curr] > maxVal)
		  {
		    maxAccumulator = curr;
		    maxVal = accumulators[curr];
		  }
	      }
	    
	    switch (maxAccumulator)
	      {
	      case 0:
		htmlOut << "<p>Orientation: North</p>" << endl;
		break;
	      case 1:
		htmlOut << "<p>Orientation: NorthEast</p>" << endl;
		break;
	      case 2:
		htmlOut << "<p>Orientation: East</p>" << endl;
		break;
	      case 3:
		htmlOut << "<p>Orientation: SouthEast</p>" << endl;
		break;
	      case 4:
		htmlOut << "<p>Orientation: South</p>" << endl;
		break;
	      case 5:
		htmlOut << "<p>Orientation: SouthWest</p>" << endl;
		break;
	      case 6:
		htmlOut << "<p>Orientation: West</p>" << endl;
		break;
	      case 7:
		htmlOut << "<p>Orientation: NorthWest</p>" << endl;
		break;
	      default:
		htmlOut << "<p>Orientation: ?</p>" << endl;
	      }
		
	    for (int i = 0; i < (int)thumbnailList.size(); i++)
	      {
		htmlOut << "<img src=\"" << thumbnailList[i] << "\"/>" << endl;
	      }
	    htmlOut << "<br /><br />" << endl;
	  }
	targetNode = targetNode.nextSibling();
      }
  }

  void mouseEvent(AGPoint point, int flags)
  {
    int eventType = flags & AG::EventTypeMask;
    int button = flags & AG::MouseButtonMask;
    int keyButton = flags & AG::KeyButtonMask;

    if ( keyButton == AG::ControlButton ||  button == AG::RightButton )
      {
	if ( imageIndex == -1 ) // if not displaying an image
	  {

	  }
	else // if clicking on an actual image
	  {
	    if (imageList[imageIndex].telemAvailable)
	      {
		struct telemetry telem;

		//if ( (eventType == AG::Move || eventType == AG::Release) )
		  {
		    point.x = umbotica::clamp(point.x, 0, highResWidth-1);
		    point.y = umbotica::clamp(point.y, 0, highResHeight-1);
		    telem = imageList[imageIndex].m_telem;

		  }

		if ( eventType == AG::Move || eventType == AG::Release )
		  {
		    m_drag_point[0] = point.x;
		    m_drag_point[1] = point.y;
		    GpsCoord gpsDrag = rotatedGPSCoordFromImageCoord( point.x, point.y, telem );
		    vigra::TinyVector<double, 2> diff = gpsDrag - m_gpsClick;
		    m_orientation_radians = atan2(diff[1], diff[0]);

		    std::cout << "radians: " << m_orientation_radians << endl;

		    // convert to a representation with 0 degrees == north, 90 degrees == east, etc.
		    m_orientation_degrees = (int)Radian::radiansToDegrees(m_orientation_radians);
		    if (m_orientation_degrees < 0)
		      {
			m_orientation_degrees += 360;
		      }
		    m_orientation_degrees = 90 - m_orientation_degrees;
		    if (m_orientation_degrees < 0)
		      {
			m_orientation_degrees += 360;
		      }
 
		    clickedOrientationString = boost::lexical_cast<string>(m_orientation_degrees);
		  }
		else
		  {
		    m_click_point[0] = point.x;
		    m_click_point[1] = point.y;
		    m_gpsClick = rotatedGPSCoordFromImageCoord( point.x, point.y, telem );
		    clickedGPSLongitude = m_gpsClick.longitudeString();
		    clickedGPSLatitude = m_gpsClick.latitudeString();
		  }
	      }
	    else
	      {
		clickedGPSLongitude = string("no telemetry");
		clickedGPSLatitude = string("no telemetry");
	      }
	  }
	updateGui();
      }
  }

  boost::shared_ptr<AGImageContainer> getImageContainer()
  {
    return boost::dynamic_pointer_cast<AGImageContainer>(imageContainer);
  }

private:
  vector<AGShape> imageShapes;
  vector<AGShape> overlayShapes;
  vector<HiResImage> imageList;
  double gpsTop, gpsBottom, gpsLeft, gpsRight;
  int imageIndex; // the currently viewed image (-1 = overlay)
  int lastImageIndex; // used to trigger update of the image buffer
  string bgImagePath;
  vector<TargetConfirmationPoint> targetVector;
  GpsCoord m_gpsClick;
  double m_orientation_radians;
  int m_orientation_degrees;
  vigra::TinyVector<int, 2> m_click_point;
  vigra::TinyVector<int, 2> m_drag_point;
  string outFileName;
  ofstream outStream;
  QWidget* m_parent;
  QDomDocument m_xmldoc;
  string m_thumbnailDirectory;
  int m_farmMode;

  int getOperatorID()
  {
    return QInputDialog::getInteger("High Res Viewer", "Enter operator ID: ", 1, 1, 3, 1, 0, m_parent);
  }

  void prepareXMLDoc()
  {
    m_xmldoc = QDomDocument( "targetXML" );

    boost::posix_time::ptime startTime;
    QStringList xmlFiles;
    QString result;
    bool makeNew = false;

    if (!m_farmMode)
      {
	fs::directory_iterator di = fs::directory_iterator(fs::initial_path());
	fs::directory_iterator end = fs::directory_iterator();
	
	while ( di != end )
	  {
	    string currFileString = di->leaf();
	    
	    if ( currFileString.find("target", 0) != string::npos 
		 && currFileString.find("xml", 0) != string::npos )
	      {
		xmlFiles.push_back( QString(currFileString) );
	      }
	    
	    di++;
	  };
	
	
	if ( (int)xmlFiles.size() != 0 )
	  {
	    bool okay;
	    result = QInputDialog::getItem( "Standalone Image Station",
					    "Former target xml file(s) were found.\n\n\nSelect partial file or select cancel to create a new file:",
					    xmlFiles, 0, false,
					    &okay,
					    m_parent );
	    if ( okay )
	      {
		outFileName = string(result.ascii());
		QFile file( result.ascii() );
		if ( !file.open( IO_ReadOnly ) )
		  {
		    QMessageBox::warning(m_parent, "Load XML", "Could not open file. You should restart.");
		  }
		if ( !m_xmldoc.setContent( &file ) )
		  {
		    QMessageBox::warning(m_parent, "Load XML", "Could not load data from file. You should restart.");
		  }
		file.close();
		
		QDomElement root = m_xmldoc.documentElement();
		QDomElement xmlTarget = root.firstChild().toElement();
		
		while ( !xmlTarget.isNull() )
		  {
		    string targetID = xmlTarget.attribute("id");
		    QDomElement xmlConfirmation = xmlTarget.firstChild().toElement();
		    
		    while ( !xmlConfirmation.isNull() )
		      {
			TargetConfirmationPoint targetPoint;
			targetPoint.id = targetID;
			targetPoint.location = 
			  GpsCoord(boost::lexical_cast<double>(xmlConfirmation.attribute("longitude")),
				   boost::lexical_cast<double>(xmlConfirmation.attribute("latitude")));
			targetPoint.orientation = boost::lexical_cast<int>(xmlConfirmation.attribute("orientation"));
			string temp = xmlConfirmation.attribute("thumb");
			targetPoint.thumbnail = temp;
			targetVector.push_back(targetPoint);
			
			xmlConfirmation = xmlConfirmation.nextSibling().toElement();
		      } // for each confirmation point
		    xmlTarget = xmlTarget.nextSibling().toElement();
		  } // for each target
		
		cout << "Loaded " << targetVector.size() << " previously confirmed targets." << endl;
		for ( int i = 0; i < (int)targetVector.size(); i++ )
		  {
		    cout << targetVector[i].id << endl;
		    cout << "\t" << targetVector[i].location.toCompetitionString() << endl;
		    cout << "\t" << targetVector[i].orientation << endl;
		    cout << "\t" << targetVector[i].thumbnail << endl;
		  }
		
	      }
	    else
	      {
		makeNew = true;
	      }
	
	  }
	else
	  {
	    makeNew = true;
	  }
	
	if ( makeNew )
	  {
	    QDomElement root = m_xmldoc.createElement( "TargetList" );
	    m_xmldoc.appendChild( root );
	    
	    startTime = boost::posix_time::second_clock::local_time();
	    outFileName = string("target-" + boost::posix_time::to_iso_string(startTime) + ".xml");
	    saveXML();
	  }
      }
    else // if farm mode
      {
	QDomElement root = m_xmldoc.createElement( "TargetList" );
	m_xmldoc.appendChild( root );
	
	startTime = boost::posix_time::second_clock::local_time();
	outFileName = string("/nfsshare/target_" + boost::lexical_cast<string>(m_operator_id) + string("_") + ".xml");
	saveXML();
      }
  }


  void addToXML(TargetConfirmationPoint target)
  {
    QDomElement newConfirmation = m_xmldoc.createElement("confirmation");
    newConfirmation.setAttribute("longitude", target.location.longitude());
    newConfirmation.setAttribute("latitude", target.location.latitude());
    newConfirmation.setAttribute("orientation", target.orientation);
    newConfirmation.setAttribute("thumb", target.thumbnail);

    QDomElement root = m_xmldoc.documentElement();

    bool added = false;
    QDomNode oldTarget = root.firstChild();
    while ( !oldTarget.isNull() && !added )
      {
	if ( oldTarget.isElement() ) {
	  QDomElement e = oldTarget.toElement();
	  if (e.attribute("id") == target.id) // add a confirmation to this one
	    {
	      e.appendChild(newConfirmation);
	      added = true;
	    }
	}
	oldTarget = oldTarget.nextSibling();
      }
    if (!added)
      {
	QDomElement newTarget = m_xmldoc.createElement("target");
	newTarget.setAttribute("id", target.id);
	root.appendChild(newTarget);
	newTarget.appendChild(newConfirmation);
      }
  }


  void saveXML()
  {

    cout << "saving xml to: " << outFileName << endl;
    outStream.open( outFileName.c_str(), ios_base::out | ios_base::trunc );
    outStream << m_xmldoc.toString();
    outStream.close();
  }
  
  void gpsToBGCanvas(GpsCoord c, int *canvasX, int *canvasY)
  {
    *canvasX = (int)(bgImage.width() * (c.longitude() - gpsLeft) / (gpsRight - gpsLeft) );
    *canvasY = (int)(bgImage.height() * (c.latitude() - gpsTop) / (gpsBottom - gpsTop));
  }

  void canvasToGps(int canvasX, int canvasY, GpsCoord & c)
  {
    // longitude
    c[0] = ( canvasX / (double)(image.width() - 1) ) * (gpsRight - gpsLeft) + gpsLeft;
    // latitude
    c[1] = ( canvasY / (double)(image.height() - 1 ) ) * (gpsBottom - gpsTop) + gpsTop;
  }

};

vigra::TinyVector<int, 2> findCanvasFromGPS(GpsCoord query, telemetry telem, int xMin, int xMax, int yMin, int yMax)
{
  int xRange = xMax - xMin;
  int yRange = yMax - yMin;

  if (xRange <= 1 || yRange <= 1)
    {
      return vigra::TinyVector<int, 2>(xMin, yMin);
    }
  else
    {
      // Get gps coords of the four quadrants of this search
      vector<GpsCoord> quadrantCentres;
      quadrantCentres.push_back(rotatedGPSCoordFromImageCoord( xMin + xRange / 4 , yMin + yRange / 4, telem )); // top left
      quadrantCentres.push_back(rotatedGPSCoordFromImageCoord( xMin + 3 * xRange / 4, yMin + yRange / 4, telem )); // top right
      quadrantCentres.push_back(rotatedGPSCoordFromImageCoord( xMin + xRange / 4, yMin + 3 * yRange / 4, telem )); // lower left
      quadrantCentres.push_back(rotatedGPSCoordFromImageCoord( xMin + 3 * xRange / 4, yMin + 3 * yRange / 4, telem )); // lower right

      double minDistance = numeric_limits<double>::max();

      int closestQuadrant = 0;

      for (int quad = 0; quad < 4; quad++)
	{
	  double thisDistance = query.straightLineDistance(quadrantCentres[quad]);
	  if (thisDistance < minDistance)
	    {
	      closestQuadrant = quad;
	      minDistance = thisDistance;
	    }
	}

      switch (closestQuadrant)
	{
	case 0:
	  return findCanvasFromGPS(query, telem, xMin, xMin + xRange / 2, yMin, yMin + yRange / 2);
	  break;
	case 1:
	  return findCanvasFromGPS(query, telem, xMin + xRange / 2, xMax, yMin, yMin + yRange / 2);
	  break;
	case 2:
	  return findCanvasFromGPS(query, telem, xMin, xMin + xRange / 2, yMin + yRange / 2, yMax);
	  break;
	case 3:
	  return findCanvasFromGPS(query, telem, xMin + xRange / 2, xMax, yMin + yRange / 2, yMax);
	  break;
	default:
	  return vigra::TinyVector<int, 2>(0, 0);
	}
      
    }
}


GpsCoord rotatedGPSCoordFromImageCoord( int x, int y, struct telemetry telem )
{
  double rotatedYaw = (telem.track * M_PI / 180) + M_PI_2;
  double rotatedPitch = -telem.roll;
  double rotatedRoll = telem.pitch;

  return AuvsiCalibration::Instance()->gpsCoordFromImageCoord( HighRes, x, y,
							       rotatedRoll,
							       rotatedPitch,
							       rotatedYaw,
							       GpsCoord( telem.longitude, 
									 telem.latitude ),
							       telem.altitude * 0.3048 );
}

int main(int argc, char* argv[])
{
  QApplication a(argc, argv);
  AGMainWindow w;
  HiResViewer* hrViewer;

  bool failure; // any failure, partial or complete should set this flag to true

  w.resize(1280, 980);

  a.connect( &a, SIGNAL( lastWindowClosed() ), &a, SLOT( quit() ) );
  AGManager & gui = *w.getAGManager();
    
  if( !Configuration::init("standalone.conf") )
    {
      failure = true;
      cout << "Config file error\n";
      exit(1);
    }
  
  Configuration calibrationConfig("Calibration");
  Configuration commonConfig("Common");
  Configuration overlayConfig("Overlay");
  Configuration farmConfig("Farm");

  string highResCalibrationFile = calibrationConfig.get<string>("highResFile", "calibration.dat");
  highResWidth = calibrationConfig.get<int>("highResWidth", 2032);
  highResHeight = calibrationConfig.get<int>("highResHeight", 1524);

  int farmMode = farmConfig.get<int>("farm", 0);

  double referenceLongitude = commonConfig.get<double>("referenceLongitude", -97.0);
  double referenceLatitude = commonConfig.get<double>("referenceLatitude", 50.0);
  string thumbnailDirectory;
  if (!farmMode)
    {
      thumbnailDirectory = commonConfig.get<string>("thumbnailDirectory", "thumbs");
    }
  else
    {
      thumbnailDirectory = string("/nfsshare/thumbs");
    }

  string overlayBackgroundImage = overlayConfig.get<string>( "backgroundImage", string("local-color.jpg") );
  double overlayGpsTop = overlayConfig.get<double>( "gpsTop", 45 );
  double overlayGpsBottom = overlayConfig.get<double>( "gpsBottom", -45 );
  double overlayGpsLeft = overlayConfig.get<double>( "gpsLeft", -90 );
  double overlayGpsRight = overlayConfig.get<double>( "gpsRight", 90 );

  if ( !fs::exists(thumbnailDirectory) )
    {
      fs::create_directory(thumbnailDirectory);
    }
  else if ( !fs::is_directory(thumbnailDirectory) )
    {
      throw std::runtime_error("Cannot create " + thumbnailDirectory + ". It already exists as a plain file.");
    }

  GpsCoord::setReference(GpsCoord(referenceLongitude, referenceLatitude));
  AuvsiCalibration::Instance()->initialize(HighRes, highResWidth, highResHeight, highResCalibrationFile);

  hrViewer = new HiResViewer( overlayBackgroundImage, overlayGpsTop, overlayGpsBottom, 
			      overlayGpsLeft, overlayGpsRight, thumbnailDirectory, farmMode, gui.getCurrentParent() );

  gui.pushGroup("Hi Res Viewer");
  //  addToGui(gui, hrViewer->image, boost::bind(&HiResViewer::getImageShapes, hrViewer), boost::bind(&HiResViewer::mouseEvent, 
  //										     hrViewer, _1, _2), "Hi Res", 200);
  gui.addFrameBuffer("Hi Res Viewer", boost::bind(&HiResViewer::getImageContainer,hrViewer),
		     boost::bind(&HiResViewer::getImageShapes, hrViewer), boost::bind(&HiResViewer::mouseEvent, hrViewer, _1, _2), 800); 

  gui.setWindowSize(950, 800);
  gui.popGroup();

  gui.pushGroup("Control");
  gui.addInt("", boost::bind(&HiResViewer::getCurrIndexPlusOne, hrViewer));
  gui.addInt("/", boost::bind(&HiResViewer::getTotal, hrViewer));
  gui.nextLine();
  gui.addButton("Load Images", boost::bind(&HiResViewer::loadImages, hrViewer, gui.getCurrentParent()));
  gui.nextLine();
  gui.addButton("Five Back", boost::bind(&HiResViewer::fiveBack, hrViewer));
  gui.nextLine();
  gui.addButton("Prev Image", boost::bind(&HiResViewer::previousImage, hrViewer));
  gui.nextLine();
  gui.addButton("Next Image", boost::bind(&HiResViewer::nextImage, hrViewer));
  gui.nextLine();
  gui.addButton("Five Ahead", boost::bind(&HiResViewer::fiveAhead, hrViewer));
  gui.nextLine();
  gui.addString("Long: ", hrViewer->clickedGPSLongitude, true);
  gui.nextLine();
  gui.addString("Lat: ", hrViewer->clickedGPSLatitude, true);
  gui.nextLine();
  gui.addString("Orient: ", hrViewer->clickedOrientationString, true);
  gui.nextLine();
  gui.addString("ID: ", hrViewer->targetID, true);
  gui.nextLine();
  gui.addButton("Confirm", boost::bind(&HiResViewer::confirmTarget, hrViewer));
  gui.nextLine();
  gui.addButton("Review All", boost::bind(&HiResViewer::review, hrViewer));
  gui.nextLine();
  gui.addButton("Report", boost::bind(&HiResViewer::report, hrViewer));
  gui.nextLine();
  addToGui(gui, hrViewer->bgImage, boost::bind(&HiResViewer::getOverlayShapes, hrViewer), "Overlay", 500);
  gui.setWindowTimer(300);
  gui.setWindowSize(200, 1000);
  gui.popGroup();
  
  w.show();
  int ret = a.exec();
  
  if ( hrViewer )
    {
      delete hrViewer;
    }

  return ret;
}

