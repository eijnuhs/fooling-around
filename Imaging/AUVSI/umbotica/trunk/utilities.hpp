#ifndef UMB_UTILS_DEP
#define UMB_UTILS_DEP

#warning(the umbotica header utilities.hpp has been deprecated.  Please use math/math.hpp)

#include <umbotica/math/math.hpp>

#endif
