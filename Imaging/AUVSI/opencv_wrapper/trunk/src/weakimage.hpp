#ifndef _WEAK_IMAGE_H_
#define _WEAK_IMAGE_H_

#include "imageformats.hpp"
#include "boost/shared_array.hpp"
#include <inttypes.h>
#include "umimage.hpp"
//#include "rgb565value.hxx"
#include <vigra/rgbvalue.hxx>
#include "bgrvalue.hxx"
#include "bgravalue.hxx"

// This null deleter allows us to pass boost::shared_arrays out
// without the worry that the mapped memory will be deleted.
template<class BufferType>
struct NullDeleter
{
  void operator()(BufferType *){};
};

template<class PixelType>
class UmImage;

// the weak image.
// This class should work similar to the
// std::auto_ptr. 
// it should pass out exactly one shared_array
class WeakImage
{
public:
  WeakImage(uint8_t * buffer = 0, 
	    unsigned int width = 0, 
	    unsigned int height = 0, 
	    ImageFormats::ImageFormat format = ImageFormats::UNSUPPORTED,
	    ImageFormats::Fields field = ImageFormats::INTERLACED,
	    bool deleteBuffer = false): 
    buffer(buffer), width(width), height(height), 
    format(format), field(field), deleteBuffer(deleteBuffer){}
  unsigned int getWidth(){return width;}
  unsigned int getHeight(){return height;}
  ImageFormats::ImageFormat getFormat(){return format;}
  ImageFormats::Fields getField(){return field;}
  
  // std::auto_ptr style copy constructor
  // and assignment operator
  WeakImage(WeakImage & rhs)
  {
    copy(rhs);
  }
  WeakImage & operator=(WeakImage & rhs)
  {
    copy(rhs);
    return *this;
  }

  ~WeakImage()
  {
    if(deleteBuffer && buffer)
      {
	std::cout << "weak image deleting buffer\n";
	delete buffer;
      }
  }
  // std::auto_ptr style. copies the memory and
  // wipes the original
  void copy(WeakImage & rhs)
  {
    height = rhs.height;
    width = rhs.width;
    buffer = rhs.buffer;
    format = rhs.format;
    deleteBuffer = rhs.deleteBuffer;
    rhs.wipe();
  }

  //template <class ImageType>
  //void packUmImage( ImageType  & image);
  template <class PixelType>
  void packUmImage(UmImage<PixelType>  & image);

  template<class PixelDestination, class PixelSource>
  void copyBuffer(UmImage<PixelDestination> & image);

  void init(uint8_t * buffer = 0, 
	    unsigned int width = 0, 
	    unsigned int height = 0, 
	    ImageFormats::ImageFormat format = ImageFormats::UNSUPPORTED,
	    ImageFormats::Fields field = ImageFormats::INTERLACED,
	    bool deleteBuffer = false)
  {
    if(deleteBuffer && buffer)
      delete buffer;
    
    this->buffer = buffer;
    this->width = width;
    this->height = height;
    this->format = format;
    this->field = field;
    this->deleteBuffer = deleteBuffer;
  } 

private:
  void wipe()
  {
    height = 0;
    width = 0;
    buffer = 0;
    format = ImageFormats::UNSUPPORTED;
    deleteBuffer = false;
  }


  uint8_t * buffer;
  unsigned int width;
  unsigned int height;
  ImageFormats::ImageFormat format;
  ImageFormats::Fields field;
  bool deleteBuffer; // determines if the buffer must be deleted.
};


template<class PixelDestination, class PixelSource>
void WeakImage::copyBuffer(UmImage<PixelDestination> & image)
{
  boost::shared_array<PixelSource> newBuffer;
  if(deleteBuffer)
    {	
      // pass ownership to smart pointer with teeth.
      newBuffer.reset((PixelSource*)buffer);
    }
  else
    {
      // create a smart ptr with the null deleter
      // the buffer will not be deleted.
      newBuffer.reset((PixelSource*)buffer, NullDeleter<PixelSource>());
    }
  UmImage<PixelSource> temp(newBuffer,width,height);
  image.copyFrom(temp);
}

template <class PixelType>
void WeakImage::packUmImage( UmImage<PixelType> & umimage)
{
  if((int)format != (int)umbotica::V4L2Traits<PixelType>::ImageFormat)
    {
      // copy the image over.  This is pretty ugly but there's no other way I know of
      // to map a variable to a type dynamically.
      switch(format)
	{
	case ImageFormats::RGB565:
	  //	  copyBuffer<PixelType,vigra::Rgb565Value>(umimage);
	  std::cout << "RGB565 is not supported yet\n";
	  break;
	case ImageFormats::RGB24:
	  copyBuffer<PixelType,vigra::RGBValue<uint8_t> >(umimage);
	  break;
	case ImageFormats::BGR24:
	  copyBuffer<PixelType, vigra::BGRValue<uint8_t> >(umimage);
	  break;
	case ImageFormats::RGB32:
	  copyBuffer<PixelType, vigra::RGBAValue<uint8_t> >(umimage);
	  break;
	case ImageFormats::BGR32:
	  copyBuffer<PixelType, vigra::BGRAValue<uint8_t> >(umimage);
	  break;
	case ImageFormats::GREY:
	  copyBuffer<PixelType, uint8_t>(umimage);
	  break;
	default:
	  std::cerr << "Weak image asked to perform a conversion on from an unsupported type\n";
	}
    }
  else
    {

      boost::shared_array<PixelType> newBuffer;
      if(deleteBuffer)
	{	
	  // pass ownership to smart pointer with teeth.
	  newBuffer.reset((PixelType*)buffer);
	}
      else
	{
	  // create a smart ptr with the null deleter
	  // the buffer will not be deleted.
	  newBuffer.reset((PixelType*)buffer, NullDeleter<PixelType>());
	}
      
      // take control of the  buffer.
      umimage.cv_init(newBuffer,width, height, 0);	
    }
  // erase any memory of this event.
  // like a std::auto_ptr the weak image
  // may pass on its data exactly once.
  wipe();
}




#endif

