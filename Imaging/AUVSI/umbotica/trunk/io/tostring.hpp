#ifndef _UMB_TO_STRING_H_
#define _UMB_TO_STRING_H_

#include <sstream>

namespace umbotica
{
  template<typename T>
  std::string s_tuple(T t)
  {
    std::ostringstream s;
    s << t;
    return s.str();
  }

  template<typename T, typename U>
  std::string s_tuple(T t, U u)
  {
    std::ostringstream s;
    s << "[" << t << "," << u << "]";
    return s.str();
  }

  template<typename T, typename U, typename V>
  std::string s_tuple(T t, U u, V v)
  {
    std::ostringstream s;
    s << "[" << t << "," << u << "," << v << "]";
    return s.str();
  }

  template<typename T, typename U, typename V, typename W>
  std::string s_tuple(T t, U u, V v, W w)
  {
    std::ostringstream s;
    s << "[" << t << "," << u << "," << v << "," << w << "]";
    return s.str();
  }

  template<typename T, typename U, typename V, typename W, typename X>
  std::string s_tuple(T t, U u, V v, W w, X x)
  {
    std::ostringstream s;
    s << "[" << t << "," << u << "," << v << "," << w << "," << x <<"]";
    return s.str();
  }

  template<typename T, typename U, typename V, typename W, typename X, typename Y>
  std::string s_tuple(T t, U u, V v, W w, X x, Y y)
  {
    std::ostringstream s;
    s << "[" << t << "," << u << "," << v << "," << w << "," << x << "," << y << "]";
    return s.str();
  }

  template<typename T, typename U, typename V, typename W, typename X, typename Y, typename Z>
  std::string s_tuple(T t, U u, V v, W w, X x, Y y, Z z)
  {
    std::ostringstream s;
    s << "[" << t << "," << u << "," << v << "," << w << "," << x << "," << y << "," << z << "]";
    return s.str();
  }


  template<typename T>
  std::string arr(T * thearr, unsigned int size)
  {
    return arr(thearr, thearr + size);
  }
  template<typename Iterator>
  std::string arr(Iterator start, Iterator end)
  {
    std::ostringstream s;
    s << "[";
    Iterator curr = start;
    for(; curr < end; ++curr)
      {
	s << *curr;
	if( (curr + 1) != end)
	  s << ",";
      }
    s << "]";
    return s.str();
  }
  
}



#endif
