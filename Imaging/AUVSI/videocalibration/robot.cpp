#include "robot.h"
#include <iostream>
#include "run.h"
#include "framebuffer.h"
#include "framebufferworld.h"
#include "featurecircle.h"
#include "videointerpreter2d.h"
#include <sstream>

// the portion of the radius feature circle definitions
const double CIRCLES[C_NUM] = {0.5,0.56,0.62};

#define NO_OF_CELLS 8
#define NO_OF_C_CELLS 6

// the width of a cell in radians.
#define CELL_RAD (2 * M_PI) / (double)NO_OF_CELLS

#define CELL_LENGTH 15

#define RAD_P_P CELL_RAD / (double) CELL_LENGTH

#define NUM_OF_P CELL_LENGTH * 8

#define MIN_RATIO .75
#define MAX_RATIO 1.25

#define SCRATCHPAD_SIZE 20


// static member initialization
void swap(int &item1, int &item2)
{
  int temp = item1;
  item1 = item2;
  item2 = temp;
}


Robot::Robot(std::string name, double height, double radius, double mmPerPix, int subtype) :
  Trackable(ROBOT,name,height,radius,mmPerPix,subtype),
  intensityArrays(0),
  medianIntensities(0),
  hist(256),
  black(0),
  white(0),
  minRatio(MIN_RATIO),
  maxRatio(MAX_RATIO)
{
  intensityArrays = new unsigned int * [C_NUM];
  for(uint i = 0; i < C_NUM; ++i)
    {
      intensityArrays[i] = new unsigned int[NUM_OF_P];
    }
  medianIntensities = new unsigned int[NUM_OF_P];
  
  // initialize the feature circles.
  for(uint i = 0; i < C_NUM; ++i)
    {
      features[i]  = new FeatureCircle(CIRCLES[i]*radius, RAD_P_P);
    }  

  Trackable::scratchpad()->initialize(FrameBuffer::RGB32, SCRATCHPAD_SIZE,SCRATCHPAD_SIZE);
}


Robot::~Robot()
{
  for(uint i = 0; i < C_NUM; ++i)
    {
      delete [] intensityArrays[i];
    }
  delete [] intensityArrays;
  delete [] medianIntensities;
  for(uint i = 0; i < C_NUM; ++i)
    {
      delete features[i];
    }
}

bool Robot::trackMe(FrameBuffer * inFrame, FrameBufferWorld * worldFrame)
{
  bool found = false;
  double wx,wy, theta;
  int subtype;
  //  unsigned int lrx,lry;
  //  unsigned int startX, endX, startY, endY;
  //  unsigned int radiusX, radiusY;
  if(getPercentFound() > 0.01)
    {
      //      std::cout << "Robot::trackMe()\n";
      predictPosition(wx,wy,inFrame->t);
      //      std::cout << "Predicted Position: " << wx << "," << wy << "\n";
      if(findRobot(wx,wy,inFrame,&subtype,&theta))
	{
	  //	  std::cout << "Robot::trackMe() Robot Found:" << subtype << "\n";
	  if(subtype == getSubtype())
	    {
	      setPosition(wx,wy, theta, inFrame->t); 
	      found = true;
	      
	      Trackable::maskObject(worldFrame);

	      // add a debug view to the scratchpad;
	      //	      if(USE_SCRATCHPAD)
		addMeToScratchpad(inFrame);
	    }
	}
    }
  return found;
}

// attempts to find a robot with centre world coordinates wx,wy
bool Robot::findRobot(double & wx, double & wy, FrameBuffer * inFrame, int * retSubtype, double * retTheta)
{
  bool found = false;   
  unsigned int min;     // max and min intensity
  unsigned int max;
  //  unsigned int intensities[features[0]->getSize()];
  int cellColours[NO_OF_C_CELLS] = {0,0,0,0,0,0}; // >0 is oher <0 is white
  double theta;
  int subtype, mask;
  unsigned int avg, lmax,lmin, lmed;
  unsigned int size = features[0]->getSize();

  // for each feature circle
  hist.reset();
  for(unsigned int i = 0; i < C_NUM; ++i)
    {
      // interpolate the strip of pixels
      interpolateStrip(wx, wy, *(features[i]), intensityArrays[i], min, max, avg, inFrame);
      // display debugging info
      paintDebugFrame(wx, wy, *(features[i]), intensityArrays[i]); 
    }

  // find the median intensity for each cell
  for(uint i = 0; i < size; i++)
    {
      lmax = lmin = lmed = 0;
      for(uint j = 1; j < C_NUM; j++)
	{
	  lmax = intensityArrays[lmax][i] < intensityArrays[j][i] ? lmax : j;
	  lmin = intensityArrays[lmin][i] > intensityArrays[j][i] ? lmin : j;
	}
      
      for(uint j = 1; j < C_NUM; j++)
	if(lmax != j && lmin!=j)
	  lmed = j;
      medianIntensities[i] = intensityArrays[lmed][i];

    }

  // colour threshold the intensities
  colourThreshold(min,max,avg,medianIntensities);     
  // distinguish robot
  found = identifyRobot(medianIntensities, cellColours, theta, wx, wy);
  
  if(found)
    {
      subtype = 0;
      mask = 1;
      
      for(int i = 0; i < NO_OF_C_CELLS; i++)
	{
	  if(cellColours[i] > 0)
	    subtype += mask;
	  
	  mask = mask << 1;
	}
  
      *retTheta = theta;
      *retSubtype = subtype;
      found = subtype != 0;
    }
  
  
  return found;
}


#if 0
// attempts to find a robot with centre world coordinates wx,wy
bool Robot::findRobot(double & wx, double & wy, FrameBuffer * inFrame, int * retSubtype, double * retTheta)
{
  bool found = false;   
  unsigned int min;     // max and min intensity
  unsigned int max;
  //  unsigned int intensities[features[0]->getSize()];
  int cellColours[NO_OF_C_CELLS] = {0,0,0,0,0,0}; // >0 is oher <0 is white
  int successCount = 0;
  double theta;
  int subtype, mask;
  double avgTheta = 0;
  double lastTheta = 0;
  unsigned int avg;
  // for each feature circle
  hist.reset();
  for(unsigned int i = 0; i < C_NUM && !found; ++i)
    {
      // interpolate the strip of pixels
      interpolateStrip(wx, wy, *(features[i]), intensityArrays[i], min, max, avg, inFrame);
      // colour threshold the intensities
      colourThreshold(min,max,avg,intensityArrays[i]);     
      // distinguish robot
      if(identifyRobot(intensityArrays[i], cellColours, theta, wx, wy))
	{
	  //	std::cout << "ID success: theta:" << theta << "  last theta: " << lastTheta << "\n";
	  successCount++;
	  if(successCount == 1)
	    {
	      lastTheta = theta;
	    }
	  else
	    {
	      // account for wraparound in the orientation
	      if(fabs(lastTheta - theta) > M_PI)
		{
		  //		  std::cout << "flipping sign of theta\n";
		  theta = -theta;
		}
	    }
	  avgTheta += theta;
	}
      
      // display debugging info
      paintDebugFrame(wx, wy, *(features[i]), intensityArrays[i]);
      
    }
  
  // determine which robot it is based on the features.
  if(successCount > 1)
    {
      // account for wraparound in orientation.
      theta = avgTheta / (double)successCount;
      if(theta > M_PI)
	{
	  theta = -(2.0 * M_PI) + theta;
	}
      else if(theta < -M_PI)
	{
	  theta = (2.0 * M_PI) + theta;
	}
      subtype = 0;
      mask = 1;

      for(int i = 0; i < NO_OF_C_CELLS; i++)
	{
	  if(cellColours[i] > 0)
	    subtype += mask;
	  
	  mask = mask << 1;
	}

      found = true;
      *retTheta = theta;
      *retSubtype = subtype;
      
    }
  return found;
}

#endif


bool Robot::isMe(RunRegion & region, FrameBuffer * inFrame, FrameBufferWorld * worldFrame, unsigned int avgIntensity)
{
  bool found = false;
  int subtype = avgIntensity; // kill warning = bad programming
  double theta;
  double lrx,lry,wx,wy;

  // check if the region has been previously identified
  if(region.type == Trackable::ROBOT)
    {
      found = (region.subtype == getSubtype());
      lrx = region.getCOMXD();
      lry = region.getCOMYD();
      worldFrame->bufferToWorldCoordinates(lrx,lry,&wx,&wy);
    }
  else if(region.numPixels > minPixels && region.numPixels < maxPixels)
    {
      // otherwise, try to identify the region.

      lrx = region.getCOMXD();
      lry = region.getCOMYD();
      
      //       lrx = region.getAvgCentreX();
      //       lry = region.getAvgCentreY();
      //       lrx = region.getBBCentreX();
      //       lry = region.getBBCentreY();
      
      worldFrame->bufferToWorldCoordinates(lrx,lry,&wx,&wy);
      if(findRobot(wx,wy,inFrame, &subtype, &theta))
	{
	  region.type = Trackable::ROBOT;
	  region.subtype = subtype;
	  region.theta = theta;
	  found = subtype == getSubtype();
	  //	  std::cout << "Robot isMe() wx,wy: (" << wx << "," << wy << ")\n";
	}
    }
  
  if(found)
    {
      //      std::cout << "Robot isMe() setting position: wx,wy: (" << wx << "," << wy << ")\n";
      setPosition(wx,wy, region.theta, inFrame->t); 
      region.used = true;
    }
  return found;
  
}

void Robot::paintDebugFrame(double wxc, double wyc, FeatureCircle & feature, unsigned int intensities[]){
  RawPixel pixel;
  unsigned int dbx,dby;     // debug frame x and y;

  if(Trackable::debugFrame)
    {  
      for(uint k = 0; k < feature.getSize(); ++k)
	{
	  //      std::cout << intensities[k];

	  // std::cout << "there is a debug frame\n";
	  // draw the pixel in the debug frame
	  double wx = wxc + feature[k].column;
	  double wy = wyc + feature[k].row;
	  
	  if(!Trackable::debugFrame->worldToBufferCoordinates(wx,wy,&dbx,&dby))
	    {
	      if(intensities[k] == R_BLACK)
		pixel = RawPixel(23,23,23);
	      else if(intensities[k] == R_WHITE)
		pixel = RawPixel(200,200,200);
	      else
		pixel = RawPixel(255,0,0);
	      //		std::cout << "adding (" << dby << "," << dbx << ")\n";
	      Trackable::debugFrame->setPixelAt( dby, dbx, pixel );
	    }
	}
    }
  //  std::cout << "\n";
  
}



void Robot::colourThreshold(unsigned int min, unsigned int max, unsigned int avg, unsigned int intensities[])
{
  Histogram localHistogram(256);
  unsigned int weightedHistogram[256];
  unsigned int size = features[0]->getSize();
  unsigned int left, right;
  unsigned int total = avg = min; // killing warning = bad programming.
  for(uint i = 0; i < size; ++i)
    localHistogram.increment(intensities[i]);

  for(uint i = 0; i < 256; ++i)
    weightedHistogram[i] = (localHistogram.get(i) * i);
  
  // the black threshold is set so that it captures 20%
  // of the strip.
  black = localHistogram.getLowerThreshold(25);
  white = max - ((max - black) / 2);

  // std::cout << "black: " << black << "  white: " << white << "\n";

  // 
  for(uint i = 0; i < 2; i++)
    {

      total = left = right = 0;
      for(uint j = black; j < white; j++)
	{
	  left += weightedHistogram[j];
	  total += localHistogram.get(j);
	}
      left = ((total == 0) || (left == 0)) ? black : left / total;
      total = 0;
      for(uint j = white; j <= max; j++)
	{
	  right += weightedHistogram[j];
	  total += localHistogram.get(j);
	}
      right = ((total == 0) || (right == 0)) ? max : right / total;
  
      white = (left + right) / 2;
      //      std::cout << "setting white: (" << left << " + " << right << ") / 2 = " << white << "\n";
    }
  // threshold the colors
  for(uint i = 0; i < features[0]->getSize(); ++i)
    {
      if(intensities[i] < black)
	intensities[i] = R_BLACK;
      else if(intensities[i] > white)
	intensities[i] = R_WHITE;
      else
	intensities[i] = R_OTHER;
    }  
}
#if 0
void Robot::colourThreshold(unsigned int min, unsigned int max, unsigned int avg, unsigned int intensities[])
{
  Histogram localHistogram(256);
  unsigned int weightedHistogram[256];
  unsigned int size = features[0]->getSize();
  unsigned int left, right, range;
  unsigned int total = avg;
  for(uint i = 0; i < size; ++i)
    localHistogram.increment(intensities[i]);

  for(uint i = 0; i < 256; ++i)
    weightedHistogram[i] = (localHistogram.get(i) * i);
  
  range = (max - min) / 3;

  black = min + range;
  white = max - range;

  //  std::cout << "black: " << black << "  white: " << white << "\n";

  for(uint i = 0; i < 2; i++)
    {
      total = left = right = 0;
      for(uint j = min; j < black; j++)
	{
	  left += weightedHistogram[j];
	  total += localHistogram.get(j);
	}
      left = ((total == 0) || (left == 0)) ? min : left / total;
      total = 0;
      for(uint j = black; j < white; j++)
	{
	  right += weightedHistogram[j];
	  total += localHistogram.get(j);
	}
      right = ((total == 0) || (right == 0)) ? white : right / total;
      
      
      black = (left + right) / 2;
      //  std::cout << "setting black: (" << left << " + " << right << ") / 2 = " << black << "\n";
      
      total = left = right = 0;
      for(uint j = black; j < white; j++)
	{
	  left += weightedHistogram[j];
	  total += localHistogram.get(j);
	}
      left = ((total == 0) || (left == 0)) ? black : left / total;
      total = 0;
      for(uint j = white; j <= max; j++)
	{
	  right += weightedHistogram[j];
	  total += localHistogram.get(j);
	}
      right = ((total == 0) || (right == 0)) ? max : right / total;
  
      white = (left + right) / 2;
      //      std::cout << "setting white: (" << left << " + " << right << ") / 2 = " << white << "\n";
    }
  // threshold the colors
  for(uint i = 0; i < features[0]->getSize(); ++i)
    {
      if(intensities[i] < black)
	intensities[i] = R_BLACK;
      else if(intensities[i] > white)
	intensities[i] = R_WHITE;
      else
	intensities[i] = R_OTHER;
    }  
}


void Robot::colourThreshold(unsigned int min, unsigned int max, unsigned int avg, unsigned int intensities[])
{
  // intensity thresholds for black and white
  // if i < black it is black.  if i > white it is white.
  unsigned int black, white; 
  //  int range;
  //  range = avg; // kill warning (bad programming)
  //  range = (max - min) / 3;
  
  //  black = min + range;
  //  white = max - range;

    black = min + ((avg - min)/2);
    white = max - ((max - avg)/3);

  for(uint i = 0; i < features[0]->getSize(); ++i)
    {
      if(intensities[i] < black)
	intensities[i] = R_BLACK;
      else if(intensities[i] > white)
	intensities[i] = R_WHITE;
      else
	intensities[i] = R_OTHER;
#ifdef XXDEBUG
      std::cout << intensities[i];
    }
   std::cout << "\n";
#else
    }
#endif

}

#endif


void Robot::interpolateStrip(double wxc, double wyc, FeatureCircle & feature, 
			     unsigned int intensities[], unsigned int & min, 
			     unsigned int & max, unsigned int & avg, FrameBuffer * inFrame)
{
  RawPixel pixel;
  double wx, wy;            // world x and y
  min = 255;
  max = 0;
  avg = 0;
  // std::cout << "interpolating feature " << i << "\n";
  // fill the feature circle
  // with interpolated pixels
  for(unsigned int j = 0; j < feature.getSize(); j++)
    {
      wx = wxc + feature[j].column;
      wy = wyc + feature[j].row;
      
      // 	std::cout << "Centre: (" << wyc << "," << wxc << ")\n";
      // 	std::cout << "Offset: (" << wy << "," << wx << ")\n";
      Trackable::vi2d->interpolatePixel(inFrame, wx, wy, 0.0, &pixel);
      
      intensities[j] = pixel.intIntensity();
      min = min > intensities[j] ? intensities[j] : min;
      max = max < intensities[j] ? intensities[j] : max;
      avg += intensities[j];
      hist.increment(intensities[j]);
    }
  avg = avg / feature.getSize();
}

bool Robot::identifyRobot(unsigned int colours[], int cellColours[6], double & theta, double & wx, double & wy)
{
  int j;
  double centre, centreB; // the centres of the two black regions
  bool left;
  int len = NUM_OF_P;
  double length = (double)NUM_OF_P;
  int thetaIdx;
  int b1start, b2start, b1end, b2end;
  int blackCount;
  int cellLen;

  j = -1;
  //  std::cout << "ID robot\n";
  // Skip the first black region.
  // this is necessary to skip black regions that overlap the edges.
  while(j < (len - 1) && !(colours[++j] != R_BLACK && colours[j+1] != R_BLACK));  
  //  std::cout << "skipped first black region, j=" << j << "\n";

  if(j == len - 1)
    return false; // no black regions == fail  

  // find the next (first unbroken) black region 
  while(j < (len - 1) && !(colours[++j] == R_BLACK && colours[j+1] == R_BLACK));
  //  std::cout << "first unbroken black region, j=" << j << "\n";

  if(j == len - 1)
    return false;
  
  // get the centre of the black region
  b1start = j;
  while(j < (len - 1) && (colours[++j] == R_BLACK || colours[j+1] == R_BLACK ));
  b1end = j;
  centre = (double)(b1start + b1end) / 2.0;
  //  std::cout << "centreA = " << centre << "\n";
  if(b1end - b1start < CELL_LENGTH / 2)
    {
      //      std::cout << "first black region too short: " << b1end - b1start << "\n";
      return false;
    }

  j = b1end + CELL_LENGTH; // move j beyond the border of the black region.
  // find the start of the second black region.
  while(!(colours[(++j)%len] == R_BLACK && colours[(j+1)%len] == R_BLACK));
  //  std::cout << "startB = " << j << "\n";
  // find the centre of the second black region
  b2start = j;
  while(colours[(++j)%len] == R_BLACK || colours[(j+1)%len] == R_BLACK );
  b2end = j;
  //  std::cout << "endB = " << b2end << "\n";
  centreB = (b2start + b2end) / 2;
  //  std::cout << "centreB = " << centreB << "\n";

  if(b2end - b2start < CELL_LENGTH / 2)
    {
      //      std::cout << "second black region too short: " << b2end - b2start << "\n";
      return false;
    }

  if( (b2start - b1end) < (CELL_LENGTH + 2) || 
      (b2start - b1end) > (CELL_LENGTH * 6) || 
      (b1start + len - b2end) > (CELL_LENGTH * 6) ||
      (b1start + len - b2end) < (CELL_LENGTH + 2) )
    {
      //       std::cout << "spaces between black regions do not make sense.\n";
      //       std::cout << "B2-B1: " << b2start - b1end << "\n";
      //       std::cout << "B1-B2: " << (b1start + len - b2end) << "\n";


      return false;
    }
//    else
//      {
//        std::cout << "distances OK:\n";
//        std::cout << "B2-B1: " << b2start - b1end << "\n";
//        std::cout << "B1-B2: " << (b1start + len - b2end) << "\n";
      
//      }

  left = (b2start - b1end) > (b1start + len - b2end);
  //  std::cout << "centreB = " << centreB << "\n";

  // determine theta
  if(left)
    {
      centre += length;
      swap(b1start, b2start);
      swap(b1end, b2end);
      b2start += len;
      b2end += len;
      //      std::cout << "left    ";
     }
//     else
//       std::cout << "right    ";

//  std::cout << "b1: " << b1start << "," << b1end << "    ";
//  std::cout << "b2: " << b2start << "," << b2end << "    ";
  
//  std::cout << "C1, C2: " << centre << "," << centreB << "  avg: " << ((centre + centreB)/2.0) << "\n";

  theta = ((centre + centreB)/2.0);
  while(theta > length)
    theta -= length;
  //  std::cout << "theta between 0 and " << length << ": " << theta << "\n";
  thetaIdx = (int)rint(theta);

  if(theta > (length / 2.0))
    {
      theta = (-length + theta) * RAD_P_P;
    }
  else
    {
      theta = theta * RAD_P_P;
    }
  
  //    std::cout << "theta = " << theta << "    ";
  
  // process the first two cells based on the end of the 
  // first black cell and the start of the second.
  cellLen = (b2start - b1end) / 2;
  //  std::cout << "first cell lengths: " << cellLen << "\n";
  
  j = b1end;
  blackCount = 0;

  for(int i = 0; i < NO_OF_C_CELLS; i++)
    {

      for(int l = 0; l < cellLen; ++l)
	{
	  if(colours[j++ % len] == R_OTHER)
	    cellColours[i]++;
	  else if(colours[j % len] == R_WHITE)
	    cellColours[i]--;
	  else
	    blackCount++;
	}

      if(i==1)
	{
	  j = b2end + 2;
	  cellLen = ((b1start + len) - b2end) / 4;
	  //	  std::cout << "second cell lengths: " << cellLen << "\n";
	}
    }
  
  if(blackCount > CELL_LENGTH)
    {
      //      std::cout << "Too much noise in the signal.  Not confident this is a robot\n";
      return false;
    }

    adjustCentre(b1start, b1end, wx, wy);
    adjustCentre(b2start, b2end, wx, wy);

  // bot found and identified.
  return true;
    
}

void Robot::adjustCentre(int start, int end, double & wx, double & wy)
{
  int centre, size;
  double angle;
  int length = NUM_OF_P;
  centre = (start + end) / 2;
  size = end - start;
  double adjustment;

  if(size < CELL_LENGTH - 3)
    {
      // the cell is too small, move the centre towards it.
      angle = (*features[0])[(centre) % length].theta;
      //      adjustment = (CELL_LENGTH - 3) - size;
      adjustment = 1;
    }
  else if(size > CELL_LENGTH + 3)
    {
      // the cell is too big, move the centre away from it.
      angle = (*features[0])[(centre + (length / 2) ) % length].theta;
      // adjustment = (size - CELL_LENGTH);
      adjustment = 1;
    } 
  wx += cos(angle) * adjustment;
  wy += sin(angle) * adjustment;      

}

std::string Robot::getIntensityStripOutput()
{
  std::ostringstream os;
  for(uint j = 0; j < features[0]->getSize(); j++)
    {
      os << medianIntensities[j];
    }
  os << "\n";
  
  return os.str();
    
}

void Robot::addMeToScratchpad(FrameBuffer * inFrame)
{
  FrameBuffer * scratch = Trackable::scratchpad();
  if(scratch && inFrame)
    {
      //      std::cout << "addMeToScratchpad()\n";
     // assume the scratchpad is square.
      double radius = Trackable::getRadius();
      double size = (double) scratch->height;
      // capture the robot and some surrounding area
      double pixelDim = (radius * 3.0) / size;
      double offset = -(radius * 1.5);
      double wx,wy;
      double cx,cy; // the centre of the robot.
      RawPixel p;

      cx = Trackable::getX();
      cy = Trackable::getY();
       
      //      std::cout << "bot centre: " << cy << "," << cx << "\n";
      FrameBufferIterator fbit(scratch);
      wy = cy + offset;
      for(uint row = 0; row < scratch->height; ++row)
	{
	  wx = cx + offset;
	  fbit.goPosition(row,0);
	  for(uint col = 0; col < scratch->width; ++col)
	    {
	      // std::cout << row << "," << col << "-->" << wy << "," << wx << "\n";
	      Trackable::vi2d->interpolatePixel( inFrame, wx, wy, 0.0, &p);
	      fbit.setPixel(p);
	      wx += pixelDim;
	      fbit.goRight();
	    }
	  wy += pixelDim;
	}
    }


}

struct RobotStruct{
  RobotStruct(  double ix, double iy, double itheta, double isubtype)
  {
    x = ix;
    y = iy;
    theta = itheta;
    subtype = isubtype;
  }
  double x;
  double y;
  double theta;
  double subtype;
};



// This code sucks... I'm sorry.
void Robot::breakUpBigRegion(FrameBuffer * inFrame, FrameBufferWorld * worldFrame, std::vector<Robot *> & notFound, RegionList & regionList, unsigned int regionIdx)
{
  //  std::cout << "breakUpBigRegion() numNotFound: " << notFound.size() << "  Region: " << regionList[regionIdx] << "\n";
  unsigned int pixelRadius;
  int searchRadius;
  unsigned int searchRow;
  unsigned int pixelDiameter;
  double wx,wy, theta;
  int foundSubtype;
  bool found;

  RunRegion & region = regionList[regionIdx];
  // make sure the region is big enough to hold two robots.
  // use the first robot as the prototypical robot.
  if(notFound.size() > 0 && region.numPixels > notFound[0]->minPixels  )
    {
      //  std::cout << "Region is an appropriate size\n";
      // make sure the region is at least one diameter wide.
      // The 0.8 is just to give some breathing room
      pixelRadius = (unsigned int)ceil(notFound[0]->getRadius()/worldFrame->mmPerPix);
      pixelDiameter = (pixelRadius * 16)/10;
      //   std::cout << "pixDiam: " << pixelDiameter << ", pixelRadius: " << pixelRadius << "Region width: " << (region.xMax - region.xMin) << "\n";
      if( (region.xMax - region.xMin) >= pixelDiameter)
	{
	  //  std::cout << "Region has appropriate width\n";
	  // search the runs one pixelradius down from the top.
	  searchRow = region.yMin + pixelRadius;
	  searchRadius = pixelDiameter / 2;
	  //std::cout << "pixDiam: " << pixelDiameter << ", searchRow: " << searchRow << ", searchRadius: " << searchRadius << ", pixelRadius: " << pixelRadius << "\n";

	  std::vector<Run> runList(regionList.getRegionRunsOnRow(searchRow, regionIdx));
	  if(runList.size() > 0)
	    {
	      //  std::cout << "Got runs back: " << runList.size() << "\n";
	      found = false;
	      for(uint i = 0; !found && i < runList.size(); ++i)
		{
		  
		  //  std::cout << "Searching run of length: " << runList[i].length << "\n";
		  for(int j = runList[i].col + searchRadius; !found && j <= (((int)runList[i].colMax()) - searchRadius); ++j)
		    {
		      //  std::cout << "searching row: " << searchRow << "  col: " << j << "\n";
		      worldFrame->setPixelAt( searchRow, j, RawPixel(255,0,0) );
		      worldFrame->bufferToWorldCoordinates(j,searchRow,&wx,&wy);
		      if(notFound[0]->findRobot(wx,wy,inFrame,&foundSubtype,&theta))
			{
			  found = true;
// 			  std::cout << "Found a robot like this! (" << wx << "," << wy << "," << theta << ") subtype: " << foundSubtype << "\n";
// 			  std::cout << "Pixel (r,c): (" << searchRow << "," << j << ")\n";
 
			}
		    }
		}
	      
	      if(found)
		{
		  // search the robot list for a matching subtype
		  found = false;
		  for(uint i = 0; !found && i < notFound.size(); ++i)
		    {
		      if(notFound[i]->getSubtype() == foundSubtype)
			{
			  found = true;
			  notFound[i]->setPosition(wx, wy, theta, inFrame->t);
			}
		    }
		  
		}
	    }
	 
	}

    }

}

