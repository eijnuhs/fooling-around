#include "visitedmap.h"
#include <iostream>
#include "framebuffer.h"


VisitedMap::VisitedMap(unsigned int init_height, unsigned int init_width):
  map(0),
  height(init_height),
  width(init_width)
{
  initMap(); 
  
}
VisitedMap::VisitedMap(FrameBuffer * frame) :
  map(0),
  height(frame->height),
  width(frame->width)
{
  initMap();
}

VisitedMap::VisitedMap(FrameBuffer * frame, list< sPoint > & scanlines) :
  map(0),
  height(frame->height),
  width(frame->width)
{
  initialize(frame, scanlines);
}

void VisitedMap::initMap(bool initialValue)
{
  if(width != 0 && height != 0)
    {
      if(map)
	{
	  delete [] map;
	}
      map = new bool[width * height];
      memset (map, initialValue, sizeof (bool) *width*height);
    }
}


void VisitedMap::copyMap(const VisitedMap & rhs)
{
  delete map;
  height = rhs.height;
  width = rhs.width;
  if(height == 0 || width == 0)
    {
      map = 0;
    }
  else
    {
      map = new bool[width*height];
      for(unsigned int i = 0; i < width; ++i)
	{
	  for(unsigned int j = 0; j < height; ++j)
	    {
	      getItem(i,j) = rhs.isVisited(i,j);
	    }
	}
    }
}

VisitedMap::VisitedMap() :
  height(0),
  width(0)
{
  map = 0;
}
VisitedMap::~VisitedMap()
{
  delete map;
}
VisitedMap::VisitedMap(const VisitedMap & rhs)
{
  copyMap(rhs);
}


VisitedMap & VisitedMap::operator= (const VisitedMap & rhs)
{
  copyMap(rhs);
  return *this;
}


void VisitedMap::setVisited(unsigned int x, unsigned int y, bool visited)
{
  if(x < width &&  y < height)
    {
      getItem(x,y) = visited;
    }
  else
    {
      std::cerr << "VisitedMap::setVisited attempted to set out of map range\n";
      std::cerr << "Map size: (" << width << "," << height << ")\n";
      std::cerr << "Attempted to set: (" << x << "," << y << ")\n";
    }
}

bool VisitedMap::isVisited(unsigned int x, unsigned int y) const
{
  if( x < width &&  y < height)
    {
      return map[(y*width) + x];
    }
  else
    {
      std::cerr << "VisitedMap::isVisited attempted to query out of map range\n";
      std::cerr << "Map size: (" << width << "," << height << ")\n";
      std::cerr << "Attempted to query: (" << x << "," << y << ")\n";
      std::cerr << "Returning true\n";
      return true;
    }

}


bool & VisitedMap::getItem(unsigned int x, unsigned int y)
{
  return map[y*width + x];
}


void VisitedMap::initialize(FrameBuffer * frame)
{
  height = frame->height;
  width = frame->width;

  initMap(false);
}
void VisitedMap::initialize(FrameBuffer * frame, list< sPoint > & scanlines)
{
  height = frame->height;
  width = frame->width;
  initMap(true);
  int xMin, xMax, y;
  int lastXmax = scanlines.front().xMax;
  for (list < struct sPoint >::const_iterator it = scanlines.begin (); it != scanlines.end (); ++it)
    {
      xMin = (*it).xMin;
      xMax = (*it).xMax;
      if(xMin == xMax && lastXmax > xMin)
	{
	  xMax = lastXmax;
	}
      y =    (*it).y;
      // cout << "Processing scanline: (" << xMin << "->" << xMax << "," << y << ")\n";
      memset(&getItem(xMin,y), false, sizeof(bool) * (xMax - xMin));
      lastXmax = xMax;
    }

}
