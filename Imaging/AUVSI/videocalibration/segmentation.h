// $Id: segmentation.h,v 1.1.1.1.2.17 2005/02/05 17:59:28 cvs Exp $
// Jacky Baltes <jacky@cs.umanitoba.ca> Mon May 17 13:06:17 CDT 2004
//

#ifndef _SEGMENTATION_H_
#define _SEGMENTATION_H_

enum SEGMENTATION_MODES{
  SM_SEGMENTATION = 0,
  SM_MOTION,
  SM_SCANLINES,
  SM_SEG_AND_MOTION
};

#include "run.h"
#include <sys/time.h>
#include "framebufferrgb32.h"
#include "framebufferworld.h"
#include "framebufferworldmotion.h"
#include <qobject.h>
#include <list>
#include "region.h"
#include <qmutex.h>
#include "imageprocessing.h"
#include <string>

// Forward declarations
class VideoInterpreter2D;
class FrameBuffer;
class SegmentationConfig;

/* class SegmentationStatistics  */
/* { */
/*  public: */
/*   int numScanLines; */
/*   int numFilteredRegions; */
/*   int numSmallRegions; */

/*   double averageLength; */
/*   double worldAverageLength; */
  
/* }; */

class Segmentation : public QObject
{
  Q_OBJECT

 public:
  Segmentation( VideoInterpreter2D * vi2d, SegmentationConfig * scfg );
  
  // processing methods
  void segmentFrame( FrameBuffer * frame );
  void interpolateFrame( FrameBuffer * inFrame );
  void addRegionToWorldFrame(RunRegion & r, FrameBuffer * inFrame); 
  void updateWVFrame();
  void updateMotionFrame(std::vector<Run> & unused, int fieldNo);
  // deprecated processing methods
  // int attachScanLine( ScanLine & s, double threshold );
  // int addRegion( SegmentationRegion * r );
  // void growRegions( void );
  // void segmentWorldFrame( FrameBuffer * frame, FrameBuffer * outFrame );
  // void segmentWithMotion( FrameBuffer * frame, FrameBuffer * outFrame );
  // void detectMotion(FrameBuffer * frame, FrameBuffer * outFrame, RawPixel motionPixel = RawPixel(0,0,255) );
  // void createWorldFrame( FrameBuffer const * frame, FrameBufferWorld const * regionFrame, FrameBufferWorld * outFrame );
  //  void runLengthEncode(FrameBuffer * frame);
  //  void runRegionMerge();
  //  void newRunRegion(unsigned int runIdx);


  // Mutators:
  void setMode(int mode);
  void setThreshold( unsigned int threshold );     // motion threshold
  void setIntThreshold ( unsigned int threshold ); // intensity threshold

  int getMode();
  int getThreshold();
  int getIntThreshold();
  void saveBackground();
 
  // deprecated mutators
  //  bool setBlurring( bool flag );
  //  bool setFilterRegions( bool flag );
/*   void setMMPerPixel(unsigned int mm); */
/*   void setMaxLength(unsigned int maxL); */
/*   void setMinLength(unsigned int minL); */
//  SegmentationStatistics stats;

 signals:
  void newFrame( FrameBuffer * frame );
  void newSegmentedFrame( FrameBuffer * frame );
  void newInterpolatedFrame( FrameBuffer * frame);
  void newBackgroundFrame( FrameBuffer * frame );
 public:
  VideoInterpreter2D * vi2d;

  double factor;
  // moved to framebufferworldmotion
  //  unsigned int threshold;
  //  unsigned int intThreshold;
  bool blurring;
  bool filterRegions;
  int mode;
  FrameBufferWorld segmentedFrame;
  FrameBufferWorld ipFrame;
  FrameBufferWorld wvFrame;
  FrameBufferWorldMotion motionFrameTop;
  FrameBufferWorldMotion motionFrameBottom;
  QMutex mutex;
  RegionList regionList;
  std::string backFilePrefix;
  struct timeval timestamp;
  // deprecated
  //  int mmPerPixel;
  //  RunList runList;
  //  std::vector<int> parent;
 private:
  friend class SegmentationViewer;
  unsigned int mmppix, mmppixwv;
};


#endif


