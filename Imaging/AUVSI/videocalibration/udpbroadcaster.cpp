/**
 * <p>Title: UDPBroadcaster</p>
 * <p>Description: UDPBroadcaster class</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: Xiao-Wen Terry Liu</p>
 * @author Xiao-Wen Terry Liu
 * @version 1.02
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <string.h> /* memset() */
//#include <sys/time.h> /* select() */

#include <fcntl.h>
#include <iostream>
#include "udpbroadcaster.h"

#define MAX_UDP_MSG_LENGTH 1024

using namespace std;

/**
 * Initialize the  UDPBroadcaster object
 */
UDPBroadcaster::UDPBroadcaster( unsigned int servPort, char * servIP ) 
{
  int rc;                         //socket descripter, response code
  int broadcastPermission = 1;        //broadcast permission
  
  //Note: constant in UDPBroadcaster.h
  
  //Create the datagram/UDP socket
  sd = socket(AF_INET,SOCK_DGRAM, IPPROTO_UDP);

  if( sd < 0) 
    {
      cerr << "Error: UDPBroadcaster: Cannot create a socket." << endl;
    }//end if
  
  //Allow for broadcasting capabilities
  // 1 = broadcast on
  rc = setsockopt(sd, SOL_SOCKET, SO_BROADCAST, (void *) &broadcastPermission, sizeof(broadcastPermission));
  if (rc < 0) 
    {
      cerr << "Error: UDPBroadcaster: Cannot set to allow broadcast." << endl;
    }//end if
  
  //Make it non blocking
  rc = fcntl(sd, F_SETFL, O_NONBLOCK | FASYNC);
  if (rc < 0) 
    {
      cerr << "Error: UDPBroadcaster: Cannot set to non-blocking." << endl;
    }//end if
  this->servIP = servIP;
  setServPort( servPort );
}//end constructor


UDPBroadcaster::~UDPBroadcaster( )
{
  if ( sd > 0 )
    {
      close(sd);              //close the socket
    }
}

/**
 * Sets the server port
 * @param newPort the port to set it to
 */
void UDPBroadcaster::setServPort(unsigned int servPort) {

  this->servPort = servPort;

  //Create the struct to the remote server
  memset(&remoteServAddr, 0, sizeof(remoteServAddr));
  remoteServAddr.sin_family = AF_INET;
  remoteServAddr.sin_addr.s_addr = inet_addr(servIP);
  remoteServAddr.sin_port = htons(this->servPort);    
}//end setServPort

/**
 * Returns the server port
 */
unsigned int UDPBroadcaster::getServPort() {
    return servPort;
}//end setServPort


/**
 * Prints the server port
 */
void UDPBroadcaster::printServPort() {
    cout << "Server port: " << servPort << endl;
}//end printServPort


/**
 * Sets the IP to transmit to
 * @param new_ip_addr The IP address to transmit to
 */
void UDPBroadcaster::setServIP(char* new_ip_addr) {
    servIP = new_ip_addr;
}//end setServIP()


/**
 * Return the IP address to transmit to
 */
char* UDPBroadcaster::getServIP() {
    return servIP;
}//end getServIP()

/**
 * Sends the data if message length > 0
 * @param   incoming_msg    The message to send
 * @return  int returns 0 if everything works ok, >0 if not
 */
int UDPBroadcaster::broadcast( const char * incoming_msg, bool hide_output ) 
{
  int rc;
  int msgLength;                      //the message length
  
  //copy the message
  msgLength = strlen( incoming_msg );

  if ( ! hide_output )
    {
      std::cout << incoming_msg << "\n";
    }
  //Send the data
  rc = sendto(sd, incoming_msg, msgLength, 0, (struct sockaddr *) &remoteServAddr, sizeof(remoteServAddr));
  if(rc<0) {
    cerr << "Error: UDPBroadcaster: Cannot send data." << endl;
    close(sd);          //close the socket
    return 1;           //exit function with error
  }//end if
  
  return 0;
}//end broadcast()
