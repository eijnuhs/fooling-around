/*
 * $Id: v4ldevice.cpp,v 1.1.1.1 2005/04/25 03:02:45 cvs Exp $
 */

using namespace std;

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <linux/videodev.h>
#include <string>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

#include "v4lDevice.h"
#include "framebuffer.h"

void
V4LDevice::printCapabilities (FILE * fp, struct video_capability cap)
{

  fprintf (fp, "Device:       %s\n", cap.name);
  fprintf (fp, "Capabilities:");
  if (cap.type & VID_TYPE_CAPTURE)
    fprintf (fp, " capture via read();");
  if (cap.type & VID_TYPE_TUNER)
    fprintf (fp, " tuner;");
  if (cap.type & VID_TYPE_OVERLAY)
    fprintf (fp, " overlay;");
  if (cap.type & VID_TYPE_CHROMAKEY)
    fprintf (fp, " chromakey;");
  if (cap.type & VID_TYPE_CLIPPING)
    fprintf (fp, " clipping;");
  if (cap.type & VID_TYPE_FRAMERAM)
    fprintf (fp, " frameram,");
  if (cap.type & VID_TYPE_SCALES)
    fprintf (fp, " scales,");
  if (cap.type & VID_TYPE_MONOCHROME)
    fprintf (fp, " monochrome,");
  if (cap.type & VID_TYPE_SUBCAPTURE)
    fprintf (fp, " sub capture,");
  printf ("\n");

  fprintf (stderr, "Channels: %d\n", cap.channels);
  fprintf (stderr, "Audio: %d\n", cap.audios);
  fprintf (stderr, "Max dimensions: %d x %d\n", cap.maxwidth, cap.maxheight);
  fprintf (stderr, "Min dimensions: %d x %d\n", cap.minwidth, cap.minheight);
}


V4LDevice::V4LDevice (char *devname)
{
  strcpy (this->devname, devname);
}

int
V4LDevice::init (int fps,
		 int brightness,
		 int colorSat,
		 int contrast,
		 int hue,
		 int gain,
		 char *input,
		 int field, char *standard, int width, int height, int depth)
{
  struct video_capability cap;
  struct video_channel channel;
  struct video_picture pict;
  int err;

  this->fps = fps;
  this->brightness = brightness;
  this->colorSat = colorSat;
  this->contrast = contrast;
  this->hue = hue;
  this->gain = gain;
  strcpy (this->input, input);
  this->field = field;
  strcpy (this->standard, standard);
  this->width = width;
  this->height = height;
  this->depth = depth;

  if ((field == 1) || (field == 2))
    {
      this->fieldsPerSecond = this->fps;
    }
  else if (field == 3)
    {
      this->fieldsPerSecond = this->fps * 2;
    }
  else if (field == 7)
    {
      this->fieldsPerSecond = this->fps;
    }
  else
    {
      fprintf (stderr, "V4LDevice::V4LDevice invalid field mode %d\n", field);
      exit (1);
    }

  if ((fd = open (devname, O_RDWR)) < 0)
    {
      perror ("open video device:");

    }
  if ((err = ioctl (fd, VIDIOCGCAP, &cap) != 0))
    {
      close (fd);
      return 1;
    }

#ifdef DEBUG
  V4LDevice::printCapabilities (stderr, cap);
#endif

  if (cap.type != VID_TYPE_CAPTURE)
    {
      fprintf (stderr, "Not a capture device.\n");
      exit (1);
    }

  win.clips = NULL;
  win.chromakey = win.flags = win.clipcount = win.x = win.y = 0;
  win.width = width > cap.maxwidth ? cap.maxwidth : width;
  win.height = height > cap.maxheight ? cap.maxheight : height;
  if (ioctl (fd, VIDIOCSWIN, &win) == -1)
    {
      perror ("set capture window");
      exit (1);
    };
  if (ioctl (fd, VIDIOCGWIN, &win) == -1)
    {
      perror ("query capture window");
      exit (1);
    };

#ifdef DEBUG
  V4LDevice::printCaptureWindow (stderr, win);
#endif

  for (int i = 0; i < cap.channels; i++)
    {
      channel.channel = i;
      if (ioctl (fd, VIDIOCGCHAN, &channel) == -1)
	{
	  perror ("V4LDevice::query channel");
	  exit (1);
	}
      printChannel (stderr, channel);
    };

  channel.norm = VIDEO_MODE_NTSC;

  if (0 == strcasecmp(this->standard, "PAL")) {
    channel.norm = VIDEO_MODE_PAL;
  } else if (0 == strcasecmp(this->standard, "NTSC")) {
    channel.norm = VIDEO_MODE_NTSC;
  } else if (0 == strcasecmp(this->standard, "SECAM")) {
    channel.norm = VIDEO_MODE_SECAM;
  }

  if (ioctl (fd, VIDIOCSCHAN, &channel) == -1)
    {
      perror ("V4LDevice::query channel");
      exit (1);
    }
  
  pict.brightness = pict.hue = pict.colour = pict.contrast = 32768;
  pict.whiteness = 0;
  pict.palette = VIDEO_PALETTE_RGB32;
  pict.depth = 32;
  if (ioctl (fd, VIDIOCSPICT, &pict) == -1)
    {
      perror ("set image properties");
      exit (1);
    };
  if (ioctl (fd, VIDIOCGPICT, &pict) == -1)
    {
      perror ("query image properties");
      exit (1);
    }
  V4LDevice::printPicture (stderr, pict);

  if (!(pict.palette & VIDEO_PALETTE_RGB32))
    {
      fprintf (stderr, "rgb32 not supported\n");
      exit (1);
    }

  if (ioctl (fd, VIDIOCGMBUF, &mbuf) == -1)
    {
      perror ("mbuf");
      exit (2);
    }

  printMMap (stderr, mbuf);
  if ((data =
       mmap (NULL, mbuf.size, PROT_READ, MAP_SHARED, fd,
	     getpagesize ())) == (void *) -1)
    {
      perror ("mmap");
      exit (10);
    }
  absFrameNo = 0;
  return 0;
}

void
V4LDevice::printCaptureWindow (FILE * fp, struct video_window win)
{
  fprintf (fp, "Capture window at %d, %d\n", win.x, win.y);
  fprintf (fp, "Dimensions: %d x %d\n", win.width, win.height);
  fprintf (fp, "Chromakey: %#x\n", win.chromakey);
  fprintf (fp, "Flags:");
  if (win.flags & VIDEO_CAPTURE_ODD)
    fprintf (fp, " odd");
  if (win.flags & VIDEO_CAPTURE_EVEN)
    fprintf (fp, " even");
  fprintf (fp, "\n");
  fprintf (fp, "# of clipping rectangles: %d\n", win.clipcount);
};


V4LDevice::~V4LDevice (void)
{
  close (fd);
}

int
V4LDevice::setFrame ( FrameBuffer *frame)
{
  frame->buffer = (unsigned char *) data + mbuf.offsets[0];
  frame->width = win.width;
  frame->height = win.height;
  frame->bytesPerLine = win.width * depth / 8;
  frame->bytesPerPixel = depth / 8;
  frame->absFrameNo = 0;
  frame->fieldNo = 0;
  if (frame->width / frame->height >= 2)
    {
      frame->interlaced = 0;
    }
  else
    {
      frame->interlaced = 1;
    }
  startCapture ();
  nextFrame (frame);
  releaseCurrentBuffer ();
  stopCapture ();
  return 0;
}

void
V4LDevice::printChannel (FILE * fp, struct video_channel channel)
{
  fprintf (fp, "Channel %d:\n", channel.channel);
  fprintf (fp, "Name: %s\n", channel.name);
  fprintf (fp, "Tuners: %d\n", channel.tuners);
  fprintf (fp, "Flags:");
  if (channel.flags & VIDEO_VC_TUNER)
    fprintf (fp, " tuners");
  if (channel.flags & VIDEO_VC_AUDIO)
    fprintf (fp, " audio");
  fprintf (fp, "%s\n", channel.flags ? "" : " nothing");
  fprintf (fp, "Type: %s (%d)\n", channel.type == VIDEO_TYPE_TV ? "tv" :
	   channel.type == VIDEO_TYPE_CAMERA ? "camera" : "unknown",
	   channel.type);
  fprintf (fp, "Norm: %d\n", channel.norm);
};


void
V4LDevice::printPicture (FILE * fp, struct video_picture pict)
{
  fprintf (fp, "Brightness: %d\n", pict.brightness);
  fprintf (fp, "Hue: %d\n", pict.hue);
  fprintf (fp, "Colour: %d\n", pict.colour);
  fprintf (fp, "Contrast: %d\n", pict.contrast);
  fprintf (fp, "Whiteness: %d\n", pict.whiteness);
  fprintf (fp, "Depth: %d\n", pict.depth);
  fprintf (fp, "Format:");
  if (pict.palette & VIDEO_PALETTE_GREY)
    fprintf (fp, " grey");
  if (pict.palette & VIDEO_PALETTE_HI240)
    fprintf (fp, " hi240");
  if (pict.palette & VIDEO_PALETTE_RGB565)
    fprintf (fp, " rgb565");
  if (pict.palette & VIDEO_PALETTE_RGB555)
    fprintf (fp, " rgb555");
  if (pict.palette & VIDEO_PALETTE_RGB24)
    fprintf (fp, " rgb24");
  if (pict.palette & VIDEO_PALETTE_RGB32)
    fprintf (fp, " rgb32");
  if (pict.palette & VIDEO_PALETTE_YUV422)
    fprintf (fp, " rgb422");
  if (pict.palette & VIDEO_PALETTE_YUYV)
    fprintf (fp, " yuyv");
  if (pict.palette & VIDEO_PALETTE_UYVY)
    fprintf (fp, " uyvy");
  if (pict.palette & VIDEO_PALETTE_YUV420)
    fprintf (fp, " yuv420");
  if (pict.palette & VIDEO_PALETTE_YUV411)
    fprintf (fp, " yuv411");
  if (pict.palette & VIDEO_PALETTE_RAW)
    fprintf (fp, " raw");
  if (pict.palette & VIDEO_PALETTE_YUV422P)
    fprintf (fp, " yuv422p");
  if (pict.palette & VIDEO_PALETTE_YUV411P)
    fprintf (fp, " yuv411p");
  fprintf (fp, "\n");
};

void
V4LDevice::printMMap (FILE * fp, struct video_mbuf mbuf)
{
  fprintf (fp, "MMap supports %d frames, mapping %d bytes\n", mbuf.frames,
	   mbuf.size);

}

int
V4LDevice::nextFrame ( FrameBuffer *curFrame)
{

#ifdef XXDEBUG
  printf ("V4LDevice::nextFrame called\n");
#endif

  absFrameNo++;

  map.frame = 0;
  map.height = win.height;
  map.width = win.width;
  map.format = VIDEO_PALETTE_RGB32;
  if (ioctl (fd, VIDIOCMCAPTURE, &map) < 0)
    {
      perror ("set mmap capture size");
      return -1;
    }

  curFrame->buffer = (unsigned char *) data + mbuf.offsets[map.frame];

  curFrame->width = win.width;
  curFrame->height = win.height;
  curFrame->bytesPerPixel = depth / 8;
  curFrame->bytesPerLine = curFrame->width * curFrame->bytesPerPixel;
  curFrame->fieldNo = 0;

  if (curFrame->width / curFrame->height >= 2)
    {
      curFrame->interlaced = 0;
    }
  else
    {
      curFrame->interlaced = 1;
    }
  curFrame->fieldsPerSecond = this->fieldsPerSecond;
  curFrame->absFrameNo = absFrameNo;

#ifdef XXDEBUG
  printf
    ("V4LDevice::nextFrame FrameBuffer Information: data=%p %dx%d, bpp = %d, bpl=%d, fieldNo=%d absFrameNo=%d, interlaced=%d\n",
     curFrame->buffer, curFrame->width, curFrame->height,
     curFrame->bytesPerPixel, curFrame->bytesPerLine, curFrame->fieldNo,
     curFrame->absFrameNo, curFrame->interlaced);
#endif
  return 0;
}

int
V4LDevice::startCapture (void)
{
  return 0;
}

int
V4LDevice::stopCapture (void)
{
  return 0;
}

int
V4LDevice::releaseCurrentBuffer (void)
{
  int frame = map.frame;
  if (ioctl (fd, VIDIOCSYNC, &frame) < 0)
    {
      perror ("sync frame");
      return -1;
    };
  return 0;
}
