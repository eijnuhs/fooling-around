// $Id: videodevice.h,v 1.1.1.1 2005/04/25 03:02:45 cvs Exp $
// Superclass to encapsulate different video capturing devices.
// In particular V4L and V4L2 devices. FreeBSD maybe later
//

#ifndef _VIDEODEVICE_H_
#define _VIDEODEVICE_H_

#include <string>
#include <boost/shared_array.hpp>
#include "weakimage.hpp"

class VideoDevice
{
 public:
  VideoDevice( std::string driver, std::string devname, std::string input, std::string standard, int fps, int width, int height, ImageFormats::ImageFormat format, bool interlaced);
  virtual ~VideoDevice();
  
  virtual bool getError( void ) const;
  virtual int startCapture( void ) = 0;
  virtual int stopCapture( void ) = 0;
  virtual int getWidth() const;
  virtual int getHeight() const; 
  virtual int releaseCurrentBuffer() = 0;
  virtual WeakImage & nextFrame() = 0;
  
 protected:
  std::string driver;
  std::string devname;
  std::string input;
  std::string standard;
  int fps;
  int width;
  int height;
  bool error;
  ImageFormats::ImageFormat format;
  bool interlaced;
};


#endif
