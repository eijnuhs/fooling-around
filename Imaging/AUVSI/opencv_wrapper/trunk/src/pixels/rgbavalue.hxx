/************************************************************************/
/*                                                                      */
/*               Copyright 1998-2002 by Ullrich Koethe                  */
/*       Cognitive Systems Group, University of Hamburg, Germany        */
/*                                                                      */
/*    This file is part of the VIGRA computer vision library.           */
/*    ( Version 1.3.2, Jan 27 2005 )                                    */
/*    You may use, modify, and distribute this software according       */
/*    to the terms stated in the LICENSE file included in               */
/*    the VIGRA distribution.                                           */
/*                                                                      */
/*    The VIGRA Website is                                              */
/*        http://kogs-www.informatik.uni-hamburg.de/~koethe/vigra/      */
/*    Please direct questions, bug reports, and contributions to        */
/*        koethe@informatik.uni-hamburg.de                              */
/*                                                                      */
/*  THIS SOFTWARE IS PROVIDED AS IS AND WITHOUT ANY EXPRESS OR          */
/*  IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED      */
/*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. */
/*                                                                      */
/************************************************************************/


#ifndef VIGRA_RGBAVALUE_HXX
#define VIGRA_RGBAVALUE_HXX

#include <cmath>    // abs(double)
#include <cstdlib>  // abs(int)
#include "vigra/config.hxx"
#include "vigra/numerictraits.hxx"
#include "vigra/accessor.hxx"
#include "vigra/tinyvector.hxx"

namespace vigra {

/********************************************************/
/*                                                      */
/*                      RGBAValue                        */
/*                                                      */
/********************************************************/

/** \brief Class for a single RGBA value.

    This class contains four values (of the specified type) that
    represent red, green, and blue color channels as well as the alpha
    channel. There are four possibilities to access these values:
    accessor functions (\ref red(), \ref green(), \ref blue(), \ref
    alpha()), index operator (operator[](dx), where 0 is alpha, 1 is
    red, 2 is green and 3 is blue) and iterator (STL-compatible random access
    iterator that references the four channels in turn). The latter two
    methods, together with the necessary embedded typedefs, ensure
    compatibility of a RGBAValue with a STL vector.

    \ref RGBAValueOperators "Arithmetic operations" are defined as component-wise applications of these
    operations. Addition, subtraction, and multiplication of two RGBAValues
    (+=, -=, *=, +, -, *, unary -), multiplication and division of an
    RGBAValue with a double, and NumericTraits/PromoteTraits are defined,
    so that RGBAValue fulfills the requirements of a \ref LinearAlgebra.

    A number of \ref RGBAValueAccessors "accessors" are provided
    that support access to RGBAValues as a whole, to a selected
    color component, or to the luminance value.

    <b>\#include</b> "<a href="rgbvalue_8hxx-source.html">vigra/rgbvalue.hxx</a>"<br>
    Namespace: vigra
*/
template <class VALUETYPE>
class RGBAValue
: public TinyVector<VALUETYPE, 4>
{
    typedef TinyVector<VALUETYPE, 4> Base;
  public:
        /** STL-compatible definition of valuetype
        */
    typedef VALUETYPE value_type;
        /** STL-compatible definition of iterator
        */
    typedef typename TinyVector<VALUETYPE, 4>::iterator iterator;
        /** STL-compatible definition of const iterator
        */
    typedef typename TinyVector<VALUETYPE, 4>::const_iterator const_iterator;

        /** Construct from explicit color values
	 */
  RGBAValue(value_type red, value_type green, value_type blue, value_type alpha = vigra::NumericTraits<value_type>::zero())
    : Base(alpha, red, green, blue)
     {}

        /** Construct gray value
        */
    RGBAValue(value_type gray)
    : Base(gray, gray, gray, gray)
    {}

        /** Construct from another sequence (must have length 4!)
        */
    template <class Iterator>
    RGBAValue(Iterator i, Iterator end)
    : Base(i[0], i[1], i[2], i[4])
    {}

        /** Default constructor (sets all components to 0)
        */
    RGBAValue()
    : Base(0, 0, 0, 0)
    {}

#if !defined(TEMPLATE_COPY_CONSTRUCTOR_BUG)

    RGBAValue(RGBAValue const & r)
    : Base(r)
    {}

    RGBAValue & operator=(RGBAValue const & r)
    {
        Base::operator=(r);
        return *this;
    }

#endif // TEMPLATE_COPY_CONSTRUCTOR_BUG


        /** Copy constructor.
        */
    template <class U>
    RGBAValue(RGBAValue<U> const & r)
    : Base(r)
    {}

        /** Copy assignment.
        */
    template <class U>
    RGBAValue & operator=(RGBAValue<U> const & r)
    {
        Base::operator=(r);
        return *this;
    }

        /** construct from TinyVector
        */
    RGBAValue(TinyVector<value_type, 4> const & r)
    : Base(r)
    {}

        /** assign TinyVector.
        */
    RGBAValue & operator=(TinyVector<value_type, 4> const & r)
    {
        Base::operator=(r);
        return *this;
    }

        /** Unary negation (construct RGBAValue with negative values)
        */
    RGBAValue operator-() const
    {
        return RGBAValue(-red(), -green(), -blue(), -alpha());
    }

        /** Access red component.
        */
  value_type & red() { return (*this)[1]; }

        /** Access green component.
        */
    value_type & green() { return (*this)[2]; }

        /** Access blue component.
        */
    value_type & blue() { return (*this)[3]; }

        /** Access alpha component.
        */
    value_type & alpha() { return (*this)[0]; }

        /** Get red component.
        */
    value_type const & red() const { return (*this)[1]; }

        /** Get green component.
        */
    value_type const & green() const { return (*this)[2]; }

        /** Get blue component.
        */
    value_type const & blue() const { return (*this)[3]; }

        /** Get alpha component.
        */
    value_type const & alpha() const { return (*this)[0]; }

        /** Calculate luminance.
        */
    value_type luminance() const {
         return detail::RequiresExplicitCast<value_type>::cast(0.3*red() + 0.59*green() + 0.11*blue()); }

        /** Calculate magnitude.
        */
    typename NumericTraits<VALUETYPE>::RealPromote
    magnitude() const {
         return VIGRA_CSTD::sqrt(
            (typename NumericTraits<VALUETYPE>::RealPromote)squaredMagnitude());
    }

        /** Calculate squared magnitude.
        */
    typename NumericTraits<VALUETYPE>::Promote
    squaredMagnitude() const {
         return red()*red() + green()*green() + blue()*blue();
    }

        /** Set red component. The type <TT>V</TT> of the passed
            in <TT>value</TT> is automatically converted to <TT>VALUETYPE</TT>.
        */
    template <class V>
    void setRed(V value) { (*this)[1] = detail::RequiresExplicitCast<value_type>::cast(value); }

        /** Set green component.The type <TT>V</TT> of the passed
            in <TT>value</TT> is automatically converted to <TT>VALUETYPE</TT>.
        */
    template <class V>
    void setGreen(V value) { (*this)[2] = detail::RequiresExplicitCast<value_type>::cast(value); }

        /** Set blue component.The type <TT>V</TT> of the passed
            in <TT>value</TT> is automatically converted to <TT>VALUETYPE</TT>.
        */
    template <class V>
    void setBlue(V value) { (*this)[3] = detail::RequiresExplicitCast<value_type>::cast(value); }

        /** Set alpha component.The type <TT>V</TT> of the passed
            in <TT>value</TT> is automatically converted to <TT>VALUETYPE</TT>.
        */
    template <class V>
    void setAlpha(V value) { (*this)[0] = detail::RequiresExplicitCast<value_type>::cast(value); }


    template <class V>
    void setRGB(V r, V g, V b) 
    { 
        (*this)[1] = detail::RequiresExplicitCast<value_type>::cast(r); 
        (*this)[2] = detail::RequiresExplicitCast<value_type>::cast(g); 
        (*this)[3] = detail::RequiresExplicitCast<value_type>::cast(b); 
	(*this)[0] = NumericTraits<VALUETYPE>::zero();
    }

    template <class V>
    void setRGBA(V r, V g, V b, V a) 
    { 
        (*this)[1] = detail::RequiresExplicitCast<value_type>::cast(r); 
        (*this)[2] = detail::RequiresExplicitCast<value_type>::cast(g); 
        (*this)[3] = detail::RequiresExplicitCast<value_type>::cast(b); 
        (*this)[0] = detail::RequiresExplicitCast<value_type>::cast(a); 
    }
};

/********************************************************/
/*                                                      */
/*                     RGBAValue Comparison              */
/*                                                      */
/********************************************************/

/** \addtogroup RGBAValueOperators Functions for RGBAValue

    \brief <b>\#include</b> "<a href="rgbvalue_8hxx-source.html">vigra/rgbvalue.hxx</a>

    These functions fulfill the requirements of a Linear Algebra.
    Return types are determined according to \ref RGBAValueTraits.

    Namespace: vigra
    <p>

 */
//@{
    /// component-wise equal 
template <class V1, class V2>
inline
bool
operator==(RGBAValue<V1> const & l, RGBAValue<V2> const & r)
{
  return (l.red() == r.red()) &&
    (l.green() == r.green())  &&
    (l.blue() == r.blue())    &&
    (l.alpha() == r.alpha());
}

    /// component-wise not equal
template <class V1, class V2>
inline
bool
operator!=(RGBAValue<V1> const & l, RGBAValue<V2> const & r)
{
  return (l.red() != r.red()) ||
    (l.green() != r.green())  ||
    (l.blue() != r.blue())    ||
    (l.alpha() != r.alpha());
}


//@}

/********************************************************/
/*                                                      */
/*                      RGBAValue-Traits                 */
/*                                                      */
/********************************************************/

/** \page RGBAValueTraits Numeric and Promote Traits of RGBAValue
    The numeric and promote traits for RGBAValues follow
    the general specifications for \ref NumericPromotionTraits.
    They are implemented in terms of the traits of the basic types by
    partial template specialization:

    \code

    template <class T>
    struct NumericTraits<RGBAValue<T> >
    {
        typedef RGBAValue<typename NumericTraits<T>::Promote> Promote;
        typedef RGBAValue<typename NumericTraits<T>::RealPromote> RealPromote;

        typedef typename NumericTraits<T>::isIntegral isIntegral;
        typedef VigraFalseType isScalar;

        // etc.
    };

    template <class T1, class T2>
    struct PromoteTraits<RGBAValue<T1>, RGBAValue<T2> >
    {
        typedef RGBAValue<typename PromoteTraits<T1, T2>::Promote> Promote;
    };
    \endcode

    <b>\#include</b> "<a href="rgbvalue_8hxx-source.html">vigra/rgbvalue.hxx</a>"<br>
    Namespace: vigra

*/

#if !defined(NO_PARTIAL_TEMPLATE_SPECIALIZATION)

template <class T>
struct NumericTraits<RGBAValue<T> >
{
    typedef RGBAValue<T> Type;
    typedef RGBAValue<typename NumericTraits<T>::Promote> Promote;
    typedef RGBAValue<typename NumericTraits<T>::RealPromote> RealPromote;
    typedef RGBAValue<typename NumericTraits<T>::ComplexPromote> ComplexPromote;
    typedef T ValueType; 
    
    typedef typename NumericTraits<T>::isIntegral isIntegral; 
    typedef VigraFalseType isScalar; 
    typedef VigraFalseType isOrdered;
    typedef VigraFalseType isComplex; 

    static RGBAValue<T> zero() {
        return RGBAValue<T>(NumericTraits<T>::zero());
    }
    static RGBAValue<T> one() {
        return RGBAValue<T>(NumericTraits<T>::one());
    }
    static RGBAValue<T> nonZero() {
        return RGBAValue<T>(NumericTraits<T>::nonZero());
    }

    static Promote toPromote(RGBAValue<T> const & v) {
        return Promote(v);
    }
    static RealPromote toRealPromote(RGBAValue<T> const & v) {
        return RealPromote(v);
    }
    static RGBAValue<T> fromPromote(Promote const & v) {
        return RGBAValue<T>(NumericTraits<T>::fromPromote(v.red()),
                           NumericTraits<T>::fromPromote(v.green()),
                           NumericTraits<T>::fromPromote(v.blue()));
    }
    static RGBAValue<T> fromRealPromote(RealPromote const & v) {
        return RGBAValue<T>(NumericTraits<T>::fromRealPromote(v.red()),
                           NumericTraits<T>::fromRealPromote(v.green()),
                           NumericTraits<T>::fromRealPromote(v.blue()));
    }
};

template <class T1, class T2>
struct PromoteTraits<RGBAValue<T1>, RGBAValue<T2> >
{
    typedef RGBAValue<typename PromoteTraits<T1, T2>::Promote> Promote;
};

template <class T>
struct PromoteTraits<RGBAValue<T>, double >
{
    typedef RGBAValue<typename NumericTraits<T>::RealPromote> Promote;
};

template <class T>
struct PromoteTraits<double, RGBAValue<T> >
{
    typedef RGBAValue<typename NumericTraits<T>::RealPromote> Promote;
};

#else // NO_PARTIAL_TEMPLATE_SPECIALIZATION

#define RGBAVALUE_NUMTRAITS(T) \
template<>\
struct NumericTraits<RGBAValue<T> >\
{\
    typedef RGBAValue<T> Type; \
    typedef RGBAValue<NumericTraits<T>::Promote> Promote; \
    typedef RGBAValue<NumericTraits<T>::RealPromote> RealPromote; \
    typedef RGBAValue<NumericTraits<T>::ComplexPromote> ComplexPromote; \
    typedef T ValueType; \
    \
    typedef NumericTraits<T>::isIntegral isIntegral; \
    typedef VigraFalseType isScalar; \
    typedef VigraFalseType isOrdered; \
    typedef VigraFalseType isComplex; \
    \
    static RGBAValue<T> zero() { \
        return RGBAValue<T>(NumericTraits<T>::zero()); \
    }\
    static RGBAValue<T> one() { \
        return RGBAValue<T>(NumericTraits<T>::one()); \
    }\
    static RGBAValue<T> nonZero() { \
        return RGBAValue<T>(NumericTraits<T>::nonZero()); \
    }\
    \
    static Promote toPromote(RGBAValue<T> const & v) { \
        return Promote(v); \
    }\
    static RealPromote toRealPromote(RGBAValue<T> const & v) { \
        return RealPromote(v); \
    }\
    static RGBAValue<T> fromPromote(Promote const & v) { \
        RGBAValue<T> res;\
        RGBAValue<T>::iterator d = res.begin();\
        Promote::const_iterator s = v.begin();\
        for(; d != res.end(); ++d, ++s)\
            *d = NumericTraits<T>::fromPromote(*s);\
        return res;\
    }\
    static RGBAValue<T> fromRealPromote(RealPromote const & v) {\
        RGBAValue<T> res;\
        RGBAValue<T>::iterator d = res.begin();\
        RealPromote::const_iterator s = v.begin();\
        for(; d != res.end(); ++d, ++s)\
            *d = NumericTraits<T>::fromRealPromote(*s);\
        return res;\
    }\
};

#define RGBAVALUE_PROMTRAITS1(type1) \
template<> \
struct PromoteTraits<RGBAValue<type1>, RGBAValue<type1> > \
{ \
    typedef RGBAValue<PromoteTraits<type1, type1>::Promote> Promote; \
    static Promote toPromote(RGBAValue<type1> const & v) { \
        return static_cast<Promote>(v); } \
};

#define RGBAVALUE_PROMTRAITS2(type1, type2) \
template<> \
struct PromoteTraits<RGBAValue<type1>, RGBAValue<type2> > \
{ \
    typedef RGBAValue<PromoteTraits<type1, type2>::Promote> Promote; \
    static Promote toPromote(RGBAValue<type1> const & v) { \
        return static_cast<Promote>(v); } \
    static Promote toPromote(RGBAValue<type2> const & v) { \
        return static_cast<Promote>(v); } \
};

RGBAVALUE_NUMTRAITS(unsigned char)
RGBAVALUE_NUMTRAITS(int)
RGBAVALUE_NUMTRAITS(float)
RGBAVALUE_NUMTRAITS(double)
RGBAVALUE_PROMTRAITS1(unsigned char)
RGBAVALUE_PROMTRAITS1(int)
RGBAVALUE_PROMTRAITS1(float)
RGBAVALUE_PROMTRAITS1(double)
RGBAVALUE_PROMTRAITS2(float, unsigned char)
RGBAVALUE_PROMTRAITS2(unsigned char, float)
RGBAVALUE_PROMTRAITS2(int, unsigned char)
RGBAVALUE_PROMTRAITS2(unsigned char, int)
RGBAVALUE_PROMTRAITS2(int, float)
RGBAVALUE_PROMTRAITS2(float, int)
RGBAVALUE_PROMTRAITS2(double, unsigned char)
RGBAVALUE_PROMTRAITS2(unsigned char, double)
RGBAVALUE_PROMTRAITS2(int, double)
RGBAVALUE_PROMTRAITS2(double, int)
RGBAVALUE_PROMTRAITS2(double, float)
RGBAVALUE_PROMTRAITS2(float, double)

#undef RGBAVALUE_NUMTRAITS
#undef RGBAVALUE_PROMTRAITS1
#undef RGBAVALUE_PROMTRAITS2

#endif // NO_PARTIAL_TEMPLATE_SPECIALIZATION


/********************************************************/
/*                                                      */
/*                      RGBAValue-Arithmetic             */
/*                                                      */
/********************************************************/

/** \addtogroup RGBAValueOperators
 */
//@{
    /// componentwise add-assignment
template <class V1, class V2>
inline
RGBAValue<V1> &
operator+=(RGBAValue<V1> & l, RGBAValue<V2> const & r)
{
    l.red() += r.red();
    l.green() += r.green();
    l.blue() += r.blue();
    l.alpha() += r.alpha();
    return l;
}

    /// componentwise subtract-assignment
template <class V1, class V2>
inline
RGBAValue<V1> &
operator-=(RGBAValue<V1> & l, RGBAValue<V2> const & r)
{
    l.red() -= r.red();
    l.green() -= r.green();
    l.blue() -= r.blue();
    l.alpha = r.alpha();
    return l;
}

    /// componentwise multiply-assignment
template <class V1, class V2>
inline
RGBAValue<V1> &
operator*=(RGBAValue<V1> & l, RGBAValue<V2> const & r)
{
    l.red() *= r.red();
    l.green() *= r.green();
    l.blue() *= r.blue();
    l.alpha() *= r.alpha();
    return l;
}

    /// componentwise scalar multiply-assignment
template <class V>
inline
RGBAValue<V> &
operator*=(RGBAValue<V> & l, double r)
{
    l.red() *= r;
    l.green() *= r;
    l.blue() *= r;
    l.alpha() *= r;
    return l;
}

    /// componentwise scalar divide-assignment
template <class V>
inline
RGBAValue<V> &
operator/=(RGBAValue<V> & l, double r)
{
    l.red() /= r;
    l.green() /= r;
    l.blue() /= r;
    l.alpha() /= r;
    return l;
}

using VIGRA_CSTD::abs;

    /// component-wise absolute value
template <class T>
inline
RGBAValue<T> abs(RGBAValue<T> const & v) {
    return RGBAValue<T>(abs(v.red()), abs(v.green()),  abs(v.blue()), abs(v.alpha()));
}



    /// component-wise addition
template <class V1, class V2>
inline
typename PromoteTraits<RGBAValue<V1>, RGBAValue<V2> >::Promote
operator+(RGBAValue<V1> const & r1, RGBAValue<V2> const & r2)
{
    typename PromoteTraits<RGBAValue<V1>, RGBAValue<V2> >::Promote res(r1);

    res += r2;

    return res;
}

    /// component-wise subtraction
template <class V1, class V2>
inline
typename PromoteTraits<RGBAValue<V1>, RGBAValue<V2> >::Promote
operator-(RGBAValue<V1> const & r1, RGBAValue<V2> const & r2)
{
    typename PromoteTraits<RGBAValue<V1>, RGBAValue<V2> >::Promote res(r1);

    res -= r2;

    return res;
}

    /// component-wise multiplication
template <class V1, class V2>
inline
typename PromoteTraits<RGBAValue<V1>, RGBAValue<V2> >::Promote
operator*(RGBAValue<V1> const & r1, RGBAValue<V2> const & r2)
{
    typename PromoteTraits<RGBAValue<V1>, RGBAValue<V2> >::Promote res(r1);

    res *= r2;

    return res;
}

    /// component-wise left scalar multiplication
template <class V>
inline
typename NumericTraits<RGBAValue<V> >::RealPromote
operator*(double v, RGBAValue<V> const & r)
{
    typename NumericTraits<RGBAValue<V> >::RealPromote res(r);

    res *= v;

    return res;
}

    /// component-wise right scalar multiplication
template <class V>
inline
typename NumericTraits<RGBAValue<V> >::RealPromote
operator*(RGBAValue<V> const & r, double v)
{
    typename NumericTraits<RGBAValue<V> >::RealPromote res(r);

    res *= v;

    return res;
}

    /// component-wise scalar division
template <class V>
inline
typename NumericTraits<RGBAValue<V> >::RealPromote
operator/(RGBAValue<V> const & r, double v)
{
    typename NumericTraits<RGBAValue<V> >::RealPromote res(r);

    res /= v;

    return res;
}

/// cross product
/// This has no definition for a four component vector but we'll leave it
/// in for the RGB.
template <class V1, class V2>
inline
typename PromoteTraits<RGBAValue<V1>, RGBAValue<V2> >::Promote
cross(RGBAValue<V1> const & r1, RGBAValue<V2> const & r2)
{
    typedef typename PromoteTraits<RGBAValue<V1>, RGBAValue<V2> >::Promote
      Res;
    return  Res(r1.green()*r2.blue() - r1.blue()*r2.green(),
                r1.blue()*r2.red() - r1.red()*r2.blue(),
                r1.red()*r2.green() - r1.green()*r2.red());
}

    /// dot product
template <class V1, class V2>
inline
typename PromoteTraits<V1, V2>::Promote
dot(RGBAValue<V1> const & r1, RGBAValue<V2> const & r2)
{
    return r1.red()*r2.red() + r1.green()*r2.green() + r1.blue()*r2.blue();
}

using VIGRA_CSTD::ceil;

    /** Apply ceil() function to each RGB component.
    */
template <class V>
inline
RGBAValue<V>
ceil(RGBAValue<V> const & r)
{
    return RGBAValue<V>(ceil(r.red()),
                       ceil(r.green()),
                       ceil(r.blue()),
			ceil(r.alpha()));
}

using VIGRA_CSTD::floor;

    /** Apply floor() function to each RGB component.
    */
template <class V>
inline
RGBAValue<V>
floor(RGBAValue<V> const & r)
{
    return RGBAValue<V>(floor(r.red()),
                       floor(r.green()),
                       floor(r.blue()),
			floor(r.alpha()));
}

//@}

//@}
//@}


} // namespace vigra

#endif // VIGRA_RGBAVALUE_HXX
