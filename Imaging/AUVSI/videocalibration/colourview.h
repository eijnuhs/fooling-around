#ifndef _COLOURVIEW_H
#define _COLOURVIEW_H


#include <qcolor.h>
#include <qwidget.h>

class ColourView : public QWidget 
{
  Q_OBJECT
 public:
  ColourView(QWidget * parent = 0, const char * name = 0);
  ~ColourView();
  void paintEvent ( QPaintEvent * p);
  public slots:
  void setColour(const QColor & c);
 private:
  QColor colour;
};

#endif




