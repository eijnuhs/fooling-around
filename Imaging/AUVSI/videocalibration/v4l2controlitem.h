#ifndef __V4L2_CONTROL_ITEM_H
#define __V4L2_CONTROL_ITEM_H

#include <qhbox.h>
#include <linux/videodev.h>

///// Forward Declarations /////
class V4L2Device;

class V4L2ControlItem : public QHBox 
{
  Q_OBJECT
 public:
  V4L2ControlItem(V4L2Device * vDev, struct v4l2_queryctrl qctl,  QWidget * parent, const char * name = 0 );
 public slots:
  void setV4LValue(int value);
 private:
  unsigned int controlId;
  V4L2Device * videoDevice;
  QString controlName;
};

#endif

