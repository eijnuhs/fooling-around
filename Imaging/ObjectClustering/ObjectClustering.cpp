// mjpeg_Capture 1.cpp : Defines the entry point for the console application.
// TODO split and merge / shape scanning 

#include <stdio.h>
#include <string.h>
#include <time.h>
#include "windows.h"

// OpenCV-Learn.cpp : Defines the entry point for the console application.

// OpenCV includes.
#include <cv.h>
#include <highgui.h>

// C++ includes.
#include <algorithm>
#include <iostream>
#include <sstream>
#include <vector>
 
//iSharp includes.
#include "captureData.hpp"

int camNumber = 0;
int waitTime = 2;
bool cameraFound = true;
bool cameraOn = true;
char * defaultPic = "C:\\uavImages\\testimg.png";
char * captureSaveFolder = "C:\\uavImages\\";
double captureFrequency = 3.0;		//time 5 = 1sec/captureFrequency

bool captureVideo = false;
bool captureNow = false;
clock_t nextCaptureTime = 0;
int targetID = 0;
int captureCount = 0;
int lastSaved = -1;

CvFont font;
double fontHScale		=0.5;
double fontVScale		=0.5;
int    fontLineWidth	=1;

long startTimeMS = 0;

bool smallVideo		= false;
bool smallCapture	= true;

int viewType = 0;
int viewTypeCount = 4;

//NORMALIZING OF COLOR USING CAMERA COLORBAR
enum colors {	
	WHITE = 0,
	YELLOW,
	CYAN,
	GREEN,
	MAGENTA,
	RED,
	BLUE,
	N_COLOR
};
int bar_color[] = {7,6,3,2,5,4,1};
const int N_RGB = 4; //storage for each channel


// Images
IplImage* img			= 0;
IplImage* img_s			= 0;
IplImage* imgCapture	= 0;
IplImage* imgCapture_s	= 0;

static std::vector<captureData*> captureList;

IplImage* img_temp			= 0; 
IplImage* img_sobel			= 0; // the edge image
IplImage* img_gray			= 0; // the gray image
IplImage* img_gray_smooth	= 0;
IplImage* prev_img_gray		= 0;
IplImage* img_pyramid		= 0;
IplImage* prev_img_pyramid	= 0;
IplImage* swap_temp;

CvCapture* capture = 0;


#define max_pts 256
const static int MIN_DIST_BETWEEN_TARGETS = 30;
int n_pts	= 0;
int coord_x [max_pts]; //found shapes x coordinate
int coord_y [max_pts]; //found shapes y coordinate
int max_d		  = 26;		//Max detection range
int edgeThresh1   = 1;		//Initial Threshold
int edgeThresh2   = 255;
int aperture	  = 3;		//best value to use 
int autoThreshold = 0;
double scalingConst	= 0.5;
int sizeOfImage		= 0;

//normalize
bool normalized		= 0;
bool normalizing	= 0;
int normalizeCount	= 0;
const int MAX_NORMALIZE_COUNT = 12;
double scalingR = 1.5;
double scalingG = 1.5;
double scalingB = 1.5;
//double scalingR		= 2.00;	//actual to use
//double scalingG		= 2.45;
//double scalingB		= 1.55;
double r_buffer;
double g_buffer;
double b_buffer;

//PID for thresholding
double Kp = 4.0;
double Ki = 0.0;
double Kd = 1.0;
const int RANGE = 100;
double threshIdealConst	= 0.005;

//Tracking
bool tracking = 0;
IplImage *hsv = 0, *hue = 0, *mask = 0, *backproject = 0, *histimg = 0;
CvHistogram *hist = 0;

int backproject_mode = 0;
int select_object = 0;
int track_object = 0;
int show_hist = 1;
CvPoint target_point;
CvRect selection;
const int WIN_SIZE = 10;
CvRect track_window;
CvBox2D track_box;
CvConnectedComp track_comp;
int hdims = 16;
float hranges_arr[] = {0,180};
float* hranges = hranges_arr;
int vmin = 10, vmax = 256, smin = 30;

using namespace std;

void initialize(){
	if(!img) return;
	
	img_temp		= cvCreateImage(cvSize(img->width/2,img->height/2), IPL_DEPTH_8U, 3);
	img_gray		= cvCreateImage(cvSize(img->width/2,img->height/2), IPL_DEPTH_8U,1);
	img_gray_smooth	= cvCreateImage(cvSize(img->width/2,img->height/2), IPL_DEPTH_8U,1);	
	img_sobel		= cvCreateImage(cvSize(img->width/2,img->height/2), IPL_DEPTH_8U,1);

	//tracking used	
	//cvNamedWindow( "CamShiftDemo", 1 );
	cvNamedWindow( "Histogram", 1 );
    	
	hsv = cvCreateImage( cvGetSize(img), 8, 3 );
    hue = cvCreateImage( cvGetSize(img), 8, 1 );
    mask = cvCreateImage( cvGetSize(img), 8, 1 );
    backproject = cvCreateImage( cvGetSize(img), 8, 1 );
    hist = cvCreateHist( 1, &hdims, CV_HIST_ARRAY, &hranges, 1 );
    histimg = cvCreateImage( cvSize(320,200), 8, 3 );
    cvZero( histimg );
}

void capture_toggle(){
	captureVideo = !captureVideo;
	if( captureVideo )
	{
		nextCaptureTime = (clock_t)(clock() + CLOCKS_PER_SEC/captureFrequency);
		captureNow = true;
	}
	else
	{
		targetID++;
	}
}

void on_mouse( int event, int x, int y, int flags, void* param )
{
    if( !img )
        return;

    if( img->origin )
        y = img->height - y;

    switch( event )
    {
    case CV_EVENT_LBUTTONDOWN:
		captureVideo = true;
		if(tracking){
			target_point = cvPoint(x,y);
			selection = cvRect(max(0,x-WIN_SIZE/2),max(0,y-WIN_SIZE/2),WIN_SIZE,WIN_SIZE);
			track_object = -1;
			//cout << "x,y = " << x <<","<<y<<endl;
			break;
		}
	case CV_EVENT_RBUTTONDOWN:
		captureVideo = false;
		track_object = 0;
		cvZero( histimg );
	}
}

CvScalar hsv2rgb( float hue )
{
    int rgb[3], p, sector;
    static const int sector_data[][3]=
        {{0,2,1}, {1,2,0}, {1,0,2}, {2,0,1}, {2,1,0}, {0,1,2}};
    hue *= 0.033333333333333333333333333333333f;
    sector = cvFloor(hue);
    p = cvRound(255*(hue - sector));
    p ^= sector & 1 ? 255 : 0;

    rgb[sector_data[sector][0]] = 255;
    rgb[sector_data[sector][1]] = 0;
    rgb[sector_data[sector][2]] = p;

    return cvScalar(rgb[2], rgb[1], rgb[0],0);
}

void tracking_target(IplImage* image){
	int _vmin = vmin, _vmax = vmax;
	int i, bin_w;

	cvCvtColor( image, hsv, CV_BGR2HSV );
	cvInRangeS( hsv, cvScalar(0,smin,MIN(_vmin,_vmax),0),
				cvScalar(180,256,MAX(_vmin,_vmax),0), mask );
	cvSplit( hsv, hue, 0, 0, 0 );
	
	if(track_object){
		//create histogram
		if( track_object < 0 )
		{
			float max_val = 0.f;
			cvSetImageROI( hue, selection );
			cvSetImageROI( mask, selection );
			cvCalcHist( &hue, hist, 0, mask );
			cvGetMinMaxHistValue( hist, 0, &max_val, 0, 0 );
			cvConvertScale( hist->bins, hist->bins, max_val ? 255. / max_val : 0., 0 );
			cvResetImageROI( hue );
			cvResetImageROI( mask );
			track_window = selection;
			track_object = 1;

			//drawing histogram
			cvZero( histimg );
			bin_w = histimg->width / hdims;
			for( i = 0; i < hdims; i++ )
			{
				int val = cvRound( cvGetReal1D(hist->bins,i)*histimg->height/255 );
				CvScalar color = hsv2rgb(i*180.f/hdims);
				cvRectangle( histimg, cvPoint(i*bin_w,histimg->height),
							 cvPoint((i+1)*bin_w,histimg->height - val),
							 color, -1, 8, 0 );
			}
		}

		cvCalcBackProject( &hue, backproject, hist );
		cvAnd( backproject, mask, backproject, 0 );
		cvCamShift( backproject, track_window,
				cvTermCriteria( CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 10, 1 ),
				&track_comp, &track_box );
		track_window = track_comp.rect;

		if( backproject_mode )
			cvCvtColor( backproject, image, CV_GRAY2BGR );
		if( !image->origin )
			track_box.angle = -track_box.angle;
		track_box.size.width += WIN_SIZE;
		track_box.size.height+= WIN_SIZE;
		track_box.center.x = max<int>(0,min<int>(image->width,track_box.center.x));
		track_box.center.y = max<int>(0,min<int>(image->height,track_box.center.y));
		cvEllipseBox( image, track_box, CV_RGB(255,0,0), 1, CV_AA, 0 );		
		/*
		CvPoint pt1 = {	(int)(track_box.center.x-track_box.size.width/2.0),
						(int)(track_box.center.y-track_box.size.height/2.0)};
		CvPoint pt2 = {	(int)(track_box.center.x+track_box.size.width/2.0),
						(int)(track_box.center.y+track_box.size.height/2.0)};
		pt1.x = max(min(pt1.x,image->width),0);
		pt1.y = max(min(pt1.y,image->height),0);
		pt2.x = max(min(pt2.x,image->width),0);
		pt2.y = max(min(pt2.y,image->height),0);
		cvRectangle(image,pt1,pt2,CV_RGB(255,0,0));
		*/
	}

	//cvShowImage( "CamShiftDemo", image );
    cvShowImage( "Histogram", histimg );
}

void normalize_image(IplImage* image){
	//Method 1
	/*
	int height     = image->height;
	int width      = image->width;
	int step       = image->widthStep/sizeof(uchar);
	int channels   = image->nChannels;
	uchar* data    = (uchar *)image->imageData;
	uchar r,g,b;
	for (int i=0;i<image->height;i++){
		for (int j=0; j<image->width; j++){
			b = (uchar) min(255.0,data[i*step+j*channels+0]*scalingB);
			g = (uchar) min(255.0,data[i*step+j*channels+1]*scalingG);
			r = (uchar) min(255.0,data[i*step+j*channels+2]*scalingR);
			data[i*step+j*channels+0] = b;
			data[i*step+j*channels+1] = g;
			data[i*step+j*channels+2] = r;
		}
	}
	*/
	//Method 2	
	double scale = (scalingB+scalingG+scalingR)/3.0;
	cvConvertScale( image, image, scale, 0 );
	//Method 3 
	
}

int num_edgepixel(IplImage* image){
	int num=0;
	int c;
	for (int i=0;i<img_sobel->height;i++){
		for (int j=0; j<img_sobel->width; j++){
			c = ((uchar *)(img_sobel->imageData + i*img_sobel->widthStep))[j];
			//Draw the pixel red if Canny found an edge {i,j}
			if(c > 200){
				num++;
			}
		}
	}
	return num;
}

void show_images(){
	
	if( img )
	{
		if(smallVideo)
		{
			cvResize(img, img_s, 0);
			cvShowImage("Video Feed 1", img_s);
		}
		else
		{
			cvShowImage("Video Feed 1", img);
		}
	}
	
	if( imgCapture )
	{
		if(smallCapture)
			cvShowImage("Capture 1", imgCapture_s);
		else
			cvShowImage("Capture 1", imgCapture);
	}
}
/* 
	Check if a shape is found. If found, draw a rect relative to its size.
*/
//Some constants used by function i = y, j = x

void detect_draw(int i,int j, IplImage* image){
	//STOP if exceed max number of shape found !
	if(n_pts >= max_pts)
		return;

	int k = 0, lim_pixel = MIN_DIST_BETWEEN_TARGETS; // LIMITING PIXEL RANGE PER DETECTION
	//check if its close to any of the found shapes
	for(k=0;k<n_pts;k++){
		//distance between found point and new point
		double dist = sqrt( (double)(coord_x[k]-j)*(coord_x[k]-j) + (coord_y[k]-i)*(coord_y[k]-i) );
		//throw away if too close
		if(dist <= lim_pixel ){
			return;
		}
	}
	// A VALID POINT TO TEST

	int pf = 0, pd = 0, pu = 0;
	int pne = 0;
	int pnw = 0;
	int pse = 0;
	int psw = 0;
	
	//finding horizontal across, always try to find the last edge
	//hf = horizontal front pixel, hb = horizonal back pixel
	int fColor = 0;
	int bColor = 255;
	for(int h = j+1; h < j+max_d && h < img_sobel->width; h++){
		fColor = ((uchar *)(img_sobel->imageData + i*img_sobel->widthStep))[h];
		if(fColor > 200	&& bColor == 0  ){
			//found an edge if hf == 255 and hb == 0
			pf = h;
		}
		bColor = fColor;
	}
	if( pf == 0 )
		return;
	//mid point of across
	int mc = (pf + j)/2;
	//length of across
	int f = pf-j; 
	
	//finding vertically up
	fColor = 0;
	bColor = 255;
	for(int u = i-1; u > i-max_d && u > 0; u--){
		fColor = ((uchar *)(img_sobel->imageData + u*img_sobel->widthStep))[mc];
		if( fColor > 200 && bColor == 0){
			pu = u;
		}
		bColor = fColor;
	}
	if( pu == 0 )
		return;

	//finding vertically down
	fColor = 0;
	bColor = 255;
	for(int w = i+1; w < i+max_d && w < img_sobel->height; w++){
		fColor = ((uchar *)(img_sobel->imageData + w*img_sobel->widthStep))[mc];
		if( fColor > 200 && bColor == 0){
			pd = w;
		}
		bColor = fColor;
	}
	if( pd == 0 )
		return;
	//mid point of down
	int md = (pd + pu)/2;
	//height of shape
	int d = pd-pu;


	//finding diagonal north-east
	fColor = 0;
	bColor = 255;
	int temp_x = mc+1;
	for(int s = i-1;	
		s > i-max_d && s > 0 && //bounds y axis range
		temp_x < img_sobel->widthStep; //bounds x axis range
		s--){
		fColor = ((uchar *)(img_sobel->imageData + s*img_sobel->widthStep))[temp_x++];
		if( fColor > 200 && bColor == 0){
			pne = s;
		}
		bColor = fColor;
	}
	if( pne == 0 )
		return;

	//finding diagonal north-west
	fColor = 0;
	bColor = 255;
	temp_x = mc-1;
	for(int s = i-1;	
		s > i-max_d && s > 0 && //bounds y axis range
		temp_x > 0; //bounds x axis range
		s--){
		fColor = ((uchar *)(img_sobel->imageData + s*img_sobel->widthStep))[temp_x--];
		if( fColor > 200 && bColor == 0){
			pnw = s;
		}
		bColor = fColor;
	}
	if( pnw == 0 )
		return;

	//finding diagonal south-east
	fColor = 0;
	bColor = 255;
	temp_x = mc+1;
	for(int s = i+1;	
		s < i+max_d && s < img_sobel->height && //bounds y axis range
		temp_x < img_sobel->widthStep; //bounds x axis range
		s++){
		fColor = ((uchar *)(img_sobel->imageData + s*img_sobel->widthStep))[temp_x++];
		if( fColor > 200 && bColor == 0){
			pse = s;
		}
		bColor = fColor;
	}
	if( pse == 0 )
		return;

	//finding diagonal south-west
	fColor = 0;
	bColor = 255;
	temp_x = mc-1;
	for(int s = i+1;	
		s < i+max_d && s < img_sobel->height && //bounds y axis range
		temp_x > 0; //bounds x axis range
		s++){
		fColor = ((uchar *)(img_sobel->imageData + s*img_sobel->widthStep))[temp_x--];
		if( fColor > 200 && bColor == 0){
			psw = s;
		}
		bColor = fColor;
	}
	if( psw == 0 )
		return;

	//found something interesting ?
	if( d+f < 2*max_d &&		// this is to make sure its within max_dim allowed 
		(max(d,f)/min(d,f)) <2) // this is to find a regular shape
	{
		//Check if its near a recently found.
		CvPoint pt1 = {mc-(f),md-(d)};
		CvPoint pt2 = {mc+(f),md+(d)};

		if( viewType == 0 )
		{
			pt1.x *= 2;
			pt1.y *= 2;
			pt2.x *= 2;
			pt2.y *= 2;
		}

		pt1.x = max( pt1.x, 0 );
		pt1.y = max( pt1.y, 0 );
		pt1.x = min( pt1.x, image->width-1 );
		pt1.y = min( pt1.y, image->height-1 );
		pt2.x = max( pt2.x, 0 );
		pt2.y = max( pt2.y, 0 );
		pt2.x = min( pt2.x, image->width-1 );
		pt2.y = min( pt2.y, image->height-1 );
		
		//adding to stored points
		coord_x[n_pts] = mc;
		coord_y[n_pts] = md;
		n_pts++;
		cvRectangle( image, pt1, pt2, CV_RGB(0,255,255),1);
		
	}				
}

void canny_move(IplImage* image)
{
	if(!image){
		cout << "Error reading image" << endl;	
		return;
	}
	//initializing
	n_pts = 0;
	cvZero( img_sobel );
	/*if(!img_gray){
		cout << "Error reading gray image header" << endl;
		return;
	}*/
	cvResize( image, img_temp, 0);
	cvCvtColor( img_temp, img_gray, CV_RGB2GRAY);
    cvSmooth( img_gray, img_gray, CV_GAUSSIAN, 3);
	//scale down
	cvConvertScale( img_gray, img_gray, scalingConst, 0 );
	cvCanny(img_gray, img_sobel, edgeThresh1, edgeThresh2, aperture);
	
	/*
	// SOBEL CODE
	cvZero( img_sobel_x );	
	cvZero( img_sobel_y );	
	cvSmooth( img_gray, img_sobel_x, CV_BLUR, 3, 3, 0, 0 );
	cvSmooth( img_gray, img_sobel_y, CV_BLUR, 3, 3, 0, 0 );
	cvNot( img_gray, img_sobel_x );
	cvNot( img_gray, img_sobel_y );
	cvSobel(img_gray, img_sobel_x, 1, 0, 3);
	cvSobel(img_gray, img_sobel_y, 0, 1, 3);
	cvConvertScale(img_sobel,img_sobel,1);
	cvZero(img_sobel);
		
	int a = 0,b = 0,r = 0;
	for(int i=0;i<img_sobel->height;i++){
		for(int j=0;j<img_sobel->width;j++){
			a = ((uchar *)(img_sobel_x->imageData + i*img_sobel_x->widthStep))[j];
			b = ((uchar *)(img_sobel_y->imageData + i*img_sobel_y->widthStep))[j];
			if(	a > edge_thresh * 2.55 || b >edge_thresh * 2.55){
				((uchar *)(img_sobel->imageData + i*img_sobel->widthStep))[j]= max(a,b);
			}
		}
	}
	*/
	
	// Merging with Video Feed 1
	int c = 0; 
	int step = 0;
	for (int i=0;i<img_sobel->height;i=i+2){
		for (int j=0; j<img_sobel->width; j=j+1){
			c = ((uchar *)(img_sobel->imageData + i*img_sobel->widthStep))[j];
			//Draw the pixel red if Canny found an edge {i,j}
			if(c > 200){
				if( viewType == 0 )
					detect_draw(i, j, image);
				if( viewType == 1 )
					detect_draw(i, j, img_gray);
				if( viewType == 2 )
					detect_draw(i, j, img_sobel);
			}
		}
	}

	if( viewType == 1 )
	{
		cvCvtColor( img_gray, img_temp, CV_GRAY2RGB);
		cvResize( img_temp, image, 1);
	}
	else if( viewType == 2 )
	{
		cvCvtColor( img_sobel, img_temp, CV_GRAY2RGB);
		cvResize( img_temp, image, 1);
	}

}


void start_stream()
{
	if( !capture )
	{
		capture = cvCaptureFromCAM(camNumber);
		cout << "(Capture object created)" << endl;
	}
	
	if( !capture )
		cameraOn = false;
}

void stop_stream(){
	if(capture)
	{
		cvReleaseCapture( &capture );
		capture = 0;
		cout << "(Capture object released)" << endl;
	}
}
 
void get_Frame(){

	if( capture )
	{
		IplImage* frame;
		frame = cvQueryFrame( capture );
		//cout << "Image size " << frame->width << "," << frame->height << endl;
		if( frame )
		{
			img = frame;
		}

		/*if( !videoImage )
        {
            videoImage = cvCreateImage( cvGetSize(frame), 8, 3 );
            //videoImage->origin = frame->origin;
        }*/
	}
}

void printUsage(){
	cout << endl;
	cout << "Usage:" << endl;
	cout << "----------------" << endl;
	cout << "Press" << endl;
	cout << "Space : Start/stop capturing frames" << endl;
	cout << "C : Capture single frame" << endl;
	cout << "S : Start/stop saving captured frames" << endl;
	cout << "U : Show this screen" << endl;
	cout << "Q : Quit Program" << endl;
	cout << "? : Start color normalizing using color bar" << endl;
	cout << "/ : Show color normalized image" << endl;
	cout << "` : Change view (raw, canny input, canny output, normal)" << endl;
	cout << "1 : Change video feed window size" << endl;
	cout << "2 : Change capture window size" << endl;
	cout << "N : Stop video feed" << endl;
	cout << "M : Start video feed" << endl;
	cout << endl;
}

long getCurrentTime()
{
	/*
	long time;
	time = clock();
	time -= startTimeMS;
	time *= (long)((double)1000 / (double)CLOCKS_PER_SEC);
	*/
	
	SYSTEMTIME time;
	GetSystemTime(&time);

	int hours = (time.wHour + 24-5) % 24;
	long millis = hours*1000*60*60;
	millis += (time.wMinute * 1000*60)
		+ (time.wSecond * 1000)
		+ time.wMilliseconds;

	return millis;
}
//Little subroutines to look normalization
int getHighestIndex(int arr[],int size){
	int largestIndex=0;
	int temp=arr[0];
	int i;
	for(i=0;i<size;i++){
		if(arr[i]>temp){
			largestIndex = i;
			temp=arr[i];
		}
	}
	return largestIndex;
}
int getLowestIndex(int arr[],int size){
	int smallestIndex=0;
	int temp=arr[0];
	int i;
	for(i=0;i<size;i++){
		if(arr[i]<temp){
			smallestIndex = i;
			temp=arr[i];
		}
	}
	return smallestIndex;
}
//Normalized using a single or multiple frames
void normalize_colors(IplImage* image){
	if(!img)	return;
	
	//7 color bars are used
	int bar_width = image->width/N_COLOR;
	int x1 = 0;
	int x2 = bar_width;
	int xm = bar_width/2;
	int ym = image->height/2;
	int r[N_RGB];
	int g[N_RGB];
	int b[N_RGB];
	double r_temp;
	double g_temp;
	double b_temp;
	//init to 0;
	for(int i = 0; i < N_RGB; i++)	{r[i] = 0; g[i] = 0; b[i] = 0;}
	uchar * data = (uchar *) image->imageData;
	int step       = image->widthStep/sizeof(uchar);
	int channels   = image->nChannels;
	int lowestIndexR,lowestIndexG,lowestIndexB;
	for(int i = 0; i < N_COLOR; i++){
		b_temp = (uchar) data[ym*step+xm*channels+0];
		g_temp = (uchar) data[ym*step+xm*channels+1];
		r_temp = (uchar) data[ym*step+xm*channels+2];
		lowestIndexR = getLowestIndex(r,N_RGB);
		lowestIndexG = getLowestIndex(g,N_RGB);
		lowestIndexB = getLowestIndex(b,N_RGB);
		if(r[lowestIndexR] < r_temp)
			r[lowestIndexR] = (int) r_temp;
		if(g[lowestIndexG] < g_temp)
			g[lowestIndexG] = (int) g_temp;
		if(b[lowestIndexB] < b_temp)
			b[lowestIndexB] = (int) b_temp;
		xm += bar_width;
		/*for(int k = 0;k<N_RGB;k++){
			cout <<  "ARRAY " << r[k] << "," << g[k] << "," << b[k] << "," << endl;
		}*/
	}
	//throw away the largest value NOTE: smallest is already throug
	r[getHighestIndex(r,N_RGB)] = 0;
	g[getHighestIndex(g,N_RGB)] = 0;
	b[getHighestIndex(b,N_RGB)] = 0;
	r_temp = g_temp = b_temp = 0;
	
	for(int i = 0;i<N_RGB;i++){
		r_temp += r[i];
		g_temp += g[i];
		b_temp += b[i];
	}
	//cout << "total = " << r_temp << " " << g_temp << " " << b_temp << endl;
	r_temp /= (N_RGB - 1);
	g_temp /= (N_RGB - 1);
	b_temp /= (N_RGB - 1);
	//cout << "average = " << r_temp << " " << g_temp << " " << b_temp << endl;
	r_temp = 255.0/r_temp;
	g_temp = 255.0/g_temp;
	b_temp = 255.0/b_temp;
	//cout << "ratio   = " << r_temp << " " << g_temp << " " << b_temp << endl;

	if(normalizeCount > 1){
		r_buffer = (r_buffer+r_temp)/2.0;
		g_buffer = (g_buffer+g_temp)/2.0;
		b_buffer = (b_buffer+b_temp)/2.0;
	}
	else{
		r_buffer = r_temp;
		g_buffer = g_temp;
		b_buffer = b_temp;
	}
	
	//cout << "R,G,B = " << scalingR << "," << scalingG << "," << scalingB << endl;
	
}

void start_loop(){
	//PID controller stuff
	double prev_err = 0.0;
	double curr_err = 0;
	double err_sum;
	double err_diff;
	double pid_out;
	
	//REMOVE
	double avg_pixel = 0;
	double curr_avg = 0;

	// Initialising the Images with frame size
	if( cameraOn )
		get_Frame();

	if( img )
	{
		cameraFound = true;
	}
	else
	{
		cameraFound = false;
		cout << "Capture device not found!  Loading default image." << endl
			<< "  Restart program to use video input." << endl;
		img = cvLoadImage(defaultPic);
	}
	initialize();
	sizeOfImage = img->width * img->height;
	img_s = cvCreateImage( cvSize(img->width/2, img->height/2), 8, 3 );
	imgCapture_s = cvCreateImage( cvSize(img->width/2, img->height/2), 8, 3 );

	imgCapture = cvCloneImage( img );
	if( imgCapture )
		cvResize(imgCapture, imgCapture_s);

	bool saving = false;
	captureList.clear();

	

	//things to print
	long currentTime = 0;
	int fps = 0;

	int imageCount = 0;
	

	cout << "Image size " << img->width << "," << img->height << endl;

	// Loop forever until the user quits.
	bool done = false;
	while(!done)
	{	
		//WAIT TIME
		int keyCode = cvWaitKey(waitTime);

		if( cameraOn )
		{
			currentTime = getCurrentTime();
			//fps = (int) cvGetCaptureProperty(capture, CV_CAP_PROP_FPS);
			//cout << fps << endl;

			//cout << (double)imageCount / (currentTime/1000.0) << endl;

			if( cameraFound )
			{
				get_Frame();
			}
			else
			{
				img = cvLoadImage(defaultPic);
			}
			
			// THE MAIN IMAGE PROCESSING STUFF
			if( img )
			{	
				if(normalized)
					normalize_image(img);
				if(viewType != 3)
					canny_move(img);
				if(tracking)
					tracking_target(img);
				
				stringstream text;
				text << currentTime / (1000*60*60) << ":"
					 << (currentTime / (1000*60)) % 60 << ":"
					 << (currentTime / (1000)) % 60 << "."
					 << (currentTime) % 1000 ;
					 //<<"FPS: " << fps;
				cvPutText (img, text.str().c_str(), cvPoint(5,img->height-5), &font, cvScalar(50,0,50));
				imageCount++;
			}			
		}

		captureNow = false;

		//KEY CODES
		if (' ' == (char)keyCode )	//CAPTURING using SPACE
		{
			capture_toggle();
		}
		else if ('c' == (char)keyCode)
		{
			captureNow = true;
		}
		else if('s' == (char)keyCode)
		{
			saving = !saving;
			if( saving )
				cout << "Saving..." << endl;
			else
				cout << "Saving stopped." << endl;
		}
		else if ('`' == (char)keyCode)
		{
			viewType++;
			if( viewType >= viewTypeCount )
				viewType = 0;
		}
		else if ('0' == (char)keyCode)
		{
			autoThreshold = autoThreshold ^ 1;
			if(autoThreshold)
				cout << "automatic thresholding on" << endl;
			else
				cout << "automatic thresholding off" << endl;
		}
		else if ('=' == (char)keyCode)
		{
			edgeThresh1 += 5;
			edgeThresh1 = min(edgeThresh1,255);
			cout << "edge threshold = " << edgeThresh1 << "," << edgeThresh2 << endl;
		}
		else if ('-' == (char)keyCode)
		{
			edgeThresh1 -= 5;
			edgeThresh1 = max(edgeThresh1,1);
			cout << "edge threshold = " << edgeThresh1 << "," << edgeThresh2 << endl;
		}
		else if ('+' == (char)keyCode)
		{
			edgeThresh2 += 5;
			edgeThresh2 = min(edgeThresh2,255);
			cout << "edge threshold = " << edgeThresh1 << "," << edgeThresh2 << endl;
		}
		else if ('_' == (char)keyCode)
		{
			edgeThresh2 -= 5;
			edgeThresh2 = max(edgeThresh2,1);
			cout << "edge threshold = " << edgeThresh1 << "," << edgeThresh2 << endl;
		}
		else if('q' == (char)keyCode || (char) keyCode == 27)
		{
			done = true;
		}
		else if('n' == (char)keyCode)
		{
			cout << "Video feed stopped" << endl;
			cameraOn = false;
			if( cameraFound && capture )
			{
				IplImage* frame = 0;
				if( img )
				{
					frame = cvCloneImage( img );
				}

				stop_stream();

				if( frame )
				{
					img = frame;
				}
			}
		}
		else if('m' == (char)keyCode)
		{
			cout << "Video feed started" << endl;
			cameraOn = true;
			if( cameraFound && !capture )
			{
				start_stream();
			}
		}
		else if('u' == (char)keyCode)
		{
			printUsage();
		}
		else if('1' == (char)keyCode){
			smallVideo = !smallVideo;
		}
		else if('2' == (char)keyCode){
			smallCapture = !smallCapture;
		}
		//Camera Callibration
		else if('?' == (char) keyCode){
			normalizing = !normalizing;
			if(normalizing){
				cout << "Color normalizing started" << endl;
				normalizeCount = 0;
			}
		}
		else if('/' == (char) keyCode) {
			normalized = !normalized;
			if(normalized)
				cout << "color normalizing ON" << endl;
			else
				cout << "color normalizing OFF" << endl;
		}
		else if('t' == (char) keyCode){
			tracking = !tracking;
			if(tracking)
				cout << "Tracking target ON" << endl;
			else
				cout << "Tracking target OFF" << endl;
		}
		
		//normalize
		if(normalizing)
		{
			if(	normalizeCount >= MAX_NORMALIZE_COUNT ) 
			{
				normalizing = false;
				scalingR = r_buffer;
				scalingG = g_buffer;
				scalingB = b_buffer;
				cout << "(R,G,B) = " << scalingR <<","<<scalingG<<","<<scalingB<<endl;
			}
			else
			{
				normalizeCount++;
				normalize_colors(img);				
			}
		}
		if(tracking){
			tracking_target(img);
		}
		
		if( captureVideo && !captureNow )
		{
			if( clock() >= nextCaptureTime )
			{
				if( n_pts > 0 || clock() >= nextCaptureTime + CLOCKS_PER_SEC/captureFrequency )
				{
					nextCaptureTime += (clock_t) (CLOCKS_PER_SEC/captureFrequency);
					nextCaptureTime = max( nextCaptureTime, clock() );
					captureNow = true;
				}
			}
		}
		// capturing images
		if( captureNow )
		{
			captureNow = false;
			if( img )
			{
				if( imgCapture )
				{
					cvReleaseImage( &imgCapture );
				}
				if( imgCapture_s )
				{
					cvReleaseImage( &imgCapture_s );
					imgCapture_s = cvCreateImage( cvSize(img->width/2, img->height/2), 8, 3 );
				}

				imgCapture = cvCloneImage(img);
				cvResize(imgCapture, imgCapture_s, 0);
				
				int n_tracked_target = 0;
				if(track_box.center.x && track_box.center.y){
					n_tracked_target = 1;
				}

				captureData* newData = new captureData( n_pts + n_tracked_target);

				IplImage* storedImage = cvCloneImage(imgCapture);
				newData->image = storedImage;
				newData->time = currentTime;
				for( int i=0; i < n_pts; i++ )
				{
					newData->coordX[i] = coord_x[i];
					newData->coordY[i] = coord_y[i];
				}
				if(n_tracked_target){
					newData->coordX[n_pts] = track_box.center.x;
					newData->coordY[n_pts] = track_box.center.y;
				}
				else
				
				newData->imageID = captureCount;
				newData->targetID = targetID;

				captureList.push_back( newData );

				captureCount++;

				cout << "Unsaved images captured: " << captureList.size()-(lastSaved+1) << endl;
			}
		}
		//automatic thresholding
		if(autoThreshold)
		{
			//make use of a PID controller
			curr_avg = (double)num_edgepixel(img_sobel)/sizeOfImage;
			avg_pixel = (curr_avg+avg_pixel)/2;
			
			curr_err = curr_avg - threshIdealConst;
			err_sum  = min<int>(curr_err + prev_err,1);
			err_diff = curr_err - prev_err;
			
			pid_out = Kp * curr_err + Ki * err_sum + Kd * err_diff;
			edgeThresh2 = (int) min<int>(max<int>(edgeThresh2 + RANGE * pid_out,edgeThresh1),255);
			
			//cout << "average pixel = " << avg_pixel << endl;
			//cout << "pixel percentage  = " << curr_err << endl;
			//cout << "PO = " << pid_out << " ET = " << edgeThresh2 << endl;
			//cout << "VALUE CHANGED = " << RANGE * pid_out << endl;
			prev_err = curr_err;
		}

		show_images();

		if( saving )
		{
			lastSaved++;

			if( lastSaved < (int)captureList.size() && captureList[lastSaved]->image )
			{
				//saving image
				captureList[lastSaved]->saveData( captureSaveFolder );
				cout << "  (image " << lastSaved+1 << " of "
					<< captureList.size() << ")" << endl;

				captureList[lastSaved]->destroyData();

				delete captureList[lastSaved];
				captureList[lastSaved] = 0;	
				
			}

			if( lastSaved >= (int)captureList.size()-1 )
			{
				captureList.clear();
				lastSaved = -1;
				saving = false;
				cout << "All images saved!" << endl;
			}
		}
		
	}//end while
}

int main(int argc, char* argv[])
{
	cvInitFont(&font,CV_FONT_HERSHEY_SIMPLEX|CV_FONT_ITALIC, fontHScale,fontVScale,0,fontLineWidth);
	cvNamedWindow("Video Feed 1");
	cvNamedWindow("Capture 1");
	cvSetMouseCallback( "Video Feed 1", on_mouse, 0 );

	printUsage();

	// Start Sensoray Stream
	start_stream();	
	
	// Main Loop
	start_loop();

	// Destroy the Capture object and the window.
	stop_stream();

	if(img_s)
		cvReleaseImage(&img_s);
	if(imgCapture)
		cvReleaseImage(&imgCapture);
	if(imgCapture_s)
		cvReleaseImage(&imgCapture_s);
	if(img_temp)
		cvReleaseImage(&img_temp);
	if(img_gray)
		cvReleaseImage(&img_gray);
	if(img_sobel)
		cvReleaseImage(&img_sobel);
	cvDestroyWindow("Capture 1");
	cvDestroyWindow("Video Feed 1");
	printf("Quit Program...\n");
	return 0;
}


