#ifndef _BALL_H_
#define _BALL_H_

#include "spot.h"
#include "run.h"




class Ball : public Spot
{
 public:
  Ball(std::string name, double height, double radius, double mmPerPix, ColourDefinition * cd);
  ~Ball();

  virtual void initForFrame();
  
  // methods required by trackable.
  bool isMe(RunRegion & region, FrameBuffer * inFrame, FrameBufferWorld * worldFrame, unsigned int avgIntensity);
  bool trackMe(FrameBuffer * inFrame, FrameBufferWorld * worldFrame);

  // allows the ball to decide who it is.
  void trackingComplete();
  
 private:
  void addRegion(double wx, double wy, const RunRegion & r, double mmPerPix);
  void addRegion(WorldRegion wr);
  std::vector<WorldRegion> regions;
  double idealArea;
  struct timeval t;
};

#endif

