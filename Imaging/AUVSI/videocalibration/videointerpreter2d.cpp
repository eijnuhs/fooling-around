// $Id: videointerpreter2d.cpp,v 1.1.1.1.2.24 2005/01/27 02:33:46 cvs Exp $


#include "videointerpreter2d.h"
#include "videointerpreter2dconfig.h"
#include "videostream.h"
#include <string>
#include "rotationmatrix.h"
#include "pixel.h"
#include <inttypes.h>
#include "framebuffer.h"
#include "geometry.h"
#include <assert.h>
#include <iostream>
#include "mask.h"
#include "framebufferrgb32.h"
#include "framebufferworld.h"

using namespace std;

// deprecated constructor.
VideoInterpreter2D::VideoInterpreter2D( VideoStream * vs, string cameraFilename, string calibrationPointsFilename, string rotationMatrixFilename ) :
  vs( vs ),
  calibration( cameraFilename, calibrationPointsFilename ),
  rotationMatrix( rotationMatrixFilename ),
  field( 2740.0, 1520.0, 200.0 ),
  mask(0),
  worldMap( 0 ),
  screenMap( 0 )
{
  init();
}

VideoInterpreter2D::VideoInterpreter2D( VideoStream * vs, 
					VideoInterpreter2DConfig * vi2dc ) :
  vs( vs ),
  calibration( vi2dc->getString("cameraFileName","/home/jacky/video_config/camera.dat"), 
	       vi2dc->getString("calibrationPoints","/home/jacky/video_config/calibrationpoints.dat")),
  rotationMatrix( vi2dc->getString("rotationMatrixFilename", "/home/jacky/video_config/rotationmatrix.dat")),
  field( vi2dc->getDouble("fieldX",2740.0), vi2dc->getDouble("fieldY",1520.0), vi2dc->getDouble("fieldBorder", 200.0)),
  mask(0),
  worldMap( 0 ),
  screenMap( 0 )
{
  init();
  ControlPoints pts(vi2dc->getControlPoints(this));
  rotationMatrix.setControlPoints(pts);
}

void VideoInterpreter2D::init()
{
  xFactor = ( 2 * field.margin + field.width ) / MAP_DIMENSION;
  yFactor = ( 2 * field.margin + field.height ) / MAP_DIMENSION;

  fwidth = vs->getVideoDevice()->getWidth();
  fheight = vs->getVideoDevice()->getHeight() * (vs->isInterlaced() ? 1 : 2);

  updateScreenToWorldMap();
  updateWorldToScreenMap();

  vs->setVideoInterpreter2D( this );
  
}


// deprecated.  Control points are now loaded from the config file.
void VideoInterpreter2D::loadControlPoints(std::string filename)
{
  int xw,yw, xm, ym;
  char cps_token[256];
  ControlPoints pts;
  FILE *fd = fopen (filename.c_str (), "r");
  if (NULL == fd)
    {
      std::cerr << "RotationMatrix:: rotationMatrix file error";
      return;
    }
  while(fscanf(fd, "%s", cps_token) == 1 && strcmp("CONTROL_POINT", cps_token) == 0)
    {
      if(fscanf(fd,"%d %d", &xw,&yw) == 2)
        {
	  worldToScreenCoordinates ((double)xw, (double)yw, 0.0, &xm, &ym);
	  ControlPoint p(xw,yw,0,xm,ym,0);
	  pts.addPoint(p);
        }
    }
  
  rotationMatrix.setControlPoints(pts);

}

VideoInterpreter2D::~VideoInterpreter2D( )
{
  if ( worldMap != 0 )
    {
      delete [] worldMap;
    }
  if ( screenMap != 0 )
    {
      delete [] screenMap;
    }
}

void VideoInterpreter2D::worldToScreenCoordinates( double x, double y, double z, int * ix, int * iy, bool useMap)
{
  double xx, yy;
  worldToScreenCoordinates( x, y, z, &xx, &yy, useMap);
  *ix = static_cast< int >( rint( xx ) );
  *iy = static_cast< int >( rint( yy ) );
}

void VideoInterpreter2D::worldToScreenCoordinates( double x, double y, double z, double * ix, double * iy, bool useMap)
{
  if ( ( z == 0.0 ) && ( worldMap != 0 ) && useMap )
    {
      unsigned int xi = static_cast< unsigned int >( rint( ( x + field.margin ) / xFactor ) );
      unsigned int yi = static_cast< unsigned int >( rint( ( y + field.margin ) / yFactor ) );
      if(xi < MAP_DIMENSION && yi < MAP_DIMENSION)
	{
	  *ix = worldMap[ xi ][ yi ].x;
	  *iy = worldMap[ xi ][ yi ].y;
	}
      else
	worldToScreenCoordinates(x,y,z,ix,iy,false);
    }
  else
    {
      double xr, yr;
      rotationMatrix.rotatei( x, y, & xr, & yr );
      calibration.worldCoordToImageCoord( xr, yr, z, ix, iy);
    }
}

void VideoInterpreter2D::screenToWorldCoordinates( double ix, double iy, double z, double * x, double * y, bool useMap)
{
  if ( ( z == 0.0 ) && ( screenMap != 0 ) && useMap )
    {
      unsigned int xi = static_cast< int >( rint( ix ) );
      unsigned int yi = static_cast< int >( rint( iy ) );
            assert( (int)xi >= 0 );
            assert( (int)xi < (int)fwidth );
            assert( (int)yi >= 0 );
            assert( (int)yi < (int)fheight );

      *x = screenMap[ yi * fwidth + xi ].x;
      *y = screenMap[ yi * fwidth + xi ].y;
    }
  else
    {
      double xr, yr;
      //       assert( (int)ix >= 0 );
      //       a/ssert( (int)ix < (int)fwidth );
      //       assert( (int)iy >= 0 );
      //
      //       assert( (int)iy < (int)fheight );
      calibration.imageCoordToWorldCoord( ix, iy, z, & xr, & yr);
      rotationMatrix.rotate( xr, yr, x, y );
    }
}

void VideoInterpreter2D::screenToWorldCoordinates( unsigned int ix, unsigned int iy, double z, double * x, double * y , bool useMap )
{
  if ( ( z == 0.0 ) && ( screenMap != 0 ) && useMap)
    {
#ifndef NDEBUG
      if ( ( ix >= fwidth ) || ( iy >= fheight ) )
	{
	  std::cerr << "VI2D::screenToWorldCoordinates( " << ix << "," << iy << "," << z << "...) fwdith " << fwidth << ", fheight " << fheight << std::endl;
	}
      assert( ( (int) ix >= 0 ) && ( ix < fwidth ) );
      assert( ( (int) iy >= 0 ) && ( iy < fheight ) );
#endif
      *x = screenMap[ iy * fwidth + ix ].x;
      *y = screenMap[ iy * fwidth + ix ].y;
    }
  else
    {
      assert( ( (int) ix >= 0 ) && ( ix < fwidth ) );
      assert( ( (int) iy >= 0 ) && ( iy < fheight ) );
      double xx = static_cast< double >( ix );
      double yy = static_cast< double >( iy );
      screenToWorldCoordinates( xx, yy, z, x, y );
    }
}

/* Paul's Crappy Blur Neighbor Interpolation */
#if 1
bool
VideoInterpreter2D::interpolatePixelBlur( FrameBuffer const * frame, double const x, double const y, double const z, RawPixel * pixel)
{
#ifdef XXDEBUG
  cout << "vi2d::interpolatePixelBlur ";
  cout << "Frame: " << (void *)frame << ", (" << x << "," << y << "," << z << ")\n";
#endif
  unsigned int xc;
  unsigned int yc;
  double xi, yi;
  bool success = false;
  //  assert( ( x >= - field.margin ) && ( x <= field.width + field.margin ) );
  //  assert( ( y >= - field.margin ) && ( y <= field.height + field.margin ) );

  worldToScreenCoordinates( x, y, z, &xi, &yi );
  if(!frame->interlaced)
    {
      yi = yi / 2.0;
    }
  //  if(frame->fieldNo == 3 && yi > 1.0)
  //    {
      // cout << "Field";
  //      yi = yi - 1.0;
  //    }
  xc = static_cast<unsigned int>( rint( xi ) );
  yc = static_cast<unsigned int>( rint( yi ) );

#ifdef XXDEBUG
  std::cout << "VideoInterpreter2D::interpolatePixel called: frame->buffer" << (void*)frame->buffer ;
  std::cout << "(x,y)->(xc,yc): (" << x << "," << y << ")->(" << xc <<  "," << yc << ")\n";
#endif 

  if ( ( (int) xc > 2 ) && ( xc < frame->width-3 ) && ( (int) yc > 2 ) && ( yc < frame->height-3 ) )
    {
      assert( ( (int)xc >= 0 ) && ( xc < frame->width ) );
      assert( ( (int)yc >= 0 ) && ( yc < frame->height ) );
      FrameBufferIterator fIter( const_cast< FrameBuffer * > ( frame ), yc, xc );
      fIter.getBlurred3x3Pixel(pixel);
      success = true;
     }
  else
    {
      *pixel = Pixel(0,0,0);
    }
  return success;
}
#endif

#if 1
/* Nearest Neighbor Interpolation */
bool
VideoInterpreter2D::interpolatePixel( FrameBuffer const * frame, double const x, double const y, double const z, RawPixel * pixel )
{
#ifdef XXDEBUG
  cout << "vi2d::interpolatePixel ";
  cout << "Frame: " << (void *)frame << ", (" << x << "," << y << "," << z << ")\n";
#endif
  unsigned int xc;
  unsigned int yc;
  bool success = false;

  enum Neighbors
    {
      BOTTOM_LEFT = 0,
      BOTTOM_RIGHT,
      TOP_LEFT,
      TOP_RIGHT,
      BOTTUM_ROW,
      TOP_ROW,
      PIXEL,
      LAST
    };

  RawPixel neighbors[ LAST ];

  double xi, yi;

  //  assert( ( x >= - field.margin ) && ( x <= field.width + field.margin ) );
  //  assert( ( y >= - field.margin ) && ( y <= field.height + field.margin ) );

  worldToScreenCoordinates( x, y, z, &xi, &yi );
  if(!frame->interlaced)
    {
      yi = yi / 2.0;
    }
  xc = static_cast<unsigned int>( floor( xi ) );
  yc = static_cast<unsigned int>( floor( yi ) );

#ifdef XXDEBUG
  std::cout << "VideoInterpreter2D::interpolatePixel called: frame->buffer" << (void*)frame->buffer ;
  std::cout << "(x,y)->(xc,yc): (" << x << "," << y << ")->(" << xc <<  "," << yc << ")\n";
#endif 

  if ( ( (int) xc > 0 ) && ( xc < frame->width-1 ) && ( (int) yc > 0 ) && ( yc < frame->height-1 ) )
    {
      assert( ( (int)xc >= 0 ) && ( xc < frame->width ) );
      assert( ( (int)yc >= 0 ) && ( yc < frame->height ) );
      
      FrameBufferIterator fIter( const_cast< FrameBuffer * > ( frame ), yc, xc );
      fIter.getPixel( & neighbors[ TOP_LEFT ] );
      fIter.goRight();
      fIter.getPixel( & neighbors[ TOP_RIGHT ]);
      fIter.goUp();
      fIter.getPixel( & neighbors[ BOTTOM_RIGHT ]);
      fIter.goLeft();
      fIter.getPixel( & neighbors[ BOTTOM_LEFT ]);      
      double dummy;
      double xFrac = modf( xi, & dummy );
      double yFrac = modf( yi, & dummy );
      
      if ( ( xFrac < 0.5 ) && ( yFrac < 0.5 ) )
	{
	  neighbors[ PIXEL ] = neighbors[ BOTTOM_LEFT ];
	}
      else if ( ( xFrac > 0.5 ) && ( yFrac < 0.5 ) )
	{
	  neighbors[ PIXEL ] = neighbors[ BOTTOM_RIGHT ];
	}
      else if ( ( xFrac < 0.5 ) && ( yFrac > 0.5 ) )
	{
	  neighbors[ PIXEL ] = neighbors[ TOP_LEFT ];
	}
      else if ( ( xFrac > 0.5 ) && ( yFrac > 0.5 ) )
	{
	  neighbors[ PIXEL ] = neighbors[ TOP_RIGHT ];
	}
      
      neighbors[ PIXEL ].red = Geometry::clamp( 0, neighbors[ PIXEL ].red, 255 );
      neighbors[ PIXEL ].green = Geometry::clamp( 0, neighbors[ PIXEL ].green, 255 );
      neighbors[ PIXEL ].blue = Geometry::clamp( 0, neighbors[ PIXEL ].blue, 255 );
      success = true;
    }
  else
    {
#ifdef XXDEBUG
      std::cout << "VI2D::interpolatePixel xc " << xc << "," << yc << " is out of range " << frame->width << "," << frame->height << std::endl;
#endif
      neighbors[ PIXEL ] = Pixel( 0,0,0 );
    }
  *pixel = neighbors[ PIXEL ];
  return success;
}
#endif


#if 0
/* Jacky Interpolation */
void
VideoInterpreter2D::interpolatePixel( FrameBuffer const * frame, double const x, double const y, double const z, RawPixel * pixel )
{
  int xc;
  int yc;

  enum Neighbors
    {
      BOTTOM_LEFT = 0,
      BOTTOM_RIGHT,
      TOP_LEFT,
      TOP_RIGHT,
      BOTTUM_ROW,
      TOP_ROW,
      PIXEL,
      LAST
    };

  RawPixel neighbors[ LAST ];

  double xi, yi;
 
  //  assert( ( x >= - field.margin ) && ( x < field.width + field.margin ) );
  //  assert( ( y >= - field.margin ) && ( y < field.height + field.margin ) );

  worldToScreenCoordinates( x, y, z, &xi, &yi );
  screenToframeCoordinates(xi,yi,&xi,&yi);
  xc = static_cast<unsigned int>( floor( xi ) );
  yc = static_cast<unsigned int>( floor( yi ) );

  if ( ( (int) xc >= 0 ) && ( xc < frame->width ) && ( (int) yc >= 0 ) && ( yc < frame->height ) )
    {
      assert( ( (int)xc >= 0 ) && ( xc < frame->width ) );
      assert( ( (int)yc >= 0 ) && ( yc < frame->height ) );
      
      FrameBufferIterator fIter( const_cast< FrameBuffer * > ( frame ), yc, xc );
      fIter.getPixel( & neighbors[ BOTTOM_LEFT ] );
      fIter.getPixel( & neighbors[ BOTTOM_RIGHT ], frame->bytesPerPixel );
      fIter.getPixel( & neighbors[ TOP_LEFT ], frame->bytesPerLine );
      fIter.getPixel( & neighbors[ TOP_RIGHT ], frame->bytesPerLine + frame->bytesPerPixel );

      double dummy;
      double xFrac = modf( xi, & dummy );
      double yFrac = modf( yi, & dummy );
      
      double d1 = 4.0;
      double d2 = 4.0;
      double d3 = 4.0;
      double d4 = 4.0;

      neighbors[ PIXEL ] = neighbors[ BOTTOM_LEFT ] / d1 + neighbors[ BOTTOM_RIGHT ] /  d2 + neighbors[ TOP_LEFT ] / d3 + neighbors[ TOP_RIGHT ] / d4;

      neighbors[ PIXEL ].red = Geometry::clamp( 0, neighbors[ PIXEL ].red, 255 );
      neighbors[ PIXEL ].green = Geometry::clamp( 0, neighbors[ PIXEL ].green, 255 );
      neighbors[ PIXEL ].blue = Geometry::clamp( 0, neighbors[ PIXEL ].blue, 255 );
      
    }
  else
    {
#ifdef XX_XXDEBUG
      std::cout << "VI2D::interpolatePixel xc " << xc << "," << yc << " is out of range " << frame->width << "," << frame->height << std::endl;
#endif
      neighbors[ PIXEL ] = Pixel( 0,0,0 );
    }
  *pixel = neighbors[ PIXEL ];
}
#endif

unsigned int
VideoInterpreter2D::getHeight( void ) const
{
  return fheight;
}

unsigned int
VideoInterpreter2D::getWidth( void ) const
{
  return fwidth;
}

void
VideoInterpreter2D::interpolateFrame ( FrameBuffer const * inFrame, FrameBuffer * outFrame ) 
{
#ifdef XXDEBUG
  cout << "VideoInterpreter2D::interpolateFrame called with inFrame: " << (void *)inFrame << endl;
#endif
  double x0 = - field.margin;
  double y0 = - field.margin;
  double x1 = field.width + field.margin;
  double y1 = field.height + field.margin;

  double xFactor = ( x1 - x0 ) / outFrame->width;
  double yFactor = ( y1 - y0 ) / outFrame->height;

  FrameBufferIterator outFIter( outFrame );
  int i; double yy;
  for( i = 0, yy  = y0; yy < y1; yy = yy + yFactor, i++ )
    {
      outFIter.goRow( i );
      for( double xx = x0; xx < field.width + field.margin; xx = xx + xFactor, outFIter.goRight() )
	{
	  RawPixel pixel;
	  interpolatePixel( inFrame, xx, yy, 0.0, & pixel );
	  outFIter.setPixel( pixel );
	}
    }
}


void
VideoInterpreter2D::interpolateFrame ( FrameBuffer const * inFrame, FrameBufferWorld * worldFrame, bool blur) 
{
  interpolateFrame(inFrame,worldFrame,0,0,worldFrame->height,worldFrame->width,blur);
}

void VideoInterpreter2D::interpolateFrame ( FrameBuffer const * inFrame, FrameBufferWorld * worldFrame, unsigned int row, unsigned int col, unsigned int endRow, unsigned int endCol, bool blur)
{
#ifdef XXDEBUG
  cout << "VideoInterpreter2D::interpolateFrame called with inFrame: " << (void *)inFrame << endl;
#endif
  FrameBufferIterator outFIter( worldFrame );
  // for each pixel in the world frame.
  for( unsigned int i = row; i < endRow; i++ )
    {
      outFIter.goPosition( i,col );
      for( unsigned int j = col; j < endCol; j++, outFIter.goRight( ) )
	{
	  RawPixel pixel;
	  double x, y;
	  if ( worldFrame->bufferToWorldCoordinates( j, i, &x, &y ) )
	    {
	      std::cerr << "VideoInterpreter2D::interpolateFrame coordinate conversion for " << i << "," << y << " failed\n";
	      abort();
	    }
#ifdef XXDEBUG
	  std::cout << "VI2D::interpolateFrame(..) interpolate pixel at World(" << x << "," << y << ")\n";
#endif
	  if(blur)
	    interpolatePixelBlur( inFrame, x, y, 0.0, & pixel );
	  else
	    interpolatePixel( inFrame, x, y, 0.0, & pixel );
#ifdef XXDEBUG
	  if((x>-3.0 && x < 3.0) || (y>-3.0 && y < 3.0) || (x > field.width -3.0 && x < field.width +3.0 ) || (y > field.height -3.0 && y < field.height + 3.0))
	    {
	      outFIter.setPixel(Pixel(255,0,0));

	    }
	  else
#endif
	    outFIter.setPixel( pixel );
	}
    }
}

#if 0
void
VideoInterpreter2D::createWorldView( FrameBuffer * inFrame, FrameBufferWorld * regionFrame, FrameBufferWorld * wvFrame )
{
  RawPixel pix;

  FrameBufferIterator rFIter;

  memset( wvFrame->buffer, 0x00, wvFrame->frameSize );
  for( unsigned int i = 0; i < regionFrame->height; i++ )
    {
      rFIter.goRow( i );
      for( unsigned int j = 0; j < regionFrame->width; j++, rFIter.goRight() )
	{
	  rFIter->getPixel( & pix );
	  if ( pix.intensity() > 0 )
	    {
	      for( double yy = -50.0; yy < 50.0; yy = yy + 10.0 )
		{
		  for( double xx = -50.0; xx < 50.0; xx = xx + 10.0 )
		    {
		      regionFrame.world
		      interpolatePixel( frame, x, y, 0.0, & pix );
		      wvFrame.setPixel( x, y, pix );
		    }
		}
	    }
	}
    }
  emit newFrame( & wvFrame );
}
#endif

void VideoInterpreter2D::updateWorldToScreenMap( void )
{
  struct Mapping (*map)[MAP_DIMENSION] = new struct Mapping[ MAP_DIMENSION ][ MAP_DIMENSION ];
  assert( map != 0 );

  for( int i = 0; i < MAP_DIMENSION; i++ )
    {
      for( int j = 0; j < MAP_DIMENSION; j++ )
	{
	  double xx, yy;
	  worldToScreenCoordinates( - field.margin + i * xFactor, - field.margin + j * yFactor, 0.0, & xx, & yy, false );
	  
	  map[ i ][ j ].x = xx;
	  map[ i ][ j ].y = yy;
	}
    }
  

  if(worldMap)
    {  
      struct Mapping (*tempMap)[MAP_DIMENSION];
      tempMap = worldMap;
      worldMap = map;
      delete tempMap;
    }
  else
    {
      worldMap = map;
    }
  
#ifdef XXDEBUG
  std::cout << "Updating worldMap\n";
#endif
  
  assert( worldMap != 0 );

}
void VideoInterpreter2D::updateScreenToWorldMap( void )
{
  struct Mapping *smap = new struct Mapping[ fwidth * fheight ];
  assert( smap != 0 );

  for( unsigned int i = 0; i < fheight; i++ )
    {
      for( unsigned int j = 0; j < fwidth; j++ )
	{
	  double xx, yy;
	  screenToWorldCoordinates( j, i, 0.0, & xx, & yy , false);
	  
	  smap[ i * fwidth +  j ].x = xx;
	  smap[ i * fwidth +  j ].y = yy;
	}
    }
  if( screenMap )
    {
      struct Mapping *tempMap;
      tempMap = screenMap;
      screenMap = smap;	
      delete tempMap;
    }
  else
    {
      screenMap = smap;
    }

#ifdef XXDEBUG
  std::cout << "Updating screenMap\n";
#endif

  assert( screenMap != 0 );

  delete mask;
  mask = new Mask( this );


}


unsigned int VideoInterpreter2D::getFrameHeight( void )
{
  return fwidth;
}

unsigned int VideoInterpreter2D::getFrameWidth( void )
{
  return fheight;
}

void VideoInterpreter2D::loadRotation(std::string filename)
{
  rotationMatrix.loadMatrix(filename);
  updateScreenToWorldMap();
  updateWorldToScreenMap();

}
void VideoInterpreter2D::saveRotation(std::string filename)
{
  rotationMatrix.saveMatrix(filename);
}


std::string VideoInterpreter2D::getRotationFilename( void )
{
  return rotationMatrix.filename;
}

void VideoInterpreter2D::resetControlPoints()
{
  // resets the control points so they appear on the screen.
  int numPoints;
  double xMax, xMin, yMax, yMin;
  double wx,wy,sx,sy;
  double tempX, tempY;
  double bufferX, bufferY;
  double distX,distY;
  double scaleX,scaleY;
  ControlPoint * pointPtr;

  numPoints = rotationMatrix.points.size();
  if(numPoints > 0)
    {
      // get the min and max of the control points.`
      xMax = xMin = rotationMatrix.points.getPointRef(0)->xw;
      yMax = yMin = rotationMatrix.points.getPointRef(0)->yw; 
      for(int i = 0; i < numPoints; ++i)
	{
	  tempX = rotationMatrix.points.getPointRef(i)->xw;
	  tempY = rotationMatrix.points.getPointRef(i)->yw;
	  if(tempX > xMax)
	    xMax = tempX;
	  if(tempX < xMin)
	    xMin = tempX;
	  if(tempY > yMax)
	    yMax = tempY;
	  if(tempY < yMin)
	    yMin = tempY;
	}

      // distribute the points on the screen according
      // their xm,ym.
      bufferX = (double)fwidth * 0.1;
      bufferY = (double)fheight * 0.1;
      distX = xMax - xMin;
      distY = yMax - yMin;
      if(distY == 0)
	distY = 1;
      if(distX == 0)
	distX = 1;

      scaleX = (fwidth  - (2 * bufferX)) /distX;
      scaleY = (fheight - (2 * bufferY))/distY;

      // assign screen points to the control points
      for(int i = 0; i < numPoints; ++i)
	{
	  pointPtr = rotationMatrix.points.getPointRef(i);
	  // get the world x and y
	  wx = pointPtr->xw;
	  wy = pointPtr->yw;
	  // calculate appropiate screen points
	  sx = bufferX + ( (wx - xMin) * scaleX);
	  sy = fheight - (bufferY + ( (wy - yMin) * scaleY)); 
	  pointPtr->xm = sx; 
	  pointPtr->ym = sy; 
	}
      // recompute the rotation matrix.
      rotationMatrix.computeRotationMatrix(&calibration);

    } // end if there are points
  

}
