#include <boost/test/unit_test.hpp>
#include "math/math.hpp"
#include "math/geometry.hpp"
#include <cmath>
#include <iostream>
#include <vector>

using namespace boost::unit_test;
void rounding_test();
void geometry_test();

test_suite*
init_unit_test_suite( int argc, char* argv[] )
{
  test_suite* test = BOOST_TEST_SUITE( "Umbotica Library" );

  test->add( BOOST_TEST_CASE( &rounding_test ) );
  test->add( BOOST_TEST_CASE( &geometry_test ) );
  
  return test;
}

void rounding_test()
{
  BOOST_CHECK_EQUAL( umbotica::symmetricRound(0.5), 1 );
  BOOST_CHECK_EQUAL( umbotica::symmetricRound(1.4), 1 );
  BOOST_CHECK_EQUAL( umbotica::symmetricRound(0.4), 0 );
  BOOST_CHECK_EQUAL( umbotica::symmetricRound(-0.5), -1 );
  BOOST_CHECK_EQUAL( umbotica::symmetricRound(-0.4), 0 );
  BOOST_CHECK_EQUAL( umbotica::symmetricRound(-1), -1 );
  BOOST_CHECK_EQUAL( umbotica::symmetricRound(0), 0 );
  BOOST_CHECK_EQUAL( umbotica::symmetricRound(1), 1 );
 
}

void geometry_test()
{
  // Create a skewed shape
  std::vector<int*> bounds;
  int topLeft[] = {0,10};
  int topRight[] = {10,10};
  int bottomRight[] = {10,0};
  int bottomLeft[] = {0,0};
  bounds.push_back(topLeft);
  bounds.push_back(topRight);
  bounds.push_back(bottomRight);
  bounds.push_back(bottomLeft);
  
  for (int i = 1; i <= 9; i++)
    {
      for (int j = 1; j <= 9; j++)
	{
	  int insidePoint[2];
	  insidePoint[0] = i;
	  insidePoint[1] = j;
	  BOOST_CHECK_EQUAL( umbotica::geometry::isInside(bounds, insidePoint), true );
	}
    }

  for (int i = 0; i <= 10; i++)
    {
      int borderPoint[2];
      borderPoint[0] = 10;
      borderPoint[1] = i;
      BOOST_CHECK_EQUAL( umbotica::geometry::isInside(bounds, borderPoint), true ); // border case is true
    }

  for (int i = -5; i < 0; i++)
    {
      for (int j = 11; j < 15; j++)
	{
	  int outsidePoint[2];
	  outsidePoint[0] = i;
	  outsidePoint[1] = j;
	  BOOST_CHECK_EQUAL( umbotica::geometry::isInside(bounds, outsidePoint), false );
	}
    }

}
