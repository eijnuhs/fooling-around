#ifndef FRAMES_PER_SECOND_H
#define FRAMES_PER_SECOND_H

#include <qmutex.h>
#include <qobject.h>
#include <qtimer.h>
#include <sys/time.h>

class FramesPerSecond : public QObject
{
  Q_OBJECT
 public:
  FramesPerSecond();
  ~FramesPerSecond();

  static long getTimeDiff(struct timeval early, struct timeval late, bool debug = false);

 public slots:
  void recieveFPS(double fps);

 signals:
  void updateFPS(double fps);
  
 private:
  struct timeval prev;
  double fps;
  QMutex mutex;
  QTimer timer;
  private slots:
  void emitFPS();
};

#endif
