#ifndef UM_IMAGE_TYPEDEFS_H
#define UM_IMAGE_TYPEDEFS_H

#include "umimage.hpp"
#include <vigra/rgbvalue.hxx>
#include <inttypes.h>

typedef UmImage<uint8_t> UmGrey8;
typedef UmImage<uint16_t> UmGrey16;
typedef UmImage<int32_t> UmGreyS32;
typedef UmImage<float> UmGreyF;

typedef vigra::RGBValue<uint8_t> RGB24Pixel;
typedef UmImage<RGB24Pixel> UmRGB24;

// signed rgb
typedef vigra::RGBValue<uint8_t> RGB24SPixel;
typedef UmImage<RGB24SPixel> UmRGBS24;

#endif
