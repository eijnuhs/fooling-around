// $Id: videostream.cpp,v 1.1.1.1.2.17 2004/12/28 19:30:05 cvs Exp $
//

#include "videostream.h"
#include "filevideodevice.h"
#include "v4l2device.h"
#include <iostream>
#include "framebufferrgb565.h"
#include "segmentation.h"
#include "videointerpreter2d.h"
#include "videocalibration.h"
#include "videostreamconfig.h"
#include "framespersecond.h"
#include "tracker.h"


#ifdef DEBUG_PTF
#include <iostream>
using namespace std;
#endif

VideoStream::VideoStream( std::string driver, std::string name, std::string input, std::string standard, int fps, int width, int height, int depth ) : done( false ), device( 0 ), videoInterpreter2D( 0 ), tracker(0)
{
  initialize(driver,name,input,standard,fps,width,height,depth);
}

VideoStream::VideoStream(VideoStreamConfig * vsc) : done( false ), device( 0 ), videoInterpreter2D( 0 ), tracker(0)
{
  if(vsc)
    {
      std::string driver = vsc->getString("driver", "V4L2");
      std::string name = vsc->getString("name", "/dev/v4l/video0");
      std::string input = vsc->getString("input", "Composite0");
      std::string standard = vsc->getString("standard", "NTSC");
      int fps = vsc->getUnsigned("fps", 30, 10);
      int width = vsc->getUnsigned("width", 640, 10);
      int height = vsc->getUnsigned("height", 240, 10);
      int depth = vsc->getUnsigned("depth", 16, 10);

      initialize(driver,name,input,standard,fps,width,height,depth);
    }
  else
    {
      std::cerr << "VideoStream::constructor error: initialized with null configuration object.\n";
      exit();
    }

}

void VideoStream::initialize( std::string driver, std::string name, std::string input, std::string standard, int fps, int width, int height, int depth )
{
  if ( driver == "V4L2" )
    {
      device = new V4L2Device( name, input, standard, fps, width, height, depth );
      if ( device == 0 )
	{
	  std::cerr << "ERROR: unable to create V4L2 video device";
	  perror(":");
	  std::exit( EXIT_FAILURE );
	}
      else if ( device->getError() )
	{
	  std::cerr << "ERROR: V4L2 video device creation failed, error code = " << endl;
	  perror(":");
	  std::exit( EXIT_FAILURE );
	}
    }
   else if( driver == "file" )
     {
       device = new FileVideoDevice(name);
     }
   else
     {
       std::cout << "Unrecognized device: " << driver << std::endl;
       std::cout << "Supported: V4L2, file\n";
       ::exit(1);
     }

  ipFrame.initialize( FrameBuffer::RGB565, 320, 240 );
    if ( gettimeofday( & prev, 0  ) != 0 )
    {
      std::cerr << "Videostream gettimeofday failed\n";
      perror( "VideoStream gettimeofday ");
    }

}

void VideoStream::sendFPSupdate()
{
  struct timeval now;
  double timeDiff;
  double fps;

  if ( gettimeofday( & now, 0  ) != 0 )
    {
      std::cerr << "VideoStream::updateFPS gettimeofday failed\n";
      perror( "VideoStream::updateFPS gettimeofday ");
    }

  timeDiff = FramesPerSecond::getTimeDiff(prev,now);

  memcpy( & prev, & now, sizeof( struct timeval ) );
  //  fps = (fps * 0.8) + ((1000000.0/timeDiff)*0.2);
  fps = (1000000.0/timeDiff);
  emit fpsUpdate(fps);
}
  

void VideoStream::run( void )
{
  device->startCapture( );
  FrameBufferRGB565 frame;
  done = false;
  device->nextFrame( & frame );
  while( ! done )
    {
      //      std::cout << "Locking: " << (void*)&frame << "\n";
      frame.mutex->lock();
      device->releaseCurrentBuffer( );
      device->nextFrame( & frame );
      frame.mutex->unlock();
      //      std::cout << "UnLocking: " << (void*)&frame << "\n";

      vCalibration->preprocessFrame( & frame );

      emit newFrame( & frame );
      sendFPSupdate();
    }
  device->stopCapture( );
}


bool 
VideoStream::isInterlaced( void )
{
  return device->isInterlaced( );
}


VideoDevice *
VideoStream::getVideoDevice() const
{
  return device;
}

void
VideoStream::setVideoInterpreter2D( VideoInterpreter2D * vi2d )
{
  videoInterpreter2D = vi2d;
}

VideoCalibration * 
VideoStream::getVideoCalibration( void )
{
  return vCalibration;
}
void 
VideoStream::setVideoCalibration(VideoCalibration * vCalibration )
{
#ifdef DEBUG_PTF
  std::cout << "VideoStream::VideoCalibrationViewer::setVideoCalibration\n";
  std::cout << "vCal = " << (void *) vCalibration << std::endl;
#endif
  this->vCalibration = vCalibration;
}


void VideoStream::setTracker(Tracker * trak)
{
  tracker = trak;
}
