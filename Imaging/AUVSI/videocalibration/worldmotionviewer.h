#ifndef __WORLD_MOTION_VIEWER_H
#define __WORLD_MOTION_VIEWER_H
#include "framebufferviewer.h"

class QPainter;
class FrameBuffer;

typedef enum WMV_modes{
  WMV_NONE = 0,
  WMV_MEAN,
  WMV_VARIANCE
} WMV_MODE;

class WorldMotionViewer : public FrameBufferViewer
{
 public:
  WorldMotionViewer( QWidget * parent = 0, char const * name = 0 );
  ~WorldMotionViewer();
  
  void newFrame(FrameBuffer * );
  void paintChild(QPainter & p);
  WMV_MODE mode;
};

#endif
