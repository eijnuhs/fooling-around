/* $Id: v4ldevice.h,v 1.1.1.1 2005/04/25 03:02:45 cvs Exp $
 *
 */

#ifndef _V4L_CLASS_H_
#define _V4L_CLASS_H_

#include "VideoDevice.h"
#include <linux/videodev.h>

class V4LDevice:public VideoDevice
{
public:
  V4LDevice (char *devname);
   ~V4LDevice (void);
  int init (int fps,
	    int brightness,
	    int colorSat,
	    int contrast,
	    int hue,
	    int gain,
	    char *input,
	    int field, char *standard, int width, int height, int depth);
  int startCapture (void);
  int stopCapture (void);
  int nextFrame (class FrameBuffer *frameBuffer);
  int releaseCurrentBuffer (void);
  int setFrame (class FrameBuffer *frame);

  static void printCapabilities (FILE * fp, struct video_capability cap);
  static void printCaptureWindow (FILE * fp, struct video_window win);
  static void printChannel (FILE * fp, struct video_channel channel);
  static void printPicture (FILE * fp, struct video_picture pict);
  static void printMMap (FILE * fp, struct video_mbuf mbuf);

private:
  struct video_window win;
  struct video_mbuf mbuf;
  struct video_mmap map;
};

#endif
