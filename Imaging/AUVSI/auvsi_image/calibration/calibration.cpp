// $Id: calibration.cpp,v 1.1.1.1.2.3 2004/11/18 21:54:20 cvs Exp $

using namespace std;

#include <iostream>
#include <fstream>
#include "calibration.h"
#include <math.h>
#include <stdlib.h>
#include <errno.h>
#include "matrix/matrix.h"
#include "controlpoints.h"
#include "square.h"

Calibration::Calibration ()
{
}

Calibration::Calibration ( std::string calibrationPointsFilename )
{
  loadCalibrationPoints( calibrationPointsFilename );
}

Calibration::~Calibration ()
{
}

struct ControlPoint *
Calibration::controlPoint (int x, int y)
{
  struct ControlPoint *p;
  p = points.selectPoint (x, y);

  return p;
}

void
Calibration::loadCalibrationPoints ( std::string filename)
{
  char buffer[1024];
  double xw, yw, zw, xm, ym;

  ifstream fin (filename.c_str ());

  if (!fin)
    {
      cerr << "Calibration::loadCalibrationPoints() unable to open file " << filename <<
	endl;
      exit (1);
    }
  while (fin.getline (buffer, sizeof (buffer) - 1))
    {
      if ((buffer[0] == '#') || (buffer[0] == ';'))
	{
	  continue;
	}
      if (sscanf
	  (buffer, "%lf %lf %lf %lf %lf", & xw, & yw, & zw,
	   & xm, & ym) != 5)
	{
	  cerr << "Unable to parse camera point on line\n";
	  cerr << buffer << endl;
	  continue;
	}
      points.addPoint ( ControlPoint( xw, yw, zw, xm, ym, 0.0 ) );
    }
  pointsFile = filename;
}

void
Calibration::newCalibrationPoints (ControlPoints points)
{
  this->points = points;
  recalibrate ();
}

void
Calibration::newCalibrationPoints (list < Square > squares)
{

  points.clear ();
  for (list < struct Square >::const_iterator it = squares.begin ();
       it != squares.end (); ++it)
    {
      Square square = *it;
      if ((square.block_x != -1) && (square.block_y != -1))
	{
	  points.addPoint ( ControlPoint( square.xr, square.yr, 0.0, square.x, square.y, 0.0 ) );
	}
    }
  recalibrate ();
}

void
Calibration::loadCalibrationData (string filename)
{
  ifstream fin (filename.c_str ());

  if (!fin)
    {
      cerr << "TPC::loadCalibrationData unable to open file " << filename <<
	endl;
      exit (1);
    }
  cerr << "Unimplemented method loadCalibrationData\n";
}

void
Calibration::saveCalibration (string filename)
{
  unlink (filename.c_str ());
  ofstream fout (filename.c_str ());

  if (!fout)
    {
      cerr << "TPC::saveCalibration unable to open file " << filename << endl;
      // exit (1);
      return;
    }
  fout << "# Automatically generated file (Do not edit)\n";

  for (int i = 0; i < points.size (); i++)
    {
      ControlPoint point = points.getPoint (i);
      fout << point.xw << "\t"
	<< point.yw << "\t"
	<< point.zw << "\t" << point.xm << "\t" << point.ym << "\t" << endl;
    }
  fout.close ();
}

void
Calibration::generateCalibrationPoints (void)
{
  points.clear ();

  points.addPoint ( ControlPoint( 1000.0, 1180.0, 0.0, 232.5, 282.375, 0.0 ) );
  
  points.addPoint ( ControlPoint( 1057.95, 1181.0, 0.0, 252.25, 282.5, 0.0 ) );
  
  points.addPoint ( ControlPoint( 1000.0, 1120.0, 0.0, 232.0, 310.0, 0.0 ) );

  points.addPoint ( ControlPoint( 1000.0, 1060.0, 0.0, 232.0, 338.0, 0.0 ) );

  points.addPoint ( ControlPoint( 1000.0, 1000.0, 0.0, 231.0, 365.0, 0.0 ) );

  points.addPoint ( ControlPoint( 1300.0, 1000.0, 0.0, 367.0, 367.0, 0.0 ) );

  points.addPoint ( ControlPoint( 1298.65, 1060.58, 0.0, 369.0, 340.0, 0.0 ) );

  points.addPoint ( ControlPoint( 1117.0, 1118.0, 0.0, 286.0, 312.0, 0.0 ) );

  points.addPoint ( ControlPoint( 1180.0, 1000.0, 0.0, 313.0, 368.0, 0.0 ) );

  points.addPoint ( ControlPoint( 1173.0, 1180.0, 0.0, 314.0, 342.0, 0.0 ) );

  recalibrate ();
}

std::string Calibration::getPointsFile( void )
{
  return pointsFile;
}

