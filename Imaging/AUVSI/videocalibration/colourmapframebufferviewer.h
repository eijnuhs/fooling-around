#ifndef _COLOUR_MAP_FRAME_BUFFER_VIEWER_H_
#define _COLOUR_MAP_FRAME_BUFFER_VIEWER_H_

#include "framebufferviewer.h"
class ColourMap;


class ColourMapFrameBufferViewer : public FrameBufferViewer
{
  Q_OBJECT
 public:
  ColourMapFrameBufferViewer( QWidget * parent = 0, char const * name = 0 );
 ~ColourMapFrameBufferViewer();

  virtual void updateQImageFromFrameBuffer( FrameBuffer * frame );
  virtual void mousePressEvent( QMouseEvent * );
  virtual void mouseMoveEvent( QMouseEvent * e );
  void updateColour(QMouseEvent * e);
  void setColourMap(ColourMap * cd);
  void updateView();
 public slots:
  void nextColour();
  void previousColour();
  void addColour();
  void loadColours();
  void saveColours();
 signals:
  void newName(const QString & name);
  void newColour(const QColor & c);
 private:
  ColourMap * cMap;
};

#endif
