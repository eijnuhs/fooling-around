#ifndef THREAD_WRAPPER_H
#define THREAD_WRAPPER_H

#include <boost/thread.hpp>
#include <boost/bind.hpp>
// a superclass to simplify thread management.

namespace umbotica { namespace threads {
  class ThreadWrapper
  {
  public:
    ThreadWrapper() : m_thread(NULL), m_done(true){}
    virtual ~ThreadWrapper()
    {
      stopThread();
    }
    void startThread()
    {
      if(!m_thread)
	{
	  m_done = false;
	  init();
	  m_thread = new boost::thread(boost::bind(&umbotica::threads::ThreadWrapper::processingLoop,this));
	}
    }

    void stopThread()
    {
      if(m_thread)
	{
	  m_done = true;
	  m_thread->join();
	  die();
	}
    }

    // this function is called in the processing loop
    // this should not be a loop but rather the processing
    // to be executed in the loop
    //
    virtual void processing() = 0;
    
    virtual void init(){}
    virtual void cleanUp(){}
    bool done(){return m_done;}
  protected:
    void die()
    {
      if(m_thread)
	{
	  m_done = true;
	  cleanUp();
	  delete m_thread;
	  m_thread = 0;
	}
    }
  private:
    
    virtual void processingLoop()
    {
      while(!done())
	{
	  processing();
	}
    }

 
    boost::thread * m_thread;
    volatile bool m_done;
  };
}} // namespace umbotica::threads


#endif

