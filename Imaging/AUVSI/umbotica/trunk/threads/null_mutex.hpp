#ifndef UMBOTICA_NULL_MUTEX_HPP
#define UMBOTICA_NULL_MUTEX_HPP

// a null mutex object to use as a mutex poicy in single threaded apps.

namespace umbotica { namespace threads {

	class null_scoped_lock;

	class null_mutex
	{
	public:
		typedef null_scoped_lock scoped_lock;
	};

	class null_scoped_lock
	{
	public:
		null_scoped_lock(null_mutex & mutex)
		{}
		void unlock(){}
		bool isLocked() { return false;}
	};


}} // namespace umbotica::threads

#endif
