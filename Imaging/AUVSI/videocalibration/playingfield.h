/*! $Id: playingfield.h,v 1.1.1.1.2.1 2005/01/23 05:24:37 cvs Exp $
 * Class to encapsulate information about the playing field
 */

#ifndef _PLAYINGFIELD_H_
#define _PLAYINGFIELD_H_

class PlayingField
{
public:
  PlayingField ( double w = 2740.0, double h = 1520.0, double m = 200.0 );

  bool isInside( double x, double y );
  double getFullHeight();
  double getFullWidth();

  double width;
  double height;
  double margin;
};

#endif /* _PLAYING_FIELD_H_ */
