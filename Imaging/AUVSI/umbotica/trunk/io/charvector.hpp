#ifndef CHAR_VECTOR_H
#define CHAR_VECTOR_H

#include <string>
#include <vector>
#include <iostream>

inline std::string toString(const std::vector<char> & rhs)
{
  std::string str;
  std::vector<char> temp = rhs;
  replace(temp.begin(), temp.end(), '\0', '_');
  str.insert(str.begin(),temp.begin(), temp.end());
  str.push_back('\0');
  return str;
}

inline std::vector<char> fromString(const std::string & rhs)
{
  std::vector<char> val;
  val.insert(val.begin(), rhs.begin(), rhs.end());
  return val;
}

inline std::vector<char> fromBuffer(const char * buffer, unsigned int length)
{
  std::vector<char> val;
  val.insert(val.begin(), buffer, buffer + length);
  return val;
}


inline std::ostream & operator<<(std::ostream & str, const std::vector<char> & rhs)
{
  for(unsigned int i = 0; i < rhs.size(); ++i)
    str << rhs[i];
  return str;
}


template<typename T>
inline T extractFB(std::vector<char>::iterator vecPos)
{
  unsigned int bytes = sizeof(T);
  char temp[bytes];
  for(int i = bytes - 1; i >= 0; --i, vecPos++)
      temp[i] = *vecPos;

  return *reinterpret_cast<T*>(temp);
}

// extracts this type from the bits of the raw data
template<typename T>
inline T extract(std::vector<char>::iterator vecPos)
{
  unsigned int bytes = sizeof(T);
  char temp[bytes];
  for(unsigned int i = 0; i < bytes; ++i, vecPos++)
      temp[i] = *vecPos;

  return *reinterpret_cast<T*>(temp);
}



// extracts this type from the bits of the raw data
// flips the order of the bits for conversion.
template<typename T, typename CHAR_ITERATOR_TYPE>
inline CHAR_ITERATOR_TYPE extractFB(CHAR_ITERATOR_TYPE vecPos, T & retVal)
{
  unsigned int bytes = sizeof(T);
  char temp[bytes];
  for(int i = bytes - 1; i >= 0; --i, vecPos++)
      temp[i] = *vecPos;

  retVal = *reinterpret_cast<T*>(temp);
  return vecPos;
}

// extracts this type from the bits of the raw data
template<typename T, typename CHAR_ITERATOR_TYPE>
inline CHAR_ITERATOR_TYPE extract(CHAR_ITERATOR_TYPE vecPos, T & retVal)
{
  unsigned int bytes = sizeof(T);
  char temp[bytes];
  for(unsigned int i = 0; i < bytes; ++i, vecPos++)
      temp[i] = *vecPos;

  retVal = *reinterpret_cast<T*>(temp);
  return vecPos;
}


#endif
