CWD = $$system("pwd")
message( Including the OpenCV Wrapper library from path $$CWD)
DEPENDPATH += $${CWD}/src $${CWD}/src/pixels
INCLUDEPATH += $${CWD}/src $${CWD}/src/pixels /usr/include/  /usr/local/include/opencv $${UTILS_INCLUDE_PATH}

MACHINENAME = $$(MACHINE)
equals(MACHINENAME, "sancho") {
INCLUDEPATH += /usr/local/include/boost-1_33/
QMAKE_LFFLAGS = -headerpad_max_install_names
LIBS += -L/sw/lib -bind_at_load
}

CONFIG_LIB_PATH  = $${CWD}/../configuration
UTILS_INCLUDE_PATH = $${CWD}/../umbotica

LIBS += -L/usr/local/lib -L/usr/lib -lboost_thread  -lcv -lcxcore -lcvaux -lvigraimpex -ltiff 
HEADERS += umimage.hpp imageformats.hpp umimagelocker.hpp umimagetypedefs.hpp

# include the videostream files
# by adding VIDEOSTREAM = 1 to your project file
!isEmpty(VIDEOSTREAM){

message(Including the OpenCV Wrapper optional videostream component)

include( $${CONFIG_LIB_PATH}/configurationinclude.pro )
!exists( $${CONFIG_LIB_PATH}/configurationinclude.pro ){
  error( Configuration library not found...It may be checked out from the sculpin svn )
}

SOURCES	+= videostreamconfig.cpp v4l2device.cpp videodevice.cpp 
HEADERS += videostream.h videostreamconfig.h v4l2device.h videodevice.h 

}
isEmpty(VIDEOSTREAM){
	# some compatibility parameters.
DEFINES += V4L_COMPATIBILITY
}

!isEmpty(UMIMAGEGUI){
message(Including the OpenCv Wrapper AutoGui components.  Make sure the AutoGui files are included.)
HEADERS += umimageagcontainer.hpp umimagegui.hpp
}
