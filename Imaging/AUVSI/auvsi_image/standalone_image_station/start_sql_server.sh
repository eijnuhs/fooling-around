#!/bin/bash
# Switching to root and starting the sql server

ROOT_UID=0
E_WRONG_USER=65

if [ "$UID" -ne "$ROOT_UID" ]
then
    echo;
    echo "You must be root to run this script.";
    echo;
    exit $E_WRONG_USER
fi

mysqld_safe --user=mysql &
